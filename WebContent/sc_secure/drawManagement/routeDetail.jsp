<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/sc_secure/drawManagement/RouteDetail.java" --%><%-- /jsf:pagecode --%><%-- tpl:insert page="/theme/JSP-C-blue_mod.jtpl" --%><!DOCTYPE HTML>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%
	OmnitureTrackingCode otc = (OmnitureTrackingCode) session
			.getAttribute("omnitureTracking");
	if (otc == null) {
		otc = new OmnitureTrackingCode();
	}
	String omnitureP1 = otc.getTrackingCodePart1();

	// following work around due to bug in RAD
	String useMap = "#map_800x125_top";
	String headURL = "/scweb/index.usat";
%>
<!-- NEW path -->


<link rel="stylesheet" href="/scweb/theme/Master.css" type="text/css">
<link href="/scweb/theme/C_master_blue.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/scweb/theme/stylesheet.css"
	title="Style">

<script src="/scweb/scripts/windowutility.js"></script>
<script language="JavaScript">
	// method called on page load
	function _initPage() {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this

		if (typeof window.initPage == "function") {
			window.initPage();
		}

	}

	// method called on page unload
	function _unloadPage(thisObj, thisEvent) {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this
		if (typeof window.unloadPage == "function") {
			window.unloadPage(thisObj, thisEvent);
		}

	}

	// method called on page unload
	function _onbeforeunloadPage(thisObj, thisEvent) {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this
		if (typeof window.onbeforeunloadPage == "function") {
			window.onbeforeunloadPage(thisObj, thisEvent);
		}

	}
</script>

<%=omnitureP1%>

<%-- tpl:put name="headarea" --%>
<TITLE>Route # ${route.route.routeID} Detail</TITLE>
<LINK rel="stylesheet" type="text/css"	href="${pageContext.request.contextPath}/theme/tabpanel.css" title="Style">

<script src="${pageContext.request.contextPath}/scripts/scriptaculous/prototype.js" type="text/javascript"></script>
<%	
	String openPrint = "false";
	String focusTab = "";
	Object o = request.getAttribute("openPrintWindow");
	Object openRDLTab = request.getAttribute("openRDLTab");
	String alertMsg = (String)request.getAttribute("openRDLPrintFailureAlert");
	
	if ((o != null) && ((String)o).equals("true")) {
		openPrint = "true";
	}
	if ((openRDLTab != null) && ((String)openRDLTab).equals("true")) {
		focusTab = "thisObj.initialPanelId='form1:tabbedPanelWeeklyReturnPanel:bfpanelReturnDrawForDay';";
	}
	//
	// check for alert message
	if (alertMsg != null && alertMsg.trim().length() > 0){
		alertMsg = "alert('" + alertMsg + "');";
	}
	else {
		alertMsg = "";
	}

	com.usatoday.champion.handlers.RouteHandler route  = (com.usatoday.champion.handlers.RouteHandler)session.getAttribute("route");
	String currentMrkDstRteKey = route.getRoute().getMarketID() + route.getRoute().getDistrictID() + route.getRoute().getRouteID();
	
 %>
<SCRIPT type="text/javascript">

// change Omniture Default Page name
s.pageName = "SC Route Detail";
s.channel = "Contractor Route Management";
				
function initPage() {
	if (<%=openPrint%>) {
		setTimeout("openPrintWindow()", 200);
	}
	<%=alertMsg %>
}

function openPrintWindow() {
	 printWindow = window.open('/scweb/sc_secure/drawManagement/forday/printerFriendly.jsp?useprintonly=Y', 'printWin', 'width=725,height=650,status=no,scrollbars=yes,resizable=yes,toolbar=yes');
	 if (!printWindow || printWindow.closed) {
	 	alert ('We were unable to open the print window. This web application requires the use of pop-up windows. You must configure your browser pop-up blocker to allow this site, usatodaycustomer.com, to allow pop-ups. Look for a banner at the top of this page to configure your pop-up blocker. Detailed information on this topic can be found by clicking the Help icon.');
	 }
}

var cancelDailyOpen = false;
var cancelWeeklyReturnOpen = false;
var cancelWeeklyReturnDrawOpen = false;
var badDateMsg = "alert('Selected date is outside of the valid range. Use the calendar to select a valid date.')";

var rtekey = "<%=currentMrkDstRteKey %>";

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'

// use one below for single page
// RDL for Day

	thisObj.disabled = true;
	if (cancelDailyOpen) {
		setTimeout(badDateMsg, 100);
		return false;
	}
	
	
	var row = thisObj.parentNode;
	while(row.nodeName != "TR") {
		row = row.parentNode;
	}
	
	var dateField = $('form1:textReturnDrawChosenDate');
	var selectedDate = dateField.value;

	// add more sophisticated date validation logic
	if (selectedDate.length <= 0) {
		setTimeout(badDateMsg, 100);
		thisObj.disabled = false;		
	}
	else {

		var urlToOpen = '/scweb/sc_secure/drawManagement/forday/loadReturnAndDrawForDate.usat?d=' + selectedDate + '&rteid=' + rtekey;

		if(rdlWindow && rdlWindow.open && !rdlWindow.closed)  {
			rdlWindow.focus();
			thisObj.disabled = false;			
		}
		else {
			rdlWindow = window.open(urlToOpen, 'rdlWindow', 'status=yes,scrollbars=yes,resizable=yes');
			thisObj.disabled = false;
		}
	}
	
	return false;
}



function openWeeklyDrawReturnWindow(thisObj, thisEvent, thisDate, thisPub, viewOnly) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
// use one below for single page

var urlToOpen = '/scweb/sc_secure/drawManagement/forweek/loadReturnAndDrawForWeek.usat?d=' + thisDate + '&pc=' + thisPub + '&v=' + viewOnly + '&rteid=' + rtekey;


if(weeklyRDLWindow && weeklyRDLWindow.open && !weeklyRDLWindow.closed)  {
	weeklyRDLWindow.focus();
}
else {
	weeklyRDLWindow = window.open(urlToOpen, 'weekRdlWindow', 'status=yes,scrollbars=yes,resizable=yes' );
}
return false;
}

var weeklyRetDrawSubmitButton = null;
// Initiates the Weekly Draw feature
function func_7(thisObj, thisEvent, thisPub, viewOnly) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'

// get value of date picker and go

	var row = thisObj.parentNode;
	while(row.nodeName != "TR") {
		row = row.parentNode;
	}
	
	var selectedDate = '';
	var dateFieldError = false;
		
	var rowFields = row.getElementsByTagName("input");
	for (i = 0; i < rowFields.length; i++) {
		var rowFieldCurrent = rowFields[i];
		if ( (rowFields[i].type == "text") && (rowFields[i].id.indexOf('_HXB_GHOST') == -1) ) {
			selectedDate = rowFields[i].value;
			dateFieldError = $(rowFields[i]).hasClassName('inputText_Error');
			break;
		}
	}	

	// if date field is in error then abort
	if (dateFieldError) {
		setTimeout(badDateMsg, 100);
		return false;
	}
	
	// add more sophisticated date validation logic
	if (selectedDate.length <= 0) {
		alert('Please choose a date first.');
	}
	else {
		thisObj.disabled = true;
		if (weeklyRetDrawSubmitButton == null) {
			weeklyRetDrawSubmitButton = $$('input.weeklyReturnDrawButton');
		}
	
		for (i = 0; i < weeklyRetDrawSubmitButton.length; i++) {
			weeklyRetDrawSubmitButton[i].disabled = true;
		}

		openWeeklyDrawReturnWindow(thisObj, thisEvent, selectedDate, thisPub, viewOnly);
	
		for (i = 0; i < weeklyRetDrawSubmitButton.length; i++) {
			weeklyRetDrawSubmitButton[i].disabled = false;
		}
	}
	
	return false;
}
function func_8(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
return false;
}
function func_9(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
helpScreen('/scweb/sc_secure/drawManagement/drawManagementHelp.usat'); 
if (helpWindow) {
	helpWindow.focus();
}
return false;
}
function func_10(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
window.status='Click for Help On This Page';

}
function func_11(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
window.status='';
}

// daily on error
function func_12(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	if (!cancelDailyOpen) {
		cancelDailyOpen = true;
		setTimeout(badDateMsg, 50);
		if (rdlWindow) {
			rdlWindow.close();
		}
	}
}

// daily onsuccess
function func_13(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
cancelDailyOpen = false;
}

// Weekly Returns error
function func_14(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	if (!cancelWeeklyReturnOpen) {
		cancelWeeklyReturnOpen = true;
		setTimeout(badDateMsg, 50);
		if (weeklyRDLWindow) {
			weeklyRDLWindow.close();
		}
	}
}
// Weekly Returns onsuccess
function func_15(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
cancelWeeklyReturnOpen = false;
}

// Weekly Returns & Draw onerror
function func_16(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	if (!cancelWeeklyReturnDrawOpen) {
		cancelWeeklyReturnDrawOpen = true;
		setTimeout(badDateMsg, 50);
		if (weeklyRDLWindow) {
			weeklyRDLWindow.close();
		}
	}
}
// Weekly Returns & Draw onsuccess
function func_17(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
cancelWeeklyReturnDrawOpen = false;
}
function func_18(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	if (cancelDailyOpen) {
		setTimeout(badDateMsg, 100);
		return false;
	}
	
	if (printWindow != null && printWindow.open && !printWindow.closed) {
		printWindow.close();
	}
	window.status='Please Wait...';
	thisObj.value = 'Please Wait...';

	return true;
}
function func_19(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword this
//use 'thisEvent' to refer to the event generated instead of keyword event

//This event handler will be called before displaying the tabbed panel.
//If the id of a panel is returned then that will be initial panel displayed, if not the first panel will be displayed.
<%=focusTab %>
}</SCRIPT>
	<%-- /tpl:put --%>
<script language="javascript">
	//var logoutMsg = 'Please close all Draw Management windows before logging out';
	function cleanupWindows() {
		// switch to:
		if (rdlWindow != null) {
			//if (rdlWindow.closed == false) {
			if (rdlWindow.open && !rdlWindow.closed) {
				rdlWindow.setForceClose();
				rdlWindow.close();
			}

		}

		if (weeklyRDLWindow != null) {
			if (weeklyRDLWindow.open && !weeklyRDLWindow.closed) {
				weeklyRDLWindow.setForceClose();
				weeklyRDLWindow.close();
			}
		}

		if (helpWindow != null) {
			if (helpWindow.open && !helpWindow.closed) {
				helpWindow.close();
			}
		}

		if (printWindow != null) {
			if (printWindow.open && !printWindow.closed) {
				printWindow.close();
			}
		}

		return true;
	}

	function okToLogout() {
		var alert = false;
		if (rdlWindow != null) {
			if (rdlWindow.open && !rdlWindow.closed) {
				alert = true;
			}
		}

		if (weeklyRDLWindow != null) {
			if (weeklyRDLWindow.open && !weeklyRDLWindow.closed) {
				alert = true;
			}
		}

		if (alert) {
			setTimeout(
					'alert("You must close all Draw Management windows before logging out.")',
					100);
			return false;
		} else {
			cleanupWindows();
			return true;
		}
	}

	function func_logoutOK(thisObj, thisEvent) {
		//use 'thisObj' to refer directly to this component instead of keyword 'this'
		//use 'thisEvent' to refer to the event generated instead of keyword 'event'
		return okToLogout();
	}
	function getCurrentYear() {
		today = new Date();
		y0 = today.getFullYear();
		return y0;
	}
</script>
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="charset=ISO-8859-1">
</head>
<f:view locale="#{userLocaleHandler.locale}">
	<f:loadBundle basename="resourcesFile" var="labels" />
	<body onLoad="_initPage();" onUnload="_unloadPage(this, event);"
		onbeforeunload="_onbeforeunloadPage(this, event);">
		<hx:scriptCollector id="templateScriptCollector">


			<!-- start header area -->
			<div class="topAreaBox1"></div>
			<div class="topAreaLogo">
				<!-- 				<img border="0" src="/scweb/theme/images/800x125_top.jpg" -->
				<%-- 					width="800" height="125" usemap="<%=useMap %>"> --%>
				<img border="0" src="/scweb/theme/images/USATODAY_Header_940x145.jpg"
					width="940" height="145" usemap="<%=useMap%>">
				<map name="map_800x125_top">
					<area shape="rect" target="_self" href="<%=headURL%>"
						coords="19,17,268,112">
					<area shape="default" nohref>
				</map>
			</div>
			<div class="topAreaLogoLine">
				<img border="0" src="/scweb/theme/images/800x20_art2.jpg" height="20"
					width="800">
			</div>
			<!-- end header area -->

			<!-- start header navigation bar -->
			<div class="topNavBk"></div>${omnitureTracking.trackingCodePart2}<div
				class="topNav"></div>
			<div class="topAreaDatePlacement">
				<fmt:formatDate value="${pc_JSPCblue_mod.now}" dateStyle="full" />
			</div>
			<h:outputText styleClass="globalMessageDivStyle" id="textGlobalMessageText"
				value="#{globalMessage.message}"
				rendered='#{ (globalMessage.showMessage) and ((globalMessage.showToWhom eq "all") or ((globalMessage.showToWhom eq "hotel") and userHandler.canWorkWithBlueChipDraw ) or ((globalMessage.showToWhom eq "contractor") and userHandler.canWorkWithRoutes) )}'></h:outputText>
			<!-- end header navigation bar bar -->

			<!-- start left-hand navigation -->
			<table class="mainBox" border="0" cellpadding="0" width="100%" height="87%"
				cellspacing="0">
				<tbody>
					<tr>
						<td class="leftNavTD" align="left" valign="top">
							<div class="leftNavBox">
								<h:form styleClass="form" id="formLogout">
									<%-- tpl:put name="LeftNavTopMostJSPContentArea" --%><%-- /tpl:put --%>
									<div id="logoutLinkDiv" class="noDisplay" style="${userHandler.divCss}">
										<h:outputFormat styleClass="outputFormat" id="formatWelcomeMessage"
											value="Welcome, {0}">
											<f:param name="displayName" value="#{userHandler.displayName}"></f:param>
										</h:outputFormat>
										<br>
										<hx:commandExButton type="submit" value="#{labels.logoutLink}"
											styleClass="commandExButtonSmall" id="buttonDoLogoutBtn"
											title="Logout" alt="Logout"
											action="#{pc_JSPCblue_mod.doButtonDoLogoutBtnAction}"
											onclick="return func_logoutOK(this, event);"></hx:commandExButton>
										<hr width="95%" align="left">
									</div>
									<siteedit:navbar spec="/scweb/theme/nav_vertical_tree_left.jsp"
										targetlevel="1-5" onlychildren="true" navclass="leftNav"
										target="topchildren" />
									<br>
									<br>
									<br>
									<!-- If we add internatiolization
								<h:selectOneMenu styleClass="selectOneMenu" id="menui18nPref" value="#{userLocaleHandler.localeStr}">
									<f:selectItem itemValue="en" itemLabel="English" />
									<f:selectItem itemValue="es" itemLabel="Spanish" />
								</h:selectOneMenu><hx:commandExButton type="submit" value="Go"
									styleClass="commandExButton" id="buttonUpdateLangPref"
									action="#{pc_JSPCblue_mod.doButtonUpdateLangPrefAction}"></hx:commandExButton> -->
									<br>
									<hx:inputHelperKeybind key="Enter" id="inputHelperKeybind1"
										targetAction="nothing" />
								</h:form>
								<br>
								<hr width="95%" align="left">
								<b>Related Links:</b><br> <a href="http://usatoday.com"
									target="_blank">USA TODAY.com</a><br> <a
									href="https://service.usatoday.com/welcome.jsp" target="_blank">USA
									TODAY Home Delivery</a><br> <a
									href="https://service.usatoday.com/shop" target="_blank">USA TODAY
									Past Issues</a><br> <br> <br>

							</div>
						</td>
						<!-- end left-hand navigation -->
						<td><img border="0" src="/scweb/theme/img/JSF_1x1.gif" width="1"
							height="1" hspace="6">
						</td>
						<td class="mainContentWideTD" align="left" valign="top" rowspan="2">
							<!-- start main content area -->
							<div class="mainContentWideBox">
								<img border="0" src="/scweb/theme/images/USATODAY_Subhead_740x76.jpg"
									width="740" height="76" usemap="<%=useMap%>">
								<!-- bread crumbs if we want em-->
								<div class="navTrailLoc">
									<siteedit:navtrail start="[" end="]" target="home,parent,ancestor,self"
										separator="&gt;&gt;" spec="/scweb/theme/trail.jsp" />
								</div>
								<a name="navskip"><img border="0" src="/scweb/theme/img/JSF_1x1.gif"
									width="1" height="1" alt="Beginning of page content"> </a>
								<%-- tpl:put name="jspbodyarea" --%>
							
							
								<h:form styleClass="form" id="form1">
									<table border="0" cellpadding="2" cellspacing="1" width="600">
										<tbody>
											<tr>
												<td nowrap="nowrap" align="left" colspan="7">
												<table border="0" cellpadding="2" cellspacing="0">
													<tbody>
														<tr>
															<td colspan="7" align="left">
																<h:messages styleClass="messages" id="messages1" globalOnly="true"></h:messages>
															</td>
															<td align="right">
																
															</td>
														</tr>
														<tr>
															<td nowrap="nowrap" align="right"><h:outputText
																styleClass="outputText_Med" id="textMarketLabel"
																value="#{labels.market}:" style="font-weight: bold"></h:outputText></td>
															<td align="left" nowrap="nowrap"><h:outputText
																id="text4" value="#{route.route.marketID}"
																styleClass="outputText_Med" style="margin-right: 7px">
															</h:outputText></td>
															<td align="right" nowrap="nowrap"><h:outputText
																styleClass="outputText_Med" id="textDistrictLabel"
																value="#{labels.district}:" style="font-weight: bold"></h:outputText></td>
															<td align="left" nowrap="nowrap"><h:outputText
																id="text3" value="#{route.route.districtID}"
																styleClass="outputText_Med" style="margin-right: 7px">
															</h:outputText></td>
															<td nowrap="nowrap" align="right"><h:outputText
																styleClass="outputText_Med" id="textRouteIDLabel"
																value="#{labels.route}:" style="font-weight: bold"></h:outputText></td>
															<td nowrap="nowrap" align="left"><h:outputText
																id="textDetailRouteID" value="#{route.route.routeID}"
																styleClass="outputText_Med" style="margin-right: 7px">
															</h:outputText></td>
															<td align="right" nowrap="nowrap"><h:outputText
																styleClass="outputText_Med" id="textDescriptionLabel"
																value="#{labels.description}:" style="font-weight: bold"></h:outputText></td>
															<td align="left" nowrap="nowrap"><h:outputText
																id="text2" value="#{route.route.routeDescription}"
																styleClass="outputText_Med">
															</h:outputText></td>
															
														</tr>
													</tbody>
												</table>
												</td>
												<td align="right">
													<div class="mainContentHelpIcon"><hx:graphicImageEx
																	styleClass="graphicImageEx"
																	style="margin-left: 5px; margin-right: 5px; margin-bottom: 2px; margin-top: 2px; float: none; cursor: pointer"
																	id="imageExHelpIcon1" value="/scweb/images/question_mark.gif"
																	width="16" height="16" hspace="0" title="Draw Management Help"
																	onclick="return func_9(this, event);"
																	onmouseover="return func_10(this, event);"
																	onmouseout="return func_11(this, event);"></hx:graphicImageEx>
													</div>
												</td>
											</tr>
											<tr>
												<td nowrap="nowrap" align="left" colspan="7"></td>
												<td align="right" valign="top">  
												</td>
											</tr>
											<tr>
												<td nowrap="nowrap" align="left" colspan="8"></td>
											</tr>
											<tr>
												<td><h:inputHidden id="hiddenMktDstRteKey"
													value="#{route.marketDistRouteKey}"></h:inputHidden>
												</td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
										</tbody>
									</table>
									<odc:tabbedPanel slantActiveRight="4"
										styleClass="tabbedPanel_2" width="540" slantInactiveRight="4"
										variableTabLength="true"
										showBackNextButton="false" showTabs="true"
										id="tabbedPanelWeeklyReturnPanel" numOfTabs="4"
										oninitialpanelshow="return func_19(this, event);">
										<odc:bfPanel id="bfpanelRteMsgPanel" name="#{labels.messages}"
											showFinishCancelButton="false">
											<hx:jspPanel id="jspPanelTabbedPannelRteMsg">
												<hx:panelLayout styleClass="panelLayout" id="layout1"
													width="100%">
													<f:facet name="body">
														<h:dataTable border="1" cellpadding="1" cellspacing="0"
															columnClasses="columnClass1" headerClass="headerClass2"
															footerClass="footerClass" rowClasses="rowClass1"
															styleClass="dataTable" id="tableRouteBroadcasstMessages"
															width="100%" value="#{route.routeBroadcastMessages}"
															var="varrouteBroadcastMessages" rows="15">
															<h:column id="columnBMsgIdCol">
																<f:facet name="header">
																</f:facet>
																<h:outputText id="text6"
																	title="#{labels.routeBMsgIDHeader}"
																	value="#{varrouteBroadcastMessages.messageID}"
																	styleClass="outputText_Med">
																</h:outputText>
																<f:attribute value="50" name="width" />
																<f:attribute value="center" name="align" />
																<f:attribute value="top" name="valign" />
															</h:column>
															<h:column id="columnBMsgTxtCol">
																<f:facet name="header">
																</f:facet>
																<h:outputText id="text8"
																	value="#{varrouteBroadcastMessages.message}"
																	styleClass="outputText_Med">
																</h:outputText>
																<f:attribute value="top" name="valign" />
															</h:column>
														</h:dataTable>
													</f:facet>
													<f:facet name="left"></f:facet>
													<f:facet name="right"></f:facet>
													<f:facet name="bottom"></f:facet>
													<f:facet name="top">

														<hx:jspPanel id="jspPanel1111111">
															<table width="100%" cellspacing="0" border="0">
																<tr bgcolor="#95a5b9">
																	<td align="left"><h:outputFormat
																		styleClass="outputText_LG" id="formatMessageHeaderf1"
																		value="#{labels.routeBMsgTableHeader}"
																		style="color: white; font-weight: bold; margin-left: 5px;margin-top: 3px;">
																		<f:param name="NameNumMsgs"
																			value="#{route.numberBroadcastMessages}"></f:param>
																		<f:param name="DistributionDate"
																			value="#{drawManagmentUtil.distributionDateString}"></f:param>
																	</h:outputFormat></td>
																	<td align="right" width="30" nowrap="nowrap"></td>
																</tr>
															</table>
														</hx:jspPanel>
													</f:facet>
												</hx:panelLayout>
											</hx:jspPanel>
										</odc:bfPanel>
										<odc:bfPanel id="bfpanelReturnDrawForDay"
											name="#{labels.rdlForDayTabHeader}"
											showFinishCancelButton="false">


											<table border="0" cellpadding="0" cellspacing="0"
												width="100%">
												<tbody>
													<tr>
														<td align="center">
														<table border="0" cellpadding="2" cellspacing="1">
															<tbody>
																<tr>
																	<td align="center" colspan="3"><img border="0"
																		src="/scweb/theme/1x1.gif" width="1" height="1"
																		vspace="10"><h:outputFormat
																		styleClass="outputFormat"
																		id="formatManagePubByDayInstructions"
																		value="#{labels.rdlForDayInstructions}"
																		style="font-size: 10pt; font-weight: bold"
																		escape="false">
																		<f:param name="distributionDate"
																			value="#{drawManagmentUtil.distributionDateString}"></f:param>
																	</h:outputFormat></td>
																</tr>
																<tr>
																	<td colspan="3" align="center" nowrap="nowrap"
																		valign="top">
																	<table>
																		<tr>
																			<td nowrap="nowrap"><h:outputText
																				styleClass="outputText" id="text10"
																				value="#{labels.chooseDate}:"></h:outputText></td>
																			<td nowrap="nowrap" width="140"><h:inputText
																			styleClass="inputText" id="textReturnDrawChosenDate"
																			value="#{route.selectedRDLDate}" size="12">
																			<f:convertDateTime pattern="M/d/yyyy" />
																			<hx:inputHelperDatePicker firstDay="1" />
																			<hx:inputHelperAssist validation="true"
																				errorClass="inputText_Error"
																				onerror="return func_12(this, event);"
																				onsuccess="return func_13(this, event);" />
																			<hx:validateDateTimeRange
																				minimum="#{route.RDLBeginDateAsObject}"
																				maximum="#{route.RDLEndDateAsObject}" />
																		</h:inputText></td>
																			<td><hx:commandExButton type="submit"
																				value="#{labels.enterReturnsDrawByDate}"
																				styleClass="commandExButton"
																				id="buttonEnterReturnDrawForDate"
																				onclick="return func_1(this, event);"
																				action="#{pc_Run_list_detail.doButtonEnterReturnDrawForDateAction}"
																				style="width: 250px;"></hx:commandExButton></td>
																		</tr>
																		<tr>
																			<td colspan="2" align="right"><h:message
																				styleClass="message" id="messageBadReturnDrawDate"
																				for="textReturnDrawChosenDate"></h:message></td>
																			<td><hx:commandExButton type="submit"
																				value="#{labels.printFriendlyButton}"
																				styleClass="commandExButton"
																				id="buttonRDLPrintFriendly"
																				action="#{pc_Run_list_detail.doButtonRDLPrintFriendlyAction}"
																				onclick="return func_18(this, event);"
																				style="width: 250px;"
																				title="#{labels.RDLPrintHoverMsg}">
																			</hx:commandExButton></td>
																		</tr>
																	</table>
																	</td>
																</tr>
															</tbody>
														</table>
														</td>
													</tr>
													<tr>
														<td></td>
													</tr>
												</tbody>
											</table>
										</odc:bfPanel>
										<odc:bfPanel id="bfpanelWeeklyReturnsDraw"
											name="#{labels.managePubByWeek}"
											showFinishCancelButton="false">
											<hx:jspPanel id="jspPanelWeeklyReturnDrawOuterPanel">
												<center>
												<table border="0" cellpadding="0" cellspacing="0">
													<tbody>
														<tr>
															<td align="center"><img border="0"
																src="/scweb/theme/1x1.gif" width="1" height="1"
																vspace="10"><h:outputFormat
																styleClass="outputText"
																id="formatManagePubByWeekReturnsOnlyInstructions1"
																value="#{labels.managePubByWeekReturnsOnlyInstructions}"
																style="font-size: 10pt; font-weight: bold; padding-bottom: 20px"
																escape="false">
																<f:param name="distDate"
																	value="#{drawManagmentUtil.distributionDateString}"></f:param>
															</h:outputFormat></td>
														</tr>
														<tr>
															<td align="center"></td>
														</tr>
														<tr>
															<td align="center"><h:dataTable border="1"
																cellpadding="3" cellspacing="1"
																columnClasses="columnClass1" headerClass="headerClass"
																footerClass="footerClass" rowClasses="rowClass1"
																styleClass="dataTable"
																id="tablePublicationsForWeeklyReturnDraw"
																value="#{route.weeklyEndDatesHandler}"
																var="varweeklyEndDatesHandler">
																<h:column id="columnWeeklyReturnDrawPubsCol">
																	<hx:outputLinkEx styleClass="outputLinkExNoUnderline"
																		value="javascript:void(0);"
																		id="linkExWeeklyDRProdDescriptionLink"
																		onclick="return func_8(this, event);" tabindex="9999"
																		title="#{varweeklyEndDatesHandler.productDescription}">
																		<h:outputText styleClass="outputText"
																			id="textWeeklyDrawReturnProductID"
																			value="#{varweeklyEndDatesHandler.pub}"></h:outputText>
																	</hx:outputLinkEx>
																	<f:facet name="header">
																		<h:outputText id="textWeeklyDrawRetPubHeader"
																			styleClass="outputText" value="#{labels.publication}"></h:outputText>
																	</f:facet>
																	<f:attribute value="center" name="align" />
																	<f:attribute value="middle" name="valign" />
																</h:column>
																<h:column id="columnWeeklyReturnDrawWeeksHeader">
																	<h:message styleClass="message"
																		id="messageInvalidWeeklyReturnDrawMsg"
																		for="textWeekEndingDateValue"></h:message>
																	<hx:jspPanel id="jspPanelAvailableDatesPanel">
																	<h:inputText styleClass="inputText"
																		id="textWeekEndingDateValue"
																		value="#{varweeklyEndDatesHandler.selectedDate}"
																		size="12">
																		<hx:convertDateTime pattern="M/d/yyyy" />
																		<hx:inputHelperDatePicker firstDay="1" />
																		<hx:validateDateTimeRange
																			minimum="#{route.weeklyBeginDateAsObject}"
																			maximum="#{route.weeklyEndDateAsObject}" />
																		<hx:inputHelperAssist validation="true"
																			errorClass="inputText_Error"
																			onerror="return func_16(this, event);"
																			onsuccess="return func_17(this, event);" />
																		<hx:inputHelperDatePicker id="datePicker1" />
																	</h:inputText>

																</hx:jspPanel>
																	<f:facet name="header">
																		<h:outputText styleClass="outputText"
																			value="#{labels.availableWeeksHeader}"
																			id="textWeeklyDrawReturnAvailWeekHeader"></h:outputText>
																	</f:facet>
																	<f:attribute value="center" name="align" />
																	<f:attribute value="150" name="width" />
																	<f:attribute value="middle" name="valign" />
																</h:column>
																<h:column id="columnGoButtonsCol">
																	<hx:commandExButton type="submit"
																		value="#{labels.weeklydrawreturnActionButtonLabel}"
																		styleClass="commandExButton weeklyReturnDrawButton"
																		id="buttonWeeklyReturnDrawLaunch"
																		onclick="func_7(this, event, '#{varweeklyEndDatesHandler.pub}', 'N');return false;"></hx:commandExButton>
																	<f:facet name="header">
																	</f:facet>
																	<f:attribute value="center" name="align" />
																	<f:attribute value="middle" name="valign" />
																</h:column>
															</h:dataTable></td>
														</tr>
													</tbody>
												</table>
												</center>
											</hx:jspPanel>
										</odc:bfPanel>
										<f:facet name="back">
											<hx:commandExButton type="submit" value="&lt; Back"
												id="tabbedPanelWeeklyReturnPanel1_back" style="display:none"></hx:commandExButton>
										</f:facet>
										<f:facet name="next">
											<hx:commandExButton type="submit" value="Next &gt;"
												id="tabbedPanelWeeklyReturnPanel1_next" style="display:none"></hx:commandExButton>
										</f:facet>
										<f:facet name="finish">
											<hx:commandExButton type="submit" value="Finish"
												id="tabbedPanelWeeklyReturnPanel1_finish"
												style="display:none"></hx:commandExButton>
										</f:facet>
										<f:facet name="cancel">
											<hx:commandExButton type="submit" value="Cancel"
												id="tabbedPanelWeeklyReturnPanel1_cancel"
												style="display:none"></hx:commandExButton>
										</f:facet>
									</odc:tabbedPanel>
									<hx:inputHelperKeybind key="Enter" id="inputHelperKeybind1"
										targetAction="nothing" />
								</h:form>
							
						<%-- /tpl:put --%>
							</div> <br> <!-- end main content area -->
						</td>
					</tr>
					<tr>
						<td class="leftNavTD" align="left" valign="bottom">
							<div class="bottomNavAreaImage"></div>
						</td>
						<td></td>

					</tr>
					<tr>
						<td align="left" valign="top" colspan="3" height="20"><img
							class="footerImage" src="/scweb/theme/images/USATODAY_Footer_940x12.jpg">
							<!-- 							class="footerImage" src="/scweb/theme/images/800x20_art2.jpg"> -->

						</td>
					</tr>
					<tr>
						<td align="center" valign="top"><div class="footer"></div>
						</td>
						<td></td>
						<td class="mainContentWideTD" align="center" valign="top">
							<div class="footer">
								<table border="0" cellspacing="0" cellpadding="0" width="675">
									<tbody>
										<tr>
											<td class="mainContentWideTD" align="center">
												<!-- someting in this table drastically impacts how this template works so leave it in for now -->
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top" colspan="3">
							<div class="footer">
								<table border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td><img src="/scweb/theme/1x1.gif" hspace="15">
											</td>
											<!-- 											<td class="mainContentWideTD" align="center">&copy; Copyright -->
											<!-- 												2007 <a href="http://www.usatoday.com/marketing/credit.htm" -->
											<!-- 												target="_blank">USA TODAY</a>, a division of <a -->
											<!-- 												href="http://www.gannett.com" target="_blank">Gannett Co.</a> Inc. <a -->
											<!-- 												href="/scweb/legal/privacy.html" target="_blank">Privacy Policy</a>, -->
											<!-- 												By using this service, you accept our <a -->
											<!-- 												href="/scweb/legal/service.html" target="_blank">Terms of -->
											<!-- 													Service.</a></td> -->
											<td><br>&copy; <script>
												document.write(getCurrentYear());
											</script> USA TODAY, a <a href="http://www.gannett.com">Gannett Company</a> <a
												href= "/scweb/legal/privacy.html" target="_blank">| Privacy
													Policy</a>| By using this service, you accept
												our <a href="/scweb/legal/service.html" target="_blank">Terms of
													Service</a>.</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:scriptCollector>
	</body>
</f:view>
<!--  following link put here because of a bug in faces that seems to prevent styles from working properly -->
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
</html>
<%-- /tpl:insert --%>
<!DOCTYPE HTML>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/sc_secure/drawManagement/forweek/Return_and_draw_for_week.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true" buffer="none" autoFlush="true"
	isThreadSafe="true" isErrorPage="false"%>

<%@page import="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode"%><HTML>
<f:view locale="#{userLocaleHandler.locale}">
<f:loadBundle basename="resourcesFile" var="labels" />
<HEAD>
<%
	int locationSize = ((com.usatoday.champion.handlers.RDLForWeekHandler)request.getSession().getAttribute("RDLForWeek")).getLocations().size();

	OmnitureTrackingCode otc = (OmnitureTrackingCode)session.getAttribute("omnitureTracking");
	if (otc == null) {
		otc = new OmnitureTrackingCode();
	}
	String omnitureP1 = otc.getTrackingCodePart1();
 %>
 <%=omnitureP1 %>
	
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="/scweb/theme/Master.css" rel="stylesheet" type="text/css">
<LINK href="/scweb/theme/C_master_blue.css" rel="stylesheet"
	type="text/css">
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet"
	type="text/css">
<TITLE>USA TODAY - Weekly Draw Management</TITLE>
<LINK rel="stylesheet" type="text/css"
	href="/scweb/theme/stylesheet.css" title="Style">
<LINK rel="stylesheet" type="text/css"
	href="/scweb/theme/tabpanel.css" title="Style">
<LINK rel="stylesheet" type="text/css"
	href="../../../theme/stylesheet.css" title="Style">
<style type="text/css" media="screen">
    #thelist li { background: green; margin:5px; padding: 30px; }
    #thelist2 li { background: #ffb; margin:2px; padding: 2px; }
    #revertbox2 { position:absolute;top:40px;left:50px;z-index:1000;width:150px;height:150px;background:#bbf; }
</style>
<SCRIPT src="/scweb/scripts/browserVersion.js"  type="text/javascript"></SCRIPT>
<script src="/scweb/scripts/scriptaculous/prototype.js" type="text/javascript"></script>
<script src="/scweb/scripts/scriptaculous/scriptaculous.js?load=effects,dragdrop" type="text/javascript"></script>
<script src="/scweb/scripts/drawmngmt.js"></script>
<script src="/scweb/scripts/windowutility.js"></script>
<script src="/scweb/scripts/keepalive.js"></script>
<SCRIPT type="text/javascript">
// change Omniture Default Page name
s.pageName = "SC Weekly Draw Edit Entries";

Event.observe(window, 'load', function() {

	var inputArray = Form.getInputs('formManageDraw', 'text');
	var firstFound = false;
	var focusIndex = -1;
	for (i = 0; i < inputArray.length; i++) {
		if (inputArray[i].disabled == false && inputArray[i] != $('formManageDraw:textNumberLocsToDisplay')) {
			if (firstFound == false) {
				focusIndex = i;
				firstFound = true;
			}
		}
		else {
			if (inputArray[i] == $('formManageDraw:textNumberLocsToDisplay')) {
				;
			}
			else {
				// 
				$(inputArray[i]).addClassName('inputText_disabled');
			}
		}
	}
	if (focusIndex > -1) {
		inputArray[focusIndex].focus();
		inputArray[focusIndex].select();
	}
	
	// attach listeners to the pagination in the data table so no warning wil
	// be dipslayed
	inputArray =  $A($('formManageDraw').getElementsByTagName('a'));
	for (i = 0; i < inputArray.length; i++) {
		var cE = $(inputArray[i]);
		if (cE.id.indexOf('formManageDraw:tableWeeklyDrawDataTable:web1__pagerWeb') == 0) {
			$(inputArray[i]).observe('mouseover', function() {
				sClicked();
			});
			$(inputArray[i]).observe('mouseout', function() {
				sUnClicked();
			});
		}
	}	
	
	Event.observe('formManageDraw:buttonSubmitWeeklyDrawChanges', 'click', function() {
		sClicked();
	});
	

	Event.observe('formManageDraw:buttonUpdateNumLocsToDisplay', 'click', function() {
		sClicked();
	});
	
	startKeepAlive();
	
	$('StatusMessageField').innerHTML = '&nbsp;';	
});
//-->

</SCRIPT></HEAD>
	<BODY onbeforeunload="confirmUnload(this, event)"><hx:scriptCollector id="scriptCollector1">
	<CENTER><h:form styleClass="form" id="formManageDraw">
			<TABLE border="0" cellpadding="0" cellspacing="0" align="left" style="margin-left: 5px">
				<TBODY>
					<TR>
						<TD colspan="4"><hx:outputSeparator styleClass="outputSeparator"
							id="separator1" color="#90a0b8" align="center" height="3"></hx:outputSeparator>${omnitureTracking.trackingCodePart2}</TD>
					</TR>
					<TR>
						<TD nowrap="nowrap"><SPAN
							style="color: black; font-size: 12pt; font-weight: bold;text-align: center"><h:outputText styleClass="outputText"
							id="textDMTitleLabel"
							style="color: black; font-size: 12pt; font-weight: bold;text-align: center" value="#{labels.dmWeekTitle}"></h:outputText><SPAN
							style="color: black; font-size: 12pt; font-weight: bold;text-align: center">:
						</SPAN></SPAN><h:outputText
							id="textCurrentRDLDate" value="#{RDLForWeek.weekEndingDate}"
							styleClass="outputText" style="font-size: 16px">
							<f:convertDateTime />
						</h:outputText><a href="javascript:void(0);"  onclick="javascript: helpScreen('/scweb/sc_secure/drawManagement/forweek/weeklyHelpFile.usat'); return false;" title="Weekly Draw Entry Help" onmouseover="window.status='Click for Help On This Page'" onmouseout="window.status=' '">
							<IMG border="0"	src="/scweb/images/question_mark.gif" width="16" height="16"
							hspace="10"></a>
							</TD>
						<TD colspan="2" align="left">
							<DIV id="LateReturnsDiv"
										style="${RDLForWeek.lateReturnsString}"><h:outputText
										id="textLateReturnsMsg" value="#{labels.dmLateReturnsMsg}"
										styleClass="message"
										 escape="false">
									</h:outputText>
							</DIV>
							<DIV id="edittableFieldsMsgDiv" style="${RDLForWeek.edittableFieldsCSS}" >
								<h:outputText styleClass="message" id="textNotEdittable" value="#{labels.RDLNoEdittableFieldsWeekly}"></h:outputText>
							</DIV>
						</TD>
						<TD nowrap="nowrap" align="right"><h:outputText styleClass="outputText_Med"
							id="textNumLocationHeader" value="#{labels.dmNumLocations}: " style="color: black"></h:outputText><SPAN class="outputText_Med" style=""><%=locationSize %>&nbsp;&nbsp;</SPAN></TD>
					</TR><TR>
						<TD colspan="4">
						<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
							<TBODY>
								<TR>
									<TD align="left" nowrap="nowrap"><h:outputText styleClass="outputText"
										id="textRouteIDHeaderLabel" value="#{labels.route}: "
										style="color: black; font-size: 12px; font-weight: bold"></h:outputText><h:outputText
										styleClass="outputText_Med" id="textRouteIDHeaderValue"
										value="#{RDLForWeek.routeID}"></h:outputText>
									<h:outputText styleClass="outputText"
										id="textRouteDescHeaderLabel" value="#{labels.description}: "
										style="color: black; font-size: 12px; font-weight: bold"></h:outputText><h:outputText
										styleClass="outputText_Med"
										id="textRouteDescriptionHeaderValue"
										value="#{RDLForWeek.wdm.route.routeDescription}"></h:outputText></TD>
									<TD align="right">
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						</TD>
					</TR>
					<TR>
						<TD><IMG border="0" src="/scweb/theme/1x1.gif" width="1"
							height="1" vspace="5"> <h:messages styleClass="messages"
							id="messagesCatchAll" globalOnly="true" showDetail="false"
							style="font-size: 12px; font-weight: bold" layout="list"></h:messages></TD>
						<TD colspan="3" nowrap="nowrap" align="right" valign="bottom">
							<DIV id="StatusMessageField" class="outputText_LG">Please Wait...</DIV>
						</TD>
					</TR>
					<TR>
						<TD colspan="4" align="left" valign="bottom" nowrap="nowrap">
						<TABLE width="100%" border="0" cellpadding="0" cellspacing="1"
							bgcolor="#95a5b9">
							<TBODY>
								<TR>
									<TD nowrap="nowrap" width="165">&nbsp;&nbsp;<h:outputText
										styleClass="outputText" id="textLocationInfo"
										value="#{labels.dmLocationHeader}"
										style="color: white; font-size: 12px; font-weight: bold"></h:outputText></TD>
									<TD nowrap="nowrap"><FONT color="white"><SPAN
										style="font-weight: bold;font-size: 12px"></SPAN></FONT><h:outputText
										styleClass="outputText" id="textHeaderPubLabel"
										value="#{labels.dmProductOrdersHeader}:  "
										style="color: white; font-size: 12px; font-weight: 500"></h:outputText><h:outputText
										styleClass="outputText" id="textHeaderPublicationValue"
										value="#{RDLForWeek.publicationCode}"
										style="color: white; font-size: 12px; font-weight: 500"></h:outputText><h:outputText
										styleClass="outputText" id="textProductDescriptionHeader"
										value="- (#{RDLForWeek.productDescription})"
										style="color: white; font-size: 12px; font-weight: 500"></h:outputText></TD>
									<TD align="right" width="350"><h:outputText
										styleClass="outputText" id="textNumLocsPrompt"
										value="#{labels.maxLocationsPref}: "
										style="color: white; font-size: 11px; font-weight: bold"></h:outputText><h:inputText
										id="textNumberLocsToDisplay"
										value="#{userHandler.numberOfLocsPerPagePref}"
										styleClass="inputText" tabindex="0"
										title="Set the Maximum Locations to Display on a Page"
										size="8">
										<f:convertNumber type="number" integerOnly="true" />
										<f:validateLongRange minimum="5" maximum="1000"></f:validateLongRange>
										<hx:inputHelperAssist errorClass="inputText_Error" errorAction="selected"/>
									</h:inputText><hx:commandExButton type="submit"
										value="#{labels.redisplay}" styleClass="commandExButton"
										id="buttonUpdateNumLocsToDisplay"
										action="#{pc_Return_and_draw_for_week.doButtonUpdateNumLocsToDisplayAction}"
										style="font-size: 10px; margin-left: 3px; margin-right: 3px"></hx:commandExButton><BR>
									<h:message
										styleClass="message" id="message1"
										for="textNumberLocsToDisplay" style="color: white"></h:message></TD>
								</TR></TBODY>
						</TABLE>
						</TD>
					</TR>
					<TR>
						<TD align="left" valign="top" nowrap colspan="4"><odc:tabbedPanel
							slantActiveRight="4" styleClass="tabbedPanel" width="975"
							slantInactiveRight="4" height="530" variableTabLength="false"
							showBackNextButton="false" showTabs="false"
							id="tabbedPanelWeeklyDrawTabbedPanel">
							<odc:bfPanel id="bfpanelMainTabbedPanelWeeklyDraw"
								name="Weekly Draw" showFinishCancelButton="false">
								<hx:jspPanel id="jspPanelInsidePanelForDataGridWeeklyDraw">
									<h:dataTable border="0" cellpadding="2" cellspacing="0"
										columnClasses="columnClass1" headerClass="headerClass"
										footerClass="footerClass" rowClasses="rowClass1, rowClass3"
										styleClass="dataTable" id="tableWeeklyDrawDataTable"
										width="935" value="#{RDLForWeek.locations}" var="varlocations"
										rows="#{userHandler.numberOfLocsPerPagePref}"
										first="#{RDLForWeek.firstRowToDisplay}">
										<h:column id="columnLocDataTableLoc">
											<hx:jspPanel id="jspPanelLocationInfoColumnPanel">
												<TABLE border="0" cellpadding="0" cellspacing="1"
													width="153">
													<TBODY>
														<TR>
															<TD><h:outputText id="textLocationIDColumnValue"
																value="#{varlocations.locationID}"
																styleClass="outputText">
															</h:outputText></TD>
														</TR>
														<TR>
															<TD><h:outputText id="textLocationNameColumnValue"
																value="#{varlocations.locationName}"
																styleClass="outputText">
															</h:outputText></TD>
														</TR>
														<TR>
															<TD><h:outputText id="textLocationAddress1ColumnValue"
																value="#{varlocations.locationAddress1}"
																styleClass="outputText">
															</h:outputText></TD>
														</TR>
														<tr>
															<td><h:outputText styleClass="outputText"
																id="textLocationPhoneNumber"
																value="#{varlocations.locationPhoneNumber}">
																<hx:convertMask mask="(###) ###-####" />
															</h:outputText></td>
														</tr>
													</TBODY>
												</TABLE>
											</hx:jspPanel>
											<f:facet name="header">
											</f:facet>
											<f:attribute value="155" name="width" />
											<f:attribute value="top" name="valign" />
											<f:attribute value="left" name="align" />
										</h:column>
										<h:column id="columnLocDataTableProductOrders">
											<hx:jspPanel id="jspPanelProductOrderDataTablePanel">
												<h:dataTable border="1" cellpadding="2" cellspacing="0"
													columnClasses="columnClass1" headerClass="headerClass"
													footerClass="footerClass" rowClasses="rowClass1, rowClass2"
													styleClass="dataTable"
													id="tableInsideProductOrderDataTable"
													value="#{varlocations.productOrders}"
													var="varproductOrders">
													<h:column id="columnProductOrderIDValue">
														<h:outputText styleClass="outputText"
															id="textProductOrderIDValue"
															value="#{varproductOrders.productOrderID}"></h:outputText>
														<f:facet name="header">
															<h:outputText id="textProductOrderIDColHeader"
																styleClass="outputText" value="ID"></h:outputText>
														</f:facet>
														<f:attribute value="center" name="align" />
														<f:attribute value="middle" name="valign" />
													</h:column>
													<h:column id="column2">
														<hx:outputLinkEx styleClass="outputLinkEx"
															value="javascript:void(0);"
															id="linkExProductOrderTypeLink"
															title="#{varproductOrders.productOrderTypeDescription}"
															tabindex="9999" onclick="return false;">
															<h:outputText id="textProductOrderTypeValue"
																styleClass="outputText"
																value="#{varproductOrders.productOrderType}"></h:outputText>
														</hx:outputLinkEx>
														<f:facet name="header">
															<h:outputText styleClass="outputText" value="Type"
																id="textProductOrderTypeHeader"></h:outputText>
														</f:facet>
														<f:attribute value="center" name="align" />
														<f:attribute value="middle" name="valign" />
													</h:column>
													<h:column id="column1">
														<hx:jspPanel id="jspPanelMondayDrawMngmt">
															<TABLE border="0" cellpadding="1" cellspacing="1"
																class="weeklyReturnDraw">
																<TBODY>
																	<TR>
																		<TD colspan="2"><h:message styleClass="message"
																			id="messageMonDrawErr" for="textMondayDrawValue"></h:message><h:message
																			styleClass="message" id="messageMonReturnErr"
																			for="textMondayReturnValue"></h:message></TD>
																	</TR>
																	<TR>
																		<TD><h:inputText
																			styleClass="#{varproductOrders.mondayDrawCSS}"
																			id="textMondayDrawValue" size="3"
																			value="#{varproductOrders.mondayDrawValueObj}"
																			onchange="return drawValueChanged(this, event, #{varproductOrders.mondayMaxDrawValue}, #{varproductOrders.mondayDrawValue});"
																			title="Monday Draw: #{varproductOrders.mondayErrorMessage}"
																			readonly="#{varproductOrders.mondayDrawFieldOpen}"
																			disabled="#{varproductOrders.mondayDrawFieldOpen}">
																			<f:convertNumber integerOnly="true" />
																		</h:inputText> <h:inputHidden
																			id="hiddenMonZeroDrawReasonCode"
																			value="#{varproductOrders.mondayZeroDrawReason}"></h:inputHidden>
																		</TD>
																		<TD><h:inputText
																			styleClass="#{varproductOrders.mondayReturnCSS}"
																			id="textMondayReturnValue"
																			disabled="#{varproductOrders.mondayReturnFieldOpen}"
																			readonly="#{varproductOrders.mondayReturnFieldOpen}"
																			value="#{varproductOrders.mondayReturnValueObj}"
																			size="3"
																			title="Monday Returns: #{varproductOrders.mondayErrorMessage}"
																			onblur="return returnValueChanged(this, event, #{varproductOrders.mondayReturnValue});">
																			<f:convertNumber integerOnly="true" />
																		</h:inputText></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</hx:jspPanel>
														<f:facet name="header">
															<hx:jspPanel id="jspPanelMondayColHeaderPanel">
																<CENTER>
																<TABLE border="0" cellpadding="0" cellspacing="0">
																	<TBODY>
																		<TR>
																			<TD colspan="2" align="center" nowrap="nowrap"><h:outputText
																				styleClass="outputText"
																				value="#{labels.dmMonShort} (#{RDLForWeek.mondayDateString})"
																				id="textMondayDrawMngmtHeaderLabel"></h:outputText></TD>
																		</TR>
																		<TR>
																			<TD align="left" valign="bottom" nowrap="nowrap" width="35"><h:outputText
																				styleClass="outputText"
																				id="textMondayDrawHeaderLabel" value="Draw"
																				style="color: black; font-size: 10px; margin: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px"></h:outputText></TD>
																			<TD align="right" valign="bottom" nowrap="nowrap" width="35"><h:outputText
																				styleClass="outputText"
																				id="textMondayReturnsHeaderLabel" value="Returns"
																				style="color: black; font-size: 10px; margin: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px"></h:outputText></TD>
																		</TR>
																	</TBODY>
																</TABLE>
																</CENTER>
															</hx:jspPanel>
														</f:facet>
														<f:attribute value="center" name="align" />
													</h:column>
													<h:column id="column3">
														<hx:jspPanel id="jspPanelTuesdayDrawMngmtPanel">
															<TABLE border="0" cellpadding="1" cellspacing="1"
																class="weeklyReturnDraw">
																<TBODY>
																	<TR>
																		<TD colspan="2"><h:message styleClass="message"
																			id="messageTuesDrawErr" for="textTuesdayDrawValue"></h:message><h:message
																			styleClass="message" id="messageTuesReturnErr"
																			for="textTuesdayReturnValue"></h:message></TD>
																	</TR>
																	<TR>
																		<TD><h:inputText
																			styleClass="#{varproductOrders.tuesdayDrawCSS}"
																			id="textTuesdayDrawValue" size="3"
																			value="#{varproductOrders.tuesdayDrawValueObj}"
																			readonly="#{varproductOrders.tuesdayDrawFieldOpen}"
																			disabled="#{varproductOrders.tuesdayDrawFieldOpen}"
																			onchange="return drawValueChanged(this, event, #{varproductOrders.tuesdayMaxDrawValue}, #{varproductOrders.tuesdayDrawValue});"
																			title="Tuesday Draw: #{varproductOrders.tuesdayErrorMessage}">
																			<f:convertNumber integerOnly="true" />
																		</h:inputText> <h:inputHidden
																			id="hiddenTuesZeroDrawReasonCode"
																			value="#{varproductOrders.tuesdayZeroDrawReason}"></h:inputHidden>
																		</TD>
																		<TD><h:inputText
																			styleClass="#{varproductOrders.tuesdayReturnCSS}"
																			id="textTuesdayReturnValue"
																			value="#{varproductOrders.tuesdayReturnValueObj}"
																			size="3"
																			title="Tuesday Returns: #{varproductOrders.tuesdayErrorMessage}"
																			disabled="#{varproductOrders.tuesdayReturnFieldOpen}"
																			readonly="#{varproductOrders.tuesdayReturnFieldOpen}"
																			onblur="return returnValueChanged(this, event, #{varproductOrders.tuesdayReturnValue});">
																			<f:convertNumber integerOnly="true" />
																		</h:inputText></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</hx:jspPanel>
														<f:facet name="header">
															<hx:jspPanel id="jspPanelTuesdayColHeaderPanel">
																<CENTER>
																<TABLE border="0" cellpadding="0" cellspacing="0">
																	<TBODY>
																		<TR>
																			<TD colspan="2" align="center" nowrap="nowrap"><h:outputText
																				styleClass="outputText"
																				value="#{labels.dmTueShort} (#{RDLForWeek.tuesdayDateString})"
																				id="textTuesdayDrawMngmtHeaderLabel"></h:outputText></TD>
																		</TR>
																		<TR>
																			<TD align="center" valign="bottom" nowrap="nowrap" width="35"><h:outputText
																				styleClass="outputText"
																				id="textTuesdayDrawHeaderLabel" value="Draw"
																				style="color: black; font-size: 10px; margin: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px"></h:outputText></TD>
																			<TD align="center" valign="bottom" nowrap="nowrap" width="35"><h:outputText
																				styleClass="outputText"
																				id="textTuesdayReturnsHeaderLabel" value="Returns"
																				style="color: black; font-size: 10px; margin: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px"></h:outputText></TD>
																		</TR>
																	</TBODY>
																</TABLE>
																</CENTER>
															</hx:jspPanel>
														</f:facet>
														<f:attribute value="center" name="align" />
													</h:column>
													<h:column id="column4">
														<hx:jspPanel id="jspPanelWednesdayDrawMngmtPanel">
															<TABLE border="0" cellpadding="1" cellspacing="1"
																class="weeklyReturnDraw">
																<TBODY>
																	<TR>
																		<TD colspan="2"><h:message styleClass="message"
																			id="messageWedDrawErr" for="textWednesdayDrawValue"></h:message><h:message
																			styleClass="message" id="messageWedReturnErr"
																			for="textWednesdayReturnValue"></h:message></TD>
																	</TR>
																	<TR>
																		<TD><h:inputText
																			styleClass="#{varproductOrders.wednesdayDrawCSS}"
																			id="textWednesdayDrawValue" size="3"
																			value="#{varproductOrders.wednesdayDrawValueObj}"
																			title="Wednesday Draw: #{varproductOrders.wednesdayErrorMessage}"
																			readonly="#{varproductOrders.wednesdayDrawFieldOpen}"
																			disabled="#{varproductOrders.wednesdayDrawFieldOpen}"
																			onchange="return drawValueChanged(this, event, #{varproductOrders.wednesdayMaxDrawValue}, #{varproductOrders.wednesdayDrawValue});">
																			<f:convertNumber integerOnly="true" />
																		</h:inputText> <h:inputHidden
																			id="hiddenWedZeroDrawReasonCode"
																			value="#{varproductOrders.wednesdayZeroDrawReason}"></h:inputHidden>
																		</TD>
																		<TD><h:inputText
																			styleClass="#{varproductOrders.wednesdayReturnCSS}"
																			id="textWednesdayReturnValue"
																			disabled="#{varproductOrders.wednesdayReturnFieldOpen}"
																			readonly="#{varproductOrders.wednesdayReturnFieldOpen}"
																			value="#{varproductOrders.wednesdayReturnValueObj}"
																			size="3"
																			title="Wednesday Returns: #{varproductOrders.wednesdayErrorMessage}"
																			onblur="return returnValueChanged(this, event, #{varproductOrders.wednesdayReturnValue});">
																			<f:convertNumber integerOnly="true" />
																		</h:inputText></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</hx:jspPanel>
														<f:facet name="header">
															<hx:jspPanel id="jspPanelWednesdayColHeaderPanel">
																<CENTER>
																<TABLE border="0" cellpadding="0" cellspacing="0">
																	<TBODY>
																		<TR>
																			<TD colspan="2" align="center" nowrap="nowrap"><h:outputText
																				styleClass="outputText"
																				value="#{labels.dmWedShort} (#{RDLForWeek.wednesdayDateString})"
																				id="text3"></h:outputText></TD>
																		</TR>
																		<TR>
																			<TD align="center" valign="bottom" nowrap="nowrap" width="35"><h:outputText
																				styleClass="outputText"
																				id="textWednesdayDrawHeaderLabel" value="Draw"
																				style="color: black; font-size: 10px; margin: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px"></h:outputText></TD>
																			<TD align="center" valign="bottom" nowrap="nowrap" width="35"><h:outputText
																				styleClass="outputText"
																				id="textWednesdayReturnsHeaderLabel" value="Returns"
																				style="color: black; font-size: 10px; margin: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px"></h:outputText></TD>
																		</TR>
																	</TBODY>
																</TABLE>
																</CENTER>
															</hx:jspPanel>
														</f:facet>
														<f:attribute value="center" name="align" />
													</h:column>
													<h:column id="column5">
														<hx:jspPanel id="jspPanelThursdayDrawMngmtPanel">
															<TABLE border="0" cellpadding="1" cellspacing="1"
																class="weeklyReturnDraw">
																<TBODY>
																	<TR>
																		<TD colspan="2"><h:message styleClass="message"
																			id="messageThurDrawErr" for="textThursdayDrawValue"></h:message><h:message
																			styleClass="message" id="messageThurReturnErr"
																			for="textThursdayReturnValue"></h:message></TD>
																	</TR>
																	<TR>
																		<TD><h:inputText
																			styleClass="#{varproductOrders.thursdayDrawCSS}"
																			id="textThursdayDrawValue" size="3"
																			value="#{varproductOrders.thursdayDrawValueObj}"
																			title="Thursday Draw: #{varproductOrders.thursdayErrorMessage}"
																			readonly="#{varproductOrders.thursdayDrawFieldOpen}"
																			disabled="#{varproductOrders.thursdayDrawFieldOpen}"
																			onchange="return drawValueChanged(this, event, #{varproductOrders.thursdayMaxDrawValue}, #{varproductOrders.thursdayDrawValue});">
																			<f:convertNumber integerOnly="true" />
																		</h:inputText> <h:inputHidden
																			id="hiddenThurZeroDrawReasonCode"
																			value="#{varproductOrders.thursdayZeroDrawReason}"></h:inputHidden>
																		</TD>
																		<TD><h:inputText
																			styleClass="#{varproductOrders.thursdayReturnCSS}"
																			id="textThursdayReturnValue"
																			disabled="#{varproductOrders.thursdayReturnFieldOpen}"
																			readonly="#{varproductOrders.thursdayReturnFieldOpen}"
																			value="#{varproductOrders.thursdayReturnValueObj}"
																			size="3"
																			title="Thursday Returns: #{varproductOrders.thursdayErrorMessage}"
																			onblur="return returnValueChanged(this, event, #{varproductOrders.thursdayReturnValue});">
																			<f:convertNumber integerOnly="true" />
																		</h:inputText></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</hx:jspPanel>
														<f:facet name="header">
															<hx:jspPanel id="jspPanelThursdayColHeaderPanel">
																<CENTER>
																<TABLE border="0" cellpadding="0" cellspacing="0">
																	<TBODY>
																		<TR>
																			<TD colspan="2" align="center" nowrap="nowrap"><h:outputText
																				styleClass="outputText"
																				value="#{labels.dmThShort} (#{RDLForWeek.thursdayDateString})"
																				id="text4"></h:outputText></TD>
																		</TR>
																		<TR>
																			<TD align="center" valign="bottom" nowrap="nowrap" width="35"><h:outputText
																				styleClass="outputText"
																				id="textThursdayDrawHeaderLabel" value="Draw"
																				style="color: black; font-size: 10px; margin: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px"></h:outputText></TD>
																			<TD align="center" valign="bottom" nowrap="nowrap" width="35"><h:outputText
																				styleClass="outputText"
																				id="textThursdayReturnsHeaderLabel" value="Returns"
																				style="color: black; font-size: 10px; margin: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px"></h:outputText></TD>
																		</TR>
																	</TBODY>
																</TABLE>
																</CENTER>
															</hx:jspPanel>
														</f:facet>
														<f:attribute value="center" name="align" />
													</h:column>
													<h:column id="column6">
														<hx:jspPanel id="jspPanelFridayDrawMngmtPanel">
															<TABLE border="0" cellpadding="1" cellspacing="1"
																class="weeklyReturnDraw">
																<TBODY>
																	<TR>
																		<TD colspan="2"><h:message styleClass="message"
																			id="messageFriDrawErr" for="textFridayDrawValue"></h:message><h:message
																			styleClass="message" id="messageFriReturnErr"
																			for="textFridayReturnValue"></h:message></TD>
																	</TR>
																	<TR>
																		<TD><h:inputText
																			styleClass="#{varproductOrders.fridayDrawCSS}"
																			id="textFridayDrawValue" size="3"
																			value="#{varproductOrders.fridayDrawValueObj}"
																			title="Friday Draw: #{varproductOrders.fridayErrorMessage}"
																			readonly="#{varproductOrders.fridayDrawFieldOpen}"
																			disabled="#{varproductOrders.fridayDrawFieldOpen}"
																			onchange="return drawValueChanged(this, event, #{varproductOrders.fridayMaxDrawValue}, #{varproductOrders.fridayDrawValue});">
																			<f:convertNumber integerOnly="true" />
																		</h:inputText> <h:inputHidden
																			id="hiddenFriZeroDrawReasonCode"
																			value="#{varproductOrders.fridayZeroDrawReason}"></h:inputHidden>
																		</TD>
																		<TD><h:inputText
																			styleClass="#{varproductOrders.fridayReturnCSS}"
																			id="textFridayReturnValue"
																			disabled="#{varproductOrders.fridayReturnFieldOpen}"
																			readonly="#{varproductOrders.fridayReturnFieldOpen}"
																			value="#{varproductOrders.fridayReturnValueObj}"
																			size="3"
																			title="Friday Returns: #{varproductOrders.fridayErrorMessage}"
																			onblur="return returnValueChanged(this, event, #{varproductOrders.fridayReturnValue});">
																			<f:convertNumber integerOnly="true" />
																		</h:inputText></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</hx:jspPanel>
														<f:facet name="header">
															<hx:jspPanel id="jspPanelFridayColHeaderPanel">
																<CENTER>
																<TABLE border="0" cellpadding="0" cellspacing="0">
																	<TBODY>
																		<TR>
																			<TD colspan="2" align="center" nowrap="nowrap"><h:outputText
																				styleClass="outputText"
																				value="#{labels.dmFriShort} (#{RDLForWeek.fridayDateString})"
																				id="text5"></h:outputText></TD>
																		</TR>
																		<TR>
																			<TD align="center" valign="bottom" nowrap="nowrap" width="35"><h:outputText
																				styleClass="outputText"
																				id="textFridayDrawHeaderLabel" value="Draw"
																				style="color: black; font-size: 10px; margin: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px"></h:outputText></TD>
																			<TD align="center" valign="bottom" nowrap="nowrap" width="35"><h:outputText
																				styleClass="outputText"
																				id="textFridayReturnsHeaderLabel" value="Returns"
																				style="color: black; font-size: 10px; margin: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px"></h:outputText></TD>
																		</TR>
																	</TBODY>
																</TABLE>
																</CENTER>
															</hx:jspPanel>
														</f:facet>
														<f:attribute value="center" name="align" />
													</h:column>
													<h:column id="column7">
														<hx:jspPanel id="jspPanelSaturdayDrawMngmtPanel">
															<TABLE border="0" cellpadding="1" cellspacing="1"
																class="weeklyReturnDraw">
																<TBODY>
																	<TR>
																		<TD colspan="2"><h:message styleClass="message"
																			id="messageSatDrawErr" for="textSaturdayDrawValue"></h:message><h:message
																			styleClass="message" id="messageSatReturnErr"
																			for="textSaturdayReturnValue"></h:message></TD>
																	</TR>
																	<TR>
																		<TD><h:inputText
																			styleClass="#{varproductOrders.saturdayDrawCSS}"
																			id="textSaturdayDrawValue" size="3"
																			value="#{varproductOrders.saturdayDrawValueObj}"
																			title="Saturday Draw: #{varproductOrders.saturdayErrorMessage}"
																			readonly="#{varproductOrders.saturdayDrawFieldOpen}"
																			disabled="#{varproductOrders.saturdayDrawFieldOpen}"
																			onchange="return drawValueChanged(this, event, #{varproductOrders.saturdayMaxDrawValue}, #{varproductOrders.saturdayDrawValue});">
																			<f:convertNumber integerOnly="true" />
																		</h:inputText> <h:inputHidden
																			id="hiddenSatZeroDrawReasonCode"
																			value="#{varproductOrders.saturdayZeroDrawReason}"></h:inputHidden>
																		</TD>
																		<TD><h:inputText
																			styleClass="#{varproductOrders.saturdayReturnCSS}"
																			id="textSaturdayReturnValue"
																			disabled="#{varproductOrders.saturdayReturnFieldOpen}"
																			readonly="#{varproductOrders.saturdayReturnFieldOpen}"
																			value="#{varproductOrders.saturdayReturnValueObj}"
																			size="3"
																			title="Saturday Returns: #{varproductOrders.saturdayErrorMessage}"
																			onblur="return returnValueChanged(this, event, #{varproductOrders.saturdayReturnValue});">
																			<f:convertNumber integerOnly="true" />
																		</h:inputText></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</hx:jspPanel>
														<f:facet name="header">
															<hx:jspPanel id="jspPanelSaturdayColHeaderPanel">
																<CENTER>
																<TABLE border="0" cellpadding="0" cellspacing="0">
																	<TBODY>
																		<TR>
																			<TD colspan="2" align="center" nowrap="nowrap"><h:outputText
																				styleClass="outputText"
																				value="#{labels.dmSatShort} (#{RDLForWeek.saturdayDateString})"
																				id="text6"></h:outputText></TD>
																		</TR>
																		<TR>
																			<TD align="center" valign="bottom" nowrap="nowrap" width="35"><h:outputText
																				styleClass="outputText"
																				id="textSaturdayDrawHeaderLabel" value="Draw"
																				style="color: black; font-size: 10px; margin: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px"></h:outputText></TD>
																			<TD align="center" valign="bottom" nowrap="nowrap" width="35"><h:outputText
																				styleClass="outputText"
																				id="textSaturdayReturnsHeaderLabel" value="Returns"
																				style="color: black; font-size: 10px; margin: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px"></h:outputText></TD>
																		</TR>
																	</TBODY>
																</TABLE>
																</CENTER>
															</hx:jspPanel>
														</f:facet>
														<f:attribute value="center" name="align" />
													</h:column>
													<h:column id="column8">
														<hx:jspPanel id="jspPanelSundayDrawMngmtPanel">
															<TABLE border="0" cellpadding="1" cellspacing="1"
																class="weeklyReturnDraw">
																<TBODY>
																	<TR>
																		<TD colspan="2"><h:message styleClass="message"
																			id="messageSunDrawErr" for="textSundayDrawValue"></h:message><h:message
																			styleClass="message" id="messageSunReturnErr"
																			for="textSundayReturnValue"></h:message></TD>
																	</TR>
																	<TR>
																		<TD><h:inputText
																			styleClass="#{varproductOrders.sundayDrawCSS}"
																			id="textSundayDrawValue" size="3"
																			value="#{varproductOrders.sundayDrawValueObj}"
																			title="Sunday Draw: #{varproductOrders.sundayErrorMessage}"
																			readonly="#{varproductOrders.sundayDrawFieldOpen}"
																			disabled="#{varproductOrders.sundayDrawFieldOpen}"
																			onchange="return drawValueChanged(this, event, #{varproductOrders.sundayMaxDrawValue}, #{varproductOrders.sundayDrawValue});">
																			<f:convertNumber integerOnly="true" />
																		</h:inputText> <h:inputHidden
																			id="hiddenSunZeroDrawReasonCode"
																			value="#{varproductOrders.sundayZeroDrawReason}"></h:inputHidden>
																		</TD>
																		<TD><h:inputText
																			styleClass="#{varproductOrders.sundayReturnCSS}"
																			id="textSundayReturnValue"
																			disabled="#{varproductOrders.sundayReturnFieldOpen}"
																			readonly="#{varproductOrders.sundayReturnFieldOpen}"
																			value="#{varproductOrders.sundayReturnValueObj}"
																			size="3"
																			title="Sunday Returns: #{varproductOrders.sundayErrorMessage}"
																			onblur="return returnValueChanged(this, event, #{varproductOrders.sundayReturnValue});">
																			<f:convertNumber integerOnly="true" />
																		</h:inputText></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</hx:jspPanel>
														<f:facet name="header">
															<hx:jspPanel id="jspPanelSundayColHeaderPanel">
																<CENTER>
																<TABLE border="0" cellpadding="0" cellspacing="0">
																	<TBODY>
																		<TR>
																			<TD colspan="2" align="center" nowrap="nowrap"><h:outputText
																				styleClass="outputText"
																				value="#{labels.dmSunShort} (#{RDLForWeek.sundayDateString})"
																				id="text7"></h:outputText></TD>
																		</TR>
																		<TR>
																			<TD align="center" valign="bottom" nowrap="nowrap" width="35"><h:outputText
																				styleClass="outputText"
																				id="textSundayDrawHeaderLabel" value="Draw"
																				style="color: black; font-size: 10px; margin: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px"></h:outputText></TD>
																			<TD align="center" valign="bottom" nowrap="nowrap" width="35"><h:outputText
																				styleClass="outputText"
																				id="textSundayReturnsHeaderLabel" value="Returns"
																				style="color: black; font-size: 10px; margin: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px"></h:outputText></TD>
																		</TR>
																	</TBODY>
																</TABLE>
																</CENTER>
															</hx:jspPanel>
														</f:facet>
														<f:attribute value="center" name="align" />
													</h:column>
												</h:dataTable>
											</hx:jspPanel>
											<f:facet name="header">
											</f:facet>
											<f:attribute value="left" name="align" />
											<f:attribute value="top" name="valign" />

											<f:attribute value="776" name="width" />
										</h:column>
										<f:facet name="footer">
											<hx:panelBox styleClass="panelBox" id="box1FooterNav"
												align="center" cellpadding="5" layout="lineDirection"
												width="100%">
												<hx:panelLayout styleClass="panelLayout" id="layout1"
													width="95%" align="center">
													<f:facet name="body"></f:facet>
													<f:facet name="left">
														<hx:outputStatistics styleClass="outputStatistics"
															style="color: black; font-size: 12pt; font-weight: bold"
															id="statistics1"></hx:outputStatistics>
													</f:facet>
													<f:facet name="right">
														<hx:pagerWeb styleClass="pagerWeb" id="web1"
															style="color: black; font-size: 12pt; font-weight: bold"
															numberOfPages="20" />
													</f:facet>
													<f:facet name="bottom"></f:facet>
													<f:facet name="top"></f:facet>
												</hx:panelLayout>
											</hx:panelBox>
										</f:facet>
									</h:dataTable>
									<!-- End of Location Date Table -->
								</hx:jspPanel>
							</odc:bfPanel>
							<f:facet name="back">
								<hx:commandExButton type="submit" value="&lt; Back"
									id="tabbedPanel1_back" style="display:none"></hx:commandExButton>
							</f:facet>
							<f:facet name="next">
								<hx:commandExButton type="submit" value="Next &gt;"
									id="tabbedPanel1_next" style="display:none"></hx:commandExButton>
							</f:facet>
							<f:facet name="finish">
								<hx:commandExButton type="submit" value="Finish"
									id="tabbedPanel1_finish" style="display:none"></hx:commandExButton>
							</f:facet>
							<f:facet name="cancel">
								<hx:commandExButton type="submit" value="Cancel"
									id="tabbedPanel1_cancel" style="display:none"></hx:commandExButton>
							</f:facet>
						</odc:tabbedPanel></TD>
					</TR>
					<TR>
						<TD colspan="4" align="center" valign="middle" nowrap="nowrap">
						<TABLE border="0" cellpadding="2" cellspacing="2" width="95%">
							<TBODY>
								<TR>
									<TD align="center"><hx:commandExButton type="submit"
										value="#{labels.dmSubmitFormButtonLabel}"
										styleClass="commandExButton"
										id="buttonSubmitWeeklyDrawChanges"
										action="#{pc_Return_and_draw_for_week.doButtonSubmitWeeklyDrawChangesAction}" onclick="return validateWeeklyDrawMngmtForm(this, event);"></hx:commandExButton></TD>
									<TD align="center"><hx:commandExButton type="reset"
										value="#{labels.dmResetFormButtonLabel}"
										styleClass="commandExButton" id="buttonResetAllValues"
										confirm="#{labels.resetFormWarningPrompt}"></hx:commandExButton></TD>
									<TD align="center"><hx:commandExButton type="submit"
										value="#{labels.dmQuiteFormLabel}"
										styleClass="commandExButton"
										id="buttonQuitWithoutSavingWeeklyDraw"
										action="#{pc_Return_and_draw_for_week.doButtonQuitWithoutSavingWeeklyDrawAction}"></hx:commandExButton></TD>
								</TR>
							</TBODY>
						</TABLE>
						</TD>
					</TR>
					<TR>
						<TD></TD>
						<TD></TD>
						<TD></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD>
						<DIV id="ZeroDrawReasonChooser" class="box1" align="center"
							style="display: none;z-index:1000;border-bottom: 0px solid #90a0b8;border-top: 0px solid #90a0b8;border-left: 0px solid #90a0b8;border-right: 0px solid #90a0b8;width: 400px;background: white;position:absolute;; top: 250px; left: 200px; height: 200px">
						<TABLE class="ZeroDrawTable" width="350" border="0"
							cellpadding="2" cellspacing="1" bgcolor="white">
							<TBODY>
								<TR>
									<TD></TD>
									<TD><FONT color="black"><SPAN
										style="font-size: 14px;color: white"><SPAN
										style="font-size: 14px;color: white;font-weight: bold"><SPAN
										style="font-size: 14px;color: #90a0b8;font-weight: bold">Select
									a Zero Draw Reason:</SPAN></SPAN></SPAN></FONT></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD><SELECT size="1" name="ZeroDrawReasonSelector"
										id="ZeroDrawReasonSelector" class="selectOneListbox">
										<%
											com.usatoday.singlecopy.model.util.ZeroDrawReasonCodeCache rc = com.usatoday.singlecopy.model.util.ZeroDrawReasonCodeCache.getInstance();
											java.util.Iterator<com.usatoday.singlecopy.model.interfaces.drawmngmt.ZeroDrawReasonCodeIntf> itr =  rc.getZeroDrawReasonCodes().iterator();
											while (itr.hasNext()) {
												com.usatoday.singlecopy.model.interfaces.drawmngmt.ZeroDrawReasonCodeIntf zdr = itr.next();
												
										 %>
											<OPTION value="<%=zdr.getCode()%>"><%=zdr.getDescription() %></OPTION>
										<%
											}
										 %>
									</SELECT></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD><INPUT type="button" name="ZDRFinished" value="Done"
										onclick="return zeroDrawValueSelected(this, event);"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="center" valign="middle"
										style="font-size: 14px;color: columnFields;font-weight: bold"></TD>
								</TR>
							</TBODY>
						</TABLE>
						</DIV>
						</TD>
						<TD></TD>
						<TD></TD>
						<TD></TD>
					</TR>
				</TBODY>
			</TABLE>
			<script type="text/javascript" language="javascript" charset="utf-8">
						// <![CDATA[
						  new Draggable('ZeroDrawReasonChooser',{scroll:window,zindex:1000});
					  	// ]]>
						</script>
			<hx:inputHelperKeybind key="Enter" id="inputHelperKeybind1" targetAction="nexttab"/>
		</h:form></CENTER>
	</hx:scriptCollector>
	</BODY>
</f:view>
</HTML>

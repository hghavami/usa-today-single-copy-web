<!DOCTYPE HTML>
<jsp:useBean id="RDLForWeek"
	class="com.usatoday.champion.handlers.RDLForWeekHandler" scope="session"></jsp:useBean>
<jsp:useBean id="omnitureTracking" class="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode" scope="application"/>
<jsp:useBean id="globalMessage" class="com.usatoday.champion.handlers.MessageHandler" scope="application"></jsp:useBean>

<%@page import="com.usatoday.champion.handlers.WeeklyLocationHandler"%>
<%@page import="com.usatoday.champion.handlers.WeeklyProductOrderHandler"%>
<%@page import="com.usatoday.singlecopy.model.interfaces.drawmngmt.ZeroDrawReasonCodeIntf"%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="/scweb/theme/Master.css" rel="stylesheet" type="text/css">
<LINK href="/scweb/theme/C_master_blue.css" rel="stylesheet"
	type="text/css">
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet"
	type="text/css">
<TITLE>USA TODAY - Weekly Draw Management</TITLE>
<LINK rel="stylesheet" type="text/css" href="/scweb/theme/tabpanel.css" title="Style">
<%=omnitureTracking.getTrackingCodePart1() %>
<%
		int locationSize = ((com.usatoday.champion.handlers.RDLForWeekHandler)request.getSession().getAttribute("RDLForWeek")).getLocations().size();
		
		com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyDrawManangementIntf wdm = RDLForWeek.getWdm();

		org.joda.time.DateTime weekEndingDate = wdm.getWeekEndingDate();
		
		org.joda.time.format.DateTimeFormatter fmt = new org.joda.time.format.DateTimeFormatterBuilder()
            .appendMonthOfYearShortText()
            .appendLiteral(' ')
            .appendDayOfMonth(2)
            .appendLiteral(',')
            .appendLiteral(' ')
            .appendYear(4,4)
            .toFormatter();
            
        String weekEndingDateString = fmt.print(weekEndingDate);
        
        String globalMessageStyle = "display:none";
        String globalMessageText = "";
        if (globalMessage.getShowMessage().booleanValue() && globalMessage.showToContractors()) {
        	globalMessageStyle = "display: absolute";
        	globalMessageText = globalMessage.getMessage() == null ? "" : globalMessage.getMessage();
        }
%>

<SCRIPT src="/scweb/scripts/browserVersion.js"  type="text/javascript"></SCRIPT>
<script src="/scweb/scripts/scriptaculous/prototype.js" type="text/javascript"></script>
<script src="/scweb/scripts/scriptaculous/scriptaculous.js?load=effects,dragdrop" type="text/javascript"></script>
<script src="/scweb/scripts/commonValidations.js"></script>
<script src="/scweb/scripts/drawmngmt.js"></script>
<script src="/scweb/scripts/windowutility.js"></script>
<script src="/scweb/scripts/keepalive.js"></script>
<SCRIPT type="text/javascript">
// change Omniture Default Page name
s.pageName = "SC Weekly Draw Edit Entries";
s.channel = "Contractor Route Management";

var originalWindowHeight = 689;
var origingalDivHeight = 512;

Event.observe(window, 'load', function() {

	$('formManageDraw:buttonSubmitWeeklyDrawChanges').blur();

	var inputArray = $$('input.dmEditField');
	// this global holds the set of input fields to optimize the auto tab feature
	textInputFields = inputArray;
	var firstFound = false;
	var focusIndex = -1;
	for (i = 0; i < inputArray.length; i++) {
		if (firstFound == false && inputArray[i].hasClassName('returnInputText')) {
			focusIndex = i;
			firstFound = true;
		}
		
		Event.observe(inputArray[i], 'keydown', function(event) {
			if(aKeyPressed(event) == false) {
				if( typeof window.stop == "function" && ((BrowserDetect.browser == 'Explorer')==false)) {
					Event.stop(event);
				}
			}
		});
	}
	if (focusIndex > -1) {
		inputArray[focusIndex].focus();
		inputArray[focusIndex].select();
		lastSelectedElement = inputArray[focusIndex];
	}

	// this call turns off any open draw fields, it MUST be called after the above logic and not before.
	var drawCheckBox = $('disableEditableDrawFields');
	drawCheckBox.checked = false;
	var returnCheckBox = $('disableEditableReturnFields');
	returnCheckBox.checked = true;
	func_toggleDrawFields(drawCheckBox, null);	
	func_toggleReturnFields(returnCheckBox, null);
	
	confirmParentOpenWeekly();		

	Event.observe('formManageDraw:buttonSubmitWeeklyDrawChanges', 'keydown', function(event) {
		// never allow enter key to submit form
		if (event.keyCode == Event.KEY_RETURN) {
			return false;
		}
	}); 
	
	Event.observe(window, 'resize', function(event) {
		
		// only resize if greater than 745
		var poDiv = $('formManageDraw:tabbedPanelWeeklyDrawTabbedPanel:bfpanelMainTabbedPanelWeeklyDraw');
		var currentWindowHeight = originalWindowHeight;
		if (parseInt(navigator.appVersion)>3) {
		 if (navigator.appName=="Netscape") {
		  currentWindowHeight = window.innerHeight;
		 }
		 if (navigator.appName.indexOf("Microsoft")!=-1) {
		  currentWindowHeight = document.body.offsetHeight;
		 }
		}
		
		if (currentWindowHeight < originalWindowHeight) {
			poDiv.style.height = origingalDivHeight + 'px';
			return;
		}
		
		var height = poDiv.getHeight();
		
		var heightDif =  currentWindowHeight - originalWindowHeight;
		if (heightDif > 0) {
			var newDivHeight = origingalDivHeight + heightDif;
			poDiv.style.height = newDivHeight + 'px';
		}
		
	});
	
	startKeepAlive();

	// start countdown
	updateCountdown();
	
	$('StatusMessageField').innerHTML = '&nbsp;';	

	// set upinitial window size
	if (parseInt(navigator.appVersion)>3) {
	 if (navigator.appName=="Netscape") {
	  originalWindowHeight = window.innerHeight;
	 }
	 if (navigator.appName.indexOf("Microsoft")!=-1) {
	  originalWindowHeight = document.body.offsetHeight;
	 }
	}
	
});

</SCRIPT>
</HEAD>
<BODY onbeforeunload="confirmUnload(this, event)" onunload="enableCheckBoxes();">

<TABLE border="0" cellpadding="0" cellspacing="0" style="margin-left: 5px" width="979">
	<TBODY>
		<TR>
			<TD colspan="4">
				<div id="globalMessageDivArea" style="<%=globalMessageStyle %>" class="globalMessageDivStyle globalMessageDivStyleNoTemplate globalMessageDivStyleHiddenNoTemplate"><%=globalMessageText %></div>
			</TD>
		</TR>
		<TR>
			<TD nowrap="nowrap">
				<span class="outputText" style="color: black; font-size: 12pt; font-weight: bold;text-align: center">Pub Management For Week Ending: </span>
				<span class="outputText" style="font-size: 16px"><%=weekEndingDateString %></span>
			</TD>
			<TD colspan="2" align="left">
			</TD>
			<TD nowrap="nowrap" align="right">
				<span id="formManageDraw:textNumLocationHeader" class="outputText_Med" style="color: black">Number Of Locations On Route: </span><SPAN class="outputText_Med" style=""><%=locationSize %>&nbsp;&nbsp;</SPAN>
			</TD>
		</TR>
		<TR>
			<TD colspan="4">
				<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
				  <TBODY>
					<TR>
						<TD align="left" nowrap="nowrap">
							<span class="outputText" style="color: black; font-size: 12px; font-weight: bold">District: </span>
							<span class="outputText_Med"><%=RDLForWeek.getWdm().getRoute().getDistrictID() %></span>
							<span class="outputText" style="color: black; font-size: 12px; font-weight: bold">  Route: </span>
							<span class="outputText_Med"><%=RDLForWeek.getRouteID() %></span>
							<span class="outputText" style="color: black; font-size: 12px; font-weight: bold">Description: </span>
							<span class="outputText_Med"><%=wdm.getRoute().getRouteDescription() %></span>
						</TD>
						<TD align="right"><%=omnitureTracking.getTrackingCodePart2() %>
						</TD>
					</TR>
				</TBODY>
				</TABLE>
			</TD>
		</TR>
	  	<tr>
	  		<td colspan="4" align="center">
	  			<DIV id="countdownMSG" class="outputTextAlt" align="center">Note: You have about 80 minutes to submit your changes before your web session will expire due to inactivity.</DIV>
	  		</td>
	  	</tr>
		<TR>
			<TD align="left" colspan="4"><span class="message"><%=RDLForWeek.getSaveFailedMessage() %></span></TD>
		</TR>
		<TR>
			<TD><IMG border="0" src="/scweb/theme/1x1.gif" width="1"
				height="1" vspace="3"> 
				<DIV id="LateReturnsDiv" style="<%=RDLForWeek.getLateReturnsString() %>">
					<span class="message">NOTICE: Late returns week, you have already been invoiced for this week.</span>
				</DIV>
				<DIV id="noEditsDiv" style="<%=RDLForWeek.getEdittableFieldsCSS() %>">
					<p><span class="message">VIEW ONLY NOTICE: There are no draw or return fields open for edits this week.</span></p>
				</DIV>
			</TD>
			<TD colspan="3" nowrap="nowrap" align="right" valign="bottom">
				<DIV id="StatusMessageField" class="outputText_LG">Please Wait...<IMG
							border="0" src="/scweb/images/squaresAnimated.gif" width="80"
							height="10"></DIV>
			</TD>
		</TR>
		<TR>
			<TD colspan="4" align="left" valign="bottom" nowrap="nowrap">
			<TABLE width="100%" border="0" cellpadding="0" cellspacing="1"
				bgcolor="#d8d8c8">
				<TBODY>
					<TR bgcolor="#d8d8c8">
						<TD nowrap="nowrap" width="165">
							&nbsp;&nbsp;<span class="outputText" style="color: black; font-size: 12px; font-weight: bold">Location Information</span>
						</TD>
						<TD nowrap="nowrap">
							<span class="outputText" style="color: black; font-size: 12px; font-weight: 500">Product Orders Information for Pub: <%=RDLForWeek.getPublicationCode() %></span>
							<span id="formManageDraw:textProductDescriptionHeader" class="outputText" style="color: black; font-size: 12px; font-weight: 500">- (<%=RDLForWeek.getProductDescription() %>)</span>
						</TD>
						<TD align="right" width="350">
						<TABLE border="0" cellpadding="0" cellspacing="0">
							<TBODY>
								<TR>
									<TD align="left" nowrap="nowrap">
									   <INPUT type="checkbox"
													name="disableEditableDrawFields"
													id="disableEditableDrawFields" value="disableDrawFields"
													onclick="return func_toggleDrawFields(this, event);">
												<SPAN
													style="font-weight: bold;font-size: 11px;color: black;">Edit Draw</SPAN>
										<INPUT type="checkbox"
													name="disableEditableReturnFields"
													id="disableEditableReturnFields" value="disableReturnFields"
													onclick="return func_toggleReturnFields(this, event);" checked="checked">
												<SPAN
													style="font-weight: bold;font-size: 11px;color: black;">Edit Returns&nbsp;&nbsp;&nbsp;</SPAN>
									</TD>
									<TD align="right" valign="middle">
										<a href="javascript:void(0);"  onclick="javascript: helpScreen('/scweb/sc_secure/drawManagement/forweek/weeklyHelpFile.usat'); if (helpWindow) {helpWindow.focus();} return false;" title="Weekly Draw Entry Help" onmouseover="window.status='Click for Help On This Page'" onmouseout="window.status=' '">
											<IMG border="0"	src="/scweb/images/question_mark.gif" width="16" height="16" hspace="10">
										</a>									
									</TD>
								</TR>
							</TBODY>
						</TABLE>

						</TD>
					</TR>
				</TBODY>
			</TABLE>
			</TD>
		</TR>
	</tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" style="margin-left: 5px" width="977">
	<tbody>	
		<TR>
			<TD align="center" valign="top" nowrap="nowrap" colspan="4">
			
			<form id="formManageDraw" name="formManageDraw" action="/scweb/sc_secure/drawManagement/forWeek/submitWeeklyDrawEntries" method="post">
	
			<!-- Outer table -->
			<TABLE class="tabbedPanel" width="975" cellspacing="0" cellpadding="0" border="0">
				<TBODY>
					<TR>
						<TD class="tabbedPanel-Body" width="100%" valign="top" height="100%">
						
							<div id="formManageDraw:tabbedPanelWeeklyDrawTabbedPanel:bfpanelMainTabbedPanelWeeklyDraw" class="tabbedPanel_DIV" style="overflow: auto; visibility: visible; width: 967px; height: 512px;">
								<table id="formManageDraw:tableWeeklyDrawDataTable" class="dataTable" width="935" cellspacing="0" cellpadding="2" border="0">
									<tbody>
									<%
										int dtMon = 1;
										int rtMon = dtMon + 1;
										int dtTue = rtMon + 1;
										int rtTue = dtTue + 1;
										int dtWed = rtTue + 1;
										int rtWed = dtWed + 1;
										int dtThu = rtWed + 1;
										int rtThu = dtThu + 1;
										int dtFri = rtThu + 1;
										int rtFri = dtFri + 1;
										int dtSat = rtFri + 1;
										int rtSat = dtSat + 1;
										int dtSun = rtSat + 1;
										int rtSun = dtSun + 1;
										
										java.util.Collection<WeeklyLocationHandler> locations = RDLForWeek.getLocations();
										java.util.Iterator<WeeklyLocationHandler> lItr = locations.iterator();
										boolean isOddRow = true;
										int i = 0;
										while (lItr.hasNext()) {
											WeeklyLocationHandler wloc = lItr.next();
											String locId = wloc.getLocationID();
											String locName = wloc.getLocationName();
											String locNameNoApostrophes = locName.replaceAll("'","");
											locNameNoApostrophes = locNameNoApostrophes.replaceAll("\"","");
											String locAddr1 = wloc.getLocationAddress1();
											String locPhone = wloc.getLocationPhoneNumberFormatter();
											String locHashKey = wloc.getLocation().getLocationHashKey();
											String rowClassName = null;
											if (isOddRow) {
												rowClassName = "rowClass1";
												isOddRow = false;
											}
											else {
												rowClassName = "rowClass3";
												isOddRow = true;
											}
											
											java.util.Iterator<WeeklyProductOrderHandler> poItr = wloc.getProductOrders().iterator();
									 %>
										<tr class="<%=rowClassName %>">
											<td class="columnClass1" width="155" valign="top" align="left">
												<!-- location column -->
												<table width="153" cellspacing="1" cellpadding="0" border="0">
													<tbody>
														<tr>
															<td>
																<span class="outputText"><%=locId %></span>
															</td>
														</tr>
														<tr>
															<td>
																<span class="outputText"><%=locName %></span>
															</td>
														</tr>
														<tr>
															<td>
																<span class="outputText"><%=locAddr1 %></span>
															</td>
														</tr>
														<tr>
															<td>
																<span class="outputText"><%=locPhone %></span>
															</td>
														</tr>
													</tbody>
												</table>
												<!-- end location column -->																													
											</td>
											<td class="columnClass1" width="776" valign="top" align="left">
												<!-- inner data table -->
												<table id="formManageDraw:tableWeeklyDrawDataTable:<%=i %>:tableInsideProductOrderDataTable" class="dataTable" cellspacing="0" cellpadding="2" border="1">
													<thead>
														<tr>
															<th class="headerClass" scope="col">
																<span class="outputText">ID</span>
															</th>
															<th class="headerClass" scope="col">
																<span class="outputText">Type</span>
															</th>
															<th class="headerClass" scope="col" align="center">
																<table cellspacing="0" cellpadding="0" border="0" width="100%">
																	<tbody>
																		<tr>
																			<td nowrap="nowrap" align="center" colspan="2">
																				<span class="outputText">Mon (<%=RDLForWeek.getMondayDateString() %>)</span>
																			</td>
																		</tr>
																		<tr>
																			<td width="50%" valign="bottom" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Draw</span>
																			</td>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Returns</span>
																			</td>
																		</tr>
																	</tbody>
																</table>																			
															</th>
															<th class="headerClass" scope="col">
																<table cellspacing="0" cellpadding="0" border="0" width="100%">
																	<tbody>
																		<tr>
																			<td nowrap="nowrap" align="center" colspan="2">
																				<span class="outputText">Tue (<%=RDLForWeek.getTuesdayDateString() %>)</span>
																			</td>
																		</tr>
																		<tr>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Draw</span>
																			</td>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Returns</span>
																			</td>
																		</tr>
																	</tbody>
																</table>																			
															</th>
															<th class="headerClass" scope="col">
																<table cellspacing="0" cellpadding="0" border="0" width="100%">
																	<tbody>
																		<tr>
																			<td nowrap="nowrap" align="center" colspan="2">
																				<span class="outputText">Wed (<%=RDLForWeek.getWednesdayDateString() %>)</span>
																			</td>
																		</tr>
																		<tr>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Draw</span>
																			</td>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Returns</span>
																			</td>
																		</tr>
																	</tbody>
																</table>																			
															</th>
															<th class="headerClass" scope="col">
																<table cellspacing="0" cellpadding="0" border="0" width="100%">
																	<tbody>
																		<tr>
																			<td nowrap="nowrap" align="center" colspan="2">
																				<span class="outputText">Thur (<%=RDLForWeek.getThursdayDateString() %>)</span>
																			</td>
																		</tr>
																		<tr>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Draw</span>
																			</td>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Returns</span>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</th>
															<th class="headerClass" scope="col">
																<table cellspacing="0" cellpadding="0" border="0" width="100%">
																	<tbody>
																		<tr>
																			<td nowrap="nowrap" align="center" colspan="2">
																				<span  class="outputText">Fri (<%=RDLForWeek.getFridayDateString() %>)</span>
																			</td>
																		</tr>
																		<tr>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Draw</span>
																			</td>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Returns</span>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</th>
															<th class="headerClass" scope="col">
																<table cellspacing="0" cellpadding="0" border="0" width="100%">
																	<tbody>
																		<tr>
																			<td nowrap="nowrap" align="center" colspan="2">
																				<span class="outputText">Sat (<%=RDLForWeek.getSaturdayDateString() %>)</span>
																			</td>
																		</tr>
																		<tr>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Draw</span>
																			</td>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Returns</span>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</th>
															<th class="headerClass" scope="col">
																<table cellspacing="0" cellpadding="0" border="0" width="100%">
																	<tbody>
																		<tr>
																			<td nowrap="nowrap" align="center" colspan="2">
																				<span class="outputText">Sun (<%=RDLForWeek.getSundayDateString() %>)</span>
																			</td>
																		</tr>
																		<tr>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Draw</span>
																			</td>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Returns</span>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</th>
														</tr>
													</thead>
													<tbody>
													<%
														boolean innerRowIsOdd = true;
														while (poItr.hasNext()) {
															WeeklyProductOrderHandler po = poItr.next();
															String poID = po.getProductOrderID();
															String innerRowClass = null;
															if (innerRowIsOdd) {
																innerRowClass = "rowClass1";
																innerRowIsOdd = false;
															}
															else {
																innerRowClass = "rowClass2";
																innerRowIsOdd = true;
															}
															
													 %>
														<tr class="<%=innerRowClass %>">
															<td class="columnClass1" valign="middle" align="center">
																<span class="outputText"><%=po.getProductOrderID() %></span>
															</td>
															<td class="columnClass1" valign="middle" align="center">
																<a id="formManageDraw:tableWeeklyDrawDataTable:poDescLink<%=locHashKey %>:<%=po.getProductOrderID() %>" class="outputLinkExNoUnderline" onclick="return false;" title="<%=po.getProductOrderTypeDescription() %>" tabindex="0" href="javascript:void(0);" onfocus="autoTab(this, event)">
																	<span class="outputText"><%=po.getProductOrderType() %></span>
																</a>
															</td>
															<td class="columnClass1" align="center" valign="top">
																<table class="weeklyReturnDraw" cellspacing="1" cellpadding="1" border="0">
																	<tbody>
																		<tr>
																			<td colspan="2"/>
																		</tr>
																		<tr>
																			<td nowrap="nowrap">
																				<input id="<%=locHashKey %>:<%=poID %>:Monday:DrawValue" class="<%=po.getMondayDrawCSS() %>" type="text" title="Origingal Monday Draw Value: <%=po.getOriginalMondayDrawDisplayValue() %> <%=po.getMondayErrorMessage() %>" size="3" <%=po.getMondayDrawReadonly() %> value="<%=po.getMondayDrawDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Monday:DrawValue" onfocus="lastSelectedElement=this; focusGainedWeekly(this, event);"
																					onkeypress="$(this).removeClassName('inputText_Error');" onblur="return drawValueChangedWeekly(this, event, <%=po.getMondayMaxDrawValue() %>, <%=po.getMondayDrawValue() %>, '<%=locHashKey %>:<%=poID %>:Monday:ZeroDrawReasonCode', '<%=po.getMondayZeroDrawReason() %>', '<%=locHashKey %>:<%=poID %>:Monday:ZDRIndicator', '<%=locHashKey %>:<%=poID %>:Monday:zdrIndDiv', '<%=locId %>', '<%=locNameNoApostrophes  %>', '<%=poID %>', '<%=po.getProductID() %>', '<%=locHashKey %>:<%=poID %>:Monday:ReturnValue', 'Monday');" tabindex="<%=dtMon %>"/>
																				<input id="<%=locHashKey %>:<%=poID %>:Monday:ZeroDrawReasonCode" type="hidden" value="<%=po.getMondayZeroDrawReason() %>" name="<%=locHashKey %>:<%=poID %>:Monday:ZeroDrawReasonCode"/>
																			</td>
																			<td>
																				<input id="<%=locHashKey %>:<%=poID %>:Monday:ReturnValue" class="<%=po.getMondayReturnCSS() %>" type="text" title="Original Monday Returns Value: <%=po.getOriginalMondayReturnDisplayValue() %>  <%=po.getMondayErrorMessage() %>" size="3" <%=po.getMondayReturnReadonly() %> onfocus="lastSelectedElement=this; focusGainedWeekly(this, event);" value="<%=po.getMondayReturnDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Monday:ReturnValue" tabindex="<%=rtMon %>" 
																				onkeypress="$(this).removeClassName('inputText_Error');" onblur="return returnValueChangedWeekly(this, event, <%=po.getMondayReturnValue() %>, '<%=locHashKey %>:<%=poID %>:Monday:DrawValue');" />
																			</td>
																		</tr>
																	</tbody>
																</table>
																<div align="left" id="<%=locHashKey %>:<%=poID %>:Monday:zdrIndDiv" style="<%=po.getMondayZDRCSSString() %>" >
																	<img class="graphicImageEx" style="margin-left:5px"	
																	id="<%=locHashKey %>:<%=poID %>:Monday:ZDRIndicator"
																	title="<%=po.getMondayZDRImageTitle() %>"
																	onclick="showZRDPanel(this, '<%=locHashKey %>:<%=poID %>:Monday:ZeroDrawReasonCode', '<%=locId %>', '<%=locNameNoApostrophes %>', '<%=poID %>', '<%=po.getProductID() %>', 'Monday', '<%=po.getMondayDrawFieldOpen() %>', '<%=po.getOriginalMondayZeroDrawReason() %>', '<%=po.getOriginalMondayDrawDisplayValue() %>');"
																	src="<%=po.getMondayZDRImageIcon() %>" width="18" border="0"
																	hspace="0" vspace="0" height="14"/>
																</div>
															</td>
															<td class="columnClass1" align="center" valign="top">
																<table class="weeklyReturnDraw" cellspacing="1" cellpadding="1" border="0">
																	<tbody>
																		<tr>
																			<td colspan="2"/>
																		</tr>
																		<tr>
																			<td>
																				<input id="<%=locHashKey %>:<%=poID %>:Tuesday:DrawValue" class="<%=po.getTuesdayDrawCSS() %>" type="text" title="Original Tuesday Draw Value: <%=po.getOriginalTuesdayDrawDisplayValue() %> <%=po.getTuesdayErrorMessage() %>" size="3" <%=po.getTuesdayDrawReadonly() %> value="<%=po.getTuesdayDrawDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Tuesday:DrawValue" onfocus="lastSelectedElement=this; focusGainedWeekly(this, event);"
																					onkeypress="$(this).removeClassName('inputText_Error');"  tabindex="<%=dtTue %>" onblur="return drawValueChangedWeekly(this, event, <%=po.getTuesdayMaxDrawValue() %>, <%=po.getTuesdayDrawValue() %>, '<%=locHashKey %>:<%=poID %>:Tuesday:ZeroDrawReasonCode', '<%=po.getTuesdayZeroDrawReason() %>', '<%=locHashKey %>:<%=poID %>:Tuesday:ZDRIndicator', '<%=locHashKey %>:<%=poID %>:Tuesday:zdrIndDiv', '<%=locId %>', '<%=locNameNoApostrophes  %>', '<%=poID %>', '<%=po.getProductID() %>', '<%=locHashKey %>:<%=poID %>:Tuesday:ReturnValue', 'Tuesday');"/>
																				<input id="<%=locHashKey %>:<%=poID %>:Tuesday:ZeroDrawReasonCode" type="hidden" value="<%=po.getTuesdayZeroDrawReason() %>" name="<%=locHashKey %>:<%=poID %>:Tuesday:ZeroDrawReasonCode"/>
																			</td>
																			<td>
																				<input id="<%=locHashKey %>:<%=poID %>:Tuesday:ReturnValue" class="<%=po.getTuesdayReturnCSS() %>" type="text" title="Original Tuesday Returns Value: <%=po.getOriginalTuesdayReturnDisplayValue() %> <%=po.getTuesdayErrorMessage() %>" size="3" 
																				  <%=po.getTuesdayReturnReadonly() %> onblur="return returnValueChangedWeekly(this, event, <%=po.getTuesdayReturnValue() %>, '<%=locHashKey %>:<%=poID %>:Tuesday:DrawValue');" onfocus="lastSelectedElement=this; focusGainedWeekly(this, event);" value="<%=po.getTuesdayReturnDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Tuesday:ReturnValue" tabindex="<%=rtTue %>" onkeypress="$(this).removeClassName('inputText_Error');"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
																<div align="left" id="<%=locHashKey %>:<%=poID %>:Tuesday:zdrIndDiv" style="<%=po.getTuesdayZDRCSSString() %>" >
																	<img class="graphicImageEx" style="margin-left:5px"
																	id="<%=locHashKey %>:<%=poID %>:Tuesday:ZDRIndicator"
																	title="<%=po.getTuesdayZDRImageTitle() %>"
																	onclick="showZRDPanel(this, '<%=locHashKey %>:<%=poID %>:Tuesday:ZeroDrawReasonCode', '<%=locId %>', '<%=locNameNoApostrophes %>', '<%=poID %>', '<%=po.getProductID() %>', 'Tuesday', '<%=po.getTuesdayDrawFieldOpen() %>', '<%=po.getOriginalTuesdayZeroDrawReason() %>', '<%=po.getOriginalTuesdayDrawDisplayValue() %>');"
																	src="<%=po.getTuesdayZDRImageIcon() %>" width="18" border="0"
																	hspace="0" vspace="0" height="14"/>
																</div>
															</td>
															<td class="columnClass1" align="center" valign="top">
																<table class="weeklyReturnDraw" cellspacing="1" cellpadding="1" border="0">
																	<tbody>
																		<tr>
																			<td colspan="2"/>
																		</tr>
																		<tr>
																			<td>
																				<input id="<%=locHashKey %>:<%=poID %>:Wednesday:DrawValue" class="<%=po.getWednesdayDrawCSS() %>" type="text" title="Original Wednesday Draw Value: <%=po.getOriginalWednesdayDrawDisplayValue() %> <%=po.getWednesdayErrorMessage() %>" size="3" <%=po.getWednesdayDrawReadonly() %> value="<%=po.getWednesdayDrawDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Wednesday:DrawValue" onfocus="lastSelectedElement=this;focusGainedWeekly(this, event);" 
																					tabindex="<%=dtWed %>" onblur="return drawValueChangedWeekly(this, event, <%=po.getWednesdayMaxDrawValue() %>, <%=po.getWednesdayDrawValue() %>, '<%=locHashKey %>:<%=poID %>:Wednesday:ZeroDrawReasonCode', '<%=po.getWednesdayZeroDrawReason() %>', '<%=locHashKey %>:<%=poID %>:Wednesday:ZDRIndicator', '<%=locHashKey %>:<%=poID %>:Wednesday:zdrIndDiv', '<%=locId %>', '<%=locNameNoApostrophes  %>', '<%=poID %>', '<%=po.getProductID() %>', '<%=locHashKey %>:<%=poID %>:Wednesday:ReturnValue', 'Wednesday');" onkeypress="$(this).removeClassName('inputText_Error');" />
																				<input id="<%=locHashKey %>:<%=poID %>:Wednesday:ZeroDrawReasonCode" type="hidden" value="<%=po.getWednesdayZeroDrawReason() %>" name="<%=locHashKey %>:<%=poID %>:Wednesday:ZeroDrawReasonCode"/>
																			</td>
																			<td>
																				<input id="<%=locHashKey %>:<%=poID %>:Wednesday:ReturnValue" class="<%=po.getWednesdayReturnCSS() %>" type="text" title="Original Wednesday Returns Value: <%=po.getOriginalWednesdayReturnDisplayValue() %> <%=po.getWednesdayErrorMessage() %>" size="3" 
																				<%=po.getWednesdayReturnReadonly() %> onfocus="lastSelectedElement=this;focusGainedWeekly(this, event);" value="<%=po.getWednesdayReturnDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Wednesday:ReturnValue" tabindex="<%=rtWed %>" onblur="return returnValueChangedWeekly(this, event, <%=po.getWednesdayReturnValue() %>, '<%=locHashKey %>:<%=poID %>:Wednesday:DrawValue');" onkeypress="$(this).removeClassName('inputText_Error');" />
																			</td>
																		</tr>
																	</tbody>
																</table>
																<div align="left" id="<%=locHashKey %>:<%=poID %>:Wednesday:zdrIndDiv" style="<%=po.getWednesdayZDRCSSString() %>" >
																	<img class="graphicImageEx" style="margin-left:5px"
																	id="<%=locHashKey %>:<%=poID %>:Wednesday:ZDRIndicator"
																	title="<%=po.getWednesdayZDRImageTitle() %>"
																	onclick="showZRDPanel(this, '<%=locHashKey %>:<%=poID %>:Wednesday:ZeroDrawReasonCode', '<%=locId %>', '<%=locNameNoApostrophes %>', '<%=poID %>', '<%=po.getProductID() %>', 'Wednesday', '<%=po.getWednesdayDrawFieldOpen() %>', '<%=po.getOriginalWednesdayZeroDrawReason() %>', '<%=po.getOriginalWednesdayDrawDisplayValue() %>');"
																	src="<%=po.getWednesdayZDRImageIcon() %>" width="18" border="0"
																	hspace="0" vspace="0" height="14"/>
																</div>
															</td>
															<td class="columnClass1" align="center" valign="top">
																<table class="weeklyReturnDraw" cellspacing="1" cellpadding="1" border="0">
																	<tbody>
																		<tr>
																			<td colspan="2"/>
																		</tr>
																		<tr>
																			<td>
																				<input id="<%=locHashKey %>:<%=poID %>:Thursday:DrawValue" class="<%=po.getThursdayDrawCSS() %>" type="text" title="Original Thursday Draw Value: <%=po.getOriginalThursdayDrawDisplayValue() %> <%=po.getThursdayErrorMessage() %>" size="3" <%=po.getThursdayDrawReadonly() %> value="<%=po.getThursdayDrawDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Thursday:DrawValue" onfocus="lastSelectedElement=this;focusGainedWeekly(this, event);" 
																					onkeypress="$(this).removeClassName('inputText_Error');" tabindex="<%=dtThu %>" onblur="return drawValueChangedWeekly(this, event, <%=po.getThursdayMaxDrawValue() %>, <%=po.getThursdayDrawValue() %>, '<%=locHashKey %>:<%=poID %>:Thursday:ZeroDrawReasonCode', '<%=po.getThursdayZeroDrawReason() %>', '<%=locHashKey %>:<%=poID %>:Thursday:ZDRIndicator', '<%=locHashKey %>:<%=poID %>:Thursday:zdrIndDiv', '<%=locId %>', '<%=locNameNoApostrophes  %>', '<%=poID %>', '<%=po.getProductID() %>', '<%=locHashKey %>:<%=poID %>:Thursday:ReturnValue', 'Thursday');"/>
																				<input id="<%=locHashKey %>:<%=poID %>:Thursday:ZeroDrawReasonCode" type="hidden" value="<%=po.getThursdayZeroDrawReason() %>" name="<%=locHashKey %>:<%=poID %>:Thursday:ZeroDrawReasonCode"/>
																			</td>
																			<td>
																				<input id="<%=locHashKey %>:<%=poID %>:Thursday:ReturnValue" class="<%=po.getThursdayReturnCSS() %>" type="text" title="Original Thursday Returns Value: <%=po.getOriginalThursdayReturnDisplayValue() %> <%=po.getThursdayErrorMessage() %>" size="3" 
																				onkeypress="$(this).removeClassName('inputText_Error');" <%=po.getThursdayReturnReadonly() %> onfocus="lastSelectedElement=this;focusGainedWeekly(this, event);" value="<%=po.getThursdayReturnDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Thursday:ReturnValue" tabindex="<%=rtThu %>" onblur="return returnValueChangedWeekly(this, event, <%=po.getThursdayReturnValue() %>, '<%=locHashKey %>:<%=poID %>:Thursday:DrawValue');"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
																<div align="left" id="<%=locHashKey %>:<%=poID %>:Thursday:zdrIndDiv" style="<%=po.getThursdayZDRCSSString() %>" >
																	<img class="graphicImageEx" style="margin-left:5px"
																	id="<%=locHashKey %>:<%=poID %>:Thursday:ZDRIndicator"
																	title="<%=po.getThursdayZDRImageTitle() %>"
																	onclick="showZRDPanel(this, '<%=locHashKey %>:<%=poID %>:Thursday:ZeroDrawReasonCode', '<%=locId %>', '<%=locNameNoApostrophes %>', '<%=poID %>', '<%=po.getProductID() %>', 'Thursday', '<%=po.getThursdayDrawFieldOpen() %>', '<%=po.getOriginalThursdayZeroDrawReason() %>', '<%=po.getOriginalThursdayDrawDisplayValue() %>');"
																	src="<%=po.getThursdayZDRImageIcon() %>" width="18" border="0"
																	hspace="0" vspace="0" height="14"/>
																</div>
															</td>
															<td class="columnClass1" align="center" valign="top">
																<table class="weeklyReturnDraw" cellspacing="1" cellpadding="1" border="0">
																	<tbody>
																		<tr>
																			<td colspan="2"/>
																		</tr>
																		<tr>
																			<td>
																				<input id="<%=locHashKey %>:<%=poID %>:Friday:DrawValue" class="<%=po.getFridayDrawCSS() %>" type="text" title="Original Friday Draw Value: <%=po.getOriginalFridayDrawDisplayValue() %> <%=po.getFridayErrorMessage() %>" size="3" <%=po.getFridayDrawReadonly() %> value="<%=po.getFridayDrawDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Friday:DrawValue" onfocus="lastSelectedElement=this;focusGainedWeekly(this, event);" 
																					onkeypress="$(this).removeClassName('inputText_Error');" tabindex="<%=dtFri %>" onblur="return drawValueChangedWeekly(this, event, <%=po.getFridayMaxDrawValue() %>, <%=po.getFridayDrawValue() %>, '<%=locHashKey %>:<%=poID %>:Friday:ZeroDrawReasonCode', '<%=po.getFridayZeroDrawReason() %>', '<%=locHashKey %>:<%=poID %>:Friday:ZDRIndicator', '<%=locHashKey %>:<%=poID %>:Friday:zdrIndDiv', '<%=locId %>', '<%=locNameNoApostrophes  %>', '<%=poID %>', '<%=po.getProductID() %>', '<%=locHashKey %>:<%=poID %>:Friday:ReturnValue', 'Friday');"/>
																				<input id="<%=locHashKey %>:<%=poID %>:Friday:ZeroDrawReasonCode" type="hidden" value="<%=po.getFridayZeroDrawReason() %>" name="<%=locHashKey %>:<%=poID %>:Friday:ZeroDrawReasonCode"/>
																			</td>
																			<td>
																				<input id="<%=locHashKey %>:<%=poID %>:Friday:ReturnValue" class="<%=po.getFridayReturnCSS() %>" type="text" title="Original Friday Returns Value: <%=po.getOriginalFridayReturnDisplayValue() %> <%=po.getFridayErrorMessage() %>" size="3" 
																				onkeypress="$(this).removeClassName('inputText_Error');" <%=po.getFridayReturnReadonly() %> onfocus="lastSelectedElement=this; focusGainedWeekly(this, event);" value="<%=po.getFridayReturnDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Friday:ReturnValue" tabindex="<%=rtFri %>" onblur="return returnValueChangedWeekly(this, event, <%=po.getFridayReturnValue() %>, '<%=locHashKey %>:<%=poID %>:Friday:DrawValue');"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
																<div align="left" id="<%=locHashKey %>:<%=poID %>:Friday:zdrIndDiv" style="<%=po.getFridayZDRCSSString() %>" >
																	<img class="graphicImageEx" style="margin-left:5px"
																	id="<%=locHashKey %>:<%=poID %>:Friday:ZDRIndicator"
																	title="<%=po.getFridayZDRImageTitle() %>"
																	onclick="showZRDPanel(this, '<%=locHashKey %>:<%=poID %>:Friday:ZeroDrawReasonCode', '<%=locId %>', '<%=locNameNoApostrophes %>', '<%=poID %>', '<%=po.getProductID() %>', 'Friday', '<%=po.getFridayDrawFieldOpen() %>', '<%=po.getOriginalFridayZeroDrawReason() %>', '<%=po.getOriginalFridayDrawDisplayValue() %>');"
																	src="<%=po.getFridayZDRImageIcon() %>" width="18" border="0"
																	hspace="0" vspace="0" height="14"/>
																</div>															
															</td>
															<td class="columnClass1" align="center" valign="top">
																<table class="weeklyReturnDraw" cellspacing="1" cellpadding="1" border="0">
																	<tbody>
																		<tr>
																			<td colspan="2"/>
																		</tr>
																		<tr>
																			<td>
																				<input id="<%=locHashKey %>:<%=poID %>:Saturday:DrawValue" class="<%=po.getSaturdayDrawCSS() %>" type="text" title="Original Saturday Draw Value: <%=po.getOriginalSaturdayDrawDisplayValue() %> <%=po.getSaturdayErrorMessage() %>" size="3" <%=po.getSaturdayDrawReadonly() %> value="<%=po.getSaturdayDrawDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Saturday:DrawValue" onfocus="lastSelectedElement=this;focusGainedWeekly(this, event);" 
																					onkeypress="$(this).removeClassName('inputText_Error');" tabindex="<%=dtSat %>" onblur="return drawValueChangedWeekly(this, event, <%=po.getSaturdayMaxDrawValue() %>, <%=po.getSaturdayDrawValue() %>, '<%=locHashKey %>:<%=poID %>:Saturday:ZeroDrawReasonCode', '<%=po.getSaturdayZeroDrawReason() %>', '<%=locHashKey %>:<%=poID %>:Saturday:ZDRIndicator', '<%=locHashKey %>:<%=poID %>:Saturday:zdrIndDiv', '<%=locId %>', '<%=locNameNoApostrophes  %>', '<%=poID %>', '<%=po.getProductID() %>', '<%=locHashKey %>:<%=poID %>:Saturday:ReturnValue', 'Saturday');"/>
																				<input id="<%=locHashKey %>:<%=poID %>:Saturday:ZeroDrawReasonCode" type="hidden" value="<%=po.getSaturdayZeroDrawReason() %>" name="<%=locHashKey %>:<%=poID %>:Saturday:ZeroDrawReasonCode"/>
																			</td>
																			<td>
																				<input id="<%=locHashKey %>:<%=poID %>:Saturday:ReturnValue" class="<%=po.getSaturdayReturnCSS() %>" type="text" title="Original Saturday Returns Value: <%=po.getOriginalSaturdayReturnDisplayValue() %> <%=po.getSaturdayErrorMessage() %>" size="3" 
																				onkeypress="$(this).removeClassName('inputText_Error');" <%=po.getSaturdayReturnReadonly() %> onfocus="lastSelectedElement=this;focusGainedWeekly(this, event);" value="<%=po.getSaturdayReturnDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Saturday:ReturnValue" tabindex="<%=rtSat %>" onblur="return returnValueChangedWeekly(this, event, <%=po.getSaturdayReturnValue() %>, '<%=locHashKey %>:<%=poID %>:Saturday:DrawValue');"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
																<div align="left" id="<%=locHashKey %>:<%=poID %>:Saturday:zdrIndDiv" style="<%=po.getSaturdayZDRCSSString() %>" >
																	<img class="graphicImageEx" style="margin-left:5px"
																	id="<%=locHashKey %>:<%=poID %>:Saturday:ZDRIndicator"
																	title="<%=po.getSaturdayZDRImageTitle() %>"
																	onclick="showZRDPanel(this, '<%=locHashKey %>:<%=poID %>:Saturday:ZeroDrawReasonCode', '<%=locId %>', '<%=locNameNoApostrophes %>', '<%=poID %>', '<%=po.getProductID() %>', 'Saturday', '<%=po.getSaturdayDrawFieldOpen() %>', '<%=po.getOriginalSaturdayZeroDrawReason() %>', '<%=po.getOriginalSaturdayDrawDisplayValue() %>');"
																	src="<%=po.getSaturdayZDRImageIcon() %>" width="18" border="0"
																	hspace="0" vspace="0" height="14"/>
																</div>
															</td>
															<td class="columnClass1" align="center" valign="top">
																<table class="weeklyReturnDraw" cellspacing="1" cellpadding="1" border="0">
																	<tbody>
																		<tr>
																			<td colspan="2"/>
																		</tr>
																		<tr>
																			<td>
																				<input id="<%=locHashKey %>:<%=poID %>:Sunday:DrawValue" class="<%=po.getSundayDrawCSS() %>" type="text" title="Original Sunday Draw Value: <%=po.getOriginalSundayDrawDisplayValue() %> <%=po.getSundayErrorMessage() %>" size="3" <%=po.getSundayDrawReadonly() %> value="<%=po.getSundayDrawDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Sunday:DrawValue" onfocus="lastSelectedElement=this;focusGainedWeekly(this, event);" 
																					onkeypress="$(this).removeClassName('inputText_Error');" tabindex="<%=dtSun %>" onblur="return drawValueChangedWeekly(this, event, <%=po.getSundayMaxDrawValue() %>, <%=po.getSundayDrawValue() %>, '<%=locHashKey %>:<%=poID %>:Sunday:ZeroDrawReasonCode', '<%=po.getSundayZeroDrawReason() %>', '<%=locHashKey %>:<%=poID %>:Sunday:ZDRIndicator', '<%=locHashKey %>:<%=poID %>:Sunday:zdrIndDiv', '<%=locId %>', '<%=locNameNoApostrophes  %>', '<%=poID %>', '<%=po.getProductID() %>', '<%=locHashKey %>:<%=poID %>:Sunday:ReturnValue', 'Sunday');"/>
																				<input id="<%=locHashKey %>:<%=poID %>:Sunday:ZeroDrawReasonCode" type="hidden" value="<%=po.getSundayZeroDrawReason() %>" name="<%=locHashKey %>:<%=poID %>:Sunday:ZeroDrawReasonCode"/>
																			</td>
																			<td>
																				<input id="<%=locHashKey %>:<%=poID %>:Sunday:ReturnValue" class="<%=po.getSundayReturnCSS() %>" type="text" title="Original Sunday Returns Value: <%=po.getOriginalSundayReturnDisplayValue() %> <%=po.getSundayErrorMessage() %>" size="3" 
																				onkeypress="$(this).removeClassName('inputText_Error');" <%=po.getSundayReturnReadonly() %> onfocus="lastSelectedElement=this;focusGainedWeekly(this, event);" value="<%=po.getSundayReturnDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Sunday:ReturnValue" tabindex="<%=rtSun %>" onblur="return returnValueChangedWeekly(this, event, <%=po.getSundayReturnValue() %>, '<%=locHashKey %>:<%=poID %>:Sunday:DrawValue');"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
																<div align="left" id="<%=locHashKey %>:<%=poID %>:Sunday:zdrIndDiv" style="<%=po.getSundayZDRCSSString() %>" >
																	<img class="graphicImageEx" style="margin-left:5px"
																	id="<%=locHashKey %>:<%=poID %>:Sunday:ZDRIndicator"
																	title="<%=po.getSundayZDRImageTitle() %>"
																	onclick="showZRDPanel(this, '<%=locHashKey %>:<%=poID %>:Sunday:ZeroDrawReasonCode', '<%=locId %>', '<%=locNameNoApostrophes %>', '<%=poID %>', '<%=po.getProductID() %>', 'Sunday', '<%=po.getSundayDrawFieldOpen() %>', '<%=po.getOriginalSundayZeroDrawReason() %>', '<%=po.getOriginalSundayDrawDisplayValue() %>');"
																	src="<%=po.getSundayZDRImageIcon() %>" width="18" border="0"
																	hspace="0" vspace="0" height="14"/>
																</div>
															</td>
														</tr>
													<% 
																// bump up tab orders by 1
																dtMon = rtSun + 1;
																rtMon = dtMon + 1;
																dtTue = rtMon + 1;
																rtTue = dtTue + 1;
																dtWed = rtTue + 1;
																rtWed = dtWed + 1;
																dtThu = rtWed + 1;
																rtThu = dtThu + 1;
																dtFri = rtThu + 1;
																rtFri = dtFri + 1;
																dtSat = rtFri + 1;
																rtSat = dtSat + 1;
																dtSun = rtSat + 1;
																rtSun = dtSun + 1;
													
															} // end while more product orders
													%>
													</tbody>																															
												</table>
												<!-- end inner data table -->
											</td>
										</tr>
										<%		
												i++;
											} // end while more locations
										%>															
									</tbody>
								</table>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				<!-- End outer data table -->
					<table cellspacing="5" cellpadding="2" border="0">
						<tbody>
							<tr>
								<td align="right" nowrap="nowrap">
									<input id="formManageDraw:buttonSubmitWeeklyDrawChanges" class="commandExButton" type="submit" name="formManageDraw:buttonSubmit" value="Submit Draw And Returns Entries" tabindex="9991" onmouseover="this.focus();" onclick="return validateDrawMngmtForm(this, event);"/>
								</td>
								<td align="center" nowrap="nowrap">
									<input id="formManageDraw:buttonResetAllValues" class="commandExButton" type="reset" name="formManageDraw:buttonResetAllValues" value="Reset Form" " style="margin-left: 30;margin-right: 30" onclick="return confirmDrawReset(this, event);" tabindex="9992"/>
								</td>
								<td align="left" nowrap="nowrap">
									<input id="formManageDraw:buttonQuitWithoutSavingWeeklyDraw" class="commandExButton" type="submit" name="formManageDraw:buttonSubmit" value="Quit Without Saving" tabindex="9993"/>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</TD>
		</TR>
		<tr>
			<td valign="middle" nowrap="nowrap" align="center" colspan="4">
			</td>
		</tr>		
		
		<TR>
			<TD></TD>
			<TD></TD>
			<TD></TD>
			<TD></TD>
		</TR>
		<!-- Zero Draw Chooser -->
		<TR>
			<TD>
			<DIV id="ZeroDrawReasonChooser" class="box1" align="center"
				style="display: none;z-index:1000;border-bottom: 0px solid #90a0b8;border-top: 0px solid #90a0b8;border-left: 0px solid #90a0b8;border-right: 0px solid #90a0b8;width: 400px;background: white;position:absolute;; top: 250px; left: 200px; height: 200px; background-color: white;filter:alpha(opacity=50);opacity: 0.5;-moz-opacity:0.5;">
			<TABLE id="ZDRTableHolder" class="ZeroDrawTable" border="0" cellpadding="2" cellspacing="1" bgcolor="white"
				style="z-index:1001">
				<TBODY>
					<tr bgcolor="#90a0b8">
						<td colspan="2" align="left">
							<SPAN style="font-size: 14px;color: white;font-weight: bold">
								Select a Zero Draw Reason:</SPAN>
						</td>
					</tr>
					<TR>
						<td>
						</td>
						<td nowrap="nowrap">
							<table cellspacing="1" cellpadding="0" border="0" >
								<tbody>
								<tr>
									<td nowrap="nowrap" align="right">Location ID: </td>
									<TD nowrap="nowrap"><INPUT class="inputText_disabledV2" type="text"
										value="" id="tempLocID" name="tempLocID" readonly="readonly"></TD>
								</tr>
								<tr>
									<td nowrap="nowrap" align="right">Location Name: </td>
									<TD><div id="tempLocName"></div>
									</TD>
								</tr>
								<tr>
									<td nowrap="nowrap" align="right">POID: </td>
									<TD nowrap="nowrap"><INPUT class="inputText_disabledV2" type="text"
										value="" id="tempPOID" name="tempPOID" readonly="readonly"></TD>
								</tr>
								<tr>
									<td nowrap="nowrap" align="right">Pub: </td>
									<TD nowrap="nowrap"><INPUT class="inputText_disabledV2" type="text"
										value="" id="tempPubCode" name="tempPubCode" readonly="readonly"></TD>
								</tr>
								<TR>
									<TD nowrap="nowrap" align="right">Day of Week: </TD>
									<TD nowrap="nowrap"><div id="tempDayOfWeek"></div></TD>
								</TR>
							</tbody>
							</table>
						</td>
					</TR>
					<TR>
						<td></td>
						<td><hr/></td>
					</TR>
					<TR>
						<TD></TD>
						<TD><SPAN style="font-size: 11px;color: #90a0b8;font-weight: bold">
							Choose the best reason from the list below:</SPAN></TD>
					</TR>
					<TR>
						<TD></TD>
						<TD><SELECT size="1" name="ZeroDrawReasonSelector"
							id="ZeroDrawReasonSelector" class="selectOneListbox">
							<OPTION value=""> </OPTION>
							<%
								com.usatoday.singlecopy.model.util.ZeroDrawReasonCodeCache rc = com.usatoday.singlecopy.model.util.ZeroDrawReasonCodeCache.getInstance();
								java.util.Iterator<ZeroDrawReasonCodeIntf> itr =  rc.getZeroDrawReasonCodes().iterator();
								while (itr.hasNext()) {
									ZeroDrawReasonCodeIntf zdr = itr.next();
									
							 %>
							<OPTION value="<%=zdr.getCode()%>"><%=zdr.getDescription() %></OPTION>
							<%
								}
							 %>
						</SELECT></TD>
					</TR>
					<TR>
						<TD></TD>
						<TD><input type="hidden" id="zdrFieldID" value="" /><INPUT type="button" name="ZDRFinished" value="Set Zero Draw Reason"
							onclick="return zeroDrawValueSelected(this, event);"> &nbsp;<INPUT type="button" id="ZDRCodeCancel" name="ZDRCodeCancel" style="display: none" value="Cancel"
							onclick="return zeroDrawValueCanceled(this, event);"></TD>
					</TR>
					<TR>
						<TD></TD>
						<TD></TD>
					</TR>
				</TBODY>
			</TABLE>
			</DIV>
			</TD>
			<TD></TD>
			<TD></TD>
			<TD></TD>
		</TR>
		
		
	</TBODY>
</TABLE>




</BODY>
</HTML>

<!DOCTYPE HTML>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/sc_secure/drawManagement/forweek/LoadReturnAndDrawForWeek.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>

<%@page import="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode"%><HTML>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%
	String date = request.getParameter("d");
	String pub = request.getParameter("pc");
	String viewOnly = request.getParameter("v"); 
	String rteid = request.getParameter("rteid");	
 %>
<f:view locale="#{userLocaleHandler.locale}">
<f:loadBundle basename="resourcesFile" var="labels" />

	<HEAD>
	<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<LINK href="/scweb/theme/Master.css" rel="stylesheet" type="text/css">
	<LINK href="/scweb/theme/C_master_blue.css" rel="stylesheet"
		type="text/css">
	<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet"
		type="text/css">
	<LINK href="/scweb/theme/stylesheet.css" rel="stylesheet"
		type="text/css">
	<TITLE>Loading Data...</TITLE>
<%
	OmnitureTrackingCode otc = (OmnitureTrackingCode)session.getAttribute("omnitureTracking");
	if (otc == null) {
		otc = new OmnitureTrackingCode();
	}
	String omnitureP1 = otc.getTrackingCodePart1();
 %>
 
 <%=omnitureP1 %>

<SCRIPT type="text/javascript">
// change Omniture Default Page name
s.pageName = "SC Load Weekly Draw Data";
s.channel = "Contractor Route Management";

var doneLoadingData = false;
var dataExists = false;

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
window.moveTo(5, 20);
window.resizeTo(1018, 755);

loadWeeklyData();
setTimeout('doCheckforward()', 500);
}

function doCheckforward() {
	if (doneLoadingData == true) {
		document.RDLForWeekLoaderBean.submit();
	}
	else {
		setTimeout('doCheckforward()', 300);
	}
}
function initRequest(url) {
       if (window.XMLHttpRequest) {
           return new XMLHttpRequest();
       } else if (window.ActiveXObject) {
           isIE = true;
           return new ActiveXObject("Microsoft.XMLHTTP");
       }
}

function loadWeeklyData() {
	var url = "/scweb/sc_secure/loadWeeklyDrawReturnData.do?d=<%=date%>&pc=<%=pub%>&v=<%=viewOnly%>&rte=<%=rteid %>";
    var req = initRequest(url);
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
           if (req.status == 200) {
                parseMessages(req.responseXML);
           } 
           else if (req.status == 204){
               return;
           }
        }
        };
     req.open("GET", url, true);
     req.send(null);
}

function parseMessages(respMessage) {
	// assume any response is a good response and set complete flag
	
	
		var response = respMessage.getElementsByTagName(
                   "DataLoaded")[0];
       //alert('response: ' + response.childNodes[0].nodeValue);
       if (response && response.childNodes[0].nodeValue.length > 0 && response.childNodes[0].nodeValue == "Y") {
			dataExists = true;	
			var hiddenField = document.getElementsByName('dataExists')[0];
			hiddenField.value = "Y";
			doneLoadingData = true;
       }
       else {
       		if (response && response.childNodes[0].nodeValue.length > 0 && response.childNodes[0].nodeValue == "N") {
				dataExists = false;	
				var hiddenField = document.getElementsByName('dataExists')[0];
				hiddenField.value = "N";
				doneLoadingData = true;
			}
			else {
				dataExists = false;	
				var hiddenField = document.getElementsByName('dataExists')[0];
				hiddenField.value = "X";
				doneLoadingData = true;
			}
       }     	
}

</SCRIPT>
	
	</HEAD>
	<BODY onload="return func_1(this, event);">

	<hx:scriptCollector id="scriptCollector1">
		<P>${omnitureTracking.trackingCodePart2}<BR>
		</P>
		<CENTER>
		<br />
		<TABLE width="95%" border="1" cellpadding="15" cellspacing="1"
			bgcolor="white" height="95%">
			<TBODY>
				<TR>
					<TD align="center" valign="middle"><SPAN
						style="color: navy; font-family: Arial; vertical-align: middle; font-size: 24px; text-align: center"><h:outputText styleClass="outputText" id="textPleaseWait" value="#{labels.pleaseWait}"></h:outputText>
					<BR>
					<hx:graphicImageEx styleClass="graphicImageEx" id="imageExBusy"
						value="/scweb/images/squaresAnimated.gif"></hx:graphicImageEx> <BR>
					<BR><h:outputText styleClass="outputText" id="textLoading1" value="#{labels.loadingWeeklyDataP1}"></h:outputText>&nbsp;<%=pub %>   &nbsp;<h:outputText
						styleClass="outputText" id="textLoadingP2"
						value="#{labels.loadingWeeklyDataP2}"></h:outputText>&nbsp;<%=date %>...<BR><h:outputText styleClass="outputText" id="textLoadingDisclaimer" value="#{labels.loadDisclaimer}" style="font-size: 12px"></h:outputText>
					<BR></SPAN></TD>
				</TR>
			</TBODY>
		</TABLE>
		</CENTER>
		<P><SPAN
			style="color: navy; font-family: Arial; vertical-align: middle; font-size: 24px; text-align: center"></SPAN></P>
		<html:form action="/sc_secure/forwardRDLForWeek" method="get">
			<html:hidden property="d" value="<%=date %>" />
			<html:hidden property="pubCode" value="<%=pub %>" />
			<html:hidden property="hideViewOnly" value="<%=viewOnly %>" />
			<html:hidden property="rte" value="<%=rteid%>" />
			<html:hidden property="dataExists" value="Y" />


		</html:form>
	</hx:scriptCollector>
	</BODY>
</f:view>
</HTML>

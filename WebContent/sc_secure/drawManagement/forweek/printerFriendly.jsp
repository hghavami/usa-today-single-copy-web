<!DOCTYPE HTML>
<jsp:useBean id="RDLForWeek"
	class="com.usatoday.champion.handlers.RDLForWeekHandler" scope="session"></jsp:useBean>
<jsp:useBean id="omnitureTracking" class="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode" scope="application"/>

<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<%@page import="com.usatoday.champion.handlers.WeeklyLocationHandler"%>
<%@page import="com.usatoday.champion.handlers.ProductOrderHandler"%>
<%@page import="com.usatoday.champion.handlers.WeeklyProductOrderHandler"%><html:html>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="/scweb/theme/Master.css" rel="stylesheet" type="text/css">
<LINK href="/scweb/theme/C_master_blue.css" rel="stylesheet"
	type="text/css">
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet"
	type="text/css">
<TITLE>USA TODAY - Weekly Draw Management (Printer Friendly)</TITLE>
<LINK rel="stylesheet" type="text/css"
	href="/scweb/theme/stylesheet.css" title="Style">
<LINK rel="stylesheet" type="text/css"
	href="/scweb/theme/tabpanel.css" title="Style">
<%=omnitureTracking.getTrackingCodePart1() %>
<%
		int locationSize = ((com.usatoday.champion.handlers.RDLForWeekHandler)request.getSession().getAttribute("RDLForWeek")).getLocations().size();
		
		com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyDrawManangementIntf wdm = RDLForWeek.getWdm();

		org.joda.time.DateTime weekEndingDate = wdm.getWeekEndingDate();
		
		org.joda.time.format.DateTimeFormatter fmt = new org.joda.time.format.DateTimeFormatterBuilder()
            .appendMonthOfYearShortText()
            .appendLiteral(' ')
            .appendDayOfMonth(2)
            .appendLiteral(',')
            .appendLiteral(' ')
            .appendYear(4,4)
            .toFormatter();
            
        String weekEndingDateString = fmt.print(weekEndingDate);
        
        String printTime = new String("&nbsp;Date/Time Printed: " + (new org.joda.time.DateTime()).toString("yyyy-MM-dd HH:mm:ss.SSSZ"));
%>
<SCRIPT type="text/javascript">
// change Omniture Default Page name
s.pageName = "SC Weekly Draw Edit Entries - Printer Friendly";
s.channel = "Contractor Route Management";

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	window.print();
	return false;
}

function func_2(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	window.close();
	return false;
}
//-->

</SCRIPT>
<SCRIPT src="/scweb/scripts/windowutility.js"></SCRIPT>
</HEAD>
<BODY onload="confirmParentOpenPrintWin(); window.print();">
<TABLE border="0" cellpadding="0" cellspacing="0" style="margin-left: 2px" width="935">
	<TBODY>
		<TR>
			<TD colspan="4"><%=omnitureTracking.getTrackingCodePart2() %></TD>
		</TR>
		<TR>
			<TD nowrap="nowrap">
				<span class="outputText" style="color: black; font-size: 12pt; font-weight: bold;text-align: center">Pub Management For Week Ending: </span>
				<span class="outputText" style="font-size: 16px"><%=weekEndingDateString %></span>
			</TD>
			<TD colspan="2" align="left">
				<DIV id="LateReturnsDiv" style="<%=RDLForWeek.getLateReturnsString() %>">
					<span class="message">NOTICE: Late returns week, you have already been invoiced for this week.</span>
				</DIV>
				<DIV id="noEditsDiv" style="<%=RDLForWeek.getEdittableFieldsCSS() %>">
					<p><span class="message">VIEW ONLY NOTICE: There are no edittable draw or return fields for selected week.</span></p>
				</DIV>
			</TD>
			<TD nowrap="nowrap" align="right">
				<span id="formManageDraw:textNumLocationHeader" class="outputText_Med" style="color: black">Number Of Locations On Route: </span><SPAN class="outputText_Med" style=""><%=locationSize %>&nbsp;&nbsp;</SPAN>
			</TD>
		</TR>
		<TR>
			<TD colspan="4">
				<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
				  <TBODY>
					<TR>
						<TD align="left" nowrap="nowrap">
							<span class="outputText" style="color: black; font-size: 12px; font-weight: bold">District: </span>
							<span class="outputText_Med"><%=RDLForWeek.getWdm().getRoute().getDistrictID() %></span>
							<span class="outputText" style="color: black; font-size: 12px; font-weight: bold">  Route: </span>
							<span class="outputText_Med"><%=RDLForWeek.getRouteID() %></span>
							<span class="outputText" style="color: black; font-size: 12px; font-weight: bold">Description: </span>
							<span class="outputText_Med"><%=wdm.getRoute().getRouteDescription() %></span>
						</TD>
						<TD align="right">
						</TD>
					</TR>
				  </TBODY>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD colspan="4" align="left" valign="bottom" nowrap="nowrap">
			<TABLE width="100%" border="0" cellpadding="0" cellspacing="1">
				<TBODY>
					<TR bgcolor="#d8d8c8">
						<TD nowrap="nowrap" width="165">
							&nbsp;&nbsp;<span class="outputText"
							style="color: black; font-size: 12px; font-weight: bold">Location
						Information</span>
						</TD>
						<TD nowrap="nowrap">
							<span class="outputText"
							style="color: black; font-size: 12px; font-weight: 500">Product
						Orders Information for Pub: <%=RDLForWeek.getPublicationCode() %></span>
							<span id="formManageDraw:textProductDescriptionHeader" class="outputText" style="color: black; font-size: 12px; font-weight: 500">- (<%=RDLForWeek.getProductDescription() %>)</span>
						</TD>
						<TD align="right" width="350">
							<input value="Send To Printer"
							name="formManageDraw:buttonPrintPage"
							id="formManageDraw:buttonPrintPage"
							onclick="return func_1(this, event);" tabindex="1"
							class="commandExButton" type="reset"
							style="margin-top: 2px;margin-bottom: 2px; width: 120px">&nbsp;<input value="Close Window" name="formManageDraw:buttonCloseWindow" id="formManageDraw:buttonCloseWindow" onclick="return func_2(this, event);" class="commandExButton" type="reset" style="margin-top: 2px;margin-bottom: 2px; width: 115px">
						</TD>
					</TR>
				</TBODY>
			</TABLE>
			</TD>
		</TR>
	</tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" style="margin-left: 2px" width="977">
	<tbody>	
		<TR>
			<TD align="left" valign="top" nowrap="nowrap" colspan="4">
			
			<!-- Outer table -->
								<table id="formManageDraw:tableWeeklyDrawDataTable" class="dataTable" width="795" cellspacing="0" cellpadding="2" border="1">
									<tbody>
									<%
										
										java.util.Collection<WeeklyLocationHandler> locations = RDLForWeek.getLocations();
										java.util.Iterator<WeeklyLocationHandler> lItr = locations.iterator();
										boolean isOddRow = true;
										int i = 0;
										while (lItr.hasNext()) {
											WeeklyLocationHandler wloc = lItr.next();
											String locId = wloc.getLocationID();
											String locName = wloc.getLocationName();
											String locNameNoApostrophes = locName.replaceAll("'","");
											String locAddr1 = wloc.getLocationAddress1();
											String locPhone = wloc.getLocationPhoneNumberFormatter();
											String locHashKey = wloc.getLocation().getLocationHashKey();
											String rowClassName = null;
											if (isOddRow) {
												rowClassName = "rowClass1";
												isOddRow = false;
											}
											else {
												rowClassName = "rowClass3";
												isOddRow = true;
											}
											
											java.util.Iterator<WeeklyProductOrderHandler> poItr = wloc.getProductOrders().iterator();
									 %>
										<tr class="<%=rowClassName %>">
											<td class="columnClass1" width="155" valign="top" align="left">
												<!-- location column -->
												<table width="153" cellspacing="1" cellpadding="0" border="0">
													<tbody>
														<tr>
															<td>
																<span class="outputText"><%=locId %></span>
															</td>
														</tr>
														<tr>
															<td>
																<span class="outputText"><%=locName %></span>
															</td>
														</tr>
														<tr>
															<td>
																<span class="outputText"><%=locAddr1 %></span>
															</td>
														</tr>
														<tr>
															<td>
																<span class="outputText"><%=locPhone %></span>
															</td>
														</tr>
													</tbody>
												</table>
												<!-- end location column -->																													
											</td>
											<td class="columnClass1" width="776" valign="top" align="left">
												<!-- inner data table -->
												<table id="formManageDraw:tableWeeklyDrawDataTable:<%=i %>:tableInsideProductOrderDataTable" class="dataTable" cellspacing="0" cellpadding="2" border="1">
													<thead>
														<tr>
															<th class="headerClass" scope="col">
																<span class="outputText">ID</span>
															</th>
															<th class="headerClass" scope="col">
																<span class="outputText">Type</span>
															</th>
															<th class="headerClass" scope="col" align="center">
																<table cellspacing="0" cellpadding="0" border="0" width="100%">
																	<tbody>
																		<tr>
																			<td nowrap="nowrap" align="center" colspan="2">
																				<span class="outputText">Mon (<%=RDLForWeek.getMondayDateString() %>)</span>
																			</td>
																		</tr>
																		<tr>
																			<td width="50%" valign="bottom" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Draw</span>
																			</td>
																			<td width="50%" valign="bottom" nowrap align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Returns</span>
																			</td>
																		</tr>
																	</tbody>
																</table>																			
															</th>
															<th class="headerClass" scope="col">
																<table cellspacing="0" cellpadding="0" border="0" width="100%">
																	<tbody>
																		<tr>
																			<td nowrap="nowrap" align="center" colspan="2">
																				<span class="outputText">Tue (<%=RDLForWeek.getTuesdayDateString() %>)</span>
																			</td>
																		</tr>
																		<tr>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Draw</span>
																			</td>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Returns</span>
																			</td>
																		</tr>
																	</tbody>
																</table>																			
															</th>
															<th class="headerClass" scope="col">
																<table cellspacing="0" cellpadding="0" border="0" width="100%">
																	<tbody>
																		<tr>
																			<td nowrap="nowrap" align="center" colspan="2">
																				<span class="outputText">Wed (<%=RDLForWeek.getWednesdayDateString() %>)</span>
																			</td>
																		</tr>
																		<tr>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Draw</span>
																			</td>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Returns</span>
																			</td>
																		</tr>
																	</tbody>
																</table>																			
															</th>
															<th class="headerClass" scope="col">
																<table cellspacing="0" cellpadding="0" border="0" width="100%">
																	<tbody>
																		<tr>
																			<td nowrap="nowrap" align="center" colspan="2">
																				<span class="outputText">Thur (<%=RDLForWeek.getThursdayDateString() %>)</span>
																			</td>
																		</tr>
																		<tr>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Draw</span>
																			</td>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Returns</span>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</th>
															<th class="headerClass" scope="col">
																<table cellspacing="0" cellpadding="0" border="0" width="100%">
																	<tbody>
																		<tr>
																			<td nowrap="nowrap" align="center" colspan="2">
																				<span  class="outputText">Fri (<%=RDLForWeek.getFridayDateString() %>)</span>
																			</td>
																		</tr>
																		<tr>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Draw</span>
																			</td>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Returns</span>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</th>
															<th class="headerClass" scope="col">
																<table cellspacing="0" cellpadding="0" border="0" width="100%">
																	<tbody>
																		<tr>
																			<td nowrap="nowrap" align="center" colspan="2">
																				<span class="outputText">Sat (<%=RDLForWeek.getSaturdayDateString() %>)</span>
																			</td>
																		</tr>
																		<tr>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Draw</span>
																			</td>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Returns</span>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</th>
															<th class="headerClass" scope="col">
																<table cellspacing="0" cellpadding="0" border="0" width="100%">
																	<tbody>
																		<tr>
																			<td nowrap="nowrap" align="center" colspan="2">
																				<span class="outputText">Sun (<%=RDLForWeek.getSundayDateString() %>)</span>
																			</td>
																		</tr>
																		<tr>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Draw</span>
																			</td>
																			<td width="50%" valign="bottom" nowrap="nowrap" align="center">
																				<span class="outputText" style="margin: 0px; color: black; font-size: 10px;">Returns</span>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</th>
														</tr>
													</thead>
													<tbody>
													<%
														boolean innerRowIsOdd = true;
														while (poItr.hasNext()) {
															WeeklyProductOrderHandler po = poItr.next();
															String poID = po.getProductOrderID();
															String innerRowClass = null;
															if (innerRowIsOdd) {
																innerRowClass = "rowClass1";
																innerRowIsOdd = false;
															}
															else {
																innerRowClass = "rowClass2";
																innerRowIsOdd = true;
															}
															
													 %>
														<tr class="<%=innerRowClass %>">
															<td class="columnClass1" valign="middle" align="center">
																<span class="outputText"><%=po.getProductOrderID() %></span>
															</td>
															<td class="columnClass1" valign="middle" align="center">
																	<span class="outputText"><%=po.getProductOrderType() %></span>
															</td>
															<td class="columnClass1" align="center" valign="top">
																<table class="weeklyReturnDraw" cellspacing="1" cellpadding="1" border="0">
																	<tbody>
																		<tr>
																			<td colspan="2"/>
																		</tr>
																		<tr>
																			<td nowrap="nowrap">
																				<input class="outputText" type="text"  size="3" readonly="readonly" value="<%=po.getMondayDrawDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Monday:DrawValue"/>
																			</td>
																			<td>
																				<input class="outputText" type="text" size="3" readonly="readonly" value="<%=po.getMondayReturnDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Monday:ReturnValue"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
															<td class="columnClass1" align="center" valign="top">
																<table class="weeklyReturnDraw" cellspacing="1" cellpadding="1" border="0">
																	<tbody>
																		<tr>
																			<td colspan="2"/>
																		</tr>
																		<tr>
																			<td>
																				<input class="outputText" type="text" size="3" readonly="readonly" value="<%=po.getTuesdayDrawDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Tuesday:DrawValue"/>
																			</td>
																			<td>
																				<input class="outputText" type="text" size="3" readonly="readonly" value="<%=po.getTuesdayReturnDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Tuesday:ReturnValue"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
															<td class="columnClass1" align="center" valign="top">
																<table class="weeklyReturnDraw" cellspacing="1" cellpadding="1" border="0">
																	<tbody>
																		<tr>
																			<td colspan="2"/>
																		</tr>
																		<tr>
																			<td>
																				<input  class="outputText" type="text"  size="3" readonly="readonly" value="<%=po.getWednesdayDrawDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Wednesday:DrawValue"/>
																			</td>
																			<td>
																				<input class="outputText" type="text" size="3" readonly="readonly" value="<%=po.getWednesdayReturnDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Wednesday:ReturnValue"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
															<td class="columnClass1" align="center" valign="top">
																<table class="weeklyReturnDraw" cellspacing="1" cellpadding="1" border="0">
																	<tbody>
																		<tr>
																			<td colspan="2"/>
																		</tr>
																		<tr>
																			<td>
																				<input class="outputText" type="text" size="3" readonly="readonly" value="<%=po.getThursdayDrawDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Thursday:DrawValue"/>
																			</td>
																			<td>
																				<input class="outputText" type="text" size="3" readonly="readonly" value="<%=po.getThursdayReturnDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Thursday:ReturnValue"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
															<td class="columnClass1" align="center" valign="top">
																<table class="weeklyReturnDraw" cellspacing="1" cellpadding="1" border="0">
																	<tbody>
																		<tr>
																			<td colspan="2"/>
																		</tr>
																		<tr>
																			<td>
																				<input class="outputText" type="text" size="3" readonly="readonly" value="<%=po.getFridayDrawDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Friday:DrawValue"/>
																			</td>
																			<td>
																				<input class="outputText" type="text"  size="3" readonly="readonly" value="<%=po.getFridayReturnDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Friday:ReturnValue"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
															<td class="columnClass1" align="center" valign="top">
																<table class="weeklyReturnDraw" cellspacing="1" cellpadding="1" border="0">
																	<tbody>
																		<tr>
																			<td colspan="2"/>
																		</tr>
																		<tr>
																			<td>
																				<input class="outputText" type="text" size="3"  readonly="readonly" value="<%=po.getSaturdayDrawDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Saturday:DrawValue"/>
																			</td>
																			<td>
																				<input class="outputText" type="text" size="3" readonly="readonly" value="<%=po.getSaturdayReturnDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Saturday:ReturnValue"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
															<td class="columnClass1" align="center" valign="top">
																<table class="weeklyReturnDraw" cellspacing="1" cellpadding="1" border="0">
																	<tbody>
																		<tr>
																			<td colspan="2"/>
																		</tr>
																		<tr>
																			<td>
																				<input class="outputText" type="text" size="3" readonly="readonly" value="<%=po.getSundayDrawDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Sunday:DrawValue"/>
																			</td>
																			<td>
																				<input class="outputText" type="text" size="3" readonly="readonly" value="<%=po.getSundayReturnDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:Sunday:ReturnValue"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													<% 
															} // end while more product orders
													%>
													</tbody>																															
												</table>
												<!-- end inner data table -->
											</td>
										</tr>
										<%		
												i++;
											} // end while more locations
										%>															
									</tbody>
								</table>
			</TD>
		</TR>
	</tbody>
</table>


<%=printTime %>
</BODY>
</html:html>

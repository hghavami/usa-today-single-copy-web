<!DOCTYPE HTML>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/sc_secure/drawManagement/forweek/Nodata.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>

<%@page import="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode"%><HTML>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<f:view locale="#{userLocaleHandler.locale}">
	<f:loadBundle basename="resourcesFile" var="labels" />
	<HEAD>
<jsp:useBean id="omnitureTracking" class="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode" scope="application"/>
<%
	String date = request.getParameter("d");
	String pub = request.getParameter("pubCode");

	OmnitureTrackingCode otc = (OmnitureTrackingCode)session.getAttribute("omnitureTracking");
	if (otc == null) {
		otc = new OmnitureTrackingCode();
	}
	String omnitureP1 = otc.getTrackingCodePart1();
 %>
<%=omnitureP1 %>

	<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<META http-equiv="Content-Style-Type" content="text/css">
	<LINK href="/scweb/theme/Master.css" rel="stylesheet" type="text/css">
	<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet"
		type="text/css">
	<LINK href="/scweb/theme/C_master_blue.css" rel="stylesheet"
		type="text/css">
	<TITLE>No Data Found</TITLE>
	<LINK rel="stylesheet" type="text/css"
		href="/scweb/theme/stylesheet.css" title="Style">
	<SCRIPT type="text/javascript">
// change Omniture Default Page name
s.pageName = "SC Weekly Draw No Data";
s.channel = "Contractor Route Management";

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
window.close();
window.opener.focus();
}
function func_2(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
window.close();
window.opener.focus();
}</SCRIPT>
	</HEAD>
	<BODY>
	<hx:scriptCollector id="scriptCollector1">
		<h:form styleClass="form" id="form1">
			<CENTER><br><br>
			<TABLE width="95%" border="1" cellpadding="15" cellspacing="1"
				bgcolor="white" height="95%">
				<TBODY>
					<TR>
						<TD align="center" valign="middle">		
						${omnitureTracking.trackingCodePart2}
						<SPAN
							style="color: navy; font-family: Arial; vertical-align: middle; font-size: 18px; text-align: center"><h:outputText styleClass="outputText"
							id="textNoData" value="#{labels.noDataDailyMessage}"></h:outputText><BR>
						<BR>
						<h:outputText styleClass="outputText" id="textRoute"
							value="#{labels.route} :"></h:outputText> <h:outputText styleClass="outputText" id="textRouteIDText"
							value="#{route.route.routeID}" style="color: black"></h:outputText> <h:outputText
							styleClass="outputText" id="textPub"
							value="#{labels.publication} :"></h:outputText><font color="black">
						<%=pub %></font> &nbsp;<h:outputText styleClass="outputText"
							id="textWeekOf" value="#{labels.weekOf} :"></h:outputText> <font color="black"><%=date %></font><BR>
						<BR>
						<hx:commandExButton type="reset"
							value="#{labels.returnToDetailWinBtn}"
							styleClass="commandExButton" id="buttonCloseWin" onclick="return func_2(this, event);"></hx:commandExButton> <BR> </SPAN></TD>
					</TR>
				</TBODY>
			</TABLE>
			</CENTER>
		</h:form>
	</hx:scriptCollector>
	</BODY>
</f:view>
</HTML>

<!DOCTYPE HTML>

<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<%@page import="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode"%>
<%@page import="com.usatoday.champion.handlers.LocationHandler"%>
<%@page import="com.usatoday.champion.handlers.ProductOrderHandler"%><html:html>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<jsp:useBean id="RDLForDay"
	class="com.usatoday.champion.handlers.RDLForDayHandler" scope="session"></jsp:useBean>
<jsp:useBean id="RouteDetailRDLPrintWin"
	class="com.usatoday.champion.handlers.RDLForDayHandler" scope="session"></jsp:useBean>
<%
	OmnitureTrackingCode otc = (OmnitureTrackingCode)session.getAttribute("omnitureTracking");
	if (otc == null) {
		otc = new OmnitureTrackingCode();
	}
	String omnitureP1 = otc.getTrackingCodePart1();
 %>
<%=omnitureP1 %>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
	<LINK rel="stylesheet" type="text/css"	href="/scweb/theme/stylesheet.css" title="Style">
	<LINK href="/scweb/theme/Master.css" rel="stylesheet" type="text/css">
	<LINK href="/scweb/theme/C_master_blue.css" rel="stylesheet" type="text/css">
	<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
	<LINK rel="stylesheet" type="text/css" href="/scweb/theme/tabpanel.css" title="Style">
	
	<%
		String usePrintOnly = request.getParameter("useprintonly");
		
		com.usatoday.champion.handlers.RDLForDayHandler rdlForPrint = null;
		if (RouteDetailRDLPrintWin != null && RouteDetailRDLPrintWin.getDdm() != null && usePrintOnly != null) {
			rdlForPrint = RouteDetailRDLPrintWin;
			// remove the printed version from the session once we pull it out.
			session.removeAttribute("RouteDetailRDLPrintWin");
		}
		else {
			rdlForPrint = RDLForDay;
		}
		
		int locationSize = rdlForPrint.getLocations().size();
		
		com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyDrawManagementIntf ddm = rdlForPrint.getDdm();

		org.joda.time.DateTime rdlDate = ddm.getRDLDate();
		
		org.joda.time.format.DateTimeFormatter fmt = new org.joda.time.format.DateTimeFormatterBuilder()
            .appendMonthOfYearShortText()
            .appendLiteral(' ')
            .appendDayOfMonth(2)
            .appendLiteral(',')
            .appendLiteral(' ')
            .appendYear(4,4)
            .toFormatter();
            
        String rdlDateString = fmt.print(rdlDate);
        
        String printTime = new String("&nbsp;Date/Time Printed: " + (new org.joda.time.DateTime()).toString("yyyy-MM-dd HH:mm:ss.SSSZ"));
	%>
<TITLE>USA TODAY - Daily Draw Management (Printer Friendly)</TITLE>
<SCRIPT src="/scweb/scripts/windowutility.js"></SCRIPT>
<SCRIPT type="text/javascript">
// change Omniture Default Page name
s.pageName = "SC Daily Draw Edit Entries - Printer Friendly";
s.channel = "Contractor Route Management";

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	window.print();
	return false;
}

function func_2(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	window.close();
	return false;
}
</SCRIPT>
</HEAD>

<BODY onload="confirmParentOpenPrintWin(); window.print();">
<TABLE width="657" border="0" cellpadding="0" cellspacing="0" style="margin-left: 5px">
	<TBODY>
		<TR>
			<TD colspan="4"><%=otc.getTrackingCodePart2() %></TD>
		</TR>
		<TR>
			<TD nowrap="nowrap" colspan="3"><span class="outputText"
				style="color: black; font-size: 12pt; font-weight: bold;text-align: center">Draw
			Management for RDL Date: </span> <span class="outputText"
				style="font-size: 16px"><%=rdlDateString %></span> </TD>
			<TD nowrap="nowrap" align="right">
				<span id="formManageDraw:textNumLocationHeader" class="outputText_Med" style="color: black">Number Of Locations On Route: </span><SPAN class="outputText_Med" style=""><%=locationSize %>&nbsp;&nbsp;</SPAN>
			</TD>
		</TR>
		<TR>
			<TD nowrap="nowrap" colspan="4" align="center">
				<DIV id="noEditsDiv" style="<%=rdlForPrint.getEdittableFieldsCSS() %>">
					<span class="message">VIEW ONLY NOTICE: There are no edittable draw or return fields for selected date.</span>
				</DIV>
			</TD>
		</TR>
		<TR>
			<TD colspan="4">
				<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
				  <TBODY>
					<TR>
						<TD align="left" nowrap="nowrap">
							<span class="outputText" style="color: black; font-size: 12px; font-weight: bold">District: </span>
							<span class="outputText_Med"><%=ddm.getRoute().getDistrictID() %></span>
							<span class="outputText" style="color: black; font-size: 12px; font-weight: bold">Route: </span>
							<span class="outputText_Med"><%=ddm.getRoute().getRouteID() %></span>
							<span class="outputText" style="color: black; font-size: 12px; font-weight: bold">Description: </span>
							<span class="outputText_Med"><%=ddm.getRoute().getRouteDescription() %></span>
						</TD>
						<TD align="right">
						</TD>
					</TR>
				  </TBODY>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD colspan="4" align="left" valign="bottom" nowrap="nowrap">
			<TABLE width="100%" border="0" cellpadding="0" cellspacing="1"
				bgcolor="#d8d8c8">
				<TBODY>
					<TR bgcolor="#d8d8c8">
						<TD nowrap="nowrap" width="230">
							&nbsp;&nbsp;<span class="outputText"
							style="color: black; font-size: 12px; font-weight: bold">Location
						Information</span>
						</TD>
						<TD nowrap="nowrap">
							<SPAN style="font-weight: bold;font-size: 12px;color: white"><span
							id="formManageDraw:textPOInfo" class="outputText"
							style="color: black">Product Order Information</span></SPAN>
						</TD>
						<TD align="right" nowrap="nowrap">
							<input value="Send To Printer"
							name="formManageDraw:buttonPrintPage"
							id="formManageDraw:buttonPrintPage"
							onclick="return func_1(this, event);" tabindex="1"
							class="commandExButton" type="reset"
							style="margin-left: 2px;margin-top: 2px;margin-bottom: 2px; width: 120px">&nbsp;<input
							value="Close Window" name="formManageDraw:buttonCloseWindow"
							id="formManageDraw:buttonCloseWindow"
							onclick="return func_2(this, event);" class="commandExButton"
							type="reset"
							style="margin-top: 2px;margin-bottom: 2px;; width: 115px">
						</TD>
					</TR>
				</TBODY>
			</TABLE>
			</TD>
		</TR>
	</TBODY>
</TABLE>
<!-- Tables split to enhance page loading/browser rendering -->
<TABLE  width="667" border="0" cellpadding="0" cellspacing="0" style="margin-left: 5px">
	<TBODY>
		<TR>
			<TD align="left" valign="top" nowrap="nowrap" colspan="4">

			<!-- Outer table -->
			<TABLE  width="675" cellspacing="0" cellpadding="0" border="0">
				<TBODY>
					<TR>
						<TD class="tabbedPanel-Body" width="100%" valign="top" height="100%">
						
								<table id="formManageDraw:tableDailyDrawDataTable" class="dataTable" width="635" cellspacing="0" cellpadding="2" border="1">
									<tbody>
									<%
										
										java.util.Collection<LocationHandler> locations = rdlForPrint.getLocations();
										java.util.Iterator<LocationHandler> lItr = locations.iterator();
										boolean isOddRow = true;
										int i = 0;
										while (lItr.hasNext()) {
											LocationHandler dloc = lItr.next();
											String locId = dloc.getLocationID();
											String locName = dloc.getLocationName();
											String locNameNoApostrophes = locName.replaceAll("'","");
											String locAddr1 = dloc.getLocationAddress1();
											String locPhone = dloc.getLocationPhoneNumberFormatted();
											String locHashKey = dloc.getDdl().getLocationHashKey();
											String rowClassName = null;
											if (isOddRow) {
												rowClassName = "rowClass1";
												isOddRow = false;
											}
											else {
												rowClassName = "rowClass3";
												isOddRow = true;
											}
											
											java.util.Iterator<ProductOrderHandler> poItr = dloc.getProductOrders().iterator();
									 %>
										<tr class="<%=rowClassName %>">
											<td class="columnClass1" width="225" valign="top" align="left">
												<!-- location column -->
												<table width="223" cellspacing="1" cellpadding="0" border="0">
													<tbody>
														<tr>
															<td>
																<span class="outputText"><%=locId %></span>
															</td>
														</tr>
														<tr>
															<td>
																<span class="outputText"><%=locName %></span>
															</td>
														</tr>
														<tr>
															<td>
																<span class="outputText"><%=locAddr1 %></span>
															</td>
														</tr>
														<tr>
															<td>
																<span class="outputText"><%=locPhone %></span>
															</td>
														</tr>
													</tbody>
												</table>
												<!-- end location column -->																													
											</td>
											<td class="columnClass1" width="450" valign="top" align="left">
												<!-- inner data table -->
												<table id="formManageDraw:tableDailyDrawDataTable:<%=i %>:tableInsideProductOrderDataTable" class="dataTable" cellspacing="0" cellpadding="2" border="1">
													<thead>
													<tr>
														<th scope="col" class="headerClass"><span class="outputText">ID</span>
														</th>
														<th scope="col" class="headerClass"><span class="outputText">Type</span>
														</th>
														<th scope="col" class="headerClass"><span class="outputText">Pub ID</span>
														</th>
														<th scope="col" class="headerClass" align="center"><span class="outputText">&nbsp;&nbsp;Draw&nbsp;&nbsp;</span>
														</th>
														<th scope="col" class="headerClass" align="center" nowrap="nowrap"><span class="outputText">&nbsp;Returns Date</span></th>
														<th scope="col" class="headerClass" align="center"><span class="outputText">Returns</span>
														</th>
													</tr>
													</thead>												
													<tbody>

													<%
														boolean innerRowIsOdd = true;
														while (poItr.hasNext()) {
															ProductOrderHandler po = poItr.next();
															String poID = po.getProductOrderID();
															String innerRowClass = null;
															if (innerRowIsOdd) {
																innerRowClass = "rowClass1";
																innerRowIsOdd = false;
															}
															else {
																innerRowClass = "rowClass2";
																innerRowIsOdd = true;
															}
															String returnFieldClass = "inputText";
															if (po.getReturnDate() == null) {
																returnFieldClass += " inputText_disabled";
															}
															
													 %>
														<tr class="<%=innerRowClass %>">
															<td class="columnClass1" valign="middle" align="center">
																<span class="outputText"><%=po.getProductOrderID() %></span>
															</td>
															<td class="columnClass1" valign="middle" align="center">
																	<span class="outputText"><%=po.getProductOrderType() %></span>
															</td>
															<td align="center" class="columnClass1">
																<span class="outputText"><%=po.getProductID() %></span>
															</td>
															<td class="columnClass1" width="70" nowrap="nowrap" align="center">
																<table width="70">
																	<tr>
																		<td align="center">
																			<input type="text" size="3" class="outputText" readonly="readonly" value="<%=po.getDrawDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:DrawValue" />
																		</td>
																		<td width="18">
																		</td>
																	</tr>
																</table>
															</td>
															<td align="center" class="columnClass1" width="100">
																<table>
																<tbody>
																	<tr>
																		<td>
																			<img hspace="3" class="graphicImageEx" src="/scweb/theme/1x1.gif"/>
																		</td>
																		<td align="right">
																			<span class="outputText"><%=po.getReturnDate2Str() %></span>
																		</td>
																		<td>
																			<img width="16" height="16" class="graphicImageEx" title="NOTICE: Late Returns Day, you have already been invoiced for this day." style="<%=po.getLateReturnsCSSString() %>" src="/scweb/images/imageTextBorder.gif"/>
																		</td>
																	</tr>
																</tbody>
																</table>
															</td>															
															<td class="columnClass1">
																<input type="text" size="3" class="outputText"  readonly="readonly" value="<%=po.getReturnsDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:ReturnValue"/>
															</td>
														</tr>

															
													<% 
														} // end while more product orders
													%>
													</tbody>																															
												</table>
												<!-- end inner data table -->
											</td>
										</tr>
										<%		
												i++;
											} // end while more locations
										%>															
									</tbody>
								</table>
							</td>
						</tr>
				</TBODY>
			</TABLE>
			<!-- End Outer Table (locations) -->
		</TD>
	</TR>
  </TBODY>
</TABLE>
<%=printTime %>

</BODY>
</html:html>

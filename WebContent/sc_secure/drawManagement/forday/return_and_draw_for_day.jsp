<!DOCTYPE HTML>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/sc_secure/drawManagement/forday/Return_and_draw_for_day.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<%@page import="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode"%><HTML>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<f:view locale="#{userLocaleHandler.locale}">
<f:loadBundle basename="resourcesFile" var="labels" />
	<HEAD>

<%
	int locationSize = ((com.usatoday.champion.handlers.RDLForDayHandler)request.getSession().getAttribute("RDLForDay")).getLocations().size();

	OmnitureTrackingCode otc = (OmnitureTrackingCode)session.getAttribute("omnitureTracking");
	if (otc == null) {
		otc = new OmnitureTrackingCode();
	}
	String omnitureP1 = otc.getTrackingCodePart1();
 %>
 <%=omnitureP1 %>


	<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<META http-equiv="Content-Style-Type" content="text/css">
	<LINK href="/scweb/theme/Master.css" rel="stylesheet" type="text/css">
	<LINK href="/scweb/theme/C_master_blue.css" rel="stylesheet"
		type="text/css">
	<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet"
		type="text/css">
	<TITLE>Daily Draw Management</TITLE>
	<LINK rel="stylesheet" type="text/css" href="/scweb/theme/datagrid.css"
		title="Style">
	<LINK rel="stylesheet" type="text/css" href="/scweb/theme/tree.css"
		title="Style">
	<LINK rel="stylesheet" type="text/css" href="/scweb/theme/tabpanel.css"
		title="Style">
	<LINK rel="stylesheet" type="text/css"
		href="/scweb/theme/stylesheet.css" title="Style">
	<SCRIPT src="/scweb/scripts/browserVersion.js" type="text/javascript"></SCRIPT>
	<SCRIPT src="/scweb/scripts/scriptaculous/prototype.js"
		type="text/javascript"></SCRIPT>
	<SCRIPT src="/scweb/scripts/scriptaculous/scriptaculous.js"
		type="text/javascript"></SCRIPT>
	<SCRIPT src="/scweb/scripts/drawmngmt.js"></SCRIPT>
	<SCRIPT src="/scweb/scripts/windowutility.js"></SCRIPT>
	<SCRIPT src="/scweb/scripts/keepalive.js"></SCRIPT>
	<SCRIPT type="text/javascript">
// change Omniture Default Page name
s.pageName = "SC Daily Draw Edit Entries";

Event.observe(window, 'load', function() {

	$('StatusMessageField').value = 'Please Wait....';
	var inputArray = Form.getInputs('formManageDraw', 'text');
	var firstFound = false;
	var focusIndex = -1;
	for (i = 0; i < inputArray.length; i++) {
		if (inputArray[i].disabled == false && inputArray[i] != $('StatusMessageField')) {
			if (firstFound == false) {
				focusIndex = i;
				firstFound = true;
			}
		}
		else {
			if (inputArray[i] != $('StatusMessageField')) {
				$(inputArray[i]).addClassName('inputText_disabled');
			}
		}
	}
	if (focusIndex > -1) {
		inputArray[focusIndex].focus();
		inputArray[focusIndex].select();
	}

	//Event.observe('formManageDraw', 'keydown', function(anEvent) {
	//	return aKeyPressed(anEvent);
	//});

	//Event.observe('formManageDraw:buttonSubmitDrawChanges', 'keydown', function(anEvent) {
	//	if (anEvent.keyCode == Event.KEY_RETURN) {
	//		sClicked();
	//	}
	//});

	Event.observe('formManageDraw:buttonSubmitDrawChanges', 'click', function() {
		sClicked();
	});
	
	startKeepAlive();
	
	$('StatusMessageField').value = '';
		
});

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'

}</SCRIPT>
	</HEAD>
	<BODY onbeforeunload="confirmUnload(this, event)">
	<hx:scriptCollector id="scriptCollector1">
		<DIV id="tooltip" class="usat_tooltip"></DIV>
		<SCRIPT src="/scweb/scripts/usat_tooltip.js"></SCRIPT>
		<h:form styleClass="form" id="formManageDraw">
			<CENTER>
			<TABLE width="95%" border="0" cellpadding="0" cellspacing="0">
				<TBODY>
					<TR>
						<TD colspan="4"><hx:outputSeparator styleClass="outputSeparator"
							id="separator1" color="#90a0b8" height="3" align="center"></hx:outputSeparator>${omnitureTracking.trackingCodePart2}</TD>
					</TR>
					<TR>
						<TD nowrap="nowrap"><SPAN
							style="color: black; font-size: 12pt; font-weight: bold;text-align: center"><h:outputText
							styleClass="outputText" id="textDailyDMTitleLabel"
							value="#{labels.dmDailyTitle}:"
							style="color: black; font-size: 12pt; font-weight: bold;text-align: center"></h:outputText>
						</SPAN><h:outputText id="textCurrentRDLDate"
							value="#{RDLForDay.day}" styleClass="outputText"
							style="font-size: 18px">
							<f:convertDateTime />
						</h:outputText><A href="javascript:void(0);"
							onclick="javascript: helpScreen('/scweb/sc_secure/drawManagement/forday/dailyHelpFile.usat'); return false;"
							title="Weekly Draw Entry Help"
							onmouseover="window.status='Click for Help On This Page'"
							onmouseout="window.status=' '"> <IMG border="0"
							src="/scweb/images/question_mark.gif" width="16" height="16"
							hspace="10"></A></TD>
						<TD nowrap="nowrap"><FONT color="black"></FONT></TD>
						<TD></TD>
						<TD align="right"><FONT color="black"><h:outputText
							styleClass="outputText" id="textNumLocationHeader"
							value="#{labels.dmNumLocations}: "></h:outputText></FONT> <%=locationSize %></TD>
					</TR>
					<TR>
						<TD nowrap="nowrap" colspan="4" align="center">
							<DIV id="edittableFieldsMsgDiv" style="${RDLForDay.edittableFieldsCSS}" >
								<h:outputText styleClass="message" id="textNotEdittable" value="#{labels.RDLNoEdittableFieldsDaily}"></h:outputText>
							</DIV>
						</TD>
					</TR>
					<TR>
						<TD nowrap="nowrap"><SPAN
							style="font-weight: bold;font-size: 12px;color: black"><h:outputText
							styleClass="outputText" id="textRouteIDHeaderLabel"
							value="#{labels.route}:"
							style="color: black; font-size: 12px; font-weight: 500"></h:outputText></SPAN>
						<h:outputText id="textRouteIDText" value="#{route.route.routeID}"
							styleClass="outputText" style="font-size: 14px; font-weight: 500">
						</h:outputText> <SPAN
							style="font-weight: bold;font-size: 12px;color: black"><h:outputText
							styleClass="outputText" id="textRouteDescHeaderLabel"
							value="#{labels.description}: "
							style="color: black; font-size: 12px; font-weight: 500"></h:outputText></SPAN>
						<h:outputText styleClass="outputText" id="textRouteDescription"
							value="#{route.route.routeDescription}"
							style="font-size: 14px; font-weight: 500"></h:outputText></TD>
						<TD></TD>
						<TD></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD colspan="4" align="left">
							<table width="100%">
								<tr>
									<td width="300">
										<IMG border="0" src="/scweb/theme/1x1.gif" width="1" height="1" vspace="5">
										<h:messages styleClass="messages" id="messagesPageLevelErrors" globalOnly="true"></h:messages>
									</td>
									<td align="right" nowrap="nowrap">
										<h:outputText styleClass="outputText" id="textDrawEntryStatus" value="#{labels.status}: " style="color: black; font-weight: bold"></h:outputText>
										<INPUT type="text" name="StatusMessageField" id="StatusMessageField" style="color: red font-size 10px" readonly tabindex="998" size="60">
									</td>
								</tr>
							</table>
						</TD>
					</TR>
					<TR>
						<TD nowrap="nowrap" colspan="4" valign="bottom">
						<TABLE width="100%" border="0" cellpadding="2" cellspacing="0"
							bgcolor="#95a5b9">
							<TBODY>
								<TR>
									<TD width="230" nowrap="nowrap"><FONT color="white"><SPAN
										style="font-weight: bold;font-size: 12px">&nbsp;&nbsp;<h:outputText
										styleClass="outputText" id="textLocationInfo"
										value="#{labels.dmLocationHeader}" style="color: white"></h:outputText></SPAN></FONT></TD>
									<TD nowrap="nowrap"><FONT color="white"><SPAN
										style="font-weight: bold;font-size: 12px"><h:outputText
										styleClass="outputText" id="textPOInfo" style="color: white"
										value="#{labels.dmPOHeaderInformationDailyHeader}"></h:outputText></SPAN></FONT></TD>
									<TD nowrap="nowrap" align="right"></TD>
								</TR>
							</TBODY>
						</TABLE>
						</TD>
					</TR>
					<TR>
						<TD nowrap="nowrap" colspan="4" align="left" valign="top"><odc:tabbedPanel
							slantActiveRight="4" styleClass="tabbedPanel" width="675"
							slantInactiveRight="4" height="550" variableTabLength="true"
							showBackNextButton="false" showTabs="false"
							id="tabbedPanelProductOrders">
							<odc:bfPanel id="bfpanelMainPOPanel" name="DrawManagement"
								showFinishCancelButton="false">
								
								<hx:panelLayout styleClass="panelLayout" id="layout1"
									align="left">
									<f:facet name="body">
										<h:dataTable border="0" cellpadding="2" cellspacing="0"
											columnClasses="columnClass1" headerClass="headerClass"
											footerClass="footerClass" rowClasses="rowClass1, rowClass3"
											styleClass="dataTable" id="table1"
											value="#{RDLForDay.locations}" var="varlocations" width="645">
											<h:column id="columnLocationColumn">
												<hx:jspPanel id="jspPanelLocationInfoPanel1">


													<TABLE border="0" cellpadding="0" cellspacing="1"
														width="225">
														<TBODY>
															<TR>
																<TD align="left"><h:outputText id="textLocIdVar"
																	value="#{varlocations.locationID}"
																	styleClass="outputText">
																</h:outputText></TD>
																<TD align="left"></TD>
															</TR>
															<TR>
																<TD colspan="2" width="225" align="left"><h:outputText
																	id="textLocNameVar"
																	value="#{varlocations.locationName}"
																	styleClass="outputText">
																</h:outputText></TD>
															</TR>
															<TR>
																<TD colspan="2" align="left"><h:outputText
																	id="textLocAddr1Var"
																	value="#{varlocations.locationAddress1}"
																	styleClass="outputText">
																</h:outputText></TD>
															</TR>
															<TR>
																<TD colspan="2" align="left"><h:outputText
																	styleClass="outputText" id="textLocPhoneNumberVar"
																	value="#{varlocations.phoneNumber}">
																	<hx:convertMask mask="(###) ###-####" />
																</h:outputText></TD>
															</TR>
														</TBODY>
													</TABLE>

												</hx:jspPanel>
												<f:facet name="header">
												</f:facet>
												<f:attribute value="top" name="valign" />
												<f:attribute value="left" name="align" />
												<f:attribute value="225" name="width" />
											</h:column>
											<h:column id="columnProductOrdersOuterColumn">
												<h:dataTable border="0" cellpadding="2" cellspacing="0"
													columnClasses="columnClass1" headerClass="headerClass"
													footerClass="footerClass" rowClasses="rowClass1, rowClass2"
													styleClass="dataTableDailyPO"
													id="tableProductOrdersDataTable"
													value="#{varlocations.productOrders}"
													var="varproductOrders">
													<h:column id="column376">
														<h:outputText id="textProductOrder"
															value="#{varproductOrders.productOrderID}"
															styleClass="outputText"></h:outputText>
														<f:facet name="header">
															<h:outputText id="text4" styleClass="outputText"
																value="ID"></h:outputText>
														</f:facet>
														<f:attribute value="center" name="align" />
													</h:column>
													<h:column id="column344">
														<hx:outputLinkEx styleClass="outputLinkEx"
															value="javascript:void(0);" id="linkExProductOrderType"
															title="#{varproductOrders.productOrderTypeDescription}"
															tabindex="9999" onclick="return false;">
															<h:outputText id="textProductOrderType"
																value="#{varproductOrders.productOrderType}"
																styleClass="outputText"></h:outputText>
														</hx:outputLinkEx>

														<f:facet name="header">
															<h:outputText styleClass="outputText" value="Type"
																id="text3"></h:outputText>
														</f:facet>
														<f:attribute value="center" name="align" />

													</h:column>
													<h:column id="column498">
														<f:facet name="header">
															<h:outputText styleClass="outputText" value="Pub ID"
																id="text6"></h:outputText>
														</f:facet>
														<hx:outputLinkEx styleClass="outputLinkEx"
															value="javascript:void(0);" tabindex="9999"
															id="linkExProductDescriptionLink"
															title="#{varproductOrders.productDescription}"
															onclick="return false;">
															<h:outputText styleClass="outputText"
																id="textProductTypeText"
																value="#{varproductOrders.productID}"></h:outputText>
														</hx:outputLinkEx>
														<f:attribute value="center" name="align" />
													</h:column>
													<h:column id="column625">
														<h:inputText styleClass="inputText"
															id="textDrawValueInput"
															value="#{varproductOrders.drawValueObj}" size="3"
															onchange="return drawValueChangedDaily(this, event, #{varproductOrders.maxDrawValue}, #{varproductOrders.drawValue});">
															<f:convertNumber integerOnly="true" />
														</h:inputText>
														<h:inputHidden id="hiddenZeroDrawReason"
															value="#{varproductOrders.zeroDrawReasonCode}"></h:inputHidden>
														<h:message styleClass="message" id="messageDrawInvalid"
															for="textDrawValueInput"></h:message>
														<f:facet name="header">
															<h:outputText styleClass="outputText" value="Draw"
																id="text9"></h:outputText>
														</f:facet>
													</h:column>
													<h:column id="column959">
														<hx:jspPanel id="jspPanel1">
															<table>
																<tr>
																	<td><hx:graphicImageEx styleClass="graphicImageEx"
																		id="imageEx99" value="/scweb/theme/1x1.gif" hspace="3"></hx:graphicImageEx>
																	</td>
																	<td align="right"><h:outputText
																		id="textProductOrderReturnDate"
																		value="#{varproductOrders.returnDate}"
																		styleClass="outputText">
																		<f:convertDateTime dateStyle="short" />
																	</h:outputText></td>
																	<td><hx:graphicImageEx styleClass="graphicImageEx"
																		id="imageEx1" title="#{labels.dmLateReturnDailyMsg}"
																		value="/scweb/images/imageTextBorder.gif"
																		style="#{varproductOrders.lateReturnsCSSString}"
																		width="16" height="16"
																		onclick="alert('#{labels.dmLateReturnDailyMsg}');"></hx:graphicImageEx>
																	</td>
																</tr>
															</table>

														</hx:jspPanel>
														<f:facet name="header">
															<h:outputText styleClass="outputText"
																value="Returns Date" id="text2"></h:outputText>
														</f:facet>
														<f:attribute value="center" name="align" />
													</h:column>
													<h:column id="column567">
														<h:inputText styleClass="inputText"
															id="textReturnValueInput"
															value="#{varproductOrders.returnValueObj}" size="3"
															onchange="return returnValueChanged(this, event, #{varproductOrders.returnValue}, #{varproductOrders.maxReturnValue});">
															<f:convertNumber integerOnly="true" />
														</h:inputText>
														<h:message styleClass="message" id="messageReturnMesssage"
															for="textReturnValueInput"></h:message>
														<f:facet name="header">
															<h:outputText styleClass="outputText" value="Returns"
																id="text8"></h:outputText>
														</f:facet>
													</h:column>
												</h:dataTable>
												<f:facet name="header">
												</f:facet>
												<f:attribute value="top" name="valign" />
											</h:column>
										</h:dataTable>
									</f:facet>
									<f:facet name="left"></f:facet>
									<f:facet name="right"></f:facet>
									<f:facet name="bottom"></f:facet>
									<f:facet name="top"></f:facet>
								</hx:panelLayout>
							</odc:bfPanel>
							<f:facet name="back">
								<hx:commandExButton type="submit" value="&lt; Back"
									id="tabbedPanel1_back" style="display:none"></hx:commandExButton>
							</f:facet>
							<f:facet name="next">
								<hx:commandExButton type="submit" value="Next &gt;"
									id="tabbedPanel1_next" style="display:none"></hx:commandExButton>
							</f:facet>
							<f:facet name="finish">
								<hx:commandExButton type="submit" value="Finish"
									id="tabbedPanel1_finish" style="display:none"></hx:commandExButton>
							</f:facet>
							<f:facet name="cancel">
								<hx:commandExButton type="submit" value="Cancel"
									id="tabbedPanel1_cancel" style="display:none"></hx:commandExButton>
							</f:facet>
						</odc:tabbedPanel></TD>
					</TR>
					<TR>
						<TD colspan="4" align="center">
						<TABLE width="90%" border="0" cellpadding="7" cellspacing="4">
							<TBODY>
								<TR>
									<TD width="33%" align="right"><hx:commandExButton type="submit"
										value="#{labels.dmSubmitFormButtonLabel}"
										styleClass="commandExButton" id="buttonSubmitDrawChanges"
										action="#{pc_Return_and_draw_for_dayV2.doButtonSubmitDrawChangesAction}"></hx:commandExButton></TD>
									<TD width="33%" align="center"><hx:commandExButton type="reset"
										value="#{labels.dmResetFormButtonLabel}"
										styleClass="commandExButton" id="buttonResetValues"
										confirm="#{labels.resetFormWarningPrompt}"></hx:commandExButton></TD>
									<TD align="left" width="34%"><hx:commandExButton type="submit"
										value="#{labels.dmQuiteFormLabel}"
										styleClass="commandExButton" id="buttonQuitNoSave"
										action="#{pc_Return_and_draw_for_dayV2.doButtonQuitNoSaveAction}"></hx:commandExButton></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD colspan="3">
									<DIV id="ZeroDrawReasonChooser" class="box1" align="center"
										style="display: none;z-index:1000;border-bottom: 0px solid #90a0b8;border-top: 0px solid #90a0b8;border-left: 0px solid #90a0b8;border-right: 0px solid #90a0b8;width: 400px;background: white;position:absolute;; top: 250px; left: 200px; height: 200px">
									<TABLE class="ZeroDrawTable" width="350" border="0"
										cellpadding="2" cellspacing="1" bgcolor="white">
										<TBODY>
											<TR>
												<TD></TD>
												<TD><FONT color="black"><SPAN
													style="font-size: 14px;color: white"><SPAN
													style="font-size: 14px;color: white;font-weight: bold"><SPAN
													style="font-size: 14px;color: #90a0b8;font-weight: bold">Select
												a Zero Draw Reason:</SPAN></SPAN></SPAN></FONT></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD><SELECT size="1" name="ZeroDrawReasonSelector"
													id="ZeroDrawReasonSelector" class="selectOneListbox">
													<%
														com.usatoday.singlecopy.model.util.ZeroDrawReasonCodeCache rc = com.usatoday.singlecopy.model.util.ZeroDrawReasonCodeCache.getInstance();
														java.util.Iterator<com.usatoday.singlecopy.model.interfaces.drawmngmt.ZeroDrawReasonCodeIntf> itr =  rc.getZeroDrawReasonCodes().iterator();
														while (itr.hasNext()) {
															com.usatoday.singlecopy.model.interfaces.drawmngmt.ZeroDrawReasonCodeIntf zdr = itr.next();
															
													 %>
													<OPTION value="<%=zdr.getCode()%>"><%=zdr.getDescription() %></OPTION>
													<%
														}
													 %>
												</SELECT></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD><INPUT type="button" name="ZDRFinished" value="Done"
													onclick="return zeroDrawValueSelected(this, event);"></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD align="center" valign="middle"
													style="font-size: 14px;color: columnFields;font-weight: bold"></TD>
											</TR>
										</TBODY>
									</TABLE>
									</DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						</TD>
					</TR>
				</TBODY>
			</TABLE>
			</CENTER>
			<SCRIPT type="text/javascript" language="javascript" charset="utf-8">
						// <![CDATA[
						  new Draggable('ZeroDrawReasonChooser',{scroll:window,zindex:1000});
					  	// ]]>
						</SCRIPT>
			<BR>
			<hx:inputHelperKeybind key="Enter" id="inputHelperKeybind1"
				targetAction="nexttab" />
		</h:form>
	</hx:scriptCollector>
	</BODY>
</f:view>
</HTML>

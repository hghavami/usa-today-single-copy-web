<!DOCTYPE HTML>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/sc_secure/drawManagement/forday/CloseRDLWindow.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<HTML>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<f:view locale="#{userLocaleHandler.locale}">
	<HEAD>
	<f:loadBundle basename="resourcesFile" var="labels" />
	<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<META http-equiv="Content-Style-Type" content="text/css">
	<LINK href="/scweb/theme/Master.css" rel="stylesheet" type="text/css">
	<LINK href="/scweb/theme/C_master_blue.css" rel="stylesheet"
		type="text/css">
	<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet"
		type="text/css">
	<TITLE>Please Wait..Cleaning up.</TITLE>
<SCRIPT type="text/javascript" language="javascript">
	function func_1() {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	window.close();
}

</SCRIPT>
	</HEAD>
	<BODY onload="func_1();">
	<hx:scriptCollector id="scriptCollector1">
		<CENTER>
		<P></P>
		<TABLE width="95%" border="1" height="95%">
			<TBODY>
				<TR>
					<TD align="center"><h:outputText styleClass="outputText"
						id="textCleanUp" value="#{labels.cleanUp}" style="font-size: 14pt" escape="false"></h:outputText></TD>
				</TR>
			</TBODY>
		</TABLE>
		</CENTER>
	</hx:scriptCollector>
	</BODY>
</f:view>
</HTML>

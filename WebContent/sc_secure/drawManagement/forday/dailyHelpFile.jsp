<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/sc_secure/drawManagement/forday/DailyHelpFile.java" --%><%-- /jsf:pagecode --%>
<%-- tpl:insert page="/theme/JSP_usat_helpTemplate.jtpl" --%><!DOCTYPE HTML>

<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<HTML>
<f:view  locale="#{userLocaleHandler.locale}">
	<f:loadBundle basename="resourcesFile" var="labels" />
	<f:loadBundle basename="resourcesHelpContent" var="help" />
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
 <h:outputText id="textOmnitureTrackingCode1" escape="false" value="#{omnitureTracking.trackingCodePart1}"></h:outputText><META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="${pageContext.request.contextPath}/theme/Master.css" rel="stylesheet" type="text/css">
<LINK href="${pageContext.request.contextPath}/theme/C_master_blue.css" rel="stylesheet" type="text/css">
<LINK href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
<script src="${pageContext.request.contextPath}/scripts/scriptaculous/prototype.js" type="text/javascript"></script>
<SCRIPT src="${pageContext.request.contextPath}/scripts/windowutility.js"></SCRIPT>
<%-- tpl:put name="headarea" --%>
	<TITLE>Weekly Draw Management Help</TITLE>
	<SCRIPT language="JavaScript">
	// change Omniture Default Page name
	s.pageName = "SC Daily Draw Help";
	s.channel = "Contractor Route Management";
	
	// this method will attempt to scroll to the specified element
	//
	function scrollToElement(eName){
		$(eName).scrollTo();
		return false;
	}
	</SCRIPT>
			
	<%-- /tpl:put --%>
<SCRIPT type="text/javascript">

Event.observe(window, 'load', function() {
	confirmParentOpenHelpWin();
		
});

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
window.close();
}</SCRIPT></HEAD>
	<BODY bgcolor="#5677bc"><hx:scriptCollector id="scriptCollector1">
		<TABLE class="usatHelpTable">
			<TBODY>
				<TR>
					<TD style='background-image: url("/scweb/theme/images/800x20_art2.jpg"); background-repeat: repeat-x'>&nbsp;</TD>
				</TR>
				<TR align="right">
					<TD>${omnitureTracking.trackingCodePart2}
					<hx:outputLinkEx styleClass="outputLinkEx"
						value="javascript:void(0);return false;" id="linkExCloseWindowTop"
						onclick="return func_1(this, event);">
						<h:outputText id="textCloseWindowTop" styleClass="outputText"
							value="Close Window"></h:outputText>
					</hx:outputLinkEx></TD>
				</TR>
				<TR>
					<TD><%-- tpl:put name="bodyarea" --%><hx:scriptCollector id="scriptCollector2">
									<A name="topOfPage"></A>
								<TABLE width="95%" border="0" cellpadding="0" cellspacing="0">
									<TBODY>
										<tr>
											<td align="center" nowrap="nowrap">
												<h:outputText styleClass="outputText_helpHeader"
													id="textDMHelpHeader" value="#{help.DailyDrawHelpHeader}"></h:outputText>
													<BR><BR>
											</td>
										</tr>
										<TR>
											<TD>
												<UL>
													<LI>
														<hx:outputLinkEx styleClass="outputLinkEx" value="javascript:;"
															id="linkExHQ1" onclick="scrollToElement('textQ1');">
															<h:outputText id="textQ1H"
																styleClass="outputText_help_question_header"
																value="#{help.GQ4}"></h:outputText>
														</hx:outputLinkEx>
													</LI>
													<LI>
														<hx:outputLinkEx styleClass="outputLinkEx" value="javascript:;"
															id="linkExHQ2" onclick="scrollToElement('textQ2');">
															<h:outputText id="textQ2H"
																styleClass="outputText_help_question_header"
																value="#{help.GQ5}"></h:outputText>
														</hx:outputLinkEx>
													</LI>
													<LI>
														<hx:outputLinkEx styleClass="outputLinkEx" value="javascript:;"
															id="linkExHQ3" onclick="scrollToElement('textQ3');">
															<h:outputText id="textQ3H"
																styleClass="outputText_help_question_header"
																value="#{help.GQ2}"></h:outputText>
														</hx:outputLinkEx>
													</LI>
													<LI>
														<hx:outputLinkEx styleClass="outputLinkEx" value="javascript:;"
															id="linkExHQ4" onclick="scrollToElement('textQ4');">
															<h:outputText id="textQ4H"
																styleClass="outputText_help_question_header"
																value="#{help.GQ6}"></h:outputText>
														</hx:outputLinkEx>
													</LI>
													<LI>
														<hx:outputLinkEx styleClass="outputLinkEx" value="javascript:;"
															id="linkExHQ5" onclick="scrollToElement('textQ5');">
															<h:outputText id="textQ5H"
																styleClass="outputText_help_question_header"
																value="#{help.GQ7}"></h:outputText>
														</hx:outputLinkEx>
													</LI>
													<LI>
														<hx:outputLinkEx styleClass="outputLinkEx" value="javascript:;"
															id="linkExHQ6" onclick="scrollToElement('textQ6');">
															<h:outputText id="textQ6H"
																styleClass="outputText_help_question_header"
																value="#{help.GQ8}"></h:outputText>
														</hx:outputLinkEx>
													</LI>
													<LI>
														<hx:outputLinkEx styleClass="outputLinkEx" value="javascript:;"
															id="linkExHQ7" onclick="scrollToElement('textQ7');">
															<h:outputText id="textQ7H"
																styleClass="outputText_help_question_header"
																value="#{help.GQ9}"></h:outputText>
														</hx:outputLinkEx>
													</LI>
													<LI>
														<hx:outputLinkEx styleClass="outputLinkEx" value="javascript:;"
															id="linkExHQ8" onclick="scrollToElement('textQ8');">
															<h:outputText id="textQ8H"
																styleClass="outputText_help_question_header"
																value="#{help.GQ10}"></h:outputText>
														</hx:outputLinkEx>
													</LI>
													<LI>
														<hx:outputLinkEx styleClass="outputLinkEx" value="javascript:;"
															id="linkExHQ9" onclick="scrollToElement('textQ9');">
															<h:outputText id="textQ9H"
																styleClass="outputText_help_question_header"
																value="#{help.GQ11}"></h:outputText>
														</hx:outputLinkEx>
													</LI>
													<LI>
														<hx:outputLinkEx styleClass="outputLinkEx" value="javascript:;"
															id="linkExHQ10" onclick="scrollToElement('textQ10');">
															<h:outputText id="textQ10H"
																styleClass="outputText_help_question_header"
																value="#{help.GQ13}"></h:outputText>
														</hx:outputLinkEx>
													</LI>
												</UL>
											</TD>
										</TR>
										<TR>
											<TD>
												<hx:outputSeparator styleClass="outputSeparator"
													id="separator1"></hx:outputSeparator>
											<BR>
											</TD>
										</TR>
									</TBODY>
								</TABLE>
								
								<TABLE width="95%" border="0" cellpadding="0" cellspacing="0">
									<TBODY>
										<TR>
											<TD>
								
											<hx:panelBox styleClass="panelBox"
												id="boxAnswers" layout="pageDirection" align="left"
												cellpadding="2" cellspacing="2">

												<h:outputText styleClass="outputText_help_question"
													id="textQ1" value="#{help.GQ4}"></h:outputText>
												<h:outputText 
													id="textA1" value="#{help.GA4}" escape="false"></h:outputText>
												<hx:outputLinkEx styleClass="outputLinkEx2"
													value="#topOfPage" id="linkExBTP1">
													<h:outputText id="textBTP1" styleClass="outputText"
														value="Back to Top"></h:outputText>
												</hx:outputLinkEx>

												<h:outputText styleClass="outputText_help_question"
													id="textQ2" value="#{help.GQ5}"></h:outputText>
												<h:outputText 
													id="textA2" value="#{help.GA5}" escape="false"></h:outputText>
												<hx:outputLinkEx styleClass="outputLinkEx2"
													value="#topOfPage" id="linkExBTP2">
													<h:outputText id="textBTP2" styleClass="outputText"
														value="Back to Top"></h:outputText>
												</hx:outputLinkEx>

												<h:outputText styleClass="outputText_help_question"
													id="textQ3" value="#{help.GQ2}"></h:outputText>
												<h:outputText 
													id="textA3" value="#{help.GA2}" escape="false"></h:outputText>
												<hx:outputLinkEx styleClass="outputLinkEx2"
													value="#topOfPage" id="linkExBTP3">
													<h:outputText id="textBTP3" styleClass="outputText"
														value="Back to Top"></h:outputText>
												</hx:outputLinkEx>

												<h:outputText styleClass="outputText_help_question"
													id="textQ4" value="#{help.GQ6}"></h:outputText>
												<h:outputText 
													id="textA4" value="#{help.GA6}" escape="false"></h:outputText>
												<hx:outputLinkEx styleClass="outputLinkEx2"
													value="#topOfPage" id="linkExBTP4">
													<h:outputText id="textBTP4" styleClass="outputText"
														value="Back to Top"></h:outputText>
												</hx:outputLinkEx>

												<h:outputText styleClass="outputText_help_question"
													id="textQ5" value="#{help.GQ7}"></h:outputText>
												<h:outputText 
													id="textA5" value="#{help.GA7}" escape="false"></h:outputText>
												<hx:outputLinkEx styleClass="outputLinkEx2"
													value="#topOfPage" id="linkExBTP5">
													<h:outputText id="textBTP5" styleClass="outputText"
														value="Back to Top"></h:outputText>
												</hx:outputLinkEx>

												<h:outputText styleClass="outputText_help_question"
													id="textQ6" value="#{help.GQ8}"></h:outputText>
												<h:outputText 
													id="textA6" value="#{help.GA8}" escape="false"></h:outputText>
												<hx:outputLinkEx styleClass="outputLinkEx2"
													value="#topOfPage" id="linkExBTP6">
													<h:outputText id="textBTP6" styleClass="outputText"
														value="Back to Top"></h:outputText>
												</hx:outputLinkEx>
												
												<h:outputText styleClass="outputText_help_question"
													id="textQ7" value="#{help.GQ9}"></h:outputText>
												<h:outputText 
													id="textA7" value="#{help.GA9}" escape="false"></h:outputText>
												<hx:outputLinkEx styleClass="outputLinkEx2"
													value="#topOfPage" id="linkExBTP7">
													<h:outputText id="textBTP7" styleClass="outputText"
														value="Back to Top"></h:outputText>
												</hx:outputLinkEx>
												
												<h:outputText styleClass="outputText_help_question"
													id="textQ8" value="#{help.GQ10}"></h:outputText>
												<h:outputText 
													id="textA8" value="#{help.GA10}" escape="false"></h:outputText>
												<hx:outputLinkEx styleClass="outputLinkEx2"
													value="#topOfPage" id="linkExBTP8">
													<h:outputText id="textBTP8" styleClass="outputText"
														value="Back to Top"></h:outputText>
												</hx:outputLinkEx>
												
												<h:outputText styleClass="outputText_help_question"
													id="textQ9" value="#{help.GQ11}"></h:outputText>
												<h:outputText 
													id="textA9" value="#{help.GA11}" escape="false"></h:outputText>
												<hx:outputLinkEx styleClass="outputLinkEx2"
													value="#topOfPage" id="linkExBTP9">
													<h:outputText id="textBTP9" styleClass="outputText"
														value="Back to Top"></h:outputText>
												</hx:outputLinkEx>

												<h:outputText styleClass="outputText_help_question"
													id="textQ10" value="#{help.GQ13}"></h:outputText>
												<h:outputText 
													id="textA10" value="#{help.GA13}" escape="false"></h:outputText>
												<hx:outputLinkEx styleClass="outputLinkEx2"
													value="#topOfPage" id="linkExBTP10">
													<h:outputText id="textBTP10" styleClass="outputText"
														value="Back to Top"></h:outputText>
												</hx:outputLinkEx>

											</hx:panelBox>
											</TD>
										</TR>
									</TBODY>
								</TABLE></hx:scriptCollector><%-- /tpl:put --%></TD>
				</TR>
				<TR>
					<TD align="right"><hx:outputLinkEx styleClass="outputLinkEx"
						value="javascript:void(0);return false;"
						id="linkExCloseWindowBottom" onclick="return func_1(this, event);">
						<h:outputText id="textCloseWindowBottom" styleClass="outputText"
							value="Close Window"></h:outputText>
					</hx:outputLinkEx></TD>
				</TR>
				<TR>
					<TD style='background-image: url("/scweb/theme/images/800x20_art2.jpg"); background-repeat: repeat-x'>&nbsp;</TD>
				</TR>
			</TBODY>
		</TABLE>
	</hx:scriptCollector></BODY>
</f:view>
</HTML>
<%-- /tpl:insert --%>
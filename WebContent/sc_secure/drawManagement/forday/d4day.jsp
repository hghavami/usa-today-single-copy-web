<!DOCTYPE HTML>
<jsp:useBean id="RDLForDay"
	class="com.usatoday.champion.handlers.RDLForDayHandler" scope="session"></jsp:useBean>
<jsp:useBean id="globalMessage"
	class="com.usatoday.champion.handlers.MessageHandler" scope="application"></jsp:useBean>
<jsp:useBean id="omnitureTracking" class="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode" scope="application"/>


<%@page import="com.usatoday.champion.handlers.ProductOrderHandler"%><HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<META http-equiv="Content-Style-Type" content="text/css">
	<LINK href="/scweb/theme/Master.css" rel="stylesheet" type="text/css">
	<LINK rel="stylesheet" type="text/css"	href="/scweb/theme/stylesheet.css" title="Style">
	<LINK rel="stylesheet" type="text/css"	href="/scweb/theme/tabpanel.css" title="Style">
	<LINK href="/scweb/theme/C_master_blue.css" rel="stylesheet" type="text/css">
	<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
	<TITLE>USA TODAY - Daily Draw Management</TITLE>
	<%=omnitureTracking.getTrackingCodePart1() %>
	<%
		int locationSize = ((com.usatoday.champion.handlers.RDLForDayHandler)request.getSession().getAttribute("RDLForDay")).getLocations().size();
		
		com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyDrawManagementIntf ddm = RDLForDay.getDdm();

		org.joda.time.DateTime rdlDate = ddm.getRDLDate();
		
		org.joda.time.format.DateTimeFormatter fmt = new org.joda.time.format.DateTimeFormatterBuilder()
            .appendMonthOfYearShortText()
            .appendLiteral(' ')
            .appendDayOfMonth(2)
            .appendLiteral(',')
            .appendLiteral(' ')
            .appendYear(4,4)
            .toFormatter();
            
        String rdlDateString = fmt.print(rdlDate);
        
        String globalMessageStyle = "display:none";
        String globalMessageText = "";
        if (globalMessage.getShowMessage().booleanValue() && globalMessage.showToContractors()) {
        	globalMessageStyle = "display: absolute";
        	globalMessageText = globalMessage.getMessage() == null ? "" : globalMessage.getMessage();
        }
	%>
<SCRIPT src="/scweb/scripts/browserVersion.js"  type="text/javascript"></SCRIPT>
<script src="/scweb/scripts/scriptaculous/prototype.js" type="text/javascript"></script>
<script src="/scweb/scripts/scriptaculous/scriptaculous.js?load=effects,dragdrop" type="text/javascript"></script>
<script src="/scweb/scripts/commonValidations.js"></script>
<script src="/scweb/scripts/drawmngmt.js"></script>
<script src="/scweb/scripts/windowutility.js"></script>
<script src="/scweb/scripts/keepalive.js"></script>

<SCRIPT type="text/javascript">
// change Omniture Default Page name
s.pageName = "SC Daily Draw Edit Entries";
s.channel = "Contractor Route Management";

var originalWindowHeight = 689;
var origingalDivHeight = 512;

Event.observe(window, 'load', function() {

	$('formManageDraw:buttonSubmitDrawChanges').blur();
	
	var inputArray = $$('input.dmEditField');
	textInputFields = inputArray;	
	var firstFound = false;
	var focusIndex = -1;
	for (i = 0; i < inputArray.length; i++) {
		if (firstFound == false && inputArray[i].hasClassName('returnInputText')) {
			focusIndex = i;
			firstFound = true;
		}
		Event.observe(inputArray[i], 'keydown', function(event) {
			if(aKeyPressed(event) == false) {
				if( typeof window.stop == "function" && ((BrowserDetect.browser == 'Explorer')==false)) {
					Event.stop(event);
				}
			}
		});			
	}
	if (focusIndex > -1) {
		inputArray[focusIndex].focus();
		inputArray[focusIndex].select();
		lastSelectedElement = inputArray[focusIndex];
	}

	// this call turns off any open draw fields, it MUST be called after the above logic and not before.
	var drawCheckBox = $('disableEditableDrawFields');
	drawCheckBox.checked = false;
	var returnCheckBox = $('disableEditableReturnFields');
	returnCheckBox.checked = true;
	func_toggleDrawFields(drawCheckBox, null);	
	func_toggleReturnFields(returnCheckBox, null);
	
	confirmParentOpenDaily();
	
	// start countdown
	updateCountdown();
		
	Event.observe('formManageDraw:buttonSubmitDrawChanges', 'keydown', function(event) {
		// never allow enter key to submit form
		if (event.keyCode == Event.KEY_RETURN) {
			return false;
		}
	}); 

	Event.observe(window, 'resize', function(event) {
		
		// only resize if greater than 745
		var poDiv = $('formManageDraw:tabbedPanelDailyDrawTabbedPanel:bfpanelMainTabbedPanelDailyDraw');
		var currentWindowHeight = originalWindowHeight;
		if (parseInt(navigator.appVersion)>3) {
		 if (navigator.appName=="Netscape") {
		  currentWindowHeight = window.innerHeight;
		 }
		 if (navigator.appName.indexOf("Microsoft")!=-1) {
		  currentWindowHeight = document.body.offsetHeight;
		 }
		}
		
		if (currentWindowHeight < originalWindowHeight) {
			poDiv.style.height = origingalDivHeight + 'px';
			return;
		}
		
		var height = poDiv.getHeight();
		
		var heightDif =  currentWindowHeight - originalWindowHeight;
		if (heightDif > 0) {
			var newDivHeight = origingalDivHeight + heightDif;
			poDiv.style.height = newDivHeight + 'px';
		}
		
	});
	
	startKeepAlive();
	
	$('StatusMessageField').innerHTML = '&nbsp;';	
	
	// set upinitial window size
	if (parseInt(navigator.appVersion)>3) {
	 if (navigator.appName=="Netscape") {
	  originalWindowHeight = window.innerHeight;
	 }
	 if (navigator.appName.indexOf("Microsoft")!=-1) {
	  originalWindowHeight = document.body.offsetHeight;
	 }
	}
		
});

// -->
</SCRIPT>
</HEAD>
<BODY onbeforeunload="confirmUnload(this, event)" onunload="enableCheckBoxes();">

<TABLE width="677" border="0" cellpadding="0" cellspacing="0" style="margin-left: 5px">
	<TBODY>
		<TR valign="top">
			<TD colspan="4">
			<div id="globalMessageDivArea" style="<%=globalMessageStyle %>" class="globalMessageDivStyle globalMessageDivStyleNoTemplate globalMessageDivStyleHiddenNoTemplate"><%=globalMessageText %></div>
			</TD>
		</TR>		
		<TR>
			<TD nowrap="nowrap" colspan="3"><span class="outputText"
				style="color: black; font-size: 12pt; font-weight: bold;text-align: center">Draw
			Management for RDL Date: </span> <span class="outputText"
				style="font-size: 16px;font-weight: bold;"><%=rdlDateString %></span> <!-- old help --></TD>
			<TD nowrap="nowrap" align="right">
				<span id="formManageDraw:textNumLocationHeader" class="outputText_Med" style="color: black">Number Of Locations On Route: </span><SPAN class="outputText_Med" style=""><%=locationSize %>&nbsp;&nbsp;</SPAN>
			</TD>
		</TR>
		<TR>
			<TD colspan="4">
				<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
				  <TBODY>
					<TR>
						<TD align="left" nowrap="nowrap">
							<span class="outputText" style="color: black; font-size: 12px; font-weight: bold">District: </span>
							<span class="outputText_Med"><%=ddm.getRoute().getDistrictID() %></span>
							<span class="outputText" style="color: black; font-size: 12px; font-weight: bold">Route: </span>
							<span class="outputText_Med"><%=ddm.getRoute().getRouteID() %></span>
							<span class="outputText" style="color: black; font-size: 12px; font-weight: bold">Description: </span>
							<span class="outputText_Med"><%=ddm.getRoute().getRouteDescription() %></span>
						</TD>
						<TD align="right">
							<%=omnitureTracking.getTrackingCodePart2() %>
						</TD>
					</TR>
					<TR>
						<TD align="left" colspan="2"></TD>
					</TR>
				</TBODY>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD colspan="4">
				<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
				  <TBODY>
				  	<tr>
				  		<td colspan="2" align="left">
				  			<DIV id="countdownMSG" class="outputTextAlt" align="center">Note: You have about 80 minutes to submit your changes before your web session will expire due to inactivity.</DIV>
				  		</td>
				  	</tr>
				  	<tr>
				  		<td colspan="2" align="left">
				  			<span class="message"><%=RDLForDay.getSaveFailedMessage() %></span>
				  		</td>
				  	</tr>
					<TR>
						<td>
							<IMG border="0" src="/scweb/theme/1x1.gif" width="1"
							height="1" vspace="3"> 
							<DIV id="noEditsDiv" style="<%=RDLForDay.getEdittableFieldsCSS() %>">
								<span class="message">VIEW ONLY NOTICE: There are no  draw or return fields open for edit  on this date.</span>
							</DIV>
						</td>
						<td nowrap="nowrap" align="right" valign="bottom">
							<DIV id="StatusMessageField" class="outputText_LG">Please Wait...<IMG
							border="0" src="/scweb/images/squaresAnimated.gif" width="80"
							height="10"></DIV>
						</td>
					</TR>
				  </tbody>
				</table>					
			</TD>		
		</TR>
		<TR>
			<TD colspan="4" align="left" valign="bottom" nowrap="nowrap">
			
			<TABLE width="100%" border="0" cellpadding="0" cellspacing="1"
				bgcolor="#d8d8c8">
				<TBODY>
					<TR bgcolor="#d8d8c8">
						<TD nowrap="nowrap" width="230">
							&nbsp;&nbsp;<span class="outputText"
							style="color: black; font-size: 12px; font-weight: bold">Location
						Information</span>
						</TD>
						<TD nowrap="nowrap">
							<SPAN style="font-weight: bold;font-size: 12px;color: white"><span
							id="formManageDraw:textPOInfo" class="outputText"
							style="color: black">  &nbsp;Product Order Information</span></SPAN></TD>
						<TD align="right">
						<TABLE border="0" cellpadding="0" cellspacing="0">
							<TBODY>
								<TR>
									<TD align="left" nowrap="nowrap">
									   <INPUT type="checkbox"
													name="disableEditableDrawFields"
													id="disableEditableDrawFields" value="disableDrawFields"
													onclick="return func_toggleDrawFields(this, event);">
												<SPAN
													style="font-weight: bold;font-size: 11px;color: black;">Edit Draw</SPAN>
										<INPUT type="checkbox"
													name="disableEditableReturnFields"
													id="disableEditableReturnFields" value="disableReturnFields"
													onclick="return func_toggleReturnFields(this, event);" checked="checked">
												<SPAN
													style="font-weight: bold;font-size: 11px;color: black;">Edit Returns&nbsp;&nbsp;&nbsp;</SPAN>
									</TD>
									<TD align="right" valign="middle">
										<a	href="javascript:void(0);"
										onclick="javascript: helpScreen('/scweb/sc_secure/drawManagement/forday/dailyHelpFile.usat'); if (helpWindow) {helpWindow.focus();} return false;"
										title="Daily Draw Entry Help"
										onmouseover="window.status='Click for Help On This Page'"
										onmouseout="window.status=' '"><IMG border="0"
										src="/scweb/images/question_mark.gif" width="16" height="16"
										hspace="10"></a>									
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						</TD>
					</TR>
				</TBODY>
			</TABLE>
			</TD>
		</TR>
	</TBODY>
</TABLE>
<!-- Tables split to enhance page loading/browser rendering -->
<TABLE  width="677" border="0" cellpadding="0" cellspacing="0" style="margin-left: 5px">
	<TBODY>
		<TR>
			<TD align="left" valign="top" nowrap="nowrap" colspan="4">
			<form id="formManageDraw" name="formManageDraw" action="/scweb/sc_secure/drawManagement/forDay/submitDailyDrawEntries" method="post">

			<!-- Outer table -->
			<TABLE class="tabbedPanel" width="675" cellspacing="0" cellpadding="0" border="0">
				<TBODY>
					<TR>
						<TD class="tabbedPanel-Body" width="100%" valign="top" height="100%">
						
							<div id="formManageDraw:tabbedPanelDailyDrawTabbedPanel:bfpanelMainTabbedPanelDailyDraw" class="tabbedPanel_DIV" style="overflow: auto; visibility: visible; width: 667px; height: 512px;">
								<table id="formManageDraw:tableDailyDrawDataTable" class="dataTable" width="635" cellspacing="0" cellpadding="2" border="0">
									<tbody>
									<%
										int drawTabOrder = 1;
										int returnTabOrder = drawTabOrder+1;
										java.util.Collection<com.usatoday.champion.handlers.LocationHandler> locations = RDLForDay.getLocations();
										java.util.Iterator<com.usatoday.champion.handlers.LocationHandler> lItr = locations.iterator();
										boolean isOddRow = true;
										int i = 0;
										while (lItr.hasNext()) {
											com.usatoday.champion.handlers.LocationHandler dloc = lItr.next();
											String locId = dloc.getLocationID();
											String locName = dloc.getLocationName();
											String locNameNoApostrophes = locName.replaceAll("'","");
											locNameNoApostrophes = locNameNoApostrophes.replaceAll("\"","");
											String locAddr1 = dloc.getLocationAddress1();
											String locPhone = dloc.getLocationPhoneNumberFormatted();
											String locHashKey = dloc.getDdl().getLocationHashKey();
											String rowClassName = null;
											if (isOddRow) {
												rowClassName = "rowClass1";
												isOddRow = false;
											}
											else {
												rowClassName = "rowClass3";
												isOddRow = true;
											}
											
											java.util.Iterator<ProductOrderHandler> poItr = dloc.getProductOrders().iterator();
									 %>
										<tr class="<%=rowClassName %>">
											<td class="columnClass1" width="225" valign="top" align="left">
												<!-- location column -->
												<table width="223" cellspacing="1" cellpadding="0" border="0">
													<tbody>
														<tr>
															<td>
																<span class="outputText"><%=locId %></span>
															</td>
														</tr>
														<tr>
															<td>
																<span class="outputText"><%=locName %></span>
															</td>
														</tr>
														<tr>
															<td>
																<span class="outputText"><%=locAddr1 %></span>
															</td>
														</tr>
														<tr>
															<td>
																<span class="outputText"><%=locPhone %></span>
															</td>
														</tr>
													</tbody>
												</table>
												<!-- end location column -->																													
											</td>
											<td class="columnClass1" width="450" valign="top" align="left">
												<!-- inner data table -->
												<table id="formManageDraw:tableDailyDrawDataTable:<%=i %>:tableInsideProductOrderDataTable" class="dataTable" cellspacing="0" cellpadding="2" border="1">
													<thead>
													<tr>
														<th scope="col" class="headerClass"><span class="outputText">ID</span>
														</th>
														<th scope="col" class="headerClass"><span class="outputText">Type</span>
														</th>
														<th scope="col" class="headerClass"><span class="outputText">Pub ID</span>
														</th>
														<th scope="col" class="headerClass" align="center"><span class="outputText">&nbsp;&nbsp;Draw&nbsp;&nbsp;</span>
														</th>
														<th scope="col" class="headerClass" align="center" nowrap="nowrap"><span class="outputText">&nbsp;Returns Date</span></th>
														<th scope="col" class="headerClass" align="center"><span class="outputText">Returns</span>
														</th>
													</tr>
													</thead>												
													<tbody>

													<%
														boolean innerRowIsOdd = true;
														while (poItr.hasNext()) {
															com.usatoday.champion.handlers.ProductOrderHandler po = poItr.next();
															String poID = po.getProductOrderID();
															String innerRowClass = null;
															if (innerRowIsOdd) {
																innerRowClass = "rowClass1";
																innerRowIsOdd = false;
															}
															else {
																innerRowClass = "rowClass2";
																innerRowIsOdd = true;
															}
															
													 %>
														<tr class="<%=innerRowClass %>">
															<td class="columnClass1" valign="middle" align="center">
																<span class="outputText"><%=po.getProductOrderID() %></span>
															</td>
															<td class="columnClass1" valign="middle" align="center">
																<a id="formManageDraw:tableWeeklyDrawDataTable:poDescLink<%=locHashKey %>:<%=po.getProductOrderID() %>" class="outputLinkExNoUnderline" onclick="return false;" title="<%=po.getProductOrderTypeDescription() %>" tabindex="0" href="javascript:void(0);"
																  onfocus="autoTab(this, event)">
																	<span class="outputText"><%=po.getProductOrderType() %></span>
																</a>
															</td>
															<td align="center" class="columnClass1">
																<a class="outputLinkExNoUnderline" onclick="return false;" title="<%=po.getProductDescription() %>" tabindex="0" href="javascript:void(0);" 
																   onfocus="autoTab(this, event)">
																<span class="outputText"><%=po.getProductID() %></span>
																</a>
															</td>
															<td class="columnClass1" width="70" nowrap align="center">
																<table width="70">
																	<tr>
																		<td align="center">
																			<input type="text" size="3" class="<%=po.getDrawCSS() %>" <%=po.getDrawReadOnly() %> value="<%=po.getDrawDisplayValue() %>" id="<%=locHashKey %>:<%=poID %>:DrawValue" name="<%=locHashKey %>:<%=poID %>:DrawValue" onfocus="lastSelectedElement=this; focusGainedDaily(this, event);" title="Original Draw Value: <%=po.getOriginalDrawDisplayValue() %> <%=po.getErrorMessage() %>"
																			    onkeypress="$(this).removeClassName('inputText_Error');"  tabindex="<%=drawTabOrder %>" onblur="return drawValueChangedDaily(this, event, <%=po.getMaxDrawValue() %>, <%=po.getDrawValue() %>, '<%=locHashKey %>:<%=poID %>:ZeroDrawReasonCode', '<%=po.getZeroDrawReasonCode() %>', '<%=locHashKey %>:<%=poID %>:ZDRIndicator', '<%=locHashKey %>:<%=poID %>:zdrIndDiv', '<%=locId %>', '<%=locNameNoApostrophes  %>', '<%=poID %>', '<%=po.getProductID() %>', <%=po.getDrawDayReturns() %>);"/>
																			<input type="hidden" name="<%=locHashKey %>:<%=poID %>:ZeroDrawReasonCode" id="<%=locHashKey %>:<%=poID %>:ZeroDrawReasonCode"  value="<%=po.getZeroDrawReasonCode() %>"/>
																		</td>
																		<td width="18">
																			<div id="<%=locHashKey %>:<%=poID %>:zdrIndDiv" style="<%=po.getZDRCSSString() %>" >
																				<img width="18" height="14" class="graphicImageEx" id="<%=locHashKey %>:<%=poID %>:ZDRIndicator" title="<%=po.getZDRImageTitle() %>" style="margin-left: 2px;" 
																				   onclick="showZRDPanel(this, '<%=locHashKey %>:<%=poID %>:ZeroDrawReasonCode', '<%=locId %>', '<%=locNameNoApostrophes %>', '<%=poID %>', '<%=po.getProductID() %>', null, '<%=po.getDrawFieldOpen() %>', '<%=po.getOriginalZeroDrawReasonCode() %>', '<%=po.getOriginalDrawDisplayValue() %>');" src="<%=po.getZDRImageIcon() %>" />
																			</div>
																		</td>
																	</tr>
																</table>
															</td>
															<td align="center" class="columnClass1" width="100">
																<table>
																<tbody>
																	<tr>
																		<td>
																			<img hspace="3" class="graphicImageEx" src="/scweb/theme/1x1.gif"/>
																		</td>
																		<td align="right">
																			<span class="outputText"><%=po.getReturnDate2Str() %></span>
																		</td>
																		<td>
																			<img width="16" height="16" class="graphicImageEx" title="NOTICE: Late Returns Day, you have already been invoiced for this day." style="<%=po.getLateReturnsCSSString() %>" onclick="alert('NOTICE: Late Returns Day, you have already been invoiced for this day.');" src="/scweb/images/imageTextBorder.gif"/>
																		</td>
																	</tr>
																</tbody>
																</table>
															</td>															
															<td class="columnClass1">
																<input type="text" size="3" class="<%=po.getReturnCSS() %>"  <%=po.getReturnReadOnly() %> value="<%=po.getReturnsDisplayValue() %>" name="<%=locHashKey %>:<%=poID %>:ReturnValue" id="<%=locHashKey %>:<%=poID %>:ReturnValue" title="Original Return Value: <%=po.getOriginalReturnsDisplayValue() %> <%=po.getErrorMessage() %>"
																    onkeypress="$(this).removeClassName('inputText_Error');" onfocus="lastSelectedElement=this; focusGainedDaily(this, event);" tabindex="<%=returnTabOrder %>" onblur="return returnValueChangedDaily(this, event, <%=po.getMaxReturnValue() %>);"/>
															</td>
														</tr>

															
													<% 
															drawTabOrder +=2;
															returnTabOrder = drawTabOrder + 1;
														} // end while more product orders
													%>
													</tbody>																															
												</table>
												<!-- end inner data table -->
											</td>
										</tr>
										<%		
												i++;
											} // end while more locations
										%>															
									</tbody>
								</table>
								</div>
							</td>
						</tr>
				</TBODY>
			</TABLE>
			<!-- End Outer Table (locations) -->


			<table cellspacing="5" cellpadding="2" border="0" width="100%">
				<tbody>
					<tr>
						<td align="right" nowrap="nowrap">
							<input id="formManageDraw:buttonSubmitDrawChanges" class="commandExButton" type="submit" onmouseover="this.focus();"  onclick="return validateDrawMngmtForm(this, event);" name="formManageDraw:buttonSubmitDrawChanges" value="Submit Draw And Returns Entries" tabindex="9991"/>
						</td>
						<td align="center" nowrap="nowrap">
							<input id="formManageDraw:buttonResetAllValues" class="commandExButton" type="reset" name="formManageDraw:buttonResetAllValues" value="Reset Form" " style="margin-left: 30;margin-right: 30"  onclick="return confirmDrawReset(this, event);" tabindex="9992"/>
						</td>
						<td align="left" nowrap="nowrap">
							<input id="formManageDraw:buttonQuitNoSave" class="commandExButton" type="submit" name="formManageDraw:buttonSubmitDrawChanges" value="Quit Without Saving" tabindex="9993"/>
						</td>
					</tr>
				</tbody>
			</table>
			</form> 
			<!-- end draw management form -->
			</TD>
		</TR>
		<TR>
			<TD></TD>
			<TD></TD>
			<TD></TD>
			<TD></TD>
		</TR>
		<!-- Zero Draw Chooser -->
		<TR>
			<TD>
			<DIV id="ZeroDrawReasonChooser" class="box1" align="center" 
				style="display: none;z-index:1000;border-bottom: 0px solid #90a0b8;border-top: 0px solid #90a0b8;border-left: 0px solid #90a0b8;border-right: 0px solid #90a0b8;width: 400px;background: white;position:absolute;; top: 250px; left: 200px; height: 200px; background-color: white;">
			<TABLE id="ZDRTableHolder" class="ZeroDrawTable" border="0"
				cellpadding="2" cellspacing="1" bgcolor="white" style="z-index:1001" >
				<TBODY>
					<tr bgcolor="#90a0b8">
						<td colspan="2" align="left"><SPAN
							style="font-size: 14px;color: white;font-weight: bold"> Select a
						Zero Draw Reason:</SPAN></td>
					</tr>
					<TR>
						<td></td>
						<td>
						<table cellspacing="1" cellpadding="0" border="0">
							<tbody>
								<tr>
									<td nowrap="nowrap" align="right">Location ID:</td>
									<TD nowrap="nowrap"><INPUT class="inputText_disabledV2" type="text"
										value="" id="tempLocID" name="tempLocID" readonly="readonly"></TD>
								</tr>
								<tr>
									<td nowrap="nowrap" align="right">Location Name:</td>
									<TD>
									<div id="tempLocName"></div>
									</TD>
								</tr>
								<tr>
									<td nowrap="nowrap" align="right">POID:</td>
									<TD nowrap="nowrap"><INPUT class="inputText_disabledV2" type="text"
										value="" id="tempPOID" name="tempPOID" readonly="readonly"></TD>
								</tr>
								<tr>
									<td nowrap="nowrap" align="right">Pub:</td>
									<TD nowrap="nowrap"><INPUT class="inputText_disabledV2" type="text"
										value="" id="tempPubCode" name="tempPubCode" readonly="readonly"></TD>
								</tr>
							</tbody>
						</table>
						</td>
					</TR>
					<TR>
						<td></td>
						<td>
						<hr />
						</td>
					</TR>
					<TR>
						<TD></TD>
						<TD><SPAN style="font-size: 11px;color: #90a0b8;font-weight: bold">
						Choose the best reason from the list below:</SPAN></TD>
					</TR>
					<TR>
						<TD></TD>
						<TD><SELECT size="1" name="ZeroDrawReasonSelector"
							id="ZeroDrawReasonSelector" class="selectOneListbox">
							<OPTION value=""></OPTION>
							<%
								com.usatoday.singlecopy.model.util.ZeroDrawReasonCodeCache rc = com.usatoday.singlecopy.model.util.ZeroDrawReasonCodeCache.getInstance();
								java.util.Iterator<com.usatoday.singlecopy.model.interfaces.drawmngmt.ZeroDrawReasonCodeIntf> itr =  rc.getZeroDrawReasonCodes().iterator();
								while (itr.hasNext()) {
									com.usatoday.singlecopy.model.interfaces.drawmngmt.ZeroDrawReasonCodeIntf zdr =  itr.next();
									
							 %>
							<OPTION value="<%=zdr.getCode()%>"><%=zdr.getDescription() %></OPTION>
							<%
								}
							 %>
						</SELECT></TD>
					</TR>
					<TR>
						<TD></TD>
						<TD><input type="hidden" id="zdrFieldID" value="" /><INPUT
							type="button" name="ZDRFinished" value="Set Zero Draw Reason"
							onclick="return zeroDrawValueSelected(this, event);"> &nbsp;<INPUT
							type="button" id="ZDRCodeCancel" name="ZDRCodeCancel"
							style="display: none" value="Cancel"
							onclick="return zeroDrawValueCanceled(this, event);"></TD>
					</TR>
					<TR>
						<TD></TD>
						<TD></TD>
					</TR>
				</TBODY>
			</TABLE>
			</DIV>
			</TD>
			<TD></TD>
			<TD></TD>
			<TD></TD>
		</TR>
		
	</TBODY>
</TABLE>

</BODY>
</HTML>

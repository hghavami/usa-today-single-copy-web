<!DOCTYPE HTML>

<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%
	String date = request.getParameter("d");
	String rteid = request.getParameter("rteid");
 %>

<%@page import="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode"%><html:html>
<f:view locale="#{userLocaleHandler.locale}">
<f:loadBundle basename="resourcesFile" var="labels" />
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	OmnitureTrackingCode otc = (OmnitureTrackingCode)session.getAttribute("omnitureTracking");
	if (otc == null) {
		otc = new OmnitureTrackingCode();
	}
	String omnitureP1 = otc.getTrackingCodePart1();
 %>
 
 <%=omnitureP1 %>
	
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="/scweb/theme/Master.css" rel="stylesheet" type="text/css">
<LINK href="/scweb/theme/C_master_blue.css" rel="stylesheet"
	type="text/css">
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet"
	type="text/css">
<LINK href="/scweb/theme/stylesheet.css" rel="stylesheet"
	type="text/css">
<TITLE></TITLE>
<SCRIPT type="text/javascript">
// change Omniture Default Page name
s.pageName = "SC Load Daily Draw Data";
s.channel = "Contractor Route Management";

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	try {
    	window.moveTo(5, 15);
		window.resizeTo(725, 750);
	}
	catch (e) {
		// ignore
	}
	loadDailyData();
	setTimeout('doCheckforward()', 500);
}

var doneLoadingData = false;
var dataExists = false;

function doCheckforward() {
	if (doneLoadingData == true) {
		document.RDLForDayLoaderBean.submit();
	}
	else {
		setTimeout('doCheckforward()', 300);
	}
}
function initRequest(url) {
       if (window.XMLHttpRequest) {
           return new XMLHttpRequest();
       } else if (window.ActiveXObject) {
           isIE = true;
           return new ActiveXObject("Microsoft.XMLHTTP");
       }
}

function loadDailyData() {
	var url = "/scweb/sc_secure/loadRDLForDay.do?d=<%=date%>&rte=<%=rteid %>";
    var req = initRequest(url);
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
           if (req.status == 200) {
                parseMessages(req.responseXML);
           } 
           else if (req.status == 204){
               return;
           }
        }
        };
     req.open("GET", url, true);
     req.send(null);
}

function parseMessages(respMessage) {
	// assume any response is a good response and set complete flag
	
	
		var response = respMessage.getElementsByTagName(
                   "DataLoaded")[0];
       //alert('response: ' + response.childNodes[0].nodeValue);
       if (response && response.childNodes[0].nodeValue.length > 0 && response.childNodes[0].nodeValue == "Y") {
			dataExists = true;	
			var hiddenField = document.getElementsByName('dataExists')[0];
			hiddenField.value = "Y";
			doneLoadingData = true;
       }
       else {
       		if (response && response.childNodes[0].nodeValue.length > 0 && response.childNodes[0].nodeValue == "N") {
				dataExists = false;	
				var hiddenField = document.getElementsByName('dataExists')[0];
				hiddenField.value = "N";
				doneLoadingData = true;
			}
			else {
				dataExists = false;	
				var hiddenField = document.getElementsByName('dataExists')[0];
				hiddenField.value = "X";
				doneLoadingData = true;
			}
       }     	
}

</SCRIPT>
	</HEAD>
<BODY onload="return func_1(this, event);"><hx:scriptCollector id="scriptCollector1">
<CENTER>
<br>
<br>
<TABLE width="95%" border="1" cellpadding="15" cellspacing="1" bgcolor="white" height="95%">
	<TBODY>
		<TR>
			<TD align="center" valign="middle"><SPAN style="color: navy; font-family: Arial; vertical-align: middle; font-size: 24px; text-align: center">${omnitureTracking.trackingCodePart2}<h:outputText
						styleClass="outputText" id="textPleaseWait" value="#{labels.pleaseWait}"></h:outputText> <BR><hx:graphicImageEx styleClass="graphicImageEx" id="imageEx1"
						value="/scweb/images/squaresAnimated.gif"></hx:graphicImageEx>
			<BR>
					<BR>
					<h:outputText styleClass="outputText" id="textLoading"
						value="#{labels.loadingDailyData}"></h:outputText> <%=date %>...<BR>
					<h:outputText styleClass="outputText" id="textDisclaimer"
						value="#{labels.loadDisclaimer}" style="font-size: 12px"></h:outputText>
					</SPAN>
					</TD>
		</TR>
	</TBODY>
</TABLE></CENTER>
<P><SPAN style="color: navy; font-family: Arial; vertical-align: middle; font-size: 24px; text-align: center"></SPAN></P>
<html:form action="/sc_secure/forwardRDLForDay">
	<html:hidden property="d" value="<%=date%>" />
	<html:hidden property="rte" value="<%=rteid%>" />
	<html:hidden property="dataExists" value="Y" />
</html:form>
</hx:scriptCollector></BODY>
</f:view>
</html:html>

<!DOCTYPE HTML>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/sc_secure/drawManagement/forday/Complete.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>

<%@page import="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode"%><HTML>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<f:view locale="#{userLocaleHandler.locale}">
<f:loadBundle basename="resourcesFile" var="labels" />
<HEAD>
<%
	OmnitureTrackingCode otc = (OmnitureTrackingCode)session.getAttribute("omnitureTracking");
	if (otc == null) {
		otc = new OmnitureTrackingCode();
	}
	String omnitureP1 = otc.getTrackingCodePart1();
 %>
<%=omnitureP1 %>
	
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="/scweb/theme/Master.css" rel="stylesheet" type="text/css">
<LINK rel="stylesheet" type="text/css"
	href="/scweb/theme/stylesheet.css" title="Style">
<LINK href="/scweb/theme/C_master_blue.css" rel="stylesheet"
	type="text/css">
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet"
	type="text/css">
<TITLE>Draw Entry Complete</TITLE>
<script src="/scweb/scripts/scriptaculous/prototype.js" type="text/javascript"></script>
<SCRIPT src="/scweb/scripts/windowutility.js"></SCRIPT>

<SCRIPT type="text/javascript">
// change Omniture Default Page name
s.pageName = "SC Daily Draw Save Entries Complete";
s.channel = "Contractor Route Management";

var printButton = null;
var oldLabel = "";

function printWinCallback() {
	printButton.value = oldLabel;
	$('StatusMessageField').innerHTML = '';
}

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
window.opener.focus();
window.close();
}

function func_2(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	printButton = thisObj;
	oldLabel = thisObj.value;
	
	thisObj.value = "Please Wait...";
	$('StatusMessageField').innerHTML = '<IMG border="0" src="/scweb/images/squaresAnimated.gif" width="80" height="10">';
	printWindow = window.open('/scweb/sc_secure/drawManagement/forday/printerFriendly.jsp', 'printWin', 'width=725,height=650,status=no,scrollbars=yes,resizable=yes,toolbar=yes');
	setTimeout("printWinCallback()", 7000);
	
	return false;
}
</SCRIPT></HEAD>
	<BODY><hx:scriptCollector id="scriptCollector1">
		<BR>${omnitureTracking.trackingCodePart2}
		<h:form styleClass="form" id="form1"><TABLE border="0" cellpadding="2" cellspacing="0" width="95%">
			<TBODY>
				<TR>
					<TD colspan="2" valign="top" align="center"><hx:outputSeparator styleClass="outputSeparator" id="separator1" color="#90a0b8" height="3"></hx:outputSeparator><BR>
					<FONT size="+1"> <h:outputText
							styleClass="outputText" id="textCompletetitle" value="#{labels.dmCompleteTitle}" style="font-weight: bold"></h:outputText></FONT> 
					</TD></TR>
				<TR>
					<TD align="center" valign="top" colspan="2"><h:messages styleClass="messages" id="messages1"></h:messages></TD>
				</TR>
				<TR>
					<TD colspan="2" align="center" valign="top">
					<TABLE>
						<TBODY>
								<TR>
									<TD align="center" colspan="3"><BR>
									</TD>
								</TR>
								<TR>
								<TD align="right"><B><h:outputText styleClass="outputText"
										id="textRouteLabel" value="#{labels.route}:"></h:outputText></B></TD>
								<TD style="width:5px">&nbsp;</TD>
								<TD><h:outputText id="text1" value="#{RDLForDay.ddm.route.routeID}"
										styleClass="outputText_Med">
									</h:outputText></TD>
							</TR>
							<TR>
								<TD align="right"><h:outputText styleClass="outputText"
										id="textRouteDesLabel" value="#{labels.routeDescColName}:"
										style="font-weight: bold"></h:outputText></TD>
								<TD style="width:5px">&nbsp;</TD>
								<TD><h:outputText id="text4"
										value="#{RDLForDay.ddm.route.routeDescription}"
										styleClass="outputText_Med">
									</h:outputText></TD>
							</TR>
							<TR>
								<TD align="right"></TD>
								<TD style="width:5px">&nbsp;</TD>
								<TD></TD>
							</TR>
							<TR>
									<TD align="center" colspan="3"></TD>
								</TR>
							<TR>
								<TD align="center" colspan="3"><hx:commandExButton type="reset"
										value="#{labels.dmCloseWindowLabel}"
										styleClass="commandExButton" id="button1"
										onclick="return func_1(this, event);"></hx:commandExButton>&nbsp;<hx:commandExButton
										type="reset" value="#{labels.printFriendlyButton}"
										styleClass="commandExButton" id="buttonOpenPrinterFriendly"
										onclick="return func_2(this, event);"
										title="#{labels.RDLPrintHoverMsg}"></hx:commandExButton></TD>
							</TR>
								<TR>
									<TD align="center" colspan="3">
									<DIV id="StatusMessageField" style="margin-top:10px"></DIV>
									</TD>
								</TR>
							</TBODY>
					</TABLE>
					</TD></TR>
				<TR>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colspan="2"><hx:outputSeparator styleClass="outputSeparator"
							id="separator2" color="#90a0b8" height="3"></hx:outputSeparator></TD></TR>
			</TBODY>
		</TABLE></h:form>
		<BR>
		<P></P>
	</hx:scriptCollector></BODY>
</f:view>
</HTML>

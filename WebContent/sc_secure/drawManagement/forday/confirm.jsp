<!DOCTYPE HTML>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/sc_secure/drawManagement/forday/Confirm.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>

<%@page import="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode"%><HTML>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<f:view locale="#{userLocaleHandler.locale}">
<f:loadBundle basename="resourcesFile" var="labels" />
	<HEAD>


<%
	OmnitureTrackingCode otc = (OmnitureTrackingCode)session.getAttribute("omnitureTracking");
	if (otc == null) {
		otc = new OmnitureTrackingCode();
	}
	String omnitureP1 = otc.getTrackingCodePart1();
 %>
<%=omnitureP1 %>
	

	<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<META http-equiv="Content-Style-Type" content="text/css">
	<LINK href="/scweb/theme/Master.css" rel="stylesheet" type="text/css">
	<LINK href="/scweb/theme/C_master_blue.css" rel="stylesheet"
		type="text/css">
	<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet"
		type="text/css">
	<TITLE>Daily Draw - Confirm Entries</TITLE>
	<LINK rel="stylesheet" type="text/css"
		href="/scweb/theme/stylesheet.css" title="Style">
	<SCRIPT src="/scweb/scripts/scriptaculous/prototype.js"
		type="text/javascript"></SCRIPT>
	<SCRIPT src="/scweb/scripts/windowutility.js"></SCRIPT>
	<SCRIPT type="text/javascript">
// change Omniture Default Page name
s.pageName = "SC Confirm Daily Draw Entries";
s.channel = "Contractor Route Management";

Event.observe(window, 'load', function() {

	Event.observe('formWeeklyMngmtConfirmation:buttonSaveChanges', 'keydown', function(anEvent) {
		if (anEvent.keyCode == Event.KEY_RETURN) {
			sClicked();
		}
	});
	
	Event.observe('formWeeklyMngmtConfirmation:buttonSaveChanges', 'click', function() {
		sClicked();
	});

	Event.observe('formWeeklyMngmtConfirmation:buttonBack', 'keydown', function(anEvent) {
		if (anEvent.keyCode == Event.KEY_RETURN) {
			sClicked();
			backToEditPage();
		}
	});
	
	Event.observe('formWeeklyMngmtConfirmation:buttonBack', 'click', function() {
		sClicked();
		backToEditPage();
	});
		
	confirmParentOpenDaily();
		
});
		

function backToEditPage() {

document.location = '/scweb/sc_secure/drawManagement/forday/d4day.jsp';
//history.go(-1);

}

var saveClicked = false;

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	if (!saveClicked) {
		saveClicked = true;
		thisObj.value = "Please Wait...";
		$('StatusMessageField').innerHTML = '<IMG border="0" src="/scweb/images/squaresAnimated.gif" width="80" height="10">';
		return true;
	}
	else {
		return false;
	}
}
</SCRIPT>

	</HEAD>
	<BODY onbeforeunload="confirmUnload(this, event)">
	<hx:scriptCollector id="scriptCollector1">
		<h:form styleClass="form" id="formWeeklyMngmtConfirmation">
			<CENTER>
			<TABLE border="0" cellpadding="0" cellspacing="0" width="95%">
				<TBODY>
					<TR>
						<TD colspan="4"><hx:outputSeparator styleClass="outputSeparator"
							id="separator1" color="#90a0b8" align="center" height="3"></hx:outputSeparator>${omnitureTracking.trackingCodePart2}</TD>
					</TR>
					<TR>
						<TD align="center" colspan="4"><BR>
						<FONT size="+1"><B></B></FONT><h:outputText
							styleClass="outputText" id="textConfirmTitle"
							value="#{labels.dmConfirmTitle}"
							style="color: black; font-size: 24px; font-weight: bold"></h:outputText></TD>
					</TR>
					<TR>
						<TD colspan="4" align="center"><BR>
						<BR>
						<h:outputText styleClass="outputText" id="textSaveWarning"
							value="#{labels.dmConfirmWarningText}"
							style="color: #e83840; font-size: 18px; font-weight: bold"></h:outputText></TD>
					</TR>
					<TR>
						<TD colspan="2"></TD>
						<TD></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD colspan="2"></TD>
						<TD></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD colspan="2"></TD>
						<TD></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD align="center" colspan="4">&nbsp;
						<TABLE border="0">
							<TBODY>
								<TR>
									<TD align="right"><B><h:outputText styleClass="outputText"
										id="textWeekEndingLabel" value="#{labels.dmRDLDateLabel}:"></h:outputText></B></TD>
									<TD></TD>
									<TD><h:outputText id="textHeaderWeekEndingDate"
										value="#{RDLForDay.day}" styleClass="outputText_Med">
										<f:convertDateTime />
									</h:outputText></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><B><h:outputText styleClass="outputText"
										id="textRouteLabel" value="#{labels.route}:"></h:outputText></B></TD>
									<TD style="width:5px">&nbsp;</TD>
									<TD><h:outputText id="text2"
										value="#{RDLForDay.ddm.route.routeID}"
										styleClass="outputText_Med">
									</h:outputText></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><B><h:outputText styleClass="outputText"
										id="textNumPOChangedLabel" value="#{labels.dmNumPOChanged}:"></h:outputText></B></TD>
									<TD style="width:5px"></TD>
									<TD><h:outputText styleClass="outputText_Med"
										id="textNumberChangedPOs"
										value="#{RDLForDay.ddm.numberOfChangedProductOrders}"></h:outputText></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="center" colspan="5">
									<TABLE border="1" cellpadding="2" cellspacing="1">
										<TBODY>
											<TR bgcolor="#dadfe7">
												<TH bgcolor="white"></TH>
												<TH width="75"><h:outputText styleClass="outputText"
													id="textDrawHeaderLabel" value="#{labels.dmDraw}"
													style="font-weight: bold"></h:outputText></TH>
												<TH width="75"><h:outputText styleClass="outputText"
													id="textReturnsHeaderLabel" value="#{labels.dmReturns}"
													style="font-weight: bold"></h:outputText></TH>
											</TR>
											<TR>
												<TH align="right"><h:outputText styleClass="outputText"
													id="textOriginalLabel" value="#{labels.dmOriginalVal}:"
													style="font-weight: bold"></h:outputText></TH>
												<TD align="center"><h:outputText styleClass="outputText"
													id="textOrigTotDraw" value="#{RDLForDay.originalTotalDraw}"
													style="color: black">
													<f:convertNumber />
												</h:outputText></TD>
												<TD align="center"><h:outputText styleClass="outputText"
													id="textOrigTotReturns"
													value="#{RDLForDay.originalTotalReturns}"
													style="color: black">
													<f:convertNumber />
												</h:outputText></TD>
											</TR>
											<TR bgcolor="#ebebeb">
												<TH align="right"><h:outputText styleClass="outputText"
													id="textNewValLabel" value="#{labels.dmNewVal}:"
													style="font-weight: bold"></h:outputText></TH>
												<TD align="center"><h:outputText id="text14"
													value="#{RDLForDay.totalDraw}" styleClass="outputText"
													style="color: navy; font-weight: bold">
													<f:convertNumber integerOnly="true" />
												</h:outputText></TD>
												<TD align="center"><h:outputText id="text15"
													value="#{RDLForDay.totalReturns}" styleClass="outputText"
													style="color: navy; font-weight: bold">
													<f:convertNumber integerOnly="true" />
												</h:outputText></TD>
											</TR>
										</TBODY>
									</TABLE>
									</TD>
								</TR>
								<TR>
									<TD align="right"><B></B></TD>
									<TD style="width:5px">&nbsp;</TD>
									<TD></TD>
									<TD></TD>
									<TD></TD>
								</TR>

							</TBODY>
						</TABLE>
						</TD>
					</TR>

					<TR>
						<TD align="center"><hx:commandExButton type="submit" onclick="return func_1(this,event)"
							value="#{labels.dmSaveEntries}" styleClass="commandExButton"
							id="buttonSaveChanges"
							action="#{pc_Confirm1.doButtonSaveChangesAction}"></hx:commandExButton>&nbsp;<hx:commandExButton
							type="reset" value="#{labels.dmBack}"
							styleClass="commandExButton" id="buttonBack" style="margin-left: 10px"></hx:commandExButton></TD>
						<TD style="width:5px"></TD>
						<TD></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD align="center">
							<DIV id="StatusMessageField" style="margin-top:10px"></DIV>
						</TD>
						<TD style="width:5px"></TD>
						<TD></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD colspan="4"><hx:outputSeparator styleClass="outputSeparator"
							id="separator2" color="#90a0b8" align="center" height="3"></hx:outputSeparator></TD>

					</TR>
					<TR>
						<TD></TD>
						<TD></TD>
						<TD></TD>
						<TD></TD>
					</TR>

				</TBODY>
			</TABLE>
			</CENTER>
		</h:form>
	</hx:scriptCollector>
	</BODY>
</f:view>
</HTML>

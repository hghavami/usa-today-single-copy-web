<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/sc_secure/Home.java" --%><%-- /jsf:pagecode --%>
	<%-- tpl:insert page="///theme/JSP-C-blue_mod.jtpl" --%><!DOCTYPE HTML>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%
	OmnitureTrackingCode otc = (OmnitureTrackingCode) session
			.getAttribute("omnitureTracking");
	if (otc == null) {
		otc = new OmnitureTrackingCode();
	}
	String omnitureP1 = otc.getTrackingCodePart1();

	// following work around due to bug in RAD
	String useMap = "#map_800x125_top";
	String headURL = "/scweb/index.usat";
%>
<!-- NEW path -->


<link rel="stylesheet" href="/scweb/theme/Master.css" type="text/css">
<link href="/scweb/theme/C_master_blue.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/scweb/theme/stylesheet.css"
	title="Style">

<script src="/scweb/scripts/windowutility.js"></script>
<script language="JavaScript">
	// method called on page load
	function _initPage() {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this

		if (typeof window.initPage == "function") {
			window.initPage();
		}

	}

	// method called on page unload
	function _unloadPage(thisObj, thisEvent) {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this
		if (typeof window.unloadPage == "function") {
			window.unloadPage(thisObj, thisEvent);
		}

	}

	// method called on page unload
	function _onbeforeunloadPage(thisObj, thisEvent) {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this
		if (typeof window.onbeforeunloadPage == "function") {
			window.onbeforeunloadPage(thisObj, thisEvent);
		}

	}
</script>

<%=omnitureP1%>

<%-- tpl:put name="headarea" --%>
<title>USA TODAY - Home Page</title>
<LINK rel="stylesheet" type="text/css" href="/scweb/theme/tree.css"	title="Style">
<LINK rel="stylesheet" type="text/css" href="/scweb/theme/tabpanel.css"	title="Style">
<script language="javascript">
// change Omniture Default Page name
s.pageName = "SC Authorized User Home";

function initPage() {
	
}

function func_9(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
helpScreen('/scweb/sc_secure/drawManagement/drawManagementHelp.usat'); 
if (helpWindow) {
	helpWindow.focus();
}
return false;
}
function func_10(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
window.status='Click for Help On This Page';

}
function func_11(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
window.status='';
}
</script>

		<%-- /tpl:put --%>
<script language="javascript">
	//var logoutMsg = 'Please close all Draw Management windows before logging out';
	function cleanupWindows() {
		// switch to:
		if (rdlWindow != null) {
			//if (rdlWindow.closed == false) {
			if (rdlWindow.open && !rdlWindow.closed) {
				rdlWindow.setForceClose();
				rdlWindow.close();
			}

		}

		if (weeklyRDLWindow != null) {
			if (weeklyRDLWindow.open && !weeklyRDLWindow.closed) {
				weeklyRDLWindow.setForceClose();
				weeklyRDLWindow.close();
			}
		}

		if (helpWindow != null) {
			if (helpWindow.open && !helpWindow.closed) {
				helpWindow.close();
			}
		}

		if (printWindow != null) {
			if (printWindow.open && !printWindow.closed) {
				printWindow.close();
			}
		}

		return true;
	}

	function okToLogout() {
		var alert = false;
		if (rdlWindow != null) {
			if (rdlWindow.open && !rdlWindow.closed) {
				alert = true;
			}
		}

		if (weeklyRDLWindow != null) {
			if (weeklyRDLWindow.open && !weeklyRDLWindow.closed) {
				alert = true;
			}
		}

		if (alert) {
			setTimeout(
					'alert("You must close all Draw Management windows before logging out.")',
					100);
			return false;
		} else {
			cleanupWindows();
			return true;
		}
	}

	function func_logoutOK(thisObj, thisEvent) {
		//use 'thisObj' to refer directly to this component instead of keyword 'this'
		//use 'thisEvent' to refer to the event generated instead of keyword 'event'
		return okToLogout();
	}
	function getCurrentYear() {
		today = new Date();
		y0 = today.getFullYear();
		return y0;
	}
</script>
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="charset=ISO-8859-1">
</head>
<f:view locale="#{userLocaleHandler.locale}">
	<f:loadBundle basename="resourcesFile" var="labels" />
	<body onLoad="_initPage();" onUnload="_unloadPage(this, event);"
		onbeforeunload="_onbeforeunloadPage(this, event);">
		<hx:scriptCollector id="templateScriptCollector">


			<!-- start header area -->
			<div class="topAreaBox1"></div>
			<div class="topAreaLogo">
				<!-- 				<img border="0" src="/scweb/theme/images/800x125_top.jpg" -->
				<%-- 					width="800" height="125" usemap="<%=useMap %>"> --%>
				<img border="0" src="/scweb/theme/images/USATODAY_Header_940x145.jpg"
					width="940" height="145" usemap="<%=useMap%>">
				<map name="map_800x125_top">
					<area shape="rect" target="_self" href="<%=headURL%>"
						coords="19,17,268,112">
					<area shape="default" nohref>
				</map>
			</div>
			<div class="topAreaLogoLine">
				<img border="0" src="/scweb/theme/images/800x20_art2.jpg" height="20"
					width="800">
			</div>
			<!-- end header area -->

			<!-- start header navigation bar -->
			<div class="topNavBk"></div>${omnitureTracking.trackingCodePart2}<div
				class="topNav"></div>
			<div class="topAreaDatePlacement">
				<fmt:formatDate value="${pc_JSPCblue_mod.now}" dateStyle="full" />
			</div>
			<h:outputText styleClass="globalMessageDivStyle" id="textGlobalMessageText"
				value="#{globalMessage.message}"
				rendered='#{ (globalMessage.showMessage) and ((globalMessage.showToWhom eq "all") or ((globalMessage.showToWhom eq "hotel") and userHandler.canWorkWithBlueChipDraw ) or ((globalMessage.showToWhom eq "contractor") and userHandler.canWorkWithRoutes) )}'></h:outputText>
			<!-- end header navigation bar bar -->

			<!-- start left-hand navigation -->
			<table class="mainBox" border="0" cellpadding="0" width="100%" height="87%"
				cellspacing="0">
				<tbody>
					<tr>
						<td class="leftNavTD" align="left" valign="top">
							<div class="leftNavBox">
								<h:form styleClass="form" id="formLogout">
									<%-- tpl:put name="LeftNavTopMostJSPContentArea" --%>
								
						<%-- /tpl:put --%>
									<div id="logoutLinkDiv" class="noDisplay" style="${userHandler.divCss}">
										<h:outputFormat styleClass="outputFormat" id="formatWelcomeMessage"
											value="Welcome, {0}">
											<f:param name="displayName" value="#{userHandler.displayName}"></f:param>
										</h:outputFormat>
										<br>
										<hx:commandExButton type="submit" value="#{labels.logoutLink}"
											styleClass="commandExButtonSmall" id="buttonDoLogoutBtn"
											title="Logout" alt="Logout"
											action="#{pc_JSPCblue_mod.doButtonDoLogoutBtnAction}"
											onclick="return func_logoutOK(this, event);"></hx:commandExButton>
										<hr width="95%" align="left">
									</div>
									<siteedit:navbar spec="/scweb/theme/nav_vertical_tree_left.jsp"
										targetlevel="1-5" onlychildren="true" navclass="leftNav"
										target="topchildren" />
									<br>
									<br>
									<br>
									<!-- If we add internatiolization
								<h:selectOneMenu styleClass="selectOneMenu" id="menui18nPref" value="#{userLocaleHandler.localeStr}">
									<f:selectItem itemValue="en" itemLabel="English" />
									<f:selectItem itemValue="es" itemLabel="Spanish" />
								</h:selectOneMenu><hx:commandExButton type="submit" value="Go"
									styleClass="commandExButton" id="buttonUpdateLangPref"
									action="#{pc_JSPCblue_mod.doButtonUpdateLangPrefAction}"></hx:commandExButton> -->
									<br>
									<hx:inputHelperKeybind key="Enter" id="inputHelperKeybind1"
										targetAction="nothing" />
								</h:form>
								<br>
								<hr width="95%" align="left">
								<b>Related Links:</b><br> <a href="http://usatoday.com"
									target="_blank">USA TODAY.com</a><br> <a
									href="https://service.usatoday.com/welcome.jsp" target="_blank">USA
									TODAY Home Delivery</a><br> <a
									href="https://service.usatoday.com/shop" target="_blank">USA TODAY
									Past Issues</a><br> <br> <br>

							</div>
						</td>
						<!-- end left-hand navigation -->
						<td><img border="0" src="/scweb/theme/img/JSF_1x1.gif" width="1"
							height="1" hspace="6">
						</td>
						<td class="mainContentWideTD" align="left" valign="top" rowspan="2">
							<!-- start main content area -->
							<div class="mainContentWideBox">
								<img border="0" src="/scweb/theme/images/USATODAY_Subhead_740x76.jpg"
									width="740" height="76" usemap="<%=useMap%>">
								<!-- bread crumbs if we want em-->
								<div class="navTrailLoc">
									<siteedit:navtrail start="[" end="]" target="home,parent,ancestor,self"
										separator="&gt;&gt;" spec="/scweb/theme/trail.jsp" />
								</div>
								<a name="navskip"><img border="0" src="/scweb/theme/img/JSF_1x1.gif"
									width="1" height="1" alt="Beginning of page content"> </a>
								<%-- tpl:put name="jspbodyarea" --%>
								<hx:scriptCollector id="scriptCollectorHomePageCollector">
									<h:form styleClass="form" id="formHomePageMainForm">

									<DIV class="mainContentHelpIcon" style="display: none"><hx:graphicImageEx
										styleClass="graphicImageEx"
										style="cursor: pointer; margin-bottom: 2px; margin-left: 5px; margin-right: 5px; margin-top: 2px"
										id="imageExHelpIcon1" value="/scweb/images/question_mark.gif"
										width="16" height="16" hspace="0" title="Draw Management Help"
										onclick="return func_9(this, event);"
										onmouseover="return func_10(this, event);"
										onmouseout="return func_11(this, event);"></hx:graphicImageEx>
									</DIV>


								<hx:jspPanel id="jspPanelBlueChipPanel" rendered="#{userHandler.canWorkWithBlueChipDraw}">
								
										<DIV id="bcdHomePageDiv" style="margin-bottom: 10px;">
										<TABLE border="0" cellpadding="0" cellspacing="0" width="600">
											<TBODY>
												<TR>
													<TD align="center" nowrap="nowrap" bgcolor="#7D95B3" width="250"><h:outputText
														styleClass="outputText" id="textBlueChipDrawText"
														value="My Hotel"
														style="color: white; font-size: 12px; font-weight: bold"></h:outputText></TD>
													<TD bgcolor="white" width="350">&nbsp;</TD>
												</TR>
												<tr>
													<TD colspan="2">
													<TABLE class="borderedTable" border="0" cellpadding="0"
														cellspacing="0" width="100%">
														<TBODY>
															<tr>
																<td colspan="2" valign="middle" nowrap="nowrap">&nbsp;&nbsp;&nbsp;<h:outputText
																	styleClass="outputText" id="textBlueChipHomePageTxt"
																	value="#{labels.bcdHomePageText}"></h:outputText> <hx:outputLinkEx
																	styleClass="outputLinkEx"
																	value="/scweb/sc_secure/bcd/distributionDayDraw.usat"
																	id="linkEx1"
																	rendered="#{userHandler.canWorkWithBlueChipDraw}"
																	style="margin-left: 4px; text-decoration: none; cursor: pointer">
																	<h:outputText id="text2" styleClass="outputText"
																		value="My Hotel"></h:outputText>
																	<hx:graphicImageEx styleClass="graphicImageEx"
																		id="imageExHotelIcon"
																		value="/scweb/images/hotelIcon2.jpg" hspace="10"
																		border="0" vspace="0" width="41" height="45"
																		rendered="#{userHandler.canWorkWithBlueChipDraw}"
																		align="middle"></hx:graphicImageEx>
																</hx:outputLinkEx></td>
																<TD align="left"></TD>
															</tr>
														</tbody>
													</table>
													</TD>
												</tr>
											</TBODY>
										</TABLE>
										</DIV>
								</hx:jspPanel>


				<hx:jspPanel id="jspPanelRouteSummary" rendered="#{userHandler.canWorkWithRoutes}">
						<hx:jspPanel id="jspPanelHasRoutesAssigned" rendered="#{routeListHandler.hasRoutesAssigned}">
									
									<TABLE border="0" cellpadding="0" cellspacing="0" width="600">
						<TBODY>
							<TR>
								<TD align="center" nowrap="nowrap" bgcolor="#7D95B3" width="250"><h:outputText
									styleClass="outputText" id="text1"
									value="#{labels.routeHeader} (#{routeListHandler.numberOfRoutes})"
									style="color: white; font-size: 12px; font-weight: bold"></h:outputText></TD>
								<TD bgcolor="white" align="left" width="350">&nbsp;</TD>
							</TR>
							<TR>
								<TD colspan="2">
								<TABLE class="borderedTable" border="0" cellpadding="0"
									cellspacing="0" width="100%">
									<TBODY>
										<TR>
											<TD>&nbsp;</TD>
											<TD><hx:dataTableEx border="0" cellpadding="2"
																		cellspacing="0" columnClasses="columnClass1"
																		headerClass="headerClass" footerClass="footerClass"
																		rowClasses="rowClass1,rowClass2"
																		styleClass="dataTableEx"
																		id="tableExRouteListDataTable"
																		style="margin-bottom: 3px; margin-top: 3px"
																		value="#{routeListHandler.routesAsCollection}"
																		var="varroutesAsCollection" width="100%" rows="15">
																		<f:facet name="footer">
																			<hx:panelBox styleClass="panelBox"
																				id="boxDataTableFooter" width="100%">
																				<hx:outputStatistics styleClass="outputStatistics"
																					id="statistics1"></hx:outputStatistics>
																				<hx:pagerWeb styleClass="pagerWeb" id="web1" />
																			</hx:panelBox>
																		</f:facet>
																		<f:facet name="header">
																			<hx:panelBox styleClass="panelBox" id="box1">
																				<h:outputText styleClass="outputText"
																					id="textDataTableHeaderText"
																					value="#{labels.routeSummaryInstructions}"
																					style="font-weight: bold"></h:outputText>
																			</hx:panelBox>
																		</f:facet>
																		<hx:columnEx id="columnEx1">
																			<f:facet name="header">
																				<h:outputText id="text3" styleClass="outputText"
																					value="#{labels.routeMarketIDColName}"></h:outputText>
																			</f:facet>
																			<h:outputText styleClass="outputText"
																				id="textDataTableMarketIDVal"
																				value="#{varroutesAsCollection.marketID}"
																				style="margin-left: 9px"></h:outputText>
																		</hx:columnEx>
																		<hx:columnEx id="columnEx2">
																			<f:facet name="header">
																				<h:outputText
																					value="#{labels.routeDistrictIDColName}"
																					styleClass="outputText" id="text4"></h:outputText>
																			</f:facet>
																			<h:outputText styleClass="outputText"
																				id="textRouteListDataTableDistIdVal"
																				value="#{varroutesAsCollection.districtID}"
																				style="margin-left: 9px"></h:outputText>
																		</hx:columnEx>
																		<hx:columnEx id="columnEx3">
																			<f:facet name="header">
																				<h:outputText value="#{labels.routeIDColName}"
																					styleClass="outputText" id="text5"></h:outputText>
																			</f:facet>
																			<h:outputText styleClass="outputText"
																				id="textDataTableRouteListRouteIDVal"
																				style="margin-left: 9px"
																				value="#{varroutesAsCollection.routeID}"></h:outputText>
																		</hx:columnEx>
																		<hx:columnEx id="columnEx4">
																			<f:facet name="header">
																				<h:outputText value="#{labels.routeDescColName}"
																					styleClass="outputText" id="text6"></h:outputText>
																			</f:facet>
																			<h:outputText styleClass="outputText"
																				id="textDataTableRouteListRteDesVal"
																				style="margin-left: 5px"
																				value="#{varroutesAsCollection.routeDescription}"></h:outputText>
																		</hx:columnEx>
																		<hx:columnEx id="columnEx5">
																							<hx:commandExRowAction id="rowAction1"
																								action="#{pc_Home.doRowAction1Action}">
																								<f:param
																									value="#{varroutesAsCollection.marketID}"
																									name="marketID" id="param1"></f:param>
																								<f:param
																									value="#{varroutesAsCollection.districtID}"
																									name="districtID" id="param2"></f:param>
																								<f:param
																									value="#{varroutesAsCollection.routeID}"
																									name="routeID" id="param3"></f:param>
																							</hx:commandExRowAction>
																							<f:facet name="header"></f:facet>
																		</hx:columnEx>
																	</hx:dataTableEx>
											 
											</TD>
											<TD>&nbsp;</TD>
										</TR>
									</TBODY>
								</TABLE>
								</TD>
							</TR>
						</TBODY>
					</TABLE>
									
									
									
						</hx:jspPanel>
				
									<hx:jspPanel id="jspPanelNoRoutesAssigned"
										rendered="#{not routeListHandler.hasRoutesAssigned }">

										<TABLE border="0" cellpadding="0" cellspacing="0" width="600">
											<TBODY>
												<TR>
													<TD align="center" nowrap="nowrap" bgcolor="#7D95B3"
														width="250"><h:outputText styleClass="outputText"
														id="textNoRoutesHeader"
														value="#{labels.routeHeader} (#{routeListHandler.numberOfRoutes})"
														style="color: white; font-size: 12px; font-weight: bold"></h:outputText></TD>
													<TD bgcolor="white" width="350">&nbsp;</TD>
												</TR>
												<tr>
													<TD colspan="2">
													<TABLE class="borderedTable" border="0" cellpadding="0"
														cellspacing="0" width="100%">
														<TBODY>
															<TR>
																<td><IMG border="0" src="/scweb/theme/1x1.gif"
																	width="1" height="1" hspace="10" vspace="10"></td>
															</tr>
															<tr>
																<td><h:outputText styleClass="outputText"
																	id="textNoRoutesMessage"
																	value="#{labels.routesNoRoutes}"
																	style="color: #ee3e41; font-size: 14px; font-weight: bold; text-indent: 5px"></h:outputText>
																</td>
															</tr>
															<TR>
																<td><IMG border="0" src="/scweb/theme/1x1.gif"
																	width="1" height="1" hspace="10" vspace="10"></td>
															</tr>
														</tbody>
													</table>
													</TD>
												</tr>
											</TBODY>
										</TABLE>
									</hx:jspPanel>
								</hx:jspPanel>

			</h:form>									
		</hx:scriptCollector><%-- /tpl:put --%>
							</div> <br> <!-- end main content area -->
						</td>
					</tr>
					<tr>
						<td class="leftNavTD" align="left" valign="bottom">
							<div class="bottomNavAreaImage"></div>
						</td>
						<td></td>

					</tr>
					<tr>
						<td align="left" valign="top" colspan="3" height="20"><img
							class="footerImage" src="/scweb/theme/images/USATODAY_Footer_940x12.jpg">
							<!-- 							class="footerImage" src="/scweb/theme/images/800x20_art2.jpg"> -->

						</td>
					</tr>
					<tr>
						<td align="center" valign="top"><div class="footer"></div>
						</td>
						<td></td>
						<td class="mainContentWideTD" align="center" valign="top">
							<div class="footer">
								<table border="0" cellspacing="0" cellpadding="0" width="675">
									<tbody>
										<tr>
											<td class="mainContentWideTD" align="center">
												<!-- someting in this table drastically impacts how this template works so leave it in for now -->
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top" colspan="3">
							<div class="footer">
								<table border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td><img src="/scweb/theme/1x1.gif" hspace="15">
											</td>
											<!-- 											<td class="mainContentWideTD" align="center">&copy; Copyright -->
											<!-- 												2007 <a href="http://www.usatoday.com/marketing/credit.htm" -->
											<!-- 												target="_blank">USA TODAY</a>, a division of <a -->
											<!-- 												href="http://www.gannett.com" target="_blank">Gannett Co.</a> Inc. <a -->
											<!-- 												href="/scweb/legal/privacy.html" target="_blank">Privacy Policy</a>, -->
											<!-- 												By using this service, you accept our <a -->
											<!-- 												href="/scweb/legal/service.html" target="_blank">Terms of -->
											<!-- 													Service.</a></td> -->
											<td><br>&copy; <script>
												document.write(getCurrentYear());
											</script> USA TODAY, a <a href="http://www.gannett.com">Gannett Company</a> <a
												href= "/scweb/legal/privacy.html" target="_blank">| Privacy
													Policy</a>| By using this service, you accept
												our <a href="/scweb/legal/service.html" target="_blank">Terms of
													Service</a>.</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:scriptCollector>
	</body>
</f:view>
<!--  following link put here because of a bug in faces that seems to prevent styles from working properly -->
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
</html>
<%-- /tpl:insert --%>
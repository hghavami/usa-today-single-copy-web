<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/sc_secure/profile/My_profile.java" --%><%-- /jsf:pagecode --%>
<%-- tpl:insert page="/theme/JSP-C-blue_mod.jtpl" --%><!DOCTYPE HTML>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%
	OmnitureTrackingCode otc = (OmnitureTrackingCode) session
			.getAttribute("omnitureTracking");
	if (otc == null) {
		otc = new OmnitureTrackingCode();
	}
	String omnitureP1 = otc.getTrackingCodePart1();

	// following work around due to bug in RAD
	String useMap = "#map_800x125_top";
	String headURL = "/scweb/index.usat";
%>
<!-- NEW path -->


<link rel="stylesheet" href="/scweb/theme/Master.css" type="text/css">
<link href="/scweb/theme/C_master_blue.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/scweb/theme/stylesheet.css"
	title="Style">

<script src="/scweb/scripts/windowutility.js"></script>
<script language="JavaScript">
	// method called on page load
	function _initPage() {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this

		if (typeof window.initPage == "function") {
			window.initPage();
		}

	}

	// method called on page unload
	function _unloadPage(thisObj, thisEvent) {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this
		if (typeof window.unloadPage == "function") {
			window.unloadPage(thisObj, thisEvent);
		}

	}

	// method called on page unload
	function _onbeforeunloadPage(thisObj, thisEvent) {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this
		if (typeof window.onbeforeunloadPage == "function") {
			window.onbeforeunloadPage(thisObj, thisEvent);
		}

	}
</script>

<%=omnitureP1%>

<%-- tpl:put name="headarea" --%>
<TITLE>Update Profile</TITLE>
<LINK rel="stylesheet" type="text/css" href="../../theme/stylesheet.css" title="Style">
<script type="text/javascript" src="/scweb/scripts/scriptaculous/prototype.js"></script>
<SCRIPT language="JavaScript">
// change Omniture Default Page name
s.pageName = "SC User Profile Mngmnt";		
// Allow one submission
var fpSubmitClicked = false;

Event.observe(window, 'load', function() {

	if (document.getElementById('formUpdateProfile:reminderOptInHidden').value == 'No') {
		// Disable frequency radio buttons
		$('formUpdateProfile:radioEmailTime').hide();
	}
});

function func_1(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	
	if (thisObj.value == 'No') {
		// Disable frequency radio buttons
		$('formUpdateProfile:radioEmailTime').hide();

	}
	else {
			// Enable frequency radio buttons
		$('formUpdateProfile:radioEmailTime').show();
	}

}

function func_2(thisObj, thisEvent) {

	if (fpSubmitClicked == false) {
		fpSubmitClicked = true;
		thisObj.value = "Please Wait...";
		return true;
	}
	else {
		return false;
	}
}

</SCRIPT>
				<%-- /tpl:put --%>
<script language="javascript">
	//var logoutMsg = 'Please close all Draw Management windows before logging out';
	function cleanupWindows() {
		// switch to:
		if (rdlWindow != null) {
			//if (rdlWindow.closed == false) {
			if (rdlWindow.open && !rdlWindow.closed) {
				rdlWindow.setForceClose();
				rdlWindow.close();
			}

		}

		if (weeklyRDLWindow != null) {
			if (weeklyRDLWindow.open && !weeklyRDLWindow.closed) {
				weeklyRDLWindow.setForceClose();
				weeklyRDLWindow.close();
			}
		}

		if (helpWindow != null) {
			if (helpWindow.open && !helpWindow.closed) {
				helpWindow.close();
			}
		}

		if (printWindow != null) {
			if (printWindow.open && !printWindow.closed) {
				printWindow.close();
			}
		}

		return true;
	}

	function okToLogout() {
		var alert = false;
		if (rdlWindow != null) {
			if (rdlWindow.open && !rdlWindow.closed) {
				alert = true;
			}
		}

		if (weeklyRDLWindow != null) {
			if (weeklyRDLWindow.open && !weeklyRDLWindow.closed) {
				alert = true;
			}
		}

		if (alert) {
			setTimeout(
					'alert("You must close all Draw Management windows before logging out.")',
					100);
			return false;
		} else {
			cleanupWindows();
			return true;
		}
	}

	function func_logoutOK(thisObj, thisEvent) {
		//use 'thisObj' to refer directly to this component instead of keyword 'this'
		//use 'thisEvent' to refer to the event generated instead of keyword 'event'
		return okToLogout();
	}
	function getCurrentYear() {
		today = new Date();
		y0 = today.getFullYear();
		return y0;
	}
</script>
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="charset=ISO-8859-1">
</head>
<f:view locale="#{userLocaleHandler.locale}">
	<f:loadBundle basename="resourcesFile" var="labels" />
	<body onLoad="_initPage();" onUnload="_unloadPage(this, event);"
		onbeforeunload="_onbeforeunloadPage(this, event);">
		<hx:scriptCollector id="templateScriptCollector">


			<!-- start header area -->
			<div class="topAreaBox1"></div>
			<div class="topAreaLogo">
				<!-- 				<img border="0" src="/scweb/theme/images/800x125_top.jpg" -->
				<%-- 					width="800" height="125" usemap="<%=useMap %>"> --%>
				<img border="0" src="/scweb/theme/images/USATODAY_Header_940x145.jpg"
					width="940" height="145" usemap="<%=useMap%>">
				<map name="map_800x125_top">
					<area shape="rect" target="_self" href="<%=headURL%>"
						coords="19,17,268,112">
					<area shape="default" nohref>
				</map>
			</div>
			<div class="topAreaLogoLine">
				<img border="0" src="/scweb/theme/images/800x20_art2.jpg" height="20"
					width="800">
			</div>
			<!-- end header area -->

			<!-- start header navigation bar -->
			<div class="topNavBk"></div>${omnitureTracking.trackingCodePart2}<div
				class="topNav"></div>
			<div class="topAreaDatePlacement">
				<fmt:formatDate value="${pc_JSPCblue_mod.now}" dateStyle="full" />
			</div>
			<h:outputText styleClass="globalMessageDivStyle" id="textGlobalMessageText"
				value="#{globalMessage.message}"
				rendered='#{ (globalMessage.showMessage) and ((globalMessage.showToWhom eq "all") or ((globalMessage.showToWhom eq "hotel") and userHandler.canWorkWithBlueChipDraw ) or ((globalMessage.showToWhom eq "contractor") and userHandler.canWorkWithRoutes) )}'></h:outputText>
			<!-- end header navigation bar bar -->

			<!-- start left-hand navigation -->
			<table class="mainBox" border="0" cellpadding="0" width="100%" height="87%"
				cellspacing="0">
				<tbody>
					<tr>
						<td class="leftNavTD" align="left" valign="top">
							<div class="leftNavBox">
								<h:form styleClass="form" id="formLogout">
									<%-- tpl:put name="LeftNavTopMostJSPContentArea" --%><%-- /tpl:put --%>
									<div id="logoutLinkDiv" class="noDisplay" style="${userHandler.divCss}">
										<h:outputFormat styleClass="outputFormat" id="formatWelcomeMessage"
											value="Welcome, {0}">
											<f:param name="displayName" value="#{userHandler.displayName}"></f:param>
										</h:outputFormat>
										<br>
										<hx:commandExButton type="submit" value="#{labels.logoutLink}"
											styleClass="commandExButtonSmall" id="buttonDoLogoutBtn"
											title="Logout" alt="Logout"
											action="#{pc_JSPCblue_mod.doButtonDoLogoutBtnAction}"
											onclick="return func_logoutOK(this, event);"></hx:commandExButton>
										<hr width="95%" align="left">
									</div>
									<siteedit:navbar spec="/scweb/theme/nav_vertical_tree_left.jsp"
										targetlevel="1-5" onlychildren="true" navclass="leftNav"
										target="topchildren" />
									<br>
									<br>
									<br>
									<!-- If we add internatiolization
								<h:selectOneMenu styleClass="selectOneMenu" id="menui18nPref" value="#{userLocaleHandler.localeStr}">
									<f:selectItem itemValue="en" itemLabel="English" />
									<f:selectItem itemValue="es" itemLabel="Spanish" />
								</h:selectOneMenu><hx:commandExButton type="submit" value="Go"
									styleClass="commandExButton" id="buttonUpdateLangPref"
									action="#{pc_JSPCblue_mod.doButtonUpdateLangPrefAction}"></hx:commandExButton> -->
									<br>
									<hx:inputHelperKeybind key="Enter" id="inputHelperKeybind1"
										targetAction="nothing" />
								</h:form>
								<br>
								<hr width="95%" align="left">
								<b>Related Links:</b><br> <a href="http://usatoday.com"
									target="_blank">USA TODAY.com</a><br> <a
									href="https://service.usatoday.com/welcome.jsp" target="_blank">USA
									TODAY Home Delivery</a><br> <a
									href="https://service.usatoday.com/shop" target="_blank">USA TODAY
									Past Issues</a><br> <br> <br>

							</div>
						</td>
						<!-- end left-hand navigation -->
						<td><img border="0" src="/scweb/theme/img/JSF_1x1.gif" width="1"
							height="1" hspace="6">
						</td>
						<td class="mainContentWideTD" align="left" valign="top" rowspan="2">
							<!-- start main content area -->
							<div class="mainContentWideBox">
								<img border="0" src="/scweb/theme/images/USATODAY_Subhead_740x76.jpg"
									width="740" height="76" usemap="<%=useMap%>">
								<!-- bread crumbs if we want em-->
								<div class="navTrailLoc">
									<siteedit:navtrail start="[" end="]" target="home,parent,ancestor,self"
										separator="&gt;&gt;" spec="/scweb/theme/trail.jsp" />
								</div>
								<a name="navskip"><img border="0" src="/scweb/theme/img/JSF_1x1.gif"
									width="1" height="1" alt="Beginning of page content"> </a>
								<%-- tpl:put name="jspbodyarea" --%><hx:scriptCollector id="scriptCollector1">
									<h:form styleClass="form" id="formUpdateProfile">
										
										
										<TABLE border="0" cellpadding="1" cellspacing="1" width="550">
											<TBODY>
												<TR>
													<TD colspan="2">
													<CENTER>
													<TABLE border="0" cellpadding="1" cellspacing="2"
														width="400" bgcolor="white">
														<TBODY>
															<TR>
																<TD align="left" nowrap="nowrap"><UL type="disc">
																	<h:outputText styleClass="outputText"
																		id="textProfileUpdateInstructions"
																		value="#{labels.profileUpdateInstructionsL1}"
																		style="font-size: 12px; font-weight: bold; left: -12px; position: relative"></h:outputText>
																	<LI type="disc"><h:outputText styleClass="outputText"
																		id="textUpdateProfileInstructionL2"
																		value="#{labels.profileUpdateInstructionsL2}"></h:outputText></LI>
																	<LI type="disc"><h:outputText styleClass="outputText"
																		id="textUpdateProfileInstructionL3"
																		value="#{labels.profileUpdateInstructionsL3}"></h:outputText></LI>
																</UL>
																</TD>
															</TR>
														</TBODY>
													</TABLE>
													</CENTER>
													</TD>
													<TD></TD>
												</TR>
												<TR>
													<TD colspan="3"><h:messages styleClass="messages"
														id="messagesGeneralUpdateProfileErrors" globalOnly="true" style="font-size: 12pt"></h:messages></TD>
												</TR>
												<TR>
													<TD></TD>
													<TD></TD>
													<TD></TD>
												</TR>
												<TR>
													<TD colspan="2" bgcolor="#eeeeee"><SPAN style="font-weight: bold; font-size: 12px">:: <h:outputText
														styleClass="outputText" id="textUpdateEmailAddressHeader" value="#{labels.updateEmailAddressHeader}"></h:outputText> ::</SPAN></TD><TD></TD>
												</TR>
												<TR>
													<TD align="right" nowrap="nowrap"><h:outputText styleClass="outputText"
														id="textCurrentEmailAddress"
														value="#{labels.emailAddressCurrent}:"></h:outputText></TD>
													<TD align="left"><h:outputText styleClass="outputText"
														id="textCurrentEmailOutputText"
														value="#{userHandler.user.emailAddress}" style="color: black"></h:outputText></TD>
													<TD></TD>
												</TR>
												<TR>
													<TD align="right" nowrap="nowrap"></TD>
													<TD align="left" colspan="2"></TD></TR>
												<TR>
													<TD align="right" nowrap="nowrap"><h:outputText styleClass="outputText"
														id="textNewEmailAddress"
														value="#{labels.emailAddressNew}:"></h:outputText></TD>
													<TD><h:inputText styleClass="inputText"
														id="text_inputNewEmail" size="30" maxlength="50">
														<f:validateLength maximum="50"></f:validateLength>
													</h:inputText></TD>
													<TD nowrap="nowrap"><h:message styleClass="message" id="message1"
														for="text_inputNewEmail" style="font-size: 11pt"></h:message></TD>
												</TR>
												<TR>
													<TD align="right" nowrap="nowrap"><h:outputText styleClass="outputText"
														id="textConfirmNewEmail" value="#{labels.emailAddressConfirmNew}:"></h:outputText></TD>
													<TD nowrap="nowrap"><h:inputText styleClass="inputText"
														id="text_inputConfirmNewEmail" size="30" maxlength="50">
														<f:validateLength maximum="50"></f:validateLength>
													</h:inputText></TD>
													<TD nowrap="nowrap"><h:message styleClass="message" id="message2"
														for="text_inputConfirmNewEmail"></h:message></TD>
												</TR>
												<TR>
													<TD nowrap="nowrap"></TD>
													<TD></TD>
													<TD nowrap="nowrap"></TD>
												</TR>
												<TR>
													<TD nowrap="nowrap"></TD>
													<TD></TD>
													<TD nowrap="nowrap"></TD>
												</TR>
												<TR>
													<TD nowrap="nowrap" colspan="2" bgcolor="#eeeeee"><SPAN style="font-weight: bold; font-size: 12px">:: <h:outputText
														styleClass="outputText" id="textUpdatePasswordHeader"
														value="#{labels.updatePasswordHeader}"></h:outputText> ::</SPAN></TD><TD nowrap="nowrap"></TD>
												</TR>
												<TR>
													<TD align="right" nowrap="nowrap"></TD>
													<TD colspan="2"><h:outputText styleClass="outputText"
														id="textPasswordRules" value="#{labels.passwordRules}"></h:outputText></TD></TR>
												<TR>
													<TD align="right" nowrap="nowrap"></TD>
													<TD colspan="2" align="left"></TD>
												</TR>
												<TR>
													<TD align="right" nowrap="nowrap"><h:outputText styleClass="outputText"
														id="textNewPassword" value="#{labels.passwordNew}:"></h:outputText></TD>
													<TD><h:inputSecret styleClass="inputSecret"
														id="secret_inputNewPassword" size="30">
														<f:validateLength minimum="6" maximum="10"></f:validateLength>
														<hx:validateConstraint regex="^[A-Za-z0-9]+$" />
													</h:inputSecret></TD>
													<TD nowrap="nowrap"><h:message styleClass="message" id="message3"
														for="secret_inputNewPassword" style="font-size: 11pt"></h:message></TD>
												</TR>
												<TR>
													<TD align="right" nowrap="nowrap"><h:outputText
														styleClass="outputText" id="textConfirmNewPassword"
														value="#{labels.passwordConfirmNew}:"></h:outputText></TD>
													<TD><h:inputSecret styleClass="inputSecret"
														id="secret_inputConfirmNewPassword" size="30">
														<f:validateLength minimum="6" maximum="10"></f:validateLength>
														<hx:validateConstraint regex="^[A-Za-z0-9]+$" />
													</h:inputSecret></TD>
													<TD nowrap="nowrap"><h:message styleClass="message" id="message4"
														for="secret_inputConfirmNewPassword"></h:message></TD>
												</TR>
												<TR>
													<TD></TD>
													<TD></TD>
													<TD></TD>
												</TR>
											<tr>
												<td colspan="2" bgcolor="#eeeeee"><hx:jspPanel	id="jspPanelEmailHeader" rendered="#{userHandler.canWorkWithBlueChipDraw}">
												<SPAN
																	style="font-weight: bold; font-size: 12px">:: <h:outputText
																	styleClass="outputText" id="text1"
																	value="Email Reminders"></h:outputText> ::</SPAN></hx:jspPanel>
												</td><td></td>
											</tr>
											<TR>
													<TD colspan="3"><hx:jspPanel
														id="jspPanelBlueChipEmailReminders"
														rendered="#{userHandler.canWorkWithBlueChipDraw}">
														<table>
															<TR>
																<TD valign="top" colspan="3" width="550"><h:selectOneRadio
																	disabledClass="selectOneRadio_Disabled"
																	styleClass="selectOneRadio" id="radioReminderOptIn"
																	layout="pageDirection"
																	value="#{userHandler.receivingEmailReminders}"
																	onclick="return func_1(this, event);">
																	<f:selectItem itemValue="No"
																		itemLabel="Don't Send Me Email Reminders" />
																	<f:selectItem itemValue="Yes"
																		itemLabel="Send Me Email Reminders" />
																</h:selectOneRadio><h:inputHidden
																	id="reminderOptInHidden" value="#{userHandler.receivingEmailRemindersReadOnly}"></h:inputHidden></TD>
															</TR>
															<TR>
																<TD colspan="3" nowrap="nowrap" align="left" width="550"><h:selectOneRadio
																	disabledClass="selectOneRadio_Disabled"
																	styleClass="selectOneRadio" id="radioEmailTime"
																	layout="pageDirection" style="margin-left: 15px"
																	value="#{userHandler.emailReminderTimePreference}">
																	<f:selectItems
																		value="#{blueChipEmailScheduleCacheHandler.blueChipEmailSchedulesForForSelectItems}" />
																</h:selectOneRadio></TD>
															</TR>
														</table>
													</hx:jspPanel></TD>
												</TR>
												<TR>
													<TD>&nbsp;</TD>
													<TD></TD>
													<TD></TD>
												</TR>
												<tr>
													<td></td>
													<td><hx:commandExButton type="submit"
														value="#{labels.updateProfileBtn}"
														styleClass="commandExButton"
														id="buttonSubmitProfileUpdate"
														action="#{pc_My_profile.doButtonSubmitProfileUpdateAction}" onclick="return func_2(this, event);"></hx:commandExButton></td>
													<td></td>
												</tr>
											</TBODY>
										</TABLE>
									</h:form>
								</hx:scriptCollector><%-- /tpl:put --%>
							</div> <br> <!-- end main content area -->
						</td>
					</tr>
					<tr>
						<td class="leftNavTD" align="left" valign="bottom">
							<div class="bottomNavAreaImage"></div>
						</td>
						<td></td>

					</tr>
					<tr>
						<td align="left" valign="top" colspan="3" height="20"><img
							class="footerImage" src="/scweb/theme/images/USATODAY_Footer_940x12.jpg">
							<!-- 							class="footerImage" src="/scweb/theme/images/800x20_art2.jpg"> -->

						</td>
					</tr>
					<tr>
						<td align="center" valign="top"><div class="footer"></div>
						</td>
						<td></td>
						<td class="mainContentWideTD" align="center" valign="top">
							<div class="footer">
								<table border="0" cellspacing="0" cellpadding="0" width="675">
									<tbody>
										<tr>
											<td class="mainContentWideTD" align="center">
												<!-- someting in this table drastically impacts how this template works so leave it in for now -->
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top" colspan="3">
							<div class="footer">
								<table border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td><img src="/scweb/theme/1x1.gif" hspace="15">
											</td>
											<!-- 											<td class="mainContentWideTD" align="center">&copy; Copyright -->
											<!-- 												2007 <a href="http://www.usatoday.com/marketing/credit.htm" -->
											<!-- 												target="_blank">USA TODAY</a>, a division of <a -->
											<!-- 												href="http://www.gannett.com" target="_blank">Gannett Co.</a> Inc. <a -->
											<!-- 												href="/scweb/legal/privacy.html" target="_blank">Privacy Policy</a>, -->
											<!-- 												By using this service, you accept our <a -->
											<!-- 												href="/scweb/legal/service.html" target="_blank">Terms of -->
											<!-- 													Service.</a></td> -->
											<td><br>&copy; <script>
												document.write(getCurrentYear());
											</script> USA TODAY, a <a href="http://www.gannett.com">Gannett Company</a> <a
												href= "/scweb/legal/privacy.html" target="_blank">| Privacy
													Policy</a>| By using this service, you accept
												our <a href="/scweb/legal/service.html" target="_blank">Terms of
													Service</a>.</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:scriptCollector>
	</body>
</f:view>
<!--  following link put here because of a bug in faces that seems to prevent styles from working properly -->
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
</html>
<%-- /tpl:insert --%>
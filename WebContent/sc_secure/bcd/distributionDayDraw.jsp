<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/sc_secure/bcd/DistributionDayDraw.java" --%><%-- /jsf:pagecode --%>
<%-- tpl:insert page="/theme/JSP-C-blue_mod.jtpl" --%><!DOCTYPE HTML>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%
	OmnitureTrackingCode otc = (OmnitureTrackingCode) session
			.getAttribute("omnitureTracking");
	if (otc == null) {
		otc = new OmnitureTrackingCode();
	}
	String omnitureP1 = otc.getTrackingCodePart1();

	// following work around due to bug in RAD
	String useMap = "#map_800x125_top";
	String headURL = "/scweb/index.usat";
%>
<!-- NEW path -->


<link rel="stylesheet" href="/scweb/theme/Master.css" type="text/css">
<link href="/scweb/theme/C_master_blue.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/scweb/theme/stylesheet.css"
	title="Style">

<script src="/scweb/scripts/windowutility.js"></script>
<script language="JavaScript">
	// method called on page load
	function _initPage() {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this

		if (typeof window.initPage == "function") {
			window.initPage();
		}

	}

	// method called on page unload
	function _unloadPage(thisObj, thisEvent) {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this
		if (typeof window.unloadPage == "function") {
			window.unloadPage(thisObj, thisEvent);
		}

	}

	// method called on page unload
	function _onbeforeunloadPage(thisObj, thisEvent) {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this
		if (typeof window.onbeforeunloadPage == "function") {
			window.onbeforeunloadPage(thisObj, thisEvent);
		}

	}
</script>

<%=omnitureP1%>

<%-- tpl:put name="headarea" --%>
<script type="text/javascript" src="/scweb/scripts/scriptaculous/prototype.js"></script>
<script src="/scweb/scripts/commonValidations.js"></script>
<script type="text/javascript" src="/scweb/scripts/blueChipDrawMngmt.js"></script>
<SCRIPT type="text/javascript">
s.pageName = "Blue Chip Hotel Current Distribution Day Draw";
s.channel = "Blue Chip Draw";

var firstField = null;
				
Event.observe(window, 'load', function() {

	var inputArray = $$('input.drawInputText');

	var firstFound = false;
	
	if (inputArray != null && inputArray.length > 0) {
		editableDrawFields = inputArray;
		inputArray[0].focus();
		inputArray[0].select();
		firstField = inputArray[0];

		countDownMinutes = parseInt($('formDistributionDatePOForm:minutesToStaleData').value, 10);
	
		// start countdown
		updateBlueChipCountdown();

	}

	
}
);

</SCRIPT>
<SCRIPT type="text/javascript">
function func_showHistory(thisObj, thisEvent, divHTML) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	var div = $('DrawHistoryBox');
	var source = $(thisObj);
	div.innerHTML = divHTML;
	div.show();
	Element.clonePosition(div, source, {setHeight: false, setWidth: false, offsetTop: source.offsetHeight});
	return false;
}
function func_hideDrawHistory(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	var div = $('DrawHistoryBox');
	div.hide();
	div.innerHTML = "";	
	return false;
}

function func_9(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
helpScreen('/scweb/sc_secure/bcd/bcdHelp.usat'); 
if (helpWindow) {
	helpWindow.focus();
}
return false;
}
function func_10(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
window.status='Click for Help On This Page';

}
function func_11(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
window.status='';
}
function func_12(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	window.print();
	return false;
}
</SCRIPT>
			<TITLE>Next Distribution (Delivery) Day</TITLE>
					<LINK rel="stylesheet" type="text/css"
						href="/scweb/theme/stylesheet.css" title="Style">
				<%-- /tpl:put --%>
<script language="javascript">
	//var logoutMsg = 'Please close all Draw Management windows before logging out';
	function cleanupWindows() {
		// switch to:
		if (rdlWindow != null) {
			//if (rdlWindow.closed == false) {
			if (rdlWindow.open && !rdlWindow.closed) {
				rdlWindow.setForceClose();
				rdlWindow.close();
			}

		}

		if (weeklyRDLWindow != null) {
			if (weeklyRDLWindow.open && !weeklyRDLWindow.closed) {
				weeklyRDLWindow.setForceClose();
				weeklyRDLWindow.close();
			}
		}

		if (helpWindow != null) {
			if (helpWindow.open && !helpWindow.closed) {
				helpWindow.close();
			}
		}

		if (printWindow != null) {
			if (printWindow.open && !printWindow.closed) {
				printWindow.close();
			}
		}

		return true;
	}

	function okToLogout() {
		var alert = false;
		if (rdlWindow != null) {
			if (rdlWindow.open && !rdlWindow.closed) {
				alert = true;
			}
		}

		if (weeklyRDLWindow != null) {
			if (weeklyRDLWindow.open && !weeklyRDLWindow.closed) {
				alert = true;
			}
		}

		if (alert) {
			setTimeout(
					'alert("You must close all Draw Management windows before logging out.")',
					100);
			return false;
		} else {
			cleanupWindows();
			return true;
		}
	}

	function func_logoutOK(thisObj, thisEvent) {
		//use 'thisObj' to refer directly to this component instead of keyword 'this'
		//use 'thisEvent' to refer to the event generated instead of keyword 'event'
		return okToLogout();
	}
	function getCurrentYear() {
		today = new Date();
		y0 = today.getFullYear();
		return y0;
	}
</script>
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="charset=ISO-8859-1">
</head>
<f:view locale="#{userLocaleHandler.locale}">
	<f:loadBundle basename="resourcesFile" var="labels" />
	<body onLoad="_initPage();" onUnload="_unloadPage(this, event);"
		onbeforeunload="_onbeforeunloadPage(this, event);">
		<hx:scriptCollector id="templateScriptCollector">


			<!-- start header area -->
			<div class="topAreaBox1"></div>
			<div class="topAreaLogo">
				<!-- 				<img border="0" src="/scweb/theme/images/800x125_top.jpg" -->
				<%-- 					width="800" height="125" usemap="<%=useMap %>"> --%>
				<img border="0" src="/scweb/theme/images/USATODAY_Header_940x145.jpg"
					width="940" height="145" usemap="<%=useMap%>">
				<map name="map_800x125_top">
					<area shape="rect" target="_self" href="<%=headURL%>"
						coords="19,17,268,112">
					<area shape="default" nohref>
				</map>
			</div>
			<div class="topAreaLogoLine">
				<img border="0" src="/scweb/theme/images/800x20_art2.jpg" height="20"
					width="800">
			</div>
			<!-- end header area -->

			<!-- start header navigation bar -->
			<div class="topNavBk"></div>${omnitureTracking.trackingCodePart2}<div
				class="topNav"></div>
			<div class="topAreaDatePlacement">
				<fmt:formatDate value="${pc_JSPCblue_mod.now}" dateStyle="full" />
			</div>
			<h:outputText styleClass="globalMessageDivStyle" id="textGlobalMessageText"
				value="#{globalMessage.message}"
				rendered='#{ (globalMessage.showMessage) and ((globalMessage.showToWhom eq "all") or ((globalMessage.showToWhom eq "hotel") and userHandler.canWorkWithBlueChipDraw ) or ((globalMessage.showToWhom eq "contractor") and userHandler.canWorkWithRoutes) )}'></h:outputText>
			<!-- end header navigation bar bar -->

			<!-- start left-hand navigation -->
			<table class="mainBox" border="0" cellpadding="0" width="100%" height="87%"
				cellspacing="0">
				<tbody>
					<tr>
						<td class="leftNavTD" align="left" valign="top">
							<div class="leftNavBox">
								<h:form styleClass="form" id="formLogout">
									<%-- tpl:put name="LeftNavTopMostJSPContentArea" --%><%-- /tpl:put --%>
									<div id="logoutLinkDiv" class="noDisplay" style="${userHandler.divCss}">
										<h:outputFormat styleClass="outputFormat" id="formatWelcomeMessage"
											value="Welcome, {0}">
											<f:param name="displayName" value="#{userHandler.displayName}"></f:param>
										</h:outputFormat>
										<br>
										<hx:commandExButton type="submit" value="#{labels.logoutLink}"
											styleClass="commandExButtonSmall" id="buttonDoLogoutBtn"
											title="Logout" alt="Logout"
											action="#{pc_JSPCblue_mod.doButtonDoLogoutBtnAction}"
											onclick="return func_logoutOK(this, event);"></hx:commandExButton>
										<hr width="95%" align="left">
									</div>
									<siteedit:navbar spec="/scweb/theme/nav_vertical_tree_left.jsp"
										targetlevel="1-5" onlychildren="true" navclass="leftNav"
										target="topchildren" />
									<br>
									<br>
									<br>
									<!-- If we add internatiolization
								<h:selectOneMenu styleClass="selectOneMenu" id="menui18nPref" value="#{userLocaleHandler.localeStr}">
									<f:selectItem itemValue="en" itemLabel="English" />
									<f:selectItem itemValue="es" itemLabel="Spanish" />
								</h:selectOneMenu><hx:commandExButton type="submit" value="Go"
									styleClass="commandExButton" id="buttonUpdateLangPref"
									action="#{pc_JSPCblue_mod.doButtonUpdateLangPrefAction}"></hx:commandExButton> -->
									<br>
									<hx:inputHelperKeybind key="Enter" id="inputHelperKeybind1"
										targetAction="nothing" />
								</h:form>
								<br>
								<hr width="95%" align="left">
								<b>Related Links:</b><br> <a href="http://usatoday.com"
									target="_blank">USA TODAY.com</a><br> <a
									href="https://service.usatoday.com/welcome.jsp" target="_blank">USA
									TODAY Home Delivery</a><br> <a
									href="https://service.usatoday.com/shop" target="_blank">USA TODAY
									Past Issues</a><br> <br> <br>

							</div>
						</td>
						<!-- end left-hand navigation -->
						<td><img border="0" src="/scweb/theme/img/JSF_1x1.gif" width="1"
							height="1" hspace="6">
						</td>
						<td class="mainContentWideTD" align="left" valign="top" rowspan="2">
							<!-- start main content area -->
							<div class="mainContentWideBox">
								<img border="0" src="/scweb/theme/images/USATODAY_Subhead_740x76.jpg"
									width="740" height="76" usemap="<%=useMap%>">
								<!-- bread crumbs if we want em-->
								<div class="navTrailLoc">
									<siteedit:navtrail start="[" end="]" target="home,parent,ancestor,self"
										separator="&gt;&gt;" spec="/scweb/theme/trail.jsp" />
								</div>
								<a name="navskip"><img border="0" src="/scweb/theme/img/JSF_1x1.gif"
									width="1" height="1" alt="Beginning of page content"> </a>
								<%-- tpl:put name="jspbodyarea" --%><hx:scriptCollector id="scriptCollectorDistributionDayDraw" preRender="#{pc_CurrentDayDraw.onPageLoadBegin}">
					<h:form styleClass="form" id="formDistributionDatePOForm">
					
										<TABLE border="0" cellpadding="0" cellspacing="1" width="605">
											<TBODY>
												<TR>
													<TD><h:messages styleClass="messages"
													id="messagesGlobalErrors" globalOnly="true" layout="list"
													style="font-size: 11pt"></h:messages></TD>
												<td align="right">
													<div class="mainContentHelpIcon"><hx:graphicImageEx
															styleClass="graphicImageEx" id="imageExPrintIcon"
															style="cursor: pointer;"
															value="/scweb/images/print.gif" width="20" height="21"
															border="0" alt="Send To Printer" title="Send To Printer" 
															onclick="return func_12(this,event);"></hx:graphicImageEx>
														<hx:graphicImageEx
																styleClass="graphicImageEx"
																style="cursor: pointer; margin-bottom: 2px; margin-left: 5px; margin-right: 5px; margin-top: 2px"
																id="imageExHelpIcon1"
																value="/scweb/images/question_mark.gif" width="16"
																height="16" hspace="0" title="Hotel Order Quantity Help"
																onclick="return func_9(this, event);"
																onmouseover="return func_10(this, event);"
																onmouseout="return func_11(this, event);"></hx:graphicImageEx>
													</div>												
												</td>

											</TR>
												<TR>
													<TD colspan="2">
													</TD>
												</TR>
												<TR>
													<TD class="columnClass3" colspan="2">
													
														<h:outputText styleClass="outputText_Med"
														id="textRestrictedAccessText"
														value="#{labels.bcdAutoAuthMsg}"
														rendered="#{userHandler.autoAuthOnly}"
														style="padding-left: 1px; "></h:outputText><hx:outputLinkEx
														styleClass="outputLinkEx" value="/scweb/index.usat"
														id="linkExRestrictedLogIn">
														<h:outputText id="textLoginLinkLabel"
															styleClass="outputText_Med"
															rendered="#{userHandler.autoAuthOnly}"
															value="#{labels.loginButtonLabel}"></h:outputText>
													</hx:outputLinkEx></TD></TR>
												<TR>
													<TD width="600" colspan="2"><h:outputText styleClass="messages"
														id="textUpdateErrorsMessage"
														value="#{labels.bcdSaveChangesFailed}"
														style="font-size: 11pt"
														rendered="#{blueChipDrawManagementHandler.showSaveFailedMessage}"></h:outputText></TD></TR>
												<TR>
													<TD nowrap="nowrap" colspan="2"><h:outputText
														styleClass="outputText messages"
														id="textUnsavedChangesMsg"
														value="#{labels.bcdUnsavedChangesMsg}"
														style="font-size: 11pt; line-height: 30px; vertical-align: top"
														rendered="#{blueChipDrawManagementHandler.hasUnsavedChanges}">
													</h:outputText></TD>
												
											</TR>
											</TBODY>
										</TABLE>


									<h:dataTable border="1" cellpadding="2" cellspacing="0"
										columnClasses="columnClass1" headerClass="headerClass"
										footerClass="footerClass" rowClasses="rowClass1, rowClass3"
										styleClass="dataTable, dataTableDailyPO"
										id="tableProductOrderData"
										value="#{blueChipDrawManagementHandler.blueChipLocations}"
										var="varblueChipLocations"
										rendered="#{blueChipDrawManagementHandler.hasProductOrdersForCurrentTimeline}"
										style="border-color: #e5e5e5">
										<h:column id="column2">

											<f:facet name="header">
												<h:outputText id="textHotelLocHeaderLabel"
													styleClass="outputText" value="Hotel Information"></h:outputText>
											</f:facet>
											<h:panelGrid styleClass="panelGrid" id="grid2"
												cellpadding="0" cellspacing="0" columns="1">

												<hx:jspPanel id="jspPanel3">
													<h:outputText styleClass="outputText_SM"
														id="textLocationMarketID"
														value="#{varblueChipLocations.owningMarketID}-"></h:outputText>
													<h:outputText styleClass="outputText_SM"
														id="textLocationIDStr"
														value="#{varblueChipLocations.locationID}"
														style="font-weight: bold"></h:outputText>
												</hx:jspPanel>

												<h:outputText styleClass="outputText" id="textLocationName"
													value="#{varblueChipLocations.locationName}"></h:outputText>

												<h:outputText styleClass="outputText"
													id="textLocationAddress1"
													value="#{varblueChipLocations.address1}"></h:outputText>
												<h:outputText styleClass="outputText" id="textLocationPhone"
													value="#{varblueChipLocations.phoneNumber}">
													<hx:convertMask mask="(###) ###-####" />
												</h:outputText>


											</h:panelGrid>
											<f:attribute value="top" name="valign" />
										</h:column>
										<h:column id="columnOrderInformation">
											<f:facet name="header">
												<h:outputText styleClass="outputText"
													value="Order Information"
													id="textHotelOrderInfoHeaderLabel"></h:outputText>
											</f:facet>
											<hx:jspPanel id="jspPanelInternalPOData">
												<h:dataTable border="0" cellpadding="2" cellspacing="0"
													columnClasses="columnClass3" headerClass="headerClass"
													footerClass="footerClass" rowClasses="rowClass2, rowClass1"
													styleClass="dataTable" id="table1"
													value="#{varblueChipLocations.productOrdersForNextDistributionDate}"
													var="varproductOrdersForNextDistributionDate" width="100%">
													<h:column id="column9">
															<h:outputText id="textProductName_A"
																value="#{varproductOrdersForNextDistributionDate.po.productName}"
																styleClass="outputText" style="margin-right: 3px" rendered="false">
															</h:outputText>
														<h:outputText styleClass="outputText"
															id="textProductOrderID_A"
															value="#{varproductOrdersForNextDistributionDate.po.productOrderID}"></h:outputText>

														<f:facet name="header">
															<h:outputText styleClass="outputText" value="POID"
																id="textPODescHeaderLabel"></h:outputText>
														</f:facet>
														<f:attribute value="false" name="nowrap" />
														<f:attribute value="30" name="width" />
														<f:attribute value="center" name="align" />
													</h:column>
													<h:column id="column8">
														<f:facet name="header">
															<h:outputText id="textPOPubHeaderLabel"
																styleClass="outputText" value="Publication"></h:outputText>
														</f:facet>
														<h:outputText styleClass="outputText"
															id="textPODescription"
															value="#{varproductOrdersForNextDistributionDate.po.productDescription}"
															style="font-weight: bold" title="#{varproductOrdersForNextDistributionDate.po.productDescription}"></h:outputText>
														<f:attribute value="false" name="nowrap" />
														<f:attribute value="120" name="width" />
														<f:attribute value="left" name="align" />
													</h:column>
													<hx:columnEx id="columnExCutOffTime" align="center"
														nowrap="true" width="140">
														<f:facet name="header">
															<h:outputText value="Cut Off" styleClass="outputText"
																id="text1" style="margin-left: 50px; text-align: center"></h:outputText>
														</f:facet>
														<h:outputText styleClass="outputText"
															id="textPOCutOffTime"
															value="#{varproductOrdersForNextDistributionDate.cutOffTimeStr}" escape="false">
														</h:outputText>
													</hx:columnEx>
													<h:column id="column10">
														<f:facet name="header">
															<h:outputText styleClass="outputText" value="Quantity"
																id="textPOQtyHeaderLabel"></h:outputText>
														</f:facet>
														<hx:jspPanel id="jspPanel1">
															<h:inputText
																styleClass="#{varproductOrdersForNextDistributionDate.drawCSSNoHighlight}"
																id="textPODraw" size="4" tabindex="1"
																value="#{varproductOrdersForNextDistributionDate.drawObj}"
																readonly="#{varproductOrdersForNextDistributionDate.allowDrawEditsObj}"
																disabled="#{varproductOrdersForNextDistributionDate.allowDrawEditsObj}"
																onblur="return blueChipDrawValidation(this, event, #{varproductOrdersForNextDistributionDate.po.maxDrawThreshold}, #{varproductOrdersForNextDistributionDate.po.minDrawThreshold}, #{varproductOrdersForNextDistributionDate.po.originalProductOrder.draw}, '#{varproductOrdersForNextDistributionDate.owningMarketPhoneFormatted}');"
																title="Quantity when page loaded: #{varproductOrdersForNextDistributionDate.originalDrawStr}"
																onkeypress="$(this).removeClassName('inputText_Error');"
																maxlength="5">
																<f:convertNumber integerOnly="true"
																	maxFractionDigits="0" type="number"
																	minFractionDigits="0" minIntegerDigits="0"
																	pattern="#####" maxIntegerDigits="5" />
															</h:inputText>
														</hx:jspPanel>
														<hx:graphicImageEx styleClass="graphicImageEx"
															id="imageExViewHist" value="/scweb/images/history.gif"
															onmouseout="return func_hideDrawHistory();"
															onmouseover="return func_showHistory(this, event, '#{varproductOrdersForNextDistributionDate.historyHTML}');"
															hspace="5" width="15" height="12"></hx:graphicImageEx>
														<f:attribute value="center" name="align" />
													</h:column>
													<h:column id="column11">
														<f:facet name="header">
															<h:outputText styleClass="outputText"
																value="Delivery Date" id="textPODeliveryDateHeaderLabel"></h:outputText>
														</f:facet>
														<h:outputText styleClass="outputText"
															id="textPODeliveryDate"
															value="#{varproductOrdersForNextDistributionDate.standardDate}">
															<f:convertDateTime pattern="EEE, MMM d, yyyy" />
														</h:outputText>
														<f:attribute value="true" name="nowrap" />
														<f:attribute value="center" name="align" />
													</h:column>
													<h:column id="column12">
														<f:facet name="header">
														</f:facet>
														<h:panelGrid styleClass="panelGrid" id="gridActionsGrid"
															cellspacing="1" columns="3">

															<h:commandLink styleClass="commandLink"
																id="linkEditWeekLink"
																action="#{pc_CurrentDayDraw.doLinkWeekViewAction}"
																tabindex="800" rendered="#{!  userHandler.autoAuthOnly}">
																<h:outputText id="textEditWeekLabel"
																	styleClass="outputText" value="Edit Week"></h:outputText>
																<f:param name="locationID"
																	value="#{varblueChipLocations.locationID}"></f:param>
																<f:param name="poid"
																	value="#{varproductOrdersForNextDistributionDate.po.productOrderID}"></f:param>
																<f:param name="weekKey"
																	value="#{varproductOrdersForNextDistributionDate.po.owningWeek.hashKey}"></f:param>
															</h:commandLink>
															<h:panelGrid styleClass="panelGrid"
																id="gridSeperateorGrid1" columns="1" bgcolor="#8fa4b8"
																cellpadding="0" cellspacing="0">
																<hx:outputSeparator styleClass="outputSeparatorV2"
																	id="separatorLink1" width="1" color="#8fa4b8"
																	noshade="true"
																	rendered="#{!  userHandler.autoAuthOnly}"></hx:outputSeparator>
															</h:panelGrid>
															<h:commandLink styleClass="commandLink"
																id="viewHistoryLink" tabindex="801"
																action="#{pc_CurrentDayDraw.doViewHistoryLinkAction}">
																<h:outputText id="textViewHistoryLabel"
																	styleClass="outputText" value="History"></h:outputText>
																<f:param name="locationID"
																	value="#{varblueChipLocations.locationID}"></f:param>
																<f:param name="poid"
																	value="#{varproductOrdersForNextDistributionDate.po.productOrderID}"></f:param>
																<f:param name="weekKey"
																	value="#{varproductOrdersForNextDistributionDate.po.owningWeek.hashKey}"></f:param>
															</h:commandLink>
														</h:panelGrid>

														<f:attribute value="true" name="nowrap" />
													</h:column>
												</h:dataTable>
											</hx:jspPanel>
											<f:attribute value="right" name="align" />
											<f:attribute value="top" name="valign" />
											<f:attribute value="true" name="nowrap" />
										</h:column>
										<f:facet name="footer">
											<hx:jspPanel id="jspPanel4">
												<table width="100%">
													<tr>
														<td align="left"><hx:graphicImageEx
															styleClass="graphicImageEx" id="imageEx1"
															value="/scweb/images/history.gif" width="15" height="12"></hx:graphicImageEx><h:outputText
															styleClass="outputText" id="textHistoryKey"
															value="#{labels.bcdHistoryKeyText}"></h:outputText></td>
														<td align="right"><hx:commandExButton type="submit"
															value="#{labels.bcdUpdateDrawButtonLabel}"
															styleClass="commandExButton submitChangesStyle"
															id="buttonSaveDrawEdits"
															action="#{pc_CurrentDayDraw.doButtonSaveDrawEditsAction}"
															tabindex="9"
															onclick="return func_SaveClicked(this, event);"
															confirm="#{labels.bcdConfirmSaveMsg}"
															onmouseover="this.focus();"></hx:commandExButton> <hx:commandExButton
															type="submit"
															value="#{labels.bcdCancelChangesButtonLabel}"
															styleClass="commandExButton cancelChangesStyle"
															id="buttonCancelChanges"
															action="#{pc_CurrentDayDraw.doButtonCancelChangesAction}"
															confirm="#{labels.bcdConfirmDiscardChangesMsg}"
															onclick="return func_discardClicked(this, event);"
															style="margin-left: 5px"></hx:commandExButton></td>
													</tr>
												</table>
											</hx:jspPanel>
										</f:facet>
										<f:facet name="header">
											<hx:panelBox styleClass="panelBox" id="box1" width="100%">
												<hx:panelLayout styleClass="panelLayout" id="layout1"
													width="100%">
													<f:facet name="body"></f:facet>
													<f:facet name="left">
													</f:facet>
													<f:facet name="right">
														
													</f:facet>
													<f:facet name="bottom"></f:facet>
													<f:facet name="top">
														<hx:jspPanel id="jspPanelCountDownMsg">
															<DIV id="countdownMSG" class="outputTextAlt" align="left"
																style="display: none;">You have a limited time to
															perform updates before the data will require a refresh.</DIV>
														</hx:jspPanel>
													</f:facet>
												</hx:panelLayout>
											</hx:panelBox>
										</f:facet>
									</h:dataTable>



									<h:outputText styleClass="outputText_LG"
											id="textNoAccountsAssigned"
											value="#{labels.bcdNoPOForTimeline}"
											rendered="#{blueChipDrawManagementHandler.hasNoProductOrdersForCurrentTimeline}"></h:outputText>
										<BR>
									<h:outputFormat styleClass="outputText message"
										id="formatDisclaimer" value="#{labels.bcdCutOffTimeMessage}"
										rendered="#{blueChipDrawManagementHandler.hasProductOrdersForCurrentTimeline}"></h:outputFormat>
									<BR>
										
										

										<BR>
										<DIV id="DrawHistoryBox" class="boxBCDrawHist" align="center" 
				style="display: none;z-index:1000;border-bottom: 1px solid #90a0b8;border-top: 1px solid #90a0b8;border-left: 1px solid #90a0b8;border-right: 1px solid #90a0b8;width: 150px;background: white; background-color: white;position: absolute;">
				</DIV>
										<BR>
										<BR><BR>
										&nbsp;&nbsp;&nbsp;&nbsp;<BR>
										<h:inputHidden
											value="#{blueChipDrawManagementHandler.minutesToStaleData}"
											id="minutesToStaleData"></h:inputHidden>
										
										<hx:inputHelperKeybind key="Enter" id="inputHelperKeybind1"
											targetAction="nexttab" />
									</h:form></hx:scriptCollector><%-- /tpl:put --%>
							</div> <br> <!-- end main content area -->
						</td>
					</tr>
					<tr>
						<td class="leftNavTD" align="left" valign="bottom">
							<div class="bottomNavAreaImage"></div>
						</td>
						<td></td>

					</tr>
					<tr>
						<td align="left" valign="top" colspan="3" height="20"><img
							class="footerImage" src="/scweb/theme/images/USATODAY_Footer_940x12.jpg">
							<!-- 							class="footerImage" src="/scweb/theme/images/800x20_art2.jpg"> -->

						</td>
					</tr>
					<tr>
						<td align="center" valign="top"><div class="footer"></div>
						</td>
						<td></td>
						<td class="mainContentWideTD" align="center" valign="top">
							<div class="footer">
								<table border="0" cellspacing="0" cellpadding="0" width="675">
									<tbody>
										<tr>
											<td class="mainContentWideTD" align="center">
												<!-- someting in this table drastically impacts how this template works so leave it in for now -->
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top" colspan="3">
							<div class="footer">
								<table border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td><img src="/scweb/theme/1x1.gif" hspace="15">
											</td>
											<!-- 											<td class="mainContentWideTD" align="center">&copy; Copyright -->
											<!-- 												2007 <a href="http://www.usatoday.com/marketing/credit.htm" -->
											<!-- 												target="_blank">USA TODAY</a>, a division of <a -->
											<!-- 												href="http://www.gannett.com" target="_blank">Gannett Co.</a> Inc. <a -->
											<!-- 												href="/scweb/legal/privacy.html" target="_blank">Privacy Policy</a>, -->
											<!-- 												By using this service, you accept our <a -->
											<!-- 												href="/scweb/legal/service.html" target="_blank">Terms of -->
											<!-- 													Service.</a></td> -->
											<td><br>&copy; <script>
												document.write(getCurrentYear());
											</script> USA TODAY, a <a href="http://www.gannett.com">Gannett Company</a> <a
												href= "/scweb/legal/privacy.html" target="_blank">| Privacy
													Policy</a>| By using this service, you accept
												our <a href="/scweb/legal/service.html" target="_blank">Terms of
													Service</a>.</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:scriptCollector>
	</body>
</f:view>
<!--  following link put here because of a bug in faces that seems to prevent styles from working properly -->
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
</html>
<%-- /tpl:insert --%>
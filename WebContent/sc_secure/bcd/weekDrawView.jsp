<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/sc_secure/bcd/WeekDrawView.java" --%><%-- /jsf:pagecode --%>
<%-- tpl:insert page="/theme/JSP-C-blue_mod.jtpl" --%><!DOCTYPE HTML>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%
	OmnitureTrackingCode otc = (OmnitureTrackingCode) session
			.getAttribute("omnitureTracking");
	if (otc == null) {
		otc = new OmnitureTrackingCode();
	}
	String omnitureP1 = otc.getTrackingCodePart1();

	// following work around due to bug in RAD
	String useMap = "#map_800x125_top";
	String headURL = "/scweb/index.usat";
%>
<!-- NEW path -->


<link rel="stylesheet" href="/scweb/theme/Master.css" type="text/css">
<link href="/scweb/theme/C_master_blue.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/scweb/theme/stylesheet.css"
	title="Style">

<script src="/scweb/scripts/windowutility.js"></script>
<script language="JavaScript">
	// method called on page load
	function _initPage() {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this

		if (typeof window.initPage == "function") {
			window.initPage();
		}

	}

	// method called on page unload
	function _unloadPage(thisObj, thisEvent) {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this
		if (typeof window.unloadPage == "function") {
			window.unloadPage(thisObj, thisEvent);
		}

	}

	// method called on page unload
	function _onbeforeunloadPage(thisObj, thisEvent) {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this
		if (typeof window.onbeforeunloadPage == "function") {
			window.onbeforeunloadPage(thisObj, thisEvent);
		}

	}
</script>

<%=omnitureP1%>

<%-- tpl:put name="headarea" --%>
				

			<TITLE>USA TODAY Hotel Draw - Order Detail View</TITLE>
					<LINK rel="stylesheet" type="text/css"
						href="/scweb/theme/stylesheet.css" title="Style">
<script type="text/javascript" src="/scweb/scripts/scriptaculous/prototype.js"></script>
<script src="/scweb/scripts/commonValidations.js"></script>
<script type="text/javascript" src="/scweb/scripts/blueChipDrawMngmt.js"></script>
<script language="javascript">
s.pageName = "Blue Chip Hotel Draw PO Week Detail View";
s.channel = "Blue Chip Draw";

Event.observe(window, 'load', function() {

	var inputArray = $$('input.drawInputText');
	
	if (inputArray.length > 0) {
		editableDrawFields = inputArray;
		inputArray[0].focus();
		inputArray[0].select();
		firstField = inputArray[0];
	}

	countDownMinutes = parseInt($('formWeeklyDrawEdits:minutesToStaleData').value, 10);

	updateBlueChipCountdown();
		
}
);

function func_9(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
helpScreen('/scweb/sc_secure/bcd/bcdHelp.usat'); 
if (helpWindow) {
	helpWindow.focus();
}
return false;
}
function func_10(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
window.status='Click for Help On This Page';

}
function func_11(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
window.status='';
}
function func_12(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	window.print();
	return false;
}
</script>						
				<%-- /tpl:put --%>
<script language="javascript">
	//var logoutMsg = 'Please close all Draw Management windows before logging out';
	function cleanupWindows() {
		// switch to:
		if (rdlWindow != null) {
			//if (rdlWindow.closed == false) {
			if (rdlWindow.open && !rdlWindow.closed) {
				rdlWindow.setForceClose();
				rdlWindow.close();
			}

		}

		if (weeklyRDLWindow != null) {
			if (weeklyRDLWindow.open && !weeklyRDLWindow.closed) {
				weeklyRDLWindow.setForceClose();
				weeklyRDLWindow.close();
			}
		}

		if (helpWindow != null) {
			if (helpWindow.open && !helpWindow.closed) {
				helpWindow.close();
			}
		}

		if (printWindow != null) {
			if (printWindow.open && !printWindow.closed) {
				printWindow.close();
			}
		}

		return true;
	}

	function okToLogout() {
		var alert = false;
		if (rdlWindow != null) {
			if (rdlWindow.open && !rdlWindow.closed) {
				alert = true;
			}
		}

		if (weeklyRDLWindow != null) {
			if (weeklyRDLWindow.open && !weeklyRDLWindow.closed) {
				alert = true;
			}
		}

		if (alert) {
			setTimeout(
					'alert("You must close all Draw Management windows before logging out.")',
					100);
			return false;
		} else {
			cleanupWindows();
			return true;
		}
	}

	function func_logoutOK(thisObj, thisEvent) {
		//use 'thisObj' to refer directly to this component instead of keyword 'this'
		//use 'thisEvent' to refer to the event generated instead of keyword 'event'
		return okToLogout();
	}
	function getCurrentYear() {
		today = new Date();
		y0 = today.getFullYear();
		return y0;
	}
</script>
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="charset=ISO-8859-1">
</head>
<f:view locale="#{userLocaleHandler.locale}">
	<f:loadBundle basename="resourcesFile" var="labels" />
	<body onLoad="_initPage();" onUnload="_unloadPage(this, event);"
		onbeforeunload="_onbeforeunloadPage(this, event);">
		<hx:scriptCollector id="templateScriptCollector">


			<!-- start header area -->
			<div class="topAreaBox1"></div>
			<div class="topAreaLogo">
				<!-- 				<img border="0" src="/scweb/theme/images/800x125_top.jpg" -->
				<%-- 					width="800" height="125" usemap="<%=useMap %>"> --%>
				<img border="0" src="/scweb/theme/images/USATODAY_Header_940x145.jpg"
					width="940" height="145" usemap="<%=useMap%>">
				<map name="map_800x125_top">
					<area shape="rect" target="_self" href="<%=headURL%>"
						coords="19,17,268,112">
					<area shape="default" nohref>
				</map>
			</div>
			<div class="topAreaLogoLine">
				<img border="0" src="/scweb/theme/images/800x20_art2.jpg" height="20"
					width="800">
			</div>
			<!-- end header area -->

			<!-- start header navigation bar -->
			<div class="topNavBk"></div>${omnitureTracking.trackingCodePart2}<div
				class="topNav"></div>
			<div class="topAreaDatePlacement">
				<fmt:formatDate value="${pc_JSPCblue_mod.now}" dateStyle="full" />
			</div>
			<h:outputText styleClass="globalMessageDivStyle" id="textGlobalMessageText"
				value="#{globalMessage.message}"
				rendered='#{ (globalMessage.showMessage) and ((globalMessage.showToWhom eq "all") or ((globalMessage.showToWhom eq "hotel") and userHandler.canWorkWithBlueChipDraw ) or ((globalMessage.showToWhom eq "contractor") and userHandler.canWorkWithRoutes) )}'></h:outputText>
			<!-- end header navigation bar bar -->

			<!-- start left-hand navigation -->
			<table class="mainBox" border="0" cellpadding="0" width="100%" height="87%"
				cellspacing="0">
				<tbody>
					<tr>
						<td class="leftNavTD" align="left" valign="top">
							<div class="leftNavBox">
								<h:form styleClass="form" id="formLogout">
									<%-- tpl:put name="LeftNavTopMostJSPContentArea" --%><%-- /tpl:put --%>
									<div id="logoutLinkDiv" class="noDisplay" style="${userHandler.divCss}">
										<h:outputFormat styleClass="outputFormat" id="formatWelcomeMessage"
											value="Welcome, {0}">
											<f:param name="displayName" value="#{userHandler.displayName}"></f:param>
										</h:outputFormat>
										<br>
										<hx:commandExButton type="submit" value="#{labels.logoutLink}"
											styleClass="commandExButtonSmall" id="buttonDoLogoutBtn"
											title="Logout" alt="Logout"
											action="#{pc_JSPCblue_mod.doButtonDoLogoutBtnAction}"
											onclick="return func_logoutOK(this, event);"></hx:commandExButton>
										<hr width="95%" align="left">
									</div>
									<siteedit:navbar spec="/scweb/theme/nav_vertical_tree_left.jsp"
										targetlevel="1-5" onlychildren="true" navclass="leftNav"
										target="topchildren" />
									<br>
									<br>
									<br>
									<!-- If we add internatiolization
								<h:selectOneMenu styleClass="selectOneMenu" id="menui18nPref" value="#{userLocaleHandler.localeStr}">
									<f:selectItem itemValue="en" itemLabel="English" />
									<f:selectItem itemValue="es" itemLabel="Spanish" />
								</h:selectOneMenu><hx:commandExButton type="submit" value="Go"
									styleClass="commandExButton" id="buttonUpdateLangPref"
									action="#{pc_JSPCblue_mod.doButtonUpdateLangPrefAction}"></hx:commandExButton> -->
									<br>
									<hx:inputHelperKeybind key="Enter" id="inputHelperKeybind1"
										targetAction="nothing" />
								</h:form>
								<br>
								<hr width="95%" align="left">
								<b>Related Links:</b><br> <a href="http://usatoday.com"
									target="_blank">USA TODAY.com</a><br> <a
									href="https://service.usatoday.com/welcome.jsp" target="_blank">USA
									TODAY Home Delivery</a><br> <a
									href="https://service.usatoday.com/shop" target="_blank">USA TODAY
									Past Issues</a><br> <br> <br>

							</div>
						</td>
						<!-- end left-hand navigation -->
						<td><img border="0" src="/scweb/theme/img/JSF_1x1.gif" width="1"
							height="1" hspace="6">
						</td>
						<td class="mainContentWideTD" align="left" valign="top" rowspan="2">
							<!-- start main content area -->
							<div class="mainContentWideBox">
								<img border="0" src="/scweb/theme/images/USATODAY_Subhead_740x76.jpg"
									width="740" height="76" usemap="<%=useMap%>">
								<!-- bread crumbs if we want em-->
								<div class="navTrailLoc">
									<siteedit:navtrail start="[" end="]" target="home,parent,ancestor,self"
										separator="&gt;&gt;" spec="/scweb/theme/trail.jsp" />
								</div>
								<a name="navskip"><img border="0" src="/scweb/theme/img/JSF_1x1.gif"
									width="1" height="1" alt="Beginning of page content"> </a>
								<%-- tpl:put name="jspbodyarea" --%>
					<hx:scriptCollector id="scriptCollectorBlueChipWeeklyView" preRender="#{pc_WeekDrawView.onPageLoadBegin}"><h:form styleClass="form" id="formWeeklyDrawEdits">
					<table border="0" cellpadding="0" cellspacing="0" width="600">
						<tr>
							<td align="right" width="100%">
								<div class="mainContentHelpIcon">
									<hx:graphicImageEx
											styleClass="graphicImageEx" id="imageExPrintIcon"
											style="cursor: pointer;"
											value="/scweb/images/print.gif" width="20" height="21"
											border="0" alt="Send To Printer" title="Send To Printer" 
											onclick="return func_12(this,event);"></hx:graphicImageEx>
									<hx:graphicImageEx
											styleClass="graphicImageEx"
											style="cursor: pointer; margin-bottom: 2px; margin-left: 5px; margin-right: 5px; margin-top: 2px"
											id="imageExHelpIcon1"
											value="/scweb/images/question_mark.gif" width="16"
											height="16" hspace="0" title="Hotel Order Quantity Help"
											onclick="return func_9(this, event);"
											onmouseover="return func_10(this, event);"
											onmouseout="return func_11(this, event);"></hx:graphicImageEx>
								</div>										
							</td>
						</tr>
					</table>
					
					
										<h:panelGrid styleClass="panelGrid" id="gridTopGrid" cellpadding="2" cellspacing="2">
											<h:outputText styleClass="outputText messages"
												id="textUnsavedMsg" value="#{labels.bcdUnsavedChangesMsg}"
												style="font-size: 11pt; line-height: 25px; vertical-align: top"
												rendered="#{blueChipDrawManagementHandler.hasUnsavedChanges}">
											</h:outputText>
										</h:panelGrid>
										<hx:panelLayout styleClass="panelLayout" id="layoutMainLayout">
											<f:facet name="body">
												<hx:jspPanel id="jspPanelMainBody">
													<TABLE border="0" cellpadding="0" cellspacing="1"
														class="dataTable dataTableDailyPO">
														<TBODY>
															<tr>
																<td  class="headerClass" scope="col" nowrap="nowrap"="nowrap="nowrap""	colspan="3">
																	<table width="575" border="0" cellpadding="0" cellspacing="0">
																		<tr>
																			<td align="left">
																				<hx:jspPanel id="jspPanelCountDownMsg">
																			<DIV id="countdownMSG" class="outputTextAlt"
																				align="left" style="display: none;">You have a
																			limited time to perform updates before the data will
																			require a refresh.</DIV>
																		</hx:jspPanel>
																			</td>
																		</tr>
																		<tr>
																			<td align="right" width="100%" nowrap="nowrap">
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
															<TR>
																<TH class="headerClass" scope="col" width="200" nowrap="nowrap"><h:outputText
																	styleClass="outputText" id="textLocationHeaderLabel"
																	value="Location Information"></h:outputText></TH>
																<TH colspan="2" class="headerClass" scope="col" nowrap="nowrap"><h:outputText
																	styleClass="outputText" id="textPOWeekHeaderLabel"
																	value="Product:  #{blueChipPOForWeek.wpo.productDescription}, Order ID: #{blueChipPOForWeek.wpo.productOrderID}"></h:outputText></TH>
															</TR>
															<TR>
																<TD valign="top" nowrap="nowrap"><h:outputText
																	styleClass="outputText_SM" id="textMarketID"
																	value="#{blueChipPOForWeek.wpo.owningLocation.owningMarketID}-"></h:outputText><h:outputText
																	styleClass="outputText_SM" id="textLocID"
																	value="#{blueChipPOForWeek.wpo.owningLocation.locationID}" style="font-weight: bold"></h:outputText><BR>
																<h:outputText styleClass="outputText" id="text15"
																	value="#{blueChipPOForWeek.wpo.owningLocation.locationName}"></h:outputText><BR>
																<h:outputText styleClass="outputText" id="text1"
																	value="#{blueChipPOForWeek.wpo.owningLocation.address1}"></h:outputText><BR>
																<h:outputText styleClass="outputText" id="text2"
																	value="#{blueChipPOForWeek.wpo.owningLocation.phoneNumber}">
																	<hx:convertMask mask="(###) ###-####"/>
																</h:outputText></TD>
																<TD align="center" nowrap="nowrap" colspan="2"
																	class="columnClass3">

																<TABLE border="0" cellpadding="1" cellspacing="1"
																	width="100%">
																	<TBODY>
																		<tr style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: gray">
																			<th>Delivery Date</th>
																			<th>Cut Off</th>
																			<th>Quantity</th>
																		</tr>
																		<TR class="rowClass1">
																			<TD width="225" align="center" nowrap="nowrap"><h:outputText
																				styleClass="outputText" id="textMondayDate"
																				value="#{blueChipPOForWeek.mondayPO.standardDate}">
																			<f:convertDateTime pattern="EEE, MMM d, yyyy" />
																		</h:outputText></TD>
																			<td align="center"><h:outputText
																			styleClass="outputText" id="text3"
																			value="#{blueChipPOForWeek.mondayPO.cutOffTimeStr}"
																			escape="false">
																		</h:outputText></td>
																			<TD width="45" align="right"><h:inputText
																				styleClass="#{blueChipPOForWeek.mondayPO.drawCSS}"
																				id="textMondayDraw"
																				value="#{blueChipPOForWeek.mondayPO.drawObj}"
																				size="4"
																				readonly="#{blueChipPOForWeek.mondayPO.allowDrawEditsObj}"
																				disabled="#{blueChipPOForWeek.mondayPO.allowDrawEditsObj}"
																				title="Quantity when page loaded: #{blueChipPOForWeek.mondayPO.originalDrawStr}"
																				tabindex="1"
																				maxlength="5"
																				onblur="return blueChipDrawValidation(this, event, #{blueChipPOForWeek.mondayPO.po.maxDrawThreshold}, #{blueChipPOForWeek.mondayPO.po.minDrawThreshold}, #{blueChipPOForWeek.mondayPO.po.originalProductOrder.draw}, '#{blueChipPOForWeek.mondayPO.owningMarketPhoneFormatted}');"
																				onkeypress="$(this).removeClassName('inputText_Error');">
																				<f:convertNumber integerOnly="true" maxFractionDigits="0" type="number" pattern="#####" maxIntegerDigits="5"/>
																			</h:inputText></TD>
																		</TR>
																		<TR class="rowClass2">
																			<TD width="225" align="center" nowrap="nowrap"><h:outputText
																				styleClass="outputText" id="textTuesdayDateLabel"
																				value="#{blueChipPOForWeek.tuesdayPO.standardDate}">
																			<f:convertDateTime pattern="EEE, MMM d, yyyy" />
																		</h:outputText></TD>
																			<td align="center"><h:outputText
																			styleClass="outputText" id="text4"
																			value="#{blueChipPOForWeek.tuesdayPO.cutOffTimeStr}"
																			escape="false">
																		</h:outputText></td>
																			<TD width="45" align="right"><h:inputText
																				styleClass="#{blueChipPOForWeek.tuesdayPO.drawCSS}"
																				id="textTuesdayDraw"
																				value="#{blueChipPOForWeek.tuesdayPO.drawObj}"
																				size="4"
																				readonly="#{blueChipPOForWeek.tuesdayPO.allowDrawEditsObj}"
																				disabled="#{blueChipPOForWeek.tuesdayPO.allowDrawEditsObj}"
																				tabindex="2"
																				maxlength="5"
																				title="Quantity when page loaded: #{blueChipPOForWeek.tuesdayPO.originalDrawStr}"
																				onblur="return blueChipDrawValidation(this, event, #{blueChipPOForWeek.tuesdayPO.po.maxDrawThreshold}, #{blueChipPOForWeek.tuesdayPO.po.minDrawThreshold}, #{blueChipPOForWeek.tuesdayPO.po.originalProductOrder.draw}, '#{blueChipPOForWeek.tuesdayPO.owningMarketPhoneFormatted}');"
																				onkeypress="$(this).removeClassName('inputText_Error');">
																				<f:convertNumber integerOnly="true" maxFractionDigits="0" type="number" pattern="#####" maxIntegerDigits="5"/>
																			</h:inputText></TD>
																		</TR>
																		<TR class="rowClass1">
																			<TD width="225" align="center" nowrap="nowrap"><h:outputText
																				styleClass="outputText" id="textWednesdayDateLabel"
																				value="#{blueChipPOForWeek.wednesdayPO.standardDate}">
																			<f:convertDateTime pattern="EEE, MMM d, yyyy" />
																		</h:outputText></TD>
																			<td align="center"><h:outputText
																			styleClass="outputText" id="text6"
																			value="#{blueChipPOForWeek.wednesdayPO.cutOffTimeStr}"
																			escape="false">
																		</h:outputText></td>
																			<TD width="45" align="right"><h:inputText
																				styleClass="#{blueChipPOForWeek.wednesdayPO.drawCSS}"
																				id="textWednesdayDraw"
																				value="#{blueChipPOForWeek.wednesdayPO.drawObj}"
																				size="4"
																				readonly="#{blueChipPOForWeek.wednesdayPO.allowDrawEditsObj}"
																				disabled="#{blueChipPOForWeek.wednesdayPO.allowDrawEditsObj}"
																				tabindex="3"
																				maxlength="5"
																				title="Quantity when page loaded: #{blueChipPOForWeek.wednesdayPO.originalDrawStr}"
																				onkeypress="$(this).removeClassName('inputText_Error');"
																				onblur="return blueChipDrawValidation(this, event, #{blueChipPOForWeek.wednesdayPO.po.maxDrawThreshold}, #{blueChipPOForWeek.wednesdayPO.po.minDrawThreshold}, #{blueChipPOForWeek.wednesdayPO.po.originalProductOrder.draw}, '#{blueChipPOForWeek.wednesdayPO.owningMarketPhoneFormatted}');">
																				<f:convertNumber integerOnly="true" maxFractionDigits="0" type="number" pattern="#####" maxIntegerDigits="5"/>
																			</h:inputText></TD>
																		</TR>
																		<TR class="rowClass2">
																			<TD width="225" align="center" nowrap="nowrap"><h:outputText
																				styleClass="outputText" id="textThursdayDateLabel"
																				value="#{blueChipPOForWeek.thursdayPO.standardDate}">
																			<f:convertDateTime pattern="EEE, MMM d, yyyy" />
																		</h:outputText></TD>
																			<td align="center"><h:outputText
																			styleClass="outputText" id="text7"
																			value="#{blueChipPOForWeek.thursdayPO.cutOffTimeStr}"
																			escape="false">
																		</h:outputText></td>
																			<TD width="45" align="right"><h:inputText
																				styleClass="#{blueChipPOForWeek.thursdayPO.drawCSS}"
																				id="textThursdayDraw"
																				value="#{blueChipPOForWeek.thursdayPO.drawObj}"
																				size="4"
																				readonly="#{blueChipPOForWeek.thursdayPO.allowDrawEditsObj}"
																				disabled="#{blueChipPOForWeek.thursdayPO.allowDrawEditsObj}"
																				tabindex="4"
																				maxlength="5"
																				title="Quantity when page loaded: #{blueChipPOForWeek.thursdayPO.originalDrawStr}"
																				onkeypress="$(this).removeClassName('inputText_Error');"
																				onblur="return blueChipDrawValidation(this, event, #{blueChipPOForWeek.thursdayPO.po.maxDrawThreshold}, #{blueChipPOForWeek.thursdayPO.po.minDrawThreshold}, #{blueChipPOForWeek.thursdayPO.po.originalProductOrder.draw}, '#{blueChipPOForWeek.thursdayPO.owningMarketPhoneFormatted}');">
																				<f:convertNumber integerOnly="true" maxFractionDigits="0" type="number" pattern="#####" maxIntegerDigits="5"/>
																			</h:inputText></TD>
																		</TR>
																		<TR class="rowClass1">
																			<TD width="225" align="center" nowrap="nowrap"><h:outputText
																				styleClass="outputText" id="textFridayDateLabel"
																				value="#{blueChipPOForWeek.fridayPO.standardDate}">
																			<f:convertDateTime pattern="EEE, MMM d, yyyy" />
																		</h:outputText></TD>
																			<td align="center"><h:outputText
																			styleClass="outputText" id="text8"
																			value="#{blueChipPOForWeek.fridayPO.cutOffTimeStr}"
																			escape="false">
																		</h:outputText></td>
																			<TD width="45" align="right"><h:inputText
																				styleClass="#{blueChipPOForWeek.fridayPO.drawCSS}"
																				id="textFridayDraw"
																				value="#{blueChipPOForWeek.fridayPO.drawObj}"
																				size="4"
																				readonly="#{blueChipPOForWeek.fridayPO.allowDrawEditsObj}"
																				disabled="#{blueChipPOForWeek.fridayPO.allowDrawEditsObj}"
																				tabindex="5"
																				maxlength="5"
																				title="Quantity when page loaded: #{blueChipPOForWeek.fridayPO.originalDrawStr}"
																				onblur="return blueChipDrawValidation(this, event, #{blueChipPOForWeek.fridayPO.po.maxDrawThreshold}, #{blueChipPOForWeek.fridayPO.po.minDrawThreshold}, #{blueChipPOForWeek.fridayPO.po.originalProductOrder.draw}, '#{blueChipPOForWeek.fridayPO.owningMarketPhoneFormatted}');"
																				onkeypress="$(this).removeClassName('inputText_Error');">
																				<f:convertNumber integerOnly="true" maxFractionDigits="0" type="number" pattern="#####" maxIntegerDigits="5"/>
																			</h:inputText></TD>
																		</TR>
																		<TR class="rowClass2">
																			<TD width="225" align="center" nowrap="nowrap"><h:outputText
																				styleClass="outputText" id="textSaturdayDateLabel"
																				value="#{blueChipPOForWeek.saturdayPO.standardDate}">
																			<f:convertDateTime pattern="EEE, MMM d, yyyy" />
																		</h:outputText></TD>
																			<td align="center"><h:outputText
																			styleClass="outputText" id="text9"
																			value="#{blueChipPOForWeek.saturdayPO.cutOffTimeStr}"
																			escape="false">
																		</h:outputText></td>
																			<TD width="45" align="right"><h:inputText
																				styleClass="#{blueChipPOForWeek.saturdayPO.drawCSS}"
																				id="textSaturdayDraw"
																				value="#{blueChipPOForWeek.saturdayPO.drawObj}"
																				size="4"
																				readonly="#{blueChipPOForWeek.saturdayPO.allowDrawEditsObj}"
																				disabled="#{blueChipPOForWeek.saturdayPO.allowDrawEditsObj}"
																				tabindex="6"
																				maxlength="5"
																				title="Quantity when page loaded: #{blueChipPOForWeek.saturdayPO.originalDrawStr}"
																				onblur="return blueChipDrawValidation(this, event, #{blueChipPOForWeek.saturdayPO.po.maxDrawThreshold}, #{blueChipPOForWeek.saturdayPO.po.minDrawThreshold}, #{blueChipPOForWeek.saturdayPO.po.originalProductOrder.draw}, '#{blueChipPOForWeek.saturdayPO.owningMarketPhoneFormatted}');"
																				onkeypress="$(this).removeClassName('inputText_Error');">
																				<f:convertNumber integerOnly="true" maxFractionDigits="0" type="number" pattern="#####" maxIntegerDigits="5"/>
																			</h:inputText></TD>
																		</TR>
																		<TR class="rowClass1">
																			<TD width="225" align="center" nowrap="nowrap"><h:outputText
																				styleClass="outputText" id="textSundayDateLabel"
																				value="#{blueChipPOForWeek.sundayPO.standardDate}">
																			<f:convertDateTime pattern="EEE, MMM d, yyyy" />
																		</h:outputText></TD>
																			<td align="center"><h:outputText
																			styleClass="outputText" id="text10"
																			value="#{blueChipPOForWeek.sundayPO.cutOffTimeStr}"
																			escape="false">
																		</h:outputText></td>
																			<TD width="45" align="right"><h:inputText
																				styleClass="#{blueChipPOForWeek.sundayPO.drawCSS}"
																				id="textSundayDraw"
																				value="#{blueChipPOForWeek.sundayPO.drawObj}"
																				size="4"
																				readonly="#{blueChipPOForWeek.sundayPO.allowDrawEditsObj}"
																				disabled="#{blueChipPOForWeek.sundayPO.allowDrawEditsObj}"
																				tabindex="7"
																				title="Quantity when page loaded: #{blueChipPOForWeek.sundayPO.originalDrawStr}"
																				onblur="return blueChipDrawValidation(this, event, #{blueChipPOForWeek.sundayPO.po.maxDrawThreshold}, #{blueChipPOForWeek.sundayPO.po.minDrawThreshold}, #{blueChipPOForWeek.sundayPO.po.originalProductOrder.draw}, '#{blueChipPOForWeek.sundayPO.owningMarketPhoneFormatted}');"
																				onkeypress="$(this).removeClassName('inputText_Error');"
																				maxlength="5">
																				<f:convertNumber integerOnly="true"
																					maxFractionDigits="0" type="number" pattern="#####"
																					maxIntegerDigits="5" />
																			</h:inputText></TD>
																		</TR>
																	</TBODY>
																</TABLE>
																</TD>
															</TR>
															<TR>
																<TH colspan="3" class="headerClass" scope="col">
																	<table border="0" width="100%" cellspacing="0">
																		<tr>
																			<td align="left"><h:commandLink
																			styleClass="commandLink" id="linkPrevWeek"
																			action="#{pc_WeekDrawView.doLinkPrevWeekAction}"
																			rendered="#{blueChipPOForWeek.hasPrevWeek}" onmouseover="this.focus();">
																			<h:outputText id="textPrevWeekLabel"
																				styleClass="outputText"
																				value="#{labels.bcdPreviousWeek}"></h:outputText>
																		</h:commandLink></td>
																			<td align="right"><h:commandLink
																			styleClass="commandLink" id="linkNextWeek"
																			action="#{pc_WeekDrawView.doLinkNextWeekAction1}"
																			rendered="#{blueChipPOForWeek.hasNextWeek}" onmouseover="this.focus();">
																			<h:outputText id="text5" styleClass="outputText"
																				value="#{labels.bcdNextWeek}"></h:outputText>
																		</h:commandLink></td>
																		</tr>
																	</table>
																</TH>
															</TR>
														</TBODY>
													</TABLE>
												</hx:jspPanel>
											</f:facet>
											<f:facet name="left"></f:facet>
											<f:facet name="right">
											</f:facet>
											<f:facet name="bottom">
												<hx:panelLayout styleClass="panelLayout"
													id="layoutBottomeLayout" width="575" align="center">
													<f:facet name="body"></f:facet>
													<f:facet name="left">

														<h:commandLink styleClass="commandLink"
															id="linkBackToDistDay"
															action="#{pc_WeekDrawView.doLinkBackToDistDayAction}" onmouseover="this.focus();">
															<h:outputText id="textBackToDistDayLabel"
																styleClass="outputText" value="#{labels.bcdBackToDaily}"
																style="padding-right: 8px"></h:outputText>
														</h:commandLink>
													</f:facet>
													<f:facet name="right">

														<hx:jspPanel id="jspPanelRightPanel">

															<hx:commandExButton type="submit"
																value="#{labels.bcdUpdateDrawButtonLabel}"
																styleClass="commandExButton submitChangesStyle"
																id="buttonUpdateDraw"
																action="#{pc_WeekDrawView.doButtonUpdateDrawAction}"
																style="margin-top: 5px"
																confirm="#{labels.bcdConfirmSaveMsg}"
																onclick="return func_SaveClicked(this, event);"
																tabindex="8" onmouseover="this.focus();"></hx:commandExButton>
															<hx:commandExButton type="submit"
																value="#{labels.bcdCancelChangesButtonLabel}"
																styleClass="commandExButton cancelChangesStyle"
																id="buttonUndoChanges"
																action="#{pc_WeekDrawView.doButtonUndoChangesAction}"
																confirm="#{labels.bcdConfirmDiscardChangesMsg}"
																tabindex="9" onclick="return func_discardClicked(this, event);" style="margin-left: 5px"></hx:commandExButton>
														</hx:jspPanel>
													</f:facet>
													<f:facet name="bottom"></f:facet>
													<f:facet name="top"></f:facet>
												</hx:panelLayout>
											</f:facet>
											<f:facet name="top">
												<hx:jspPanel id="jspPanelNorthPanel">
													<TABLE border="0" cellpadding="0" cellspacing="1" width="625">
														<TBODY>
															<TR>
																<TD><h:messages styleClass="messages"
																id="messagesErrorMessages" globalOnly="false"
																layout="list" style="font-size: 11pt"></h:messages></TD>
															</TR>
															<TR>
																<TD><h:outputText styleClass="messages"
																	id="textUpdateErrorsMessage"
																	value="#{labels.bcdSaveChangesFailed}"
																	style="font-size: 11pt"
																	rendered="#{blueChipDrawManagementHandler.showSaveFailedMessage}"></h:outputText></TD>
															</TR>
															<TR>
																<TD align="right"></TD>
															</TR>
														</TBODY>
													</TABLE>
												</hx:jspPanel>
											</f:facet>
										</hx:panelLayout>
										
										<BR>
									<h:outputFormat styleClass="outputText message"
										id="formatDisclaimer" value="#{labels.bcdCutOffTimeMessage}">
									</h:outputFormat>
									<BR>


										<BR>
										

										<BR>
										

										<BR>
										<h:inputHidden
											value="#{blueChipDrawManagementHandler.minutesToStaleData}"
											id="minutesToStaleData"></h:inputHidden>
										
										<hx:inputHelperKeybind key="Enter" id="inputHelperKeybind2" targetAction="nexttab"/>
									</h:form></hx:scriptCollector><%-- /tpl:put --%>
							</div> <br> <!-- end main content area -->
						</td>
					</tr>
					<tr>
						<td class="leftNavTD" align="left" valign="bottom">
							<div class="bottomNavAreaImage"></div>
						</td>
						<td></td>

					</tr>
					<tr>
						<td align="left" valign="top" colspan="3" height="20"><img
							class="footerImage" src="/scweb/theme/images/USATODAY_Footer_940x12.jpg">
							<!-- 							class="footerImage" src="/scweb/theme/images/800x20_art2.jpg"> -->

						</td>
					</tr>
					<tr>
						<td align="center" valign="top"><div class="footer"></div>
						</td>
						<td></td>
						<td class="mainContentWideTD" align="center" valign="top">
							<div class="footer">
								<table border="0" cellspacing="0" cellpadding="0" width="675">
									<tbody>
										<tr>
											<td class="mainContentWideTD" align="center">
												<!-- someting in this table drastically impacts how this template works so leave it in for now -->
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top" colspan="3">
							<div class="footer">
								<table border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td><img src="/scweb/theme/1x1.gif" hspace="15">
											</td>
											<!-- 											<td class="mainContentWideTD" align="center">&copy; Copyright -->
											<!-- 												2007 <a href="http://www.usatoday.com/marketing/credit.htm" -->
											<!-- 												target="_blank">USA TODAY</a>, a division of <a -->
											<!-- 												href="http://www.gannett.com" target="_blank">Gannett Co.</a> Inc. <a -->
											<!-- 												href="/scweb/legal/privacy.html" target="_blank">Privacy Policy</a>, -->
											<!-- 												By using this service, you accept our <a -->
											<!-- 												href="/scweb/legal/service.html" target="_blank">Terms of -->
											<!-- 													Service.</a></td> -->
											<td><br>&copy; <script>
												document.write(getCurrentYear());
											</script> USA TODAY, a <a href="http://www.gannett.com">Gannett Company</a> <a
												href= "/scweb/legal/privacy.html" target="_blank">| Privacy
													Policy</a>| By using this service, you accept
												our <a href="/scweb/legal/service.html" target="_blank">Terms of
													Service</a>.</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:scriptCollector>
	</body>
</f:view>
<!--  following link put here because of a bug in faces that seems to prevent styles from working properly -->
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
</html>
<%-- /tpl:insert --%>
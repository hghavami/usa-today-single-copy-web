<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/sc_secure/bcd/Order_history.java" --%><%-- /jsf:pagecode --%><%-- tpl:insert page="/theme/JSP-C-blue_mod.jtpl" --%><!DOCTYPE HTML>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<%
	OmnitureTrackingCode otc = (OmnitureTrackingCode) session
			.getAttribute("omnitureTracking");
	if (otc == null) {
		otc = new OmnitureTrackingCode();
	}
	String omnitureP1 = otc.getTrackingCodePart1();

	// following work around due to bug in RAD
	String useMap = "#map_800x125_top";
	String headURL = "/scweb/index.usat";
%>
<!-- NEW path -->


<link rel="stylesheet" href="/scweb/theme/Master.css" type="text/css">
<link href="/scweb/theme/C_master_blue.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/scweb/theme/stylesheet.css"
	title="Style">

<script src="/scweb/scripts/windowutility.js"></script>
<script language="JavaScript">
	// method called on page load
	function _initPage() {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this

		if (typeof window.initPage == "function") {
			window.initPage();
		}

	}

	// method called on page unload
	function _unloadPage(thisObj, thisEvent) {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this
		if (typeof window.unloadPage == "function") {
			window.unloadPage(thisObj, thisEvent);
		}

	}

	// method called on page unload
	function _onbeforeunloadPage(thisObj, thisEvent) {
		// insert site-wide code here

		// you typically would run this after all site-wide
		// execution has processed, but it's really up to
		// the design of your site to when you may want to run this
		if (typeof window.onbeforeunloadPage == "function") {
			window.onbeforeunloadPage(thisObj, thisEvent);
		}

	}
</script>

<%=omnitureP1%>

<%-- tpl:put name="headarea" --%>
<title>Product Order History</title>

<script type="text/javascript" src="/scweb/scripts/scriptaculous/prototype.js"></script>
<script src="/scweb/scripts/commonValidations.js"></script>
<script type="text/javascript" src="/scweb/scripts/blueChipDrawMngmt.js"></script>
<SCRIPT type="text/javascript">
s.pageName = "Blue Chip Hotel PO Draw History";
s.channel = "Blue Chip Draw";

</script>
<script language="JavaScript">

function func_9(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
helpScreen('/scweb/sc_secure/bcd/bcdHelp.usat'); 
if (helpWindow) {
	helpWindow.focus();
}
return false;
}
function func_10(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
window.status='Click for Help On This Page';

}
function func_11(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
window.status='';
}
function func_12(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	window.print();
	return false;
}				
</script>
<LINK rel="stylesheet" type="text/css"
						href="/scweb/theme/stylesheet.css" title="Style">
		
<%-- /tpl:put --%>
<script language="javascript">
	//var logoutMsg = 'Please close all Draw Management windows before logging out';
	function cleanupWindows() {
		// switch to:
		if (rdlWindow != null) {
			//if (rdlWindow.closed == false) {
			if (rdlWindow.open && !rdlWindow.closed) {
				rdlWindow.setForceClose();
				rdlWindow.close();
			}

		}

		if (weeklyRDLWindow != null) {
			if (weeklyRDLWindow.open && !weeklyRDLWindow.closed) {
				weeklyRDLWindow.setForceClose();
				weeklyRDLWindow.close();
			}
		}

		if (helpWindow != null) {
			if (helpWindow.open && !helpWindow.closed) {
				helpWindow.close();
			}
		}

		if (printWindow != null) {
			if (printWindow.open && !printWindow.closed) {
				printWindow.close();
			}
		}

		return true;
	}

	function okToLogout() {
		var alert = false;
		if (rdlWindow != null) {
			if (rdlWindow.open && !rdlWindow.closed) {
				alert = true;
			}
		}

		if (weeklyRDLWindow != null) {
			if (weeklyRDLWindow.open && !weeklyRDLWindow.closed) {
				alert = true;
			}
		}

		if (alert) {
			setTimeout(
					'alert("You must close all Draw Management windows before logging out.")',
					100);
			return false;
		} else {
			cleanupWindows();
			return true;
		}
	}

	function func_logoutOK(thisObj, thisEvent) {
		//use 'thisObj' to refer directly to this component instead of keyword 'this'
		//use 'thisEvent' to refer to the event generated instead of keyword 'event'
		return okToLogout();
	}
	function getCurrentYear() {
		today = new Date();
		y0 = today.getFullYear();
		return y0;
	}
</script>
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="charset=ISO-8859-1">
</head>
<f:view locale="#{userLocaleHandler.locale}">
	<f:loadBundle basename="resourcesFile" var="labels" />
	<body onLoad="_initPage();" onUnload="_unloadPage(this, event);"
		onbeforeunload="_onbeforeunloadPage(this, event);">
		<hx:scriptCollector id="templateScriptCollector">


			<!-- start header area -->
			<div class="topAreaBox1"></div>
			<div class="topAreaLogo">
				<!-- 				<img border="0" src="/scweb/theme/images/800x125_top.jpg" -->
				<%-- 					width="800" height="125" usemap="<%=useMap %>"> --%>
				<img border="0" src="/scweb/theme/images/USATODAY_Header_940x145.jpg"
					width="940" height="145" usemap="<%=useMap%>">
				<map name="map_800x125_top">
					<area shape="rect" target="_self" href="<%=headURL%>"
						coords="19,17,268,112">
					<area shape="default" nohref>
				</map>
			</div>
			<div class="topAreaLogoLine">
				<img border="0" src="/scweb/theme/images/800x20_art2.jpg" height="20"
					width="800">
			</div>
			<!-- end header area -->

			<!-- start header navigation bar -->
			<div class="topNavBk"></div>${omnitureTracking.trackingCodePart2}<div
				class="topNav"></div>
			<div class="topAreaDatePlacement">
				<fmt:formatDate value="${pc_JSPCblue_mod.now}" dateStyle="full" />
			</div>
			<h:outputText styleClass="globalMessageDivStyle" id="textGlobalMessageText"
				value="#{globalMessage.message}"
				rendered='#{ (globalMessage.showMessage) and ((globalMessage.showToWhom eq "all") or ((globalMessage.showToWhom eq "hotel") and userHandler.canWorkWithBlueChipDraw ) or ((globalMessage.showToWhom eq "contractor") and userHandler.canWorkWithRoutes) )}'></h:outputText>
			<!-- end header navigation bar bar -->

			<!-- start left-hand navigation -->
			<table class="mainBox" border="0" cellpadding="0" width="100%" height="87%"
				cellspacing="0">
				<tbody>
					<tr>
						<td class="leftNavTD" align="left" valign="top">
							<div class="leftNavBox">
								<h:form styleClass="form" id="formLogout">
									<%-- tpl:put name="LeftNavTopMostJSPContentArea" --%><%-- /tpl:put --%>
									<div id="logoutLinkDiv" class="noDisplay" style="${userHandler.divCss}">
										<h:outputFormat styleClass="outputFormat" id="formatWelcomeMessage"
											value="Welcome, {0}">
											<f:param name="displayName" value="#{userHandler.displayName}"></f:param>
										</h:outputFormat>
										<br>
										<hx:commandExButton type="submit" value="#{labels.logoutLink}"
											styleClass="commandExButtonSmall" id="buttonDoLogoutBtn"
											title="Logout" alt="Logout"
											action="#{pc_JSPCblue_mod.doButtonDoLogoutBtnAction}"
											onclick="return func_logoutOK(this, event);"></hx:commandExButton>
										<hr width="95%" align="left">
									</div>
									<siteedit:navbar spec="/scweb/theme/nav_vertical_tree_left.jsp"
										targetlevel="1-5" onlychildren="true" navclass="leftNav"
										target="topchildren" />
									<br>
									<br>
									<br>
									<!-- If we add internatiolization
								<h:selectOneMenu styleClass="selectOneMenu" id="menui18nPref" value="#{userLocaleHandler.localeStr}">
									<f:selectItem itemValue="en" itemLabel="English" />
									<f:selectItem itemValue="es" itemLabel="Spanish" />
								</h:selectOneMenu><hx:commandExButton type="submit" value="Go"
									styleClass="commandExButton" id="buttonUpdateLangPref"
									action="#{pc_JSPCblue_mod.doButtonUpdateLangPrefAction}"></hx:commandExButton> -->
									<br>
									<hx:inputHelperKeybind key="Enter" id="inputHelperKeybind1"
										targetAction="nothing" />
								</h:form>
								<br>
								<hr width="95%" align="left">
								<b>Related Links:</b><br> <a href="http://usatoday.com"
									target="_blank">USA TODAY.com</a><br> <a
									href="https://service.usatoday.com/welcome.jsp" target="_blank">USA
									TODAY Home Delivery</a><br> <a
									href="https://service.usatoday.com/shop" target="_blank">USA TODAY
									Past Issues</a><br> <br> <br>

							</div>
						</td>
						<!-- end left-hand navigation -->
						<td><img border="0" src="/scweb/theme/img/JSF_1x1.gif" width="1"
							height="1" hspace="6">
						</td>
						<td class="mainContentWideTD" align="left" valign="top" rowspan="2">
							<!-- start main content area -->
							<div class="mainContentWideBox">
								<img border="0" src="/scweb/theme/images/USATODAY_Subhead_740x76.jpg"
									width="740" height="76" usemap="<%=useMap%>">
								<!-- bread crumbs if we want em-->
								<div class="navTrailLoc">
									<siteedit:navtrail start="[" end="]" target="home,parent,ancestor,self"
										separator="&gt;&gt;" spec="/scweb/theme/trail.jsp" />
								</div>
								<a name="navskip"><img border="0" src="/scweb/theme/img/JSF_1x1.gif"
									width="1" height="1" alt="Beginning of page content"> </a>
								<%-- tpl:put name="jspbodyarea" --%>
							<hx:scriptCollector id="scriptCollectorPOHistoryCollector">
								<h:form styleClass="form" id="formPOHistoryForm">
									<TABLE border="0" cellpadding="0" cellspacing="1" width="605">
											<TBODY>
												<TR>
													<TD><h:messages styleClass="messages"
													id="messagesGlobalErrors" globalOnly="true" layout="list"
													style="font-size: 11pt"></h:messages></TD>
												<td align="right">
													<div class="mainContentHelpIcon"><hx:graphicImageEx
															styleClass="graphicImageEx" id="imageExPrintIcon"
															style="cursor: pointer;"
															value="/scweb/images/print.gif" width="20" height="21"
															border="0" alt="Send To Printer" title="Send To Printer" 
															onclick="return func_12(this,event);"></hx:graphicImageEx>
														<hx:graphicImageEx
																styleClass="graphicImageEx"
																style="cursor: pointer; margin-bottom: 2px; margin-left: 5px; margin-right: 5px; margin-top: 2px"
																id="imageExHelpIcon1"
																value="/scweb/images/question_mark.gif" width="16"
																height="16" hspace="0" title="Help"
																onclick="return func_9(this, event);"
																onmouseover="return func_10(this, event);"
																onmouseout="return func_11(this, event);"></hx:graphicImageEx>
													</div>												
												</td>

											</TR>
										</TBODY>
										</TABLE>

									<h:commandLink styleClass="commandLink" id="linkBackToPrevious1" action="#{pc_Order_history.doLinkBackToPreviousAction}">
										<h:outputText id="textBackToDailyLinkLabel1"
											styleClass="outputText" value="#{labels.bcdBackToDaily}"></h:outputText>
									</h:commandLink>
									<hx:panelLayout styleClass="panelLayoutTopAlign"
										id="layoutDataLayout1"
										style="vertical-align: top; border-color: #8fa4b8; border-width: 1px; border-style: solid">
										<f:facet name="body">
											<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
												columnClasses="columnClass3" headerClass="headerClass"
												footerClass="footerClass" rowClasses="rowClass2, rowClass1"
												styleClass="dataTableEx" id="tableExHistoryData"
												value="#{bcPoOrderHistoryHandler.weeks}" var="varweeks">
												<f:facet name="footer">
													<hx:panelBox styleClass="panelBox" id="box2"></hx:panelBox>
												</f:facet>
												<f:facet name="header">
													<hx:panelBox styleClass="panelBox" id="box1">
														<h:panelGrid styleClass="panelGrid"
															id="gridHeaderOuterGrid" cellpadding="2" cellspacing="2"
															columns="1">
															<h:outputText styleClass="outputText"
																id="textPOWeekHeaderLabel"
																value="<b>Product:</b>  #{bcPoOrderHistoryHandler.owningPO.productDescription}, <b>Order ID:</b> #{bcPoOrderHistoryHandler.owningPO.productOrderID}"
																escape="false"></h:outputText>
														</h:panelGrid>
													</hx:panelBox>
												</f:facet>
												<hx:columnEx id="columnExWeekEnding" nowrap="true"
													align="center">
													<f:facet name="header">

														
															<h:outputText id="text1" styleClass="outputText"
																value="Week Ending Date"></h:outputText>
													
													</f:facet>
													<h:outputText styleClass="outputText"
														id="textWeekEndingDate" style="font-weight: bold"
														value="#{varweeks.weekEndingDateObj}">
														<hx:convertDateTime dateStyle="short" />
													</h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx2" align="center">
													<f:facet name="header">
														<h:outputText value="Mon" styleClass="outputText"
															id="text2"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text10"
														value="#{varweeks.drawMondayHistoryDisplay}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx3" align="center">
													<f:facet name="header">
														<h:outputText value="Tue" styleClass="outputText"
															id="text3"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text11"
														value="#{varweeks.drawTuesdayHistoryDisplay}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx4" align="center">
													<f:facet name="header">
														<h:outputText value="Wed" styleClass="outputText"
															id="text4"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text12"
														value="#{varweeks.drawWednesdayHistoryDisplay}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx5" align="center">
													<f:facet name="header">
														<h:outputText value="Thu" styleClass="outputText"
															id="text5"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text13"
														value="#{varweeks.drawThursdayHistoryDisplay}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx6" align="center">
													<f:facet name="header">
														<h:outputText value="Fri" styleClass="outputText"
															id="text6"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text15"
														value="#{varweeks.drawFridayHistoryDisplay}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx7" align="center">
													<f:facet name="header">
														<h:outputText value="Sat" styleClass="outputText"
															id="text7"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text14"
														value="#{varweeks.drawSaturdayHistoryDisplay}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx8" align="center">
													<f:facet name="header">
														<h:outputText value="Sun" styleClass="outputText"
															id="text8"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text16"
														value="#{varweeks.drawSundayHistoryDisplay}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnExWeekTotal" align="center">
													<f:facet name="header">
														<h:outputText value="Week Total" styleClass="outputText"
															id="text17"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text18"
														value="#{varweeks.drawWeekTotalDisplay}"
														style="font-weight: bold"></h:outputText>
												</hx:columnEx>
											</hx:dataTableEx>

										</f:facet>
										<f:facet name="left">
											<h:panelGrid styleClass="panelGrid" id="gridLeftPanel1"
												cellpadding="0" cellspacing="0" columns="1"
												style="margin-left: 5px; margin-right: 10px;">

												<h:outputText styleClass="outputText_SM"
													id="textLocationIDStr"
													value="#{bcPoOrderHistoryHandler.location.location.locationID}"
													style="font-weight: bold"></h:outputText>
												<h:outputText styleClass="outputText" id="textLocationName"
													value="#{bcPoOrderHistoryHandler.location.locationName}"></h:outputText>

												<h:outputText styleClass="outputText"
													id="textLocationAddress1" value="#{bcPoOrderHistoryHandler.location.address1}"></h:outputText>
												<h:outputText styleClass="outputText" id="textLocationPhone" value="#{bcPoOrderHistoryHandler.location.phoneNumber}">
													<hx:convertMask mask="(###) ###-####" />
												</h:outputText>


											</h:panelGrid>
										</f:facet>
										<f:facet name="right"></f:facet>
										<f:facet name="bottom"></f:facet>
										<f:facet name="top"></f:facet>
									</hx:panelLayout>
									<h:commandLink styleClass="commandLink" id="linkBackToPrevious" action="#{pc_Order_history.doLinkBackToPreviousAction}">
										<h:outputText id="textBackToDailyLinkLabel2"
											styleClass="outputText" value="#{labels.bcdBackToDaily}"></h:outputText>
									</h:commandLink>
									<br>
								<br>
			
				</h:form>
	</hx:scriptCollector>
						<%-- /tpl:put --%>
							</div> <br> <!-- end main content area -->
						</td>
					</tr>
					<tr>
						<td class="leftNavTD" align="left" valign="bottom">
							<div class="bottomNavAreaImage"></div>
						</td>
						<td></td>

					</tr>
					<tr>
						<td align="left" valign="top" colspan="3" height="20"><img
							class="footerImage" src="/scweb/theme/images/USATODAY_Footer_940x12.jpg">
							<!-- 							class="footerImage" src="/scweb/theme/images/800x20_art2.jpg"> -->

						</td>
					</tr>
					<tr>
						<td align="center" valign="top"><div class="footer"></div>
						</td>
						<td></td>
						<td class="mainContentWideTD" align="center" valign="top">
							<div class="footer">
								<table border="0" cellspacing="0" cellpadding="0" width="675">
									<tbody>
										<tr>
											<td class="mainContentWideTD" align="center">
												<!-- someting in this table drastically impacts how this template works so leave it in for now -->
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top" colspan="3">
							<div class="footer">
								<table border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td><img src="/scweb/theme/1x1.gif" hspace="15">
											</td>
											<!-- 											<td class="mainContentWideTD" align="center">&copy; Copyright -->
											<!-- 												2007 <a href="http://www.usatoday.com/marketing/credit.htm" -->
											<!-- 												target="_blank">USA TODAY</a>, a division of <a -->
											<!-- 												href="http://www.gannett.com" target="_blank">Gannett Co.</a> Inc. <a -->
											<!-- 												href="/scweb/legal/privacy.html" target="_blank">Privacy Policy</a>, -->
											<!-- 												By using this service, you accept our <a -->
											<!-- 												href="/scweb/legal/service.html" target="_blank">Terms of -->
											<!-- 													Service.</a></td> -->
											<td><br>&copy; <script>
												document.write(getCurrentYear());
											</script> USA TODAY, a <a href="http://www.gannett.com">Gannett Company</a> <a
												href= "/scweb/legal/privacy.html" target="_blank">| Privacy
													Policy</a>| By using this service, you accept
												our <a href="/scweb/legal/service.html" target="_blank">Terms of
													Service</a>.</td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:scriptCollector>
	</body>
</f:view>
<!--  following link put here because of a bug in faces that seems to prevent styles from working properly -->
<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
</html>
<%-- /tpl:insert --%>
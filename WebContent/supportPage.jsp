<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/SupportPage.java" --%><%-- /jsf:pagecode --%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<f:view>
<%-- tpl:insert page="/theme/HTML-C-02_blue.htpl" --%><!DOCTYPE HTML>
<HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<!-- NEW path -->
<LINK href="/scweb/theme/Master.css" rel="stylesheet" type="text/css">
<LINK href="/scweb/theme/C_master_blue.css" rel="stylesheet" type="text/css">

<SCRIPT src="/scweb/scripts/windowutility.js"></SCRIPT>
<SCRIPT language="JavaScript">

// method called on page load
function _initPage() {
	// insert site-wide code here


	// you typically would run this after all site-wide
	// execution has processed, but it's really up to
	// the design of your site to when you may want to run this
	
	if( typeof window.initPage == "function" ) {
		window.initPage();
	}

}

// method called on page unload
function _unloadPage(thisObj, thisEvent) {
	// insert site-wide code here


	// you typically would run this after all site-wide
	// execution has processed, but it's really up to
	// the design of your site to when you may want to run this
	if( typeof window.unloadPage == "function" ) {
		window.unloadPage(thisObj, thisEvent);
	}

}

// method called on page unload
function _onbeforeunloadPage(thisObj, thisEvent) {
	// insert site-wide code here


	// you typically would run this after all site-wide
	// execution has processed, but it's really up to
	// the design of your site to when you may want to run this
	if( typeof window.onbeforeunloadPage == "function" ) {
		window.onbeforeunloadPage(thisObj, thisEvent);
	}

}

</SCRIPT>
<%-- tpl:put name="htmlheadarea" --%>
				

			<TITLE>Error Page - Support</TITLE>
					<LINK rel="stylesheet" type="text/css" href="theme/stylesheet.css" title="Style">
				<SCRIPT type="text/javascript">
</SCRIPT><%-- /tpl:put --%>
</HEAD>
<BODY onLoad="_initPage();" 
      onUnload="_unloadPage(this, event);"
      onbeforeunload="_onbeforeunloadPage(this, event);">
<!-- start header area -->
<div class="topAreaBox"></div>
<div class="topAreaLogo"><IMG border="0" src="/scweb/theme/images/800x125_top.jpg" width="800"
	height="125" usemap="#800x125_top"><MAP name="800x125_top">
	<AREA shape="rect" href="/scweb/index.jsp" coords="19,17,265,113">
	<AREA shape="default" nohref="nohref">
</MAP>
</div>
<div class="topAreaLogoLine"><IMG border="0" src="/scweb/theme/images/800x20_art2.jpg" height="20" width="800"></div>
<!-- end header area -->

<!-- start header navigation bar -->
<div class="topNavBk"></div>
<%-- tpl:put name="topNavContentArea" --%>Top Nav Content Area<%-- /tpl:put --%>

<!-- end header navigation bar bar -->

<!-- start left-hand navigation -->
<table class="mainBox" border="0" cellpadding="0" width="100%" height="87%" cellspacing="0">
   <tbody>
      <tr>
         <td class="leftNavTD" align="left" valign="top">
            <div class="leftNavBox">
	         	<%-- tpl:put name="leftNavContentArea" --%><%-- /tpl:put --%>
				<BR>
				<HR width="95%" align="left">
				<B>Related Links:</B><BR>
				<A href="http://usatoday.com" target="_blank">USA TODAY.com</A><BR>
				<A href="https://service.usatoday.com/welcome.jsp" target="_blank">USA TODAY Home Delivery</A><BR>
				<A href="https://service.usatoday.com/shop" target="_blank">USA TODAY Past Issues</A><BR>
				<BR>
				<BR>
				
			</div>
		</td>
<!-- end left-hand navigation -->

<!-- start main content area -->
			<td class="mainContentWideTD" align="left" valign="top" rowspan="2">
				<div class="mainContentWideBox">
					<%-- tpl:put name="MainAreaNavTrailContentArea" --%> <%-- /tpl:put --%>
					<a name="navskip"><IMG border="0" src="/scweb/theme/1x1.gif" width="1" height="1" alt="Beginning of page content"></a>
					<%-- tpl:put name="bodyarea" --%><hx:scriptCollector id="scriptCollector1">
								<BR>
							<h:form styleClass="form" id="form1"><TABLE width="550" border="0" cellpadding="0" cellspacing="1">
								<TBODY>
									<TR>
										<TD align="left" style="outputText_Med">
											<B> <SPAN style="font-size: 12pt">An Unexpected Error has occurred:</SPAN></B><SPAN style="font-size: 12pt"> </SPAN><BR>
										<BR>  <hx:outputLinkEx styleClass="outputLinkEx" value="/scweb/sc_secure/home.usat" id="linkExHomeLink">
														<h:outputText id="text1" styleClass="outputText" value="Click Here"></h:outputText>
													</hx:outputLinkEx> to proceed to the application.<BR>
				
				
													
													<BR>
											<h:outputText styleClass="outputText_Med" id="textSCRouteWarningMsg" value="Notice: Attempting to work with multiple routes at the same time can cause your changes to be lost. This web application is designed to work with a single route at one time." rendered="#{userHandler.canWorkWithRoutes}"></h:outputText>
											<BR>
												<BR>
										<SPAN class="outputText_Med">This web application is tested
										to work with Microsoft Internet Explorer Version 6.0 and above
										and Firefox Version 2.0 and above. Other browsers are not
										tested by USA TODAY and may not function properly. If you are
										experiencing issues working with this site, please check you
										browser version for compatibility prior to contacting USA
										TODAY.<BR>
										<BR>
											Dial-up Internet Connections are not recommended.<BR>
											<BR>
										Thank You. </SPAN></TD>
									</TR>
									<TR>
										<TD></TD>
									</TR>
								</TBODY>
							</TABLE></h:form>
<BR>
								<BR></hx:scriptCollector><%-- /tpl:put --%>
				</div>
<!-- end main content area -->
           </td>
       </tr>
		<TR>
			<TD class="leftNavTD" align="left" valign="bottom">
			<div class="bottomNavAreaImage"></div>
			</TD>
		</TR>
		<TR>
			<TD align="left" valign="top" colspan="2" height="20"><img class="footerImage" src="/scweb/theme/images/800x20_art2.jpg" /></TD></TR>
		<TR>
			<TD align="center" valign="top"><DIV class="footer">
				</DIV>
			</TD>
			<TD class="mainContentWideTD" align="center" valign="top">
			<div class="footer">
				<table border="0" cellspacing="0" cellpadding="0" width="675">
					<tbody>
						<tr>
						<td class="mainContentWideTD" align="center"><!-- someting in this table drastically impacts how this template works so leave it in for now --></td>
						</tr>
				</tbody></table>
				</div>
				
				</TD>
		</TR>
		<TR>
			<TD valign="top" colspan="2">
				<div class="footer">
				<table border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
						<td>
							<img src="/scweb/theme/1x1.gif" hspace="15">
						</td>
						<td class="mainContentWideTD" align="center">&copy; Copyright 2007 <A
							href="http://www.usatoday.com/marketing/credit.htm" target="_blank">USA
						TODAY</A>, a division of <A
						href="http://www.gannett.com" target="_blank">Gannett
						Co.</A> Inc. <A href="/scweb/legal/privacy.html" target="_blank">Privacy
						Policy</A>, 
						By using this
						service, you accept our <A href="/scweb/legal/service.html" target="_blank">Terms of Service.</A>								
						</td>
						</tr>
				</tbody></table>
				</div>
			</TD>
		</TR>
	</tbody>
</table>
</BODY>
</HTML><%-- /tpl:insert --%>
</f:view>
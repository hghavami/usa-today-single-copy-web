         
<%@ page contentType="text/html; charset=utf-8" %>

<%@ page buffer="none" autoFlush="true" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.ibm.pvc.wps.docEditor.*" %>

<%@ taglib uri="/WEB-INF/tld/DocEditor.tld" prefix="docEditor" %>

<jsp:useBean id="com_ibm_pvc_wps_docEditor" class="java.lang.String" scope="request"/>

<% String userAgent = request.getHeader("user-agent"); 
 boolean isMozilla = userAgent.indexOf("Gecko") == -1 ? false : true;  
 Editor aEditor = (Editor)request.getAttribute("com_ibm_pvc_wps_docEditor_" + com_ibm_pvc_wps_docEditor); 
 String editorName = aEditor.getName(); 
 String locale = (String)aEditor.getAttribute("locale"); 
 ResourceBundle resourceBundle = (ResourceBundle)aEditor.getAttribute("resourceBundle"); 
 boolean withSource = aEditor.getAttribute("addSource") != null; 
 String isBidi = (String)aEditor.getAttribute("isBidi"); 
 String imageDirection = "_ltr"; if (isBidi.equalsIgnoreCase("true")) imageDirection = "_rtl"; 
 String indentImg = "indent" + imageDirection + ".gif"; 
 String outdentImg = "outdent" + imageDirection + ".gif"; 
 String images = (String)aEditor.getAttribute("images"); 
 String directory = (String)aEditor.getAttribute("directory");
 String rb_27 = resourceBundle.getString("AlignLeft"); 
 String rb_28 = resourceBundle.getString("AlignCenter"); 
 String rb_29 = resourceBundle.getString("AlignRight"); 
 String rb_30 = resourceBundle.getString("OrderedList"); 
 String rb_31 = resourceBundle.getString("BulletedList"); 
 String rb_32 = resourceBundle.getString("Indent"); 
 String rb_33 = resourceBundle.getString("Outdent"); 
 String rb_34 = resourceBundle.getString("SourceMode"); 
 String rb_40 = resourceBundle.getString("Paragraph"); 
 String rb_50 = resourceBundle.getString("FlipWholeDocument"); 
 String rb_52 = resourceBundle.getString("TextDirectionLTR"); 
 String rb_53 = resourceBundle.getString("TextDirectionRTL"); 
 String fn_0 = "IBM_RTE_doFontStyle('" + editorName + "', this, 'formatblock')"; 
 String fn_1 = "IBM_RTE_doFontStyle('" + editorName + "', this, 'fontname')"; 
 String fn_2 = "IBM_RTE_doFontStyle('" + editorName + "', this, 'fontsize')"; 
 String fn_6 = "IBM_RTE_doTheCommand('" + editorName + "', 'justifyleft')"; 
 String fn_7 = "IBM_RTE_doTheCommand('" + editorName + "', 'justifycenter')"; 
 String fn_8 = "IBM_RTE_doTheCommand('" + editorName + "', 'justifyright')"; 
 String fn_9 = "IBM_RTE_doTheCommand('" + editorName + "', 'insertorderedlist')"; 
 String fn_10 = "IBM_RTE_doTheCommand('" + editorName + "', 'insertunorderedlist')"; 
 String fn_11 = "IBM_RTE_doTheCommand('" + editorName + "', 'indent')"; 
 String fn_12 = "IBM_RTE_doTheCommand('" + editorName + "', 'outdent')"; 
 String fn_13 = "IBM_RTE_accessDoToggleView()"; 
 String fn_50 = "IBM_RTE_flipDocument('" + editorName + "')"; 
 String fn_52 = "IBM_RTE_flipSelection('" + editorName + "', 'LTR')"; 
 String fn_53 = "IBM_RTE_flipSelection('" + editorName + "', 'RTL')"; 
 String fn_paraSupport = "IBM_RTE_paraSupport('" + editorName + "', this, '" + locale + "', '" + images + "', '" + directory + "','"+isBidi+"')"; 
 String toolbarName = "ToolbarStandard2"; %>

<docEditor:addToolbar editor="<%= editorName %>" name="<%= toolbarName %>" justify="left"/>
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonParagraph" text="<%= rb_40 %>" image="paragraphSupport.gif" script="<%= fn_paraSupport %>"/>
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonAlignLeft" text="<%= rb_27 %>" image="alignLeft.gif" script="<%= fn_6 %>"/>
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonAlignMiddle" text="<%= rb_28 %>" image="alignCenter.gif" script="<%= fn_7 %>"/>
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonAlignRight" text="<%= rb_29 %>" image="alignRight.gif" script="<%= fn_8 %>"/>
<docEditor:addDivider editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="Divider1" />
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonNumbers" text="<%= rb_30 %>" image="number.gif" script="<%= fn_9 %>"/>
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonBullets" text="<%= rb_31 %>" image="bullets.gif" script="<%= fn_10 %>"/>
<docEditor:addDivider editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="Divider2" />
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonIndent" text="<%= rb_32 %>" image="<%= indentImg %>" script="<%= fn_11 %>"/>
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonOutdent" text="<%= rb_33 %>" image="<%= outdentImg %>" script="<%= fn_12 %>"/>
<docEditor:addDivider editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="Divider3" />
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonDocWhole" text="<%= rb_50 %>" image="docwholebidi.gif" script="<%= fn_50 %>"/>
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonDocSelectionLTR" text="<%= rb_52 %>" image="docselectionLTR.gif" script="<%= fn_52 %>"/>
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonDocSelectionRTL" text="<%= rb_53 %>" image="docselectionRTL.gif" script="<%= fn_53 %>"/>
<docEditor:addDivider editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="Divider4" />
<% String localeInsertLeftPic = (locale.compareToIgnoreCase("ar") == 0 || locale.compareToIgnoreCase("iw") == 0)?"insertColumnRight.gif":"insertColumnLeft.gif"; 
 String localeInsertRightPic = (locale.compareToIgnoreCase("ar") == 0 || locale.compareToIgnoreCase("iw") == 0)?"insertColumnLeft.gif":"insertColumnRight.gif"; 
 String rb_51 = resourceBundle.getString("InsertTable"); 
 String rb_311 = resourceBundle.getString("NumberOfRows"); 
 String rb_321 = resourceBundle.getString("NumberOfColumns"); 
 String rb_611 = resourceBundle.getString("InsertRowAbove"); 
 String rb_621 = resourceBundle.getString("InsertRowBelow"); 
 String rb_631 = resourceBundle.getString("DeleteRow"); 
 String rb_641 = resourceBundle.getString("InsertColumnLeft"); 
 String rb_651 = resourceBundle.getString("InsertColumnRight"); 
 String rb_661 = resourceBundle.getString("DeleteColumn"); 
 String rb_701 = resourceBundle.getString("OperationCannotBeUndone"); %>

<script>
var tab_array = new Array();
tab_array[0] = "<%= rb_31 %>";
tab_array[1] = "<%= rb_32 %>";
</script>

<% String fn_51 = "IBM_RTE_doTableInsert('" + editorName + "', this, '" + locale + "', '" + images + "', '" + directory + "', tab_array, '" + isBidi + "')"; 
 String error_rowInsert = resourceBundle.getString("rte.table.incorrectRowInsert"); 
 String error_columnInsert = resourceBundle.getString("rte.table.incorrectColumnInsert"); 
 String error_rowDelete = resourceBundle.getString("rte.table.incorrectRowDelete"); 
 String error_columnDelete = resourceBundle.getString("rte.table.incorrectColumnDelete"); %>

<script>
var tab_errors1 = new Array();
tab_errors1[0] = "<%= error_rowInsert %>";
var tab_errors2 = new Array();
tab_errors2[0] = "<%= error_rowDelete %>";
var tab_errors3 = new Array();
tab_errors3[0] = "<%= error_columnInsert %>";
var tab_errors4 = new Array();
tab_errors4[0] = "<%= error_columnDelete %>";
var tab_errors5 = new Array();
tab_errors5[0] = "<%= rb_701 %>";
</script>
<% String fn_table11 = "IBM_RTE_insertRowAbove('" + editorName + "',tab_errors1)"; 
 String fn_table21 = "IBM_RTE_insertRowBelow('" + editorName + "')"; 
 String fn_table31 = "IBM_RTE_deleteRow('" + editorName + "', tab_errors5, tab_errors2)"; 
 String fn_table41 = "IBM_RTE_insertColumnLeft('" + editorName + "',tab_errors3)"; 
 String fn_table51 = "IBM_RTE_insertColumnRight('" + editorName + "',tab_errors3)";
 String fn_table61 = "IBM_RTE_deleteColumn('" + editorName + "', tab_errors5, tab_errors4)"; %>

<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonTable" text="<%= rb_51 %>" image="insertTable.gif" script="<%= fn_51 %>"/>
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonRowAbove" text="<%= rb_611 %>" image="insertRowAbove.gif" script="<%= fn_table11 %>"/>
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonRowBelow" text="<%= rb_621 %>" image="insertRowBelow.gif" script="<%= fn_table21 %>"/>
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonRowDelete" text="<%= rb_631 %>" image="deleteRow.gif" script="<%= fn_table31 %>"/>
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonColLeft" text="<%= rb_641 %>" image="<%= localeInsertLeftPic %>" script="<%= fn_table41 %>"/>
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonColRight" text="<%= rb_651 %>" image="<%= localeInsertRightPic %>" script="<%= fn_table51 %>"/>
<docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonColDelete" text="<%= rb_661 %>" image="deleteColumn.gif" script="<%= fn_table61 %>"/>
<% if (withSource) { %>
  <docEditor:addButton definition="control_button2.jsp" editor="<%= editorName %>" toolbar="<%= toolbarName %>" name="ButtonSourceCode" text="<%= rb_34 %>" image="sourceCode.gif" script="<%= fn_13 %>"/>
<% } %>


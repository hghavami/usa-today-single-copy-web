// This global variable serves as a flag for RTE acessibility support.
// This var is set in options_ie.jsp when the accessible toolbar is invoked.
var isAccessible = false;
//**START Defect# 170756 Hard to enter one character in table.
var isTDBlank = true;
// This global variable is used to transfer the editor content from the main 
// editor window to the child print window. 
// This var is added in 5.1 release. See IBM_RTE_doPrint() for details. 
var cnt;
var mode = 0;
var findReplWin = null ;
var BrTag = false;

function IBM_RTE_getEditorMode(editorName){
	if(IBM_RTE_getDocument(editorName).mode == null)
		IBM_RTE_getDocument(editorName).mode = 0;
	return IBM_RTE_getDocument(editorName).mode;
}

function IBM_RTE_isMozilla() {
    return navigator.product == 'Gecko';
}

function IBM_RTE_isNetscape() {
    return /netscape/i.test(navigator.userAgent);
}

function IBM_RTE_getFrame(editorName) {
    if (!isAccessible)
       return document.getElementById(editorName );
    else
       return this.opener.document.getElementById(editorName );      
}
function IBM_RTE_setFocusToContentWindow(editorName) {
	if (!isAccessible)
    document.getElementById(editorName).contentWindow.focus();
	else
		this.opener.document.getElementById(editorName).contentWindow.focus();
}

function IBM_RTE_getWindow(editorName) {
    return IBM_RTE_getFrame(editorName).contentWindow;
}

function IBM_RTE_getDocument(editorName) {
    return IBM_RTE_getWindow(editorName).document;
}

function IBM_RTE_setDesignMode1(editorName) {
    IBM_RTE_getDocument(editorName).designMode = "on";  
}
//In IE, use body.contentEditable to set the document to be editable.
//This allows elements to be hidden or shown as needed using style.display attribute.
//The change is mainly for 5.1 annotation feature (available in the popup editor version only).
//No existing features are affected by this change.
function IBM_RTE_setDesignMode(editorName) {
    if (IBM_RTE_isMozilla()) {
        IBM_RTE_getDocument(editorName).designMode = "on";  
    }else {

		if ((IBM_RTE_getDocument(editorName).body)==null){
			setTimeout("IBM_RTE_setDesignMode('" + editorName + "')", 1500);
		}else{
			IBM_RTE_getDocument(editorName).body.contentEditable = true;
		}
    }
	editorNameSaved = editorName;
	if(!IBM_RTE_isMozilla())
		backup[0]=IBM_RTE_getDocument(editorName).body.innerHTML;
}

function IBM_RTE_getToolbarColor(editorName) {
    return IBM_RTE_isMozilla() ? document.defaultView.getComputedStyle(document.getElementById(editorName), '').getPropertyValue("background-color") : document.getElementById(editorName).currentStyle.backgroundColor;
}
function IBM_RTE_loadEditorWithEmptyDocument(editorName, docDir) {
    IBM_RTE_getDocument(editorName).body.innerHTML = "<html><head><title></title></head><body dir='" + docDir + "'></body></html>";
}
function IBM_RTE_doTheCommand(editorName, commandname) {        
/**
 *  undo/redo breaks for RAD RTE plugin in IE.
 *  manual undo/redo implementation used.
 */
    IBM_RTE_getWindow(editorName).focus();

	if(!IBM_RTE_isMozilla() && (commandname == 'undo' || commandname == 'redo')){
		if(commandname == 'undo'){
			if(isAccessible) 
				this.opener.IBM_RTE_undo(editorName);
			else
				IBM_RTE_undo(editorName);
		}
    else{
		if(isAccessible) 
			this.opener.IBM_RTE_redo(editorName);
		else
			IBM_RTE_redo(editorName);
		}
    }
   else{
		if(!IBM_RTE_isMozilla()){
			if(isAccessible) 
				this.opener.IBM_RTE_backup(editorName);
			else
				IBM_RTE_backup(editorName);
		}
	try{
    IBM_RTE_getDocument(editorName).execCommand(commandname, false, null);
	}catch(exception){
	}
    }

	//** For defect 170739 - Firefox: Numbered list is ignored in spell check
	if (IBM_RTE_isMozilla() && ((commandname == "insertorderedlist") || (commandname == "insertunorderedlist"))){
	    IBM_RTE_editList(editorName, commandname);
	}
}

function IBM_RTE_editList(editorName, commandname){
		var cntLItags = IBM_RTE_getDocument(editorName).body.getElementsByTagName("LI");
		var cnt = 0;
		while(cntLItags[cnt] != null){
			cntLItags[cnt].innerHTML = cntLItags[cnt].innerHTML + " ";
			cnt++;
		}
	}

// function to add P tags to the bulleted text in IE. Fix for defect 171070
function IBM_RTE_addPTags_list(editorName){

		var cntLItags = IBM_RTE_getDocument(editorName).body.getElementsByTagName("LI");
		var cnt = 0;
		
		while(cntLItags[cnt] != null){
			if(cntLItags[cnt].childNodes[0].tagName == "DIV"){
					var text = cntLItags[cnt].innerHTML;
					var matches = text.match(/<div(.*?)<\/div>/i) ;
					var str = RegExp.$1;
					text = text.replace(/<div(.*?)<\/div>/i,"<P"+str+"</P>");
					cntLItags[cnt].innerHTML = text ;
			}
			else if (cntLItags[cnt].childNodes[0].tagName == "P")
				{
					cnt++;
					continue ;
				}
			else {
				cntLItags[cnt].innerHTML = "<P style=\"MARGIN: 0px\">"+cntLItags[cnt].innerHTML + "</P>";
			}
			cnt++;
		}
}
//**START Defect - 170277 Mozilla: No Drop Down box in Hperlink dialog box
var url_mozilla = null;
var ptext = null;
var editorName= null;
var prompttext = null;
String.prototype.trim = function() {
a = this.replace(/^\s+/, '');
return a.replace(/\s+$/, '');
};
function createLink_Mozilla(editorName, buttonElement, locale, images, directory, isBidi, prompttext)
{
	this.editorName= editorName;
	this.prompttext = prompttext;
	//** START Defect 174102
	var wcp_linkWin = window.open(directory + 'link_mozilla.jsp?locale=' + locale + '&images=' + images + '&editorName=' + editorName + '&isBidi=' + isBidi, 'link', 'scrollbars=0, toolbar=0, statusbar=0,resizable=yes, width=200, height=190, left=' + ((screen.width - 200) / 2) + ', top=' + ((screen.height - 170) / 2));
	wcp_linkWin.focus();
	return url_mozilla != null; 
}
function createLink_Mozilla_1(editorName, prompttext,theurl)
{
	var a = IBM_RTE_getDocument(editorName).body.getElementsByTagName("A");
		var xxx=0;
		while(a[xxx] != null){
			if(a[xxx].getAttribute("odc") != null)
				a[xxx].setAttribute("href", a[xxx].getAttribute("odc"));
				xxx++;
		}
		var range = IBM_RTE_getSelectionRange(editorName);
		range = ""+range;
		if(range == null || typeof range == 'undefined' || range=='' || range==' ')
		{
			range = theurl;
		}
		theurl = encodeURI(theurl);
		var aNode = IBM_RTE_getDocument(editorName).createElement("A");
		aNode.setAttribute("href", theurl);
	    aNode.innerHTML = range;
		IBM_RTE_getSelectionRange(editorName).deleteContents();
	    IBM_RTE_insertNodeAtSelection(IBM_RTE_getWindow(editorName),aNode);
		var a = IBM_RTE_getDocument(editorName).body.getElementsByTagName("A");
	    var xxx=0;
		while(a[xxx] != null){
			 a[xxx].setAttribute("odc", a[xxx].getAttribute("href"));
			 xxx++;
		}
		createCursorAfterNode_Mozilla(aNode,editorName);
}
//**NS this is a generic function to create and display cursor after any node.
function createCursorAfterNode_Mozilla(aNode,editorName)
{
	var doc = IBM_RTE_getDocument(editorName);
	var range = doc.createRange();
	range.setStartAfter(aNode);
	range.setEndAfter(aNode);
    createCursor(range);
	IBM_RTE_setFocusToContentWindow(editorName);
}
//**END Defect - 170277 & 174102 Mozilla: No Drop Down box in Hperlink dialog box
function IBM_RTE_doLink(editorName, buttonElement, locale, images, directory, isBidi, prompttext) {    
	if(!IBM_RTE_isMozilla()){
		if(isAccessible) 
			this.opener.IBM_RTE_backup(editorName);
		else
			IBM_RTE_backup(editorName);
	}
    if (IBM_RTE_isMozilla()) {
		//**START Defect - 170277 & 174102 Mozilla: No Drop Down box in Hperlink dialog box
		createLink_Mozilla(editorName, buttonElement, locale, images, directory, isBidi, prompttext);
		//**END Defect - 170277 Mozilla: No Drop Down box in Hperlink dialog box
    }else {
		var a = IBM_RTE_getDocument(editorName).body.getElementsByTagName("A");
		var xxx=0;

		while(a[xxx] != null){
			if(a[xxx].odc != null)
				a[xxx].href = a[xxx].odc;
				xxx++;
		}
        IBM_RTE_getWindow(editorName).focus();
        IBM_RTE_getDocument(editorName).execCommand('createlink', true, null);

		var a = IBM_RTE_getDocument(editorName).body.getElementsByTagName("A");
	    var xxx=0;

		while(a[xxx] != null){
			 a[xxx].setAttribute("odc", a[xxx].href);
			 xxx++;
		}
	   }
}	

function IBM_RTE_doFontStyle(editorName, ctrl, commandname) {
    if (ctrl) {
        var hType = ctrl.options[ctrl.selectedIndex].value;

        if (hType != '') {
            IBM_RTE_getWindow(editorName).focus();
		if(!IBM_RTE_isMozilla()){
			if(isAccessible) 
				this.opener.IBM_RTE_backup(editorName);
			else
				IBM_RTE_backup(editorName);
		}
            IBM_RTE_getDocument(editorName).execCommand(commandname, false, hType);
    }}
}

function IBM_RTE_getOffsetTop(ctrl) {
    var offsetTop = ctrl.offsetTop;
    var offsetParent = ctrl.offsetParent;

    while (offsetParent) {            
        offsetTop += offsetParent.offsetTop;
        offsetParent = offsetParent.offsetParent;
    }
    return offsetTop;
}

function IBM_RTE_getOffsetLeft(ctrl) {
    var offsetLeft = ctrl.offsetLeft;
    var offsetParent = ctrl.offsetParent;

    while (offsetParent) {
        offsetLeft += offsetParent.offsetLeft;
        offsetParent = offsetParent.offsetParent;
    }
    return offsetLeft;
}

function IBM_RTE_doColor(editorName, clrType, buttonElement, locale, images, directory, isBidi) {
  if(!IBM_RTE_isMozilla()){
		if(isAccessible) 
			this.opener.IBM_RTE_backup(editorName);
		else
			IBM_RTE_backup(editorName);
	}
    if (IBM_RTE_isMozilla()) {
        if (clrType.localeCompare("backcolor") == 0) 
             clrType = "hilitecolor"; 
    }
    IBM_RTE_getWindow(editorName).focus();
    var jrl = "colorPicker_accessible.jsp";
    jrl = directory + jrl;
    var wcp_colorWin = window.open(jrl + '?locale=' + locale + '&images=' + images + '&editorName=' + editorName + '&isBidi=' + isBidi  + '&clrType=' + clrType, 'colors', 'scrollbars=0, toolbar=0, statusbar=0, width=320, height=450, left=' + ((screen.width - 260) / 2) + ', top=' + ((screen.height - 150) / 2));
    wcp_colorWin.focus();
}
//**START Defect 172801 the selected texts can't be replaced with inserted table
function IBM_RTE_insertTableAtSelection(win, insertNode) {
	 var sel = win.getSelection();
    var range = sel.getRangeAt(0);
	range.deleteContents();
	range.insertNode(insertNode);
}
//**END Defect 172801 the selected texts can't be replaced with inserted table
// Used to insert elements in Mozilla editor.
function IBM_RTE_insertNodeAtSelection(win, insertNode) {
    var sel = win.getSelection();
    var range = sel.getRangeAt(0);

    var container = range.endContainer;
    var pos = range.endOffset;

    // make a new range for the new selection
    range = document.createRange();

    if (container.nodeType == 3 && insertNode.nodeType == 3) {
        // if we insert text in a textnode, do optimized insertion
        container.insertData(pos, insertNode.nodeValue);
        // put cursor after inserted text
        range.setEnd(container, pos + insertNode.length);
        range.setStart(container, pos + insertNode.length);        
    }else {
        var afterNode;
        if (container.nodeType == 3) {
            // when inserting into a textnode we create 2 new textnodes and put the insertNode in between
            var textNode = container;
            container = textNode.parentNode;
            var text = textNode.nodeValue;

            // text before the split
            var textBefore = text.substr(0,pos);
            // text after the split
            var textAfter = text.substr(pos);
            
            var beforeNode = document.createTextNode(textBefore);
            var afterNode = document.createTextNode(textAfter);
            
            // insert the 3 new nodes before the old one
            container.insertBefore(afterNode, textNode);
            container.insertBefore(insertNode, afterNode);
            container.insertBefore(beforeNode, insertNode);
            
            // remove the old node
            container.removeChild(textNode);
        }
        else {
            // else simply insert the node
            afterNode = container.childNodes[pos];
            container.insertBefore(insertNode, afterNode);
        }
        range.setEnd(afterNode, 0);
        range.setStart(afterNode, 0);
    }
    sel.addRange(range);
}

function IBM_RTE_doTableInsert(editorName, buttonElement, locale, images, directory, tab_array, isBidi) {
	var numrows = tab_array[0];
	var numcols = tab_array[1];

    IBM_RTE_getWindow(editorName).focus();
	if(!IBM_RTE_isMozilla()){
		if(isAccessible) 
			this.opener.IBM_RTE_backup(editorName);
		else
			IBM_RTE_backup(editorName);
	}
    var wcp_tableWin = window.open(directory + 'table_ie.jsp?locale=' + locale + '&images=' + images + '&editorName=' + editorName + '&isBidi=' + isBidi, 'table', 'scrollbars=0, toolbar=0, statusbar=0,resizable=yes, width=200, height=190, left=' + ((screen.width - 200) / 2) + ', top=' + ((screen.height - 170) / 2));
    wcp_tableWin.focus();
}
//**NS start Defect# 170756 Hard to enter one character in table.
function reloadDocumentContent(editorName)
{
	var documentContent = IBM_RTE_getDocument(editorName).body.innerHTML;
	//reloads the html content, after rplacing <TD>&nbsp;</TD> to <TD></TD>
	documentContent = documentContent.replace(/\<TD\>&nbsp;\<\/TD\>/g, "<TD></TD>");	
	IBM_RTE_getDocument(editorName).body.innerHTML = documentContent.replace(/\<TH\>&nbsp;\<\/TH\>/g, "<TH></TH>");
}
//**END Defect# 170756

function IBM_RTE_getEditorHTML(editorName) {
    var bodyDir =  IBM_RTE_getDocument(editorName).body.dir;
    var lnks = IBM_RTE_getDocument(editorName).getElementsByTagName("link");
    var iHead = '<html>\n<head>\n<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />\n';
    var ttls = IBM_RTE_getDocument(editorName).getElementsByTagName("title");

	if ((ttls) && ttls.length > 0) {
        iHead += "<title>" + ttls[0].text + "</title>\n";
    }
    
    for (i = 0; (lnks) && (i < lnks.length); i++) {
        iHead += "<link rel='" + lnks[0].rel + "' href='" + lnks[i].href + "' />\n";
    }
    iHead += "</head>\n";

    if (bodyDir.localeCompare('ltr') == 0)
        iHead += "<body dir='ltr'>\n";
    else if (bodyDir.localeCompare('rtl') == 0) 
        iHead += "<body dir='rtl'>\n";
    else
        iHead += "<body>\n";

    iHTML = IBM_RTE_getDocument(editorName).body.innerHTML;
    iFoot = "</body>\n</html>";
	
    iAll = iHead + iHTML + iFoot;
    return iAll;
}


function IBM_RTE_getEditorContent(editorName, mode,encoding) {
    var bodyDir =  IBM_RTE_getDocument(editorName).body.dir;
    var lnks = IBM_RTE_getDocument(editorName).getElementsByTagName("link");
    var iHead = '<html>\n<head>\n<meta http-equiv="content-type" content="text/html; charset="';
	iHead += encoding;
	iHead += "\" />\n";
    var ttls = IBM_RTE_getDocument(editorName).getElementsByTagName("title");
    if ((ttls) && ttls.length > 0) {
        iHead += "<title>" + ttls[0].text + "</title>\n";
    }
    
    for (i = 0; (lnks) && (i < lnks.length); i++) {
        iHead += "<link rel='" + lnks[0].rel + "' href='" + lnks[i].href + "' />\n";
    }

    iHead += "</head>\n";

    if (bodyDir.localeCompare('ltr') == 0)
        iHead += "<body dir='ltr'>\n";
    else if (bodyDir.localeCompare('rtl') == 0) 
        iHead += "<body dir='rtl'>\n";
    else
        iHead += "<body>\n";
    if (IBM_RTE_getEditorMode(editorName) == 0){
                 iHTML = IBM_RTE_getDocument(editorName).body.innerHTML;
		 IBM_RTE_getDocument(editorName).body.innerText = iHTML;
		 iText = IBM_RTE_getDocument(editorName).body.innerText;
		 IBM_RTE_getDocument(editorName).body.innerHTML = iText;
       iHTML = IBM_RTE_getDocument(editorName).body.innerHTML;
    }
    else if (IBM_RTE_getEditorMode(editorName) == 1) {
         if (IBM_RTE_isMozilla()) {
            var html = IBM_RTE_getDocument(editorName).body.ownerDocument.createRange();
            html.selectNodeContents(IBM_RTE_getDocument(editorName).body);
            iHTML = html.toString();
         }
         else {
            iHTML = IBM_RTE_getDocument(editorName).body.innerText;
         }
    }      

    iFoot = "</body>\n</html>";
	iHTML= iHTML.replace(/\<TH\>\<\/TH\>/g, "<TH>&nbsp;</TH>");
	iHTML= iHTML.replace(/\<TD\>\<\/TD\>/g, "<TD>&nbsp;</TD>");
	
    iAll = iHead + iHTML + iFoot;

	return iAll;
}

function IBM_RTE_getEditorHTML2(editorName, metaCnt) {
    var bodyDir =  IBM_RTE_getDocument(editorName).body.dir;
    var lnks = IBM_RTE_getDocument(editorName).getElementsByTagName("link");
    var iHead = '<html>\n<head>\n';
    iHead += '<meta http-equiv="content-type" content="' + metaCnt + '" />\n';
    var ttls = IBM_RTE_getDocument(editorName).getElementsByTagName("title");
    if ((ttls) && ttls.length > 0) {
        iHead += "<title>" + ttls[0].text + "</title>\n";
    }
    for (i = 0; (lnks) && (i < lnks.length); i++) {
        iHead += "<link rel='" + lnks[0].rel + "' href='" + lnks[i].href + "' />\n";
    }
    iHead += "</head>\n";
    if (bodyDir.localeCompare('ltr') == 0)
        iHead += "<body dir='ltr'>\n";
    else if (bodyDir.localeCompare('rtl') == 0) 
        iHead += "<body dir='rtl'>\n";
    else
        iHead += "<body>\n";

    iHTML = IBM_RTE_getDocument(editorName).body.innerHTML;
    iFoot = "</body>\n</html>";
    iAll = iHead + iHTML + iFoot;
    return iAll;
}
function IBM_RTE_getEditorHTMLFragment(editorName) {
	if(IBM_RTE_getEditorMode(editorName) == 0){
	 BrTag=IBM_RTE_removeDanglingP_BR(editorName); 
		if (IBM_RTE_isMozilla())
	   {
			var a = IBM_RTE_getDocument(editorName).body.getElementsByTagName("A");
			var xxx=0;
			while(a[xxx] != null)
			{
				if(a[xxx].getAttribute("odc")!=null&&a[xxx].getAttribute("odc") != "") 
				{
					a[xxx].setAttribute("href", a[xxx].getAttribute("odc"));
					a[xxx].removeAttribute("odc", 0);	
				}
				else
					a[xxx].removeAttribute("odc", 0);
		  	    
				xxx++;
			}
			
			var img = IBM_RTE_getDocument(editorName).body.getElementsByTagName("img");
			xxx=0;
			
			while(img[xxx] != null)
			{
				if(img[xxx].getAttribute("odc")!=null&&img[xxx].getAttribute("odc")!= "") 
				{
					img[xxx].setAttribute("src",img[xxx].getAttribute("odc"));
					img[xxx].removeAttribute("odc", 0);
				}
				else
					img[xxx].removeAttribute("odc", 0);
				
				xxx++;
			}
	   }
		else
	   {
			var a = IBM_RTE_getDocument(editorName).body.getElementsByTagName("A");
			var xxx=0;
			while(a[xxx] != null){
				if(a[xxx].getAttribute("odc") != "" && a[xxx].getAttribute("odc") != null) {
					a[xxx].setAttribute("href", a[xxx].getAttribute("odc"));
					a[xxx].removeAttribute("odc");				
				}
				 xxx++;
			}
			var img = IBM_RTE_getDocument(editorName).body.getElementsByTagName("img");
			var xxx=0;
			while(img[xxx] != null){
				if(img[xxx].getAttribute("odc") != "" && img[xxx].getAttribute("odc") != null) {
					img[xxx].setAttribute("src", img[xxx].getAttribute("odc"));
					img[xxx].removeAttribute("odc");
				}
				xxx++;
			}
	   }
			var hh = IBM_RTE_getDocument(editorName).body.innerHTML;
			if(hh.match(/<xmp id=9669 contentEditable=false style=\"DISPLAY: inline\">.*?<\/xmp>/i) || (/<xmp style=\"DISPLAY: inline\" id=9669 contentEditable=false>.*?<\/xmp>/i) != null )
			{
				hh = hh.replace(/<xmp id=9669 contentEditable=false style=\"DISPLAY: inline\">.*?<\/xmp>/i, "");
				hh = hh.replace(/<xmp style=\"DISPLAY: inline\" id=9669 contentEditable=false>.*?<\/xmp>/i, "");
			}
			hh = hh.replace(/&#[0-2]?[0-9];/g,"");
			hh = hh.replace(/&#3[01];/g,"");
			hh = stripControlCharacters(hh);
			return hh;
}
	else{
		if (IBM_RTE_isMozilla()) {
			var r = IBM_RTE_getDocument(editorName).body.ownerDocument.createRange();
			r.selectNodeContents(IBM_RTE_getDocument(editorName).body);
			var html = r.toString();
			if(BrTag && IBM_RTE_getDocument(editorName).body.childNodes.length == 0 ) {
				html = "<BR>" + html;
				BrTag = false;
			}
			html = html.replace(/&#[0-2]?[0-9];/g,"");
			html = html.replace(/&#3[01];/g,"");
			html = stripControlCharacters(html);

			return html;
         }
		else{
			var tt = IBM_RTE_getDocument(editorName).body.innerText;
			if(tt.match(/<xmp id=9669 contentEditable=false style=\"DISPLAY: inline\">.*?<\/xmp>/i) != null)
					tt = tt.replace(/<xmp id=9669 contentEditable=false style=\"DISPLAY: inline\">.*?<\/xmp>/i, "");
			tt = tt.replace(/&#[0-2]?[0-9];/g,"");
			tt = tt.replace(/&#3[01];/g,"");
			tt = stripControlCharacters(tt);
			return tt;
		}
	}
}

function IBM_RTE_setEditorHTML(editorName, content) {
if(IBM_RTE_getEditorMode(editorName) != 0){
	var Text1 = new String(content);

	Text1 = Text1.replace(/&#[0-2]?[0-9];/g,"");
	Text1 = Text1.replace(/&#3[01];/g,"");
	Text1 = stripControlCharacters(Text1);
	
	if(IBM_RTE_isMozilla()){
		var html = IBM_RTE_getDocument(editorName).createTextNode(Text1);
		IBM_RTE_getDocument(editorName).body.innerHTML = "";
		IBM_RTE_getDocument(editorName).body.appendChild(html);
	}
	else
		IBM_RTE_getDocument(editorName).body.innerText = Text1;
}else {
	if (IBM_RTE_isMozilla()) 
	{
		var Text1 = new String(content);

		Text1 = Text1.replace(/&#[0-2]?[0-9];/g,"");
		Text1 = Text1.replace(/&#3[01];/g,"");
		Text1 = stripControlCharacters(Text1);
	
		var str3    = "" + "<a .*?[^\/]>";
		var re3     = new RegExp(str3.toString(),"gi");
		var matches = Text1.match(re3);
		var xx      = 0;
		if(matches != null)
			while(true)
			{
				if(matches[xx] == null) break;
				
				var Smatch = matches[xx];
		
				var str69  = "" + "href=\"'";
				var re69   = new RegExp(str69.toString(),"i");

				var str96  = "" + "href='\"";
				var re96   = new RegExp(str96.toString(),"i");
				if(re69.test(Smatch) == false && re96.test(Smatch) == false && isCustomTag(Smatch)==false){
					var cmpts = matches[xx].match(/href=(["'])([^\1]*?)\1/i);
					if(cmpts != null)
					{
						var quot  = cmpts[1];
						var href  = cmpts[2];
						temp = "href=\"" + href.replace(/\"/gi,"'") + "\" odc=\"" + href.replace(/\"/gi,"'") + "\" "; 	
						matches[xx] = matches[xx].replace(/href=(["'])([^\1]*?)\1/i, temp);
						Text1 = Text1.replace(Smatch.toString(), matches[xx].toString()); 
					}
				}
				xx++;
			}
			var matches = Text1.match(/<img.*?src=(['"])[^\1]*?\1.*?[\/]?>/gi);
			var xx      = 0;
		
			if(matches != null)
				while(true)
				{
					if(matches[xx] == null) break;

					var Smatch = matches[xx];
					var str69  = "" + "src=\"'";
					var re69   = new RegExp(str69.toString(),"i");
	
					var str96  = "" + "src='\"";
					var re96   = new RegExp(str96.toString(),"i");
					if(re69.test(Smatch) == false && re96.test(Smatch) == false && isCustomTag(Smatch)==false)
					{
						var cmpts = matches[xx].match(/src=(["'])([^\1]*?)\1/i);
						if(cmpts != null)
						{
							var quot = cmpts[1];
							var src  = cmpts[2];
							temp = "src=\"" + src.replace(/\"/gi,"'") + "\" odc=\"" + src.replace(/\"/gi,"'") + "\" "; 	
							matches[xx] = matches[xx].replace(/src=(["'])([^\1]*?)\1/i, temp);
							Text1 = Text1.replace(Smatch.toString(), matches[xx].toString()); 
						}
					}
					xx++;
				}
    }
	else
	{
		var Text1 = new String(content);

		Text1 = Text1.replace(/&#[0-2]?[0-9];/g,"");
		Text1 = Text1.replace(/&#3[01];/g,"");
		Text1 = stripControlCharacters(Text1);
		if(Text1.match(/<xmp id=9669 contentEditable=false style=\"DISPLAY: inline\">.*?<\/xmp>/i) == null)
			Text1 = "<xmp id=9669 contentEditable=false style=\"DISPLAY: inline\"></xmp>" + Text1;
		var str3    = "" + "<a .*?[^/]>";
		var re3     = new RegExp(str3.toString(),"gi");
		var matches = Text1.match(re3);
		var xx      = 0;
		
		if(matches != null)
			while(true)
			{
				if(matches[xx] == null) break;
				
				var Smatch = matches[xx];
				var str69  = "" + "href=\"'";
				var re69   = new RegExp(str69.toString(),"i");
				var str96  = "" + "href='\"";
				var re96   = new RegExp(str96.toString(),"i");
				if(re69.test(Smatch) == false && re96.test(Smatch) == false && isCustomTag(Smatch)==false)
				{				
					var cmpts = matches[xx].match(/href=(["'])([^\1]*?)\1/i);
					if(cmpts != null)
					{
						var quot  = cmpts[1];
						var href  = cmpts[2];
						temp = "href=\"" + href.replace(/\"/gi,"'") + "\" odc=\"" + href.replace(/\"/gi,"'") + "\" "; 	
						matches[xx] = matches[xx].replace(/href=(["'])([^\1]*?)\1/i, temp);
						Text1 = Text1.replace(Smatch.toString(), matches[xx].toString()); 
					}
				}
				xx++;
			}	
		var matchesImg = Text1.match(/<img.*?src=(['"])[^\1]*?\1.*?[\/]?>/gi);
		xx			   = 0;
			
		if(matchesImg != null)
			while(true)
			{
				if(matchesImg[xx] == null) break;

				var Smatch = matchesImg[xx];
				var str69  = "" + "src=\"'";
				var re69   = new RegExp(str69.toString(),"i");
	
				var str96  = "" + "src='\"";
				var re96   = new RegExp(str96,"i");
				if(re69.test(Smatch) == false && re96.test(Smatch) == false && isCustomTag(Smatch)==false)
				{
					var cmpts = matchesImg[xx].match(/src=(["'])([^\1]*?)\1/i);
					if(cmpts != null)
					{
						var quot = cmpts[1];
						var src  = cmpts[2];
						temp = "src=\"" + src.replace(/\"/gi,"'") + "\" odc=\"" + src.replace(/\"/gi,"'") + "\" "; 	
						matchesImg[xx] = matchesImg[xx].replace(/src=(["'])([^\1]*?)\1/i, temp);
						Text1 = Text1.replace(Smatch.toString(), matchesImg[xx].toString()); 
					}
				}
				xx++;
			}
	}
	if(BrTag && IBM_RTE_getDocument(editorName).body.childNodes.length == 0 ) {
				Text1 = "<BR>" + Text1;
				BrTag = false;
	}
	//**START Defect# 170756 Hard to enter one character in table.
	if(isTDBlank)
	{
		Text1 = Text1.replace(/\<TH\>\<\/TH\>/g, "<TH>&nbsp;</TH>");		
		Text1 = Text1.replace(/\<TD\>\<\/TD\>/g, "<TD>&nbsp;</TD>");
	}isTDBlank = true;
	//**END Defect# 170756
IBM_RTE_getDocument(editorName).body.innerHTML = Text1;

	if (IBM_RTE_isMozilla()){
		var a = IBM_RTE_getDocument(editorName).body.getElementsByTagName("A");
		var xxx=0;
		while(a[xxx] != null)
		{
			if(a[xxx].getAttribute("odc") != "" && a[xxx].getAttribute("odc") != null) 
				a[xxx].setAttribute("href", a[xxx].getAttribute("odc"));
			xxx++;
		}

		var b = IBM_RTE_getDocument(editorName).body.getElementsByTagName("IMG");
		var xxx=0;
		while(b[xxx] != null)
		{
			if(b[xxx].getAttribute("odc") != "" && b[xxx].getAttribute("odc") != null) 
				b[xxx].setAttribute("src", b[xxx].getAttribute("odc"));
			xxx++;
		}
	}
	else{
		var a = IBM_RTE_getDocument(editorName).body.getElementsByTagName("A");
		var xxx=0;
		while(a[xxx] != null){
			if(a[xxx].odc != "" && a[xxx].odc != null) 
				a[xxx].href = a[xxx].odc;
			xxx++;
		}

		var b = IBM_RTE_getDocument(editorName).body.getElementsByTagName("IMG");
		var xxx=0;
		while(b[xxx] != null){
			if(b[xxx].odc!= "" && b[xxx].odc != null) 
				b[xxx].src = b[xxx].odc;
			xxx++;
		}
	}
}
}

function IBM_RTE_flipDocument(editorName) {
	if(!IBM_RTE_isMozilla()){
		if(isAccessible) 
			this.opener.IBM_RTE_backup(editorName);
		else
			IBM_RTE_backup(editorName);
	}
    var origDir =  IBM_RTE_getDocument(editorName).body.dir;
    if ((origDir.localeCompare('') == 0)||(origDir.localeCompare('ltr') == 0))
	{
        IBM_RTE_getDocument(editorName).body.dir = "rtl";
		//**START Defect 171272f_1 'Text Direction Right to Left' doesn't work for a single para
		applyDocumentDirectionToP(editorName,true);
	}
    else if (origDir.localeCompare('rtl') == 0)
	{
        IBM_RTE_getDocument(editorName).body.dir = "ltr";
		//**START Defect 171272f_1 'Text Direction Right to Left' doesn't work for a single para
		applyDocumentDirectionToP(editorName,false);
	}
	
    IBM_RTE_getWindow(editorName).focus();

}
function applyDocumentDirectionToP(editorName,isRTL)
{
	var arr = IBM_RTE_getDocument(editorName).body.getElementsByTagName("P");
	var i = 0;
	while(i < arr.length)
	{
		if(isRTL)
			arr[i].dir = "rtl";
		else
			arr[i].removeAttribute("dir");
		i++;
	}
}

function IBM_RTE_getSelectionRange(editorName)
{
      var doc = IBM_RTE_getDocument(editorName);
      var win = IBM_RTE_getWindow(editorName);

	var ret = null; 
	if(doc.selection)
		ret = doc.selection.createRange();
	else if (win.getSelection)
	{
		var sel = win.getSelection();
		if(sel != null && sel.rangeCount > 0)
			ret = sel.getRangeAt(0);
	}
	return ret;
}

// TODO: detect and clean up redundant DIR tags while preserving other tags.

// Mozilla has no good way to insert a new node to subsume the selection content 
// other than using surroundContents(). SurroundContents() is not very tolerant in
// the selection, such as if the Range partially selects a non-Text node, 
// surroundContents() raises an exception.
// Using separate operations extractContents(), insertNode() will NOT help, it would
// only make things worse, as when insertNode() throws exception, the range content 
// has been removed already. Using surroundContents(), when it throws exception, the 
// range content will remain unchanged.
//
// May need to find a way to mimic IE's behavior???

function IBM_RTE_flipSelection(editorName, dir) {
    // Get a selection range.
    var rng = IBM_RTE_getSelectionRange(editorName);
	if(!IBM_RTE_isMozilla()){
		if(isAccessible) 
			this.opener.IBM_RTE_backup(editorName);
		else
			IBM_RTE_backup(editorName);
	}    
    if (IBM_RTE_isMozilla()) {
        if (rng) {
		//**START Defect 171272 'Text Direction Right to Left' doesn't work for a single para
		var rangeContent = rng.startContainer;
		var parent = rangeContent.parentNode;
		if(parent.nodeName == "BODY" || parent.nodeName == "HTML" || parent.nodeName == "XMP")
		{
			if(isAccessible) {
			this.opener.IBM_RTE_Mozilla_addPTags(editorName);
			}
			else{
			IBM_RTE_Mozilla_addPTags(editorName);
			}
		}
		//**END Defect 171272 'Text Direction Right to Left' doesn't work for a single para
  		var arr = IBM_RTE_getDocument(editorName).body.getElementsByTagName('P');
		var xx = 0;
		while(arr[xx] != null)
		{
			var bodyRange = IBM_RTE_getDocument(editorName).createRange();
			var inRange = false;
			paraRange = bodyRange;
			paraRange.setStartBefore(arr[xx]);
			paraRange.setEndAfter(arr[xx]);
			var START_TO_START = 0;
			var END_TO_START = 3;
			if(paraRange.compareBoundaryPoints(START_TO_START,rng) >= 0){
				if(paraRange.compareBoundaryPoints(END_TO_START,rng) <= 0)
					inRange = true;
			}
			else if(rng.compareBoundaryPoints(END_TO_START,paraRange) <= 0)
				inRange = true;
			if(inRange){
				arr[xx].dir = dir;
			}
			xx++;
		}

		}
    }else {
        if (rng) {
			var parent = rng.parentElement();
			//**START Defect 171272 'Text Direction Right to Left' doesn't work for a single para
			if("" == IBM_RTE_getDocument(editorName).body.innerHTML)
			{IBM_RTE_getDocument(editorName).execCommand("formatblock", false, "<p>");}
			//**END Defect 171272 'Text Direction Right to Left' doesn't work for a single para
			if(isAccessible) {
			this.opener.IBM_RTE_IE_addPTags(editorName);
			}
			else{
			IBM_RTE_IE_addPTags(editorName);
			}
			var arr = IBM_RTE_getDocument(editorName).body.getElementsByTagName('P');
			var xx = 0;
			var enteredInRange = false;
			var paraRange;
			while(arr[xx]!=null)
			{
				var inRange = false;
				paraRange = IBM_RTE_getDocument(editorName).body.createTextRange();
				paraRange.moveToElementText(arr[xx]);
				try{
					if(paraRange.compareEndPoints("StartToStart",rng) <= 0){
						if(paraRange.compareEndPoints("EndToStart",rng) >= 0){
							inRange = true;
						}
					}
					else if(rng.compareEndPoints("EndToStart",paraRange) >= 0)
						inRange = true;
				}catch(e){}
				if(inRange){
					parent = arr[xx];
					parent.dir = dir;
					enteredInRange = true;
				}
				else if(enteredInRange)break;
				xx++;
			}
		}
	}
    IBM_RTE_getWindow(editorName).focus();
}
function IBM_RTE_checkEvent(editorName) {

   if (typeof checkEvent != 'undefined') {
       checkEvent();
    }
    
}

// These three functions are actually used in dialogs to highlight 
// buttons when mouse is moved over. Use document instead of 
// IBM_RTE_getDocument() because dialogs are in their own windows.
function setStyle(element, name, value) {
        if (element.style && (element.style[name] != value) )
            element.style[name] = value;
}

function IBM_RTE_btn_mouseover(id) {
        
        var elmt = document.getElementById(id);

        if (elmt) {
            setStyle(elmt, 'color', '#0000FF');
            setStyle(elmt, 'borderColor', '#0000FF');
        }
}

function IBM_RTE_btn_mouseoout(id) {
        
        var elmt = document.getElementById(id);

        if (elmt) {
            setStyle(elmt, 'color', '#405380');
            setStyle(elmt, 'borderColor', '#405380');
        }
}


function IBM_RTE_setATag(editorName) {

        var aTags = IBM_RTE_getDocument(editorName).body.getElementsByTagName("A");
        var len = aTags.length;
        for (var i=0; i < len; i++) {
            var aLink = aTags[i].href;
            if (aLink.substring(0, 5) == "http:") {
                aTags[i].target = "_blank";
            }
            else if (aLink.substring(0, 6) == "https:") {
                aTags[i].target = "_blank";
            }
            else if (aLink.substring(0, 4) == "ftp:") {
                aTags[i].target = "_blank";
            }
            else if (aLink.substring(0, 5) == "file:") {
                aTags[i].target = "_blank";
            }
        }
}

// The following functions are for advanced table support.
// Added in 5.1 release.

//This is a utility function to return the current row where the cursor is.
function IBM_RTE_getCurrentRow(editorName) {

    var selRange = IBM_RTE_getSelectionRange(editorName);
    var currentRow;

    if (IBM_RTE_isMozilla()) {
        currentRow = selRange.startContainer;
        var strName = currentRow.nodeName;
         
        while(strName != "TR") {
            currentRow = currentRow.parentNode;
            if (currentRow == null) break;
            strName = currentRow.nodeName;
        }
    }
    else {
        currentRow = selRange.parentElement();
        var strName = currentRow.tagName;
        
        while(strName != "TR") {
            currentRow = currentRow.parentNode;
            if (currentRow == null) break;
            strName = currentRow.tagName;
        }
    }
 
    return currentRow;
}

//This is a utility function to return the number of cells in one row.
//Nested table in a row is considered as one cell. 
function IBM_RTE_getNumOfCells(editorName, tagName, nextRow) {
   var currentRow = IBM_RTE_getCurrentRow(editorName);

   var cells; 
   if(!nextRow || (nextRow && (currentRow.nextSibling != 'null' && currentRow.nextSibling != 'undefined'))){
	   if(tagName == 'TH')
		cells = currentRow.getElementsByTagName("TH");
	else
		cells = currentRow.getElementsByTagName("TD");
   }else{
	   var row1;
	   if(currentRow.nextSibling != 'null' && currentRow.nextSibling != 'undefined'){
			row1 = currentRow.nextSibling;
			if(tagName == 'TH')
				cells = currentRow.nextSibling.getElementsByTagName("TH");
			else
				cells = currentRow.nextSibling.getElementsByTagName("TD");
		}
		else{

		}
   
   }
   // Can not return cells.length directly because if there is
   // a sub-table embedded, the cells.length will include the sub 
   // table cells. Using nextSibling can solve this issue. 
   // See below.
   var j=0;
   if (cells.length > 0) {
      var cellNode = cells[0];

      if (IBM_RTE_isMozilla()) {
        for (;;) {
           if (cellNode!=null && cellNode.nodeName == tagName){
              j++;
              cellNode = cellNode.nextSibling;     
           }else {
              break;
           }
        }
      }
      else {
        for (;;) {
           if (cellNode!=null && cellNode.tagName == tagName){
              j++;
              cellNode = cellNode.nextSibling;     
           }else {
              break;
           }
        }
      }
   }
   if(nextRow && (currentRow.nextSibling != 'null' && currentRow.nextSibling != 'undefined'))
	   return j-1;
   else
	   return j;
}

//Utility function to create a new cell.
//In Mozilla, <BR> has to be added in each cell.
function IBM_RTE_createNewCell(editorName, thCell) {
    // In Mozilla, the new cell needs to add a <br> to a new cell.
    var doc = IBM_RTE_getDocument(editorName);
    var newCell;
	if(thCell)
		newCell = doc.createElement("TH");
	else
		newCell = doc.createElement("TD");

	if (IBM_RTE_isMozilla()) {
        var brElem = doc.createElement("BR");
        newCell.appendChild(brElem);
    }
	// Defect# 170756 : code removed as per new implementation we do not have &nbsp; in table cells.
	/*else{
		newCell.insertAdjacentText("afterBegin"," ");
	}*/
    return newCell;
}
//**This function is to set cursor in next active cell.
function setActiveCell(activeCell,editorName)
{
	var doc = IBM_RTE_getDocument(editorName);
	var range = doc.createRange();
	var arrActiveCell = activeCell.childNodes;
	if(arrActiveCell.length == 1 && arrActiveCell[0].nodeName != "#text")
	{
		activeCell.removeChild(arrActiveCell[0]);
		activeCell.appendChild(doc.createTextNode(" "));
		activeCell.appendChild(doc.createElement("<br>"));
		arrActiveCell = activeCell.childNodes;
	}	
	range.setStart(arrActiveCell[0],arrActiveCell[0].length);
	range.setEnd(arrActiveCell[0],arrActiveCell[0].length);
	createCursor(range);
	IBM_RTE_setFocusToContentWindow(editorName);
}
//** This function is to place the cursur after table incase if last row/cell deleted.
function setCursorAfterTable(workingTable,editorName)
{
	var par_workingTable = workingTable.parentNode;
	var nextSib_workingTable = workingTable.nextSibling;
	var activeNode = null;
	var doc = IBM_RTE_getDocument(editorName);
	var range = doc.createRange();
	if(nextSib_workingTable != null)
	{//if next sibling is text node
		if(nextSib_workingTable.nodeType == 1)
		{	
			activeNode = nextSib_workingTable;
		}else if(nextSib_workingTable.nodeType == 3)
		{
			var arr = nextSib_workingTable.childNodes;
			var i = 0;			
			if(arr.length == 0)
			{
				activeNode = nextSib_workingTable;
			}
			while(i < arr.length)
			{
				if(arr[i].nodeType == 1)
				{
					activeNode = arr[i];
					break;
				}
				i++;
			}
		}
	}else
	{//create a text node with space and add.
		activeNode = doc.createTextNode(" ");
		par_workingTable.appendChild(activeNode);
		par_workingTable.appendChild(doc.createElement("<br>"));
	}
	range.setStart(activeNode,activeNode.length);
	range.setEnd(activeNode,activeNode.length);
	createCursor(range);
	IBM_RTE_setFocusToContentWindow(editorName);
}
//** This function is to create the cursor at selection range.
function createCursor(range)
{
	if (isGecko())
	{
		var doc1 = range.startContainer.ownerDocument;
		if (doc1)
		{
			var selection = doc1.defaultView.getSelection();
			selection.removeAllRanges();
			selection.addRange(range);			
		}
	} else
	{
		range.select();
	}	
}
//** This function is to delete the row from table.
function IBM_RTE_deleteRow(editorName, text, error_rowDelete)
{
	var currentRow = IBM_RTE_getCurrentRow(editorName);
	if (currentRow == null)
	{
		return;
	}
	var container = currentRow.parentNode;
	var numOfTDCells = IBM_RTE_getNumOfCells(editorName,"TD",false);  
	if(numOfTDCells == 0)
	{
		alert(error_rowDelete);
		return;
	}
	//** Defect 170393 FVT: No dialog box displayed on deleting a row/column in table only in IE
	if (!confirm(text))
	{
		return;
	}
	//** Defect 171057 Row Deletion is not working properly in FireFox,Mozilla,Netscap
	if(IBM_RTE_isMozilla() || IBM_RTE_isNetscape())
	{
		var working_table = container.parentNode ;
		var rows = working_table.rows;
		var nextRow = rows[currentRow.rowIndex+1];
		working_table.deleteRow(currentRow.rowIndex);
		//** Defect 171066 multiple rows can't be deleted by clicking successively
		if(nextRow != null)
		{
			setActiveCell(nextRow.cells[0],editorName);
		}else
		{
			setCursorAfterTable(working_table,editorName);
		}
		
	}else
	{
		container.removeChild(currentRow);
	}
	//When child nodes in TBODY is 0, delete the whole table.
	if (container.nodeName == "TBODY" && container.childNodes.length == 0)
	{
		var parTable = container.parentNode;
		parTable.parentNode.removeChild(parTable);
	}
}
//** This function is to delete the Column from table.
function IBM_RTE_deleteColumn(editorName, text, error_columnDelete)
{
	var currTable = IBM_RTE_getTable(editorName);
	if (currTable == null)
	{
		return;
	}
	var currColIndex = IBM_RTE_getColumnIndex(editorName);
	var currRow = IBM_RTE_getCurrentRow(editorName);
	var currRowIndex = currRow.rowIndex;
	var rows = currTable.rows;
	var i = 0;
	//if the column under question is first column and have TH, no need to delete this
	if(currColIndex == 0)
	{
		var tagname = IBM_RTE_isMozilla() ? rows[0].cells[currColIndex].nodeName : rows[0].cells[currColIndex].tagName;
		if(tagname == 'TH')
		{
			alert(error_columnDelete);
			return;
		}
	}	
	//** Defect 170393 FVT: No dialog box displayed on deleting a row/column in table only in IE
	if (!confirm(text))
	{
		return;
	}
	//Now delete the column under question
	while (i < rows.length && rows[i].cells.length > currColIndex)
	{
		rows[i].removeChild(rows[i].cells[currColIndex]);
		i++;
	}
	//** Defect 171057 in column.
	if(!IBM_RTE_isMozilla())
	{//if browser is IE then no need to place the cursor explicitly.
		return;
	}
	//Now place the cursor to correct position in case of mozilla
	if(currColIndex == currRow.cells.length)
	{//if last column is deleted
		if((currRowIndex + 1) == rows.length)
		{//if last column of last row is deleted.
			setCursorAfterTable(currTable,editorName);
		}else
		{//if last column of any row is deleted except last row
			setActiveCell(rows[currRowIndex + 1].cells[0],editorName);
		}
	}else
	{//other than last column is deleted.
		setActiveCell(rows[currRowIndex].cells[currColIndex],editorName);
	}
}

function IBM_RTE_insertRowAbove(editorName, error_rowInsert) {
	if(!IBM_RTE_isMozilla()){
		if(isAccessible) 
			this.opener.IBM_RTE_backup(editorName);
		else
			IBM_RTE_backup(editorName);
	}
	var currentRow = IBM_RTE_getCurrentRow(editorName);
	if (currentRow == null) return;
	var numOfTHCells = IBM_RTE_getNumOfCells(editorName,"TH",false);  
	var numOfTDCells = IBM_RTE_getNumOfCells(editorName,"TD",false);  
	// find if currentRow is a header row..
	var prevRow = currentRow.previousSibling;
	var isTHCell = false;
	if(prevRow == null || prevRow == 'undefined'){
		// see if it is a purely Header Row
		if( (numOfTHCells + numOfTDCells) == numOfTHCells){
			alert(error_rowInsert); // Put the resource bundle error msg.
			return;
		}
	}
	var newRow = IBM_RTE_getDocument(editorName).createElement("TR");
	var cellToFocus = null;
	for (i=0; i<numOfTHCells; i++) {
		var col = IBM_RTE_createNewCell(editorName, true);
		newRow.appendChild(col);
		if(i == 0)
		{
			cellToFocus = col;
		}
	}
	for (i=0; i<numOfTDCells; i++) {
		var col= IBM_RTE_createNewCell(editorName, false);
		newRow.appendChild(col);
	}
	var container = currentRow.parentNode;
	var newRow=container.insertBefore(newRow, currentRow);
	IBM_RTE_setFocusToContentWindow(editorName);	
}
function IBM_RTE_insertRowBelow(editorName) {
	if(!IBM_RTE_isMozilla()){
		if(isAccessible) 
			this.opener.IBM_RTE_backup(editorName);
		else
			IBM_RTE_backup(editorName);
	}
   var currentRow = IBM_RTE_getCurrentRow(editorName);
    if (currentRow == null) return;
	var numOfTHCells = IBM_RTE_getNumOfCells(editorName,"TH",false);  
	var numOfTDCells = IBM_RTE_getNumOfCells(editorName,"TD",false);  
	if (numOfTDCells == 0) {
		var numTDCells = IBM_RTE_getNumOfCells(editorName,"TD", true);  
		var numTHCells = IBM_RTE_getNumOfCells(editorName,"TH", true);  
		if(numTDCells == -1) {
			numOfTDCells = numOfTHCells -1;
			numOfTHCells = 1;
		}else{
			numOfTHCells = numTHCells;  
			numOfTDCells = numTDCells;  
		}
	}
   var newRow = IBM_RTE_getDocument(editorName).createElement("TR");
	var cellToFocus = null;
   for (i=0; i<numOfTHCells; i++) {
       var col = IBM_RTE_createNewCell(editorName, true);
       newRow.appendChild(col);
	   if(i == 0)
		{
		   cellToFocus = col;
		}
   }
   for (i=0; i<numOfTDCells; i++) {
       var col = IBM_RTE_createNewCell(editorName,false);
       newRow.appendChild(col);
   }

   var container = currentRow.parentNode;
   var prevRow = currentRow.nextSibling;
   var newRow=container.insertBefore(newRow, prevRow);
   IBM_RTE_setFocusToContentWindow(editorName);
}
//Utility function to return the cellIndex in the current row. 
// Cell index starts from 0.
function IBM_RTE_getColumnIndex(editorName) {
    var selRange = IBM_RTE_getSelectionRange(editorName);
    var currentCell;

   if (IBM_RTE_isMozilla()) {
        currentCell = selRange.startContainer;
        var strName = currentCell.nodeName;
        while(strName != "TD" && strName != "TH") {
            currentCell = currentCell.parentNode;
            if (currentCell == null) return null;
            strName = currentCell.nodeName;
        }
    } else {
        currentCell = selRange.parentElement();
        var strName = currentCell.tagName;

        while(strName != "TD" && strName != "TH") {
            currentCell = currentCell.parentNode;
            if (currentCell == null) return null;
            strName = currentCell.tagName;
        }
    }
    return currentCell.cellIndex;
}
//Utility function to return the current table.
function IBM_RTE_getTable(editorName) {
    var currRow = IBM_RTE_getCurrentRow(editorName);
    if (currRow == null) return null;
    var currTable;
    if (IBM_RTE_isMozilla()) {  
        currTable = currRow.parentNode.parentNode;
    }
    else {
        currTable = currRow.parentNode;
        var strName = currTable.tagName;
        while(strName != "TABLE") {
            currTable = currTable.parentNode;
            strName = currTable.tagName;
        }
    }
    return currTable;
}
function IBM_RTE_insertColumn(isLeft, editorName, error_columnInsert) {
	if(!IBM_RTE_isMozilla()){
		if(isAccessible) 
			this.opener.IBM_RTE_backup(editorName);
		else
			IBM_RTE_backup(editorName);
	}
    var currColIndex = IBM_RTE_getColumnIndex(editorName);
	var currTable = IBM_RTE_getTable(editorName);
    if (currTable == null) return;
    var rows = currTable.rows;
    var numOfRows = rows.length;
	var numOfTHCells = 0;
	var numOfTDCells = 0;
	// check to see if the coulmn is header col.
	for (var k = 0 ; k < numOfRows ; k++ ){
		var row = rows[k];
		var col = row.cells[currColIndex];
		var tagname;
		if(IBM_RTE_isMozilla())
			tagname = col.nodeName;
		else
			tagname = col.tagName;
		if(tagname == 'TH')
			numOfTHCells++;
		else
			numOfTDCells++;
	}
	if ((numOfTDCells == 0) && isLeft){
		alert(error_columnInsert);
		return;
	}
	if(currColIndex == 0 && (row.cells[currColIndex+1] == null || row.cells[currColIndex+1] == 'undefined')){
		numOfTHCells = 1;
		numOfTDCells = numOfRows -1;
	}else if(currColIndex == 0){
		numOfTHCells = 0;
		numOfTDCells = 0;
		for (var k = 0 ; k < numOfRows ; k++ ){
			var row = rows[k];
			var col = row.cells[currColIndex+1];
			var tagname;
			if(IBM_RTE_isMozilla())   
				tagname = col.nodeName;
			else
				tagname = col.tagName;
			if(tagname == 'TH')
				numOfTHCells++;
			else
				numOfTDCells++;
		}
	}
	var cellToFocus = null;
    for (i=0; i<numOfRows; i++) {
        var rowElement = rows[i];
        if (rowElement.cells.length > currColIndex) {          
           var cellNode = rowElement.cells[currColIndex];
           var newCell;
		   if((numOfTHCells > 0) && (i < numOfTHCells))
			   newCell = IBM_RTE_createNewCell(editorName, true);
			else 
				newCell = IBM_RTE_createNewCell(editorName, false);
           if (isLeft) {
               rowElement.insertBefore(newCell, cellNode);
           }
           else {
               if (rowElement.cells.length == currColIndex + 1) {
                   rowElement.appendChild(newCell);
               }
               else {
                   cellNode = rowElement.cells[currColIndex + 1];
                   rowElement.insertBefore(newCell, cellNode);
               }
           }
		   if(i == 0)
			{
			   cellToFocus = newCell;
			}
        }
    } //End of for loop.
		IBM_RTE_setFocusToContentWindow(editorName);
}
function IBM_RTE_insertColumnLeft(editorName, error_columnInsert) {
    IBM_RTE_insertColumn(true, editorName, error_columnInsert); 
}
function IBM_RTE_insertColumnRight(editorName, error_columnInsert) {
    IBM_RTE_insertColumn(false, editorName, error_columnInsert); 
}
// In IE, when move from one op to another, the selRange.parentElement 
// becomes BODY for the second op. You have to reselect a cell using 
// mouse or keyboard. This seems like an IE bug. 
// Mozilla does not have this problem. 
// The following two functions are to prove this.
function IBM_RTE_insertColumnLeft1(editorName) {
    var selRange = IBM_RTE_getSelectionRange(editorName);
}
function IBM_RTE_insertColumnRight1(editorName) {
    var selRange = IBM_RTE_getSelectionRange(editorName);
}
//Other issues with the advanced table support:
// 1. After delete a row/column, the selection goes away. This makes sense.
// 2. Undo/redo does not take effect since these new functions are DOM manipulation.

function IBM_RTE_insertPageBreak(editorName){
	if(!IBM_RTE_isMozilla()){
		if(isAccessible) 
			this.opener.IBM_RTE_backup(editorName);
		else{
			IBM_RTE_backup(editorName);
			IBM_RTE_getWindow(editorName).focus();
		}
	}

    var rand = Math.round(Math.random()*100000000000000000);
    var pageBreakID = "pagebreak_"+rand;

    var rng = IBM_RTE_getSelectionRange(editorName);

    newNode = IBM_RTE_getDocument(editorName).createElement("div");

    if (IBM_RTE_isMozilla()) {  
        //Since Mozilla does not support contentEditable attribute, 
        //per M. Kaply's suggestion, use <HR> as the page break indicator.
        newNode.setAttribute("id", pageBreakID);
        newNode.setAttribute("align", "center");
        var newChild = IBM_RTE_getDocument(editorName).createElement("HR");
        newNode.appendChild(newChild);
        newNode.style.pageBreakBefore = "always";

        //Use Koranteng's insertNodeIntoRange() utility function to insert the pagebreak node.
        rng.deleteContents();
        var container = rng.startContainer;
        var pos = rng.startOffset;

        var afterNode = null;
        if (container.nodeType == 3) {
		// when inserting into a textnode
		// we create 2 new textnodes
		// and put the insertNode in between
        var textNode = container;
        container = textNode.parentNode;
        var text = textNode.nodeValue;

		// split the text
        var textBefore = text.substr(0, pos);
        var textAfter = text.substr(pos);

        var beforeNode = IBM_RTE_getDocument(editorName).createTextNode(textBefore);
        afterNode = IBM_RTE_getDocument(editorName).createTextNode(textAfter);

		// insert the 3 new nodes before the old one
        container.insertBefore(afterNode, textNode);
        container.insertBefore(newNode, afterNode);
        container.insertBefore(beforeNode, newNode);

        // remove the old node
		container.removeChild(textNode);
        }
        else {
		// else simply insert the node
            // Somehow, after the new node is inserted, the rest of the container is selected and 
            // deselecting everyting does not seem to work. Mozilla problem ???
		afterNode = container.childNodes[pos];
		container.insertBefore(newNode, afterNode);
        }
	  if(afterNode != null) {
		rng.setEnd(afterNode, 0);
		rng.setStart(afterNode, 0);
	  }       
    }
    else {
		// ** defect 171171, 173351.
        var pTag = IBM_RTE_getDocument(editorName).createElement("p"); 
        newNode.id = pageBreakID; 
        newNode.align = "center"; 
		newNode.unSelectable="on";
        newNode.innerHTML = "<FONT size=3 > ------------ Page Break ------------ </FONT>"; 
        newNode.contentEditable = false; 
        newNode.disabled = true; 
       
        rng.pasteHTML(newNode.outerHTML); 
	    IBM_RTE_getDocument(editorName).body.appendChild(pTag); 
        pTag.innerText=""; 
        pTag.style.margin = "0px"; 
        
        rng.moveToElementText(pTag); 
        rng.select(); 

        var pbNode = IBM_RTE_getDocument(editorName).getElementById(pageBreakID); 
        pbNode.style.pageBreakBefore = "always"; 

    }
}

// When pagebreak indicators are inserted, a new getEditorContent will be 
// needed to remove the indicators for print.
function IBM_RTE_getEditorContentPrint(editorName, mode) {
    var text = "";
    var divNodes = IBM_RTE_getDocument(editorName).getElementsByTagName("div");

    if (IBM_RTE_isMozilla()) {  
        for(i=0; i<divNodes.length; i++) {
            if (divNodes[i].id.substring(0, 10) == "pagebreak_") {
                var childNode = divNodes[i].childNodes[0];
                divNodes[i].removeChild(childNode);
            }       
        }
    }
    else {
        for(i=0; i<divNodes.length; i++) {
            if (divNodes[i].id.substring(0, 10) == "pagebreak_") {
                text = divNodes[i].innerText;
                divNodes[i].innerText = "";
            }
        }
    }

    var editorCnt = IBM_RTE_getEditorContentPrint2(editorName, IBM_RTE_getEditorMode(editorName));  

	if (IBM_RTE_isMozilla()) {  
        for(i=0; i<divNodes.length; i++) {
            if (divNodes[i].id.substring(0, 10) == "pagebreak_") {
                var newChild = IBM_RTE_getDocument(editorName).createElement("HR");
                divNodes[i].appendChild(newChild);
            }       
        }
    }
    else {
        for(i=0; i<divNodes.length; i++) {
            if (divNodes[i].id.substring(0, 10) == "pagebreak_") {
                divNodes[i].innerText = text;
            }
        }
    }

    return editorCnt;
}

function IBM_RTE_getEditorContentPrint2(editorName, mode) {

    if (IBM_RTE_getEditorMode(editorName) == 0){
                 iHTML = IBM_RTE_getDocument(editorName).body.innerHTML;
		 IBM_RTE_getDocument(editorName).body.innerText = iHTML;
		 iText = IBM_RTE_getDocument(editorName).body.innerText;
		 iText = addXMPTags(iText,true);
		// ** Defect 172037
		 IBM_RTE_getDocument(editorName).body.innerHTML = iText;
       iHTML = IBM_RTE_getDocument(editorName).body.innerHTML;
    }
    else if (IBM_RTE_getEditorMode(editorName) == 1) {
         if (IBM_RTE_isMozilla()) {
            var html = IBM_RTE_getDocument(editorName).body.ownerDocument.createRange();
            html.selectNodeContents(IBM_RTE_getDocument(editorName).body);
            iHTML = html.toString();
         }
         else {
            iHTML = IBM_RTE_getDocument(editorName).body.innerText;
         }
    }      

    return iHTML;
}

function IBM_RTE_doPrint(editorName, fileName) {
   
    cnt = IBM_RTE_getEditorContentPrint(editorName, 0);
	if (IBM_RTE_isMozilla()) //making the image URLs absolute
		cnt = IBM_RTE_Mozilla_putAbsoluteURLs(editorName,cnt);
     var win = window.open(fileName, "printPreview", "width=600,height=400,menubar=yes,location=no");
}

function IBM_RTE_Mozilla_putAbsoluteURLs(editorName,cnt)
{
	Text1 = cnt;
	var url = IBM_RTE_getDocument(editorName).URL;
	url = url.substring(0,url.lastIndexOf('/'));
	var matchesImg = Text1.match(/<img .*?src=(['"])[^\1]*?\1[\/]?>/gi);
		xx      = 0;
	if(matchesImg != null)
         while(true){
            if(matchesImg[xx] == null) break;

            var Smatch = matchesImg[xx];
            var cmpts = matchesImg[xx].match(/src=(["'])([^\1]*?)\1/i);
   
            var quot = cmpts[1];
            var src = cmpts[2];

            if(quot == "'"){
               var str2 = "" + "\"";
               var re2 = new RegExp(str2.toString(),"g");

               temp = "src='" + url+"/"+src + "' "; 
               matchesImg[xx] = matchesImg[xx].replace(/src=(["'])([^\1]*?)\1/i, temp);

               // now replace matches[xx] in Text1
               var str9 = "" + Smatch.replace(/\\/g,"\\\\").replace(re2, "\\\"").replace(/\//,"\\\/"); 
               var re9 = new RegExp(str9.toString(),"g");

               Text1 = Text1.replace(re9, matchesImg[xx].toString()); 
             }
             else{
               
               var str10 = "" + "\"";
               var re10 = new RegExp(str10.toString(),"g");

               temp = "src=\"" + url+"/"+src + "\" ";    
      
               matchesImg[xx] = matchesImg[xx].replace(/src=(["'])([^\1]*?)\1/i, temp);
               
               // now replace matches[xx] in Text1
               var str11 = "" + Smatch.replace(/\\/g,"\\\\").replace(re10, "\\\"").replace(/\//g,"\\\/");
               var re11 = new RegExp(str11.toString(),"g");
               
               Text1 = Text1.replace(re11, matchesImg[xx].toString()); 
            }
            xx++;
         }

	return Text1;
}

function IBM_RTE_paraSupport(editorName, buttonElement, locale, images, directory, isBidi){
    IBM_RTE_getWindow(editorName).focus();

	if(!IBM_RTE_isMozilla()){
		if(isAccessible) 
			this.opener.IBM_RTE_backup(editorName);
		else
			IBM_RTE_backup(editorName);
	}
    // calculate the position for the dialog relative to the parent window
    var winx = IBM_RTE_isMozilla() ? window.screenX : window.screenLeft;
    var winy = IBM_RTE_isMozilla() ? window.screenY : window.screenTop;
    var winw = IBM_RTE_isMozilla() ? window.innerWidth : IBM_RTE_getDocument(editorName).body.offsetWidth;
    var winh = IBM_RTE_isMozilla() ? window.innerHeight: IBM_RTE_getDocument(editorName).body.offsetHeight;

    var w = 330;
    var h = 330;
    var x = (winw-w)/2+winx;
    var y = (winh-h)/2+winy;

    var rand = Math.round(Math.random()*100000000000000000);
    var winID = "paraSupport_"+rand;
    var flags = "screenX="+x+",screenY="+y+",left="+x+",top="+y+",width="+w+",height="+h+",resizable=yes,status=no,scrollbars=no,dependent=yes,modal=yes";

	var paraWin = window.open(directory + 'paraSupport.jsp?locale=' + locale + '&images=' + images + '&editorName=' + editorName + '&isBidi=' + isBidi+ '&isAccessible='+isAccessible,winID, flags);
    paraWin.focus();
}

function IBM_RTE_findReplace(editorName, buttonElement, locale, images, directory, isBidi){
    IBM_RTE_getWindow(editorName).focus();

    // calculate the position for the dialog relative to the parent window
    var winx = IBM_RTE_isMozilla() ? window.screenX : window.screenLeft;
    var winy = IBM_RTE_isMozilla() ? window.screenY : window.screenTop;
    var winw = IBM_RTE_isMozilla() ? window.innerWidth : IBM_RTE_getDocument(editorName).body.offsetWidth;
    var winh = IBM_RTE_isMozilla() ? window.innerHeight: IBM_RTE_getDocument(editorName).body.offsetHeight;

    var w = 450;
    var h = 230;
    var x = (winw-w)/2+winx;
    var y = (winh-h)/2+winy;

    var rand = Math.round(Math.random()*100000000000000000);
    var winID = "FindReplace_"+rand;

    var flags = "screenX="+x+",screenY="+y+",left="+x+",top="+y+",width="+w+",height="+h+",resizable=yes,status=no,scrollbars=no,dependent=yes,modal=yes";
	if(IBM_RTE_isMozilla()){
		var win = IBM_RTE_getWindow(editorName);
		win.getSelection().collapse(IBM_RTE_getDocument(editorName).body,0);
	}
	// fix for defect 172035 - new window opened everytime on clicking findReplace button
	if (findReplWin == null)
		findReplWin = window.open(directory + 'findReplace.jsp?locale=' + locale + '&images=' + images + '&editorName=' + editorName + '&isAccessible=' + isAccessible+ '&isBidi=' + isBidi, winID, flags);
	else if (findReplWin.closed)
		findReplWin = window.open(directory + 'findReplace.jsp?locale=' + locale + '&images=' + images + '&editorName=' + editorName + '&isAccessible=' + isAccessible+ '&isBidi=' + isBidi, winID, flags);
	findReplWin.focus();
}


//I don't like this way of restoring comments in Mozilla. It is a hack, 
//not a solution. However, since Mozilla currently does not support contentEditable
//attribute, we probably have to use Mozilla properrietary technologies to mimic IE 
//functions.
function IBM_RTE_MozillaRestoreComments(editorName) {
    if (IBM_RTE_isMozilla()) {
        var commentNodes = IBM_RTE_getDocument(editorName).getElementsByTagName("description");
        for (var i = 0; i < commentNodes.length; i++){  
            var cmtStyle = commentNodes[i].getAttribute("style");
            var XULNS = "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";
            var newNode = IBM_RTE_getDocument(editorName).createElementNS(XULNS, "description");
            newNode.setAttribute("value", commentNodes[i].getAttribute("value"));
            commentNodes[i].parentNode.replaceChild(newNode, commentNodes[i]);
 
			if (cmtStyle != "display: none;"){
                cmtStyle = "display:block; padding:5px; color:red !important; font-style: italic; font-size: 10pt;"
            }
            newNode.setAttribute("style", cmtStyle);
       }
    }
}

function IBM_RTE_getCell(editorName){
	var selRange = IBM_RTE_getSelectionRange(editorName);
	if(selRange == null)return null;
	if( selRange.text == null)return null;
	var currentRow= selRange.parentElement();
	if(currentRow.tagName=="TD" || currentRow.tagName=="TH"){
		return currentRow;
	}else{
		return null;
	}
}

function isBlank(val){
	if(val==null){return true;}
	for(var i=0;i<val.length;i++) {
		if ((val.charAt(i)!=' ')&&(val.charAt(i)!="\t")&&(val.charAt(i)!="\n")&&(val.charAt(i)!="\r")){return false;}
	}
	return true;
}

function IBM_RTE_changeContent(editorName){
var iHTML = IBM_RTE_getDocument(editorName).body.innerHTML;
iHTML= iHTML.replace(/\<TD\>\<\/TD\>/g, "<TD>&nbsp;</TD>");
IBM_RTE_getDocument(editorName).body.innerHTML=iHTML;
}

function IBM_RTE_validateAnchorTags(editorName, anchor)
{
	var quot;
	var i;
	var index;
	var attrList = new Array(" type=", " name="," hreflang="," rel="," rev="," accesskey="," shape="," coords="," tabindex="," onfocus="," onblur=", " onclick="," ondblclick="," onmouseup="," onmousedown="," onmouseover="," onmousemove="," onmouseout="," onkeypress="," onkeydown="," onkeyup="," id="," title="," lang="," dir="," class="," target=");		         
	
	var hrefIndex = anchor.indexOf("href=");

	if(hrefIndex == -1)	
		return anchor;

	if((index = anchor.indexOf("<",hrefIndex)) == -1) 
		index = hrefIndex;
	else 
		if((index = anchor.indexOf("/>")) == -1)
			return anchor;

	if(anchor.charAt(hrefIndex+5) != "\"")
		if(anchor.charAt(hrefIndex+5) != "\'")
			anchor = anchor.substring(0, hrefIndex+5) + "\"" + anchor.substring(hrefIndex+5);

	if(anchor.charAt(hrefIndex+5) == "\"") quot = "\""; else quot = "\'";

	index = anchor.indexOf(" charset=",hrefIndex);

	for(i=0; i<attrList.length; i++)
		if((anchor.indexOf(attrList[i],hrefIndex) < index && index != -1)  || index == -1) 
			if(anchor.indexOf(attrList[i],hrefIndex) != -1) 
				index = anchor.indexOf(attrList[i],hrefIndex);

	if(index != -1)
	{
		if(anchor.charAt(index-1) != quot)
		{
			if(anchor.charAt(index-1) == "\"")
				anchor = anchor.substring(0,index-1) + quot + anchor.substring(index);
			else 
				if(anchor.charAt(index-1) == "\'")
					anchor = anchor.substring(0,index-1)+quot+anchor.substring(index);
				else {
					    var xx=index;
						var flag = false;

						for(xx; xx > hrefIndex+5; xx--){
							if(anchor.charAt(xx) == " ") 
								continue;
							if(anchor.charAt(xx) == "\"" || anchor.charAt(xx) == "'" )
									return anchor;
							else
									return (anchor.substring(0, xx+1) + quot + anchor.substring(xx+1));
							}
					}
			}
	}
	else
	{
		var al = anchor.length;
		if(anchor.charAt(al-2) != quot)
		{
			if(anchor.charAt(al-2) == "\"")
				anchor = anchor.substring(0,al-2)+quot+">";
			else
				if(anchor.charAt(al-2) == "\'")
					anchor = anchor.substring(0,al-2)+quot+">";
				else 
					anchor = anchor.substring(0,al-1)+quot+">";
		}
	}

	return anchor;
}

function isCustomTag(str){
	var match = new String(str);
	str3 = "href=&quot;\"";
	str4 = "src=&quot;\"";
	str5 = "href=\"\"";
	str6 = "src=\"\"";
			if(match.indexOf(str3) == -1)
				if(match.indexOf(str4) == -1)
					if(match.indexOf(str5) == -1)
						if(match.indexOf(str6) == -1)
							return false;
	return true;
}

var index 	  = 0;           
var flag      = false; 
var backup    = new Array();

function IBM_RTE_backup(editorName){ // called on triggers
   if(flag){
		var removed = backup.splice(flag, backup.length - flag);
		
		for (var i = 0; i < removed.length; i++)
			delete removed[i];
	}
	backup[backup.length] = IBM_RTE_getDocument(editorName).body.innerHTML;
	index	 			  = backup.length - 1;
	flag 				  = false;
	
}

function IBM_RTE_handler(event) {// events triggering backup - space, backspace, return, del
	if (event) {
		switch (event.keyCode) {
			case 32: //space
				break;
			case 8://back
				IBM_RTE_backup(editorNameSaved);
				break;
			case 13://return
				IBM_RTE_backup(editorNameSaved);
				break;
			case 46://delete
				IBM_RTE_backup(editorNameSaved);
				break;
			default:
				return;
		}
	}
}

function IBM_RTE_undo(editorName){
	if (backup.length != 1){
		if(index == 0) 
			return;
	
	if(flag == false)
		IBM_RTE_backup(editorName);
	 
	IBM_RTE_getDocument(editorName).body.innerHTML = backup[index - 1];
	
	index = index - 1;
	}
	else{
	 if(flag == false){
		IBM_RTE_backup(editorName);// takes care of first backup if triggers not used

		IBM_RTE_getDocument(editorName).body.innerHTML = "";
		
		index = index - 1;
		}
	}
	
	 flag = index + 1;
}

function IBM_RTE_redo(editorName){
	if(index == backup.length - 1) return;
	//end of backup array
	IBM_RTE_getDocument(editorName).body.innerHTML = backup[index + 1];
	index = index + 1;
	flag 	  = index + 1;
}

function IBM_RTE_delayedBak() {
 if(backup[backup.length - 1] == "") IBM_RTE_backup(editorNameSaved);
  else
   if(IBM_RTE_getDocument(editorNameSaved).body.innerHTML != backup[backup.length - 1])
    IBM_RTE_backup(editorNameSaved);
}

function IBM_RTE_printBackup(){
	for(var i=0; i <= index; i++)
	 alert("<<" + i + ">>  " + backup[i]);
}

function IBM_RTE_doAccessibleCompliant(editorName, buttonElement, locale, images, directory, isBidi) {
        IBM_RTE_getWindow(editorName).focus();

	// calculate the position for the dialog relative to the parent window
    var winx = IBM_RTE_isMozilla() ? window.screenX : window.screenLeft;
    var winy = IBM_RTE_isMozilla() ? window.screenY : window.screenTop;
    var winw = IBM_RTE_isMozilla() ? window.innerWidth : IBM_RTE_getDocument(editorName).body.offsetWidth;
    var winh = IBM_RTE_isMozilla() ? window.innerHeight: IBM_RTE_getDocument(editorName).body.offsetHeight;

    var w = 450;
    var h = 230;
    var x = (winw-w)/2+winx;
    var y = (winh-h)/2+winy;

    var rand = Math.round(Math.random()*100000000000000000);
    var winID = "Toxhtml_"+rand;

    var flags = "screenX="+x+",screenY="+y+",left="+x+",top="+y+",width="+w+",height="+h+",resizable=yes,status=no,scrollbars=no,dependent=yes,modal=yes";
	var xhtmlWin = window.open(directory + 'makeAccessibilityCompliant.jsp?locale=' + locale + '&images=' + images + '&editorName=' + editorName + '&directory=' + directory + '&isBidi=' + isBidi+ '&isAccessible=' + isAccessible, winID, flags);
	xhtmlWin.focus();
}

function addXMPTags(iHTML, remove)
{
	if(remove == true){
		if(iHTML.match(/<xmp id=9669 contentEditable=false style=\"DISPLAY: inline\">.*?<\/xmp>/i) != null){
			iHTML = iHTML.replace(/<xmp id=9669 contentEditable=false style=\"DISPLAY: inline\">.*?<\/xmp>/i, "");
		}
	return iHTML;
	}else{
		if(iHTML.match(/<xmp id=9669 contentEditable=false style=\"DISPLAY: inline\">.*?<\/xmp>/i) == null){
					iHTML = "<xmp id=9669 contentEditable=false style=\"DISPLAY: inline\"></xmp>" + iHTML;
		}
		return iHTML;
	}
}

// only for gecko
var currentParam = -1;
var notnull = false;
var doneNodes = null;
var same_val_across_range_nodes = null;
function checkOverlap(selRange, editorName, which)
{
	if(IBM_RTE_isMozilla()){
		var bodyNode = IBM_RTE_getDocument(editorName).body;
		
		currentParam = -1;
		notnull = false;
		doneNodes = new Array();
		same_val_across_range_nodes = null;

		crawlDomTree(editorName, bodyNode, selRange, which);
		
		if(notnull == true){// display --font--
			return "notnull";
		}else if(same_val_across_range_nodes != null){// there are > 1 text nodes, but all have same font face
			return same_val_across_range_nodes;
		}else{
			return null;
		}
	}
}

function crawlDomTree(editorName, currentElement, selRange, which)
{
	if (currentElement){
		var j;
		var i=0;
		var currentElementChild=currentElement.childNodes[i];
		
		if(currentElementChild != null && currentElementChild.nodeType == 3){
			if(! checkElementAgain(currentElementChild)){
				var gotParam = MozillaIsNodeInRange(editorName, currentElementChild, selRange, which);
				doneNodes[doneNodes.length] = currentElementChild;

				if(currentParam == -1){
					currentParam = gotParam;
				}else	if(gotParam != -1 && gotParam != currentParam && notnull == false){
							if(notnull == false){
								notnull = true; //display --font--
							}
				}else if(gotParam != -1 && gotParam != null){
					same_val_across_range_nodes = gotParam;
				}
			}
		}
		
		while (currentElementChild){
			crawlDomTree(editorName, currentElementChild, selRange, which);
			i++;
			currentElementChild=currentElement.childNodes[i];
		}
	}
}

function checkElementAgain(node)
{
	for(var xxx=0; xxx < doneNodes.length; xxx++){
		if(doneNodes[xxx] == node){
			return true;
		}
	}
	return false;
}

function MozillaIsNodeInRange(editorName, node, selRange, which)
{
	var bodyRange = IBM_RTE_getDocument(editorName).createRange();
	bodyRange.setStartBefore(node);
	bodyRange.setEndAfter(node);

	if(bodyRange.compareBoundaryPoints(0, selRange) >= 0 &&
	   bodyRange.compareBoundaryPoints(3, selRange) <= 0){
				return MozillaGetParam(node, which); //yep, in range :)
	}else if(selRange.compareBoundaryPoints(3, bodyRange) <= 0 &&
		bodyRange.compareBoundaryPoints(0, selRange) <= 0 ){
				return MozillaGetParam(node, which); //yep, in range :)
	}

	return -1;
}

function MozillaGetParam(node, which)
{
	if(which == 1){
			while(node.nodeName != "BODY"){
				if(node.nodeName == "P"){
					if(node.style.fontFamily != "" && node.style.fontFamily != null){
							var mE = MozillaEval(node.style.fontFamily, 1);
							return mE;
					}
				}else if(node.nodeName == "SPAN"){
						if(node.style.fontFamily != "" && node.style.fontFamily != null){
							mE1 = MozillaEval(node.style.fontFamily, 1);
							return mE1;
						}
				}else if(node.nodeName == "FONT"){
					if(node.getAttribute("face") != "" && node.getAttribute("face") != null){
							return MozillaEval(node.getAttribute("face"), 1);
					}
				}
				node = node.parentNode;
			}

		return null;
	}else{//size
		while(node.nodeName != "BODY"){
			if(node.nodeName == "FONT"){
				return MozillaEval(node.getAttribute("size"), 0);
			}

			node = node.parentNode;
		}
		return null;
	}
}

function MozillaEval(fontParam, which)
{
	var fontFaces = new Array("Arial", "Bookman", "Courier", "Garamond", "Lucida Console", "Symbol", "Tahoma", "Times", "Trebuchet", "Verdana");
	var altFontFaces = new Array("arial,helvetica,sans-serif", "bookman old style,new york,times,serif","courier,monaco,monospace,sans-serif","garamond,new york,times,serif", "lucida console,sans-serif", "symbol,fantasy", "tahoma,new york,times,serif", "times new roman,new york,times,serif", "trebuchet ms,helvetica,sans-serif", "verdana,helvetica,sans-serif");

	var fontSizes = new Array("7pt", "9pt", "12pt", "14pt", "18pt", "24pt");

	if(which == 1){//face
		if(fontParam != null){
			fontParam = fontParam.replace(/,[ ]*/gi, ",");
		}

		var face;

		for(var xxx = 0; xxx < fontFaces.length; xxx++){
			if(fontParam.toLowerCase() == fontFaces[xxx].toLowerCase() || 
			   fontParam.toLowerCase() == altFontFaces[xxx].toLowerCase()){
				return fontFaces[xxx];
			}
		}
	}else{//size
		if(fontParam > 0 && fontParam < 7){
			pt = fontSizes[fontParam - 1];
		}else{
			fontParam = 3;
			fontParam = "12pt";
			pt = "12pt";
		}

		return fontParam;
	}
}
// only for gecko

function geckoTableDelete(editorName)
{
	var rng = IBM_RTE_getSelectionRange(editorName);
	if(rng.startOffset == 0)
		return null;
	var parNode = rng.startContainer;
	/**
	 * if (parNode is a text node){ 
	 *	 nothing  to do
	 * } else if(node at startOffset-1 is a table node){
	 *				delete it baby!
	 *          }
	 * }
	 */
	if(parNode.nodeType == 3){
		//alert("Inside nodeType 3");
		return;
	}
	if(parNode.nodeType != 3){
		if(parNode.hasChildNodes){						
			var children = parNode.childNodes;			
			if(children[rng.startOffset-1].tagName == "P"){  
				var childChildren = children[rng.startOffset-1].childNodes;
				if(childChildren.length != 0){
					if(childChildren[childChildren.length - 1].tagName == "TABLE"){
						children[rng.startOffset-1].removeChild(childChildren[childChildren.length - 1]);
					}
				}
			} else if(children[rng.startOffset-1].tagName == "TABLE"){  
						parNode.removeChild(children[rng.startOffset-1]);
			}
		}
	}else if(rng.startOffset == 0){
		var textNode  = parNode;
		var parent      = parNode.parentNode;
		/**
		 * now, recursively try to find the leftSibling
		 *
		 */
		
		deleteLeftSibling(parent, textNode);

	}
}

function IETableDelete(editorName)
{
	var rng = IBM_RTE_getSelectionRange(editorName);
	try
		{
			rng.item(0);
			return false;
		}catch(e)
		{
		return;
		}
	var parNode = rng.parentElement();
	if(parNode.nodeType != 3){
		if(parNode.hasChildNodes){						
			var children = parNode.childNodes;		

			if(children.length != 0 && rng.moveStart("Character") != 0){
				if(children[rng.moveStart("Character")-1].tagName == "P"){  
					var childChildren = children[rng.moveStart("Character")-1].childNodes;

					if(childChildren.length != 0){
						if(childChildren[childChildren.length - 1].tagName == "TABLE"){
							children[rng.moveStart("Character")-1].removeChild(childChildren[childChildren.length - 1]);
						}
					}
				} else if(children[rng.moveStart("Character")-1].tagName == "TABLE"){  
							parNode.removeChild(children[rng.moveStart("Character")-1]);
				}
			}
		}
	}else if(rng.moveStart("Character") == 0){
				var textNode  = parNode;
				var parent  = parNode.parentNode;
				deleteLeftSibling(parent, textNode);
	}
}

function deleteLeftSibling(parent, curNode)
{
	// check if for the parent, the first child is curNode - else go one level up
	var children = parent.childNodes;
	if(children[0] == curNode && parent.tagName != "BODY"){
			deleteLeftSibling(parent.parentNode, parent);
	}else{
		if(curNode.tagName != "BODY"){
			var leftSibling = null;

			for(var xxx = 1; xxx < children.length; xxx++){
				if(children[xxx] == curNode){
					leftSibling = children[xxx - 1];
					break;
				}
			}
			
			if(leftSibling.tagName == "P"){  
				var children = leftSibling.childNodes;
				if(children.length != 0){
					if(children[children.length - 1].tagName == "TABLE"){
						leftSibling.removeChild(children[children.length - 1]);
					}
				}
			} else if(children[rng.startOffset-1].tagName == "TABLE"){  
						parNode.removeChild(children[rng.startOffset-1]);
			}
			if(leftSibling.tagName == "TABLE"){
				parent.removeChild(leftSibling);
			}
		}
	}
}

function printNodes(siblings)
{
	for(var xxx=0; xxx < siblings.length; xxx++){
		alert(siblings[xxx].tagName);
	}
}
// gecko only: backspace table delete

function IBM_RTE_removeDanglingP_BR(editorName)
{
	var bodyNode     = IBM_RTE_getDocument(editorName).body;
	var bodyChildren = bodyNode.childNodes;
	var xmpString     = "<xmp id=9669 contentEditable=false style=\"DISPLAY: inline\"></xmp>";
	/**
	 * IE will always have one node for this case - <P>&nbsp;</P>
	 * Mozilla #@#@#$ has a bit more:
	 *		<br>[#text]
	 *     <p style=".."><br></p>[#text]
	 *     <p style".."><br><br></p>[#text]
	 */
	if(IBM_RTE_isMozilla()){
		switch(bodyChildren.length){
			case 1:
						if(bodyChildren[0].nodeName.toLowerCase() == "br"){
							bodyNode.removeChild(bodyChildren[0]);
							return true; // we removed a <BR> node, so add one before toggling back to view mode.
						}else if(bodyChildren[0].nodeName.toLowerCase() == "p"){ 
							var pKids = bodyChildren[0].childNodes;
							switch(pKids.length){
								case 1:
									if(pKids[0].nodeName.toLowerCase() == "br"){
										bodyNode.removeChild(bodyChildren[0]);
										return true;
									}
								case 2:
									if(pKids[0].nodeName.toLowerCase() == "br" && 
									   pKids[1].nodeName.toLowerCase() == "br" ){
										bodyNode.removeChild(bodyChildren[0]);
										return true;
									}
							}

							return false;
						}else {
								return false;
						}
			case 2: // two nodes, one #text and one <BR> or <BR> inside a <P>
						var textNode = null
						var otherNode = null;

						if(bodyChildren[0].nodeName == "#text"){
							textNode = bodyChildren[0];
							otherNode = bodyChildren[1];
						}else if(bodyChildren[1].nodeName == "#text"){
									textNode = bodyChildren[1];
									otherNode = bodyChildren[0];
						}

						var removeOtherNode = false;
						if(textNode != null && textNode.data == "\n"){ // assure an empty text node
							if(otherNode.nodeName.toLowerCase() == "br"){
								removeOtherNode = true;
							}else 	if(otherNode.nodeName.toLowerCase() == "p"){
								// two cases, innerHTML can be <br> or <br><br>
								var kids = otherNode.childNodes;
								if(kids.length == 1){
									if(kids[0].nodeName.toLowerCase() == "br"){
										removeOtherNode = true;
									}
								}else if(kids.length == 2){
									if(kids[0].nodeName.toLowerCase() == "br" &&
									   kids[1].nodeName.toLowerCase() == "br"){
											removeOtherNode = true;
									}
								}
							}else if(otherNode.nodeName.toLowerCase() == "style" &&
							   otherNode.innerHTML == ""){
								removeOtherNode = true;
							}
							if(removeOtherNode){
								bodyNode.removeChild(otherNode);
								return true;
							}

							return false;
						}
			}
	}else{
		switch(bodyChildren.length){
			case 1: 
				 if(bodyChildren[0] != null &&
					bodyChildren[0].nodeType != 3 && bodyChildren[0].tagName.toLowerCase() != "img" &&
				   bodyChildren[0].tagName.toLowerCase() != "p" && 
				  (bodyChildren[0].innerHTML == "" || 
				   bodyChildren[0].innerHTML.toLowerCase() == xmpString.toLowerCase())){ // IE
						bodyNode.removeChild(bodyChildren[0]);
						return false; // always!
				}
			case 2:
				if(bodyChildren[0] != null &&
				   bodyChildren[1] != null && typeof(bodyChildren[0])=='undefine' &&
				   bodyChildren[0].tagName.toLowerCase() == "p" && 
				  (bodyChildren[0].innerHTML == "" || 
				   bodyChildren[0].innerHTML.toLowerCase() == xmpString.toLowerCase()) &&
				   bodyChildren[1].tagName.toLowerCase() == "p" && 
				  (bodyChildren[1].innerHTML == "" || 
				   bodyChildren[1].innerHTML.toLowerCase() == xmpString.toLowerCase())){ // IE
						bodyNode.removeChild(bodyChildren[1]);
						bodyNode.removeChild(bodyChildren[0]);
						return false; // always!
				}
		}
	}
}
function EncodeInsertCmt(myDateStr)
	{
		//TempInsertCmt declared in openfile.jsp
		TempInsertCmt = TempInsertCmt.replace(/\'/,"&");
		IBM_RTE_insertComment('TheEditor', this, 'en', myDateStr, TempInsertCmt) ;
	}
function IBM_RTE_insertComment(editorName, buttonElement, locale, myDateStr, promptString){
	promptString = promptString.replace(/&/,"'") ;
	IBM_RTE_setFocusToContentWindow(editorName);
    var rng = IBM_RTE_getSelectionRange(editorName);
    if (rng == null) {
        alert("Make a selection first");
        return;
    }

    var rand = Math.round(Math.random()*100000000000000000);
    var commentID = "comment_"+rand;
    txt = window.prompt(promptString, "");
    if (!txt) {
        return;
    }
    //Show the comments if they are hidden
   var commentNodes;
   if (IBM_RTE_isMozilla()) {
       commentNodes = IBM_RTE_getDocument(editorName).getElementsByTagName("description");
       for (i = 0; i < commentNodes.length; i++) {
		   var cmtStyle = commentNodes[i].getAttribute("style");
		   if (cmtStyle != "display:block; padding:5px; color:red !important; font-style: italic; font-size: 10pt;") {
			   commentNodes[i].setAttribute("style", "display:block; padding:5px; color:red !important; font-style: italic; font-size: 10pt;");
		   }
        }//End of for loop
    } else {    
        commentNodes = IBM_RTE_getDocument(editorName).getElementsByTagName("DIV");
          for (i = 0; i < commentNodes.length; i++) {
             var commentId = commentNodes[i].id;
             if (commentId.substring(0, 8) == "comment_") {
                 var childNode = commentNodes[i].childNodes[0];
                 if (childNode.tagName == "SPAN") {
                    if (childNode.style.display != "inline") {
                        childNode.style.display = "inline";
                    }
                 }   
             }
         } //End of for loop. 
    }

    var date = myDateStr;
    var txtComment = "[ " + txt + "  " + date + " ]";
    var newNode;
    var parNode;
    if (IBM_RTE_isMozilla()) {
        parNode = rng.startContainer;
        var strName = parNode.nodeName;

        while(strName != "P" && strName != "DIV" && strName != "BODY"){
            parNode = parNode.parentNode;
            strName = parNode.nodeName;
        }

       //Mozilla does not support contentEditable attribute, so per Doron Rosenberg's 
       //suggestion, here is the workaround to mimic IE contentEditable using Mozilla's 
       //specific technologies.
       var XULNS = "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";
       newNode = IBM_RTE_getDocument(editorName).createElementNS(XULNS, "description");
       newNode.setAttribute("value", txtComment);

      // parNode.appendChild(newNode);
	   IBM_RTE_insertNodeAtSelection(IBM_RTE_getWindow(editorName),newNode);

       //Very strange that I had to set the styles after the new node was appended. 
       //Also, styles can only be set through setAttribute(). When using DIV node, 
       //styles can be set with newNode.style.color = "red";
       newNode.setAttribute("style", "display:block; padding:5px; color:red !important; font-style: italic; font-size: 10pt;")
	   //**START Defect# 169721
	   createCursor(rng);
	   IBM_RTE_setFocusToContentWindow(editorName);
	   //**END Defect# 169721
    } else {
        parNode = rng.parentElement();
        var strName = parNode.tagName;

        while(strName != "P" && strName != "DIV" && strName != "BODY"){
            parNode = parNode.parentNode;
            strName = parNode.tagName;
        } 

        // It is strange that when style or id is set in createElement, it won't work at appendChild time, 
        // need to refresh the page. But if it is set outside createElement, it works fine. 
        // Maybe browser bug???
        // newNode = IBM_RTE_getDocument(editorName).createElement("DIV style='color=red;' id='jz'");
/*
        newNode = IBM_RTE_getDocument(editorName).createElement("DIV");
        newNode.innerText = txtComment;
        newNode.contentEditable = false;
        newNode.style.color = "red";
        newNode.style.fontStyle = "italic";
        newNode.style.fontSize = "10pt";
*/

        newNode = IBM_RTE_getDocument(editorName).createElement("DIV");
        newNode.contentEditable = false;

        var newChild = IBM_RTE_getDocument(editorName).createElement("SPAN");
        newChild.innerText = txtComment;
        newChild.contentEditable = false;
        newChild.style.color = "red";
        newChild.style.fontStyle = "italic";
        newChild.style.fontSize = "10pt";
        newChild.style.display = "inline";
        newChild.id = commentID + "_child";

        newNode.appendChild(newChild);
		newNode.id = commentID;
		rng.pasteHTML(newNode.outerHTML);
        //parNode.appendChild(newNode);
    }

    newNode.id = commentID; 

/*
    parNode.id = commentID + "_parent";

    var index = commentNodes.length;
    commentNodes[index] = new commentNode(newNode, parNode);
*/
}


function IBM_RTE_showHideComments(editorName) {

   var commentNodes;
 
   if (IBM_RTE_isMozilla()) {
       commentNodes = IBM_RTE_getDocument(editorName).getElementsByTagName("description");

       for (i = 0; i < commentNodes.length; i++) {
               var cmtStyle = commentNodes[i].getAttribute("style");
				var display = commentNodes[i].style.display;
				//alert('diaplay : ' + display);
               if (cmtStyle == "padding: 5px; display: block; color: red ! important; font-style: italic; font-size: 10pt;") {
                   commentNodes[i].setAttribute("style", "display:none");
               }
               else {
                   commentNodes[i].setAttribute("style", "display:block; padding:5px; color:red !important; font-style: italic; font-size: 10pt;");
               }
        }//End of for loop
    }
    else {    
        commentNodes = IBM_RTE_getDocument(editorName).getElementsByTagName("DIV");
 
         for (i = 0; i < commentNodes.length; i++) {
             var commentId = commentNodes[i].id;
             if (commentId.substring(0, 8) == "comment_") {
                 var childNode = commentNodes[i].childNodes[0];
				 while (childNode.tagName == "P")
				 {
					childNode = childNode.childNodes[0] ;
				 }
                 if (childNode.tagName == "SPAN") {
                    if (childNode.style.display == "inline") {
                        childNode.style.display = "none";
                    }
                    else if (childNode.style.display == "none"){
                        childNode.style.display = "inline";
                    }
                 }   
             }
         } //End of for loop.
    }
}
//** Start Defect 169577 This section of code is to handel the closing of popup windows.
function closePopUpActionOnLoad()
{
	var parWin = this.opener;
	try{
	//for mozila firefox
	if (navigator.product == 'Gecko') {
		parWin.addEventListener("unload",parentWindowClose,false);
	}else
	{
		parWin.attachEvent("onunload",parentWindowClose);
	}
	}catch(exp)
	{
		
	}
}
function parentWindowClose(){
	window.close();
}
//** End Defect 169577 This section of code is to handel the closing of popup windows.

//I don't like this way of restoring comments in Mozilla. It is a hack, 
//not a solution. However, since Mozilla currently does not support contentEditable
//attribute, we probably have to use Mozilla properrietary technologies to mimic IE 
//functions.

/*function IBM_RTE_MozillaRestoreComments(editorName) {

    if (IBM_RTE_isMozilla()) {

        var commentNodes = IBM_RTE_getDocument(editorName).getElementsByTagName("description");
        for (var i = 0; i < commentNodes.length; i++){
        
            var XULNS = "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";
            var newNode = IBM_RTE_getDocument(editorName).createElementNS(XULNS, "description");
            newNode.setAttribute("value", commentNodes[i].getAttribute("value"));

            commentNodes[i].parentNode.replaceChild(newNode, commentNodes[i]);

            var cmtStyle = commentNodes[i].getAttribute("style")
            if (cmtStyle != "display:none") 
                cmtStyle = "display:block; padding:5px; color:red !important; font-style: italic; font-size: 10pt;"

            newNode.setAttribute("style", cmtStyle);
       }
    }
}

*/
// -------- script_windowControls.js START -----------------
function noDrag(){
       var oData = event.dataTransfer;
       oData.effectAllowed = "move";
       event.cancelBubble=true;
       event.returnValue=false;
}
// -------- script_windowControls.js END -----------------


//moved the contents of the editor.jsp to script editor for the defect 172843

function IBM_RTE_setMode(editorName, mode){
        IBM_RTE_getDocument(editorName).mode = mode;
}
// defect 147838 end
//control character fix
function stripControlCharacters(content)
{

		var xxx = 0;
		while(xxx < renderedCtrlChars.length){
			while(content.indexOf(renderedCtrlChars[xxx]) != -1){
				content = content.replace(renderedCtrlChars[xxx], "");
			}
			xxx++
		}

	return content;
}
//control character fix
function getFirstComments(){
     return FirstComments;
}
function setFirstComments(fcomment){
	FirstComments=fcomment;
}
function Trim(s){
	while ((s.substring(0,1) == ' ')|| (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r'))
	  {
	    s = s.substring(1,s.length);
	  }
	return s;
}
function retainFirstComments(fullString){
	var substr1="";
	var substr2="";
	var gout=true;
    var	secondTime=false;
	while(gout==true){

		if ( fullString.indexOf("<!--") !=-1  || fullString.indexOf("-->")!=-1 ){
		    var  substr3=Trim(fullString.substring(0,	fullString.indexOf("<!--")));
			    if (substr3!="" && secondTime==true)
				    gout=false;
	             else{
			    	substr1= 
					fullString.substring(0,fullString.indexOf("-->")+3);
				    substr2=substr2+substr1;
				fullString=fullString.substring(fullString.indexOf("-->")+3,fullString.length);
				secondTime=true;
 	           }//if1 end
     }// outer if

	 else {
	    gout=false;
	    }// outer end
  }// for while
	setFirstComments(substr2);
	return substr2;
 }//function end

function IBM_RTE_doToggleView(editorNameParam, sourceModeParam, document, designModeParam, theScriptToolbars,theScriptToolbars1){
	//defect 174337
		sourceModeParam = sourceModeParam.replace(/&/,"'");
		designModeParam = designModeParam.replace(/&/,"'");
	
		if(document.getElementById(editorNameParam+"ToolbarFormatButtonSourceCodeImg").title == sourceModeParam) { // to src view
			IBM_RTE_setMode(editorNameParam, 1);
			addBrTag = IBM_RTE_removeDanglingP_BR(editorNameParam);
			if (IBM_RTE_isMozilla()) {// tag fix - begin
				var a = IBM_RTE_getDocument(editorNameParam).body.getElementsByTagName("A");
				var xxx=0;
				while(a[xxx] != null){
					if(a[xxx].getAttribute("odc") != "" && a[xxx].getAttribute("odc") != null){ 
						a[xxx].setAttribute("href", a[xxx].getAttribute("odc"));
						a[xxx].removeAttribute("odc", 0);
					}
					xxx++;
			    }
				var b = IBM_RTE_getDocument(editorNameParam).body.getElementsByTagName("IMG");
				var xxx=0;
				while(b[xxx] != null){
					if(b[xxx].getAttribute("odc") != "" && b[xxx].getAttribute("odc") != null){
						b[xxx].setAttribute("src", b[xxx].getAttribute("odc"));
						b[xxx].removeAttribute("odc", 0);
					}
					xxx++;
				}
				//tag fix - end
                var html = document.createTextNode(IBM_RTE_getDocument(editorNameParam).body.innerHTML);
				// __control char fix__
				var iText = html.data;
				iText = iText.replace(/&#[0-2]?[0-9];/g,"");
			    iText = iText.replace(/&#3[01];/g,"");
				iText = stripControlCharacters(iText); 
				html.data = iText;
				// __control char fix__
                IBM_RTE_getDocument(editorNameParam).body.innerHTML = "";
                IBM_RTE_getDocument(editorNameParam).body.appendChild(html);
            }
            else {
				// tag fix - begin
				var a = IBM_RTE_getDocument(editorNameParam).body.getElementsByTagName("A");
				var xxx=0;
				while(a[xxx] != null){
					if(a[xxx].odc != "" && a[xxx].odc != null) a[xxx].href = a[xxx].odc;
					a[xxx].removeAttribute("odc", 0);
					xxx++;
			    }

				var b = IBM_RTE_getDocument(editorNameParam).body.getElementsByTagName("IMG");
				var xxx=0;
				while(b[xxx] != null){
					if(b[xxx].odc != "" && b[xxx].odc != null) b[xxx].src = b[xxx].odc;
					b[xxx].removeAttribute("odc", 0);
					xxx++;
				}
				//tag fix - end
                iHTML = IBM_RTE_getDocument(editorNameParam).body.innerHTML;

				// __control char fix__
				iHTML = iHTML.replace(/&#[0-2]?[0-9];/g,"");
				iHTML = iHTML.replace(/&#3[01];/g,"");
				iHTML = 	stripControlCharacters(iHTML); 
				// __control char fix__

				iHTML = addXMPTags(iHTML, true);

				var test2=getFirstComments();
					//CMVC defect 128202   chaged code
					if (test2.substring(0,4)=="<!--" && test2!=""  ){
		                IBM_RTE_getDocument(editorNameParam).body.innerText =test2+iHTML;
					}else{
					   IBM_RTE_getDocument(editorNameParam).body.innerText =iHTML;
					}
            }
			document.getElementById(editorNameParam+"ToolbarFormatButtonSourceCodeImg").title = designModeParam;
			document.getElementById(editorNameParam+"ToolbarFormatButtonSourceCodeImg").alt = designModeParam;
		    for (var i=0;i<theScriptToolbars.length;i++){
			    var temparray=theScriptToolbars1[theScriptToolbars[i]];
				for (var j=0;j<temparray.length;j++){	
					var tmparr = editorNameParam + theScriptToolbars[i] + temparray[j];
					if(document.getElementById(tmparr) != null)
	                document.getElementById(tmparr).style.display = 'none';
            }	
		  }

            IBM_RTE_getWindow(editorNameParam).focus();            
        }else {
            // to view mode
			IBM_RTE_setMode(editorNameParam, 0);
			if (IBM_RTE_isMozilla()) {
                var html = IBM_RTE_getDocument(editorNameParam).body.ownerDocument.createRange();
                html.selectNodeContents(IBM_RTE_getDocument(editorNameParam).body);
			   // anchor tag fix - start
				var Text1   = html.toString();
				if(addBrTag){
					Text1 = "<BR>" + Text1;
				}
				var str3    = "" + "<a .*?[^\/]>";
				var re3     = new RegExp(str3.toString(),"gi");
				var matches = Text1.match(re3);
				var xx      = 0;
				if(matches != null)
					while(true){
						if(matches[xx] == null) break;

						var Smatch = matches[xx];
						var validAnchor = IBM_RTE_validateAnchorTags(editorNameParam, matches[xx].toString());
						matches[xx]  = matches[xx].replace(matches[xx].toString(),validAnchor);
						var str69  = "" + "href=\"'";
						var re69   = new RegExp(str69.toString(),"i");
						var str96  = "" + "href='\"";
						var re96   = new RegExp(str96.toString(),"i");
						if(re69.test(Smatch) == false && re96.test(Smatch) == false && IBM_RTE_isCustomTag(Smatch) == false)
						{ 
							var cmpts = matches[xx].match(/href=(["'])([^\1]*?)\1/i);
							if(cmpts != null)
							{
								var quot  = cmpts[1];
								var href  = cmpts[2];
								temp = "href=\"" + href.replace(/\"/gi,"'") + "\" odc=\"" + href.replace(/\"/gi,"'") + "\" "; 	
								matches[xx] = matches[xx].replace(/href=(["'])([^\1]*?)\1/i, temp);
								Text1 = Text1.replace(Smatch.toString(), matches[xx].toString()); 
							}
						}
						xx++;
					}

				// anchor tag fix - end
				// img tag fix - start
				var matches = Text1.match(/<img.*?src=(['"])[^\1]*?\1.*?[\/]?>/gi);
				var xx      = 0;
				if(matches != null)
					while(true){
						if(matches[xx] == null) break;
						var Smatch = matches[xx];
						var str69  = "" + "src=\"'";
						var re69   = new RegExp(str69,"i");
						var str96  = "" + "src='\"";
						var re96   = new RegExp(str96,"i");
						if(re69.test(Smatch) == false && re96.test(Smatch) == false && IBM_RTE_isCustomTag(Smatch)== false)
						{
							var cmpts = matches[xx].match(/src=(["'])([^\1]*?)\1/i);
							if(cmpts != null)
							{
								var quot = cmpts[1];
								var src  = cmpts[2];
								temp = "src=\"" + src.replace(/\"/gi,"'") + "\" odc=\"" + src.replace(/\"/gi,"'") + "\" "; 	
								matches[xx] = matches[xx].replace(/src=(["'])([^\1]*?)\1/i, temp);
								Text1 = Text1.replace(Smatch.toString(), matches[xx].toString()); 
							}
						}
						xx++;
					}
				// img tag fix - end

				// __control char fix__
				Text1 = Text1.replace(/&#[0-2]?[0-9];/g,"");
			    Text1 = Text1.replace(/&#3[01];/g,"");
				Text1 = stripControlCharacters(Text1);
				// __control char fix__

                IBM_RTE_getDocument(editorNameParam).body.innerHTML=Text1;
		    IBM_RTE_MozillaRestoreComments(editorNameParam);
            }
            else { // IE
			   // anchor tag fix - start
				var Text1   = IBM_RTE_getDocument(editorNameParam).body.innerText;
				var str3    = "" + "<a .*?[^/]>";
				var re3     = new RegExp(str3.toString(),"gi");
				var matches = Text1.match(re3);
				var xx      = 0;
	
				if(matches != null)
					while(true){
						if(matches[xx] == null) break;

						var Smatch = matches[xx];
						var validAnchor = IBM_RTE_validateAnchorTags(editorNameParam, matches[xx].toString());
						matches[xx]  = matches[xx].replace(matches[xx].toString(),validAnchor);
						var str69  = "" + "href=\"'";
						var re69   = new RegExp(str69.toString(),"i");
						var str96  = "" + "href='\"";
						var re96   = new RegExp(str96.toString(),"i");
						if(re69.test(Smatch) == false && re96.test(Smatch) == false && IBM_RTE_isCustomTag(Smatch) == false)
						{
							var cmpts = matches[xx].match(/href=(["'])([^\1]*?)\1/i);
							if(cmpts != null)
							{
								var quot  = cmpts[1];
								var href  = cmpts[2];
								temp = "href=\"" + href.replace(/\"/gi,"'") + "\" odc=\"" + href.replace(/\"/gi,"'") + "\" "; 	
								matches[xx] = matches[xx].replace(/href=(["'])([^\1]*?)\1/i, temp);
								Text1 = Text1.replace(Smatch.toString(), matches[xx].toString());
							}
						}
						xx++;
					}

				IBM_RTE_getDocument(editorNameParam).body.innerText = Text1;
				// anchor tag fix - end
			
				// img tag fix - start
				Text1 = IBM_RTE_getDocument(editorNameParam).body.innerText;

				var matches = Text1.match(/<img.*?src=(['"])[^\1]*?\1.*?[\/]?>/gi);
				var xx      = 0;
			
				if(matches != null)
					while(true){
						if(matches[xx] == null) break;

						var Smatch = matches[xx];
						var str69  = "" + "src=\"'";
						var re69   = new RegExp(str69,"i");
						var str96  = "" + "src='\"";
						var re96   = new RegExp(str96,"i");

						if(re69.test(Smatch) == false && re96.test(Smatch) == false && IBM_RTE_isCustomTag(Smatch)== false)
						{
							var cmpts = matches[xx].match(/src=(["'])([^\1]*?)\1/i);
							if(cmpts != null)
							{
								var quot = cmpts[1];
								var src  = cmpts[2];
								temp = "src=\"" + src.replace(/\"/gi,"'") + "\" odc=\"" + src.replace(/\"/gi, "'") + "\" "; 	
								matches[xx] = matches[xx].replace(/src=(["'])([^\1]*?)\1/i, temp);
								Text1 = Text1.replace(Smatch.toString(), matches[xx].toString()); 
							}
						}
						xx++;
					}

				// img tag fix - end

				// __control char fix__
				Text1 = Text1.replace(/&#[0-2]?[0-9];/g,"");
				Text1 = Text1.replace(/&#3[01];/g,"");
				Text1 = 	stripControlCharacters(Text1); 
				// __control char fix__
	
			    IBM_RTE_getDocument(editorNameParam).body.innerText = Text1;
                iText = IBM_RTE_getDocument(editorNameParam).body.innerText;

				iText = addXMPTags(iText, false);

				var	test3=retainFirstComments(iText);
                //CMVC defect 128202   chaged code
				if (test3.substring(0,4)=="<!--" &&  test3!=""){
		        IBM_RTE_getDocument(editorNameParam).body.innerHTML =test3+iText;
				}
				else{
				   IBM_RTE_getDocument(editorNameParam).body.innerHTML=iText;
				}
				//CMVC defect 128202   chaged code
          }
		  if(IBM_RTE_isMozilla()){
		      var a = IBM_RTE_getDocument(editorNameParam).body.getElementsByTagName("A");
		 	  var xxx=0;
			  while(a[xxx] != null){
				if(a[xxx].getAttribute("odc") != "" && a[xxx].getAttribute("odc") != null)
					a[xxx].setAttribute("href", a[xxx].getAttribute("odc"));
				xxx++;
			  }

			  var b = IBM_RTE_getDocument(editorNameParam).body.getElementsByTagName("IMG");
			  var xxx=0;
			  while(b[xxx] != null){
					if(b[xxx].getAttribute("odc") != "" && b[xxx].getAttribute("odc") != null)
						b[xxx].setAttribute("src", b[xxx].getAttribute("odc"));
						xxx++;
  		       }
		  }
		  else{
			  var a = IBM_RTE_getDocument(editorNameParam).body.getElementsByTagName("A");
		 	  var xxx=0;
			  while(a[xxx] != null){
				if(a[xxx].odc != "" && a[xxx].odc != null)
					a[xxx].href= a[xxx].odc;
				xxx++;
			  }

			  var b = IBM_RTE_getDocument(editorNameParam).body.getElementsByTagName("IMG");
			  var xxx=0;
			   while(b[xxx] != null){
				if(b[xxx].odc != "" && b[xxx].odc != null)
					b[xxx].src= b[xxx].odc;
					xxx++;
 		       }
		   }
            
            document.getElementById(editorNameParam+"ToolbarFormatButtonSourceCodeImg").title = sourceModeParam;
      	    document.getElementById(editorNameParam+"ToolbarFormatButtonSourceCodeImg").alt = sourceModeParam;
	        for (var i=0;i<theScriptToolbars.length;i++){
		        var temparray=theScriptToolbars1[theScriptToolbars[i]];
			    for (var j=0;j<temparray.length;j++){	
				    var tmparr=	editorNameParam + theScriptToolbars[i] + temparray[j];
					if(document.getElementById(tmparr) != null)
	                document.getElementById(tmparr).style.display = '';
			    }	
		    }
            IBM_RTE_getWindow(editorNameParam).focus();            
        }
   
}


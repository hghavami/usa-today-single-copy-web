<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/Error.java" --%><%-- /jsf:pagecode --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@taglib
	uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<jsp:useBean id="omnitureTracking" class="com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode" scope="application" />
<HTML>
<f:view locale="#{userLocaleHandler.locale}">
	<f:loadBundle basename="resourcesFile" var="labels" />
	<f:loadBundle basename="resourcesHelpContent" var="help" />
<HEAD>
<h:outputText id="textOmnitureTrackingCode1" escape="false" value="#{omnitureTracking.trackingCodePart1}"></h:outputText>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<META http-equiv="Content-Style-Type" content="text/css">
	<LINK href="/scweb/theme/Master.css" rel="stylesheet" type="text/css">
	<LINK href="/scweb/theme/C_master_blue.css" rel="stylesheet"
		type="text/css">
	<LINK href="/scweb/theme/C_stylesheet_blue.css" rel="stylesheet"
		type="text/css">
	<SCRIPT src="/scweb/scripts/scriptaculous/prototype.js"
		type="text/javascript"></SCRIPT>
	<SCRIPT src="/scweb/scripts/windowutility.js"></SCRIPT>
	<TITLE>Unexpected Error</TITLE>

	<LINK rel="stylesheet" type="text/css" href="theme/stylesheet.css"
		title="Style">

	<SCRIPT language="JavaScript">
	// change Omniture Default Page name
	s.pageName = "Unexpected Error Page";
</SCRIPT>
	<SCRIPT type="text/javascript">

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
window.close();
}</SCRIPT>
	</HEAD>
	<BODY bgcolor="#5677bc"><hx:scriptCollector id="scriptCollector1">
		<TABLE width="95%" border="0" cellpadding="0" cellspacing="1" bgcolor="white">
			<TBODY>
				<TR>
					<TD><IMG border="0" src="/scweb/theme/images/800x20_art2.jpg" width="590" height="20"></TD>
				</TR>
				<TR align="right">
					<TD>${omnitureTracking.trackingCodePart2}
					<hx:outputLinkEx styleClass="outputLinkEx" value="javascript:void(0);return false;" id="linkExCloseWindowTop" onclick="return func_1(this, event);">
						<h:outputText id="textCloseWindowTop" styleClass="outputText" value="Close Window"></h:outputText>
					</hx:outputLinkEx></TD>
				</TR>
				<TR>
					<TD><hx:scriptCollector id="scriptCollector2">
								<TABLE border="0" cellpadding="0" cellspacing="1" width="500">
									<TBODY>
										<TR>
											<TD><!--<h2>This site is experiencing technical difficulties. The issue has been reported and we are working to correct as quickly as possible. Please try again later.</h2>--></TD>
										</TR>
										<TR>
											<TD><h:outputText styleClass="outputText  messages" id="textHeader" value="Unexpected Error Occurred" style="font-size: 12pt; font-weight: bold"></h:outputText></TD>
										</TR>
										<TR>
											<TD><h:messages styleClass="messages" id="messages1" style="color: black" layout="table"></h:messages></TD>
										</TR>
										<TR>
											<TD><h:outputText styleClass="outputText" id="textCustomMessage" rendered="#{requestMessage.showMessage}" style="font-weight: bold" value="#{requestMessage.message}"></h:outputText></TD>
										</TR>
									</TBODY>
								</TABLE>

								<BR>
							</hx:scriptCollector></TD>
				</TR>
				<TR>
					<TD align="right"><hx:outputLinkEx styleClass="outputLinkEx" value="javascript:void(0);return false;" id="linkExCloseWindowBottom" onclick="return func_1(this, event);">
						<h:outputText id="textCloseWindowBottom" styleClass="outputText" value="Close Window"></h:outputText>
					</hx:outputLinkEx></TD>
				</TR>
				<TR>
					<TD><IMG border="0" src="/scweb/theme/images/800x20_art2.jpg" width="590" height="20"></TD>
				</TR>
			</TBODY>
		</TABLE>
	</hx:scriptCollector></BODY>
</f:view>
</HTML>

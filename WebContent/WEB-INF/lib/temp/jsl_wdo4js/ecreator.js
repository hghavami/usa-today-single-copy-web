//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003.  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//==================================================================

/**
 * @class public ECreator
 * @constructor ECreator
 * This class is used to set up and populate a WDO4JS model more efficiently by dierctly
 * using JavaScript code instead of XMI coe.
 *	The constructor takes no parameters.
**/
function ECreator()
{
//DEL 	Profile.Start("ECreator", "<Init>");	// Debug

	/**
	* @method public AddERs
	*	Method is used for setting up a list of EReferences into an EClass instance.
	* @param EClass eclass
	*	An instance of EClass into which we want to add EReference types.
	* @param Array of Array refParams
	*	An Array of Array which contains all paramters for setting up an EReference.
	*	for a list of EReferences.
	* @exception EObjectError
	*	An exception will be thrown if input parameters are null.
	**/
	this.AddERs = function(/*EClass*/ eclass, /*EReferenceParams[][]*/ refParams)
	{
//DEL 		Profile.Start("ECreator", "AddERs");	// Debug

		if (null == eclass)
		{
			var Msg = NlsFormatMsg(unable_create_reference, null);
			Log.error("ECreator.AddERs", Msg);

//DEL 			Profile.End("ECreator", "AddERs");	// Debug

			throw new EObjectError(Msg);
		}

		if(refParams == null)
		{
			var Msg = NlsFormatMsg(unable_create_reference_noreference, null);
			Log.error("ECreator.AddERs", Msg);

//DEL 			Profile.End("ECreator", "AddERs");	// Debug

			throw new EObjectError(Msg);
		}
		//Loop thru all the ERefernecs we need to create
		for(i=0; i<refParams.length; i++)
		{
			//Set all paramaters for a particular EReference
			//Here we expect the array in such an order: ERefName, EClassInstance, lowerBound, upperBound, containment, and iD
			//for example, ["Usr", UClass,1,1,false, true]
			var ref = new EReference(refParams[i][0], refParams[i][1], refParams[i][6]);
			ref.setLowerBound(refParams[i][2]);
			ref.setUpperBound(refParams[i][3]);
			ref.setContainment(refParams[i][4]);
			ref.setID(refParams[i][5]);
			eclass.getEAllReferences().add(ref);
		}

//DEL 		Profile.End("ECreator", "AddERs");	// Debug
	}


  /**
	* @method public AddEAs
	*	Method is used for setting up a list of EAttributes into an EClass instance.
	* @param EClass eclass
	*	An instance of EClass into which we want to add EAttribute types.
	* @param Array of Array attriParams
	*	An Array of Array which contains all paramters for setting up an EAttribute.
	*	for a list of EAttributes.
	* @exception EObjectError
	*	An exception will be thrown if input parameters are null.
	**/
	this.AddEAs = function(/*EClass*/ eclass, /*EAttributeParams[][]*/ attriParams)
	{
//DEL 		Profile.Start("ECreator", "AddEAs");	// Debug

		if (null == eclass)
		{
			var Msg = NlsFormatMsg(add_attribute_2nullclass, null);
			Log.error("ECreator.AddEAs", Msg);

//DEL 			Profile.End("ECreator", "AddEAs");	// Debug

			throw new EObjectError(Msg);
		}

		if(attriParams == null)
		{
 			var args = new Array;
			args[0] = eclass.Name;
			var Msg = NlsFormatMsg(no_attribute_supply, args);
			Log.error("ECreator.AddEAs", Msg);

//DEL 			Profile.End("ECreator", "AddEAs");	// Debug

			throw new EObjectError(Msg);
		}

		var attr = null;
		//Loop thru all the EAttributes we need to create
		for(i=0; i<attriParams.length; i++)
		{
			//Set all paramaters for a particular EAttribute
			//Here we expect the array of
			//for EAttribuet: [Name,Type,0, lowerBound, upperBound, iD] where the last 4 are optional
			//for EAttributeCalculate: [Name,Type,1, expression]
			//and default to 0, 1, and 1 respectively.
			//for example, ["Symbol", "string",1, 1, 1, 1]

			var Params = attriParams[i];
			// if there are 5 or more parameters, then the 5th parameter must be the Cacl flag. Otherwise, default to '0'.
			var Calc = Params.length > 2 ? Params[2] : 0;

			if (Calc == 0 )
			{
				Log.debug("ECreator.addEA","Adding new attribute "+Params[0]);
				attr = new EAttribute(Params[0], Params[1], Params[6]);
				// if there are 3 or more parameters, then the 3rd parameter must be the lowerbound value. Otherwise, default to '1'.
				attr.setLowerBound(Params.length > 3 ? Params[3] : 1);
				// if there are 4 or more parameters, then the 4th parameter must be the upperbound value. Otherwise, default to '1'.
				attr.setUpperBound(Params.length > 4 ? Params[4] : 1);
				attr.setID(Params.length > 5 ? Params[5] : 0);
				
			}
			else
			{
				attr = new EAttributeCalculate(Params[0], Params[3], Params[1]);
			}
			eclass.getEAllAttributes().add(attr);
		}

//DEL 		Profile.End("ECreator", "AddEAs");	// Debug
	}


	/**
	 * @method public Init
	 *	Method is used for adding all EAttribute values and all EReference objects to an EObject instance.
	 * @param EObject eobject
	 *	An instance of EObject into which we want to add EAttribut values and EReference objects.
	 * @param Array attrValueArray
	 *	An Array of array of EAtttribute values for this eobject.
	 * @param Array referenceArray
	 *	An Array of of array of EReference objects for this eobject.
	 * @exception EObjectError
	 *	An exception will be thrown if input parameters are null or a mismatch is detected between model and data.
	 **/
	this.Init = function(/*EObject*/ eobject, /*EAttributeValue[]*/ attrValueArray, /*EReferenceValue[]*/ referenceArray)
	{
//DEL 		Profile.Start("ECreator", "Init");	// Debug

		Log.debug("ECreator.Init", "-- START ---------------------------------------------");
		//ECreator.Init(Pos56, ["390","MACR","14.75","284"], [U1,Port2, S51]);
		if (null == eobject)
		{
			var Msg = NlsFormatMsg(add_reference_2nullclass, null);
			Log.error("ECreator.Init", Msg);

//DEL 			Profile.End("ECreator", "Init");	// Debug

			throw new EObjectError(Msg);
		}

		//set in attribute values
		//Loop thru attrValueArray we need to set.
		//Here we expect the order of attributes coming in is the same as what the schema is set up and
		//values for EAttributeCalculate should not be passed in.

		var attrs = eobject.EClass.getEAllAttributes();

		Log.debug("ECreator.Init", "EClass '"+eobject.EClass.Name+"' defines "+attrs.length+" attributes. "+ (attrValueArray==null?0:attrValueArray.length)+" are being passed");

		//Counter to track the number of EAttributes, excluding EAttributeCalculate
		var counter = 0;
		for(i=0; i<attrs.length; i++)
		{
			var attr = attrs[i];
			var name = attr.Name;
			//If it is an EAttributeCalculate, skip it because it is not supposed to be passed in.
			if (attr.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTECALCULATE)
			{
				Log.debug("ECreator.Init", "Skipping calculated attribute "+name);

				continue;
			}
			
			
			if (attr.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTE )
			{
				if (attrValueArray == null || counter >= attrValueArray.length)
				{
					var Msg = "EClass '"+eobject.EClass.Name+"' defines at least "+(counter+1)+" non-calculate attributes, but only "+counter+" were passed";
					Log.error("ECreator.Init", Msg);

//DEL 					Profile.End("ECreator", "Init");	// Debug

					throw new EObjectError(Msg);
				}
 				if (attr.Type == "id")
				{
					Log.debug("ECreator.Init", "		 - 'xmi:id' = "+ attrValueArray[counter]);
					eobject.ID = attrValueArray[counter++];
				}
				/*else
				{
					Log.debug("ECreator.Init", "		 - '"+name+"' = "+ attrValueArray[counter]);
					eobject.eSet(name, attrValueArray[counter++]);
				}*/
				//If it is a multi-value attribute, we need to loop thru the array of values
				else
				if(
					eobject.eGet(name) != null
					|| attr.getUpperBound() == -1
					|| attr.getUpperBound() > 1
					|| (
						attr.getLowerBound() < attr.getUpperBound()
						&& attr.getUpperBound() > 1
					)
				)
				{
					//Handle the situation that cardinality 0 to n.  In this case, we could have an empty
					//array or null passed in. Such as I(Dir6,["Dir6","wdo4jsmediators"],[[],Dir5])
					if(attrValueArray[counter] == null || attrValueArray[counter].length == 0)
					{
						//set value as null, so GetXXXX() is always set up.
						eobject.eSet(name, null);
						++counter;
						continue;
					}
				   eobject.eSet(name, attrValueArray[counter++]);
				}
				//single value attribute
				else
				{
					Log.debug("ECreator.Init", "		 - '"+name+"' = "+ attrValueArray[counter]);
					eobject.eSet(name, attrValueArray[counter++]);
				}
			}
		}

		//set in references, passed in an array as [U1,[Port2,Port2], S51];
		//Loop thru all the EReferences we need to set value for
		//Here we expect the order of EReferences coming in is the same as what the schema is set up and
		var refs = eobject.EClass.getEAllReferences();

		Log.debug("ECreator.Init", "EClass '"+eobject.EClass.Name+"' defines "+refs.length+" references. "+ (referenceArray==null?0:referenceArray.length)+" are being passed");
		if (refs.length != (referenceArray == null ? 0 : referenceArray.length))
		{
			var args = new Array;
			args[0] = eobject.EClass.Name;
			args[1] = refs.length;
			args[2] = referenceArray.length;
			var Msg = NlsFormatMsg(mismatch_reference_length, args);
			Log.error("ECreator.Init", Msg);

//DEL 			Profile.End("ECreator", "Init");	// Debug

			throw new EObjectError(Msg);
		}

		//set in references, passed in an array as [U1,[Port2,Port2], S51];
		//Loop thru all the EReferences we need to set value for
		for(j=0; j<refs.length; j++)
		{
			var ref = refs[j];
			var refName = ref.Name;

			if (
				eobject.eGet(refName) != null
				|| ref.getUpperBound() == -1
				|| ref.getUpperBound() > 1
				|| (
					ref.getLowerBound() < ref.getUpperBound()
					&& ref.getUpperBound() > 1
				)
			)
			{
				//Handle the situation that cardinality 0 to n.  In this case, we could have an empty
				//array or null passed in. Such as I(Dir6,["Dir6","wdo4jsmediators"],[[],Dir5])
				if(referenceArray[j] == null || referenceArray[j].length == 0)
				{
					//set value as null, so GetXXXX() is always set up.
					eobject.eSet(refName, null);
					continue;
				}

				//eliminate the loop for a better performance
				eobject.eSet(refName,referenceArray[j]);
			}
			else
			{
				Log.debug("ECreator.Init", "		 - '"+refName+"' = "+ (referenceArray[j]==null?"null":referenceArray[j].ID));
				eobject.eSet(refName,referenceArray[j]);
			}
		}
      eobject.Status = 0;
//DEL 		Profile.End("ECreator", "Init");	// Debug
	}

//DEL 	Profile.End("ECreator", "<Init>");	// Debug
}

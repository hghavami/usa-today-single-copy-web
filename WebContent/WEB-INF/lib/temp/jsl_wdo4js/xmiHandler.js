//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003.  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//==================================================================

/**
 * @method public GetArrayOfIDs
 *	 Method to parse a space delimted XMIId string to return them as an array.
 * @param String idref
 *	 A space delimted XMIId string to represent a set of EReferences.
 * @return Array arr
 *	 An array of XMIId's.
 **/
function GetArrayOfIDs(idref)
{
//DEL 	Profile.Start("<Global>", "GetArrayOfIDs");	// Debug

	if (
		null == idref
		|| 0 == idref.length
	)
	{
//DEL 		Profile.End("<Global>", "GetArrayOfIDs");	// Debug

		return new Array();
	}

	while (
		idref.length > 0
		&& " " == idref.substring(0,1)
	)
	{
		idref = idref.substring(1,idref.length);
	}

	var arr = new Array();
	while (idref.length > 0)
	{
		var index = idref.indexOf(" ");
		if (index >= 0)
		{
			arr[arr.length] = idref.substring(0,index);
			idref = idref.substring(index+1,idref.length);
			while (
				idref.length > 0
				&& " " == idref.substring(0,1)
			)
			{
				idref = idref.substring(1,idref.length);
			}
		}
		else
		{
			arr[arr.length] = idref;
			idref = "";
		}
	}

//DEL 	Profile.End("<Global>", "GetArrayOfIDs");	// Debug

	return arr;
}

/**
 * @method public GetXMLString
 *	 Method to get formatted xml string recursively.
 * These are proprietary encoding to get around the portal default data encoding
 * In portal, when we submit a diff with <> for XML elements, such as   <Current>,
 * it will be encoded as &lt;Current&gt;.  This will cause a problem of XMl parsing on server side.
 * So the solution to this problem is that we replace all < and > in a diff with "&lt;'" and "&gt;'" before
 * the diff is submitted back to server.  On server side, we restore it  by reserving the eoncoding.
 * This encoding is simply XML entities plus a '.
 * @param String level
 *	 The level of an XML node.
 * @param Node node
 *	 An XML node which needs to be printed.
 * @param String isPortal
 *	 A flag, if true, we will encode < and > with "&lt;'" and "&gt;'".
 * @return String xml
 *	 An xml string for this node and its child nodes.
 **/
function GetXMLString(level,node, isPortal)
{
//DEL 	Profile.Start("<Global>", "GetXMLString");	// Debug

	var linebreak = "";
	var tabs = "";
	if (
		level != null
		&& typeof(level) == "number"
	)
	{
		linebreak = "\n";
		for (var i = 0; i < level; ++i)
		{
			tabs += "  ";
		}
	}

	var xml = tabs;
	if (node.nodeType == 3)
	{
		var res = (xml + node.nodeValue);

//DEL 		Profile.End("<Global>", "GetXMLString");	// Debug

		return res;
	}

	if(isPortal == 'undefined' || isPortal == false)
   {
      xml += "<";
   }
   else
   {
      xml += "&lt;'";
   }

	if (node.nodeType == 7)
	{
		xml += "?";
	}

	xml += node.nodeName;
	if (node.nodeType == 7)
	{
		xml += " " + node.data;
	}
	else
	{
		for (var i = 0; i < node.attributes.length; ++i)
		{
			var attr = node.attributes.item(i);
			xml += "  " + attr.name + "=\"" +  attr.value + "\"";
		}
	}

	if (node.childNodes.length > 0)
	{
		//xml += ">";
      if(isPortal == 'undefined' || isPortal == false)
      {
         xml += ">";
      }
      else
      {
         xml += "&gt;'";
      }

		var newline = "";
		for (var i = 0; i < node.childNodes.length; ++i)
		{
			var n = node.childNodes[i];
			if (n.nodeType == 1)
			{
				xml += linebreak + GetXMLString((level != null && typeof(level) == "number") ? level+1 : null, n, isPortal);
				newline = linebreak + tabs;
			}
			else
			{
				xml += n.nodeValue;
			}
		}
		//xml += newline + "</" + node.nodeName + ">";
      if(isPortal == 'undefined' || isPortal == false)
      {
         xml += newline + "</" + node.nodeName + ">";
      }
      else
      {
         xml += newline + "&lt;'/" + node.nodeName + "&gt;'";

      }

   	}
	else
	{
		//xml += (node.nodeType == 7) ? " ?>" : " />";
      if(isPortal == 'undefined' || isPortal == false)
      {
         xml += (node.nodeType == 7) ? " ?>" : " />";      }
      else
      {
         xml += (node.nodeType == 7) ? " ?&gt;'" : " /&gt;'";

      }

	}

//DEL 	Profile.End("<Global>", "GetXMLString");	// Debug

	return xml;
}

/**
 * @method public RemoveElement
 *	 Method to remove element from array based on either a name, an index, or an object itself.
 * @param Array arr
 *	 The array needs to be processed.
 * @param String/int/object nameOrIndexOrObject
 *	 The name, index, or object itself points to the object being removed.
 * @return Object obj
 *	 The object has been removed from the array.
 * @exception EObjectError
 *	 An exception will be thrown if the object being removed is null.
 **/
function RemoveElement(arr,nameOrIndexOrObject)
{
//DEL 	Profile.Start("<Global>", "RemoveElement");	// Debug

	var obj = null;
	if ("string" == typeof(nameOrIndexOrObject))
	{
		for (var i = 0; i < arr.length; ++i)
		{
			if (arr[i].Name == nameOrIndexOrObject)
			{
				obj = arr[i];
				arr[i] = null;
			}
		}
	}
	else
	{
		if ("number" == typeof(nameOrIndexOrObject))
		{
			if (
				nameOrIndexOrObject < 0
				|| nameOrIndexOrObject >= arr.length
			)
			{
				var Msg = NlsFormatMsg(invalid_argument, null);
				Log.error("xmiHandler.RemoveElement", Msg);

				throw new EObjectError(Msg);
			}
			obj = arr[nameOrIndexOrObject];
			arr[nameOrIndexOrObject] = null;
		}
		else
		{
			for (var i = 0; i < arr.length; ++i)
			{
				if (arr[i] == nameOrIndexOrObject)
				{
					obj = arr[i];
					arr[i] = null;
				}
			}
		}
	}

	if (null == obj)
	{
		var Msg = NlsFormatMsg(invalid_argument, null);
		Log.error("xmiHandler.RemoveElement", Msg);

//DEL 		Profile.End("<Global>", "RemoveElement");	// Debug

		throw new EObjectError(Msg);
	}

	for (var i = 0; i < arr.length; ++i)
	{
		if (null == arr[i])
		{
			for (var j = i; j < (arr.length - 1); ++j)
			{
				arr[j] = arr[j+1];
			}
			--arr.length;
		}
	}

//DEL 	Profile.End("<Global>", "RemoveElement");	// Debug

	return obj;
}

/**
 * @method public LoadXMLString
 *	 Method to parse an xml from a string.  Please note that the way to parse an XML string
 *	 for IE and Netscape is different.
 * @param String value
 *	 The XML string to be parsed.
 * @return Object xmldoc
 *	 The object which contains XML DOM tree.
 * @exception EObjectError
 *	 An exception will be thrown if parsing fails.
 **/
function LoadXMLString(value)
{
//DEL 	Profile.Start("<Global>", "LoadXMLString");	// Debug

	Log.debug("xmiHandler.htm.LoadXMLString()", "entering");
	var xmlDoc = null;
	if (navigator.appName == "Netscape")
	{
		Log.debug("datagrid_Diff_sample.htm.LoadXMLString()", "appName: " + navigator.appName);
		var parser = new DOMParser();
		xmlDoc = parser.parseFromString(value,"text/xml");
	}
	else
	{
		Log.debug("datagrid_Diff_sample.htm.LoadXMLString()", "loading ActiveXObject") ;
		if (window.ActiveXObject)
		{
			xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
			xmlDoc.async = false;
		}

		if (!xmlDoc.loadXML(value))
		{
			var args = new Array;
			args[0] = xmlDoc.parseError.line;
			args[1] = xmlDoc.parseError.linepos;
			args[2] = xmlDoc.parseError.srcText;
			args[3] = xmlDoc.parseError.reason;
			var Msg = NlsFormatMsg(xml_parse_error, args);
				Log.error("xmiHandler.LoadXMLString", Msg) ;

//DEL 			Profile.End("<Global>", "LoadXMLString");	// Debug

			throw new EObjectError(Msg);
		}

	}

	Log.debug("xmiHandler.htm.LoadXMLString()", "exiting");

//DEL 	Profile.End("<Global>", "LoadXMLString");	// Debug

	return xmlDoc;
}

/**
 * @method public LoadXML
 *	 Method to load xml data based on url path.  Please note that the way to parse an XML
 *	 for IE and Netscape is different.
 * @param String url
 *	 The url to the XML to be parsed.
 * @return Object xmldoc
 *	 The object which contains XML DOM tree.
 * @exception EObjectError
 *	 An exception will be thrown if parsing fails.
 **/
function LoadXML(url)
{
//DEL 	Profile.Start("<Global>", "LoadXML");	// Debug

	var xmlDoc = null;
	if (navigator.appName == "Netscape")
	{
		var p = new XMLHttpRequest();
		p.open("GET",url,false);
		p.send(null);
		xmlDoc = p.responseXML;
	}
	else
	{
		if (window.ActiveXObject)
		{
			xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
			xmlDoc.async = false;
		}

		if (!xmlDoc.load(url))
		{
			var args = new Array;
			args[0] = xmlDoc.parseError.line;
			args[1] = xmlDoc.parseError.linepos;
			args[2] = xmlDoc.parseError.srcText;
			args[3] = xmlDoc.parseError.reason;

			var Msg = NlsFormatMsg(xml_parse_error, args);
			Log.error("xmiHandler.LoadXML", Msg);

//DEL 			Profile.End("<Global>", "LoadXML");	// Debug

			throw new EObjectError(Msg);
		}
	}

//DEL 	Profile.End("<Global>", "LoadXML");	// Debug

	return xmlDoc;
}

/**
 * @method private CreateDocument
 *	 Method to create an empty DOM object.  Please note that the way to create an XML DOM
 *	 for IE and Netscape is different.
 * @return Object xmldoc
 *	 The XML DOM object.
 **/
function CreateDocument()
{
//DEL 	Profile.Start("<Global>", "CreateDocument");	// Debug

	var xmlDoc = null;
	if (navigator.appName == "Netscape")
	{
		xmlDoc = document.implementation.createDocument("", "", null);
	}
	else
	{
		if (window.ActiveXObject)
		{
			xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
			xmlDoc.async = false;
		}
	}

//DEL 	Profile.End("<Global>", "CreateDocument");	// Debug

	return xmlDoc;
}


/**
 * @class public EObjectError
 * @constructor EObjectError
 * EObjectError is used to thrown an exception, including an error message.
 *	The constructor takes one parameter.
 * @param String d
 *	The description for the exception.
 **/
function EObjectError(d)
{
	this.description = d;

	/**
	 * @method public toString
	 *	Method to return an error description.
	 *@return String d
	 *	The error description.
	 **/
	this.toString = function () { return d; }
}


/**
 * @class public XMILoader
 * @constructor XMILoader
 * XMILoader is used to load an XMI from a string or url, and also is used to
 * generate Diff string.
 *	The constructor takes three parameters.
 * @param EClass eclassroot
 *	The description for the exception.
 * @param String rootMemberName
 *	The name of EReference which represents the real root of a model.
 *	This name will be the starting point when we walk the model graph. Other EReferences
 *	of the fake root will be ignored. The way to stream out model data from server is flat,
 *	so we have many EReferences on the fake root. Without this name, it is difficult to follow
 *	the correct graph.
 * @param String ecoreNS
 *	The name space string for the encore file of a model.
 **/
function XMILoader(eclassroot, rootMemberName, ecoreNS)
{
//DEL 	Profile.Start("XMILoader", "<Init>");	// Debug

	this.NODE_ELEMENT  = 1;
	this.NODE_ATTRIBUTE = 2;
	this.NODE_TEXT = 3;
	this.NODE_CDATA_SECTION = 4;
	this.NODE_ENTITY_REFERENCE = 5;
	this.NODE_ENTITY = 6;
	this.NODE_PROCESSING_INSTRUCTION = 7;
	this.NODE_COMMENT = 8;
	this.NODE_DOCUMENT = 9;
	this.NODE_DOCUMENT_TYPE = 10;
	this.NODE_DOCUMENT_FRAGMENT = 11;
	this.NODE_NOTATION = 12;
	this.XMINS = "http://www.omg.org/XMI";
	this.ODCNS = "http://www.ibm.com/odc";
   this.ODCSTATUS = "ODCstatus";
   this.ODCID = "ODCid";



	//This is the name space string for encore and is expected to be passed in
	//by mediators.
	this.ECORENS = ecoreNS;
	//This is the name of EReference which represents the real root of a model.
	//This name will be the starting point when we walk the model graph. Other EReferences
	//of the fake root will be ignored.
	this.RootMemberName =rootMemberName;

	//JRLI
	this.CURRENT_MODE =0;
	this.ORIGINAL_MODE =1;

	this.Root = null;
	this.EClass = eclassroot;
	this.Objects = new Array();	// private

	if (this.EClass != null)
	{
		this.Root = new EObject(this.EClass);
		this.Root.IsRoot = true;
	}


	/**
	 * @method public LoadXMLString
	 *	 Method to parse an xml from a string by calling a utility function LoadXMLString().
	 *	 Then create a graph of eobjects based on the XML DOM by calling another function
	 *	 LoadXMIDoc().
	 * @param String varString
	 *	 The XML string to be parsed.
	 * @return Object rootObject
	 *	 The root object of the data model graph.
	 **/
	this.LoadXMIString = function(varString)
	{
//DEL 		Profile.Start("XMILoader", "LoadXMIString");	// Debug

		//alert("LoadXMIString begin");
		var xmlDoc = LoadXMLString(varString);
		if (null == xmlDoc)
		{
//DEL 			Profile.End("XMILoader", "LoadXMIString");	// Debug

			return null;
		}

//DEL 		Profile.End("XMILoader", "LoadXMIString");	// Debug

		return this.LoadXMIDoc(xmlDoc);
	}

	/**
	 * @method public LoadXMI
	 *	 Method to parse an xml from a string by calling a utility function LoadXML().
	 *	 Then create a graph of eobjects based on the XML DOM by calling another function
	 *	 LoadXMIDoc().
	 * @param String url
	 *	 The url to the XML file to be parsed.
	 * @return Object rootObject
	 *	 The root object of the data model graph.
	 * @exception EObjectError
	 *	An exception will be thrown if EClass is null.
	 **/
	this.LoadXMI = function(url)
	{
//DEL 		Profile.Start("XMILoader", "LoadXMI");	// Debug

		if (null == this.EClass)
		{
			var Msg = NlsFormatMsg(missing_model, null);
			Log.error("xmiHandler.LoadXMI", Msg);

//DEL 			Profile.End("XMILoader", "LoadXMI");	// Debug

			throw new EObjectError(Msg);
		}

		var xmlDoc = LoadXML(url);
		if (null == xmlDoc)
		{
//DEL 			Profile.End("XMILoader", "LoadXMI");	// Debug

			return null;
		}

		var res = this.LoadXMIDoc(xmlDoc);

//DEL 		Profile.End("XMILoader", "LoadXMI");	// Debug

		return res;
	}

	/**
	 * @method public LoadXMIDoc
	 *	 Method to walk through an XML DOM to create a graph of eobjects by calling another
	 *	 function LoadXMIElement(). At the same time, it will add the first level elements
	 *	 of the XML DOM as the EReferences of the root eobject. Then it will link all ERefernces
	 *	 by looping through all eobjects.  These ERefernces are originally represented by XMIId's
	 *	 in the incoming XMI file.
	 * @param Object xmlDoc
	 *	 The XML DOM which contains all the model data.
	 * @return Object rootObject
	 *	 The root object of the data model graph.
	 * @exception EObjectError
	 *	An exception will be thrown if EClass is null or a mismatch occurs between the model and data.
	 **/
	this.LoadXMIDoc = function(xmlDoc)
	{
//DEL 		Profile.Start("XMILoader", "LoadXMIDoc");	// Debug

		if (null == this.EClass)
		{
			var Msg = NlsFormatMsg(missing_model, null);
			Log.error("xmiHandler.LoadXMIDoc", Msg);

//DEL 			Profile.End("XMILoader", "LoadXMIDoc");	// Debug

			throw new EObjectError(Msg);
		}

		//alert("begin LoadXMIDoc");
		if (null == xmlDoc)
		{
//DEL 			Profile.End("XMILoader", "LoadXMIDoc");	// Debug

			return null;
		}

		this.Objects = new Array();
		var element = xmlDoc.documentElement;

		for (var i = 0; i < element.childNodes.length; ++i)
		{
			var node = element.childNodes[i];
			if (node.nodeType == this.NODE_ELEMENT)
			{
				var name = node.nodeName;
				var index = name.indexOf(":");
				if (index != -1)
				{
					name = name.substring(index+1,name.length);
				}

				if (name != "Documentation")
				{
					var reference = this.Root.EClass.getEStructuralFeature(node.nodeName);
					if (null == reference || reference.CLASSTYPE != EStructuralFeature.CLASSTYPE_EREFERENCE)
					{
						var args = new Array;
						args[0] = node.nodeName;
						args[1] = this.Root.EClass.Name;
						var Msg = NlsFormatMsg(reference_missing_inmodel, args);
						Log.error("xmiHandler.LoadXMIDoc", Msg);

//DEL 						Profile.End("XMILoader", "LoadXMIDoc");	// Debug

						throw new EObjectError(Msg);

					}

					if (reference.getEReferenceType() == null)
					{
						var args = new Array;
						args[0] = reference.Name;
						args[1] = this.Root.EClass.Name;

						var Msg = NlsFormatMsg(reference_not_class, args);
						Log.error("xmiHandler.LoadXMIDoc", Msg);

//DEL 						Profile.End("XMILoader", "LoadXMIDoc");	// Debug

						throw new EObjectError(Msg);
					}

					var obj = new EObject(reference.getEReferenceType());
					this.LoadXMIElement(obj,node);
					var member = this.Root.AddMember(node.nodeName,reference);
					var bIsArray = false;
					if (
						this.Root.eGet(node.nodeName) != null
						|| reference.getUpperBound() == -1
					)
					{
						bIsArray = true;
					}

					if (!bIsArray)
					{
						var diff = reference.getUpperBound() - reference.getLowerBound();
						if (diff > 1 || (diff == 1 && reference.getLowerBound() > 0))
						{
							bIsArray = true;
						}
					}

					if (bIsArray)
					{
						this.Root.eAdd(node.nodeName,obj);
					}
					else
					{
						this.Root.eSet(node.nodeName,obj);
					}
				}
			}
		}

		// link references to objects
		var bIsSorted = false;
		for (var i = 0; i < this.Objects.length; ++i)
		{
			var eobject = this.Objects[i];
			for (var j = 0; j < eobject.Members.length; ++j)
			{
				var value = eobject.Members[j].Value;
				var eReference = eobject.Members[j].EStructuralFeature;
				if (
					"string" != typeof(value)
					|| eReference.CLASSTYPE != EStructuralFeature.CLASSTYPE_EREFERENCE
				)
				{
					continue;
				}

				var name = eobject.Members[j].Name;
				var lowerBound = eReference.getLowerBound();
				var upperBound = eReference.getUpperBound();
				var valueArr = GetArrayOfIDs(value);
				valueArr.sort();
				eobject.Members[j].EStructuralFeature.ValidateCardinality(valueArr.length);
				var objComp = null;
				if (eReference.getEReferenceType() != null)
				{
					if (!eReference.getEReferenceType().IsSorted)
					{
						eReference.getEReferenceType().Objects.sort(this.SortEObject);
						eReference.getEReferenceType().IsSorted = true;
					}
					objComp = eReference.getEReferenceType().Objects;
				}
				else
				{
					if (!bIsSorted)
					{
						this.Objects.sort(this.SortEObject);
						bIsSorted = true;
					}
					objComp = this.Objects;
				}
				var z = 0;
				for (var k = 0; k < valueArr.length; ++k)
				{
					var bFound = false;
					while(z < objComp.length)
					{
						if (objComp[z].ID > valueArr[k])
						{
							break;
						}

						if (objComp[z].ID == valueArr[k])
						{
							valueArr[k] = objComp[z];
							eReference.EClass = objComp[z].EClass;
							eReference.Type = objComp[z].EClass.Name;
							//eReference.setContainment(false);
							if (objComp != objComp[z].EClass.Objects)
							{
								objComp = objComp[z].EClass.Objects;
								z = 0;
							}

							bFound = true;
							break;
						}
						++z;
					}
					if (!bFound)
					{
						var args = new Array;
						args[0] = valueArr[k];
						args[1] = name;
						args[2] = eobject.EClass.Name;

						var Msg = NlsFormatMsg(reference_not_indocument, args);
						Log.error("xmiHandler.LoadXMIDoc", Msg);

//DEL 						Profile.End("XMILoader", "LoadXMIDoc");	// Debug

						throw new EObjectError(Msg);
					}
				}
				if (valueArr.length > 0)
				{
					if (
						-1 == upperBound
						|| (
							lowerBound < upperBound
							&& upperBound > 1
						)
						|| upperBound >1
					)
					{
						eobject.eSet(name,valueArr);
					}
					else
					{
						eobject.eSet(name,valueArr[0]);
					}
				}
				else
				//To avoid the case in which we have a "" (empty string) as the value of an ERefenece
				//such as <Dir xmi:id="Dir27" Name="d:\\projects\" SubDirectories="" Parent=""/>,
				//Here Parent is empty (no parent).
				{
					eobject.eSet(name,null);
				}
			}
		}
		this.Objects = null;

//DEL 		Profile.End("XMILoader", "LoadXMIDoc");	// Debug

		return this.Root;
	}

	/**
	 * @method private SortEObject
	 *	 Method to sort two eobjects based on their XMIId's.
	 * @param Object sortObj1
	 *	 The first eobject to be sorted.
	 * @param Object sortObj2
	 *	 The second eobject to be sorted.
	 * @return int
	 *	 0 - the same, 1 - the first eobject ID is greater than the second, -1 - otherwise.
	 **/
	this.SortEObject = function(sortObj1,sortObj2)
	{
		if (sortObj1 == sortObj2 || sortObj1.ID == sortObj2.ID)
			return 0;

		return (sortObj1.ID > sortObj2.ID) ? 1 : -1;
	}

	/**
	 * @method private LoadXMIElement
	 *	 Method to walk through potion of an XML DOM to create a branch of eobject gata graph by calling itself
	 *	 recusively. At the same time, all EAttributes will be set the the parent eobject.  Child nodes of
	 *	 the passed-in element node will become ERefences of the parent eobejct.
	 * @param Object eobject
	 *	 The eobject,which will act as a parent for eoject(s) represented by the element.
	 * @param Node element
	 *	 The XML DOM node, which represent child eobjects for the passed in eobject.
	 * @return Object eobject
	 *	 The parent eobject passed in as a parameter.
	 * @exception EObjectError
	 *	An exception will be thrown if a mismatch occurs between the model and data.
	 **/
	this.LoadXMIElement = function(eobject,element) // private
	{
//DEL 		Profile.Start("XMILoader", "LoadXMIElement");	// Debug

		for (var i = 0; i < eobject.EClass.getEAllAttributes().length; ++i)
		{
			var	structFeature = eobject.EClass.getEAllAttributes()[i];
			if (
				structFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTE
				&& structFeature.Type == "id"
			)
			{
				continue;
			}

			eobject.AddMember(structFeature.Name,structFeature);
		}

		for (var i = 0; i < eobject.EClass.getEAllReferences().length; ++i)
		{
			var	structFeature = eobject.EClass.getEAllReferences()[i];
			eobject.AddMember(structFeature.Name,structFeature);
		}

		for (var i = 0; i < element.attributes.length; ++i)
		{
			var attribute = element.attributes.item(i);
			var name = attribute.name;
			var	structFeature = eobject.EClass.getEStructuralFeature(name);
			if (null == structFeature)
			{
				var args = new Array;
				args[0] = name;
				args[1] = eobject.EClass.Name;

				var Msg = NlsFormatMsg(class_missing_inmodel, args);
				Log.error("xmiHandler.LoadXMIElement", Msg);

//DEL 				Profile.End("XMILoader", "LoadXMIElement");	// Debug

				throw new EObjectError(Msg);
			}

			if (structFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTECALCULATE)
			{
				var args = new Array;
				args[0] = structFeature.Name;

				var Msg = NlsFormatMsg(attributecalculate_missing_indata, args);
				Log.error("xmiHandler.LoadXMIElement", Msg);

//DEL 				Profile.End("XMILoader", "LoadXMIElement");	// Debug

				throw new EObjectError(Msg);
			}

			if (structFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTE)
			{
				if (structFeature.Type == "id")
				{
					eobject.ID = attribute.value;
					continue;
				}
			}

			eobject.eSet(name,attribute.value);
		}

		if (eobject.ID == null)
		{
			eobject.ID = NewID();
		}

		this.Objects[this.Objects.length] = eobject;

		if (eobject.EClass.Objects == null)
		{
			eobject.EClass.Objects = new Array();
			eobject.EClass.IsSorted = false;
		}

		eobject.EClass.Objects[eobject.EClass.Objects.length] = eobject;

		for (var i = 0; i < element.childNodes.length; ++i)
		{
			var node = element.childNodes[i];

			if (node.nodeType == this.NODE_ELEMENT)
			{
				var aValue = null;
				var member = eobject.EClass.getEStructuralFeature(node.nodeName);

				if (null == member)
				{
					//alert("before exception for loadingXMI");
					var args = new Array;
					args[0] = node.nodeName;
					args[1] = eobject.EClass.Name;
					var Msg = NlsFormatMsg(member_missing_inmodel, args);
					Log.error("xmiHandler.LoadXMIElement", Msg);

//DEL 					Profile.End("XMILoader", "LoadXMIElement");	// Debug

					throw new EObjectError(Msg);
				}

				//Check its cardinality
				var bIsArray = false;
				if (
					eobject.eGet(node.nodeName) != null
					|| member.getUpperBound() == -1
				)
				{
					bIsArray = true;
				}

				if (!bIsArray)
				{
					var diff = member.getUpperBound() - member.getLowerBound();
					if (
						diff > 1
						|| (
							diff == 1
							&& member.getLowerBound() > 0
						)
					)
					{
						bIsArray = true;
					}
				}

				//if it is a multivalue eAttribute
				if(member.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTE)
				{
					//If is not a multivalue attribute, throw an exception
					if (bIsArray == false)
					{
						var args = new Array;
						args[0] = member.Name;
						args[1] = eobject.EClass.Name;

						var Msg = NlsFormatMsg(attribute_not_multivalue_inmodel, args);
						Log.error("xmiHandler.LoadXMIElement", Msg);

//DEL 						Profile.End("XMILoader", "LoadXMIElement");	// Debug

						throw new EObjectError(Msg);
					}

					//The child node has to be a text node
					aValue = node.childNodes[0].nodeValue;
				}
				else
				//if it is an eRefeence
				if(member.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE)
				{
					if (member.getEReferenceType() == null)
					{
						var args = new Array;
						args[0] = member.Name;
						args[1] = eobject.EClass.Name;

						var Msg = NlsFormatMsg(reference_not_class, args);
						Log.error("xmiHandler.LoadXMIElement", Msg);

//DEL 						Profile.End("XMILoader", "LoadXMIElement");	// Debug

						throw new EObjectError(Msg);
					}

					var aValue = new EObject(member.getEReferenceType());
					this.LoadXMIElement(aValue,node);
				}
				//it is an error
				else
				{
					var args = new Array;
					args[0] = node.nodeName;
					args[1] = eobject.EClass.Name;
					var Msg = NlsFormatMsg(member_not_eatt_or_eref_inmodel, args);
					Log.error("xmiHandler.LoadXMIElement", Msg);

//DEL 					Profile.End("XMILoader", "LoadXMIElement");	// Debug

					throw new EObjectError(Msg);
				}

				if (bIsArray)
				{
					eobject.eAdd(node.nodeName,aValue);
				}
				else
				{
					eobject.eSet(node.nodeName,aValue);
				}
			}
		}

//DEL 		Profile.End("XMILoader", "LoadXMIElement");	// Debug

		return eobject;
	}

	/**
	 * @method public SaveXMI
	 *	 Method to walk through a graph of eobjects to create an XML DOM by calling another
	 *	 function SaveXMIElement().
	 * @return Object xmlDoc
	 *	 The XML DOM representing the model data.
	 * @exception EObjectError
	 *	An exception will be thrown if Root eobject is null.
	 **/
	this.SaveXMI = function()
	{
//DEL 		Profile.Start("XMILoader", "SaveXMI");	// Debug

		if (null == this.Root)
		{
//DEL 			Profile.End("XMILoader", "SaveXMI");	// Debug

			return null;
		}

		//We need RootMemberName to be set
		if(this.RootMemberName == null)
		{
			var Msg = NlsFormatMsg(root_member_name_missing_error, null);
			Log.error("xmiHandler.SaveXMI", Msg);

//DEL 			Profile.End("XMILoader", "SaveXMI");	// Debug

			throw new EObjectError(Msg);
		}

		this.Objects = new Array();
		var xmlDoc = CreateDocument();
		xmlDoc.appendChild(xmlDoc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\""));
		var rootElement = xmlDoc.createElement("xmi:XMI");
		rootElement.setAttribute("xmi:version","2.0");
		rootElement.setAttribute("xmlns:xmi",this.XMINS);
		xmlDoc.appendChild(rootElement);
		var docElem = xmlDoc.createElement("xmi:Documentation");
		rootElement.appendChild(docElem);
		var node = xmlDoc.createElement("exporter");
		node.appendChild(xmlDoc.createTextNode("XMI Script Serializer"));
		docElem.appendChild(node);
		node = xmlDoc.createElement("exporterVersion");
		node.appendChild(xmlDoc.createTextNode("1.0"));
		docElem.appendChild(node);

		var member = this.Root.GetMember(this.RootMemberName);
		var value = member.Value;
		if (value != null)
		{
			if (value.length != null && "number" == typeof(value.length))
			{
				for (var j = 0; j < value.length; ++j)
				{
					this.SaveXMIElement(xmlDoc, member.Name,rootElement,value[j]);
				}
			}
			else
			{
				this.SaveXMIElement(xmlDoc, member.Name,rootElement,value);
			}
		}

//DEL 		Profile.End("XMILoader", "SaveXMI");	// Debug

		return xmlDoc;
	}

	/**
	 * @method private SaveXMIElement
	 *	 Method to walk through potion of an XML DOM to create a branch of eobject gata graph by calling itself
	 *	 recusively. A new XML element will be created corresponding to the eobject.
	 *	 The EAttributes of the eobject will become the attributes of the newly created XML element.
	 *	 The EReferences of the eobject will become the child elements of the newly created XML element recursively.
	 * @param Object xmlDoc
	 *	 The DOM tree for child elements to be attached.
	 * @param String nodename
	 *	 The node name which is the name of an EReference.
	 * @param Node parentElement
	 *	 The parentElement which will act as the parent of the node being created.
	 * @param EObject eobject
	 *	 The eobject corresponds to the node and whose data is used to create the node.
	 * @return void
	 *	 The call will return when the eobject has been processed.
	 **/
	this.SaveXMIElement = function(xmlDoc,nodename,parentElement,eobject)
	{
//DEL 		Profile.Start("XMILoader", "SaveXMIElement");	// Debug

		for (var i = 0; i < this.Objects.length; ++i)
		{
			// object already saved
			if (eobject == this.Objects[i])
			{
//DEL 				Profile.End("XMILoader", "SaveXMIElement");	// Debug

				return;
			}
		}

		this.Objects[this.Objects.length] = eobject;
		var elem = xmlDoc.createElement(nodename);
		elem.setAttribute(this.ODCID,eobject.ID);
		parentElement.appendChild(elem);
		for (var i = 0; i < eobject.Members.length; ++i)
		{
			var feature = eobject.Members[i].EStructuralFeature;
			if (feature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTECALCULATE)
			{
				continue;
			}

			var name = eobject.Members[i].Name;
			var value = eobject.eGet(name);
			if (null == value)
			{
				continue;
			}

			if (
				"object" == typeof(value)
				&& feature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE
			)
			{
				//value is an array
				if (
					value.length != null
					&& "number" == typeof(value.length)
				)
				{
					for (var j = 0; j < value.length; ++j)
					{
						if ("object" == typeof(value[j]))
						{
							if (
								feature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE
								&& !feature.isContainment()
							)
							{
								var attr = elem.getAttribute(name);
								if (attr != null && attr.length > 0)
								{
									attr += " " + value[j].ID;
								}
								else
								{
									attr = value[j].ID;
								}

								elem.setAttribute(name,attr);
								this.SaveXMIElement(xmlDoc,feature.getEReferenceType().Name,xmlDoc.documentElement,value[j]);
							}
							else
							{
								this.SaveXMIElement(xmlDoc,name,elem,value[j]);
							}
						}
						else
						{
							var e = xmlDoc.createElement(name);
							e.appendChild(xmlDoc.createTextNode(value[j]));
							elem.appendChild(e);
						}
					}
				}
				//Not an array
				else
				{
					if (
						feature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE
						&& !feature.isContainment()
					)
					{
						elem.setAttribute(name,value.ID);
						this.SaveXMIElement(xmlDoc,feature.getEReferenceType().Name,xmlDoc.documentElement,value);
					}
					else
					{
						this.SaveXMIElement(xmlDoc,name,elem,value);
					}
				}
			}
			//if it an attribute
			else
			{
				//if we have a multivalue attribute
				if (
					(
						feature.getUpperBound() == -1
						|| feature.getUpperBound() > 1
						|| (
							feature.getLowerBound() < feature.getUpperBound()
							&& feature.getUpperBound() > 1
						)
					)
					&& value.length != null
					&& "number" == typeof(value.length)
				)
				{
					//loop thru the value list
					for (var j = 0; j < value.length; ++j)
					{
						//create another child node for this multivalue attribute and set up its value
						var indidualValue = value[j];
						if ("boolean" == typeof(indidualValue))
						{
							indidualValue = (indidualValue) ? "true" : "false";
						}

						var attrElem = xmlDoc.createElement(name);

						attrElem.appendChild(xmlDoc.createTextNode(indidualValue));

						elem.appendChild(attrElem);
					}
				}
				//if it is a single value attribute
				else
				{
					if ("boolean" == typeof(value))
					{
						value = (value) ? "true" : "false";
					}

					elem.setAttribute(name,value);
				}
			}
		}

	}

	/**
	 * @method public GenerateDiffString
	 *	 Method to generate an Diff XMI string for sending information back to server.
	 * @return String xml
	 *	 The string containing the XMI with all changed nodes.
	 **/
	this.GenerateDiffString = function()
	{
		var xml = "";
		var xmlDoc = this.GenerateDiff();
		for (var i = 0; i < xmlDoc.childNodes.length; ++i)
		{
			//The global flag ODCPORTAL is set up by initialization emitter on a html page
         xml += GetXMLString(0,xmlDoc.childNodes[i], ODCPORTAL) + "\n";
		}

      //alert("xml" + xml);

		return xml;
	}

	/**
	 * @method public GenerateDiff
	 *	 Method to  generate an Diff XMI DOM for sending information back to server.
	 *	 This method will first call a private method: MarkElementsForDiff() to mark the states
	 *	 of each eobject on the data model graph based on flags, and then call another private method:
	 *	 SaveDiff() to generate a XML DOM for Diff.
	 * @return Object xmlDoc
	 *	 The ML DOM Object to contain all changed nodes.
	 **/
	this.GenerateDiff = function()
	{

      //alert("GenerateDiff");
		this.MarkElementsForDiff();
		var xmlDoc = this.SaveDiff();

		return xmlDoc;
	}

	/**
	 * @method private MarkElementsForDiff
	 *	 Method to mark the states of each eobject by traversing the data model graph based on flags.
	 *	 In a Diff, if a child eobject is updated, its parent eobjects have to be sent as well since
	 *	 we need to know which parent this updated child belongs to.  Therefore, the state changes of
	 *	 the child eobject has to be propagate upwards to cause a state change for its parents.  So we
	 *	 know what eobjects in the graph have to included inside a Diff.  This method will call a private
	 *	 method: MarkElement() to recursively traverse through the graph.
	 *
	 **/
	this.MarkElementsForDiff = function()
	{
      //alert("MarkElementsForDiff");
		var statusFromChildren = 0;
		var flag = 0;
		if (null == this.Root)
		{

			return null;
		}

		//We need RootMemberName to be set
		if(this.RootMemberName == null)
		{
			var Msg = NlsFormatMsg(root_member_name_missing_error, null);
			Log.error("xmiHandler.MarkElementsForDiff", Msg);

			throw new EObjectError(Msg);
		}

		this.Objects = new Array();

		for (var i = 0; i < this.Root.Members.length; ++i)
		{
			//We will only traverse the EReference whose name is designated by mediators.
			//Other ones are only set up artificially for outputing convenience
			if(this.RootMemberName == this.Root.Members[i].Name)
			{
				var value = this.Root.Members[i].Value;

				//we only need to traverse eReference
				if(this.Root.Members[i].EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE)
				{

               //Check the statusMap to see whether any child has been added or removed etc
               if(this.Root.Members[i].addedAndremovedObjStatus != null && this.Root.Members[i].addedAndremovedObjStatus.length > 0)
               {

                  statusFromChildren = statusFromChildren | this.Root.CHILD_REMOVED_OR_ADDED;
               }

               if (value != null)
               {


                  if (value.length != null && "number" == typeof(value.length))
   					{
   						for (var j = 0; j < value.length; ++j)
   						{
   							flag = this.MarkElement(this.Root.Members[i], value[j]);

   							//By doing bitwise or, we are recording every possible action
   							//which occurred to this object.
   							statusFromChildren = statusFromChildren | flag;
   						}
   					}
   					else
   					{
   						flag = this.MarkElement(this.Root.Members[i], value);
   					}
   					//By doing bitwise or, we are recording every possible action
   					//which occurred to this object.
   				   statusFromChildren = statusFromChildren | flag;
               }
				}
			}
		}

		//Reset this.Root.Status from the previous markups to only keep
		//flag for CHILD_REMOVED_OR_ADDED.  So we can restore the status in case of undo.
		//this.Root.Status = this.Root.Status & this.Root.CHILD_REMOVED_OR_ADDED;
      this.Root.Status = this.SERVER_COPY;

		this.Root.Status = statusFromChildren | this.Root.Status;
	}

	/**
	 * @method private MarkElement
	 *	 Method to walk through a branch of eobject data graph by calling itself
	 *	 recusively. The status of an eobject will be set up based on its own status and
	 *	 its children.  At the same time, the status for its parent eobject is returned.
	 * @param StructuralFeature member
	 *	 The member which the object belongs to.
	 * @param EObject eobject
	 *	 The Object of an EReference.
	 * @return int
	 *	 A bitmask to indicate what actions have been taken against the children
	 *	 of the eobject. The call will return when the eobject has been processed.
	 **/
	this.MarkElement = function(member, eobject)
	{

      /*if(member.Name== "stocks")
      {
		   alert("MarkElement");
      } */
      //This flag is used to report to a parent node what action has taken to its children
		var statusForParent = eobject.SERVER_COPY;
		var statusFromChildren = eobject.SERVER_COPY;
		var tempStatus = eobject.SERVER_COPY;
		var flag = eobject.SERVER_COPY;

      var refStatus = this.GetRefStatus(member, eobject);

      var addingToArray = true;

		for (var i = 0; i < this.Objects.length; ++i)
		{
			//if object has already been marked and has not been added, removed, deleted on the client side.
			if (eobject == this.Objects[i] && refStatus == null)
			{
				return statusForParent;
			}
         else if  (eobject == this.Objects[i] && refStatus != null)
         {

            addingToArray = false;
            break;
         }

		}

      if(addingToArray == true)
      {

		   this.Objects[this.Objects.length] = eobject;
      }

		//If this eobject has a status of NEW, report to its parant that there is a CHILD_REMOVED_OR_ADDED_OR_ADDED
		if(eobject.Status & eobject.NEW)
		{
			statusForParent = statusForParent | eobject.CHILD_REMOVED_OR_ADDED;
			tempStatus = tempStatus | eobject.NEW;
		}

		//If this eobject has a status of UPDATED, report to its parant that there is a CHILD_UPDATED
		if(eobject.Status & eobject.UPDATED)
		{
			statusForParent = statusForParent | eobject.CHILD_UPDATED;
			tempStatus = tempStatus | eobject.UPDATED;
		}

		//If this eobject has a status of CHILD_REMOVED_OR_ADDED, report to its parant that there is a CHILD_REMOVED_OR_ADDED
		/*if(eobject.Status & eobject.CHILD_REMOVED_OR_ADDED)
		{
			statusForParent = statusForParent | eobject.CHILD_REMOVED_OR_ADDED;
			tempStatus = tempStatus | eobject.CHILD_REMOVED_OR_ADDED;
		}*/

		//Reset eoject status and only keep informtion about itself (except CHILD_REMOVED_OR_ADDED)
		//in order to handle undo situations
		eobject.Status = tempStatus;

		for (var i = 0; i < eobject.Members.length; ++i)
		{
			//Attributes do not need to be processed here
			//because any object with updated attributes have been marked as UPDATED.
			//So we only need to look into references.
			if ( (eobject.Members[i].EStructuralFeature.CLASSTYPE & (EStructuralFeature.CLASSTYPE_EATTRIBUTE | EStructuralFeature.CLASSTYPE_EATTRIBUTECALCULATE)) != 0 )
			{
				continue;
			}

          //check the statusMap of each member
         //Because member.addedAndremovedObjStatus has an element for "&name", its length is at least 1 even it is empty.
         if(eobject.Members[i].addedAndremovedObjStatus != null && eobject.Members[i].addedAndremovedObjStatus.length > 0 )
         {
            statusFromChildren = statusFromChildren | eobject.CHILD_REMOVED_OR_ADDED;

         }

			var name = eobject.Members[i].Name;
			var value = eobject.eGet(name);
			if (null == value)
			{
				continue;
			}


			if ("boolean" == typeof(value))
			{
				value = (value) ? "true" : "false";
			}

			//if it is an ERerenece
			if ("object" == typeof(value))
			{
				//if it is an array
				if (
					value.length != null
					&& "number" == typeof(value.length)
				)
				{
					//loop thru all references
					for (var j = 0; j < value.length; ++j)
					{
						//if it is an object
						if ("object" == typeof(value[j]))
						{
							flag = this.MarkElement(eobject.Members[i], value[j]);

							//Aggregate with the status from its children
							statusFromChildren = statusFromChildren | flag;
						}
					}// inner loop
				}//not an array
				else
				{
					flag = this.MarkElement(eobject.Members[i], value);

					//Aggregate with the status from its children
					statusFromChildren = statusFromChildren | flag;
				}
			}
		}//outer loop

		//The status of this eobject is an status aggration from itself and its children.
		eobject.Status = eobject.Status | statusFromChildren;

		//The status for its parent is an aggration from the status of this eobject for its parent
		//and the status from its children.
		statusForParent = statusForParent | statusFromChildren

//DEL 		Profile.End("XMILoader", "MarkElement");	// Debug

		return statusForParent;
	}

	/**
	 * @method Private SaveDiff
	 *	 Method to to generate an Diff XMI for sending information back to server.
	 *	 This method takes two passes to walk through the subset of modified nodes to generate
	 *	 a set of updated current, new and deleted elements, and another set of
	 *	 original elements which are updated. This method will call a private
	 *	 method: SaveDiffElement() to recursively traverse through the graph.
	 * @return xmlDoc - XML DOM Object to contain all changed nodes.
	 * @exception EObjectError
	 *	 An exception will be thrown if ECORENS or Root eobject is null.
	 **/
	this.SaveDiff = function()
	{

      //alert("begin SaveDiff");
		Log.debug("XmiHandlier.saveDiff", "Begin Save Diff");
		if (null == this.Root)
		{
			return null;
		}

		//We need RootMemberName to be set
		if(this.ECORENS == null)
		{
			var Msg = NlsFormatMsg(ecore_namespace_missing_error, null);
			Log.error("xmiHandler.SaveDiff", Msg);

//DEL 			Profile.End("XMILoader", "SaveDiff");	// Debug

			throw new EObjectError(Msg);
		}

		this.Objects = new Array();
		var xmlDoc = CreateDocument();
		xmlDoc.appendChild(xmlDoc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\""));
		var rootElement = xmlDoc.createElement("Diff");
		rootElement.setAttribute("xmi:version","2.0");
		rootElement.setAttribute("xmlns:xmi",this.XMINS);
		rootElement.setAttribute("xmlns",this.ECORENS);
		rootElement.setAttribute("xmlns:odc",this.ODCNS);
		xmlDoc.appendChild(rootElement);
		//1st pass to generate the updated current set of elements
		var currElem = xmlDoc.createElement("Current");
		rootElement.appendChild(currElem);

		//If any update or new objects are made, we generate the subset for updated and new elements.
		if(
			(this.Root.Status & this.Root.CHILD_UPDATED) || (this.Root.Status & this.Root.CHILD_REMOVED_OR_ADDED)
		)
		{
			for (var i = 0; i < this.Root.Members.length; ++i)
			{
				//We will only traverse the EReference whose name is designated by mediators.
				//Other ones are only set up artificially for outputing convenience
				if(this.RootMemberName == this.Root.Members[i].Name)
				{
					var value = this.Root.Members[i].Value;
					var member = this.Root.Members[i];

					if (
						value != null
						&& this.Root.Members[i].EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE
					)
					{
						if (
							value.length != null
							&& "number" == typeof(value.length)
						)
						{
							for (var j = 0; j < value.length; ++j)
							{
								this.SaveDiffElement(xmlDoc,this.Root.Members[i].Name,currElem,value[j], this.Root, member, this.CURRENT_MODE);
							}
						}
						else
						{
							this.SaveDiffElement(xmlDoc,this.Root.Members[i].Name,currElem,value, this.Root, member, this.CURRENT_MODE);
						}
					}
				}
			}
		}


		//Reset the array, so we can start over
		this.Objects = new Array();

		//2nd pass to generate the updated current set of elements
		var origElem = xmlDoc.createElement("Original");
		rootElement.appendChild(origElem);

		//If any update is made or objects are deleted, we generate the original subset for updated and deleted elements.
		if(this.Root.Status & this.Root.CHILD_UPDATED)
		{
			for (var i = 0; i < this.Root.Members.length; ++i)
			{
				//We will only traverse the EReference whose name is designated by mediators.
				//Other ones are only set up artificially for outputing convenience
				if(
					this.RootMemberName == this.Root.Members[i].Name
					&& this.Root.Members[i].EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE
				)
				{
					var name = this.Root.Members[i].Name;
					var member = this.Root.Members[i];
					//We need to get a list of the original objects
					var value = this.Root.GetOrigAttrValOrCurrRefObj(name);
					if (value != null)
					{
						if (value.length != null && "number" == typeof(value.length))
						{
							for (var j = 0; j < value.length; ++j)
							{
								this.SaveDiffElement(xmlDoc,this.Root.Members[i].Name,origElem,value[j], this.Root, member, this.ORIGINAL_MODE);
							}
						}
						else
						{
							this.SaveDiffElement(xmlDoc,this.Root.Members[i].Name,origElem,value, this.Root, member, this.ORIGINAL_MODE);
						}
					}
				}
			}
		}

//DEL 		Profile.End("XMILoader", "SaveDiff");	// Debug

		return xmlDoc;
	}

	/**
	 * @method private SaveDiffElement
	 *	 Method to walk through a branch of eobject data graph by calling itself
	 *	 recusively to generate current elements which are either updated, new or deleted, or
	 *	 original elements (the original copy of an updated elemen).
	 *	 The unchanged elements will be exclued.  The method will return if an object has been
	 *	 processed or no processing is needed for a specific eobject status in a particular mode.
	 * @param Object xmlDoc
	 *	 The XML DOM Object to contain all changed nodes.
	 * @param String nodename
	 *	 The nodename of an element to be created.
	 * @param Node parentElement
	 *	 The DOM element to which a child is appended.
	 * @param EObject eobject
	 *	 The eobject which needs to be processed..
	 * @param EObject parentEObject
	 *	 The parentEObject of this eobject.
	 * @param EStructuralFeature member
	 *	 The model data of the eobject, such as indicates whether this EReference is part of primary
	 *	 key.
	 * @param int mode
	 *	 The mode to indicate whether we are exporting the current subset or original subset
	 *					 related to modified objects(updated, new and deleted).
	 **/
	this.SaveDiffElement = function(xmlDoc,nodename,parentElement, eobject, parentEObject, member, /*current or original*/ mode)
	{
      //alert("begin SaveDiffElement member.Name"+  member.Name);

      //We need to add some logic here to allow the same object output mutiple times


      var refStatus = this.GetRefStatus(member, eobject);


      var addingToArray = true;

		for (var i = 0; i < this.Objects.length; ++i)
		{
			//If an object has already been marked and has not been added, removed, deleted on the client side,
         //We do not need to process it again.  Otherwise, we have to output it even if it has been output it before
         //because we need to know which object is added or removed etc.
			if (eobject == this.Objects[i] && refStatus == null)
			{
				return;
			}
         else if  (eobject == this.Objects[i] && refStatus != null)
         {

            addingToArray = false;
            break;
         }

		}

      if(addingToArray == true)
      {

		   this.Objects[this.Objects.length] = eobject;
      }



      /*
		for (var i = 0; i < this.Objects.length; ++i)
		{
			// object already exported
			if (eobject == this.Objects[i])
			{

				return;
			}
		}

		this.Objects[this.Objects.length] = eobject;
      */

		//If the parent eobject is not a new object, this eobject is still the original copy, and it is not part of
		//primary key of the eobject(member.iD == false), no need to export
		if(
			!(parentEObject.Status & parentEObject.NEW)
			&& eobject.Status == eobject.SERVER_COPY
			&& member.EStructuralFeature.iD == false
         && refStatus == null
		)
		{

			return;
		}
		/*else
      //if we are generating the original subset
		//we do not need to export an eobject whose status is either NEW or NEW_CHILD, or
		//both NEW and NEW_CHILD.
		if(
			mode == this.ORIGINAL_MODE
			&& !(
             (eobject.Status & eobject.UPDATED)
            ||(eobject.Status & eobject.CHILD_UPDATED)
			)
		)
		{

			return;
		} */

		var elem = xmlDoc.createElement(nodename);
		//Set up an attribute to indicate the status of an object
		//C-NEW
		//U-UPDATED
		//D-DELETED
      //A-ADDED
      //R-REMOVED
		//no flag for original object


		//if(eobject.Status & eobject.NEW
      if(refStatus == eobject.REF_NEW)
		{
			elem.setAttribute(this.ODCSTATUS,"C");
		}
      else
		//if(eobject.Status & eobject.DELETED)
      if(refStatus == eobject.REF_DELETED)
		{
			elem.setAttribute(this.ODCSTATUS,"D");
		}
      //check statusMap
      else if
      (refStatus == eobject.REF_REMOVED )
      {
         elem.setAttribute(this.ODCSTATUS,"R");

      }
      //check statusMap
      else if(refStatus == eobject.REF_ADDED)
      {
         elem.setAttribute(this.ODCSTATUS,"A");
      }
		else if
      (
			(eobject.Status & eobject.UPDATED)
			&& mode == this.CURRENT_MODE
		)
		{
			elem.setAttribute(this.ODCSTATUS,"U");
		}


      //We need to make an updated object to have a unique xmiId because there are two copies
      //of the same object in both original and current section of a diff xml.
      //If we do not distinguish them between the two sections, EMF will resolve a xmiId to  a wronf copy.
      //(defect RATLC00971254)
      if ((eobject.Status & eobject.UPDATED) && mode == this.ORIGINAL_MODE)
      {
         elem.setAttribute("xmi:id", "_"+ eobject.ID);
      }
      else
      {
         elem.setAttribute("xmi:id", eobject.ID);
      }



		//TGupta NOTE: add odc:id attribute which is same as xmi:id
		//server side code need this to traverse the Diff
      elem.setAttribute(this.ODCID, eobject.ID);
		parentElement.appendChild(elem);
		for (var i = 0; i < eobject.Members.length; ++i)
		{
			var feature = eobject.Members[i].EStructuralFeature;
			Log.debug("xmiHandler:saveDiff","testing status of "+feature.Name+" status is "+(eobject.Status & 8)+" Read only is "+feature.isReadOnly());
			Log.debug("xmiHandler:saveDiff","Value is "+eobject.Members[i].Value);
			if (feature.iD == false && feature.isReadOnly() == 1 && feature.CLASSTYPE != EStructuralFeature.CLASSTYPE_EREFERENCE)
			{
				Log.debug("xmiHandler:saveDiff","Exiting out of loop");
				continue;
			}
			else
			//If this is an EAttribute and in CURRENT_MODE
			if (
				feature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTE
				&& mode == this.CURRENT_MODE
			)
			{
				//If the EAttribute is not part of primary key of the eobject and it is not a new or
				//or an updated eobject,we do not output it.
				if(
					feature.iD == false
					&& !(
						(eobject.Status & eobject.NEW)
						|| (eobject.Status & eobject.UPDATED)
						|| (eobject.Status & eobject.DELETED)
					)
				)
				{
					continue;
				}
			}
			else
			//If this is an EAttribute and in ORIGINAL_MODE
			if (
				feature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTE
				&&  mode == this.ORIGINAL_MODE
			)
			{
				//If the EAttribute is not part of primary key of the eobject,we do not output it.
				if(
					feature.iD == false
					&& !(eobject.Status & eobject.UPDATED)
				)
				{
					continue;
				}
			}

			var member = eobject.Members[i];
			var name = eobject.Members[i].Name;
			var value = null;

			//if we are generating the current subset, get its current data.
			if(mode == this.CURRENT_MODE)
			{
				value = eobject.GetCurAttrValOrCurAndDelRefObj(name);
			}
			//if we are generating the original subset, get its original data.
			else
			{
				value = eobject.GetOrigAttrValOrCurrRefObj(name);
			}

			if (null == value)
			{
				continue;
			}

			//If it is a eRef
			if (
				"object" == typeof(value)
				&& feature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE
			)
			{
				if (
					value.length != null
					&& "number" == typeof(value.length)
				)
				{
				
					for (var j = 0; j < value.length; ++j)
					{
						if ("object" == typeof(value[j]))
						{
							if (
								feature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE
								&& !feature.isContainment()
							)
							{
								refObj = value[j];



								//If the parent eobj is not a new obj, this ERef is not changed and is not part primary key,
								//we do not output xmi_ID and no further traveral
								if(
									!(eobject.Status & eobject.NEW)
									&& refObj.Status == refObj.SERVER_COPY
                           && this.GetRefStatus(member, refObj) == null
									&& feature.iD == false
								)
								{
									
									//do nothing
								}
								//otherwise, we need to output xmi_ID
								else
								{
									var processedObj = this.FindEObject(refObj);
									//If the object has been output, we need to output a ref
									//because the actual object will not be output here again.
									//otherwise, we do not need to output ref because we will
									//output the whole object below
									if(processedObj != null)
									{
										var attr = elem.getAttribute(name);


                              //We need to make an updated object to have a unique xmiId because there are two copies
                              //of the same object in both original and current section of a diff xml.
                              //If we do not distinguish them between the two sections, EMF will resolve a xmiId to  a wronf copy.
                              //(defect RATLC00971254)
                              var  tempId = null;
                              if ((processedObj.Status & processedObj.UPDATED) && mode == this.ORIGINAL_MODE)
                              {
                                 tempId= "_"+ processedObj.ID;
                              }
                              else
                              {
                                 tempId =  processedObj.ID;
                              }


										if (attr != null && attr.length > 0)
										{
											attr += " " + tempId;
										}
										else
										{
											attr = tempId;
										}
                              var encodedValue = escapeForXML(attr);
										//elem.setAttribute(name,attr);
					
                              		elem.setAttribute(name,encodedValue);
						
									}
									

									//this.SaveDiffElement(xmlDoc,eobject.Members[i].EStructuralFeature.getEReferenceType().Name,xmlDoc.documentElement,value[j], mode);
									//Even if it is not a containment, we want to export this eobject under the eobject this eobject
									//gets referenced.
									//this.SaveDiffElement(xmlDoc,eobject.Members[i].EStructuralFeature.getEReferenceType().Name,elem,value[j], eobject, member, mode);
									
									this.SaveDiffElement(xmlDoc,name,elem,value[j], eobject, member, mode);
								}
							}
							else //containment
							{
								
								this.SaveDiffElement(xmlDoc,name,elem,value[j], eobject, member, mode);
							}
						}
						else
						{
							
							var e = xmlDoc.createElement(name);
                     var encodedValue = escapeForXML(value[j]);
							//e.appendChild(xmlDoc.createTextNode(value[j]));
                     e.appendChild(xmlDoc.createTextNode(encodedValue));
							elem.appendChild(e);
						}
					}
				}
				else
				{
					if (
						feature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE
						&& !feature.isContainment()
					)
					{
						 //If the parent eobj is not a new obj, this ERef object is not changed and is not part primary key,
						 //we do not output xmi_ID and no further traveral
						 if(
						 	!(eobject.Status & eobject.NEW)
						 	&& value.Status == value.SERVER_COPY
                     && this.GetRefStatus(member, value) == null
						 	&& feature.iD == false
						 )
						 {
							 //do nothing
						 }
						 //otherwise, we need to output xmi_ID
						 else
						 {
							 var processedObj = this.FindEObject(value);
							 //If the object has been output, we need to output a ref
							 //because the actual object will not be output here again.
							 //otherwise, we do not need to output ref because we will
							 //output the whole object below
							 if(processedObj != null)
							 {

                         //We need to make an updated object to have a unique xmiId because there are two copies
                         //of the same object in both original and current section of a diff xml.
                         //If we do not distinguish them between the two sections, EMF will resolve a xmiId to  a wronf copy.
                         //(defect RATLC00971254)
                         var  tempId = null;
                         if ((processedObj.Status & processedObj.UPDATED) && mode == this.ORIGINAL_MODE)
                         {
                            tempId= "_"+ value.ID;
                         }
                         else
                         {
                            tempId =  value.ID;
                         }

                         var encodedValue = escapeForXML(tempId);

                         elem.setAttribute(name,encodedValue);

							 }
							 //ID gets printed for non-containment ERef
							 //this.SaveDiffElement(xmlDoc,feature.getEReferenceType().Name,xmlDoc.documentElement,value, mode);
							 //Even if it is not a containment, we want to export this eobject under the eobject this eobject
							 //gets referenced.
							 //this.SaveDiffElement(xmlDoc,eobject.Members[i].EStructuralFeature.getEReferenceType().Name, elem,value, eobject, member, mode);
							 this.SaveDiffElement(xmlDoc,name, elem,value, eobject, member, mode);
						 }
					}
					else //containment
					{
						this.SaveDiffElement(xmlDoc,name,elem,value, eobject, member, mode);
					}
				}
			}
			//if it is an eAttr
			else
			{
				//if we have a multivalue attribute
				if (
					(
						feature.getUpperBound() == -1
						|| feature.getUpperBound() > 1
						|| (
							feature.getLowerBound() < feature.getUpperBound()
							&& feature.getUpperBound() > 1
						)
					)
					&& value.length != null
					&& "number" == typeof(value.length)
				)
				{
					//loop thru the value list
					for (var j = 0; j < value.length; ++j)
					{
						//create another child node for this multivalue attribute and set up its value
						var indidualValue = value[j];
						if ("boolean" == typeof(indidualValue))
						{
							indidualValue = (indidualValue) ? "true" : "false";
						}
                  //convert it into a long date format which can be imported by WDO API
                  else if(feature.Type == "date")
                  {

                     indidualValue = this.FormatDateString(indidualValue);
                  }


						var attrElem = xmlDoc.createElement(name);
                  var encodedValue = escapeForXML(indidualValue);
						//attrElem.appendChild(xmlDoc.createTextNode(indidualValue));
                  attrElem.appendChild(xmlDoc.createTextNode(encodedValue));


						elem.appendChild(attrElem);
					}

				}
				//if it is a single value attribute
				else
				{
					if ("boolean" == typeof(value))
					{
						value = (value) ? "true" : "false";
					}
               //convert it into a long date format which can be imported by WDO API
               else if(feature.Type == "date")
               {
                  value = this.FormatDateString(value);

               }
               var encodedValue = escapeForXML(value);
					elem.setAttribute(name,encodedValue);
				}
			}
		}

//DEL 		Profile.End("XMILoader", "SaveDiffElement");	// Debug
	}

   this.GetRefStatus = function( member, eobject)
   {

      var refStatus =  null;
      if(member.addedAndremovedObjStatus != null)
		{
		   refStatus = IndexMapGet(member.addedAndremovedObjStatus, eobject.ID);

		}
      return refStatus;
   }
	/**
	 * @method Private FindEObject
	 *	 Method to check whether an object has been processes.
	 * @param EObject anEObject
	 *	 The EObject needs to be checked.
	 * @return targetObject - the EObject we find
	 **/
	this.FindEObject = function( anEObject )
	{
//DEL 		Profile.Start("XMILoader", "FindEObject");	// Debug

		var targetObject = null;
		for (var i = 0; i < this.Objects.length; ++i)
		{
			// object already exported
			if (anEObject == this.Objects[i])
			{
				targetObject = this.Objects[i];

//DEL 				Profile.End("XMILoader", "FindEObject");	// Debug

				return targetObject;
			}
		}

//DEL 		Profile.End("XMILoader", "FindEObject");	// Debug

		return null;
	}

   /**
	 * @method Private FormatDateString
	 *	 Method to format a date object to a string following a pattern using hxclient JS lib.
	 * @param Number value
	 *	 The value for a date field in milliseconds since 1970.
	 * @return String - the formatted date string
	 **/
	this.FormatDateString = function( value )
	{

      //var ODC_SDO_SUPPORT = "SDO";
      var formattedStr = null;
      var temp = value;
      var aDate = new Date(value);
      //If it is SDO version, it will use a patern yyyy-MM-dd'T'HH:mm:ss'.'SSSz
      if(ODC_SDO_SUPPORT == 'undefined' ||  ODC_SDO_SUPPORT == "SDO")
      {

         var converter = new hX_3.DateTimeConverter("strict:1", "format:yyyy-MM-dd'T'HH:mm:ss'.'SSSz");
         formattedStr = converter.valueToString(aDate);

      }
      //If it is WDO, the format from JavaScript Date.toString is supported
      else
      {
         formattedStr = aDate.toString();
      }
      //alert("formattedStr: "+ formattedStr + " orig value:" + temp );
		return formattedStr;
	}


//DEL 	Profile.End("XMILoader", "<Init>");	// Debug
}

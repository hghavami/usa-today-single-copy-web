//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003.  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//==================================================================

/////////////////////////////////////////////////////////////////////////////////////////////////
// EFactory
/**
 * @class public EFactory
 * @constructor EFactory
 * EFactory is used to create a new eobject,an instance of the eclass passed in through
 * its constructor.
 * The constructor takes no parameter.
**/
function EFactory()
{
}

/**
 * @method public EFactory.prototype.create
 *	Method to create a new eobject, an instance of eclass which is passed in as a patameter.
 *	Please note that this method will add all EAttributes and EReferences to this eobject, and set
 *	up a structure for this eobject.  But real values for its members are still not set until
 *	eSet() and eAdd() methods are called later on.
 * @param EClass eclass
 *	The eclass object from which the eobject will be instanciated.
 * @return EObject eobject
 *	The eobject instance that has been created.
 * @exception EObjectError
 *	An exception will be thrown if eclass is null.
 **/
EFactory.prototype.create = function(eclass)
{
//DEL 	Profile.Start("EFactory", "create");	// Debug

	if (null == eclass)
	{
		var Msg = NlsFormatMsg(unable_create_object, null);
		Log.error("EFactory.create", Msg);

//DEL 		Profile.End("EFactory", "create");	// Debug

		throw new EObjectError(Msg);
	}

	var eobject = new EObject(eclass);
	eobject.ID = NewID();

	var eall = eobject.EClass.getEAllAttributes();
	for (var i = eall.length - 1; i >= 0; --i)
	{
		var attr = eall[i];
		if (attr.Type != "id")
		{
			eobject.AddMember(attr.Name, attr);
		}
	}

	var eall = eobject.EClass.getEAllReferences();
	for (var i = eall.length - 1; i >= 0; --i)
	{
		var ref = eall[i];
		eobject.AddMember(ref.Name,ref);
	}

//DEL 	Profile.End("EFactory", "create");	// Debug

	return eobject;
}

// EFactory End
/////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////
// EPackage

/**
 * @class public EPackage
 * @constructor EPackage
 * 	EPackage represents a package.
 *	The constructor takes no parameter.
**/
function EPackage()
{
	this.EFactory = new EFactory(); // private
}

/**
 * @method public EPackage.prototype.getEFactoryInstance
 *	Method to return eFactory object.
 * @return EFactory eFactory object
 *	The EFactory object.
 **/
EPackage.prototype.getEFactoryInstance = function() // public
{
	return this.EFactory;
}

/**
 * @method public EPackage.prototype.setEFactoryInstance
 *	Method to set eFactory object.
 * @param EFactory eFactory object
 *	The EFactory object to be set.
 **/
EPackage.prototype.setEFactoryInstance = function(efactory) // public
{
	this.EFactory = efactory;
}

// EPackage End
/////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////
// Utility Functions

/**
 * @method public IndexMapCreate
 *   Method to return new Index Map.
 * @return IndexMap Index Map
 *   The new Index Map object.
 **/
function IndexMapCreate()
{
	var ary = new Array();
	ary["#names"] = new Object();
	return ary;
}

/**
 * @method public IndexMapGet
 *   Method to return object.
 * @param name object Name
 *   The object related-name
 * @return Object the object
 *   The related-name object.
 **/
function IndexMapGet(ary, name)
{
	var obj = ary["#names"][name];
	if ( obj == undefined )
	{
		return null;
	}

	return obj;
}

/**
 * @method public IndexMapAddNew
 *   Method to add new entry.
 *   This method no check the exist entry to have the same name
 * @param name object Name
 *   The object related-name
 * @return Object the object
 *   The related-name object.
 **/
function IndexMapAddNew(ary, name, obj)
{
	ary["#names"][name] = obj;
	ary[ary.length] = obj;

	return obj;
}

/**
 * @method public IndexMapAdd
 *   Method to add new entry.
 *   This method check the exist entry to have the same name.
 *   If the exist entry have the same name, return the exist entry.
 * @param name object Name
 *   The object related-name
 * @return Object the object
 *   The related-name object.
 **/
function IndexMapAdd(ary, name, obj)
{
	var existsObj = IndexMapGet(ary, name);
	if ( existsObj != null )
	{
		return existsObj;
	}

	return IndexMapAddNew(ary, name, obj);
}

/**
 * @method public IndexMapRemove
 *   Method to remove the exist entry.
 * @param nameOrIndex object Name or Index
 *   The removed object's index or name.
 * @return Object the object
 *   The removed object.
 **/
function IndexMapRemove(ary, nameOrIndex)
{
	var obj = null;
	var index = -1;
	var name = null;
	// nameOrIndex is index.
	if ( typeof(nameOrIndex) == "number" )
	{
		obj = ary[nameOrIndex];
		index = nameOrIndex;
		var names = ary["#names"];
		for ( name in names )
		{
			if ( names[name] == obj )
			{
				break;
			}
		}
	}
	else
	// nameOrIndex is name.
	{
		name = nameOrIndex;
		obj = IndexMapGet(ary, name);
		if ( obj == null )
		{
			return null;
		}

		for ( var i = ary.length - 1; i >= 0; --i )
		{
			if ( ary[i] == obj )
			{
				index = i;
				break;
			}
		}
	}

	if ( obj == null )
	{
		return null;
	}

	delete ary["#names"][name];

	var endIndex = ary.length - 1;
	for ( var i = index; i < endIndex; ++i )
	{
		ary[i] = ary[i+1];
	}
	ary.length = endIndex;

	return obj;
}

// Utility Functions End
/////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////
// EClass End

/**
 * @class public EClass
 * @constructor EClass
 * EClass is the equivalent JavaScript implementation of an EMF EClass,and represents a class which holds
 * all EAttributes and EReferences and other fields.
 *	The constructor takes two parameters, name and diffgramNeeded.
 * @param String name
 *	The class name
 * @param String diffgramNeeded
 *	The flag indicates whether any object of this class will be included as part of diffgram
 *	when any changes are made to its objects on client side.
 **/
function EClass(name, diffgramNeeded)
{
//DEL 	Profile.Start("EClass", "<Init>");	// Debug

	this.Name = name;

	//the default value is true
	//If true, we need to send back changed objects in diffgram.
	//If false, we can ignore changes because they are updated by non-users (such as WebService etc)
	this.DiffgramNeeded = true;
	if(diffgramNeeded != undefined)
	{
		this.DiffgramNeeded = diffgramNeeded;
	}

	this.Attributes = IndexMapCreate();	// private
	/**
	 * @method public Attributes.get
	 *	Method to get an EAttribute to this class. If the EAttribute not have,
	 *	this method will return null.
	 * @param name EAttribute Name
	 *	The attribute name.
	 * @return EAttribute attribute
	 *	The attribute exists.
	 **/
	this.Attributes.get = function(name)
	{
//DEL 		Profile.Start("EClass", "Attributes.get");	// Debug

		var res = IndexMapGet(this, name);

//DEL 		Profile.End("EClass", "Attributes.get");	// Debug

		return res;
	}
	/**
	 * @method public Attributes.add
	 *	Method to add an EAttribute to this class. If the EAttribute has been added already,
	 *	this method will just return the EAttribute.
	 * @param EAttribute attribute
	 *	The attribute which needs to be added.
	 * @return EAttribute attribute
	 *	The attribute which either exists or is newly added.
	 **/
	this.Attributes.add = function(attribute)
	{
//DEL 		Profile.Start("EClass", "Attributes.add");	// Debug

		var res = IndexMapAdd(this, attribute.Name, attribute);

//DEL 		Profile.End("EClass", "Attributes.add");	// Debug

		return res;
	}
	/**
	 * @method public Attributes.remove
	 *	Method to remove an EAttribute from this class.
	 * @param String nameOrIndex
	 *	The name or index of an EAttribute which needs to be removed.
	 * @return EAttribute attribute
	 *	The attribute which needs to be removed.
	 **/
	this.Attributes.remove = function(nameOrIndex)
	{
//DEL 		Profile.Start("EClass", "Attributes.remove");	// Debug

		var res = IndexMapRemove(this, nameOrIndex);

//DEL 		Profile.End("EClass", "Attributes.remove");	// Debug

		return res;
	}

	this.References = IndexMapCreate();	// private
	/**
	 * @method public Reference.get
	 *	Method to get an EReference to this class. If the EReference not have,
	 *	this method will return null.
	 * @param name EReference Name
	 *	The attribute name.
	 * @return EReference reference
	 *	The reference exists.
	 **/
	this.References.get = function(name)
	{
//DEL 		Profile.Start("EClass", "References.get");	// Debug

		var res = IndexMapGet(this, name);

//DEL 		Profile.End("EClass", "References.get");	// Debug

		return res;
	}
	/**
	 * @method public References.add
	 *	Method to add an EReference to this class. If the EReference has been added already,
	 *	this method will just return the EReference.
	 * @param EReference reference
	 *	The reference which needs to be added.
	 * @return EReference reference
	 *	The reference which either exists or is newly added.
	 **/
	this.References.add = function(reference)
	{
//DEL 		Profile.Start("EClass", "References.add");	// Debug

		var res = IndexMapAdd(this, reference.Name, reference);

//DEL 		Profile.End("EClass", "References.add");	// Debug

		return res;
	}
	/**
	 * @method public References.remove
	 *	Method to remove an EReference from this class.
	 * @param String nameOrIndex
	 *	The name or index of an EReference which needs to be removed.
	 * @return EReference reference
	 *	The reference which needs to be removed.
	 **/
	this.References.remove = function(nameOrIndex)
	{
//DEL 		Profile.Start("EClass", "References.remove");	// Debug

		var res = IndexMapRemove(this, nameOrIndex);

//DEL 		Profile.End("EClass", "References.remove");	// Debug

		return res;
	}

	this.EPackage = new EPackage();	// private

//DEL 	Profile.End("EClass", "<Init>");	// Debug
}


/**
 * @method public EClass.prototype.getEPackage
 *	Method to return EPackage object.
 * @return EPackage ePackage object
 *	The EPackage object.
 **/
EClass.prototype.getEPackage = function()
{
	return this.EPackage;
}

/**
 * @method public EClass.prototype.getEStructuralFeature
 *	Method to look up an EAttribute or EReference give a name.
 * @param String name
 *	The name of an EAttribute or EReference.
 * @return EAttribute or EReference
 *	The EAttribute or EReference which has been looked up or null if it does not exist.
 **/
EClass.prototype.getEStructuralFeature = function(name)
{
//DEL 	Profile.Start("EClass", "getEStructuralFeature");	// Debug

	var res = this.getEAllAttributes().get(name);
	if ( res != null )
	{
//DEL 		Profile.End("EClass", "getEStructuralFeature");	// Debug

		return res;
	}

	res = this.getEAllReferences().get(name);

//DEL 	Profile.End("EClass", "getEStructuralFeature");	// Debug

	return res;
}

/**
 * @method public EClass.prototype.getEAllAttributes
 *	Method to return an array of all EAttributes this class has.
 * @return Array  Attributes
 *	The array of all EAttributes this class has.
 **/
EClass.prototype.getEAllAttributes = function()
{
	return this.Attributes;
}

/**
 * @method public EClass.prototype.getEAllReferences
 *	Method to return an array of all EReferences this class has.
 * @return Array  References
 *	The array of all EReferences this class has.
 **/
EClass.prototype.getEAllReferences = function()
{
	return this.References;
}

/**
 * @method private EClass.prototype.ValidateType
 *	Method to check whether a value for either an EAttribute or EReference identified by the name
 *	is valid for this eclass.
 * @param String name
 *	The name of either an EAttribute or EReference.
 * @param EAttribute or EReference value
 *	The value of either an EAttribute or EReference.
 * @return EReference or EAttribute value or boolean
 *	The value of an EReference or EAttribute, or true or false if value is of primitive type.
 * @exception EObjectError
 *	An exception will be thrown if no such EReference or EAttribute exists.
 **/
EClass.prototype.ValidateType = function(name, value)
{
//DEL 	Profile.Start("EClass", "ValidateType");	// Debug

	var structFeat = this.getEStructuralFeature(name);
	if (structFeat == null)
	{
		var args = new Array;
		args[0] = name;
		args[1] = this.Name;

		var Msg = NlsFormatMsg(name_notexist_model, args);
		Log.error("EClass.ValidateType", Msg);

//DEL 		Profile.End("EClass", "ValidateType");	// Debug

		throw new EObjectError(Msg);
	}
	if ( structFeat.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTE )
	{
		var res = structFeat.ValidateType(value);

//DEL 		Profile.End("EClass", "ValidateType");	// Debug

		return res;
	}

//DEL 	Profile.End("EClass", "ValidateType");	// Debug

	return value;
}

/**
 * @method private EClass.prototype.ValidateCardinality
 *	Method to check whether either an EAttribute or EReference identified by the name
 *	has a valid cardinality for this eclass.
 * @param String name
 *	The name of either an EAttribute or EReference.
 * @param int length
 *	The number ofvalues for an EAttribute or EReference.
 * @return boolean
 *	The value indicates whether the cardinality is valid for this eclass.
 * @exception EObjectError
 *	An exception will be thrown if no such EReference or EAttribute exists.
 **/
EClass.prototype.ValidateCardinality = function(name,length)
{
//DEL 	Profile.Start("EClass", "ValidateCardinality");	// Debug

	var structFeat = this.getEStructuralFeature(name);
	if (null == structFeat)
	{
		var args = new Array;
		args[0] = name;
		args[1] = this.Name;

		var Msg = NlsFormatMsg(name_notexist_model, args);
		Log.error("EClass.ValidateCardinality", Msg);

//DEL 		Profile.End("EClass", "ValidateCardinality");	// Debug

		throw new EObjectError(Msg);
	}

	structFeat.ValidateCardinality(length);

//DEL 	Profile.End("EClass", "ValidateCardinality");	// Debug
}


// EClass End
/////////////////////////////////////////////////////////////////////////////////////////////////

var EStructuralFeature = new Object();
EStructuralFeature.CLASSTYPE_EATTRIBUTE				= 0x1;
EStructuralFeature.CLASSTYPE_EATTRIBUTECALCULATE	= 0x2;
EStructuralFeature.CLASSTYPE_EREFERENCE				= 0x4;


EStructuralFeature.AddProperty = function(obj, name)
{
	obj.Setter = new Function("value", 'this.eSet("' + name + '", value);');
	obj.Getter = new Function('return this.eGet("' + name + '");');
}

EStructuralFeature.toString = function(classType)
{
//DEL 	Profile.Start("EStructuralFeature", "toString");	// Debug

	var str = undefined;
	switch(classType)
	{
	case EStructuralFeature.CLASSTYPE_EATTRIBUTE: // 0x1
		str = "EAttribute";
		break;

	case EStructuralFeature.CLASSTYPE_EATTRIBUTECALCULATE: // 0x2
		str = "EAttributeCalculate";
		break;

	case EStructuralFeature.CLASSTYPE_EREFERENCE: // 0x4
		str = "EReference";
		break;
	}

//DEL 	Profile.End("EStructuralFeature", "toString");	// Debug

	return str;
}



/////////////////////////////////////////////////////////////////////////////////////////////////
// EAttributeCalculate

/**
 * @class public EAttributeCalculate
 * @constructor EAttributeCalculate
 * EAttributeCalculate represents an attribute whose value is obtained through calculation using
 * other EAttributes.
 *	The constructor takes three parameters.
 *@param String name
 *	The name of the EAttributeCalculate.
 *@param String expression
 *	The expression is usec to arrive the value of this EAttributeCalculate.
 *@param String type
 *	The type of this EAttributeCalculate, such int, boolean, string etc..
**/
function EAttributeCalculate(name, expression, type)
{
//DEL 	Profile.Start("EAttributeCalculate", "<Init>");	// Debug

	this.Name = name;
	this.Expression = expression; // private
	this.Type = type;

	EStructuralFeature.AddProperty(this, name);

//DEL 	Profile.End("EAttributeCalculate", "<Init>");	// Debug
}

EAttributeCalculate.prototype.CLASSTYPE = EStructuralFeature.CLASSTYPE_EATTRIBUTECALCULATE;

/**
 * @method public EAttributeCalculate.prototype.isReadOnly
 *	Method to return ReadOnly.
 * @return boolean true
 *	The readonly flag for this EAttributeCaculate is always returned 1 as it is always readonly.
 **/
EAttributeCalculate.prototype.isReadOnly = function()
{
	return 1;
}

/**
 * @method public EAttributeCalculate.prototype.getExpression
 *	Method to return the expression for this EAttributeCalculate.
 * @return String  Expression
 *	The expression for this EAttributeCalculate.
 **/
EAttributeCalculate.prototype.getExpression = function()
{
	return this.Expression;
}

/**
 * @method public EAttributeCalculate.prototype.getType
 *	Method to return the Type for this EAttributeCalculate.
 * @return String  Type
 *	The Type for this EAttributeCalculate.
 **/
EAttributeCalculate.prototype.getType = function() // public
{
	return this.Type;
}

// EAttributeCalculate End
/////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////
// EAttribute

/**
 * @class public EAttribute
 * @constructor EAttribute
 * EAttribute is the equivalent JavaScript implementation of EMF Java EAttribute class, which
 * is used to hold all the information of an attribute of a class.
 *	The constructor takes two parameters.
 * @param String name
 *	The name of an EAttribute.
 * @param String type
 *	The name of a primitive type for this EAttribute, such as int, boolean, and string etc.
 * @param Boolean readonly
 *  A flag that indicates whether this is a readonly attribute.
**/
function EAttribute(name, type, readonly)
{
//DEL 	Profile.Start("EAttribute", "<Init>");	// Debug
	Log.debug("EClass.Eattribute","In EAttribute constructor");
	this.Name = name;
	this.Type = type;
	this.readonly = readonly;
	
	EStructuralFeature.AddProperty(this, name);

//DEL 	Profile.End("EAttribute", "<Init>");	// Debug
}


EAttribute.prototype.LowerBound = 0;	// private
EAttribute.prototype.UpperBound = 1;	// private if unbounded, the value is -1
//iD is a boolean flag to indicate whether this EAttribute is part of Primary Key
//for the eobject to which this EAttribute belongs.
EAttribute.prototype.iD = false;



EAttribute.prototype.CLASSTYPE = EStructuralFeature.CLASSTYPE_EATTRIBUTE;


/**
 * @method public EAttribute.prototype.isReadOnly
 *	Method to return ReadOnly.
 * @return boolean readonly
 *	The readonly flag for this EAttribute.
 **/
EAttribute.prototype.isReadOnly = function()
{
	return this.readonly;
}

/**
 * @method public EAttribute.prototype.getLowerBound
 *	Method to return LowerBound.
 * @return int  LowerBound
 *	The LowerBound for this EAttribute.
 **/
EAttribute.prototype.getLowerBound = function()
{
	return this.LowerBound;
}

/**
 * @method public EAttribute.prototype.setLowerBound
 *	Method to set LowerBound.
 * @param int  value
 *	The LowerBound for this EAttribute.
 **/
EAttribute.prototype.setLowerBound = function(value)
{
	this.LowerBound = value;
}

/**
 * @method public EAttribute.prototype.getUpperBound
 *	Method to return UpperBound.
 * @return int  UpperBound
 *	The UpperBound for this EAttribute.
 **/
EAttribute.prototype.getUpperBound = function()
{
	return this.UpperBound;
}


/**
 * @method public EAttribute.prototype.setUpperBound
 *	Method to set UpperBound.
 * @param int  value
 *	The UpperBound for this EAttribute.
 **/
EAttribute.prototype.setUpperBound = function(value)
{
	this.UpperBound = value;
}

/**
 * @method public EAttribute.prototype.isID
 *	Method to return the isID flag.
 * @return boolean  isID
 *	The flag to indicate whether this EAttribute is part of a primary key for the eclass.
 **/
EAttribute.prototype.isID = function()
{
	return this.iD;
}

/**
 * @method public EAttribute.prototype.setID
 *	Method to set iD flag.
 * @param int  value
 *	The iD flag for this EAttribute.
 **/
EAttribute.prototype.setID = function(isID)
{
	this.iD = isID;
}

/**
 * @method private EAttribute.prototype.ValidateType
 *	Method to check whether the value has a valid type for this EAttribute.
 * @param AnyType value
 *	The value to be checked which could be an array or a primitive value.
 * @return primitivetype value
 *	The value itself or its converted form if the value is valid.
 * @exception EObjectError
 *	An exception will be thrown if the value does not have a right type for this EAttribute.
 **/
EAttribute.prototype.ValidateType = function(value)
{
//DEL 	Profile.Start("EAttribute", "ValidateType");	// Debug
   if (value == null)
	     return null;

   var type = this.Type;
	var ifvalid = true;
	if (null == type)
	{
//DEL 		Profile.End("EAttribute", "ValidateType");	// Debug

		return value;
	}

	//if value is an array which conatins multiple values
	if(
		(
			this.getUpperBound() == -1
			|| this.getUpperBound() > 1
			|| (
				this.getLowerBound() < this.getUpperBound()
				&& this.getUpperBound() > 1
			)
		)
		&& "object" == typeof(value)
		&& value.length != null && value.length >= 0
	)
	{
		//lop thru the array for checking
		for(var i=0; i < value.length; ++i)
		{
			aValue = value[i];
			this.ValidateIndivualValueType(type, aValue);
		}

//DEL 		Profile.End("EAttribute", "ValidateType");	// Debug

		//return the whole list after checking thru the whole array.
		return value;
	}

	//Just a simple primitive value
	var res = this.ValidateIndivualValueType(type, value);

//DEL 	Profile.End("EAttribute", "ValidateType");	// Debug

	return res;
}

/**
 * @method private EAttribute.prototype.ValidateIndivualValueType
 *	Method to check whether a simple value has a valid type for this EAttribute.
 * @param type
 *	The value type.
 * @param AnyType value
 *	The value to be checked.
 * @return value which could be an array or  a primitive value.
 *	The value itself or its converted form if the value is valid.
 * @exception EObjectError
 *	An exception will be thrown if the value does not have a right type for this EAttribute.
 **/
EAttribute.prototype.ValidateIndivualValueType = function(type, value)
{
//DEL 	Profile.Start("EAttribute", "ValidateIndivualValueType");	// Debug

	if (value == null)
	     return null;

	var ifvalid = true;


   if ("string" == type)
	{
		if (value == null)
		{
			value = "";
		}else if(typeof(value)=="number"||value instanceof Date)
		{
			return value.toString();
		}else if(typeof(value) =="string")
		{
			return value;
		}else{
			ifvalid = false;
		}



	}

	if ("boolean" == type)
	{
		if ("boolean" != typeof(value))
		{
			if (
				"true" == value.toLowerCase()
				|| "1" == value
			)
			{
				value = true;
			}
			else
			if (
				"false" == value.toLowerCase()
				|| "0" == value
			)
			{
				value = false;
			}
			else
			{
				ifvalid = false;
			}
		}
		else
		{
//DEL 			Profile.End("EAttribute", "ValidateIndivualValueType");	// Debug

			return value;
		}
	}

	if (
		"byte" == type
		|| "integer" == type
		|| "int" == type
		|| "long" == type
		|| "short" == type
		|| "decimal" == type
		|| "float" == type
		|| "double" == type
	)
	{
		var v = value * 1;
		if (isNaN(v))
		{
			ifvalid = false;
		}
		else
		{
//DEL 			Profile.End("EAttribute", "ValidateIndivualValueType");	// Debug

			return v;
		}
	}

	if (
		"unsignedByte" == type
		|| "positiveInteger" == type
		|| "nonNegativeInteger" == type
		|| "unsignedInt" == type
		|| "unsignedLong" == type
		|| "unsignedShort" == type
	)
	{
		var v = value * 1;
		if (
			isNaN(v)
			|| v<0
		)
		{
			ifvalid = false;
		}
		else
		{
//DEL 			Profile.End("EAttribute", "ValidateIndivualValueType");	// Debug

			return v;
		}
	}

	if (
		"nonPositiveInteger" == type
		|| "negativeInteger" == type
	)
	{
		var v = value * 1;
		if (
			isNaN(v)
			|| v>0
		)
		{
			ifvalid = false;
		}
		else
		{
//DEL 			Profile.End("EAttribute", "ValidateIndivualValueType");	// Debug

			return v;
		}
	}

	if(!ifvalid)
	{
		var args = new Array;
		args[0] = value;
		args[1] = type;

		var Msg = NlsFormatMsg(invalid_value_4type, args);
		Log.error("EAttribute.ValidateType", Msg);

		throw new EObjectError(Msg);
	}

//DEL 	Profile.End("EAttribute", "ValidateIndivualValueType");	// Debug

	return value;
}

/**
 * @method private EAttribute.prototype.ValidateCardinality
 *	Method to check whether a cardinality of length is valid for this EAttribute.
 * @param int length
 *	The number for its cardinality.
 * @exception EObjectError
 *	An exception will be thrown if the cardinality is not right for this EAttribute.
 **/
EAttribute.prototype.ValidateCardinality = function(length)
{
//DEL 	Profile.Start("EAttribute", "ValidateCardinality");	// Debug

	if (
		this.getUpperBound() != -1
		&& length > this.getUpperBound()
	)
	{
		var args = new Array;
		args[0] = this.getUpperBound();
		args[1] = name;
		var Msg = NlsFormatMsg(maximum_size_allowed_attribute, args);
		Log.error("EAttribute.Validatecardinality", Msg);

//DEL 		Profile.End("EAttribute", "ValidateCardinality");	// Debug

		throw new EObjectError(Msg);
	}

	if (
		this.getLowerBound() > 0
		&& length < this.getLowerBound()
	)
	{
		var args = new Array;
		args[0] = this.getLowerBound();
		args[1] = name;
		var Msg = NlsFormatMsg(minimum_size_allowed_attribute, args);
		Log.error("EAttribute.Validatecardinality",Msg);

//DEL 		Profile.End("EAttribute", "ValidateCardinality");	// Debug

		throw new EObjectError(Msg);
	}

//DEL 	Profile.End("EAttribute", "ValidateCardinality");	// Debug
}

// EAttribute End
/////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////
// EReference

/**
 * @class public EReference
 * @constructor EReference
 * EReference is the equivalent JavaScript implementation of EMF Java EReference class, which
 * is used to hold all the information of an reference of a class.
 *	The constructor takes two parameters.
 * @param String name
 *	The name of an EReference.
 * @param EClass eclass
 *	The eclass this EReference belongs to.
 * @param Boolean readonly
 *  A flag that indicates whether this is a readonly reference.
**/
function EReference(name,eclass,readonly)
{
//DEL 	Profile.Start("EReference", "<Init>");	// Debug

	this.Name = name;
	this.EClass = eclass;		// private
	this.readonly = readonly;

	EStructuralFeature.AddProperty(this, name);

//DEL 	Profile.End("EReference", "<Init>");	// Debug
}

EReference.prototype.CLASSTYPE = EStructuralFeature.CLASSTYPE_EREFERENCE;
EReference.prototype.IsContainment = true;	// private
EReference.prototype.LowerBound = 0;		// private
EReference.prototype.UpperBound = 1;		// private if unbounded, the value is -1
//iD is a boolean flag to indicate whether this EAttribute is part of Primary Key
//for the eobject to which this EAttribute belongs.
EReference.prototype.iD = false;

/**
 * @method public EReference.prototype.isReadOnly
 *	Method to return ReadOnly.
 * @return boolean readonly
 *	The readonly flag for this EReference.
 **/
EReference.prototype.isReadOnly = function()
{
	return this.readonly;
}


/**
 * @method public EReference.prototype.isContainment
 *	Method to return the isContainment flag.
 * @return boolean  isContainment
 *	The flag to indicate whether this EReference is contained by the eclass.
 **/
EReference.prototype.isContainment = function()
{
	return this.IsContainment;
}

/**
 * @method public EReference.prototype.setContainment
 *	Method to set setContainment flag.
 * @param int  value
 *	The setContainment flag for this EReference.
 **/
EReference.prototype.setContainment = function(value)
{
	this.IsContainment = value;
}

/**
 * @method public EReference.prototype.getEReferenceType
 *	Method to return EClass.
 * @return EClass  eClass
 *	The EClass for this EReference belongs to.
 **/
EReference.prototype.getEReferenceType = function(value)
{
	return this.EClass;
}

/**
 * @method public EReference.prototype.getLowerBound
 *	Method to return LowerBound.
 * @return int  LowerBound
 *	The LowerBound for this EReference.
 **/
EReference.prototype.getLowerBound = function()
{
	return this.LowerBound;
}

/**
 * @method public EReference.prototype.setLowerBound
 *	Method to set LowerBound.
 * @param int  value
 *	The LowerBound for this EReference.
 **/
EReference.prototype.setLowerBound = function(value)
{
	this.LowerBound = value;
}

/**
 * @method public EReference.prototype.getUpperBound
 *	Method to return UpperBound.
 * @return int  UpperBound
 *	The UpperBound for this EReference.
 **/
EReference.prototype.getUpperBound = function()
{
	return this.UpperBound;
}

/**
 * @method public EReference.prototype.setUpperBound
 *	Method to set UpperBound.
 * @param int  value
 *	The UpperBound for this EReference.
 **/
EReference.prototype.setUpperBound = function(value)
{
	this.UpperBound = value;
}

/**
 * @method public EReference.prototype.isID
 *	Method to return iD flag to indicate whether this EReference is part of the primary key
 *	for this eclass.
 * @return boolean  iD
 *	The flag for this EReference.
 **/
EReference.prototype.isID = function()
{
	return this.iD;
}

/**
 * @method public EReference.prototype.setID
 *	Method to set iD.
 * @param boolean  isID
 *	The flag for this EReference.
 **/
EReference.prototype.setID = function(isID)
{
	this.iD = isID;
}

/**
 * @method private EReference.prototype.ValidateCardinality
 *	Method to check whether a cardinality of length is valid for this EReference.
 * @param int length
 *	The number for its cardinality.
 * @exception EObjectError
 *	An exception will be thrown if the cardinality is not right for this EReference.
 **/
EReference.prototype.ValidateCardinality = function(length)
{
//DEL 	Profile.Start("EReference", "ValidateCardinality");	// Debug

	if (
		this.getUpperBound() != -1
		&& length > this.getUpperBound()
	)
	{
		var args = new Array;
		args[0] = this.getUpperBound();
		args[1] = name;

		var Msg = NlsFormatMsg(maximum_size_allowed_reference, args);
		Log.error("EReference.ValidateCardinality", Msg);

//DEL 		Profile.End("EReference", "ValidateCardinality");	// Debug

		throw new EObjectError(Msg);
	}

	if (
		this.getLowerBound() > 0
		&& length < this.getLowerBound()
	)
	{
		var args = new Array;
		args[0] = this.getLowerBound();
		args[1] = name;
		var Msg = NlsFormatMsg(minimum_size_allowed_reference, args);
		Log.error("EReference.ValidateCardinality", Msg);

//DEL 		Profile.End("EReference", "ValidateCardinality");	// Debug

		throw new EObjectError(Msg);
	}

//DEL 	Profile.End("EReference", "ValidateCardinality");	// Debug
}

// EReference End
/////////////////////////////////////////////////////////////////////////////////////////////////

/**
* @method public PrintEClass
*	 Method to print out the whole graph recursively for tracing purpose.
* @param EClass eclass
*	 The root eclass from which printing starts.
**/
function PrintEClass(eclass)
{
	ResetPrintFlags(eclass);
	return PrintEClassInternal(eclass,0);
}

/**
* @method private ResetPrintFlags
*	 Method to reset all flags recursively for printing.
* @param EClass eclass
*	 The eclass from which ResetPrintFlags() starts.
**/
function ResetPrintFlags(eclass)
{
	eclass.bAlreadyPrinted = false;
	for (var i=0; i < eclass.getEAllReferences().length; ++i) {
		var ref = eclass.getEAllReferences()[i];
		var eclassChild = ref.getEReferenceType();
		if (eclassChild != null && eclassChild.bAlreadyPrinted)
			ResetPrintFlags(eclassChild);
	}
}

/**
* @method private PrintEClassInternal
*	 Method to print out the whole graph recursively for tracing purpose.
* @param EClass eclass
*	 The eclass at which printing is carried out.
* @param int level
*	 The level of this eclass is at, which is used for indentation.
**/
function PrintEClassInternal(eclass,level)
{
	if (eclass.bAlreadyPrinted)
		return "";

	eclass.bAlreadyPrinted = true;
	var ident = "";
	for (var i=0; i < level; ++i)
		ident += "  ";

	var str = ident + "EClass: " + eclass.Name + "\n";
	ident += "	 ";
	for (var i=0; i < eclass.getEAllAttributes().length; ++i) {
		var attr = eclass.getEAllAttributes()[i];
		str += ident;
		if (attr.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE)
			str += "Attribute: " + attr.Name + " Type: " + attr.Type + " LowerBound: " + attr.getLowerBound()
						+ " UpperBound: " + attr.getUpperBound() + "\n";
		else
			str += "AttributeCalculate: " + attr.Name + " Expression: " + attr.getExpression() + "\n";
	}
	for (var i=0; i < eclass.getEAllReferences().length; ++i) {
		var ref = eclass.getEAllReferences()[i];
		str += ident + "Reference: " + ref.Name;
		if (ref.getEReferenceType() != null)
			str += " Type: " + ref.getEReferenceType().Name;

		str += " LowerBound: " + ref.getLowerBound()
					+ " UpperBound: " + ref.getUpperBound() + "\n";

		if (ref.getEReferenceType() != null)
			str += PrintEClassInternal(ref.getEReferenceType(),level+4);
	}
	return str;
}
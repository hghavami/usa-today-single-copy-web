//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003.  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//=================================================================


/////////////////////////////////////////////////////////////////////////////////////////////////
// Utility Functions

/**
 * @method public IndexMultiMapCreate
 *   Method to return new Index Map.
 * @return IndexMultiMap Index Map
 *   The new Index Map object.
 **/
function IndexMultiMapCreate()
{
	var ary = new Array();
	ary["#names"] = new Object();
	return ary;
}

/**
 * @method public IndexMultiMapGet
 *   Method to return object.
 * @param name object Name
 *   The object related-name
 * @return ObjectArray the object array
 *   The related-name object array.
 **/
function IndexMultiMapGet(ary, name)
{
	var objArray = ary["#names"][name];
	if ( objArray == undefined )
	{
		return null;
	}

	return objArray;
}

/**
 * @method public IndexMultiMapAddNew
 *   Method to add new entry.
 *   This method no check the exist entry to have the same name
 * @param name object Name
 *   The object related-name
 * @return ObjectArray the object
 *   The related-name object array.
 **/
function IndexMultiMapAddNew(ary, name, obj)
{
	ary[ary.length] = obj;
	var objArray = new Array();
	objArray[0] = obj;
	ary["#names"][name] = objArray;
	return objArray;
}

/**
 * @method public IndexMultiMapAdd
 *   Method to add new entry.
 *   This method check the exist entry to have the same name.
 *   If the exist entry have the same name, return the exist entry.
 * @param name object Name
 *   The object related-name
 * @return ObjectArray the object
 *   The related-name object array.
 **/
function IndexMultiMapAdd(ary, name, obj)
{
	var objArray = IndexMultiMapGet(ary, name);
	if ( objArray != null )
	{
		ary[ary.length] = obj;
		objArray[objArray.length] = obj;
      return objArray;
	}
  // ary[ary.length] = obj;
	return IndexMultiMapAddNew(ary, name, obj);
}

/**
 * @method public IndexMultiMapRemove
 *   Method to remove the exist entry.
 * @param indexOrObj object or object Name
 *   The removed object or The removed object's name.
 * @return Object the object
 *   The removed object.
 **/
function IndexMultiMapRemove(ary, indexOrObj)
{
	var obj = null;
	var index = -1;
	var name = null;
	// indexOrObj is index.
	if ( typeof(indexOrObj) == "number" )
	{
		obj = ary[indexOrObj];
		index = indexOrObj;
	}
	else
	// indexOrObj is object.
	{
		obj = indexOrObj;
		for ( var i = ary.length - 1; i >= 0; --i )
		{
			if ( this[i] == obj )
			{
				index = i;
				break;
			}
		}
	}

	var names = ary["#names"];
	for ( name in names )
	{
		var objArray = names[name];
		for ( var i = objArray.length - 1; i >= 0; --i )
		{
			if ( objArray[i] != obj )
			{
				continue;
			}

			// If the removed object is the last entry, remove the name entry.
			if ( objArray.length == 1 )
			{
				delete names[name];
				break;
			}

			// Shift the back objects.
			var endIndex = objArray.length - 1;
			for ( var j = i; j < endIndex; ++j )
			{
				objArray[j] = objArray[j+1];
			}
			objArray.length = endIndex;
		}
	}

	if ( obj == null )
	{
		return null;
	}

	if(index>-1){

		var endIndex = ary.length - 1;
		for ( var i = index; i < endIndex; ++i )
		{
			ary[i] = ary[i+1];
		}
		ary.length = endIndex;
	}
	return obj;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// EObject


/**
 * @class public EObject
 * @constructor EObject
 * 	Constructor for class EObject. It is a a javascript implementation of core WDO class EObject.
 *	which is the root of all modeled objects so all the method names start with "e" to distinguish
 *	the EMF methods from the client methods and it provides support for the behaviors and features
 *	common to all modeled objects:
 * @param EClass eclass
 *	the eclass model of the object
 * @return EObject
 *	the object instance of the eclass
 **/
function EObject(eclass)
{
//DEL 	Profile.Start("EObject", "<Init>");	// Debug

	this.EClass = eclass;
	this.ID = NewID();		// private

	this.Members = IndexMapCreate();	// private
	/**
	 * @method private Members.Get
	 * 	get a member to this eobject member's array
	 * @param String	name
	 *	the name of the member
	 * @return Member
	 *	the member object.
	 *  If not find the member, return null.
	 **/
	this.Members.Get = function(name)
	{
//DEL 		Profile.Start("EObject", "Members.Get");	// Debug

		var res = IndexMapGet(this, name);

//DEL 		Profile.End("EObject", "Members.Get");	// Debug

		return res;
	}

	//this.Containers = new Array(); // private
   this.Containers = IndexMapCreate(); // private


	//this.RegExpString = "";	// private
	//this.RegExpObject = new RegExp();	// private

	this.PropertyBinders = IndexMultiMapCreate(); // public
	this.PropertyBinders.Add = function(propertyBinder)	// public
	{
//DEL 		Profile.Start("EObject", "PropertyBinders.Add");	// Debug

		IndexMultiMapAdd(this, propertyBinder.PropertyName, propertyBinder);

//DEL 		Profile.End("EObject", "PropertyBinders.Add");	// Debug

		return propertyBinder;
	}
	this.PropertyBinders.Get = function(name)	// public
	{
//DEL 		Profile.Start("EObject", "PropertyBinders.Get");	// Debug

		var objArray = IndexMultiMapGet(this, name);

//DEL 		Profile.End("EObject", "PropertyBinders.Get");	// Debug

		return objArray;
	}
	this.PropertyBinders.Remove = function(propertyBinder)
	{
//DEL 		Profile.Strart("EObject", "PropertyBinders.Remove");	// Debug

		var res = IndexMultiMapRemove(this, propertyBinder);

//DEL 		Profile.End("EObject", "PropertyBinders.Remove");	// Debug

		return res;
	}

	this.PropertyArrayBinders = IndexMultiMapCreate(); // public
	this.PropertyArrayBinders.Add = function(propertyArrayBinder)	// public
	{
//DEL 		Profile.Start("EObject", "PropertyArrayBinders.Add");	// Debug

		IndexMultiMapAdd(this, propertyArrayBinder.PropertyName, propertyArrayBinder);

//DEL 		Profile.End("EObject", "PropertyArrayBinders.Add");	// Debug

		return propertyArrayBinder;
	}
	this.PropertyArrayBinders.Get = function(name)	// public
	{
//DEL 		Profile.Start("EObject", "PropertyArrayBinders.Get");	// Debug

		var objArray = IndexMultiMapGet(this, name);

//DEL 		Profile.End("EObject", "PropertyArrayBinders.Get");	// Debug

		return objArray;
	}
	this.PropertyArrayBinders.Remove = function(propertyArrayBinder)
	{
//DEL 		Profile.Strart("EObject", "PropertyArrayBinders.Remove");	// Debug

		var res = IndexMultiMapRemove(this, propertyArrayBinder);

//DEL 		Profile.End("EObject", "PropertyArrayBinders.Remove");	// Debug

		return res;
	}

//DEL 	Profile.End("EObject", "<Init>");	// Debug
}

//Values below are used as the status flags for this eobject.
//These values will be used as bitmask to flag each object to
//indicate what action has to be taken.
//Bitwise or (|) and and (&) will be used calculate the status
//of each node.

//Attributes of this eobject is not modified since it is brought down from server.
EObject.prototype.SERVER_COPY =0; //binary format: 00000000

//Flags to indicate action taken against this eobject.
//The attributes of this eobject is updated on client side.
EObject.prototype.UPDATED =1;	//binary format: 00000001
//this eobject is created on client side.
EObject.prototype.NEW =2;  //binary format: 00000010
//This eobject is deleted on client side.
EObject.prototype.DELETED = 4; //binary format: 00000100



//Flags to indicate the status of its chlidren.
//This eobject has at least a child updated on client side.
//Updated only means changes on attributes
EObject.prototype.CHILD_UPDATED =8;	//binary format: 00001000
//This eobject has at least a new child on client side.
//This eobject has at least a child deleted on client side.
EObject.prototype.CHILD_REMOVED_OR_ADDED = 16; //binary format: 000010000

//The follwoing flags are used in statusMap inside a eRef member
//the object is newly created and is added to an eRef for the first time,
EObject.prototype.REF_NEW = 32; //binary format: 000100000
//An existing eobject is removed to an eRef, and the object itself is deleted
EObject.prototype.REF_DELETED = 64; //binary format: 01000000
//An existing eobject is added to an eRef, but the object itself is not changed
EObject.prototype.REF_ADDED = 128; //binary format: 10000000
//An existing eobject is removed from an eRef, but the object itself is not changed
EObject.prototype.REF_REMOVED = 256; //binary format: 100000000



// private
EObject.prototype.IsRoot = false;	// private

//This is the status for all objects which are initially brought down from server.  This will
//help us to separate function calls (eAdd(), eSet(), and eRemove()) between initial downloading and runtime,
//so we can set up diffgram properly.  For those function calls during initial downloading, we do not need
//do anything for diffgram.
//After loading is down, the status of all objects will be updated as this.SERVER_COPY
//this.Status = this.SERVER_COPY;
EObject.prototype.Status = null;

//This flag is used to indicate whether a change has been processed for diffgram. For exmaple, in same cases,
//eSet() is called after eRemove() or eAdd(), by setting this flag true, we only perform logic for diffgram in eAdd() or
//eRemove().
EObject.prototype.Processed= false;

//Keep track of the number of attributes which get updated, so we achieve a better
//efficiency when an attr is updated many times, but eventually revert to the same value.
//In this case, we can set the state back without causing any work on server.
EObject.prototype.ChangedPropertyCount = 0;

//The signature of this eobject, which is a cancatenation of all id fields.
EObject.prototype.signature = "";

EObject.prototype.ACTION_ADD = 0;
EObject.prototype.ACTION_REMOVE = 1;

/**
 * @method private EObject.prototype.GetMember
 * 	get a member to this eobject member's array
 * @param String name
 *	the name of the member
 * @return Member
 *	the member object.
 *  If not find the member, return null.
 **/
EObject.prototype.GetMember = function(name)
{
//DEL 	Profile.Start("EObject", "GetMember");	// Debug

	var res = IndexMapGet(this.Members, name);

//DEL 	Profile.End("EObject", "GetMember");	// Debug

	return res;
}

/**
 * @method private EObject.prototype.AddMember
 * 	add a new member to this eobject member's array
 * @param String	name
 *	the name of the member
 * @param eStructuralFeature
 *	the type of the member
 * @return Member
 *	the member object which is created
 **/
EObject.prototype.AddMember = function(name, eStructuralFeature) // private
{
	////////////////////////////////////////////////////////////////////////
	// Private Functions
	/**
	 * @method private Member
	 * 	constructor of member.
	 * @param	String	name
	 * 	the name of the member
	 * @param	eStructuralFeature
	 * 	the type of the member
	 * @param	String
	 * 	value the value of the member
	 **/
	function Member(name, eStructuralFeature, value)
	{
//DEL 		Profile.Start("EObject.Member", "<Init>");	// Debug

		this.Name = name;
		this.EStructuralFeature = eStructuralFeature;
		this.Value = value;
		//Track any changes on a data menber for this object
		this.OriginalValue = null;

      //A hashmap to keep track of the status of added or removed EReference objects
      //In this hashmap, we will use the ref object as key and its status as value
      this.addedAndremovedObjStatus = null;

//DEL 		Profile.End("EObject.Member", "<Init>");	// Debug
	}
	// Private Functions End
	////////////////////////////////////////////////////////////////////////

//DEL 	Profile.Start("EObject", "AddMember");	// Debug

	var member = this.GetMember(name);
	if ( member == null )
	{
		member = new Member(name, eStructuralFeature, null);
		IndexMapAddNew(this.Members, name, member);
		// ReCreate Regular Expression
		/*
      if ( this.RegExpString.length != 0 )
		{
			this.RegExpString += "|";
		}
		this.RegExpString += name;
		this.RegExpObject.compile("(\\b)(" + this.RegExpString + ")(\\b)", "g");
      */

		// Add Set/Get function
		this.AddProperty(name, eStructuralFeature);
	}

//DEL 	Profile.End("EObject", "AddMember");	// Debug

	return member;
}

/**
 * @method private EObject.prototype.FindMember
 * 	This function is used to get the member from the eobject based on the member's name
 * @return Member member
 *	the member of that eattribute in the eobject
 **/
EObject.prototype.FindMember = function(name) // private
{
//DEL 	Profile.Start("EObject", "FindMember");	// Debug

	var member = this.GetMember(name);
	if (member != null)
	{
//DEL 		Profile.End("EObject", "FindMember");	// Debug

		return member;
	}

	var sf = this.EClass.getEStructuralFeature(name);
	if (sf == null)
	{
//DEL 		Profile.End("EObject", "FindMember");	// Debug

		return undefined;
	}

	if (
		sf.CLASSTYPE != EStructuralFeature.CLASSTYPE_EATTRIBUTE
		|| sf.Type != "id"
	)
	{
		var res = this.AddMember(sf.Name,sf);

//DEL 		Profile.End("EObject", "FindMember");	// Debug

		return res;
	}

//DEL 	Profile.End("EObject", "FindMember");	// Debug

	return undefined;
}


/**
 * @method private EObject.prototype.AddContainer
 *	add an eobject to this eobject's Containers array.
 * @param EObject eobject
 *	the eobject instance
 **/
EObject.prototype.AddContainer = function(eobject, memberOfEobject) // private
{

   //alert("AddContainer");
   var existsObj = IndexMapGet(this.Containers, eobject.ID);

   //only if not inside the map yet, put it in.
   if( existsObj ==  null )
   {

      //We put containing eobject and the member of the containing eobject, which contains this eobject, into an array.
      //then it is put into a map using the containing eobject's xmiId as index.
      var data = new Array();
      data[0] = eobject;
      data[1] = memberOfEobject;
      IndexMapAdd(this.Containers, eobject.ID, data);
   }

}

/**
 * @method private EObject.prototype.RemoveContainer
 *	remove an eobject from this eobject's Containers array.
 * @param EObject eobject
 *	the eobject instance
 **/
EObject.prototype.RemoveContainer = function(eobject) // private
{
   var data = IndexMapGet(this.Containers, eobject.ID);
   if(data != null)
   {
      //bFound = true;
      IndexMapRemove(this.Containers, eobject.ID);

   }


	/*  do we need it?
   if (
		this.Containers.length == 1
		&& this.Containers[0].IsRoot
	)
	{
		 this.Containers[0].eRemove(this.EClass.Name, this);
		 this.Containers = new Array();
	}*/
   //if we found it
   /*
	if (data != null)
	{
		for (var i = 0; i < this.Members.length; ++i)
		{
			//this logic is wrong here
         if (
				this.Members[i].EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE
				&& this.Members[i].EStructuralFeature.getEReferenceType() == eobject.EClass
			)
			{
				this.Members[i].Value = null;
				break;
			}
		}
	} */

//DEL 	Profile.End("EObject", "RemoveContainer");	// Debug
}

/**
 * @method private EObject.prototype.RemoveContainer
 *	remove an eobject to this eobject's Containers array.
 * @param String propertyName
 *	the name of the elcass
 * @return EObject
 *	the eobject instance of the propertyName
 * @exception EObjectError
 *	exception thrown when couldn't find the eclass of the propertyName
 **/
EObject.prototype.CreateEObject = function(propertyName)
{
//DEL 	Profile.Start("EObject", "CreateEObject");	// Debug

	var eclass = null;

	var member = this.GetMember(propertyName);
	if (
		member != null
		&& member.EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE
	)
	{
		eclass = member.EStructuralFeature.getEReferenceType();
	}

	if (null == eclass)
	{
		var args = new Array();
		args[0] = propertyName;

		var Msg = NlsFormatMsg(unable_create_object, args);
		Log.error("eobject.CreateEObject", Msg);

//DEL 		Profile.End("EObject", "CreateEObject");	// Debug

		throw new EObjectError(Msg);
	}

	var eobject = eclass.getEPackage().getEFactoryInstance().create(eclass);

	//JRLI
	//Set Status flag as NEW
	eobject.Status = this.NEW;


//DEL 	Profile.End("EObject", "CreateEObject");	// Debug

	return eobject;
}

/**
 * @method public EObject.prototype.Clone
 *	 copy an eobject to the new created eobject instance
 * @return EObject
 *	 the eobject instance of the new created eobject
 **/
EObject.prototype.Clone = function() // public
{
//DEL 	Profile.Start("EObject", "Clone");	// Debug

	var eobject = new EObject(this.EClass);

	eobject.ID = NewID();
	for (var i = 0; i < this.Members.length; ++i)
	{
		var member = eobject.AddMember(this.Members[i].Name, this.Members[i].EStructuralFeature);
		if (this.Members[i].EStructuralFeature.CLASSTYPE != EStructuralFeature.CLASSTYPE_EATTRIBUTECALCULATE)
		{
			eobject.eSet(this.Members[i].Name,this.eGet(this.Members[i].Name));
		}
	}

	//JRLI
	//Set Status flag as NEW
	eobject.Status = this.NEW;

//DEL 	Profile.End("EObject", "Clone");	// Debug

	return eobject;
}

/**
 * @method public EObject.prototype.CloneForDeletion
 *	This function is used to create a backup copy after an eoject
 * 	is deleted. In this case we need to keep its original ID and as well as OriginalValue.
 * 	OriginalValue is set when update is made to this object.
 * @return EObject
 *	the eobject instance of the new created eobject
 **/
EObject.prototype.CloneForDeletion = function() // public
{
//DEL 	Profile.Start("EObject", "CloneForDeletion");	// Debug

	var eobject = new EObject(this.EClass);

	eobject.ID = this.ID;
	for (var i = 0; i < this.Members.length; ++i)
	{
		var member = eobject.AddMember(this.Members[i].Name, this.Members[i].EStructuralFeature);
		if (this.Members[i].EStructuralFeature.CLASSTYPE != EStructuralFeature.CLASSTYPE_EATTRIBUTECALCULATE)
		{
			eobject.eSet(this.Members[i].Name,this.eGet(this.Members[i].Name));
			member.OriginalValue = this.Members[i].OriginalValue;
		}
	}

//DEL 	Profile.End("EObject", "CloneForDeletion");	// Debug

	return eobject;
}

EObject.prototype.GetChildrenEObjects = function(deep) // public
{
//DEL 	Profile.Start("EObject", "GetChildrenEObjects");	// Debug

	var arr = new Array();
	this.GetChildrenEObjectsInternal(arr,deep);

//DEL 	Profile.End("EObject", "GetChildrenEObjects");	// Debug

	return arr;
}

EObject.prototype.GetChildrenEObjectsInternal = function(arr,deep) // private
{
	function EObjectAddArray(arr, value) // private
	{
//DEL 		Profile.Start("EObject", "EObjectAddArray");	// Debug

		for (var i = 0; i < arr.length; ++i) {
			if (arr[i] == value)
			{
//DEL 				Profile.End("EObject", "EObjectAddArray");	// Debug

				return false;
			}
		}
		arr[arr.length] = value;

//DEL 		Profile.End("EObject", "EObjectAddArray");	// Debug

		return true;
	}

//DEL 	Profile.Start("EObject", "GetChildrenEObjectsInternal");	// Debug

	for (var i = 0; i < this.Members.length; ++i)
	{
		var ef = this.Members[i].EStructuralFeature;
		if (ef.CLASSTYPE != EStructuralFeature.CLASSTYPE_EREFERENCE)
		{
			continue;
		}

		var value = this.eGet(this.Members[i].Name);
		if (value == null || value == " ") // deal with the case that an eReference only has value of a 'space'
		{
			continue;
		}

		if (value.length != null)
		{
			for (var j = 0; j < value.length; ++j)
			{
				if (EObjectAddArray(arr,value[j]))
				{
					if (deep != null && deep)
					{
						value[j].GetChildrenEObjectsInternal(arr,deep);
					}
				}
			}
		}
		else
		{
			if (EObjectAddArray(arr,value))
			{
				if (deep != null && deep)
				{
					value.GetChildrenEObjectsInternal(arr,deep);
				}
			}
		}
	}

//DEL 	Profile.End("EObject", "GetChildrenEObjectsInternal");	// Debug
}

EObject.prototype.childrenlen = function(memberName)
{
//DEL 	Profile.Start("EObject", "childrenlen");	// Debug

	var value = this.eGet(memberName);

	if(value!=null&&typeof(value) == "object"){
		if(value.length!=null)
			return value.length;
		else
			return 1; //return a single object instead of an array.

	}

	return null;

}

/**
 * @method public EObject.prototype.GetOrigAttrValOrCurrRefObj
 *	This function is used to get the original value of an eAttribute and current value for an EReference. For an EAttribute
 * 	it will return the original value if it exists, or the current value if the original value
 * 	is not set. For an EReference, it will return its current value for an EReference.
 * 	The result is used to set up the original section of a diffgram.  This section of the diffgram only contains the original state
 *  of an updated object.  Please note that the current section of the diffgram contains the current states of updated objects, new objects,
 *  and deletd objects.
 * @param String name
 *	Name of a data member.
 * @return Object value
 *	Value of a data member (either an EAttribute or an EReference).
 * @exception EObjectError
 *	when EAttribute or an EReference couldn't be found in the eobject or any other run time error
 **/
EObject.prototype.GetOrigAttrValOrCurrRefObj = function(name)	// private
{
//DEL 	Profile.Start("EObject", "GetOrigAttrValOrCurrRefObj");	// Debug

	var member = this.GetMember(name);
	if (member == null)
	{
		var args = new Array;
		args[0] = name;
		var Msg = NlsFormatMsg(invalid_property, args);
		Log.error("eobject.GetOriginal", Msg);

//DEL 		Profile.End("EObject", "GetOrigAttrValOrCurrRefObj");	// Debug

		throw new EObjectError(Msg);
	}

	//For EAttributes, if no OriginalValue is set up,
	//Value is the original value because no changes are done.
	if (member.EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTE)
	{
		if(member.OriginalValue != null)
		{
//DEL 			Profile.End("EObject", "GetOrigAttrValOrCurrRefObj");	// Debug

			return member.OriginalValue;
		}
		else
		{
//DEL 			Profile.End("EObject", "GetOrigAttrValOrCurrRefObj");	// Debug

			return member.Value;
		}
	}
	else
	if(member.EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE)
	{

		//The deleted objects are sent back in the current section of the diffgram, and original section of the diffram only contains objects before
		//they are updated.

//DEL 		Profile.End("EObject", "GetOrigAttrValOrCurrRefObj");	// Debug

		//But here we are only interested in objects which are updated and get their original states.
		return member.Value;
	}

//DEL 	Profile.End("EObject", "GetOrigAttrValOrCurrRefObj");	// Debug
}

/**
 * @method private EObject.prototype.GetCurAttrValOrCurAndDelRefObj
 * 	This function is used to get the current value of an eAttribute for an updated or new objecr or a combined list of current and deleted ref objects
 *  for an eReference. For an EAttribute
 * 	it will return the current value if it exists. For an EReference, it will return a
 * 	new array containing both the deleted and current objects
 * 	if both original (deleted) and current fields exist. Otherwise, it will return the original field (deleted) if only the original value exists,
 * 	or the current value if the original value is not set.
 * 	The result is used to set up the current section of a diffgram.  This section of the diffgram  contains the current state
 *  of an updated and new object as well deleted objects.  Please note that the current section of the diffgram contains the current states of updated objects, new objects,
 *  and deletd objects.
 * @param String name
 *	Name of a data member.
 * @return Object value
 *	Value of a data member (either an EAttribute or an EReference).
 * @exception EObjectError
 *	when EAttribute or an EReference couldn't be found in the eobject or any other run time error
 **/
EObject.prototype.GetCurAttrValOrCurAndDelRefObj = function(name)	// private
{
//DEL 	Profile.Start("EObject", "GetCurAttrValOrCurAndDelRefObj");	// Debug

	var member = this.GetMember(name);
	if (member == null)
	{
		var args = new Array;
		args[0] = name;
		var Msg = NlsFormatMsg(invalid_property, args);
		Log.error("eobject.GetCurAttrValOrCurAndDelRefObj", Msg);

//DEL 		Profile.End("EObject", "GetCurAttrValOrCurAndDelRefObj");	// Debug

		throw new EObjectError(Msg);
	}

	//For EAttributes, if no OriginalValue is set up,
	//Value is the original value because no changes are done.
	if (member.EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTE)
	{
//DEL 		Profile.End("EObject", "GetCurAttrValOrCurAndDelRefObj");	// Debug

		return member.Value;
	}
	else
	if(member.EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE)
	{
		//If both Value and OriginalValue are not null,
		//we have to combine Value and OriginalValue because the original record for a deleted ERef
		//is in OriginalValue and the original record for an ERef with updated EAttr
		//is still in Value.
		if(
			member.OriginalValue != null
			&& member.Value != null
		)
		{
			var v = new Array();

			//Add records from Value with a status of UPDATED
			if (
				"object" == typeof(member.Value)
				&& member.Value.length != null
				&& member.Value.length >= 0
			)
			{
				for(i=0;i<member.Value.length;i++)
				{
					v[v.length] = member.Value[i];
				}
			}
			else
			{
				v[v.length] = member.Value;
			}

			//Add deleted records from OriginalValue
			for(i=0;i<member.OriginalValue.length;i++)
			{
				v[v.length] = member.OriginalValue[i];
			}

//DEL 			Profile.End("EObject", "GetCurAttrValOrCurAndDelRefObj");	// Debug

			return v;
		}
		else
		if(
			member.OriginalValue != null
			&& member.Value == null
		)
		{
//DEL 			Profile.End("EObject", "GetCurAttrValOrCurAndDelRefObj");	// Debug

			return member.OriginalValue;
		}
		else
		{
//DEL 			Profile.End("EObject", "GetCurAttrValOrCurAndDelRefObj");	// Debug

			return member.Value;
		}
	}

//DEL 	Profile.End("EObject", "GetCurAttrValOrCurAndDelRefObj");	// Debug
}

/**
 * @method private EObject.prototype.AddProperty
 * Add the property of the geven property's Set/Get function.
 * @param name property name
 *	The member name.
 * @param estructuralFeature member's EStructuralFeature
 *	The information of member.
 **/
EObject.prototype.AddProperty = function(name, estructuralFeature)
{
	//var setter = "Set" + name;
   var setter = "set" + uppercaseFirstLetter(name);
	if ( this[setter] == undefined )
	{
		this[setter] = estructuralFeature.Setter;
	}
	//var getter = "Get" + name;
   var getter = "get" + uppercaseFirstLetter(name);
	if ( this[getter] == undefined )
	{
		this[getter] = estructuralFeature.Getter;
	}
}

/**
 * @method private EObject.prototype.AddToOriginalArrayForERef
 * This is a utility method for adding a deleted reference object  to
 * OriginalValue.  So for ereference, deleted members are kept even if it is deleted. In this way, this can be
 * used for generating a diffgram. The deleted reference members will be sent back in the current section
 * of diffgram XMI.
 * @param Member member
 *	The member of the object which changes are being made.
 * @param deletedObj
 *	The object to be deleted from the value list for the eReference member.
 * @param boolean isDelete
 *	If the isDelete is true, we will delete the object from our model regardless of containment.
 * If the isDelete is false or undefined, we will delete the value object if the containment flag is true, or
 * just remove the reference from this object and leave the value intact.
 **/
EObject.prototype.AddToOriginalArrayForERef = function(member, deletedObj, isDelete)
{
//DEL 	Profile.Start("EObject", "AddToOriginalArrayForERef");	// Debug
   //alert("AddToOriginalArrayForERef");
	var v = null;
	if (null == member.OriginalValue)
	{
		v = new Array();
	}
	else
	{
		//it is an array
		if (
			"object" == typeof(member.OriginalValue)
			&& member.OriginalValue.length != null
			&& member.OriginalValue.length >= 0
		)
		{
			v = member.OriginalValue;
		}
		else//not an array
		{
			v = new Array();
			v[v.length] = member.OriginalValue;
		}
	}

   //remove the container of this obeject
   deletedObj.RemoveContainer(this);



   //If the isDelete is true or the containment flag is true, we will delete the object from our model.
   if (member.EStructuralFeature.IsContainment == true  || isDelete == true)
	{
		//Make a copy of this object to avoid the problem
		//resulting from RemoveContainer() call after an eobject is deleted.
		c= deletedObj.CloneForDeletion();
		c.Status= this.DELETED;
		v[v.length] = c;

		//Mark the parent eobject as below ????
		//have to consider containment
		//this.Status = this.Status | this.CHILD_DELETED;


      //loop here to notify all containers that this eobject is deleted
      for ( var i = deletedObj.Containers.length - 1; i >= 0; --i )
      {

         var data = deletedObj.Containers[i];
         var conatiner = data[0];
         var memberOfContainer = data[1];
         conatiner.eRemove( memberOfContainer.Name, deletedObj);
      }

	}
   //If the isDelete is either false or undefined, and the containment flag is false, we will
   //just remove the reference from this object and leave the value intact.
	else
	{
		v[v.length] = deletedObj;

		//Mark the parent eobject as below ????
		//have to consider containment
		//this.Status = this.Status | this.UPDATED;

	}

   //Logic to check for the status of added or removed eRef objects
   //For any given eRef object, we could have the following 4 scenarios:
   //1. added
   //2. removed
   //3. added, then removed
   //4. removed, then added
   //Case 3 and 4 basically put the object back
    //need a method here to handle XMIID array
	if(member.addedAndremovedObjStatus == null)
	{
		member.addedAndremovedObjStatus = IndexMapCreate();
	}


   var refStatus = IndexMapGet(member.addedAndremovedObjStatus, deletedObj.ID);

   //If this object is inside the status map, put it in
   if(refStatus == null || refStatus == undefined)
   {
      //If it is a true delete
      if(member.EStructuralFeature.IsContainment == true  || isDelete == true)
      {
         IndexMapAdd(member.addedAndremovedObjStatus, deletedObj.ID, this.REF_DELETED);
      }
      //just a remove
      else
      {
         IndexMapAdd(member.addedAndremovedObjStatus, deletedObj.ID, this.REF_REMOVED);

      }
   }
   //If it is alreday there, we need to check its status
   else
   {
      //If it is previuosly added, we need to remove it from the map.  So the net
      //effect is that nothing has happened
      if(refStatus == this.REF_ADDED || refStatus == this.REF_NEW)
      {
         IndexMapRemove(member.addedAndremovedObjStatus, deletedObj.ID);
      }

   }

	// till here
	member.OriginalValue = v;

//DEL 	Profile.End("EObject", "AddToOriginalArrayForERef");	// Debug
}

/**
 * @method private EObject.prototype.AddToOriginalArrayEAttr
 * This is a utility method for adding a deleted reference object or modified value list of a multivalue attribute to an array, called
 * OriginalValue.  So for ereference, deleted members are kept even if it is deleted.  For attributes, any change to the value list,
 * we will just back up the whole list. In this way, this can be
 * used for generating a diffgram. The deleted reference members will be sent back in the current section
 * of diffgram XMI.
 * @param String name
 *	The name of the member of the object.
 * @param Member member
 *	The member of the object which changes are being made.
 * @param currentValue
 *	The current value for the eAttribute.
 **/
EObject.prototype.AddToOriginalArrayEAttr = function(member, currentValue)
{
//DEL 	Profile.Start("EObject", "AddToOriginalArrayEAttr");	// Debug

	//this.ACTION_ADD
	//Only if member.OriginalValue has not been yet backed up, we save it.
	if(member.OriginalValue == null)
	{
		member.OriginalValue = member.value;
		//this object is updated.
		this.ChangedPropertyCount++;
		this.Status = this.Status | this.UPDATED;
	}
	//We have already backed it up, check if the new value is the same as the original value.
	else
	{
		//the two array are the same
		if(this.CompareAttrArray(member.OriginalValue, currentValue) == true)
		{
			member.OriginalValue = null;
			this.ChangedPropertyCount--;

			if(this.ChangedPropertyCount == 0)
			{
				this.Status = this.SERVER_COPY;
			}
		}
		else
		{
			// no status changes are needed.
		}
	}

//DEL 	Profile.End("EObject", "AddToOriginalArrayEAttr");	// Debug
}

/*
EObject.prototype.NormalizeEvelContext = function(str)
{
	return str.replace(this.RegExpObject, '$1this.eGet("$2")$3');
}
*/

/**
 * @method private EObject.prototype.CompareAttrArray
 * This is a utility method for compare the original value array with the current value array for  a multivalue attribute. Pls note that
 * the comparsion is done strictly by value and order.
 * @param Array originalValArray
 *	The original value array.
 * @param Array currentValueArray
 *	The current value array.
 * @return boolean
 *	  True if the two arrays are the same, or false otherwise.
 **/
EObject.prototype.CompareAttrArray = function(originalValArray, currentValueArray)
{
//DEL 	Profile.Start("EObject", "CompareAttrArray");	// Debug

	if(currentValueArray == null)
	{
//DEL 		Profile.End("EObject", "CompareAttrArray");	// Debug

		return false;
	}

	if(originalValArray.length!= currentValueArray.length)
	{
//DEL 		Profile.End("EObject", "CompareAttrArray");	// Debug

		return false;
	}

	for(var i=0; i <originalValArray.length; i++)
	{
		if(originalValArray[i] != currentValueArray[i])
		{
//DEL 			Profile.End("EObject", "CompareAttrArray");	// Debug

			return false;
		}
	}

//DEL 	Profile.End("EObject", "CompareAttrArray");	// Debug

	return true;
}

/**
 * @method public EObject.prototype.eAdd
 *	Method to add a value to a list either for an eAttribute or eReference.
 * @param String name
 *	The name of the member of the object.
 * @param value
 *	The value to be added to the value list for this member.
 * @exception EObjectError
 *	when this member couldn't be found in the eobject
 **/
EObject.prototype.eAdd = function(name,value) // public
{
//DEL 	Profile.Start("EObject", "eAdd");	// Debug
	Log.debug("EObject.prototype.eAdd","Start eAdd for "+name);
	var member = this.FindMember(name);
	if (member == null)
	{
		var args = new Array;
		args[0] = name;
		var Msg = NlsFormatMsg(invalid_property, args);
		Log.error("eobject.eAdd", Msg);

//DEL 		Profile.End("EObject", "eAdd");	// Debug
      alert(Msg);
		throw new EObjectError(Msg);
	}
	Log.debug("EObject.prototype.eAdd","Status = "+this.Status);
	if ( this.Status != null  )
	{
		Log.debug("EObject.prototype.eAdd","About to test for read only = ");
		if ( member.EStructuralFeature.isReadOnly() == 1 )
		{
			Log.debug("EObject.prototype.eAdd","In ReadOnly");
			var args = new Array;
			args[0] = name;
		
			var Msg = NlsFormatMsg(property_cant_change, args);
			Log.error("eobject.eSet", Msg);
		
			throw new EObjectError(Msg);
		}
	}

	var v = null;
	if (null == member.Value)
	{
		v = new Array();
	}
	else
	{
		if (
			"object" == typeof(member.Value)
			&& member.Value.length != null
			&& member.Value.length >= 0
		)
		{
			v = member.Value;
		}
		else
		{
			v = new Array();
			v[v.length] = member.Value;
		}
	}

	if (this.EClass != null)
	{
		this.EClass.ValidateCardinality(name,v.length + 1);
	}

	v[v.length] = value;

	//JRLI
	//below is the logic for diffram

	//1. Add a new value to a containment eRef
	//no need to call AddToOriginalArray because eobject itself has a status of NEW.

	//2. Add a new value to a non-containment eRef
	//need to add to XMIIDArray to track what is added to it.
	if (
		this.Status == null
		|| (this.Status & this.NEW)
		||  this.EClass.DiffgramNeeded == false
	)
	{
		//do nothing for diffgram-related work
	}
   else if(
		member.EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE
	)
	{
		//need a method here to handle XMIID array
		if(member.addedAndremovedObjStatus == null)
		{
			member.addedAndremovedObjStatus = IndexMapCreate();
		}


      var refStatus = IndexMapGet(member.addedAndremovedObjStatus, value.ID);

      var firstTimeAdded = false;

      //alert("inside aAdd() before firstTimeAdded");
      //If there is no containers for this object or Containers is empty, this object has never been
      //added to any object as eRef before.
      if(value.Containers == null || value.Containers.length< 1)
      {
         firstTimeAdded = true;
      }


      //If this object is not in the status map, put it in
      if(refStatus == null || refStatus == undefined)
      {
         if(firstTimeAdded == true && value.Status== this.NEW)
         {
            IndexMapAdd(member.addedAndremovedObjStatus, value.ID, this.REF_NEW);
         }
         else
         {
            IndexMapAdd(member.addedAndremovedObjStatus, value.ID, this.REF_ADDED);
         }
      }
      //If it is alreday there, we need to check its status
      else
      {
         //If it is previuosly removed or deleted, we need to remove it from the map.  So the net
         //effect is that nothing has happened
         if(refStatus == this.REF_REMOVED || refStatus == this.REF_DELETED)
         {
            IndexMapRemove(member.addedAndremovedObjStatus, value.ID);
         }

      }

   }
	else
	//3. add a new value to a multivalue attribute
	//need to call AddToOriginalArray
	if(member.EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTE)
	{
		//this.AddToOriginalArray(name,value);
		this.AddToOriginalArrayEAttr(member, v);
		//this.Status = this.Status | this.UPDATED;
	}

	//diffgram logic ends here

	member.Value = v;

	//JRLI comments
   //This method really only allowas a single value as an input, not an array
	//only if this is a single object, not an array, we look for container
	//means that we first time set up its value
	if (
		value != null
		&& typeof(value) == "object"
		&&  value.length == null
	)
	{
		//var bFound = false;


      //We need to change this code to use the map
      //See if its container has set up
		/*
      for (var i = 0; i < value.Containers.length; ++i)
		{
			if (value.Containers[i] == this)
			{
				bFound = true;
				break;
			}
		} */

      var containerMember = IndexMapGet(this.Containers, this.ID);
      /*if(containerMember != null)
      {
         bFound = true;
      } */


		//If not set up, set it up
		if (containerMember == null)
		{
			value.AddContainer(this, member);
			//Loop thru all members of this value, if this object is a member of this value
			//and this member is a containment, set up this object as the value of the passed-in
			//object. (eOpposite???)

         //This code may not needed???
         /*
			for (var i = 0; i < value.Members.length; ++i)
			{
				var valueMember = value.Members[i];
				if (
					valueMember.EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE
					&& valueMember.EStructuralFeature.getEReferenceType() == this.EClass
					&& !valueMember.EStructuralFeature.IsContainment
				)
				{
					value.eSet(valueMember.Name, this);
					break;
				}
			}  */
		}
	}

   var propertyBinders = this.PropertyBinders;
	var propertyNamedBinders = propertyBinders.Get(name)
	if ( propertyNamedBinders != null )
	{
		for (var i = propertyNamedBinders.length - 1; i >= 0; --i)
		{
			var propertyBinder = propertyNamedBinders[i];
			propertyBinder.FireValueChanged(name, value, propertyBinder.index);
		}
	}

	for (var i = 0; i < this.PropertyArrayBinders.length; ++i)
	{
		this.PropertyArrayBinders[i].FireRowAdded(name,value);
	}

//DEL 	Profile.End("EObject", "eAdd");	// Debug
}

/**
 * @method public EObject.prototype.eGet
 *	This function is used to get value of the eattribute name
 * @param String name
 *	Name of a data member.
 * @return Object value
 *	Value of a data member (either an EAttribute or an EReference).
 * @exception EObjectError
 *	when eattribute member couldn't be found in the eobject or any other run time error
 **/
EObject.prototype.eGet = function(name)	// public
{
//DEL 	Profile.Start("EObject", "eGet");	// Debug

	var member = this.FindMember(name);
	if (null == member)
	{
		var args = new Array;
		args[0] = name;
		var Msg = NlsFormatMsg(invalid_property, args);
		Log.error("eobject.eGet", Msg);

//DEL 		Profile.End("EObject", "eGet");	// Debug
      alert(Msg);
		throw new EObjectError(Msg);
	}

	var estructuralFeature = member.EStructuralFeature;
	if (
		estructuralFeature.CLASSTYPE != EStructuralFeature.CLASSTYPE_EATTRIBUTECALCULATE
		// If already calculated, use the result.
//		|| member.Value != undefined
	)
	{
//DEL 		Profile.End("EObject", "eGet");	// Debug

		return member.Value;
	}

	try
	{

		 // Support that there is short name("hoge" same as this.eGet("hoge")) in calculate string.

		var str = estructuralFeature.getExpression();
		//str = this.NormalizeEvelContext(str);
		// Cache the calculate result.
		member.Value = eval(str);

//DEL 		Profile.End("EObject", "eGet");	// Debug

		return member.Value;
	}
	catch (e)
	{
		var args = new Array;
		args[0] = member.EStructuralFeature.getExpression();
		args[1] = e.description;
		var Msg = NlsFormatMsg(invalid_expression, args);
		Log.error("eobject.eGet", Msg);

//DEL 		Profile.End("EObject", "eGet");	// Debug

		throw new EObjectError(Msg);
	}

//DEL 	Profile.End("EObject", "eGet");	// Debug
}

/**
 * @method public EObject.prototype.eSet
 * 	This function is used to set the value of the eattribute of that eobject
 * @param String name
 *	Name of a data member.
 * @param Object value
 *	Value of a data member (either an EAttribute or an EReference).
 * @return boolean
 *	True means successfully change the data model. otherwise false.
 * @exception EObjectError
 *	when eattribute member couldn't be found in the eobject or this member's type is
 *	eattributeCalculate
 **/
EObject.prototype.eSet = function(name,value) // public
{
//DEL 	Profile.Start("EObject", "eSet");	// Debug

	var member = this.FindMember(name);
	if (null == member)
	{
		var args = new Array;
		args[0] = name;
		var Msg = NlsFormatMsg(invalid_property, args);
		Log.error("eobject.eSet", Msg);

//DEL 		Profile.End("EObject", "eSet");	// Debug
      alert(Msg);
		throw new EObjectError(Msg);
	}

	var estructuralFeature = member.EStructuralFeature;
	
	// While setting up the model from ecreator, skip checking for read only
	// to allow the initial setting of the values.
	if ( this.Status != null  )
	{
		Log.debug("EObject.eSet","In eSet about to test for Readonly");
		if ( estructuralFeature.isReadOnly() == 1 )
		{
			var args = new Array;
			args[0] = name;
	
			var Msg = NlsFormatMsg(property_cant_change, args);
			Log.error("eobject.eSet", Msg);
	
	//DEL 		Profile.End("EObject", "eSet");	// Debug
	
			throw new EObjectError(Msg);
		}
	}

   //We do not want to do any validation when we are setting up model inside ecreator.js
	if (this.Status != null && this.EClass != null)
	//if (this.EClass != null)
	{
		value = this.EClass.ValidateType(name, value);
	}

	var bChanged = false;


	if (member.Value != value)
	{

      if (this.Status != null )
      {
         if (value == null)
   		{
   			estructuralFeature.ValidateCardinality(1);
   		}
   		else
   		if (
   			typeof(value) == "object"
   			&& value.length != null
   		)
   		{
   			estructuralFeature.ValidateCardinality(value.length);
   		}
      }


      //Why we need to remove container because if it is set up, we will just skip it.
		/*if (
			member.Value != null
			&& typeof(member.Value) == "object"
			&& member.Value.length == null
		)
		{
			member.Value.RemoveContainer(this);
		}*/

		// JRLI
		// No diffgram-related flags need to be set if
		// 1) this is the first time to set this value during initial loading
		// 2) this eobject is created on client side.  In this case, any susequent changes after creation
		//    is treated as part of creation, so we only care about the final version of the object.
		// 3) Ignore any diffgram-related work if diffgramNeeded flag is false for the eclass of this eobject
		// 4) The diffgram logic has been performed in eRemove()
		//    This case is due to the fact that eSet() is called in eRomeve().  All flags have been updated
		//    there already.
		if(
			this.Status == null
			|| (this.Status & this.NEW)
			||  this.EClass.DiffgramNeeded == false
		)
		{
			//do nothing for diffgram-related work
		}
		else
		if (this.Processed == true)
		{
			// reset the flag and do nothing else
			this.Processed = false;
		}
		else
		// We are making changes to the original value
		{
			// alert("inside eSet()");
			// first time to change this value
			if ( member.OriginalValue == null )
			{
				// update the flag
				this.Status = this.Status | this.UPDATED;
				// increament the counter
				this.ChangedPropertyCount++;
				member.OriginalValue = member.Value;
			}
			else
			//If the original value is set during 1st update, and the new value
			//during this update is the same as the original value, we will try to
			//restore the original state of the eobject.
			if(
				member.OriginalValue != null
				&& member.OriginalValue == value
			)
			{
				// decrement the counter
				this.ChangedPropertyCount--;
				member.OriginalValue = null;
				// if no other changes occurred to this object, we can treat this object
				// as unchanged
				if(this.ChangedPropertyCount == 0)
				{
					this.Status = this.SERVER_COPY;
				}
			}
			else
			// If the original value is set during 1st update,and the new value is different,
			// we do nothing for flags.
			if(
				member.OriginalValue != null
				&& member.OriginalValue != value
			)
			{
			// do nothing because we do not need to update flags any more
			}
			//write out to the jounal
		}

		//set the current value
		member.Value = value;
		Log.debug("EObject.eSet","Value is now "+member.Value);

		bChanged = true;
	}

	// Check to see if containment is defined by the EReference
	// JRLI comments
	// If it is a single object and a containment (?), add this object as the container of the value

   //we need to handle the list situation
   //If it is a list, loop thry the list
	/*if (
		(
			(estructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE)
			&& (estructuralFeature.IsContainment )
		)
		&& (
			value != null
			&& typeof(value) == "object"
			&&  value.length == null
		)
	)
	{
		value.AddContainer(this, member);
	}*/
   //If this is a EReference, we need to set up containers
   if (estructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE)
   {
      //If it is a single object
		if( value != null && typeof(value) == "object" &&  value.length == null)
      {
         value.AddContainer(this, member);
      }
      //if this is a list, we need to add a container for every object
      else if(value != null && typeof(value) == "object" &&  value.length != null)
      {
         for(var i=0; i<value.length; i++)
         {
         	if(value[i]){
            	value[i].AddContainer(this, member);
            }
         }
      }
	}

   //We do not want to fire events when we are setting up model inside ecreator.js
   if (this.Status != null && bChanged)
	//if (bChanged)
	{

      //alert("before fire");
      var propertyBinders = this.PropertyBinders;
		var propertyNamedBinders = propertyBinders.Get(name)
		if ( propertyNamedBinders != null )
		{
			for (var i = propertyNamedBinders.length - 1; i >= 0; --i)
			{
				var propertyBinder = propertyNamedBinders[i];
				propertyBinder.FireValueChanged(name, value, propertyBinder.index);
			}
		}

		// fire change to any calculate property
		var length = this.Members.length;
		for (var i = 0; i < length; ++i)
		{
			var member = this.Members[i];
			if (member.EStructuralFeature.CLASSTYPE != EStructuralFeature.CLASSTYPE_EATTRIBUTECALCULATE)
			{
				continue;
			}

			// Clear EAttributeCalculate's calculated result Value.
			member.Value = undefined;

			var name = member.Name;

			var propertyNamedBinders = propertyBinders.Get(name);
			if ( propertyNamedBinders == null )
			{
				continue;
			}

			for (var j = propertyNamedBinders.length - 1; j >= 0; --j)
			{
				// Recalculate EAttributeCalculate?
				propertyNamedBinders[j].FireValueChanged(name, this.eGet(name));
			}
		}
	}

//DEL 	Profile.End("EObject", "eSet");	// Debug

	return true;
}

/**
 * @method public EObject.prototype.eRemove
 *	Method to remove a value from a list either for an eAttribute or eReference.
 * @param String name
 *	The name of the member of the object.
 * @param value
 *	The value to be removed from the value list for this member.
 * @param boolean isDelete
 *	If the isDelete is true, we will delete the object from our model regardless of containment.
 * If the isDelete is false or undefined, we will delete the value object if the containment flag is true, or
 * just remove the reference from this object and leave the value intact.
 * @exception EObjectError
 *	when this member couldn't be found in the eobject
 **/
EObject.prototype.eRemove = function(name, value, isDelete) // public
{
//DEL 	Profile.Start("EObject", "eRemove");	// Debug

	var member = this.GetMember(name);

	//alert("begin eRemove");
	var v = this.eGet(name);
	if (null == v)
	{
		if (null == this.EClass)
		{
//DEL 			Profile.End("EObject", "eRemove");	// Debug

			return;
		}

		var args = new Array;
		args[0] = name;
		var Msg = NlsFormatMsg(invalid_property, args);
      alert(Msg);
		Log.error("eobject.eRemove", Msg);

//DEL 		Profile.End("EObject", "eRemove");	// Debug

		throw new EObjectError(Msg);
	}
	Log.debug("EObject.prototype.eRemove","About to test for read only");
		if ( member.EStructuralFeature.isReadOnly() == 1 )
		{
			Log.debug("EObject.prototype.eRemove","In ReadOnly");
			var args = new Array;
			args[0] = name;
		
			var Msg = NlsFormatMsg(property_cant_change, args);
			Log.error("eobject.eSet", Msg);
		
			throw new EObjectError(Msg);
		}

	// 1. Remove a value from a containment eRef
	// need to delete  the eobject and call AddToOriginalArray().

	// 2. Remove a value from a non-containment eRef
	// no need to delete the eobject  but need to call AddToOriginalArray().

	// 3. Remove a value from a multivalue attribute
	// need to call AddToOriginalArray() to back up the original value

	// set Processed  flag as true
	this.Processed=true;

	// JRLI comments
	// If it is an array
	if (
		"object" == typeof(v)
		&& v.length >= 0
	)
	{
		var arr = new Array();
		for (var i = 0; i < v.length; ++i)
		{
			//If two objects are not the same, keep it
			if (v[i] != value)
			{
				arr[arr.length] = v[i];
			}
			else
			//if two objects are the same and the deleted oject is not created on client,
			//remove it and keep it to the array for original references
			//if (v[i] == value && value.Status != value.NEW )
			if (
				member.EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE
				&& v[i] == value
				&& value.Status != value.NEW
			)
			{
				var eobject = v[i];
				//Only do diffgram-related work if diffgramNeeded flag is true for the eclass of this eobject
				if(this.EClass.DiffgramNeeded == true)
				{
					//this.AddToOriginalArray(name, eobject);
					this.AddToOriginalArrayForERef(member, eobject, isDelete);

				}
				//write out to the journal
				//code for journal
			}
			//if the deleted object is created on client,
			//just discard it and it will not be sent back to server
			else
			{
				//still need to write out to the journal
				//code for journal
			}
		}
		v = arr;
	}
	//If it is a single object
	else
	{
		v = null;
		//Only do diffgram-related work if diffgramNeeded flag is true for the eclass of this eobject
		if(
			member.EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE
			&& this.EClass.DiffgramNeeded == true
		)
		{
			//this.AddToOriginalArray(name, value);
			this.AddToOriginalArrayForERef(member, value, isDelete);

		}
		Log.debug  ("eobject.eRemove ", "entering else:" + name);
	}

	if (member.EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTE)
	{
		 //Only do diffgram-related work if diffgramNeeded flag is true for the eclass of this eobject
		if(this.EClass.DiffgramNeeded == true )
		{
				  //this.AddToOriginalArray(name, eobject);
			this.AddToOriginalArrayForAttr(member, v);
		}
		//write out to the journal
		//code for journal
	}

	this.eSet(name,v);

	//If the value is a single obejct, we can remove its container
   /*  this code has to be moved to  AddToOriginalArrayForERef(), so we can take care of this for every object
	if (
		value != null
		&& typeof(value) == "object"
		&&  value.length == null
	)
	{
		value.RemoveContainer(this);
	}
   */

	for (var i = 0; i < this.PropertyArrayBinders.length; ++i)
	{
		this.PropertyArrayBinders[i].FireRowDeleted(name,value);
	}

//DEL 	Profile.End("EObject", "eRemove");	// Debug
}

EObject.prototype.Sort = function(propertyname,sortOrder)	// public
{

	var rows = this.eGet(propertyname);
	if (
		null == rows
		|| typeof(rows) != "object"
		|| rows.length == null
		|| rows.length <= 0
	)
	{
//DEL 		Profile.End("EObject", "Sort");	// Debug

		return;
	}

	sortOrder = Trim(sortOrder);
	if (
		null == sortOrder
		|| sortOrder.length == 0
	)
	{
//DEL 		Profile.End("EObject", "Sort");	// Debug

		return;
	}

	var columns = sortOrder.split(",");
	if (0 == columns.length)
	{
//DEL 		Profile.End("EObject", "Sort");	// Debug

		return;
	}

	var sortArray = SortRows(columns[0],rows);
	sortArray = SortAllColumns(sortArray,columns,1);

	rows = new Array();
	for (var i = 0; i < sortArray.length; ++i)
	{
		rows[rows.length] = sortArray[i].m_row;
	}

	this.eSet(propertyname,rows);

//DEL 	Profile.End("EObject", "Sort");	// Debug
}


EObject.prototype.toStr = function (level)
{
   level = (level==null) ? '' : level+'  ';
	var refStr = " ";
	var childStr = "";
	var lineBreak = "\n";

	var str = level+"<" +this.EClass.Name + " id=" + this.ID + " ";

	for (var i = 0; i < this.Members.length; ++i) {
		   if(this.Members[i].EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTECALCULATE)
			continue;

      if (this.Members[i].EStructuralFeature.CLASSTYPE != EStructuralFeature.CLASSTYPE_EREFERENCE)
			str += this.Members[i].Name + "= \"" + this.eGet(this.Members[i].Name) + "\" ";
		else
      {
			var child = this.eGet(this.Members[i].Name);
			if (this.Members[i].EStructuralFeature.IsContainment == true)
         {
            //If it is an array
            if(child != null && child.length != null)
            {

				   for(var j = 0; j < child.length; j++)
               {
					   childStr += child[j].toStr(level);
				   }
            }
            //If it is a single obj
            else if(child != null)
            {
               childStr += child.toStr(level);
            }
			}
         else
         {
				//if ("object" == typeof (child) )
            if(child != null && child.length != null)
            {
			for(var j = 0; j < child.length; j++)
               {
				str += this.Members[i].Name + "= \"";
				str += (child[j] == null?"null":child[j].ID) + "\" ";
			  }
			}
             //If it is a single obj
            else if(child != null)
            {
               str += this.Members[i].Name + "= \"";
				   str += child.ID + "\" ";
            }
			}
		}
	}

	return childStr=="" ? str + refStr + "/>"+lineBreak
                         : str + refStr + ">"+lineBreak + childStr + level+"</" + this.EClass.Name + ">"+lineBreak;
}


/**
 * @method public EObject.prototype.getSignature
 *	This method is used to get unique signature to identify this eobject across diffrent HTTP requests. With the signature
 * of an eobject, we can restore the UI state for an eobejct on a particular control, such as a tree node (closed or open).
 * The signature of an eobject is represented by cancatenating all attributes and referneces which are part of key fields
 * of the eobject.  In the case of a reference as a part of a key, the getSignamture() of the reference object will be called.
 * @param parent the parent eobject
 * @return String signature
 *		The unique signature of this eobject.
 * @exception EObjectError
 *		Exception thrown if an EReference as part of key is not an obejct or a single obejct.
 **/
EObject.prototype.getSignature = function(parent)
{
//DEL 	Profile.Start("EObject", "getSignature");	// Debug

	//If this eobejct is the original copy and signature is not null,
	//just return it to make it more efficient

	//alert("before getSignature()");
   /*
	if(
		this.signature != ""
		&& this.Status==this.SERVER_COPY
	)
	{
//DEL 		Profile.End("EObject", "getSignature");	// Debug

		return this.signature;
	}
   */

	//prefix signature with eClass name at the begining for any object
	//If there is no key field, the eClass name will be used as the signature for
	//eobject.  We will treat this type of object as singleton.
	var signature = this.EClass.Name;
	var name = null;
	var value = null;


	//loop thru all members
	for (var i = 0; i < this.Members.length; ++i)
	{
		var feature = this.Members[i].EStructuralFeature;
		//If it is an EAttribute, just simply concatenate it to the signature.
		if(feature.iD == true)
		{
			name = this.Members[i].Name;
			value = this.GetCurAttrValOrCurAndDelRefObj(name);

			//If this is an EAttribute and in CURRENT_MODE
			if (feature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTE)
			{
				//This must be a single value to be part of primary key.
				if (
					(
						feature.getUpperBound() == -1
						|| feature.getUpperBound() > 1
						|| (
							feature.getLowerBound() < feature.getUpperBound()
							&& feature.getUpperBound() > 1
						)
					)
					&& value.length != null
					&& "number" == typeof(value.length)
				)
				{
					//Need to add this string into properties file
					var args = new Array;
					args[0] = name;
					args[1] = this.EClass.Name;
					var Msg = NlsFormatMsg(multivalue_attr_as_key_error, args);
					Log.error("eobject.getSignature", Msg);

//DEL 					Profile.End("EObject", "getSignature");	// Debug

					throw new EObjectError(Msg);
				}
				else
				{
					signature = signature + value;
				}
			}
			else
			//If this is an EAttribute
			if (feature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE)
			{
			   if(value!=null)
            {
				   //This must be a single obejct to be part of primary key.
				   //We will call getSignature() of the EReference.  This call could be recursive.
				   if ("object" == typeof(value) && value.length == null && (!parent || value != parent))
				   {
					  signature = signature + value.getSignature(this);
				   }
				   else if(value != parent) //If this is not an eobject and this is not a single object, throw an exception
				   {
					  //Need to add this string into properties file
					  var args = new Array;
						   args[0] = name;
						   args[1] = eobject.EClass.Name;
						   var Msg = NlsFormatMsg(ereference_list_as_key_error, args);
						Log.error("eobject.getSignature", Msg);
					  throw new EObjectError(Msg);
				   }
			   }//else if
         }
		}//if
	}//loop

	this.signature = signature;

//DEL 	Profile.End("EObject", "getSignature");	// Debug

	return this.signature;
}

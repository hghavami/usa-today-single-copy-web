//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003.  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//==================================================================


/////////////////////////////////////////////////////////////////////////////////////////////////
// PropertyArrayBinder

//
// Public
//
function PropertyArrayBinder(control, eobject, propertyname, onRowAdded, onRowDeleted)
{
//DEL 	Profile.Start("PropertyArrayBinder", "<Init>");	// Debug

	this.Control = control;	// public
	this.EObject = eobject;	// public
	this.PropertyName = propertyname; // public
	this.OnRowAdded	= onRowAdded;	// public
	this.OnRowDeleted = onRowDeleted;	// public

//DEL 	Profile.End("PropertyArrayBinder", "<Init>");	// Debug
}

PropertyArrayBinder.prototype.DataBind = function()	// public
{
//DEL 	Profile.Start("PropertyArrayBinder", "DataBind");	// Debug

	this.EObject.PropertyArrayBinders.Add(this);

//DEL 	Profile.End("PropertyArrayBinder", "DataBind");	// Debug
}

PropertyArrayBinder.prototype.FireRowAdded = function(name,value)	// private
{
//DEL 	Profile.Start("PropertyArrayBinder", "FireRowAdded");	// Debug

	if (
		this.PropertyName == name
		&& this.OnRowAdded != null
	)
	{
		this.OnRowAdded(this.Control,this.EObject,value);
	}

//DEL 	Profile.End("PropertyArrayBinder", "FireRowAdded");	// Debug
}

PropertyArrayBinder.prototype.FireRowDeleted = function(name,value)	// private
{
//DEL 	Profile.Start("PropertyArrayBinder", "FireRowDeleted");	// Debug

	if (
		this.PropertyName == name
		&& this.OnRowDeleted != null
	)
	{
		this.OnRowDeleted(this.Control,this.EObject,value);
	}

//DEL 	Profile.End("PropertyArrayBinder", "FireRowDeleted");	// Debug
}

// PropertyArrayBinder End
/////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////
// PropertyBinder

//
// Public
// PropertyBinder Object
//

/**
 * @class public PropertyBinder
 * @constructor PropertyBinder
 * 	 This class is functioning as a control center. so that a change in the data model will fire
 *	 all the registered events which are bound to the same data model.
 * @param EObject eobject
 *	 The bound eobject
 * @param String propertyname
 *	the bound attribute of the eobject
 * @param Object control
 *	The html control which are rendered on the browser, like "input text field".
 * @param String controlperoperty
 *	The property of the control, like the value property of "input text field".
 * @param String controlChangeEventName
 *	The event will be invoked when data model change, like "onchange" event.
 * @param String callbackChangeEvent
 *  The event will be invoked in the case that this control fire the change, and all
 *	the bound events are fired.
 **/

function PropertyBinder(
	eobject,
	propertyname,
	control,
	controlproperty,
	controlChangeEventName,
	callbackChangeEvent
)
{
//DEL 	Profile.Start("PropertyBinder", "<Init>");	// Debug

	this.EObject = eobject;	// public
	this.Control = control;	// public
	this.ControlProperty = controlproperty;	// public
	this.ControlChangeEventName = controlChangeEventName;	// public
	this.CallbackChangeEvent = callbackChangeEvent;	// public
   this.Converter = null;
   this.index= null;


	// Parse propertyname to extract propertyName and index, such as price[2]
	var pos1 = propertyname.indexOf("[");
	if(pos1 != -1)
	{
		this.PropertyName =  propertyname.substring(0, pos1);
		var pos2 = propertyname.indexOf("]");
		this.index = propertyname.substring(pos1+1, pos2);
	}
	else
	{
		this.PropertyName = propertyname;
	}

//DEL 	Profile.End("PropertyBinder", "<Init>");	// Debug
}

PropertyBinder.prototype.ControlGetFunction= null;	// public
PropertyBinder.prototype.ControlSetFunction= null;	// public
//PropertyBinder.prototype.dataProcObjArray = null;
//PropertyBinder.prototype.index= null;

PropertyBinder.prototype.activateDataSet = function(obj)
{
//DEL 	Profile.Start("PropertyBinder", "activateDataSet");	// Debug

	this.DataUnbind();
	this.EObject = obj;

//DEL 	Profile.End("PropertyBinder", "activateDataSet");	// Debug
}

PropertyBinder.prototype.refresh = function()
{
//DEL 	Profile.Start("PropertyBinder", "refresh");	// Debug

	if(this.EObject)
		this.DataBind();
	else{
		//fix bug in Yongcheng's insurance page. new customer has no contract and claim information.
		/*if(this.ControlSetFunction!= null)
		{
			this.Control[this.ControlSetFunction]("");
		}
		else
		{
			this.Control[this.ControlProperty]= "";
		}*/
		this.setValue("");
	}
//DEL 	Profile.End("PropertyBinder", "refresh");	// Debug
}
/**
 * @method public PropertyBinder.prototype.setConverter
 * 	set the converter
 * @param object aConverter
 *	the converter object
 **/
PropertyBinder.prototype.setConverter = function(aConverter)
{
   this.Converter= aConverter;
}



PropertyBinder.prototype.OnError = function(action, exception)	// public
{
	var args = new Array;
	args[0] = action;
	args[1] = exception.description;
	alert(NlsFormatMsg(error_on_propertybinder, args));
}
/**
 * @method public PropertyBinder.prototype.OnPropertyBinderChange
 * 	it is called when the control's data changed
 **/
PropertyBinder.prototype.OnPropertyBinderChange = function()	// private
{
//DEL 	Profile.Start("PropertyBinder", "OnPropertyBinderChange");	// Debug

   //alert("OnPropertyBinderChange for " + this.event.srcElement.id);

	try
	{
		//var propertyBinder = this.event.srcElement.propertyBinder;
		var value = null;
		var element = null;
		if(isIE()){
			element = this.event.srcElement ? this.event.srcElement : this.event.target;
		}
		else{
			element = this;
		}
		var propertyBinder = element.propertyBinder;
		if (propertyBinder.ControlGetFunction != null)
		{
			value = element[propertyBinder.ControlGetFunction]();
		}
		else
		{
			value = element[propertyBinder.ControlProperty];
		}

		if(propertyBinder.Converter!=null)
		{
		              //using the westford converter methods
         value = getModelValue(propertyBinder.Converter.stringToValue(value));
		}

		 //var valueInModel = propertyBinder.EObject.eGet(propertyBinder.PropertyName);
		 var valueInModel = null;
		 //Handle multi-value case
		 //Replace the value inside the vaue array before we call eSet()
		 if(propertyBinder.index != null)
		 {
			var temp = propertyBinder.EObject.eGet(propertyBinder.PropertyName);
			valueInModel = new Array();
			for(var i=0; i<temp.length ; i++){
				valueInModel[i] = temp[i];
			}
			valueInModel[propertyBinder.index] = value;
		 }
		 else
		 //Otherwise, just set it back
		 {
			valueInModel = value;
		 }
		if (propertyBinder.EObject != null)
		{
			var retval = propertyBinder.EObject.eSet(propertyBinder.PropertyName,valueInModel);
			if (typeof event != 'undefined' &&event != null)
			{
				event.returnValue = retval;
			}
		}

		if (propertyBinder.CallbackChangeEvent != null)
		{
			propertyBinder.CallbackChangeEvent(propertyBinder.EObject,propertyBinder.PropertyName,valueInModel);
		}
	}
	catch(e)
	{
		if (propertyBinder.OnError != null)
		{
			 propertyBinder.OnError("CHANGEVALUE",e);
		}

		propertyBinder.DataUnbind();
		propertyBinder.DataBind();

		if (typeof event != 'undefined' &&event != null)
		{
			event.returnValue = false;
		}
	}

//DEL 	Profile.End("PropertyBinder", "OnPropertyBinderChange");	// Debug
}
/**
 * @method public PropertyBinder.prototype.DataBind
 * 	get the value from the data model and display it on the control.
 *	Also setup all the binding.
 **/
PropertyBinder.prototype.DataBind = function()	// public
{
//DEL 	Profile.Start("PropertyBinder", "DataBind");	// Debug

	var valueInModel = this.EObject.eGet(this.PropertyName);
	//alert(" PropertyBinder.prototype.DataBind this.PropertyName:" + this.PropertyName);
	var value = null;

	//Handle multi-value case
	//Replace the value inside the vaue array before we call eSet()
	if(this.index != null)
	{
		value = valueInModel[this.index];
	}
	else
	//Otherwise, just set it back
	{
		value = valueInModel;
	}

	if(this.Converter!=null)
   {
   	//use the westford converter
      //value = this.Converter.convertFromDataToString(value);
      value = this.Converter.valueToString(modelValueToObject(this.Converter, value));
   }
   	//DO NOT change this line to if(value). It will fail for boolean values
    //if(value != null && value != 'undefined'){
		this.setValue(value);
	//}
	

   //alert("DataBind before .Add");
	this.EObject.PropertyBinders.Add(this);
	//Fenil Fix - attach to control event, dont override it...this way all handlers  attached to this event will get called
	if (this.ControlChangeEventName != null)
	{
		//this.Control[this.ControlChangeEventName] = this.OnPropertyBinderChange;
		//this.Control.propertyBinder = this;
		if(this.Control.attachEvent){
			this.Control.attachEvent(this.ControlChangeEventName, this.OnPropertyBinderChange);
		}
		else{
			if(this.ControlChangeEventName.length>2 && this.ControlChangeEventName.substring(0,2) == "on"){
				var tempString = this.ControlChangeEventName.substring(2, this.ControlChangeEventName.length);
				this.Control.addEventListener(tempString, this.OnPropertyBinderChange, false);
			}
		}
		this.Control.propertyBinder = this;
	}

//DEL 	Profile.End("PropertyBinder", "DataBind");	// Debug
}
/**
 * @method public PropertyBinder.prototype.DataBind
 * 	unbind the control with the data model. Remove control's all the binder.
 **/
PropertyBinder.prototype.DataUnbind = function()	// public
{
//DEL 	Profile.Start("PropertyBinder", "DataUnbind");	// Debug

	if(this.EObject!=null)
	{
		this.EObject.PropertyBinders.Remove(this);
		//Fenil fix...remove only the event listener that we attached
		if (this.ControlChangeEventName != null)
		{
			//this.Control[this.ControlChangeEventName] = null;
			//this.Control.propertyBinder = null;
			if(this.Control.detachEvent){
				this.Control.detachEvent(this.ControlChangeEventName, this.OnPropertyBinderChange);
			}
			else{
				if(this.ControlChangeEventName.length>2 && this.ControlChangeEventName.substring(0,2) == "on"){
					var tempString = this.ControlChangeEventName.substring(2, this.ControlChangeEventName.length);
					this.Control.removeEventListener(tempString, this.OnPropertyBinderChange, false);
				}
			}
		}
		this.Control.propertyBinder = null;
	}
//DEL 	Profile.End("PropertyBinder", "DataUnbind");	// Debug
}
/**
 * @method public PropertyBinder.prototype.FireValueChanged
 * 	When the controls value is changed, set the changed value to
 *  the data model, as well as fire all the controls which bound
 * 	to the data model.
 **/
PropertyBinder.prototype.FireValueChanged = function(name, valueInModel, index)	// private
{
//DEL 	Profile.Start("PropertyBinder", "FireValueChanged");	// Debug
	//alert("FireValueChanged called");
	//handle multivalue case
	if(index != null)
	{
		value = valueInModel[index];
	}
	else
	{
		value = valueInModel;
	}

	if (this.PropertyName != name)
	{
//DEL 		Profile.End("PropertyBinder", "FireValueChanged");	// Debug

		return;
	}

	if (this.PropertyName == name)
   {
		/*
      var formatObj = this.dataProcObjArray[0];
		if(formatObj!=null)
		{
			var resultObj = formatObj.formatOut(this.PropertyName, value, this.EObject);
			if(!resultObj.fError)
			{
				value =resultObj.value ;
			}
		}
      */
      if(this.Converter!=null)
      {
		   //use the westford converter
         //value = this.Converter.convertFromDataToString(value);
         value = this.Converter.valueToString(modelValueToObject(this.Converter, value));
      }
	}
	//DO NOT change this line to if(value). It will fail for boolean values
    //if(value != null && value != 'undefined'){
		this.setValue(value);
	//}
	


//DEL 	Profile.End("PropertyBinder", "FireValueChanged");	// Debug
}

/**
 * @method public PropertyBinder.prototype.setValue
 * 	Set the new value to the control
 * @param String value
 *	The new value
 **/
PropertyBinder.prototype.setValue = function(value)
{
	
	if(this.ControlSetFunction && (!this.ControlGetFunction || (this.Control[this.ControlGetFunction] != value || (typeof this.Control[this.ControlGetFunction] != typeof value)))){
		if(value != null && value != 'undefined'){
			this.Control[this.ControlSetFunction](value);
		}
		else{
			this.Control[this.ControlSetFunction]("");
		}
	}
	else
	{
		if(this.Control[this.ControlProperty] != value || (typeof this.Control[this.ControlProperty] != typeof value)){
			if(value != null && value != 'undefined'){
				this.Control[this.ControlProperty] = value;
			}
			else{
				this.Control[this.ControlProperty] = "";
			}
			if(this.ControlChangeEventName && this.Control[this.ControlChangeEventName]){
				hX_3.imp.fireEvent(this.Control, this.ControlChangeEventName);
			}
		}
	}
}

// PropertyBinder End
/////////////////////////////////////////////////////////////////////////////////////////////////

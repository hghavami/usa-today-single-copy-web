//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003.  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//=================================================================


/**
 * @class public TreeAdapter
 * @constructor TreeAdapter
 * 	The TreeAdapter is used to facilitate the rendering of data model in
 *  a user interface Tree control.
 * @param tree treeCtrl
 *	 tree control to render the data model
 * @param EObject eobjRoot
 *	 the root eobject which is used to populate the tree.
**/


function TreeAdapter(treeCtrl, eobjRoot)
{

	// Variable Declarations
	this.treeControl = treeCtrl;
	this.treeControl.Adapter = this;
	this.xmiModel = eobjRoot;
	this.eobjRoot = eobjRoot;
	this.treeObjects = new Array();
	this.maskedNames= new Array();
//	this.LoadedObjects = new Array();
	this.expandedNode = new Array();
	this.nodeHandler = new Array();
}

TreeAdapter.prototype.treeStyles = null;
TreeAdapter.prototype.iconStyles = null;


/**
* @method public TreeAdapter.prototype.refresh
*   This function is used to redisplay the tree
**/
	TreeAdapter.prototype.refresh = function () {

		this.treeControl.updateControl();

	}

/**
* @method public TreeAdapter.prototype.activateDataSet
*   this function is used to construct the new data set based on the passed eobject. To improve
* 	the performance, the first time the tree is displayed, only the first level of nodes are rendered.
* @param EObject eobject
*	the eobject which need to be rendered in tree control.
**/

	TreeAdapter.prototype.activateDataSet = function(eobject) {
		if (null == eobject) {
			return;
		}

		if(this.treeControl.a_index!=null)
			this.treeControl.a_index = [];

		this.eobjRoot = eobject;
//		this.LoadedObjects = new Array();
		this.eobjIndex = 0;
		//nodeLevel = 0;
		eobjInstance = eobject;


		this.treeControl.childItems = [];
		this.treeControl.depth = -1;


		this.bind();



	}

/**
* @method public TreeAdapter.prototype.setMask
*   this function is used to set the control's mask array so that all the items in this array will not displayed
* @param String maskstr
*	String of the to be masked node's name, seperated by ","
**/
	TreeAdapter.prototype.setMask = function(maskstr)
	{
		this.maskedNames=maskstr.split(",");
	}

/**
* @method private TreeAdapter.prototype.isMasked
*   this function is used to determine if the name which is passed in should be filtered out
* @param String name
*	The name of the node
* @return boolean
*	True if the name is in the masked array. otherwise false.
**/
	TreeAdapter.prototype.isMasked = function(name)
	{
		for (var i = 0; i < this.maskedNames.length; ++i) {
			if (this.maskedNames[i]==name)
				return true;
		}
		return false;
	}
/**
* @method private TreeAdapter.prototype.applyImage
*   this function is used to get the icons path of a specific class name if user supply
*	one,otherwise return null
* @param String name
*	The eclass name of the node
* @return Array
*	Take file structure as example, if the eclass name is DIR, the first element of the returned
*	array is directory open icon,the second element is directory close icon
*	If the eclass name is FILE, the first element is base icon, the second element is folder open icon
*	do we use the second icon in FILE case?
**/
	TreeAdapter.prototype.applyImage = function ( eobj ) {
		if (this.iconFilePaths == null)
			return null;

		var filePath = new Array();
		if(this.iconFilePaths!=null)
		{
			for (var i = 0; i < this.iconFilePaths.length; i++) {

				var style = this.iconFilePaths[i];


				if (eobj.EClass.Name == Trim(style[0])) {



					if(style[1].indexOf("=")!=-1){
						//every node maybe have different icon.
						var valueArr = style[1].split("=");

						if(eobj.eGet(Trim(valueArr[0])) == Trim(valueArr[1])){

							filePath[0] = Trim(style[2]);
							filePath[1] = Trim(style[3]);
							filePath[2] = style[4];
							filePath[3] = style[5];
							break;
						}

					}else{
						//same level of node has the same icon.

							filePath[0] = Trim(style[1]);
							filePath[1] = Trim(style[2]);
							filePath[2] = style[4];
							filePath[3] = style[5];

						//break; //fix defect 268153 found by mike d. 10/29
					}
				}



			}
		}

		return filePath;

	}
/**
* @method private TreeAdapter.prototype.applyStyle
*   If the user provide node display type, then when the node is rendered, it will use this type
*	otherwise display class name.
* @param String name
*	The eclass name of the node
* @param EObject obj
*	the eobject is used to get other display attribute value based on the eclass name.
* @return String displayName
*	If there is no style, node will use its eclass name. Otherwise use the display attribute value.
**/
	TreeAdapter.prototype.applyStyle = function ( name, obj ) {


		var displayObj = new Object();

		if (this.treeStyleNames == null){
			displayObj.name = name;

			return displayObj;
		}
		for (var i = 0; i < this.treeStyleNames.length; i++) {

			//var style = this.treeStyleNames[i].split(":");
			var style = this.treeStyleNames[i];

			if (name == style[0]) {
				var str = (obj.GetMember(style[1]))?obj.eGet(style[1]):style[1];


				if (str != null && "object" != typeof(str) ) {
					//in IE, if the tag has ":", have exception. so using ":" html entities to substitute it.
					if(isIE()&&typeof(str)=="string"&&str.indexOf(":")!=-1){

						str = str.replace(/:/, "&#x003A;");

					}

					displayObj.name = (str == "")? " ":str;
					displayObj.propertyName = (obj.GetMember(style[1]))?style[1]:null;

					return displayObj;
				}

				break;
			}

		}

		displayObj.name =name;
		return displayObj;

	}
/**
* @method private TreeAdapter.prototype.GetChildrenRefs
*   This function is mainly used to define how to walk throught the child node under the parent node
* @param String someEClass
*	The eclass name of the parent node
* @return Array refArray
*	The array which define what kind of children node will be displayed and in which order
**/
	TreeAdapter.prototype.GetChildrenRefs = function(someEClass) {

		var refArray = new Array();

		if (this.treeStyleNames == null)
			return refArray;

		for (var i = 0; i < this.treeStyleNames.length; i++) {

			if (this.treeStyleNames[i][0] == someEClass) {

				refArray[refArray.length] = Trim(this.treeStyleNames[i][2]);

			}

		}

		return refArray;

	}

		this.isleaf = function(eclassName)
		{

			if (this.treeStyleNames == null){

					return false;
			}


			var style = this.treeStyleNames[this.treeStyleNames.length-2][0];


			if(eclassName == style)
				return true;
			else
				return false;

		}




/**
* @method private TreeAdapter.prototype.GetChildContent
*   This method takes the eobject and constructs its children nodes dom data structure so that each
*	node will know the parent-children relationship.
* @param ElementWrapper elementWrap
*	The parent element which has two properties, one is the parent DOM object, the other one is a
*	boolean which indicate this parent node is worked through or not
* @param EObject eobject
*	the parent object which will used to get its children
**/
	TreeAdapter.prototype.GetChildContent = function(elementWrap, eobject) {

		var innerObject = eobject;

		var childrenRefs = this.GetChildrenRefs(eobject.EClass.Name);

		elementWrap.emptyFlag = false; // may need to move this further on in the code: our "new" eobject may be empty...

		// how it looks:
		// <eobject.ClassName    xmi:id=eobject.ID   eobject.Members[i].Name=eobject.eGet(eobject.Members[i].Name)

		var spanedArr = new Array();
		var value;

		for(var i=0; i<childrenRefs.length;i++){

			var strName = childrenRefs[i];
			if (this.isMasked(strName))
					continue;
			if(!spanedArr[strName]){
				//in case the style string has duplicate style.
				spanedArr[strName] = strName;
				try{
					if(strName)
						value = innerObject.eGet(strName);
				}catch(e){

					return;
				}

					if(value!=null){
						//has childrens

							// Value is an object
							if (value != null && typeof(value) == "object") {
								if (value.length != null && "number" == typeof(value.length) ) {
									//objects array

									elementWrap.element.setAttribute("childrenLen", value.length);

									// this is an array; go through it
									for (var j = 0; j < value.length; ++j) {
										this.constructChildElement(elementWrap, value[j],strName);
									}
								} else {

									//single eobject
										this.constructChildElement(elementWrap, value,strName);
								}//if (value.length != null && "number" == typeof(value.length) ) {
							}//if (value != null && typeof(value) == "object") {





					}else
					{
						//data is in the page, but without children.
						//the reason I only set the length when i==0 is, if the node has two ref, and one ref
						//has child, the other ref has no child, so that the second child len will not overwrite
						//the first child lenght.
						if(i==0)
							elementWrap.element.setAttribute("childrenLen", 0);

					}//if data has children or not

	//			}//if data is in the page

			}//if(!spanedArr[strName])
		}//for(var i=0; i<childrenRefs.length;i++)
	}//TreeAdapter.prototype.GetChildContent

/**
* @method private TreeAdapter.prototype.constructChildElement
*   This is a sub method in getChildContent method to reduce the duplicate code
* @param ElementWrapper elementWrap
*	The parent element which has two properties, one is the parent DOM object, the other one is a
*	boolean which indicate this parent node is worked through or not
* @param EObject childObj
*	Child eobject
* @param String strName
*	The propertyName of the parent eobject which is used to get the children
**/

	TreeAdapter.prototype.constructChildElement = function (elementWrap, childObj,strName)
	{
			this.expandedNode[this.expandedNode.length] = childObj;
			//this.expandedNode[childObj.getSignature()] = childObj;

			var objclsname = childObj.EClass.Name;


			var displayObj = this.applyStyle(objclsname , childObj);
			var str = displayObj.name;
			var propertyName = displayObj.propertyName;

		//	alert("index " + j + " str " + str);
		
			var strtmp = "X" + str; // make sure that it is always a string
			
			var str1 = strtmp.replace(/\ /gi,"_");
		
		
			var childElement = document.createElement(str1+"1");
			//childElement.setAttribute("EObjectID", value[j].ID);
			childElement.displayName = str;


			var childRef = this.GetChildrenRefs(objclsname);
			
			
						//Adam:  check to see if this node has instances of children.
			//this information is used to determine whether it is an empty folder or nor.
			//in getJunctionImage in treecontrol.js, neither a + or - junction will be drawn
			//if it is found here that the node won't have any children
			var hasChild = false;
			
			for ( var i=0; i<childRef.length; i++ )
			{
				var tempMember = childObj.FindMember(childRef[i]);
				if (tempMember)
				{
					if(tempMember.Value)
					{
						//alert(childObj.ID + " seems to have child(ren) with value(s)");
						hasChild=true;
						//if we have found one real child instance this is enough
						break;
					}
				}
				if (i==childRef.length-1)
				{
					//alert(childObj.ID + " does not seem to have child(ren) with value(s)");
					hasChild=false;
				}
			}
			
			if(!(childRef.length==1&&childRef==""))
			{
				if (hasChild==false)
				{
					//Has no childen
					childElement.setAttribute("hasNoChildrenValues", true);
				}

				childElement.setAttribute("childrenLen",1);

			}

			//default to false => has >=1 child value
			if ( childElement.getAttribute("hasNoChildrenValues")==null )
				childElement.setAttribute("hasNoChildrenValues", false);
			
			
			
			/*if(!(childRef.length==1&&childRef==""))
			{

				childElement.setAttribute("childrenLen", 1);

			}*/

			var handler = this.nodeHandler[objclsname];

			if(handler!=null&&handler!='undefined')
				childElement.eventHandler = handler;

			childElement.eobject = childObj;
			childElement.propertyName = propertyName;

			//var imgpath = this.applyImage(value[j].EClass.Name);

			var imgpath = this.applyImage(childObj);
			if (null != imgpath) {
				childElement.setAttribute("treecloseimgfile", imgpath[0]);
				childElement.setAttribute("treeopenimgfile", imgpath[1]);
				if(imgpath[2])
					childElement.setAttribute("showsystemiconflag", imgpath[2]);
				if(imgpath[3])
					childElement.setAttribute("treecloseimgalt", imgpath[3]);
				if(imgpath[4])
					childElement.setAttribute("treeopenimgalt", imgpath[4]);


			}

			var childElementWrap = new ElementWrapper(childElement, false);
			//if(index<1)
			//	this.GetChildContent(childElementWrap, value[j], 1);

			if (!childElementWrap.emptyFlag) {

				elementWrap.element.appendChild(childElement);




			}


	}






/**
* @method private TreeAdapter.prototype.SetPropertyBinders
*   This method is used to bind the tree with the data model so that once the data mode is changed,
*	tree's content will be updated automatically. But right now those two rows are commented out, don't
*	know why. The other issue with this is, because of performace improvement, at the first time, only
*	the first level of nodes are rendered, so only those nodes are bound to data model, need to work on
* 	the tree control to bind the later rendered node.
**/
	TreeAdapter.prototype.SetPropertyBinders = function (index) {
		//alert("SetPropertyBinders");
//Qun comments, to improve performance we don't need to expand all child node and then use updatecontrol
//to collapse all the nodes from the second level.Since right now only the nodes which will be displayed
//need to be constructed.
		//this.treeControl.rootItem.expandAllChild(this.treeControl.rootItem.itemID);

		for (var i = index; i < this.treeControl.a_index.length; i++)
		{

			var treeitem = this.treeControl.a_index[i];
			if (treeitem.eobj)
			{

				var htmlElement = get_element('i_txt' + this.treeControl.itemID+'_'+treeitem.itemID);


				if (htmlElement)
				{
					 //alert(htmlElement.innerText);
					// attach property binder
					try {
						//alert("treeitem.propertName " + treeitem.propertName);

						var binding = new PropertyBinder(treeitem.eobj, treeitem.propertName, htmlElement, "innerHTML", "onchange");
						binding.DataBind();

					} catch (e) {

						return;
					}
				}
			}
		}
	}


	TreeAdapter.prototype.setStyleSelectorText = function (styleText) {

		this.treeStyles = styleText;

	}

	TreeAdapter.prototype.setIconSelectorText = function (iconText) {

		this.iconStyles = iconText;

	}

/**
* @method public TreeAdapter.prototype.setStyleMap
*   User can use this method to define how the tree is rendered and in which order
* @param String styleString
*	The user defined style
**/
	TreeAdapter.prototype.setStyleMap = function (styleString) {

		this.styleMapString = styleString;
	}
/**
* @method public TreeAdapter.prototype.addNodeIconMap
*   User can use this method to set the node's icon image path
* @param String iconString
*	The user defined style
**/
	TreeAdapter.prototype.addNodeIconMap = function (iconString) {

	//	var iconMapString = iconString.replace(/ /g, "");
		var iconMapString = iconString;
		if (iconMapString != "") {


			var ruleArray = iconMapString.split(",");
			if(this.iconFilePaths==null)
				this.iconFilePaths = new Array();

			this.iconFilePaths[this.iconFilePaths.length] = ruleArray;


		}



	}

/**
* @method public TreeAdapter.prototype.addNodeEventHandler
*   User can use this method to add the event handler to the tree node.
* @param String handlerString
*	The user defined event handler.
**/
	TreeAdapter.prototype.addNodeEventHandler = function (handlerString) {

		if (handlerString != "") {

			var ruleArray = handlerString.split(":");
			if(this.nodeHandler==null)
				this.nodeHandler = new Array();

				if(this.nodeHandler[ruleArray[0]]==null||this.nodeHandler[ruleArray[0]]=='undefined')
					this.nodeHandler[ruleArray[0]] = ruleArray[1];
				else
					this.nodeHandler[ruleArray[0]] += ";" + ruleArray[1];


		}



	}



/**
* @method public TreeAdapter.prototype.setIconRoot
*   User can use this method to define the root icon image path
* @param String iconRootString
*	The user defined style
**/
	TreeAdapter.prototype.setIconRoot = function(iconRootString) {

		var rootString = iconRootString.replace(/ /g, "");
		rootString = rootString.split(":");

		// need to put this functionality in... to do.
		if (rootString.length > 0) {
			this.treeControl.itemClosedImage = rootString[0];

			if (rootString.length > 1 ) {
				this.treeControl.itemOpenedImage = this.treeControl.UrlPrefix +rootString[1];
			} else {
				this.treeControl.itemOpenedImage = this.treeControl.UrlPrefix +rootString[0];
			}
		}

	}



/**
* @method public TreeAdapter.prototype.setSystemIconStyle
*   User can use this method to set the system icon, for example, in file structure case,
*	we use "+" image to indicate the directory has more files, etc..
* @param String systemIconStyleString
*	The user defined style
**/
	TreeAdapter.prototype.setSystemIconStyle = function(	systemIconStyleString) {

		var styleString = systemIconStyleString.replace(/ /g, "");

		var iconArray = styleString.split("|");
		var len = iconArray.length-1;

		for(var i=1; i<len; i++)
		{

			//breff 
			//When parsing the string icon_key=icon_path;icon_key=icon_path; splitting at "=" was causing problems because sometimes there was an equals in the icon_path and therefore 
			//the icon key returned would be invalid. Fix was to get icon_key as a substring from start to first occurance of "=" 
			//old split: var temp = iconArray[i].split("=");
			var index= iconArray[i].indexOf("=");
			var temp= new Array();
			temp[0] = iconArray[i].substring(0,index);
			temp[1] = iconArray[i].substring(index+1,iconArray[i].length);
			//for(c=0;c<temp.length;c++)
			//alert("temp "+c+" is "+temp[c]);
			//endBreff
			if(temp.length>1)
				this.treeControl.treeIcons[temp[0]] = temp[1];	

		}

	}
/**
* @method private TreeAdapter.prototype.initStyles
*   This method is used to construct all the style arrays from what the user passed in.
**/
	TreeAdapter.prototype.initStyles = function() {

		if ( (null == this.treeStyles) && (null == this.styleMapString) )
			return;

		var styleString = null;
		var iconString = null;

		if (null != this.treeStyles) {

			try {
				var styleString = eval(this.treeStyles);
//				alert(styleString);
//				styleString = styleString.replace(/ /g, "");
			} catch (e) {
				this.treeStyles = null;
				return;
			}

		} else {
			styleString = this.styleMapString;
//			styleString = styleString.replace(/ /g, "");
		}
//		alert(styleString);
		if (styleString != "") {

			this.treeStyleNames = styleString.split(";");
//			alert(this.treeStyleNames);
			// for every rule:
			for (var i = 0; i < this.treeStyleNames.length; i++) {

				var ruleString = this.treeStyleNames[i];

				var ruleArray = ruleString.split(":");

				this.treeStyleNames[i] = ruleArray;

//				alert("-> " + i + ": " + this.treeStyleNames[i]);

			}

		}

//		alert(styleString);
//		alert(this.treeStyleNames);

		//deal with icon string here:this is the icon map which is assign to interface instead of call addnodeiconmap function.

		if (null != this.iconStyles) {

			try {
				iconString = eval(this.iconStyles);
//				alert(styleString);
				iconString = iconString.replace(/ /g, "");
			} catch (e) {
				this.iconStyles = null;
				return;
			}



			if (iconString != "") {

				this.iconFilePaths = iconString.split(";");

				// for every rule:
				for (var i = 0; i < this.iconFilePaths.length; i++) {

					var ruleString = this.iconFilePaths[i];

					var ruleArray = ruleString.split(":");

					this.iconFilePaths[i] = ruleArray;

					//alert("-> " + i + ": " + this.iconFilePaths[i]);

				}

			}
		}
	}
/**
* @method public TreeAdapter.prototype.setRootByID
*   This method is used to set the tree root using the xmiID
* @param String IDString
*	The xmiID of the eobject.
**/

	TreeAdapter.prototype.setRootByID = function(IDString ) {

		if ((this.eobjRoot == undefined) || (this.eobjRoot == null) ) {

			//alert("no eobjRoot set for tree");

			return;

		}
		this.eobjRoot = findEObjectByXMIID(this.eobjRoot, IDString);
		//this.expandedNode[this.expandedNode.length] = this.eobjRoot;
		this.expandedNode[this.eobjRoot.getSignature()] = this.eobjRoot;


	}





/**
* @method public TreeAdapter.prototype.bind
*   This method is used to initialize/reset the array used to keep track of
*	objects examined also bind them with data model
**/
	TreeAdapter.prototype.bind = function () {
		index=0;
		// Starting with the eobjRoot, isolate the first EObject instance
		var eobjInstance = this.eobjRoot;
		if(eobjInstance==null)
			return;

		// Get styling information priot to initial GetChildContent call:

		this.initStyles();
//		var treeParentElement = this.treeControl.xmlNode.ownerDocument.createElement( this.applyStyle(eobjInstance.EClass.Name,eobjInstance ) );


		var displayObj = this.applyStyle(eobjInstance.EClass.Name,eobjInstance ) ;
		var str = displayObj.name;
		var propertyName =displayObj.propertyName;

		var treeParentElement = document.createElement(str+"1"); //the reason for 1 is in case the node label is same as html tag.
		treeParentElement.displayName =  str;

		treeParentElement.setAttribute("EObjectID", eobjInstance.ID);
		treeParentElement.eobject = this.eobjRoot;
		treeParentElement.propertyName = propertyName;
//add this for root node to fix defect 267427(aix family)  so that root node icon will not be treated especially. 10/27

		var imgpath = this.applyImage(this.eobjRoot);
		if (null != imgpath) {
			treeParentElement.setAttribute("treecloseimgfile", imgpath[0]);
			treeParentElement.setAttribute("treeopenimgfile", imgpath[1]);
			if(imgpath[2])
				treeParentElement.setAttribute("showsystemiconflag", imgpath[2]);
			if(imgpath[3])
				treeParentElement.setAttribute("treecloseimgalt", imgpath[3]);
			if(imgpath[4])
				treeParentElement.setAttribute("treeopenimgalt", imgpath[4]);
		}
		var event = this.nodeHandler[eobjInstance.EClass.Name];

		if(event)
			treeParentElement.eventHandler = event;

		var elementWrap = new ElementWrapper(treeParentElement, true);


		this.GetChildContent(elementWrap, eobjInstance, 0);
	//	alert(GetXMLString(1,treeParentElement));


		// no real need to check the flag here, as we want to put
		//  at least one element at the root of the root of the tree, even if it is empty
//gt		this.treeControl.xmlNode.appendChild(treeParentElement);

		var newTreeItemChild = new TreeItem(this.treeControl, treeParentElement, 0);
	//	nodeLevel++;

		this.treeControl.writeTreeUI1();

		this.SetPropertyBinders(0);

	}

	this.unbind = function() {
		this.treeControl.removeItem(this.treeControl.rootItem.itemID);
	}


function ElementWrapper(element, flag) {
	this.element = element;
	this.emptyFlag = flag;
}
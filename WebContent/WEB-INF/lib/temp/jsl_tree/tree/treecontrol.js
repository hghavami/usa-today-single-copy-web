//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003.  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//=================================================================

/**
 * @class public Tree
 * @constructor Tree
 * This class is tree control which will render the tree based on the
 *	data set which is getting from the tree adapter.
 * @param Object parent
 *	The object which is point to place where the datagrid will be put
 *	in the html page
 * @param String UrlPrefix
 *	The path relative to "jsl/datagrid/icons/" of the datagrid images
 * @param String xmldoc
 *	The xml file which has all the data. Since now we change to use
 *	emitter, this parameter will not be used.
 *
**/

function Tree (parent, UrlPrefix, xmldoc,type)
{
 //   this.IconLib = new TreeIconLibrary(UrlPrefix);

    this.UrlPrefix = UrlPrefix;
	this.treeIcons = new Array();
	this.getDefaultSystemIcon(UrlPrefix);

	var xmlDoc;
    Log.debug("treecontraol.js.Tree()", "before LoadXMLString ") ;
	if (xmldoc==null)
    {

		if(type=="client")
			xmlDoc	= LoadXMLString("<startNode><root></root></startNode>");
		else
			xmlDoc	= LoadXMLString("<root />");


       //xmlDoc	= LoadXMLString("<?xml version=\"1.0\" encoding=\"utf-8\" ?><root />");
       Log.debug("treecontraol.js.Tree()", "after LoadXMLString ") ;
    }else
		xmlDoc = LoadXML(xmldoc);

	if (null == xmlDoc)
		return this;

	// Properties
	this.rootItem = this;

	this.xmlNode = xmlDoc.documentElement;
	this.itemName = this.xmlNode.nodeName;
	this.crumtree = false;

	this.parentHTMLElement = parent;
	this.a_index = [];
	this.checkedItemArray = new Array();
	this.checkedEObjectArray = new Array(); //keep all the selected eobjects for event object.

	this.childItems = [];
	this.eventArray = new Array;

	var nIndex = 0;
	for (var i = 0; i < xmlDoc.documentElement.childNodes.length; i++)
	{
		var node = xmlDoc.documentElement.childNodes[i];
		if (node.nodeType != 1)
			continue;

		new TreeItem(this, node, nIndex);
		nIndex++;
	}

	this.itemID = odcTrees.length;
	odcTrees[this.itemID] = this;

	this.styleIcons = new Array();

}



	Tree.prototype.Adapter = null;
	Tree.prototype.parentItem = null;
	Tree.prototype.selectedItem = null;

	Tree.prototype.depth = -1;

	Tree.prototype.enableSelect = false;
	Tree.prototype.rootVisibleFlag = true;
	Tree.prototype.iconLeafArray = null;
	Tree.prototype.height = null;
	Tree.prototype.width = null;
	Tree.prototype.styleClass = "";	//so two odcTrees can have different style sheet.
	Tree.prototype.alength = 0; //the tree's node length;

	Tree.prototype.type = "Tree";
	Tree.prototype.pageId = "tree1";//default pageId which will be changed by ODCRegistry when do addElementInfo


	// Methods

/**
* @method public Tree.prototype.getType
*   This function is return the type of the control
* @return String
*	The type of the control, "Tree".
**/
	Tree.prototype.getType = function(){
		return this.type;
	}


/**
* @method public Tree.prototype.getId
*   This function return the id of the control
* @return String
* 	return the pageID of this control.
**/
	Tree.prototype.getId = function(){
		return this.pageId;
	}


/**
* @method public Tree.prototype.setstyleClass
*   This function is used to set the style sheet prefix for this tree
* @param String prefix
*	the prefix which is added before the style sheet name. Like "tree1",
*	so the css name will be "tree1_labelNormalStyle".
**/
 Tree.prototype.setStyleClass = function(prefix)
 {
	 this.styleClass = prefix + "_";
	 this.parentHTMLElement.className = this.styleClass+"classForTreeSysIcons";
	 var icons = new iconLibrary(this.parentHTMLElement,"list-style-image");
	 for(var key in icons){
		 this.treeIcons[key] = icons[key];
	 }
 }



	/**
	* @method public Tree.prototype.getDefaultSystemIcon
	*   This function is used to get the default icons path
	* @param String UrlPrefix
	*	the prefix which is added before the style sheet name.
	*	Like "tree1", so the css name will be "tree1_labelNormalStyle".
	**/
	Tree.prototype.getDefaultSystemIcon = function(UrlPrefix)
	{
		this.treeIcons["minustop"]   = UrlPrefix+'jsl/tree/icons/minustop.gif';       // junctioin for first opended node
		this.treeIcons["minus"]      = UrlPrefix+'jsl/tree/icons/minus.gif';       // junctioin for last opended node
		this.treeIcons["minusbottom"]= UrlPrefix+'jsl/tree/icons/minusbottom.gif'; // junction for opened node

		this.treeIcons["plustop"]    = UrlPrefix+'jsl/tree/icons/plustop.gif';        //  junctioin for first closed node
		this.treeIcons["plus"]      = UrlPrefix+'jsl/tree/icons/plus.gif';        //  junctioin for last closed node
		this.treeIcons["plusbottom"] = UrlPrefix+'jsl/tree/icons/plusbottom.gif';  // junction for closed node

		this.treeIcons["jointtop"]   = UrlPrefix+'jsl/tree/icons/jointop.gif';        // junction for first leaf
		this.treeIcons["joint"]        = UrlPrefix+'jsl/tree/icons/join.gif';        // junction for last leaf
		this.treeIcons["jointbottom"]  = UrlPrefix+'jsl/tree/icons/joinbottom.gif';  // junction for leaf
		this.treeIcons["nodeopen"]    = UrlPrefix+'jsl/tree/icons/open_folder.gif';       // internal node node, open
		this.treeIcons["nodeclose"]  = UrlPrefix+'jsl/tree/icons/folder.gif'    ;       // internal node, closed
		this.treeIcons["leaf"]    = UrlPrefix+'jsl/tree/icons/page.gif'      ;       // leaf node node

		this.treeIcons["baseopen"]    = UrlPrefix+'jsl/tree/icons/root.gif';        // Root node, open
		//this.treeIcons["baseclose"]  = UrlPrefix+'jsl/tree/icons/base.gif';        // Root node, closed
		this.treeIcons["empty"]       = UrlPrefix+'jsl/tree/icons/empty.gif';       // junction for last leaf
		this.treeIcons["line"]      = UrlPrefix+'jsl/tree/icons/line.gif';        // junction for leaf
 }


/**
* @method public Tree.prototype.addEvent
*   This function is used to add event handler to the control array so
*	that all the event can be queued
* @param String action
*	The action which the datagrid control has event handler associated,
*	like "OnHighlight", "OnSelect" etc.
* @param String handler
*   The name of the function which wrap all the processings.
* @param String eclassType
*	to have the same API cross all the controls for select&active,
*	select&set event emitter, need to have this parameter to set the
*	node level handler
*
**/
	Tree.prototype.addHandler = function(action, handler,eclassType)
	{
		if(eclassType!=null&&eclassType!='undefined'&&eclassType!="")
		{
			//to have the same API cross all the controls for select&active, select&set event emitter, do this extra step.
			var str = eclassType + ":" + action + ","+handler;
			this.Adapter.addNodeEventHandler(str);
		}else{
			this.eventArray[this.eventArray.length] = new Array(action, handler);
		}


	}



/**
* @method public Tree.prototype.reSize
*   This function is used to resize tree when window is resized.
**/
	Tree.prototype.reSize = function()
	{

		var sizeArray = getWindowSize();

		this.width = (this.widthpercentage>1)?this.width:sizeArray[0]*this.widthpercentage;
		this.height =(this.heightpercentage>1)?this.height:sizeArray[1]*this.heightpercentage;

		var rootDiv = document.getElementById("i_div" + this.itemID + "_rootdiv");
		rootDiv.style.width = this.width;
		rootDiv.style.height = this.height;


	}

/**
* @method public Tree.prototype.setHeight
*   This function is used to set the tree's height.
* @param string height
*	The height of the tree
**/

	Tree.prototype.setHeight = function(height)
	{
		if(height.indexOf("%")!=-1)
		{
			this.heightpercentage = parseFloat(height)/100;

			this.height = getWindowSize()[1]*this.heightpercentage;

		}else{

			this.height = height;
		}


	}

/**
* @method public Tree.prototype.setWidth
*   This function is used to set the tree's width.
* @param string width
*	The width of the tree
**/

	Tree.prototype.setWidth=function(width)
	{


		if(width.indexOf("%")!=-1)
		{
			this.widthpercentage = parseFloat(width)/100;
			this.width = getWindowSize()[0]*this.widthpercentage;

		}else{

			this.width = width
		}

	}





/**
 * @method private Tree.prototype.writeTreeUI1
 * 	This code creates the UI of the tree. As it writes directly to the document object,
 *	it only runs once when the tree is initially constructed.
**/

	Tree.prototype.writeTreeUI1 = function()
	{

		if (this.parentHTMLElement==null)
		{

			return; // throw error
		}


		this.parentHTMLElement.innerHTML ="";
		strout = '<div style="border-width:0px"  id="i_div' + this.itemID + '_rootdiv"';
		var chgb = false; //just make sure if only width or height is specified, code still work.
		if(this.width!=null&&this.width>0){
			strout += ' STYLE="overflow:auto; width:'+this.width;
			chgb = true;
		}
		if(this.height!=null&&this.height>0){
			if(chgb)
				strout += '; height:'+this.height;
			else{
				strout += ' STYLE="overflow:auto; height:'+this.height;
				chgb = true;
			}
		}
		if(chgb)	strout +=';"';
		strout +='>';


		for (var i = 0; i < this.childItems.length; i++)
		{
			strout+=this.childItems[i].init();
			this.childItems[i].open();
		}
		strout+= '</div>';


		this.parentHTMLElement.innerHTML=strout;
	}


/**
 * @method public Tree.prototype.show
 * 	This method is used to expand the root node to one level deep
 *	(because of performance fix) at the first time right now. Later
 *	we will fix the function openfoldr(level) to allow user open
 *	any level of node the first time the tree is displayed.
**/
	Tree.prototype.show = function()
	{
		this.updateControl();

	}


/**
 * @method private Tree.prototype.updateControl
 * 	This method is used to expand the root node to one level deep
 *	(because of performance fix) at the first time right now. Later
 *	we will fix the function openfoldr(level) to allow user open
 *	any level of node the first time the tree is displayed.
**/
	Tree.prototype.updateControl = function()
	{
		if(this.a_index==null||this.a_index.length==0){
			alert(action_obfcontrolrender_errormsg);
			return;
		}

		this.rootItem.expand(0);

	}

/**
 * @method private Tree.prototype.openfolder
 * 	This method is used to expand the root node to any level deep based on the user's setting.
 *  Now this method doesn't work because of fix in performance issue.
 * @param int itemID
 *	The deep level the user want to open.
**/

	Tree.prototype.openfolder = function (itemID)
	{
		if(isNaN(itemID) || (itemID> this.a_index.length))
			return;

		if(itemID==-1) this.rootItem.expandAllInternal(this.rootItem.itemID);
		if(itemID==0) this.a_index[0].open(this.a_index[0].isOpened);

		for (var j = 1; j < itemID; j++)
		{
			var itemObj = this.a_index[j];
			itemObj.open(itemObj.isOpened)


		}
	}
/**
 * @method private initTree
 * 	This method is not used right now
 * @param String url
 *	The url of the data model xml file
**/

	Tree.prototype.initTree = function(url)
	{
		var htmlElement = get_element('i_div' + this.rootItem.itemID + '_' + this.rootItem.itemID);
		if (!htmlElement)
		{
			htmlElement = get_element('i_txt' + this.rootItem.itemID + '_' + this.rootItem.itemID);
			if (!htmlElement)
				return;
		}

		var xmlDoc = LoadXML(url);
		if (null == xmlDoc)
			return;

		this.xmlNode = xmlDoc.documentElement;
		this.itemName = this.xmlNode.nodeName;

		this.a_index = null;
		this.childItems = null;
		this.a_index = new Array;
		this.childItems = new Array;


		var nIndex = 0;
		for (i = 0; i < this.xmlNode.childNodes.length; i++)
		{
			var node = this.xmlNode.childNodes[i];
			if (node.nodeType != 1)
				continue;

			new TreeItem(this, node, nIndex);
			nIndex++;
		}

		htmlElement.parentNode.innerHTML = this.childItems[0].init();
		this.childItems[0].open();
	};

	Tree.prototype.loadLeaf = function(itemID, url)
	{
		var treeItem = this.a_index[itemID];
		if (treeItem == null)
			return;

		var htmlElement = get_element('i_div' + this.rootItem.itemID + '_' + itemID);
		if (!htmlElement)
		{
			htmlElement = get_element('i_txt' + this.rootItem.itemID + '_' + itemID);
			if (!htmlElement)
				return;
		}

		var xmlDoc = LoadXML(url);
		if (null == xmlDoc)
			return;

		var xmlNode = xmlDoc.documentElement;
		var nIndex = 0;
		for (i = 0; i < xmlNode.childNodes.length; i++)
		{
			var node = xmlNode.childNodes[i];
			if (node.nodeType != 1)
				continue;

			new TreeItem(treeItem, node, nIndex);
			nIndex++;
		}

		htmlElement.parentNode.innerHTML = treeItem.init();
		treeItem.open();
	};
/**
 * @method private addItem
 * 	This method is not used right now since the current tree is not editable.
**/
	Tree.prototype.addItem = function(itemID, displayText, value)
	{
		if (displayText == null || displayText == "")
			return;
		var futureParentItemObj = this.a_index[itemID];
		if (futureParentItemObj == null)
			return;

		var wasOpened = futureParentItemObj.isOpened;

		var htmlElement = get_element('i_div' + this.rootItem.itemID + '_' + itemID);
		if (!htmlElement)
		{
			htmlElement = get_element('i_txt' + this.rootItem.itemID + '_' + itemID);
			if (!htmlElement)
				return;
		}

		var htmlRootElement = get_element('i_div' + this.rootItem.itemID + '_' + this.rootItem.itemID);
		if (!htmlRootElement)
		{
			htmlRootElement = get_element('i_txt' + this.rootItem.itemID + '_' + this.rootItem.itemID);
			if (!htmlRootElement)
				return;
		}

		var newXmlNode = futureParentItemObj.xmlNode.ownerDocument.createElement(displayText);
		var textValueNode = futureParentItemObj.xmlNode.ownerDocument.createTextNode(value);
		newXmlNode.appendChild(textValueNode);

		futureParentItemObj.xmlNode.appendChild(newXmlNode);

		var newItem = new TreeItem(futureParentItemObj, newXmlNode, futureParentItemObj.childItems.length);

		if (this.depth <= 1)
		{
			htmlRootElement.parentNode.innerHTML = futureParentItemObj.rootItem.childItems[0].init();
			futureParentItemObj.rootItem.childItems[0].open();
		}
		else
			htmlElement.parentNode.innerHTML = futureParentItemObj.init();

		//htmlRootElement.parentNode.innerHTML = futureParentItemObj.rootItem.childItems[0].init();

		//futureParentItemObj.rootItem.childItems[0].open();
		futureParentItemObj.open(!wasOpened);

		return newItem;
	}


/**
 * @method private addItem
 * @param Object item
 *	 Parent node which the new node will be attached on.
 * @param String displayText
 *	 The text value of the new added node.
**/
	Tree.prototype.addNode = function(item, displayText)
	{
		var itemID = item.itemID;

		if (displayText == null || displayText == "")
			return;
		var futureParentItemObj = this.a_index[itemID];
		if (futureParentItemObj == null)
			return;

		var wasOpened = futureParentItemObj.isOpened;

		var newXmlNode = futureParentItemObj.xmlNode.ownerDocument.createElement(displayText);

		futureParentItemObj.xmlNode.appendChild(newXmlNode);

		var newItem = new TreeItem(futureParentItemObj, newXmlNode, futureParentItemObj.childItems.length);


		return newItem;
	}
/**
 * @method private updateTreeItem
 * 	This method is not used right now since the current tree is not editable.
**/
	function updateTreeItem(itemObj, editElem, htmlElement, isUpdate)
	{
		newXMLElem = itemObj.xmlNode.ownerDocument.createElement(editElem.value);
		newXMLTextNode = itemObj.xmlNode.ownerDocument.createTextNode(getXMLNodeValue(itemObj.xmlNode));
		newXMLElem.appendChild(newXMLTextNode);
		var retObj = itemObj.xmlNode.parentNode.replaceChild(newXMLElem, itemObj.xmlNode);
		var strOldName = itemObj.itemName;
		itemObj.xmlNode = newXMLElem;
		itemObj.parentItem.xmlNode = itemObj.xmlNode.parentNode;
		itemObj.itemName = editElem.value;
		itemObj.rootItem.a_index[itemObj.itemID] = itemObj;

		strInnerHTML = htmlElement.innerHTML;
		strInnerHTML = strInnerHTML.substring(0, strInnerHTML.length - strOldName.length) + editElem.value;
		htmlElement.innerHTML = strInnerHTML;
		editElem.parentNode.replaceChild(htmlElement, editElem);
	}
/**
 * @method private editItem
 * 	This method is not used right now since the current tree is not editable.
**/
	Tree.prototype.editItem = function(itemID)
	{
		var itemObj = this.a_index[itemID];
		if (itemObj == null)
			return;

		var htmlElement = get_element('i_div' + this.rootItem.itemID + '_' + itemID);
		if (!htmlElement)
		{
			htmlElement = get_element('i_txt' + this.rootItem.itemID + '_' + itemID);
			if (!htmlElement)
				return;
		}
		var strEditControlContent = "<INPUT id='txtItem' type='text' size='9' name='txtItem'>";
		var editElem = null, textNode = null;
		if (navigator.appName == "Netscape")
		{
			editElem = htmlElement.ownerDocument.createElement("input");
			textNode = htmlElement.ownerDocument.createTextNode(itemObj.itemName);
			editElem.setAttribute("type", "text");
			editElem.setAttribute("size", "9");
			editElem.style.height = "17";
			editElem.style.fontSize = 10;
			//editElem.appendChild(textNode);
		}
		else
		{
			editElem = htmlElement.ownerDocument.createElement(strEditControlContent);
			editElem.style.height = "17";
			editElem.style.fontSize = 10;
		}

		htmlElement = htmlElement.parentNode.replaceChild(editElem, htmlElement);

		editElem.value = itemObj.itemName;
		editElem.focus();
		editElem.select();

		if (this.onstartedit != null)
			this.onstartedit(itemObj, editElem);

		editElem.onblur = function onBlurEditBox()
		{
			if (this.onfinishedit != null)
			{
				var isUpdate = false;
				isUpdate = this.onfinishedit(itemObj, editElem);
				this.updateTreeItem(itemObj, editElem, htmlElement, isUpdate);
			}
			else
				this.updateTreeItem(itemObj, editElem, htmlElement, true);
		}
	}
/**
 * @method private removeItem
 * 	This method is not used right now since the current tree is not editable.
**/
	Tree.prototype.removeItem = function(itemID)
	{
		var itemObj = this.a_index[itemID];
		if (itemObj == null)
			return;

		var isLeaf = false;
		var htmlNodeName = 'i_div' + this.rootItem.itemID + '_' + itemID;
		var htmlElement = get_element(htmlNodeName);
		if (!htmlElement)
		{
			htmlNodeName = 'i_txt' + this.rootItem.itemID + '_' + itemID;
			htmlElement = get_element(htmlNodeName);
			if (!htmlElement)
				return;
			isLeaf = true;
		}

		if (navigator.appName == "Netscape")
		{
			var parent = htmlElement.parentNode;
			if (isLeaf)
			{
				htmlElement.parentNode.removeChild(htmlElement);
				parent.parentNode.removeChild(parent);
			}
			else
			{
				htmlElement.parentNode.removeChild(htmlElement);
				htmlNodeName = 'i_txt' + this.rootItem.itemID + '_' + itemID;
				var htmlElement = get_element(htmlNodeName);
				parent = htmlElement.parentNode;
				htmlElement.parentNode.removeChild(htmlElement);
				parent.parentNode.removeChild(parent);
			}
		}
		else
		{
			if (isLeaf)
				htmlElement.parentElement.removeNode(true);
			else
			{
				htmlElement.removeNode(true);
				htmlNodeName = 'i_txt' + this.rootItem.itemID + '_' + itemID;
				var htmlElement = get_element(htmlNodeName);
				htmlElement.parentElement.removeNode(true);
			}
		}

		itemObj.xmlNode.parentNode.removeChild(itemObj.xmlNode);
		var indexInParent = 0;
		// Find index of the removable item in the parent childs array
		for (i = 0; i < itemObj.parentItem.childItems.length; i++)
		{
			if (itemObj.parentItem.childItems[i] == itemObj)
			{
				indexInParent = i;
				break;
			}
		}
		itemObj.parentItem.childItems.splice(indexInParent, 1);
		itemObj.childItems.splice(0, itemObj.childItems.length);
		//this.a_index.splice(itemID, 1);
		itemObj = null;
	};
/**
 * @method private Tree.prototype.select
 * 	This method is used to highlight the node which is be selected
 * @param int itemID
 *	The position of the node in the whole big array which includes all the rendered
 *	nodes.
**/
	Tree.prototype.select = function (itemID)
	{

		return this.a_index[itemID].select();

	};


/**
 * @method private Tree.prototype.check
 * 	This method is used to call onselect event if onselect is defined when the node's
 *  checkbox is checked or unchecked.
 * @param int itemID
 *	The position of the node in the whole big array which includes all the rendered
 *	nodes.
**/

	Tree.prototype.check = function(selectObj, itemID)
	{

		if(selectObj.checked)
		{

			return this.a_index[itemID].check('ONSELECT');

		}else
		{
			return this.a_index[itemID].check('ONUNSELECT');



		}

	}

/**
 * @method private Tree.prototype.toggle
 * 	This method is used to open or close the node depending on if the node is open or close.
 *  To improve performance, if the node is never be opened before, the code will construct its
 *	children's nodes and saved to the big array. So later on, when user clicks this node again,
 *  the children's node can be got directly from that big array.
 * @param int itemID
 *	The position of the node in the whole big array which includes all the rendered
 *	nodes.
**/
	Tree.prototype.toggle = function(itemID)
	{


		if(itemID == 0)	return;


			//Call the onNodeExpand and onExpand event no matter if it has child or not. It is developer's responsibility to decide to call
			//server side data fetch or not.

		var itemObj = this.a_index[itemID];

		if(!itemObj.isOpened){
			//expand event
			if(!itemObj.acitveNodeEvent("onExpand"))
				return false;
			if(!itemObj.activeTreeEvent("onNodeExpand"))
				return false;


		}else{
			//collapse event
			if(!itemObj.acitveNodeEvent("onCollapse"))
				return false;
			if(!itemObj.activeTreeEvent("onNodeCollapse"))
				return false;



		}

		this.getChildrenItems(itemObj);
		return false;

	};
/**
 * @method private Tree.prototype.contextmenu
 * 	This method is a call back method which right now is not used
 * @param int itemID
 *	The position of the node in the whole big array which includes all the rendered
 *	nodes.
**/
	Tree.prototype.contextmenu = function(itemID)
	{
		var itemObj = this.a_index[itemID];
		if (this.oncontextmenu != null)
			this.oncontextmenu(itemObj);
	};
/**
 * @method private Tree.prototype.expand
 * 	This method is used to open the specified node
 * @param int itemID
 *	The position of the node in the whole big array which includes all the rendered
 *	nodes.
**/
	Tree.prototype.expand = function(itemID)
	{


		var itemObj = this.a_index[itemID];
		if(!itemObj.isOpened)
			itemObj.open(itemObj.isOpened);

	};

/**
 * @method private Tree.prototype.getChildrenItems
 * 	This method is used to get the children nodes of the specified node
 * @param obj itemObj
 *	The parent object
 * @param boolean expandFlag
 *	indicate this node is expand or collapse
**/

	Tree.prototype.getChildrenItems = function(itemObj,expandFlag)
	{
		if(itemObj.childItems!=null&&itemObj.childItems.length>0&&itemObj.childItems[itemObj.childItems.length-1]!=null){
			//this node is already rendered.

			if(expandFlag=="expand"&&itemObj.isOpened){
				return;
			}else if(expandFlag=="collapse"&&!itemObj.isOpened){
				return;
			}else{
				if(this.crumtree){
					//close other open node
					for(var i=1; i<this.a_index.length; i++)
					{
						var tempObj = this.a_index[i];

						 if(tempObj.isOpened)
						 {
							 tempObj.open(true);
							 break;

						 }

					}
				}

				itemObj.open(itemObj.isOpened);
			}
		}else{

			var nIndex =0;
			var xmlNode = itemObj.xmlNode;
			var eobj = itemObj.eobj;
			var elementWrap = new ElementWrapper(xmlNode, true);
			this.Adapter.GetChildContent(elementWrap, eobj, 1);

			var preNodelen = this.alength;
			for (var i = 0; i < xmlNode.childNodes.length; i++)
			{
				var node = xmlNode.childNodes[i];
				if (node.nodeType != 1)
					continue;
				new TreeItem(itemObj, node, nIndex);
				nIndex++;
			}
			if(this.crumtree){
					for(var i=1; i<this.a_index.length; i++)
					{
						var tempObj = this.a_index[i];

						 if(tempObj.isOpened)
						 {
							 tempObj.open(true);
							 break;
						 }

					}
			}

			itemObj.open(itemObj.isOpened);
			this.Adapter.SetPropertyBinders(preNodelen);

			if(this.enableSelect){
				if(this.checkedItemArray[itemObj.eobj.ID]==null){
					itemObj.check('ONUNSELECT')
				}else{
					itemObj.check('ONSELECT');
				}
			}

		}


	}

/**
 * @method public Tree.prototype.expandAll
 * 	This method is used to open all the children nodes of the specified node
 * @param Object eobject
 *	The expanded node's eobject
**/
	Tree.prototype.expandAll = function (eobject)
	{
		if(this.crumtree)
			return;
		for(var i=0; i<this.a_index.length; i++)
		{
			var obj = this.a_index[i].eobj;

			if(eobject.ID==obj.ID){
				var itemObj = this.a_index[i];
				//expand event
				if(!itemObj.acitveNodeEvent("onExpand","-1"))
					return false;
				if(!itemObj.activeTreeEvent("onNodeExpand","-1"))
					return false;

				this.expandAllInternal(i);
				break;
			}
		}
	}
/**
 * @method private Tree.prototype.expandAllInternal
 * 	This method is used to open all the children nodes of the specified node
 * @param int itemID
 *	The position of the node in the whole big array which includes all the rendered
 *	nodes.
**/
	Tree.prototype.expandAllInternal = function (itemID)
	{
		var itemObj = this.a_index[itemID];

		this.getChildrenItems(itemObj,"expand");

		for (var j = 0; j < itemObj.childItems.length; j++)
		{
			this.expandAllInternal(itemObj.childItems[j].itemID);
		}
	}





/**
 * @method public Tree.prototype.collapseAll
 * 	This method is used to collapse all the children nodes of the
 *	specified node
 * @param Object eobject
 *	The expanded node's eobject
**/
	Tree.prototype.collapseAll = function (eobject)
	{
		if(this.crumtree)
			return;

		for(var i=0; i<this.a_index.length; i++)
		{
			var obj = this.a_index[i].eobj;

			if(eobject.ID==obj.ID){
				var itemObj = this.a_index[i];
				//collapse event
				if(!itemObj.acitveNodeEvent("onCollapse", "-1"))
					return false;
				if(!itemObj.activeTreeEvent("onNodeCollapse", "-1"))
					return false;

				this.collapseAllInternal(i);
				break;
			}
		}
	}
/**
 * @method private Tree.prototype.collapseAllInternal
 * 	This method is used to open all the children nodes of the specified node
 * @param int itemID
 *	The position of the node in the whole big array which includes all the rendered
 *	nodes.
**/
	Tree.prototype.collapseAllInternal = function (itemID)
	{
		var itemObj = this.a_index[itemID];

		this.getChildrenItems(itemObj,"collapse");

		for (var j = 0; j < itemObj.childItems.length; j++)
		{
			this.collapseAllInternal(itemObj.childItems[j].itemID);
		}
	}




/**
 * @method private Tree.prototype.mout
 * 	This method is used to update the status in the browser when user's mouse out of that node
 * @param int itemID
 *	The position of the node in the whole big array which includes all the rendered
 *	nodes.
**/
	Tree.prototype.mout  = function (itemID)
	{
		if(this.a_index[itemID])
			this.a_index[itemID].upstatus(true,"mouseout")
	};

/**
 * @method private Tree.prototype.mover
 * 	This method is used to update the status in the browser when user's mouse over that node
 * @param int itemID
 *	The position of the node in the whole big array which includes all the rendered
 *	nodes.
**/
	Tree.prototype.mover = function (itemID)
	{
		if(this.a_index[itemID])
			this.a_index[itemID].upstatus(false,"mouseover")
	};



/**
 * @method public Tree.prototype.restoreUIState
 * 	This function is used to restore the UI state of the tree based
 *	on a state string.
 * @param String stateString
 *  A comma delimited signature string to represent which nodes are
 *	open, such as 1,5,8,9:10:1,2,3,4, indicating that nodes with object
 *	signatures: 1, 5, 8, 9 are expaned. 10 is highlighted.1,2,3,4 are selected.
**/
    Tree.prototype.restoreUIState = function(stateString, highlightName)
	{

      if(stateString == null || stateString == "" ||stateString == "null;null;null")
      {
         return;
      }
 //Timer.add('enter restoreUIState');

      var highlightNode = stateArray = selectArray = null;
      if(stateString.indexOf(";")!=-1){
			var openhighlightArray = stateString.split(";");


			highlightNode = openhighlightArray[1];

			stateArray = openhighlightArray[0].split(",");

			if(openhighlightArray[2]!=null&&openhighlightArray[2]!="")
				selectArray = openhighlightArray[2].split(",");
	   }else{

		    stateArray = stateString.split(",");
	   }


		var openLen = stateArray.length-1;
		for (var j = 0; j <=openLen; j++)
		  {

			 var signature = stateArray[j];
			 for(var i = 0; i< this.Adapter.expandedNode.length; i++)
			 {
				//Calculate  signature only once.
				//We are not supposed to update key fields.
				//Otherwise we could get an out-of-date signatur.


				var treeNode = this.Adapter.expandedNode[i];
				var tempSignature = treeNode.getSignature();

				if(tempSignature != null && tempSignature == signature)
				{
					if(highlightName!=null&&highlightName!='undefined'){
						var displayName = this.Adapter.applyStyle(treeNode.EClass.Name , treeNode);
						if(displayName == highlightName){
							highlightNode = tempSignature;
							j=stateArray.length; //so skip the rest of open node;
							break;

						}

					}

					var XMIId = this.Adapter.expandedNode[i].ID;
					this.expandNodeWithID(XMIId,j,openLen);



					//exit the inner loop
					break;
				}
			 }

		  }



  	  if(highlightNode!=null)
	  {

		  for(var i=0; i<this.a_index.length; i++)
		  {
			var treeNode = this.a_index[i].eobj;
			var tempSignature = treeNode.getSignature();
			if(tempSignature != null && tempSignature == highlightNode)
			{

				if(highlightName!=null&&highlightName!='undefined')
				{
					var displayName = this.Adapter.applyStyle(treeNode.EClass.Name , treeNode);

					if(displayName == highlightName)
					{
						this.a_index[i].select(false, true);
						break;
					}else{

						var xmiId = treeNode.ID;
						this.expandNodeWithID(xmiId);
						var parentNode = this.a_index[i];
						for(var j=0; j<parentNode.childItems.length; j++)
						{
							var highlightNode = parentNode.childItems[j];

							displayName = this.Adapter.applyStyle(highlightNode.eobj.EClass.Name , highlightNode.eobj);
							if(displayName == highlightName)
							{
								//in restoreUIState, don't call highlight event. Only highlight the node.

								highlightNode.select(false,true);

								break;
							}
						}

					}



				}else{
					this.a_index[i].select(false,true);
					break;
				}



		    }//if

		  }//for(var i=0
		  //in case the parent node is not open, so just set the highlightnode to it.
		  if(!this.selectedItem)
		  	this.selectedItem = highlightNode;
	  }//if(highlightNode!=null)




		if(selectArray!=null){

		  for (var j = 0; j < selectArray.length; j++)
		  {
		/*
			 var signature = selectArray[j];
			 if(signature!=null&&signature!="null"&&Trim(signature)!="")
				this.a_index[selectArray[j]].check("ONSELECT",true);
		*/
			 var signature = selectArray[j];


			 for(var k=0; k<this.a_index.length;k++){

				  var tempSignature = this.a_index[k].eobj.getSignature();

				  if(tempSignature != null && tempSignature == signature)
				  {

					  this.a_index[k].check("ONSELECT",true);
					  break;

				  }




			 }

		 }
	 }

	}
/**
* @method private Tree.prototype.getStateObj
*   This function is used to get the state object of the tree
**/

	Tree.prototype.getStateObj = function()
	{
		var stateObj = new Object();

		var openString= selectString = "";
		for(var key in this.a_index)
		{
			var itemObj = this.a_index[key];

			 if(itemObj.isOpened == true)
			 {
				 openString = constructStateString(itemObj.eobj, openString);
				//alert("inside loop stateString:" + openString);
			 }
			 if(this.rootItem.checkedItemArray[itemObj.eobj.ID]!=null)
			 {

			 	selectString = constructStateString(itemObj.eobj,selectString);
		 	 }

		}
		stateObj.openStr = openString;
		stateObj.selectStr = ";"+selectString;

		return stateObj;

	}
/**
* @method private Tree.prototype.getSelStateStr
*   This function is used to get the selection state string of the tree
**/

	Tree.prototype.getSelStateStr = function()
	{


      if(this.selectedItem){

		  var signature = (typeof(this.selectedItem)=="string")?this.selectedItem:this.selectedItem.eobj.getSignature();
		  if(signature!=null)
		  {
			return ";"+ signature;
		  }


	  }else{

		  return ";null";
	  }

	}

/**
* @method public Tree.prototype.generateStateString
* This function is used to generate a state string based on the
*	signature of the eobject on  each node. A comma delimited signature
*	string to represent which nodes are open, such as 1,5,8,9:10:1,2,3,4,
*	indicating that nodes with object signatures: 1, 5, 8, 9 are expaned.
*	10 is highlighted.1,2,3,4 are selected.
*
**/

   Tree.prototype.generateUIStateString = function()
	{
      var stateString = signature = "";

      var stateObj = this.getStateObj();

       //add highlight information to state string, required by biz porlet team Mike and Cesar.
      //use ":" to delimit open node state information and highlight node information.
		stateString += stateObj.openStr;

		stateString += this.getSelStateStr();

		stateString += stateObj.selectStr;

      //alert("inside generateTreeStateString stateString:" + stateString);

      return stateString;
	}
/**
* @method private generateStateString
* this method is used to construct open or selected nodes states string
* @param Eobject eobj
* 	The eobject of the node
* @param String stateString
*	The state string of the node
**/
	function constructStateString(eobj, stateString)
	{

				var signature = null;
				if(eobj) signature= eobj.getSignature();

				if(signature!=null)
				{
					if(stateString=="")
					{
						stateString= signature;
					}
					else
					{
						stateString+=","+signature;
					}
				}
				return stateString;


	}

/**
* @method private expandNodeWithID
* This function is used to restore the UI state of the tree based on a
*	state string.
* @param String xmiID
*    The xmiID of the EObject which need to be opened.
* @param Integer index
*    The position of the expanded node in the stateString
* @param Integer len
*	The length of the expanded node
**/

	Tree.prototype.expandNodeWithID = function(xmiID, index, len)
	{


		for(var i=0; i<this.a_index.length;i++){

			if(this.a_index[i].eobj.ID == xmiID){

				var expandNode = this.a_index[i];

				var xmlNode = expandNode.xmlNode;
				var nIndex= 0;



				var eobj = expandNode.eobj;


				var elementWrap = new ElementWrapper(xmlNode, true);

				if(xmlNode.getAttribute("childrenLen")>xmlNode.childNodes.length){
					//need to construct the next level of xml data


					this.Adapter.GetChildContent(elementWrap, eobj, 0);


				}
			//	alert(elementWrap.element.getAttribute("childrenLen"));

				//alert(GetXMLString(10,xmlNode));

				//new TreeItem(itemObj, xmlNode, 0, this.IconLib);

				if(elementWrap.element.getAttribute("childrenLen")>0){
					//has children nodes
					for (var j= 0; j < xmlNode.childNodes.length; j++)
					{
						var node = xmlNode.childNodes[j];

						if (node.nodeType != 1)
							continue;


						new TreeItem(expandNode, node, nIndex);

						nIndex++;

					}


					this.expand(i);

               //alert("after expand");
					//do onExpand event handler for the last expanded node.
					if(index==len){


						//if(!expandNode.isOpened){
							//expand event
							if(!expandNode.acitveNodeEvent("onExpand"))
								return false;
							if(!expandNode.activeTreeEvent("onNodeExpand"))
								return false;


					}



				}else{

					//change the + to leaf
					expandNode.childItems.length = 0;
					expandNode.changeNode2Leaf();
					expandNode.isOpened = true;


				}
				break;
			}//if
			//break;

		}


	}
	/**
	* @method public Tree.prototype.constructStateStrFromObj
    * 	This function is used to generate the state string from an array of
    *	eobjects, Tracy requested.
    * @param Array inputArr
    *   The array of eobjects starting from root to the to-be-highlighted
    *	nodes. for example,
    *	&nbsp;&nbsp;root
    *	&nbsp;&nbsp;&nbsp;|___dir1
    *	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|__dir12
    *	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|___file1
    *	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|___file2
    *   &nbsp;&nbsp;&nbsp;|____dir2
    *	If you want file1 to be highlighted at the first render time, so the
    *	eobjects array will look like this, [root.eobject, dir1.eobject,
    *	dir12.eobject, file1.eobject]
    * @param String stateStr
    * State string
    * @return String stateStr
    *	The state string which can be used to restore the status.
    **/

	Tree.prototype.constructStateStrFromObj = function(inputArr, stateStr)
	{

		if(inputArr==null||inputArr=='undefined')
			return null;

		var stateString = openString = "";
		var len = inputArr.length-1;

		for(var i=0; i<len; i++)
		{

			openString = constructStateString(inputArr[i], openString);

		}

		stateString += openString;

		if(inputArr[len]!=null){

			var signature = inputArr[len].getSignature();
			if(signature!=null)
			{
				stateString += ";"+ signature+";";
			}


		}else{

		    stateString += ";null;";
		}



		return stateString

	}



	/**
 	* @method public Tree.prototype.constructStateStrFromAttr
    * 	This function is used to generate the state string from attribute
    * 	name and an array of attribute value, Darryl requested.
    * @param String attrName
    *  The name of the attribute of eobject.like "UID".
    * @param Array valueArr
    *    The array of value of attribute of eobjects starting from root
    * 	 to the to-be-highlighted nodes. for example,
    *	&nbsp;&nbsp;root
    *	&nbsp;&nbsp;&nbsp;|___dir1
    *	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|__dir12
    *	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|___file1
    *	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|___file2
    *   &nbsp;&nbsp;&nbsp;|____dir2
    *	If you want file1 to be highlighted at the first render time, so
    *	the array will look like this, ["root", "dir1", "dir12", "file1"]
    * @param String stateStr
    *  The state string
    * @return String stateStr
    *	The state string which can be used to restore the status.
    **/

	Tree.prototype.constructStateStrFromAttr = function(attrName, valueArr,stateStr)
	{
		var eobjArr= new Array();

		if(attrName&&valueArr)
		{
			for(var i=0; i<valueArr.length; i++)
			{
				eobjArr[eobjArr.length]= findEObjectByAtrName(this.Adapter.eobjRoot, attrName, valueArr[i]);
			}
		}

		if(eobjArr.length>0)
			return this.constructStateStrFromObj(eobjArr, stateStr);
		else
			return null;

	}


/**
* @method public Tree.prototype.getSelectedItems
*   This function is returning an array of selected item's eobjets. If none
*	of items is selected, then return empty array
* @return Array
*	An array of selected item's eobjects
**/
	Tree.prototype.getSelectedItems = function()
	{

		return this.compactCheckedItemsArray();
	}


/**
* @method public Tree.prototype.getHighlightedItem
*   This function is returning the highlighted item's eobject. If none of items
*	is highlighted, then return null.
* @return eObject
*	the eObject of highlighted item.
**/
	Tree.prototype.getHighlightedItem = function()
	{
		return 	this.selectedItem.eobj;

	}



/**
* @method public Tree.prototype.getExpandededItems
*   This function is returning the expanded item's eobject. If none of items
*	is expanded, then return null.
* @return Array
*	An array of expanded item's eobjects
**/
	Tree.prototype.getExpandededItems = function()
	{
		return 	this.compactOpenItemsArray();

	}


/**
 * @method private Tree.prototype.compactCheckedItemsArray
 * 	This method is used to clean up the checked items array so that the event object will only carry the checked node's eobjects.
**/
	Tree.prototype.compactCheckedItemsArray = function()
	{
		var checkedItems = this.checkedEObjectArray;
		var rtArray = new Array();
		for(var k in checkedItems)
		{
			if(checkedItems[k] != null)
			{
				rtArray[rtArray.length] = checkedItems[k];
			}

		}
		return rtArray;
	}


/**
 * @method private Tree.prototype.compactOpenItemsArray
 * 	This method is used to clean up the open items array so that the event object will only carry the checked node's eobjects.
**/
	Tree.prototype.compactOpenItemsArray = function()
	{
		var openItems = this.Adapter.expandedNode;
		var rtArray = new Array();
		for(var k in openItems)
		{
			if(openItems[k] != null)
			{
				rtArray[rtArray.length] = openItems[k];
			}

		}
		return rtArray;
	}


/**
 * @class private TreeItem
 * This class is used to construct every sigle nodes and put them into a big array based on the xml
 * which is getting from the tree adapter.
 * @constructor private TreeItem
 * @param Object parentItem
 *	The object which is the parent of the to be constructed tree node
 * @param String xmlNode
 *	The paretnItem's xml string which has all the information about children nodes
 * @param int itemIndex
 *	This number is used to indicate how many childrens node of the parent node
 * @param String AnIconLib
 *	The icons' relative path which is the same as urlprefix in the tree constructor
 *
**/
function TreeItem (parentItem, xmlNode, itemIndex , isLeaf)
{
//Timer.add('1 ===== new Tree item');
   // this.IconLib = AnIconLib;

	//If isLeaf is undefined set it to default value
	if(isLeaf=="undefined"||isLeaf==null)
		this.isLeaf=2;
	else
		this.isLeaf=isLeaf;


	// TreeItem properties
	this.depth  = parentItem.depth + 1;
	this.xmlNode = xmlNode;
	//this.itemName = this.xmlNode.nodeName;
	this.itemName = this.xmlNode.displayName;
	//alert(this.itemName);
	//this.itemValue = getXMLNodeValue(this.xmlNode);

	this.rootItem = parentItem.rootItem;
	this.parentItem = parentItem;
	this.itemIndex = itemIndex;
	//this.isOpened = !this.depth;
	this.isOpened = false; // initially, the item will always be closed, starting from the top.
	this.rootItem.alength++;

	this.itemID = this.rootItem.a_index.length;
	this.rootItem.a_index[this.itemID] = this;


	parentItem.childItems[itemIndex] = this;

	this.customItemOpenedImage = xmlNode.getAttribute("treeopenimgfile");

	this.customItemClosedImage = xmlNode.getAttribute("treecloseimgfile");
	this.showSystemIconFlag = xmlNode.getAttribute("showsystemiconflag");
	this.customOpenedImagealt = xmlNode.getAttribute("treeopenimgalt")
	this.customClosedImagealt =  xmlNode.getAttribute("treecloseimgalt")

	//this.EObjectID = xmlNode.getAttribute("EObjectID");

	this.eobj = xmlNode.eobject;
	//this.itemID = this.eobj.getSignature();
	//this.rootItem.a_index[this.itemID] = this;


	this.propertName = xmlNode.propertyName;

	this.eventHandler = xmlNode.eventHandler; //node's event handler

	//alert(xmlNode.getAttribute("childrenLen"));
	/*var temp = xmlNode.getAttribute("childrenLen");
	if(temp!=null&&temp>0)
			this.childItems = new Array(1); //so that we know this is a node,not leaf.
	else
		this.childItems = new Array();*/
		
	
	//Setting up child information from limited information available
	//First two options are used in the dynamic building of a tree when the
	//child information for the node is not available at node creation time.
	//In this situation the developer must specify explicitly whether a node
	//should be a leaf or not. This flag is sent in via treeAdapter.addNode method
	//in the last parameter

	//Variable containing info regarding children
	var numChildren = xmlNode.getAttribute("childrenLen");

	//If explicitly specified as leaf at creation time
	if(this.isLeaf==1)
		this.childItems = [];
	//If explicitly specified as NOT a leaf and if xmlNode.getAttribute("childrenLen") returns a
	//number greater than 0
	else if(this.isLeaf==0)
	{
		if(numChildren!=null&&numChildren>=0)
		{
			var dummyObject="dummy";
			this.childItems = new Array(1);
			//need to do more than give it a length because if there is no value for childItems[0]
			//it will be shown as a leaf
			this.childItems[0]=dummyObject;
		}
	}
	//When NOT creating dynamically it is determined whether it has children or not is based on the value provided by
	//the variable numChildren which contains the value returned by xmlNode.getAttribute("childrenLen")
	else if( ( numChildren!=null && numChildren>0 )
			|| this.itemID==0 )
		this.childItems = new Array(1); 
	//If no valid value is returned by xmlNode.getAttribute("childrenLen") then it is considered a leaf.
	else
	{
		xmlNode.setAttribute("hasNoChildrenValues",true);
		this.childItems = [];
	}
    	

	var nIndex = 0;
	for (var i = 0; i < xmlNode.childNodes.length; i++)
	{
		var node = xmlNode.childNodes[i];

		if (node.nodeType != 1)
			continue;
		//alert("childnodes length " + node.childNodes.length);


		new TreeItem(this, node, nIndex);

		nIndex++;
		//}
	}

	//alert('here');

}

//Timer.add('2 ===== new Tree item');




//	Log.error("new treeitem", "after call another tree item");
/**
 * @method private TreeItem.prototype.getJunctionImage
 * 	This method is used to get the images which connect between the nodes.  These images are sysstem icons.
 * @return String srcImage
 *	the image's source path
 *
**/
	TreeItem.prototype.getJunctionImage = function getJunctionImage()
	{
		var srcImage = '';

		var childless = !this.hasChildren(); 

		if (this.childItems.length >0 && this.isOpened && !childless) 
			srcImage = (!this.rootItem.rootVisibleFlag&&this.itemID==1)?this.rootItem.treeIcons["minustop"]:(this.isLast() ? this.rootItem.treeIcons["minus"] : this.rootItem.treeIcons["minusbottom"]);// junction for open node
		else if (this.childItems.length >0 && !childless) 
			srcImage = (!this.rootItem.rootVisibleFlag&&this.itemID==1)?this.rootItem.treeIcons["plustop"]:(this.isLast() ? this.rootItem.treeIcons["plus"] : this.rootItem.treeIcons["plusbottom"]);// junction for closed node
		else
			srcImage = (!this.rootItem.rootVisibleFlag&&this.itemID==1)?this.rootItem.treeIcons["jointtop"]:(this.isLast() ? this.rootItem.treeIcons["joint"] : this.rootItem.treeIcons["jointbottom"]);// junction for leaf

		return srcImage;
	}

/**
 * @method private TreeItem.prototype.getImage
 * 	This method is used to get node's images.
 * @param boolean isJunction
 *	this parameter is not needed right now.
 * @return String srcImage
 *	the image's source path
**/
	TreeItem.prototype.getImage = function getImage(isJunction)
	{
		var srcImage = '';

//code change for defect 267427(aix family)  so that root node icon will not be treated especially. 10/27
		if (this.depth >= 0)
		{
			var userDefinedIconMappingArray = null;
			//NS has to explicit to check if the image is not 'undefined'.
			if (this.customItemOpenedImage&& (this.customItemOpenedImage != 'undefined')&&(this.isOpened)) { //||(this.isSelected)) ) {
				// code to check the path?
				return this.customItemOpenedImage;


			} else if (this.customItemClosedImage&&(this.customItemOpenedImage != 'undefined')&& (!this.isOpened)) {
				// code to check the path?
				return this.customItemClosedImage;

			}


			if (this.childItems.length && this.isOpened)
			{
				if(this.itemID ==0)
					srcImage = this.rootItem.treeIcons["baseopen"];
				else
				// folder icon opened
					srcImage = this.rootItem.treeIcons["nodeopen"];
			}
			else if (this.childItems.length)
			{

				// folder icon normal
					srcImage = this.rootItem.treeIcons["nodeclose"];
			}
			else
			{
				// leaf icon opened
					srcImage = this.rootItem.treeIcons["leaf"];
			}

		}

		return srcImage;
	}


	TreeItem.prototype.refresh = function()
	{
		var wasOpened = this.isOpened;
		var htmlElement = get_element('i_div' + this.rootItem.itemID + '_' + this.itemID);
		if (!htmlElement)
		{
			htmlElement = get_element('i_txt' + this.rootItem.itemID + '_' + this.itemID);
			if (!htmlElement)
				return;
		}

		var htmlRootElement = get_element('i_div' + this.rootItem.itemID + '_' + this.rootItem.itemID);
		if (!htmlRootElement)
		{
			htmlRootElement = get_element('i_txt' + this.rootItem.itemID + '_' + this.rootItem.itemID);
			if (!htmlRootElement)
				return;
		}

		if (this.depth <= 1 && this.childItems.length > 0)
		{
			htmlRootElement.parentNode.innerHTML = this.rootItem.childItems[0].init();
			this.rootItem.childItems[0].open();
		}
		else
			htmlElement.parentNode.innerHTML = this.init();

		this.open(!wasOpened);
	}



	/**
	 * @method private TreeItem.prototype.changeIcon
	 * 	This method is used to change the sytstem and folder icons
	**/
	TreeItem.prototype.changeIcon  = function item_open ()
	{

		var o_jicon = document.images['j_img' + this.rootItem.itemID + '_' + this.itemID],
			o_iicon = document.images['i_img' + this.rootItem.itemID + '_' + this.itemID];


         if (o_jicon)
            {
               o_jicon.src = this.getJunctionImage();
               o_jicon.alt = this.getJunctionImageAlt();
            }
         if (o_iicon)
            {
               o_iicon.src = this.getImage();
               o_iicon.alt = this.getImageAlt();
            }

	}

	/**
	 * @method private TreeItem.prototype.changeNode2Leaf
	 * 	This method is used to change the node icons to leaf icon, also disable the onclick/ondblclick event.
	**/

	TreeItem.prototype.changeNode2Leaf  = function ()
	{
		this.changeIcon();

		var o_jalink = document.getElementById('j_a' + this.rootItem.itemID + '_' + this.itemID),
			o_ialink = document.getElementById('i_a' + this.rootItem.itemID + '_' + this.itemID);
		if(o_jalink){

			o_jalink.onclick = o_jalink.ondblclick = "javascript:return;";
		}
		if(o_ialink){

			o_ialink.ondblclick="javascript:return;";
		}
	}

/**
 * @method private TreeItem.prototype.open
 * 	This method is used to open or close the node depending on the node's statues (opened or closed)
 * @param boolean toClose
 *	If this node is open or closed
**/
	TreeItem.prototype.open  = function item_open (toClose)
	{
		var divElem = get_element('i_div' + this.rootItem.itemID + '_' + this.itemID);
		if (!divElem)
			return;

		if (!divElem.innerHTML)
		{
			var childItems = [];
			var str;
			//alert(this.childItems.length);
			try{
				for (var i = 0; i < this.childItems.length; i++){

				//	Timer.add('===== before new Tree item');
					childItems[i]= this.childItems[i].init();

				//	Timer.add(' ===== after new Tree item');
				}
			}catch(e){
				//alert(odcTreeNoChildErr);
				this.childItems.length=0;
				this.changeNode2Leaf();

				return;

			}
			divElem.innerHTML = childItems.join('');
		}
		divElem.style.display = (toClose ? 'none' : 'block');

		this.isOpened = !toClose;
		this.changeIcon();

		this.upstatus(false, "open");

/*

		// Expand event firing
		if (!toClose && this.rootItem.onexpand != null)
			this.rootItem.onexpand(this);
		// Collapse event firing
		if (toClose && this.rootItem.oncollapse != null)
			this.rootItem.oncollapse(this);
*/

	}
/**
 * @method private TreeItem.prototype.activeTreeEvent
 * 	This method is used to active the corresponding event when user select or highlight a tree node.
 * @param String action
 *	the actions tree supports, like onHighlight, onSelect
**/
	TreeItem.prototype.activeTreeEvent = function(action,extrainfo)
	{
		var uaction = action.toUpperCase();
				if(this.rootItem.eventArray.length>0){


					for(var i=0; i<this.rootItem.eventArray.length;i++)
					{

						if(this.rootItem.eventArray[i][0].toUpperCase() == uaction){

							var handler = this.rootItem.eventArray[i][1];


								 var e = new ODCEvent(action);
								 e.eobject=this.eobj;
								 e.model = this.rootItem.Adapter.xmiModel;

								 switch(uaction){
								 	case "ONNODEHIGHLIGHT":
								 		e.propertyName = this.propertyName

								 		break;

								 	case "ONNODEEXPAND":case "ONNODECOLLAPSE":
								 		e.levels = (extrainfo)?extrainfo:"1";
										e.openedItemsArray = this.rootItem.compactOpenItemsArray();

								 		var str = this.rootItem.getStateObj();
								 		var openStr = str.openStr;
								 		var selStr = this.rootItem.getSelStateStr();
								 		var chkStr = str.selectStr;
								 		var lastExpandSig = this.eobj.getSignature();

								 		e.stateString = openStr+","+ lastExpandSig+selStr+chkStr;
								 		//alert(e.stateString);

								 		var expandArr = new Array();
								 		if(openStr.indexOf(",")!=-1)
								 			expandArr = openStr.split(",");
								 		else
								 			//in case only the root is expanded at this time.
								 			expandArr[expandArr.length] = openStr;

								 		expandArr[expandArr.length] = lastExpandSig;

								 		e.expandedNodeArray = expandArr;
								 		break;

								 	case "ONNODESELECT": case "ONNODEUNSELECT":
								 		e.checkedItemsArray = this.rootItem.compactCheckedItemsArray();
								 		break;

									default:
										break;
								}




								 /*


								 if(action.toUpperCase()=="ONNODEHIGHLIGHT")
									e.propertyName = this.propertyName
								 else if(action.toUpperCase()=="ONNODEEXPAND")
								 	e.stateString = this.rootItem.getStateObj().openStr+","+ this.eobj.getSignature();
								 else
									//e.checkedItemArray = this.rootItem.checkedItemArray;
									e.checkedItemsArray = this.compactCheckedItemsArray();
								 */

								  if (eval(handler+".call(this,this, e);") == false)
								   {
									 return false;
								   }




						}
					}

				}
				return true;

	}









/**
 * @method private TreeItem.prototype.activeNodeEvent
 * 	This method is used to active the corresponding event when user select or highlight a tree node.
 * @param String action
 *	the actions tree node supports, like onHighlight, onSelect
**/
	TreeItem.prototype.acitveNodeEvent = function(action,extrainfo)
	{
		var uaction = action.toUpperCase();
		if(this.eventHandler!=null&&this.eventHandler!='undefined')
		{




				this.eventHandler = this.eventHandler.replace(/ /g, "");
				var eventArray = this.eventHandler.split(";");
				for(var i=0; i<eventArray.length; i++)
				{
					var events = eventArray[i].split(",");
					if(events[0].toUpperCase() == uaction)
					{

						var handler = events[1];

						 var e = new ODCEvent(action);
						 e.eobject=this.eobj;
						 e.model = this.rootItem.Adapter.xmiModel;


								 switch(uaction){
								 	case "ONHIGHLIGHT":
								 		e.propertyName = this.propertyName

								 		break;

								 	case "ONEXPAND","ONCOLLAPSE":
										e.openedItemsArray = this.rootItem.compactOpenItemsArray();
										e.levels = (extrainfo)?extrainfo:"1";
										break;

								 	case "ONSELECT", "ONUNSELECT":
								 		e.checkedItemsArray = this.rootItem.compactCheckedItemsArray();
								 		break;

									default:
										break;
								}


/*
						 if(action.toUpperCase()=="ONHIGHLIGHT")
							e.propertyName = this.propertyName
						 else
							//e.checkedItemArray = this.rootItem.checkedItemArray;

							e.checkedItemsArray  = this.compactCheckedItemsArray();
*/
						  if (eval(handler+".call(this,this, e);") == false)
						   {
							 return false;
						   }

					}




			}


		}



		return true;

	}



/**
 * @method private TreeItem.prototype.select
 * 	This method is used to highlight the to be selected node or unhighlight the selected code
 * @param boolean toDeselect
 *	If this node is selected or unselected
**/
	TreeItem.prototype.select = function item_select (toDeselect,disableEvent)
	{
		//this.isSelected = true;

			if (!toDeselect)
			{


				if(!disableEvent){
						if(!this.acitveNodeEvent("onHighlight"))
							return false;
						if(!this.activeTreeEvent("onNodeHighlight"))
							return false;

				}

				var oldItem = this.rootItem.selectedItem;
				this.rootItem.selectedItem = this;


				if (oldItem) {
					if(typeof(oldItem)!="string")
						oldItem.select(true);
					//oldItem.isSelected = false;
				}
			}
		var o_iicon = document.images['i_img' + this.rootItem.itemID + '_' + this.itemID];
		if (o_iicon) o_iicon.src = this.getImage();

		var labelNormalStyle = this.rootItem.styleClass+"labelNormalStyle";
		var labelHighlightStyle = this.rootItem.styleClass+"labelHighlightStyle";


		if (get_element('i_txt' + this.rootItem.itemID + '_' + this.itemID) != null)
			//	get_element('i_txt' + this.rootItem.itemID + '_' + this.itemID).style.fontWeight = toDeselect ? 'normal' : 'bold';
			get_element('i_txt' + this.rootItem.itemID + '_' + this.itemID).className = toDeselect ? labelNormalStyle : labelHighlightStyle;


/*
		if (get_element('i_txt' + this.rootItem.itemID + '_' + this.itemID) != null)
		//	get_element('i_txt' + this.rootItem.itemID + '_' + this.itemID).style.fontWeight = toDeselect ? 'normal' : 'bold';
			get_element('i_txt' + this.rootItem.itemID + '_' + this.itemID).className = toDeselect ? labelNormalStyle : labelHighlightStyle;

*/
	//	this.upstatus(false,"select");
		window.status = this.itemName + (this.itemTarget ? ' ('+ this.itemTarget + ')' : '');
		return Boolean(this.itemTarget);
	}
/**
 * @method private TreeItem.prototype.check
 * 	This method is used to select the checkbox to select the node or unselect the selected code
 * @param String selectFlag
 *	If this node is selected "ONSELECT" or unselected "ONUNSELECT"
**/
	TreeItem.prototype.check = function item_check(selectFlag, disableEvent)
	{

		if(selectFlag == "ONSELECT"){

			this.rootItem.checkedItemArray[this.eobj.ID] = this.itemID;
			this.rootItem.checkedEObjectArray[this.eobj.ID] = this.eobj;

		}else{
			this.rootItem.checkedItemArray[this.eobj.ID] = null;
			this.rootItem.checkedEObjectArray[this.eobj.ID] = null;

		}


  		 this.recursiveCheck(this.rootItem, this, selectFlag);

		if(!disableEvent){


			//handler node leve onselect/onunselect event
			 if(!this.acitveNodeEvent(selectFlag))
				return false;

			//hanlder tree level onSelect/onunselect event
			if(selectFlag.toUpperCase == "ONSELECT"){
			 	if(!this.activeTreeEvent("onNodeSelect"))
					return false;
			}else{
				if(!this.activeTreeEvent("onNodeUnSelect"))
					return false;
			}
		}


		return true;


	}

/**
 * @method private TreeItem.prototype.recursiveCheck
 * 	This method is used to select the children node if the parent node is selected or verse vise
 * @param Object tree
 *	the tree object
 * @param Object selectItem
 *	the parent node
 * @param String selectFlag
 *	If this node is selected "ONSELECT" or unselected "ONUNSELECT"
**/

  	 TreeItem.prototype.recursiveCheck = function(tree, selectItem, selectFlag)
   	 {

		 //select the current selected node
	 	if(selectFlag=="ONSELECT"){

			get_element('ckbox' + tree.itemID + '_' + selectItem.itemID).checked = true;
		}else{
			get_element('ckbox' + tree.itemID + '_' + selectItem.itemID).checked = false;
		}


		//select the parent node
		this.backwardRecursive(tree,selectItem.parentItem, selectFlag);

		//select all the children node
		 for(var i=0; i<selectItem.childItems.length; i++)
		 {

			if(selectItem.childItems[i]!=null){

				if (get_element('ckbox' + tree.itemID + '_' + selectItem.childItems[i].itemID) != null)
					if(selectFlag=="ONSELECT"){
						get_element('ckbox' + tree.itemID + '_' + selectItem.childItems[i].itemID).checked = true;

						tree.checkedItemArray[selectItem.childItems[i].eobj.ID] = selectItem.childItems[i].itemID;
						tree.checkedEObjectArray[selectItem.childItems[i].eobj.ID] = selectItem.childItems[i].eobj;

					}else{

						get_element('ckbox' + tree.itemID + '_' + selectItem.childItems[i].itemID).checked = false;
						tree.checkedItemArray[selectItem.childItems[i].eobj.ID] = null;
						tree.checkedEObjectArray[selectItem.childItems[i].eobj.ID] = null;

					}

				this.recursiveCheck(tree, selectItem.childItems[i], selectFlag);
			}

	 	}


	 }

/**
 * @method private TreeItem.prototype.backwardRecursive
 * 	This method is used to select the parent node if all the children node are selected or verse vise
 * @param Object tree
 *	the tree object
 * @param Object selectItem
 *	the parent node
 * @param String selectFlag
 *	If this node is selected "ONSELECT" or unselected "ONUNSELECT"
**/

		 TreeItem.prototype.backwardRecursive = function(tree,parentItem, selectFlag)
		 {


			if(parentItem.eobj!=null){
				if(selectFlag=="ONUNSELECT"){

					for(var j=0; j<parentItem.childItems.length;j++)
					{
						var eobjid = parentItem.childItems[j].eobj.ID;

						if(tree.checkedItemArray[eobjid]!=null)
						{
							return;
						}



					}
					if(get_element('ckbox' + tree.itemID + '_' + parentItem.itemID)!=null)
						get_element('ckbox' + tree.itemID + '_' + parentItem.itemID).checked = false;

					tree.checkedItemArray[parentItem.eobj.ID] = null;
					tree.checkedEObjectArray[parentItem.eobj.ID] = null;

				}else{

					for(var j=0; j<parentItem.childItems.length;j++)
					{
						var eobjid = parentItem.childItems[j].eobj.ID;

						if(tree.checkedItemArray[eobjid]==null)
						{
							return;
						}



					}
				//	if(get_element('ckbox' + tree.itemID + '_' + parentItem.itemID)!=null)
						get_element('ckbox' + tree.itemID + '_' + parentItem.itemID).checked = true;
					tree.checkedItemArray[parentItem.eobj.ID] = parentItem.itemID;
					tree.checkedEObjectArray[parentItem.eobj.ID] = parentItem.eobj;


				}
				this.backwardRecursive(tree,parentItem.parentItem, selectFlag);
			}

	 }

/**
 * @method private TreeItem.prototype.getJunctionImageAlt
 * 	This method is used to construct the html print out for that node
**/
	TreeItem.prototype.getJunctionImageAlt = function()
	{

		if (this.childItems.length && this.isOpened)
         return odcTreeimgCollapse; // junction for opened node
		else if (this.childItems.length)
         return odcTreeimgExpand;  // junction for closed node
		else
		 return "";

	}

/**
 * @method private TreeItem.prototype.getImageAlt
 * 	This method is used to construct the html print out for that node
**/

	TreeItem.prototype.getImageAlt = function()
	{

		if (this.depth >=0)
		{
			if (this.childItems.length && this.isOpened)
			{

				if(this.customOpenedImagealt)
					return this.customOpenedImagealt;
				else
					return odcTreeimgFolderOpen;
			}
			else if (this.childItems.length)
			{
				if(this.customClosedImagealt)
					return this.customClosedImagealt;
				else
					return odcTreeimgFolderNormal;
			}
			else
			{
				if(this.customOpenedImagealt)
					return this.customOpenedImagealt;
				else if(this.customClosedImagealt)
					return this.customClosedImagealt;
				else
					return odcTreeimgLeaf;
			}

		}




	}

/**
 * @method private TreeItem.prototype.init
 * 	This method is used to construct the html print out for that node
**/
	TreeItem.prototype.init = function item_init()
	{

		var a_offset = [],currentItem = this.parentItem;

		for (var i = this.depth; i > 1; i--)
		{
			currentItem.isLast() ? strSrc = this.rootItem.treeIcons["empty"] : strSrc = this.rootItem.treeIcons["line"];

			a_offset[i] = '<img src="' + strSrc + '" border="0" align="absbottom">';
			currentItem = currentItem.parentItem;
		}

		var iconStyle = this.rootItem.styleClass+"nodeImageStyle";

		var nodeLableStyle = this.rootItem.styleClass+"labelNormalStyle";

		var linkclass = this.rootItem.styleClass+"linkTextStyle";

		var item = this.rootItem.selectedItem;

		if(item){
			if(typeof(item) == "string"){
				if(item ==this.eobj.getSignature()){
					nodeLableStyle = this.rootItem.styleClass+"labelHighlightStyle";
					this.rootItem.selectedItem = this;
				}

			}
		}

		var str=str1=str2=str3=str4=str5 = "";
		if(!this.rootItem.rootVisibleFlag&&this.itemID==0)
		{
			//root is not visible

			if(this.childItems.length)
				str= '<div style="border-width:0px" id="i_div' + this.rootItem.itemID + '_' + this.itemID + '" style="display:none"></div>';
			return str;

		}else{

			//regular case
			str = '<table cellpadding="1" cellspacing="0" border="0"><tr><td nowrap>';

			if(this.depth){
				//str+= a_offset.join('');
				str1 = a_offset.join('');
				if(this.hasChildren()){
				/*
					str+='<a href="javascript:;" onclick = "return odcTrees[' + this.rootItem.itemID + '].toggle(\'' + this.itemID + '\')" '
						+'onmouseover="odcTrees[' + this.rootItem.itemID + '].mover(\'' + this.itemID + '\')" '
					 	+'onmouseout="odcTrees[' + this.rootItem.itemID + '].mout(\'' + this.itemID + '\')">'
						+'<img src="' + this.getJunctionImage() + '"  alt="'+this.getJunctionImageAlt()+'" '
						+'border="0" align="absbottom" name="j_img' + this.rootItem.itemID + '_' + this.itemID + '" class="'+iconStyle+ '"></a>';
				*/
				str2 = '<a href="javascript:;" class='+linkclass+' id="j_a' + this.rootItem.itemID + '_' + this.itemID + '" onclick = "return odcTrees[' + this.rootItem.itemID + '].toggle(' + this.itemID + ')" '
						+'onmouseover="odcTrees[' + this.rootItem.itemID + '].mover(' + this.itemID + ')" '
					 	+'onmouseout="odcTrees[' + this.rootItem.itemID + '].mout(' + this.itemID + ')">'
						+'<img src="' + this.getJunctionImage() + '"  alt="'+this.getJunctionImageAlt()+'" '
						+'border="0" align="absbottom" name="j_img' + this.rootItem.itemID + '_' + this.itemID + '" class="'+iconStyle+ '"></a>';
				}
				else{
				
				str2 = '<img src="' + this.getJunctionImage() + '"  alt="'+this.getJunctionImageAlt()+'" '
						+'border="0" align="absbottom" name="j_img' + this.rootItem.itemID + '_' + this.itemID + '" class="'+iconStyle+ '">';
				
					if(this.showSystemIconFlag!="false")
						//str+='<img src="' + this.getJunctionImage() + '" class="'+iconStyle+ '" border="0" align="absbottom">';
						str2='<img src="' + this.getJunctionImage() + '" class="'+iconStyle+ '" border="0" align="absbottom">';
				}
			}

			if(odcTrees[this.rootItem.itemID].enableSelect)
			{
				/*
				str+='<input type=checkbox id="ckbox' + this.rootItem.itemID + '_' + this.itemID + '" '
					+'onclick="javascript: odcTrees[' + this.rootItem.itemID + '].check(this,\'' + this.itemID + '\')">';
					*/
				str3 = '<input type=checkbox id="ckbox' + this.rootItem.itemID + '_' + this.itemID + '" '
					+'onclick="javascript: odcTrees[' + this.rootItem.itemID + '].check(this,' + this.itemID + ')">';
			}
			
			
			if(this.hasChildren())
			{
				var linkclass =
				str4 ='<a href="javascript:;" class='+linkclass+' onclick="return odcTrees[' + this.rootItem.itemID + '].select(' + this.itemID + ')" '
					+'ondblclick="odcTrees[' + this.rootItem.itemID + '].toggle(' + this.itemID + ')" '
					+'onmouseover="odcTrees[' + this.rootItem.itemID + '].mover(' + this.itemID + ')" '
					+'onmouseout="odcTrees[' + this.rootItem.itemID + '].mout(' + this.itemID + ')" '
					+' id="i_a' + this.rootItem.itemID + '_' + this.itemID + '">'
					+'<img src="' + this.getImage() + '" alt="'+this.getImageAlt() + '" '
					+'border="0" align="absbottom" name="i_img' + this.rootItem.itemID + '_' + this.itemID + '" '
					+'class='+iconStyle+'>'
					+'<font id="i_txt'+ this.rootItem.itemID + '_' + this.itemID + '"'+' class='+nodeLableStyle+'>'+ this.itemName
					+'</font></a></td></tr></table>';
			}
			else
			{
				var linkclass =
				str4 ='<a href="javascript:;" class='+linkclass+' onclick="return odcTrees[' + this.rootItem.itemID + '].select(' + this.itemID + ')" '					
					+'onmouseover="odcTrees[' + this.rootItem.itemID + '].mover(' + this.itemID + ')" '
					+'onmouseout="odcTrees[' + this.rootItem.itemID + '].mout(' + this.itemID + ')" '
					+' id="i_a' + this.rootItem.itemID + '_' + this.itemID + '">'
					+'<img src="' + this.getImage() + '" alt="'+this.getImageAlt() + '" '
					+'border="0" align="absbottom" name="i_img' + this.rootItem.itemID + '_' + this.itemID + '" '
					+'class='+iconStyle+'>'
					+'<font id="i_txt'+ this.rootItem.itemID + '_' + this.itemID + '"'+' class='+nodeLableStyle+'>'+ this.itemName
					+'</font></a></td></tr></table>';
			}
			/*
			str+='<a href="javascript:;" onclick="return odcTrees[' + this.rootItem.itemID + '].select(\'' + this.itemID + '\')" '
				+'ondblclick="odcTrees[' + this.rootItem.itemID + '].toggle(\'' + this.itemID + '\')" '
				+'onmouseover="odcTrees[' + this.rootItem.itemID + '].mover(\'' + this.itemID + '\')" '
				+'onmouseout="odcTrees[' + this.rootItem.itemID + '].mout(\'' + this.itemID + '\')" '
				+'class='+nodeLableStyle+' id="i_txt' + this.rootItem.itemID + '_' + this.itemID + '">'
				+'<img src="' + this.getImage() + '" alt="'+this.getImageAlt() + '" '
				+'border="0" align="absbottom" name="i_img' + this.rootItem.itemID + '_' + this.itemID + '" '
				+'class='+iconStyle+'>'
				+'<font ID=i_eobject_' +this.eobj.ID+'>'+ this.itemName
				+'</font></a></td></tr></table>';
			*/

			if(this.childItems.length)
				//str+='<div style="border-width:0px" id="i_div' + this.rootItem.itemID + '_' + this.itemID + '" style="display:none"></div>';
				str5 ='<div style="border-width:0px" id="i_div' + this.rootItem.itemID + '_' + this.itemID + '" style="display:none"></div>';

		//alert("str " + str);
			return str.concat(str1, str2, str3,str4,str5);

		}

	//	return str;



	}
/**
 * @method private TreeItem.prototype.upstatus
 * 	This method is used to update the brower's status bar
 * @param boolean b_clear
 *	If true, clear all the statement, otherwise, print out the node name etc.
**/
	TreeItem.prototype.upstatus = function item_upstatus (b_clear,type)
	{



			window.status = b_clear ? '' : this.itemName + (this.itemTarget ? ' ('+ this.itemTarget + ')' : '');

			var labelMouseoverStyle = this.rootItem.styleClass+"labelMouseoverStyle";
			var labelNormalStyle = this.rootItem.styleClass+"labelNormalStyle";
			var labelHighlightStyle = this.rootItem.styleClass+"labelHighlightStyle";
			if (get_element('i_txt' + this.rootItem.itemID + '_' + this.itemID) != null)
			{
				if(type=="mouseover"){

					get_element('i_txt' + this.rootItem.itemID + '_' + this.itemID).className = labelMouseoverStyle;

				}else if(type=="mouseout"){
					var item = odcTrees[this.rootItem.itemID].selectedItem;

					if(item&&typeof(item)!="string"&&item.eobj.ID == this.eobj.ID){

						//restore the highlighted node's style
						get_element('i_txt' + this.rootItem.itemID + '_' + this.itemID).className = labelHighlightStyle;

					}
					else
						get_element('i_txt' + this.rootItem.itemID + '_' + this.itemID).className = labelNormalStyle;
				}else{

					get_element('i_txt' + this.rootItem.itemID + '_' + this.itemID).className = labelNormalStyle;
				}



			}


	}


/**
 * @method private TreeItem.prototype.isLast
 * 	This method is used to determine if this node is leaf
**/
	TreeItem.prototype.isLast = function()
	{
		return this.itemIndex == this.parentItem.childItems.length - 1;
	};




/**
 * @method public TreeItem.prototype.hasChildren
 * 	This method is used to determine if the folder has any child nodes
 * @return boolean
 *	true if the folder has children
 *
**/
	TreeItem.prototype.hasChildren = function() 
	{
		var childless = this.xmlNode.getAttribute("hasNoChildrenValues");
		//need to make sure eval is same in firefox and ie -> convert string to boolean
		if( (childless=="true") || (childless==true) )
			childless = true;
		else
			childless = false;
		return !childless;
	}



var odcTrees = [];  // global pointer to multiple odcTrees


//
// Public
// TreeCtrlError (constructor: description
//
function TreeCtrlError(d)
{
	this.description = d;
}

// Utility Function'
/**
 * @method private get_element
 * 	This is an utility function which is is used to get the html object based on the tag id
 * @param String s_id
 *	html tag id
**/
get_element = document.all ?
	function (s_id) { return document.all[s_id] } :
	function (s_id) { return document.getElementById(s_id) };

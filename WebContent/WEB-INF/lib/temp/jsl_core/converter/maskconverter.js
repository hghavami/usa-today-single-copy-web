//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003.  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//==================================================================


/**
 * @class public MaskConverter
 * @constructor MaskConverter
 * MaskConverter is the JavaScript implementation of the MaskConverter provided by the input assist. The MaskConverter converts a mask formatted
 * string to a raw string and vice versa. This version only supports # and ? as the mask characters.
 * @param String mask
 * 	The mask to be used for data to string and string to data conversion.
**/
function MaskConverter(/*String*/ mask){
	//only support # and ? in the mask for right now...add support for escape char \ and alphanumeric A if it is added to the server side converter
	this.mask = mask;
	//Regular expression to match all letters (upper and lower case) and numbers
	this.allPattern = /[a-zA-Z0-9]/;
	//Regular expression to match all letters (upper and lower case)
	this.alphaPattern = /[a-zA-Z]/;
	//Regular expression to match all numbers
	this.numPattern = /[0-9]/;

	/**
	 * @method 	public convertFromStringToData
	 * 	Method to convert from a raw string in the model to a string formatted with the mask
	 * @param String rawString
	 * 	The raw unformatted string from the model
	 * @return String
	 * 	The string formatted with the mask
	**/
	//this.convertFromStringToData = function(rawString)
   this.valueToString = function(dataString)
   {
		//no mask was defined..return the string as it is
		if(this.mask == 'undefined' || this.mask == null || this.mask == ""){
			return dataString;
		}
		else if(!dataString){
			return "";
		}
      //If dataString is a number, charAt() will cause an exception.
      var  rawString = new String(dataString);
		var formattedString = new String("");
		var j = 0;
		for(var i=0; i<this.mask.length; i++){
			var maskChar = this.mask.charAt(i);
			if(maskChar == "#"){
				if(this.numPattern.test(rawString.charAt(j))){
					formattedString += rawString.charAt(j);
					j++;
				}
				else{
					this.throwException();
				}
			}
			else if(maskChar == "?"){
				if(this.alphaPattern.test(rawString.charAt(j))){
					formattedString += rawString.charAt(j);
					j++;
				}
				else{
					this.throwException();
				}
			}
			//if the mask char is number or alphabet literal then the one in the raw string better match it and also increment the counter raw string
			else if (this.alphaPattern.test(maskChar) || this.numPattern.test(maskChar)){
				if(rawString.charAt(j) == maskChar){
					formattedString += maskChar;
					j++;
				}
				else{
					this.throwException();
				}

			}
			//if its a non alphanumeric char then just add it to the formatted string and dont increment the counter
			else{
				formattedString += maskChar;
			}
		}
		return formattedString;
	}

	/**
	 * @method 	public convertFromDataToString
	 * 	Method to convert from a mask formatted string to a raw string required in the model
	 * @param String formattedString
	 * 	The mask formatted string from the input control
	 * @return String
	 * 	The raw string to be stored in the model
	**/
	//this.convertFromDataToString = function(formattedString)
   this.stringToValue  = function(formattedString)
   {
		//if the formatted string is empty then we store null in the model
		if(!formattedString){
			return null;
		}
		//we assume that the formatted string matches the mask because the input helper takes care of this
		//this check may need to be changed if the escape char is allowed
		if(this.mask.length != formattedString.length){
			var Msg = NlsFormatMsg(mask_not_matched);
			Log.error("MaskConverter.convertFromDataToString()", Msg);
			throw new EObjectError(Msg);
		}
		else{
			//I think we can blindly remove anything other than an alphabet and number to get the raw string...there is no need to analyze the mask
			var rawString = new String("");
			for(var i=0; i<formattedString.length; i++){
				if(this.allPattern.test(formattedString.charAt(i))){
					rawString += formattedString.charAt(i);
				}
			}
			return rawString;
		}
	}

	this.throwException = function(){
		var Msg = NlsFormatMsg(mask_not_matched);
		Log.error("MaskConverter.convertFromStringToData()", Msg);
		throw new EObjectError(Msg);
	}
}


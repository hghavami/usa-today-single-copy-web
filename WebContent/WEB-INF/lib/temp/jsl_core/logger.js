//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003.  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//=================================================================


var Log = new logger();

/**
 * @class public logger
 * @constructor logger
 * logger is a client side logging utility which will open a new browser window and write different logging
 * message based on the user's logging level preference.
 *   The constructor takes no parameters and defaults the log level to -1.
**/
function logger(LogLevel)
 {
   this.logLevel = -1;

   this.NOLOG = -1;
   this.ERROR = 0;
   this.WARN = 1;
   this.INFO = 2;
   this.DEBUG = 3;

/**
* @method public setLogLevel
*   Method to set the logging level the user prefer<BR>
*   &nbsp;&nbsp;&nbsp;  loglevel=Log.NOLOG, debug window will not be open<BR>
*   &nbsp;&nbsp;&nbsp;  loglevel>=Log.ERROR, display error msg<BR>
*   &nbsp;&nbsp;&nbsp;  loglevel>=Log.WARN, display error, warn msg<BR>
*   &nbsp;&nbsp;&nbsp;  loglevel>=Log.INFO, display error, warn, info msg<BR>
*   &nbsp;&nbsp;&nbsp;  loglevel>=Log.DEGUG, display error, warn, info and debug msg<BR>
* @param String level
*   The level of logging message
**/

   this.setLogLevel = new Function('level', 'this.logLevel = level');

/**
* @method public getLogLevel
*    Method to get the logging level the user prefer
* @return int level
*   the lohgging level of logging message
**/
   this.getLogLevel = new Function('return this.logLevel');


   this.URL_PREFIX  = "";
   this.URL_POSTFIX = "";


/**
* @method public setURLRewriter
*   Method to setup relative url to the html file so that logging window can find the right path for its
* style sheet file.
* @param String Prefix
*   string which will be added to the front of the css file's path
* @param String Postfix
*   string which will be added to the end of the css file's path
**/
   this.setURLRewriter = function(Prefix, Postfix)
    {
      this.URL_PREFIX = Prefix ;
      this.URL_POSTFIX= Postfix;
    }

/**
* @method public debug
*    Method to write debug message to the logging window.
* @param String funcName
*    The name of the function where the Log.debug call is made
* @param String msg
*    The message which the user want to print out
**/

   this.debug = function(funcName, msg)
    {
      if(this.logLevel>=this.DEBUG)
       writeLog(funcName, msg, 0);
    }
/**
* @method public info
*   Method to write info message to the logging window.
* @param String funcName
*   The name of the function where the Log.info call is made
* @param String msg
*   The message which the user want to print out
**/
   this.info = function(funcName, msg)
    {
      if(this.logLevel>=this.INFO)
       writeLog(funcName, msg, 1);
    }
/**
* @method public warn
*   Method to write warn message to the logging window.
* @param String funcName
*   The name of the function where the Log.warn call is made
* @param String msg
*   The message which the user want to print out
**/

   this.warn = function(funcName, msg)
    {
      if(this.logLevel>=this.WARN)
       writeLog(funcName, msg, 2);
    }
/**
* @method public error
*    Method to write error message to the logging window.
* @param String	funcName
*    The name of the function where the Log.error call is made

* @param String	msg
*    The message which the user want to print out
**/

   this.error = function(funcName, msg)
    {
      if(this.logLevel>=this.ERROR)
       writeLog(funcName, msg, 3);
    }
/**
* @method public alert
*   Method to write debug|info|warn|error message to logger window and pop up alert message.
* @param String funcName
*   The name of the function where the Log.alert call is made
* @param String msg
*   The message which the user want to print out
* @param String msgCategory
*   It should be one of the following: "Log.debug"|"Log.info"|"Log.warn"|"Log.error", otherwise
*  pop up alert message which is not NLE since this utility is just for developer/support/internal use.)
**/

   this.alert = function(funcName, msg, msgCategory)
    {
      if(msgCategory!='undefined')
       {
         switch (msgCategory)
          {
            case 'Log.debug':  this.debug(funcName, msg); break;
            case 'Log.info' :  this.info (funcName, msg); break;
            case 'Log.warn' :  this.warn (funcName, msg); break;
            case 'Log.error':  this.error(funcName, msg); break;
            default : alert("Counldn't recognize the message category, please use 'Log.debug' or 'Log.info' or 'Log.warn' or 'Log.error'");
          }
       }
      var outputStr = funcName + " " +msg;
      alert(outputStr);
    }

/**
* @method public pause
*   Method to pause the procedure of the running code, pop up alert msg and set focus on logging window.
**/

   this.pause = function()
    {
      if(this.logLevel>-1)
       {
         logWindow.alert("Pause! Click 'OK' button to continue logging.");
       }
    }
/**
* @method public pageSeparation
*    Method to write page name to the logging window to indicate a new page is displayed.
* @param String pageName
*    Page name written to log window to indicate a new page is display
**/
   this.pageSeparation = function(pageName)
    {
      if(this.logLevel>-1)
       {
         createLogWin();
         logWindow.document.write('<br><br><hr><center><span class="pgspclr">'+pageName+'</span></center><br><br>');
       }
    }
/**
* @method public pageClose
*    Method to write the close body/html tag to the logging window
**/

   this.pageClose = function()
    {
      if(this.logLevel>-1)
       {
         createLogWin();
         logWindow.document.write('</BODY></HTML>');
       }
    }

/**
* @method private createLogWin
*   Method to open a logging window if there is not one opened.Otherwise use the existing one.
**/
   function createLogWin()
    {
      if(typeof logWindow == 'undefined' || logWindow.closed)
       {
         var winConfig='width=900,height=500,toolbar=no,location=no,directories=no,menubar=no,scrollbars=yes,resizable=yes,status=no';
         logWindow = window.open ("","loggingWin",winConfig);
         logWindow.document.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">');
         logWindow.document.write('<html><head><title>ODC Log Window</title>');
         logWindow.document.write('</head>');
         logWindow.document.write('<link rel="stylesheet" href="'+Log.URL_PREFIX+'jsl_css/logger.css'+Log.URL_POSTFIX+'">');
         logWindow.document.write('<body><pre>');
       }
    }

   var category = new Array('DEBUG ', 'INFO  ', 'WARN  ', 'ERROR ');

/**
* @method private writeLog
*    Method to write logging message to the logging window
* @param String funcName
*    The name of the function where the Log.alert call is made
* @param String msg
*    The message which the user want to print out
* @param int index
*    It indicate what kind of log message it is: debug-0, info-1, warn-2, error-3
**/
   function writeLog(funcName, msg, index)
    {
      createLogWin();
      var time = getTime();
      msg = msg.replace(/</g, "&lt;");
      msg = msg.replace(/>/g, "&gt;");
      var outputStr = category[index] + time + " "+ funcName + " "+ msg;
      logWindow.document.write('<span class="logclr'+index+'">');
      logWindow.document.write(outputStr);
      logWindow.document.write('</span><br>');
    }
 }

/**
* @method private getTime
*    Method to get the timestamp which will be printed out to the logging window.
**/
function getTime()
 {
   var time = new Date();

   var hour    = time.getHours       ();
   var min     = time.getMinutes     ();
   var second  = time.getSeconds     ();
   var msecond = time.getMilliseconds();
   var dd      = time.getDate        ();
   var mm      = time.getMonth       ()+1;
   var yy      = time.getFullYear    ();

   var msecondStr;
   if (msecond < 10)
    msecondStr = ":00"
   else if (msecond < 100)
    msecondStr = ":0";
   else
    msecondStr = ":";

   return "[" + yy + ((mm    < 10) ? "-0":"-") + mm
                   + ((dd    < 10) ? "-0":"-") + dd
                   + ((hour  < 10) ? "-0":"-") + hour
                   + ((min   < 10) ? ":0":":") + min
                   + ((second< 10) ? ":0":":") + second
                   + msecondStr + msecond + "]";
 }


/**
 * @class public TimeLogger
 * @constructor TimeLogger
 *    The TimeLogger class provides services to log timed events during a page's existence. For instance, you can time
 *  how long a page takes to load, or mark other events. Usage is as follows:<BR>
 *       <PR>
 *         var TL = new TimeLogger();
 *         // include all the script files
 *         TL.add("Load Header");
 *         // do things
 *         TL.add("Load Page");
 *         alert(TL.print(false));  // or document.writeln(TL.print(true));
 *       </PR>
 *    <BR>
 *    The result is an alert box with output like the following:<BR>
 *       <PR>
 *         Start Time: 0s; Accumulated time: 0s;
 *         Load Header: 0.18s; Accumulated time: 0.18s;
 *         Load Data: 0.361s; Accumulated time: 0.541s;
 *         Load TreeView: 4.797s; Accumulated time: 5.338s;
 *         Load Panels: 0s; Accumulated time: 5.338s;
 *         Load TabPanel: 2.313s; Accumulated time: 7.651s;
 *         Load Page: 0s; Accumulated time: 7.651s;
 *       </PR>
 *    <BR>
 *
 *    Times are in seconds. The first time is the time elapsed since the last event. The second timre is the time elapsed since
 *   the TimeLogger was created. Grammar is:<BR>
 *       <PR>
 *         $(label): $(TimeSinceLastEvent)s; SubTime: $(TimeSinceFirstEvent)s;
 *       </PR>
 *    <BR>
 *
 *    The constructor takes no parameters. It automatically starts the timer and records a "Start Time" event
**/
function TimeLogger()
 {
   this._StartTime = new Date();
   this._SeqTime   = this._StartTime;
   this._Logs      = new Array(10);
   this._Counter   = -1;
   this.previousTime = this._StartTime;  //rz
   this.accumulatedTime = 0;			// rz
   this.dbefore = 0;
   this.dafter = 0;
   this.methodTimes = new Array(10);
   this.stack = new Array(10);

   /**
    * @method public add
    *    adds a new timer marker event
    * @param String Label
    *    The text label for the timer event
   **/
   this.add = function(Label)
    {
      var d = new Date();
      this._Logs[++this._Counter] = [Label, d - this._StartTime, d - this._SeqTime];
      this._SeqTime = d;
    }

   this.add("Start Time");


   this.startAccumulate = function(method)
    {
	//alert("startAccumulate: " + method);
	this.dbefore = new Date();
	this.stack.push(this.dbefore);
    }

   this.endAccumulate = function(method)
    {
	//alert("endAccumulate: " + method);
	this.dbefore = this.stack.pop();
	if(this.methodTimes[method] == null)
	this.methodTimes[method] = 0;
	this.accumulatedTime = this.methodTimes[method];
	//this.accumulatedTime = 0;
	this.dafter = new Date();
      this.accumulatedTime += this.dafter - this.dbefore;
	this.methodTimes[method] = this.accumulatedTime;
    }


   /**
    * @method public getLogText
    *    adds a new timer marker event
    * @param boolean WebText
    *    Whether the test returned should contain "<BR>" (true) for newlines or "\n" (false)
    * @return String TimerLog
    *    A text fragment containing all the timer events so far, with one timer event, with matching times, per line. Grammar is:<BR>
    *      <PR>
    *       $(label): $(TimeSinceLastEvent)s; Accumulated time: $(TimeSinceFirstEvent)s;
    *      </PR>
   **/
   this.getLogText = function(WebText)
    {
      var Str = "";
      for (var i = 0; i <= this._Counter; ++i)
       {
         Str += this._Logs[i][0]+": "+(this._Logs[i][2]/1000.0)+"s; Accumulated time: "+(this._Logs[i][1]/1000.0)+"s"
              + (WebText == true ? "<BR>\n" : "\n");       }

	//alert("accumulatedTime: " + this.accumulatedTime/1000.0);
	for (method in this.methodTimes)
	{
		//alert("accumulatedTime for " + method + " : " + this.methodTimes[method]/1000.0);
		Str += "accumulatedTime for method " + method + " : " + this.methodTimes[method]/1000.0 + "<BR>\n";
	}

      return Str;

    }
 }

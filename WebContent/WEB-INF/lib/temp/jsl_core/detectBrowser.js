//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003.  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//==================================================================

/**
 * @method public detectBrowser
 * Detect if the browser the user is using is in the list:
 * IE 5.5+
 * Netscape 6+
 * Mozilla 1.0+
 * @return String
 *   user's browser info.
 **/

function detectBrowser()
{
	var browser = null;
	var version = null;

	var userAgent = navigator.userAgent;

	var appVersion = navigator.appVersion;

	if(navigator.appName == 'Microsoft Internet Explorer'){

		var tempArr = userAgent.split("MSIE");

		version = parseFloat(tempArr[1]);

		browser = "MSIE " + version;

		if(version<5.5){

			var args = new Array;
			args[0] = browser;
			alert(NlsFormatMsg(browser_not_support, args));
			return;
		}

	}else{

		var tempArr = userAgent.split("Mozilla/");
		var version = parseFloat(tempArr[1]);
		if(version<5.0){
			//for netscape under 6/7
			if(userAgent.indexOf("rv:")>-1){
				var tempArr = userAgent.split("rv:");
				version = parseFloat(tempArr[1]);
				browser = "Mozilla " + version;

			}else{
				browser = "Netscape " + parseFloat(appVersion);

			}

			var args = new Array;
			args[0] = browser;
			alert(NlsFormatMsg(browser_not_support, args));
			return;


		}else{

			var index = userAgent.indexOf("Netscape");

			if(index>0){
				//for netscape 6/7
					var tempArr = userAgent.split("Netscape/");
					version = parseFloat(tempArr[1]);
					browser = "Netscape " + version;

			}else{

				//for mozilla

				var tempArr = userAgent.split("rv:");
				version = parseFloat(tempArr[1]);
				browser = "Mozilla " + version


				if(version<1.0){
					//mozilla verison under 1.x
					var args = new Array;
					args[0] = browser;
					alert(NlsFormatMsg(browser_not_support, args));
					return;


				}

			}
		}
	}

	return browser;
}
/**
 * @method public isIE
 * Detect if the browser is IE
 * @return boolean
 *   if is ie, then true, otherwise false;
 **/

function isIE(){

	if(navigator.appName == 'Microsoft Internet Explorer'){
		return true;
	}else{
		return false;
	}


}
var ODCRegistry = new ODCRegistry();
/**
* @method public ElementsInfo
*   The function is used to create an element object which is registered to the registry array.
* @param String clientId
*	The client side ID of a control
* @param Object JSObject
*	The control object
**/

function ElementsInfo(clientId, JSObject)
 {
   this.clientId = clientId;
   this.JSObject = JSObject;
 }


/**
 * @class public ODCRegistry
 * @constructor ODCRegistry
 * ODCRegistry is a client side hash map to save all the needed information to find the right
 * client object when server page is push out to client side.
 *   The constructor takes no parameters
**/
function ODCRegistry()
 {
   this.Elements = new Array();
   this.Models = new Array();
   this.ModelNames = new Array();
   this.ModelsInForm = new Array();

   this.defaultClientViewID = new Array();

/**
* @method public addElementInfo
*   Method to register the control to the registry hash map
* @param String viewclientId
*   JSF view client ID
* @param String serverId
*   The server side id of the control
* @param String clientId
*   The client side id of the control, in portal case, server id is not equal to client side id which is a mangled id.
*	otherwise, server id is the same as client side id.
* @param Object JSObject
*   The control object itself.
*
**/
   this.addElementInfo = function(viewclientId,serverId, clientId, JSObject)
    {
		var key = (viewclientId!=null)?viewclientId+"_"+serverId:serverId;
		JSObject.pageId = serverId;

        this.Elements[key] = new ElementsInfo(clientId, JSObject);
    }



/**
* @method public addModelInfo
*   Method to register the model to the model's registry hash map
* @param String modelID
	either the first piece of information on a VBL or a modelname designated by a user, not clientId.
* @param Object modelObject
*   The model object itself.
* @param String formid
*	The id of the form where the model exists
*
**/
   this.addModelInfo = function(modelID, modelObject,formclientid)
   {

	   	this.Models[modelID] = modelObject;

		if(formclientid){
			if(this.ModelsInForm[formclientid]==null)
				this.ModelsInForm[formclientid] = new Array();

			var arr = this.ModelsInForm[formclientid];

			this.ModelsInForm[formclientid][arr.length] = new Array(modelID, modelObject);
		}
   }

	this.addModelName = function(vbl, modelName)
	{
		this.ModelNames[vbl] = modelName;
	}

/**
* @method private getModelById
*   Method to retrieve the model object the registry hash map
* @param String modelID - either the first piece of information on a VBL or a modelname designated by a user, not clientId.
* @return model object
*
**/
   this.getModelById = function(modelID)
   {
	   return this.Models[modelID];
   }


	this.getModelNames = function()
	{
		return this.ModelNames;
	}
/**
* @method private getElementById
*   Method to register the control to the registry hash map
* @param String viewclientId
*   JSF view client ID
* @param String serverId
*   The server side id of the control
* @param String type
*   type is used to determining what kind of type it will return.
* @return Object JSObject or String clientId
*   The control object itself or clientId
*
**/
   this.getElementById = function(serverId,type,viewclientId)
    {
	  var key = (viewclientId!=null&&viewclientId!='undefined')?viewclientId+"_"+serverId:serverId;
      var E = this.Elements[key];
      
      if(E == null)
      {
      	for(var ind =0; ind < this.defaultClientViewID.length; ind++)
      	{
    	       key = this.defaultClientViewID[ind]+"_"+serverId;
       
		       E = this.Elements[key];
	       
	       		if(E != null)
	       			break;
	       }
      }
      
      if (E == null)
       {
         return null;
       }
       var returnObj;
		switch (type) {
			case "0":
				returnObj = E.JSObject.Adapter;
				if((returnObj==null || returnObj=='undefined') && (E.JSObject!=null || E.JSObject!='undefined')){
					returnObj = E.JSObject;
				}
				break;

			case "1":
				returnObj = E.JSObject;
				break;

			case "2":
				returnObj = document.getElementById(E.clientId);
				break;

			case "3":
				returnObj = E.clientId;
				break;

			case "4":
				returnObj = E;
				break;


			default:
				break;
		}
      return returnObj;
    }
/**
* @method public getElementInfo
*   Method to get the value array based on the key from the registry hash map
* @param String viewclientId
*   JSF view client ID
* @param String serverId
*   The server side id of the control
**/
  this.getElementInfo = function(serverId,viewclientId)
  {

      return this.getElementById(serverId, "4",viewclientId);
  }
/**
* @method public getClientAdapter
*   Method to get the control adapter based on the key
* @param String viewclientId
*   JSF view client ID
* @param String serverId
*   The server side id of the control
* @return Object JSObject
*   The control adapter object
*
**/
  this.getClientAdapter = function(serverId,viewclientId)
  {
	  return this.getElementById(serverId, "0",viewclientId);
  }

/**
* @method public getClientAdapters
*   Method to get the control adapters based on the key array
* @param Array adapterArr
*   An array of key like [[serverId1, clientviewId1], [serverId2, clientviewId2]...]
* @return Array JSObjects
*   The control adapter objects
*
**/

  this.getClientAdapters = function(adapterArr)
  {
	  if(adapterArr){
		  var retAdapter = new Array();
		  for(var i=0; i<adapterArr.length; i++)
		  {
			  var serverId = adapterArr[i][0];
			  var clientviewId = adapterArr[i][1];
			  retAdapter[retAdapter.length] = this.getClientAdapter(serverId, clientviewId);

		  }
		  return retAdapter;
	  }else
	  	return null;

  }
/**
* @method public getClientControl
*   Method to get the control based on the key
* @param String viewclientId
*   JSF view client ID
* @param String serverId
*   The server side id of the control
* @return Object JSObject
*   The control object
*
**/
  this.getClientControl = function(serverId,viewclientId)
  {
	  return this.getElementById(serverId, "1",viewclientId);

  }
/**
* @method public getClientElement
*   Method to get the html element object based on the key
* @param String viewclientId
*   JSF view client ID
* @param String serverId
*   The server side id of the control
* @return Object JSObject
*   The html element object
*
**/
  this.getClientElement = function(serverId,viewclientId)
  {
	  return this.getElementById(serverId, "2",viewclientId);

  }
/**
* @method public getClientId
*   Method to get the control client side id
* @param String viewclientId
*   JSF view client ID
* @param String serverId
*   The server side id of the control
* @return String clientId
*   The client side id
*
**/
  this.getClientId = function(serverId,viewclientId)
  {
	  return this.getElementById(serverId, "3",viewclientId);

  }

/**
* @method public saveModelsToForm
*   Method to save model's diff information to form
* @param String formName
*   The name of the form
*
**/

  this.saveModelsToForm = function(FormName)
  {
	saveArrayOfThingsToForm(this.Models, "Diff", "GenerateDiffString", FormName);

  }

/**
* @method public saveControlsToForm
*   Method to save control's status information to form
* @param String formName
*   The name of the form
*
**/
   this.saveControlsToForm = function(FormName)
   {

      saveArrayOfThingsToForm(this.Elements, "UIS", "generateUIStateString", FormName);
   }


/**
* @method public saveAllToForm
*   Method to save model's diff and control's status information to form
* @param Object button
*   The html object of submit button
*
**/
   this.saveAllToForm = function(Form)
   {

//	  var Form = findForm(button);

		if(this.ModelsInForm[Form.id])

      		saveArrayOfThingsToForm(this.ModelsInForm, "Diff", "GenerateDiffString", Form, true);
		else
			saveArrayOfThingsToForm(this.Models, "Diff", "GenerateDiffString", Form, true);

      	saveArrayOfThingsToForm(this.Elements, "UIS", "generateUIStateString", Form, false);

   }


/**
* @method public addDefaultClientViewID
*   Method to save model's diff and control's status information to form
* @param string clientViewID
*   The runtime generated id of the view 
*
**/
 this.addDefaultClientViewID = function(clientViewID)
 {
	var num = this.defaultClientViewID.length + 1;

   this.defaultClientViewID[num] = clientViewID;

 }


}


 /** @method private saveArrayOfThingsToForm
       Takes an array of String/Object pairs and saves them to a Form using the String+ExtraName as the name of the input
      and and the result from Object.FuncName() as the value of the input.
     @param NameValue[] AOT
     @param String      ExtraName
     @param String      FuncName
     @param String      formid
     @exception EObjectError
       An exception will be thrown if formId is null or form is not found using formId.
 **/
 function saveArrayOfThingsToForm(AOT, ExtraName, FuncName, Form, modelFlag)
  {


     if(Form == null)
       {
          var args = new Array;
          var Msg = NlsFormatMsg(form_does_not_exist);
 	      Log.error("PageControl.saveArrayOfThingsToForm()", Msg);

 	      throw new EObjectError(Msg);
       }

    if(modelFlag){

		var arr = AOT[Form.id];

		if(arr!=null&&arr.length>0){
			//get model from form
			for(var i=0; i<arr.length;i++)
			{
				var key = arr[i][0];
				var obj = arr[i][1];
				var inputName = key+ExtraName;

         		var input = addHiddenField(inputName, Form);

				try{
					eval("input.value = obj."+FuncName+"();");

				}catch(e){

					//in case some controls don't have some functions specify here.
				}
			}
		}else{
			//get all the models from page

			for(var key in AOT){
				var inputName = key+ExtraName;
         		var input = addHiddenField(inputName, Form);

				try{
					eval("input.value = AOT[key]."+FuncName+"();");


				}catch(e){

					//in case some controls don't have some functions specify here.
				}
			}



		}
	}else{
		for(var key in AOT){
         	var clientId =  AOT[key].clientId;
         	inputName = clientId + ExtraName;

         	var input = addHiddenField(inputName, Form);

			try{
				if(AOT[key].JSObject!=null&&AOT[key].JSObject!='undefined')
					eval("input.value = AOT[key].JSObject."+FuncName+"();");
			}catch(e){
				//in case some controls don't have some functions specify here.
			}
		}

	}
	//alert("input.name" + input.name + "input.value" + input.value);


 }


 function addHiddenField(inputName, Form)
 {

			var input = Form[inputName];
			if(input == null)
			{
				input = document.createElement("input");
				input.type = "hidden";
				input.name = inputName;
				input.id = inputName;
				Form.appendChild(input);

			}
			return input;
 }

 /**
 * @method public executeWebService
 *       This function is used to execute web service
 * @param String viewclientId
 *   JSF view client ID
 * @param String serverId
 *   The server side id of the control
 **/
 function executeWebService(serverId, viewclientId)
 {
	var adapter = ODCRegistry.getClientControl(serverId, viewclientId);
	if(adapter!=null)
		adapter.Execute();
	return false;

}
/**
 * @class public ODCEvent
 * ODCEvent is a base class for creating an event object
 * @constructor String name
 *   The constructor takes 1 parameter, the name of the event.
**/
 function ODCEvent(name)
 {
    this.name=name;
 }


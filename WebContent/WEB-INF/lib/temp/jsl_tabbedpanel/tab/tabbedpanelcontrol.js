
//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//=================================================================


/**
 * @class public TabPanel
 * 	This class is to construct the tab, generate the navigation button and the tab button and
 * 	and the tab content.
 * @param Object parentElement
 *	The object which point to place where the tab will be put in the html page
 * @param boolean flagNextPrev
 *	flag to enable next/previous buttons
 * @param boolean flagTab
 *	flag to enable tab buttons
**/

function TabPanel( parentElement, flagNextPrev, flagTab, UrlPrefix)
{
  //this.IconLib = new tabIconLibrary(UrlPrefix);
  this.UrlPrefix = UrlPrefix;
  this.variableTabLength = false;
  this.tabIcons = new Array();

   // this.Name   = ''; //gt: removed: is a name really needed?
  this.height = "500";
  this.width  = "900"; // default values; directly changable by user


//  this.theme = 1; // default theme; can vary from 1-9
  this.initialPanelId = null; //the first displayed panel id.


  this.selectedTab = 1;
  this.selectedTabID = 0;
  this.containerDiv = parentElement;


  // flags
  this.flagNextPrev = flagNextPrev; // flag to enable next/previous buttons
  this.flagTabButtons = flagTab; // flag to enable tab buttons

  this.Panes = new Array();
  this.Panes[0] = "myTab";
  this.ButtonArray = new Array();
  this.ButtonArray[0] = "myButton";
  this.OriginPanes = new Array();

  //RATLC01082051 - new array for disabled Cancel and Finish buttons
  this.disableCancelButtonArray = null;
  this.disableFinishButtonArray = null;

  this.enableFinishButtonArray = null;
  this.onFinish = null;
  this.onCancel = null;
  this.styleClass = "tabbedPanel"; //two different tab can have different css.
 // this.OnFinish_Submit = false;

  this.enableFinish = false; //display finish button or not
  this.tabSize = null;
  this.tabHeight = -1;
  this.tabWidth = 1;


  this.startIndex = 1;
  this.maxTabWidth = -1;
  this.jsfFinishButton = null;
  this.jsfCancelButton = null;
  this.slantActiveLeft = 2;
  this.slantActiveRight = 2;
  this.slantInactiveLeft =2;
  this.slantInactiveRight = 2;
  this.loaded = false;
  this.scrollable = false;
  this.scrollableV = false;
  this.scrollableH = false;


  this.handlers = new Array();
	
//detect if the browser is IE5.5 so that don't display cursor style.
  var isodctpIE5 = false;
  var browser = detectBrowser();

  if(browser.substring(browser.indexOf("MSIE")+5)<6){
	isodctpIE5 = true;
  }


	
  this.type = "TabPanel";
  this.pageId = "tabpanel1";//default pageId which will be changed by ODCRegistry when do addElementInfo






  	// Methods

this.getDefaultSystemIcon = function(UrlPrefix)
{

	this.tabIcons["previous"]      = UrlPrefix+"jsl/tab/icons/previous.gif";       // previous image
    this.tabIcons["disprevious"] = UrlPrefix+"jsl/tab/icons/previous_disabled.gif";       // disable previous image
    this.tabIcons["next"]     = UrlPrefix+"jsl/tab/icons/next.gif";       // next image
    this.tabIcons["disnext"] = UrlPrefix+"jsl/tab/icons/next_disabled.gif";       // disable next image

 }

this.getDefaultSystemIcon(UrlPrefix);


  /**
  * @method public getType
  *   This function is return the type of the control
  **/
  	this.getType = function(){
  		return this.type;
  	}


  /**
  * @method public getId
  *   This function return the id of the control
  **/
  	this.getId = function(){
  		return this.pageId;
  	}




  this.setNipAndTuckStyle = function(al, ar, ial, iar)
  {
	    this.slantActiveLeft = (al&&al>0)?al:0;
	    this.slantActiveRight = (ar&&ar>0)?ar:0;
	    this.slantInactiveLeft =(ial&&ial>0)?ial:0;
	    this.slantInactiveRight = (iar&&iar>0)?iar:0;



  }




/**
* @method public addEvent
*   This function is used to add event handler to the control array so that all the event can be queued
* @param String action
*	The action which the tabpanel control has event handler associated, like "OnFinish", "OnEnter", "On Exit".
* @param String funcName
*   The function name of the user customized javascript
**/
	this.addHandler = function(action, funcName)
	{

//      this.handlers[action] = funcName;
		this.handlers[this.handlers.length] = new Array(action, funcName);

	}




/**
* @method public addFinishButton
*   This function is used to add the finish button to the tab
* @param String[] enableArray
*	  An array of strings denoting the panels where the finish button will be activated
* @param String FinishCallbackEventHandler
*	  The event handler which will be triggered when the finish button is clicked.
**/
 this.addFinishCancelButton = function(enableArray)
   {
	   if(enableArray!=null&&enableArray.length>0){
		    this.enableCancel = true;
			this.enableFinish = true;
			this.enableFinishButtonArray = enableArray;
		// 	this.onFinish = FinishCallbackEventHandler;
		}
   }

//RATLC01082051 - 2 new methods storing list of disabled finish and cancel buttons

/**
* @method public disableCancelButton
*   This function is used to store the array of panel ids that are to have their cancel button disabled
* @param String[] disableArray
*	  The array of panel ids where cancel button is to be disabled
**/
this.disableCancelButton = function(disableArray) {
	if(disableArray != null && disableArray.length > 0) {
		this.disableCancelButtonArray = disableArray;
	}
}
/**
* @method public disableFinishButton
*   This function is used to store the array of panel ids that are to have their finish button disabled
* @param String[] disableArray
*	  The array of panel ids where finish button is to be disabled
**/
this.disableFinishButton = function(disableArray) {
	if(disableArray != null && disableArray.length > 0) {
		this.disableFinishButtonArray = disableArray;
	}
}

/**
* @method public setFinishButtonId
*   This function is used to set the finish button in case the finish button is coming from outside of jsl code
* @param String finishid
*	  The id of the finish button
**/
 this.setFinishButtonId = function(finishId)
 {
	//this.jsfFinishButton = finishId;
	this.jsfFinishButton = document.getElementById(finishId);

 }

/**
* @method public setCancelButtonId
*   This function is used to set the cancel button in case the cancel button is coming from outside of jsl code
* @param String cancelid
*	  The id of the cancel button
**/
 this.setCancelButtonId = function(cancelId)
 {
	// this.jsfCancelButton = cancelId;
	 this.jsfCancelButton =document.getElementById(cancelId);



 }

 /**
 * @method public setBackButtonId
 *   This function is used to set the cancel button in case the cancel button is coming from outside of jsl code
 * @param String cancelid
 *	  The id of the cancel button
 **/
  this.setBackButtonId = function(backId)
  {
 	// this.jsfCancelButton = cancelId;
 	 this.jsfBackButton =document.getElementById(backId);



 }


 /**
* @method public setCancelButtonId
*   This function is used to set the cancel button in case the cancel button is coming from outside of jsl code
* @param String cancelid
*	  The id of the cancel button
**/
 this.setNextButtonId = function(nextId)
 {
	// this.jsfCancelButton = cancelId;
	 this.jsfNextButton =document.getElementById(nextId);



 }





 /**
* @method public setstyleClass
*   This function is used to set the style sheet prefix for this tab
* @param String prefix
*	  the prefix which is added before the style sheet name. Like "tab1", so the css name will be "tab1_jslTabBar".
**/
 this.setStyleClass = function(prefix)
 {

	 this.styleClass = prefix;

	 this.containerDiv.className = this.styleClass+"-classForTabSysIcons";

	 var icons = new iconLibrary(this.containerDiv,"list-style-image");
	 for(var key in icons){
		 if(icons[key].indexOf("http")>-1){
			this.tabIcons[key] = icons[key];
		 }else{
		 	this.tabIcons[key] = this.UrlPrefix + icons[key];
		}

	 }

 }








 this.displayPanel = function(panelId)
 {

	panelId = this.getClientId(panelId);

	
	for(var i=1; i < this.Panes.length; i++)
	{

		if ( this.Panes[i].divObj.DIVID == panelId )
		{

			this.paneSelected(panelId, null,true);


			//this.showPane(panelId, null,true);
			break;
		}

	}

 }


/**
* @method private OnFinishButtonClick
*   This function is used to hanlde finish button onclick event which use outside functor
* @param Object	e
*	window event object.
**/
this.OnFinishButtonClick = function(e) // private
{
  //  if(this.onFinish!=null)
  //   this.onFinish.handle();
  var finishButton = (isIE())?window.event.srcElement:e.target;
  var tabobj = finishButton.tabobj;
  var tabpanelForm =  findForm(finishButton);

	for(var i=0; i<tabobj.handlers.length;i++)
	{

		if(tabobj.handlers[i][0].toUpperCase() == "ONFINISH"){

		   var handler = tabobj.handlers[i][1];

		   var e = new ODCEvent("onFinish");
		   e.tabpanelForm=tabpanelForm;

		   if (eval(handler+".call(this,this, e);") == false)
		   {
			 return false;
	   	   }
	   	   break;
	   }


    }

	//Fenil suggest to remove this flag so that finish button always submit
	//if(tabobj.OnFinish_Submit)
	//{
	//	ODCRegistry.saveAllToForm(finishButton);
	//}else{
	//	return false;
	//}
}

/**
* @method private OnCancelButtonClick
*   This function is used to hanlde cancel button onclick event which use outside functor
* @param Object	e
*	window event object.
**/
this.OnCancelButtonClick = function(e) // private
{


  var cancelButton = (isIE())?window.event.srcElement:e.target;
  var tabobj = cancelButton.tabobj;
  var tabpanelForm =  findForm(cancelButton);

 	for(var i=0; i<tabobj.handlers.length;i++)
 	{

 		if(tabobj.handlers[i][0].toUpperCase() == "ONCANCEL"){

 		  var handler = tabobj.handlers[i][1];

		  var e = new ODCEvent("onCancel");
		  e.tabpanelForm=tabpanelForm;


		  if (eval(handler+".call(this,this, e);") == false)
		   {
			 return false;
	   	   }
	   	   break;
	   }

    }

  	return true;
}





/**
* @method public restoreUIState
* 	This function is used to restore the UI state of the tab based on a state string.
* @param String stateString
*    The div name which indicated that panel need to be opened, like "DG"
**/
this.restoreUIState = function(stateString)
{
	this.selectedTabID = stateString;
	if(stateString != null&&stateString!="")
		this.showPane(stateString, null,true);



}
/**
* @method public generateStateString
* 	This function is used to generate a state string based on div name of the shown panel.
*	like "DG".
**/
this.generateUIStateString = function()
{
	return this.selectedTabID;

}


/**
* @method private PreviousTab
*   This function is called to go the previous tab
**/

	this.PreviousTab= function()
	{
		if(this.startIndex==1){
			document.getElementById(this.containerDiv.id+this.styleClass+"tabnexticon").focus();
			return;
		}
		this.startIndex--;
		var tabTable = document.getElementById(this.containerDiv.id+this.styleClass+"tabtable");
		this.showHideTab(tabTable);

		try{
			document.getElementById(this.containerDiv.id+this.styleClass+"tabprevicon").focus();

		}catch(e){

		}

	}
/**
* @method private NextTab
*   This function is called to go the next tab
**/
	this.NextTab = function()
	{
		if(this.startIndex+this.tabSize > this.Panes.length-1){

			document.getElementById(this.containerDiv.id+this.styleClass+"tabprevicon").focus();
			return;
		}

		this.startIndex++;
		var tabTable = document.getElementById(this.containerDiv.id+this.styleClass+"tabtable");
		this.showHideTab(tabTable);

		try{
			document.getElementById(this.containerDiv.id+this.styleClass+"tabnexticon").focus();
		}catch(e){

		}

	}


	/**
	* @method public addCaseFromDIV
	*   This function is used to add a panel to the tab from the buffer
	* @param int ID
	*	Unique ID to reference each pane
	* @param String name
	*	panel's name
	* @param String DIVID
	*	Id of the Div tag to be used for this pane rendering
	* @param String NXTID
	* 	Next Page ID.. could be null
	* @param String PRVID
	*	Previous Pane ID. could be null
	* @param String OnEnter
	* 	Local Event to be triggered before pane selected.
	* @param String OnExit
	* 	Local Event to be triggered when pane selected.
	**/
	this.addCaseFromDIV = function(ID, name, DIVID, NXTID, PRVID, OnEnter, OnExit) {

		var ID = ID; // Unique ID to reference each pane
		var name = name; // Display name
		var DIVID = DIVID; // Id of the Div tag to be used for this pane rendering
		var NXTID = NXTID; // Next Page ID.. could be null
		var PRVID = PRVID; // Previous Pane ID.. could be NULL
		//var OnExit = OnExit; //Local Event to be triggered when pane selected.

		var divObj;

		try {

			divObj= document.getElementById(DIVID);


		} catch(e) {

			divObj = null;
		}
			this.processDivObject(ID, name, divObj, NXTID, PRVID, OnEnter, OnExit)
	}
	/**
	* @method private processDivObject
	*   This function is used to add a panel to the tab from the buffer
	* @param int ID
	*	Unique ID to reference each pane
	* @param String name
	*	panel's name
	* @param Object divObj
	*	The Div object which will be rendered on the tab panel
	* @param String NXTID
	* 	Next Page ID.. could be null
	* @param String PRVID
	*	Previous Pane ID. could be null
	* @param String OnEnter
	* 	Local Event to be triggered before pane selected.
	* @param String OnExit
	* 	Local Event to be triggered when pane selected.
	**/
	this.processDivObject = function(ID, name, divObj, NXTID, PRVID, OnEnter, OnExit) {


		var tabNo = this.Panes.length;

		var pane = new Pane();

		divObj.style.visibility="visible";

		divObj.ID = ID; // Unique ID to reference each pane
		divObj.name = name; // Display name
		divObj.DIVID = divObj.getAttribute("id"); // Id of the Div tag to be used for this pane rendering
		if (NXTID == null) {
			divObj.NXTID = "undefined";
		} else {
			divObj.NXTID = NXTID; // Next Page ID.. could be null
		}
		if (PRVID == null) {
			divObj.PRVID = "undefined";
		} else {
			divObj.PRVID = PRVID; // Previous Pane ID.. could be NULL
		}

		divObj.handlers = new Array;
		if(OnEnter!=null&& OnEnter!='undefined')
		{
			divObj.handlers["onEnter"] = OnEnter;

		}
		if(OnExit!=null&&OnExit!='undefined')
		{
			divObj.handlers["onExit"] = OnExit;

		}

				// Figure out how we size the panels -- proportionately or absolutely
				var sWidth = this.width+"";
				var sHeight= this.height+"";
		//		if (sWidth.indexOf("%") >= 0 || sHeight.indexOf("%") >= 0) {
		//			divObj.style.width = "100%";
		//			divObj.style.height = "100%";
		//		} else {
		//			// Following is provisional, it will get fixed by "reDraw" as best we can
		//			divObj.style.width = "10px";
		//			divObj.style.height = "10px";
		//			divObj.style.overflow = "auto";
		//			this.scrollable = true;
		//		}
				if (sWidth.indexOf("%") >= 0) {
					divObj.style.width = "100%";
				} else {
					// Following is provisional, it will get fixed by "reDraw" as best we can
					divObj.style.width = "10px";
					divObj.style.overflow = "auto";
					this.scrollableH = true;
				}
				if (sHeight.indexOf("%") >= 0) {
					divObj.style.height = "100%";
				} else {
					// Following is provisional, it will get fixed by "reDraw" as best we can
					divObj.style.height = "10px";
					divObj.style.overflow = "auto";
					this.scrollableV = true;
		}


		// store the divObject
		pane.divObj = divObj;
		pane.Name = name;

		this.Panes[tabNo] = pane;
		this.OriginPanes[this.OriginPanes.length]  = pane;

	}


	/**
	* @method private redraw
	*   This function is used to redraw tabpanel in IE and portal environment.
	**/
	this.redraw = function()
	{

		while (this.containerDiv.hasChildNodes())
			this.containerDiv.removeChild(this.containerDiv.lastChild);

		this.updateControl(true);

	}





	/**
	* @method public setHeight
	*   This function is used to set the tabpanel's height.
	* @param string height
	**/

	this.setHeight = function(height)
	{
			this.height = height;


	}



	/**
	* @method public setVariableTabLength
	*   This function is used to set the tabpanel's variableTabLength flag.
	* @param boolean variableTabLength
	**/

	this.setVariableTabLength = function(variableTabLength)
	{

	   this.variableTabLength = variableTabLength;
	}


	/**
	* @method public setWidth
	*   This function is used to set the tabpanel's width.
	* @param string width
	**/

	this.setWidth = function(width)
	{

			this.width = width;


	}

	this.getClientId = function(serverId)
	{
		return this.containerDiv.id+":"+serverId;

	}



	/**
	* @method public show
	*   This function integrate all the different parts and display the whole final tab panel.
	* 	first, if tab has buttons, then need to create the tab buttons.
	* 	second, add all the panels to the tab.
	* 	third, if tab has navigation buttons, then need to construct the navigation buttons.(previous, next, finish)
	**/
	this.show = function()
	{
		this.updateControl();

	}




	/**
	* @method private updateControl
	*   This function integrate all the different parts and display the whole final tab panel.
	* 	first, if tab has buttons, then need to create the tab buttons.
	* 	second, add all the panels to the tab.
	* 	third, if tab has navigation buttons, then need to construct the navigation buttons.(previous, next, finish)
	**/
	this.updateControl = function(ifredraw)
	{
		if(this.OriginPanes.length <= this.tabSize) this.tabSize = -1; //so that if the tabsize is equal or great than the tabs, then don't show next/previous icons.

		if(!ifredraw){
			//call onInitialPanelShow event

			var newinitalId = null;
			for(var i=0; i<this.handlers.length;i++)
			{

				if(this.handlers[i][0].toUpperCase() == "ONINITIALPANELSHOW"){

					 var handler = this.handlers[i][1];

					 var e = new ODCEvent("onInitialPanelShow");
					 e.initialPanelId = this.initialPanelId;

					 try{

						newinitalId = eval(handler+".call(this,this, e);");
						if(newinitalId == false)
							this.initialPanelId = null;
					 }catch(ex){
						return;
					 }


				}

			}

			if(newinitalId&&!isBoolean(newinitalId)) this.initialPanelId = this.getClientId(newinitalId);

			if(this.initialPanelId){

				//convert the initialPanelId to the index of panel id
				for(var i=1; i<this.Panes.length; i++)
				{
					if(this.Panes[i].divObj.DIVID == this.initialPanelId){
						this.selectedTabID = this.initialPanelId;
						this.selectedTab = i;
						if(this.selectedTab-this.tabSize+1>0)
							this.startIndex = this.selectedTab-this.tabSize+1;
						else
							this.startIndex = 1;

						break;

					}

				}
			}
		}

		var outDivTable = document.createElement("table");
		outDivTable.className = this.styleClass;
		outDivTable.border = "0";
		outDivTable.cellSpacing = "0";
		outDivTable.cellPadding = "0";
		outDivTable.width = this.width;
		outDivTable.height = this.height;


		var tr1 = outDivTable.insertRow(0);
		var td1 = tr1.insertCell(0);

		var tr2 = outDivTable.insertRow(1);
		var td2 = tr2.insertCell(0);
		td2.width = "100%";
		td2.height = "100%";
		td2.vAlign = "top";
		td2.className = this.styleClass+"-Body";


		var tr3 = outDivTable.insertRow(2);
		var td3 = tr3.insertCell(0);
		if (this.flagNextPrev||this.enableFinish){
			td3.width = "100%";
			td3.vAlign = "top";
			td3.className = this.styleClass+"-Footer";
		} else {
			tr3.style.display = "none";
		}


		this.containerDiv.appendChild(outDivTable);

		var tabwidth = buttonwidth=0;

		// Make some tabs in the first row
		if (!this.flagTabButtons) {
			tr1.style.display = "none";
		} else {
			tr1.style.display = "";
			td1.vAlign = "top";
			td1.className = this.styleClass+"-Header";

			var tempTable = document.createElement("table");

			td1.appendChild(tempTable);


			this.createTab(tempTable);

			this.showHideTab(tempTable);

			var maxWidth = 0;
			//detect if the tab table width greater than the body with.
			if(!isNaN(this.width)){

				maxWidth = Math.max(this.width, td1.offsetWidth);
			}


		}


		if(this.flagNextPrev||this.enableFinish){
			this.createButton(td3);
			if(!isNaN(this.width))
				maxWidth = Math.max(this.width, td3.offsetWidth)
		}

		this.createBody(td1, td2, td3);

		if(!isNaN(this.width)&&maxWidth>this.width){
			this.width = maxWidth;
			this.adjustbody();
		}


		if(isIE())
		{
		   var tabcontrol = this;
		   window.attachEvent('onload',function() {tabcontrol.redraw();});
		}
	}

	/**
	* @method private adjustbody
	*   This function is used to adjust body div size in case the button or tab size is greater than
	*	body size.
	**/


	this.adjustbody = function()
	{
			for(var i=1; i < this.Panes.length; i++) {

					var divObj = this.Panes[i].divObj;
					divObj.style.width  = this.width-10;
			}


	}
	/**
	* @method private createButton
	*   This function is used to create buttons like "back", "next", "finish", "cancel". If jsf button
	* 	is not supplied, then it automatically create such buttons.
	* @param Object td3
	*	The html cell object where all the buttons will reside.
	**/


	this.createButton = function(td3)
	{


			var divObj = this.Panes[this.selectedTab].divObj;

			var tabTable  = document.createElement("table");
			tabTable.border = 0;
			tabTable.cellpadding = 0;
			// NOTE:  Netscape only works correctly if we explicitly set ALIGN here
			// so you won't be able to override it in the stylesheet
			tabTable.align = "right";

			var tabRow    = tabTable.insertRow(0);


			var backButton = nextButton = finishButton = cancelButton = null;


		// 'back'&'next' button
				if(this.jsfBackButton!=null && this.jsfNextButton!=null){
					backButton = this.jsfBackButton;
					nextButton = this.jsfNextButton;

					var backVal = backButton.value;

					if(backVal==null||backVal=="")
						backVal = NlsFormatMsg(label_tab_previous,null);

					var nextVal = nextButton.value;
					if(nextVal==null||nextVal=="")
						nextVal = NlsFormatMsg(label_tab_next,null);


					if(backVal.indexOf("&lt;")!=-1)
					{
						backVal = backVal.replace("&lt;","<");
					}


					if(nextVal.indexOf("&gt;")!=-1)
					{
						nextVal = nextVal.replace("&gt;", ">");

					}

					backButton.value = backVal;
					nextButton.value = nextVal;

					if(this.flagNextPrev){
						nextButton.style.display = "inline";
						backButton.style.display = "inline";
					}

				}else{


					backButton = document.createElement("INPUT");
					backButton.type = 'submit';
					backButton.value = NlsFormatMsg(label_tab_previous,null);

					nextButton = document.createElement("INPUT");
					nextButton.type = 'submit';
					nextButton.value = NlsFormatMsg(label_tab_next,null);

				}


			// 'previous' button
			var tabCell  = tabRow.insertCell(0);
			backButton.controlName = this.Name;
			backButton.id = this.containerDiv.id+this.cssPrefix+"tabprevbutton";

			backButton.onclick = this.onTabSelected;
			this.previousButton = backButton;
			tabCell.appendChild(backButton);

			// 'next' button
			tabCell  = tabRow.insertCell(1);

			nextButton.controlName = this.Name;
			nextButton.id = this.containerDiv.id+this.cssPrefix+"tabnextbutton";

			nextButton.onclick = this.onTabSelected;
			this.nextButton = nextButton;

			tabCell.appendChild(nextButton);
			tabCell  = tabRow.insertCell(2);
			var cancelButton = finishButton = null;
			if(this.enableFinish){



				// 'finish'&'cancel' button
				if(this.jsfCancelButton!=null && this.jsfFinishButton!=null){
					cancelButton = this.jsfCancelButton;
					finishButton = this.jsfFinishButton;


					if(cancelButton.value==null||cancelButton.value=="")
						cancelButton.value = NlsFormatMsg(label_tab_cancel,null);

					if(finishButton.value==null||finishButton.value=="")
						finishButton.value = NlsFormatMsg(label_tab_finish,null);

					cancelButton.style.display = "inline";
					finishButton.style.display = "inline";
				}else{






					cancelButton = document.createElement("INPUT");
					cancelButton.type = 'submit';
					cancelButton.value = NlsFormatMsg(label_tab_cancel,null);

					finishButton = document.createElement("INPUT");
					finishButton.type = 'submit';
					finishButton.value = NlsFormatMsg(label_tab_finish,null);

				//	cancelButton.style.display = "none";
				//	finishButton.style.display = "none";



				}


				cancelButton.controlName = this.Name;
				cancelButton.id = this.containerDiv.id+this.cssPrefix+"tabcancelbutton";

				cancelButton.onCancel = this.onCancel;
				cancelButton.tabobj = this;

				cancelButton.onclick= this.OnCancelButtonClick;


				this.cancelButton = cancelButton;



				finishButton.controlName = this.Name;
				finishButton.id = this.containerDiv.id+this.cssPrefix+"tabfinishbutton";


				finishButton.onFinish = this.onFinish;
				finishButton.tabobj = this;

				finishButton.onclick= this.OnFinishButtonClick;


				this.finishButton = finishButton;
				tabCell.appendChild(finishButton);
				tabCell  = tabRow.insertCell(3);
				tabCell.appendChild(cancelButton);


			}

			this.setButtonIds();


		    td3.appendChild(tabTable);




	}
	/**
	* @method private createBody
	*   This function is used to create content layer
	* @param Object td2
	*	The html cell object where all contents will reside.
	**/
		this.createBody = function(td1,td2,td3){



		// First make sure all the divs are part of the table
		// and only one is shown.
		for(var i=1; i < this.Panes.length; i++) {

			td2.appendChild(this.Panes[i].divObj);

			if ( i == this.selectedTab ) {
				this.Panes[i].divObj.style.display = "";
				this.selectedTabID = this.Panes[i].divObj.DIVID;

			} else {
				this.Panes[i].divObj.style.display = "none";
			}
		}

		// If we're trying to make the div scrollable, we need to size it
		if (this.scrollableV || this.scrollableH) {
			var availableWidth = this.width;
			var availableHeight = this.height;
			if (this.flagTabButtons)
				availableHeight = availableHeight - td1.offsetHeight;
			if (this.flagNextPrev||this.enableFinish)
				availableHeight = availableHeight - td3.offsetHeight;
			// We really should rip through the left/right top/bottom borders, margins, pads
			// etc. to know exactly how much space is above/below/left/right of the div
			// but that's prohibitively expensive to do.  So we'll just "assume" it's
			// centered.
			var offsetV = this.Panes[this.selectedTab].divObj.offsetTop;
			var offsetH = this.Panes[this.selectedTab].divObj.offsetLeft;
			
			
			// decfahey: While the leftoffset works well in left to right
			// layout it breaks the rendering in right to left (bidi) encoding
			// so if the encoding is rtl, we skip changing the available width
			if (document.documentElement.dir != "rtl") {
				availableWidth -= (2*offsetH);
			}
			availableHeight -= (2*offsetV);
			
			availableWidth = Math.max(1,availableWidth);
			availableHeight= Math.max(1,availableHeight);
			for(var i=1; i < this.Panes.length; i++) {
				if (this.scrollableH)
					this.Panes[i].divObj.style.width = availableWidth + "px";
				if (this.scrollableV)
					this.Panes[i].divObj.style.height = availableHeight + "px";
			}
		}
	}
	/**
	* @method private showHideTab
	*   This function is to decide which tab will be show or not
	* @param Object tabTable
	*	The html table object where all tabs will reside.
	**/

	this.showHideTab = function(tabTable)
	{
		if(!this.flagTabButtons)
			return;

		var tr = tabTable.rows[0];

		if(this.tabSize>0){
			start = this.startIndex;
			end = (this.startIndex+this.tabSize<this.Panes.length)?this.startIndex+this.tabSize:this.Panes.length;

		}else{
			start = 1;
			end = this.Panes.length;
		}


		var activestyle = this.styleClass+"-TabActive";
		var inactivestyle = this.styleClass+"-TabInactive";
		var linkactivestyle = this.styleClass+"-HyperActive";
		var linkinactivestyle = this.styleClass+"-HyperInactive";
		var i=1;
		for(i; i<this.Panes.length; i++)
		{
			var tempCell = tr.cells[i-1].firstChild.rows[0].cells[0];
			var td = tr.cells[i-1];

			if(td&&tempCell){
				if(i>=start&&i<=end-1)
				{
					td.style.display ="";
					if(i==this.selectedTab){
						if(tempCell.className!=this.styleClass+"-TabActive")
							this.styleTabHTML(this.Panes[i].divObj, td, false, activestyle, linkactivestyle, this.slantActiveLeft, this.slantActiveRight, true, i);

					}else{
						if(tempCell.className!=this.styleClass+"-TabInactive")
							this.styleTabHTML(this.Panes[i].divObj, td, false, inactivestyle, linkinactivestyle, this.slantInactiveLeft, this.slantInactiveRight, true, i);
					}
				}else{
					td.style.display ="none";

				}
			}


		}

		//adjust prev and next icon
		var previcon = document.getElementById(this.containerDiv.id+this.styleClass+"previcon");
		if(previcon){
				previcon.border = 0;


				if(this.startIndex == 1){
					previcon.setAttribute("src", this.tabIcons["disprevious"]);
					if(previcon.parentNode.type)
					{
						var td = previcon.parentNode.parentNode;
						td.removeChild(td.firstChild);
						td.appendChild(previcon);

					}
				}else{

					previcon.setAttribute("src", this.tabIcons["previous"]);

					var a1 = document.createElement("a");
					a1.type = "hlink";
					a1.id = this.containerDiv.id+this.styleClass+"tabprevicon";
					var tableid = this.containerDiv.id+this.styleClass+"tabtable";
					var code = "window.document.getElementById('" + tableid + "').tabControl.PreviousTab()";
					a1.setAttribute("href","javascript:" + code);

					a1.appendChild(previcon);
					tr.cells[i-1].appendChild(a1);


			}
		}

		var nexticon = document.getElementById(this.containerDiv.id+this.styleClass+"nexticon");
		if(nexticon)
		{
				nexticon.border = 0;
				if(this.startIndex+this.tabSize > this.Panes.length-1){
					nexticon.setAttribute("src", this.tabIcons["disnext"]);
					if(nexticon.parentNode.type)
					{
						var td = nexticon.parentNode.parentNode;
						td.removeChild(td.firstChild);
						td.appendChild(nexticon);
					}

				}else{

					nexticon.setAttribute("src", this.tabIcons["next"]);
					nexticon.setAttribute("border", 0);

					var a2 = document.createElement("a");
					a2.type = "hlink";
					a2.id = this.containerDiv.id+this.styleClass+"tabnexticon";
					var tableid = this.containerDiv.id+this.styleClass+"tabtable";
					var code = "window.document.getElementById('" + tableid + "').tabControl.NextTab()";
					a2.setAttribute("href","javascript:" + code);

					a2.appendChild(nexticon);
					tr.cells[i].appendChild(a2);


				}

		}

	}



	/**
	* @method private createTab
	*   This function is to create all the tabs
	* @param Object tabTable
	*	The html table object where all tabs will reside.
	**/


	this.createTab = function(tabTable)
	{


			tabTable.id = this.containerDiv.id+this.styleClass+"tabtable";
			tabTable.tabControl = this;

			tabTable.border = 0;
			tabTable.cellSpacing = 0;
			tabTable.cellPadding = 0;

			var tabRow = tabTable.insertRow(0);

			var i=1;
			for(i; i < this.Panes.length; i++) {

			  if(this.Panes[i].divObj!=null)
			  {


					var tabsetCell = tabRow.insertCell(i-1);

					var select = (this.selectedTab ==i)?true:false;

					var tabclass = this.styleClass+"-TabActive";
					var linkactivestyle = this.styleClass+"-HyperActive";

					this.emitTabHTML(tabsetCell,this.Panes[i].divObj,i);

					firstTab = (i==1)?true:false;

					var width = this.styleTabHTML (this.Panes[i].divObj, tabsetCell, firstTab, tabclass, linkactivestyle,this.slantActiveLeft, this.slantActiveRight, true,i);

					this.maxTabWidth = Math.max(this.maxTabWidth, width);


				}

			}



			//add previous and next icons.
			if(this.tabSize>0){

				var tabsetCell = tabRow.insertCell(i-1);
				var	imgprevious = document.createElement("img");
				imgprevious.style.marginLeft = "6px";
				imgprevious.id = this.containerDiv.id+this.styleClass+"previcon";

				imgprevious.setAttribute("src", this.tabIcons["disprevious"]);


				tabsetCell.appendChild(imgprevious);

				tabsetCell = tabRow.insertCell(i);

				var imgnext = document.createElement("img");
				imgnext.id = this.containerDiv.id+this.styleClass+"nexticon";

				imgnext.setAttribute("src", this.tabIcons["disnext"]);

				tabsetCell.appendChild(imgnext);



			}


			if(this.variableTabLength=="false")
			{

				for(var j=1; j<this.Panes.length;j++){
					if(this.maxTabWidth>0)
						tabRow.cells[j-1].firstChild.width = this.maxTabWidth;
				}
			}



	}



	/**
	* @method public onTabSelected
	*   The call back function is used to find out what pane to show, and hide
	* @param Event evt
	*	window event object.
	**/
	this.onTabSelected = function(evt) {

		evt = (evt) ? evt : ((event) ? event: null);
		if (evt && evt.type=="click" || (evt.type=="keydown" && (evt.keyCode==32 || evt.keyCode==13))) {

			var src = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);

			if (src) {

				while(!src.name){
					src = src.parentNode;
				}
				var reqTab = src.name;
				var ctrlName = src.controlName;
				var reqType = src.type;

				var obj     = eval(ctrlName);

				if(obj!=null)
					obj.paneSelected(reqTab, obj.selectedTabID, reqType);

				//if(isIE()){
					try{

						var objfocus = document.getElementById(src.id);


						if(objfocus && objfocus.nodeType == 1)
						{

							if(objfocus.nodeName == "LABEL"){
								objfocus = objfocus.parentNode;


							}else if(objfocus.nodeName == "TD"){
								objfocus = objfocus.firstChild;
							}

							objfocus.focus();

						}
					}catch(e)
					{
					}
				//}
			}
			 return false;
		}
		return;
	}



	/**
	* @method private paneSelected
	*   The function is called by the onTabSeleted event
	* @param String show
	*	The panel's name which will be shown
	* @param String hide
	*	The panel's name which will be hidden
	* @param Object tabControl
	*	The tab control
	* @param String  type
	*	the type of the request object.
	**/
	this.paneSelected = function(show, hide, type) {
		var previousSelectPanel = this.selectedTabID;

		var divObj = null;



		var nextId = null;
		// local callback

		if(show == previousSelectPanel) return;

			for(var k=1; k < this.Panes.length; k++) {
				if(this.Panes[k].divObj.DIVID == previousSelectPanel)
				{
					//call the onExit event if specified.

					divObj = this.Panes[k].divObj;

					if(divObj.handlers["onExit"]!=null){

						 var e = new ODCEvent("onExit");
						 e.tobeExitPanel=previousSelectPanel;
						 e.defaultTobeEnterPanel = show;
						 try{

							nextId = eval(divObj.handlers["onExit"]+".call(this,this, e);");
							if(nextId&&!isBoolean(nextId)) nextId = this.getClientId(nextId);

							//expect three possible return, false, then stop process. true, use default next panel id. string panelid, then go to that panel.
							if(nextId == false)
								return; //stop processing the event;

						}catch(ex){
							return;
						}

					}



				}
			}


			//global callback
			for(var i=0; i<this.handlers.length;i++)
			{

				if(this.handlers[i][0].toUpperCase() == "ONPANELEXIT"){

					 var handler = this.handlers[i][1];

					 var e = new ODCEvent("onPanelExit");
					 e.tobeExitPanel=previousSelectPanel;
					 e.defaultTobeEnterPanel = show;

					 try{
						if(eval(handler+".call(this,this, e);") == false)
							return; //stop processing the event;
					}catch(ex){
						return;
					}
					break;
				}

			}


		if(isBoolean(nextId)||nextId==null||type!="submit") nextId = show;

		var bNextTabExit = false;

		for(var k=1; k < this.Panes.length; k++) {



			if ( this.Panes[k].divObj.DIVID == nextId)
			{


				divObj = this.Panes[k].divObj;
				bNextTabExit = true;

				//if pane has an OnEnter method associated with it, then it will be called here to check and
				//  see if any preprocessing needs to be done; if it returns true, then we continue, else, we return to
				//  the current pane.

				if(divObj.handlers["onEnter"]!=null){

					 var e = new ODCEvent("onEnter");
					 e.tobeEnterPanel = nextId;

					 try{
						if(eval(divObj.handlers["onEnter"]+".call(this,this, e);") == false)
							return; //stop processing the event;
					}catch(ex){
						return;
					}

				}


			//	break;

			}
		}
		// global callback
		for(var i=0; i<this.handlers.length;i++)
		{

			if(this.handlers[i][0].toUpperCase() == "ONPANELENTER"){

				 var handler = this.handlers[i][1];

				 var e = new ODCEvent("onPanelEnter");
				 e.tobeEnterPanel = nextId;

				 try{
					if(eval(handler+".call(this,this, e);") == false)
						return; //stop processing the event;
				}catch(ex){
					return;
				}
				break;
			}

		}
		if(!bNextTabExit) return; //in case user pass in an invalid tab id.


	//in case user want to jump to random selected panel instead of the default one.
		if(show!=nextId)
			this.showPane(nextId, hide, true);
		else
			this.showPane(nextId, hide, false);

//		tabControl.selectedTabID = nextId;



	}

	/**
	* @method public hideTab
	*   The function is used to hide the panel based on the passed in panel id.
	* @param String showID
	*	The panel's id which will be removed from panes
	**/

	this.hideTab = function(hideID)
	{
		if(hideID ==null||hideID == 'undefined') return;

		var foundId = -1;

		for(var i=1; i<this.Panes.length; i++)
		{


			if(hideID ==this.Panes[i].divObj.DIVID){
				if(hideID == this.selectedTabID)
					this.selectedTab= (i==1)?1:i-1;
				else if(this.selectedTab >i)
					this.selectedTab--;

				if(this.startIndex >i)
					this.startIndex--;

				foundId = i;

				break;
			}

		}
		if(foundId!=-1){
			 for (foundId; foundId<this.Panes.length; foundId++) {
					this.Panes[foundId] = this.Panes[foundId + 1];
			 }
			 this.Panes.length=this.Panes.length-1;

			 this.redraw();
			 /*

			while (this.containerDiv.hasChildNodes())
				this.containerDiv.removeChild(this.containerDiv.lastChild);

			this.updateControl();
			*/

		}


	}

	/**
	* @method public showTab
	*   The function is used to show the panel based on the passed in panel id.
	* @param String showID
	*	The panel's id which will be added into panes
	**/
	this.showTab = function(showID)
	{

		if(showID ==null||showID == 'undefined') return;
		var finalArr = new Array();
		finalArr[0] = "myTab";
		var found = false;
		for(var i=0; i<this.OriginPanes.length; i++)
		{
			var divid = this.OriginPanes[i].divObj.DIVID;

			if(showID == divid){
				finalArr[finalArr.length] = this.OriginPanes[i];

				if(this.selectedTab>=finalArr.length-1)
					this.selectedTab++;
				if(this.startIndex>=finalArr.length-1){

					this.startIndex++;

				}
				found = true;

			}else{
				for(var j=1; j<this.Panes.length; j++)
				{

					if(divid == this.Panes[j].divObj.DIVID)
					{
						finalArr[finalArr.length] = this.Panes[j];
						break;
					}
				}

			}

		}
		if(found){
			this.Panes = finalArr;

			/*

			while (this.containerDiv.hasChildNodes())
				this.containerDiv.removeChild(this.containerDiv.lastChild);

			this.updateControl();
			*/
			this.redraw();
		}




	}




	/**
	* @method private showPane
	*   The function is used to determine which panel is shown or hidden
	* @param String showID
	*	The panel's id which will be shown
	* @param String hideID
	*	The panel's id which will be hidden
	* @param boolean jump
	*	indicate the displayed panel is the default next one or random selected one.
	**/
	this.showPane = function(showID, hideID,jump)
	{
		var redrawPaneContents = false;
		var navDirection = "next";
		for(var i=1; i < this.Panes.length; i++)
		{

			if ( this.Panes[i].divObj.DIVID == showID )
			{
				this.Panes[i].divObj.style.display = "";
				currentDiv = this.Panes[i].divObj;
				this.selectedTab = i;
				redrawPaneContents = true;

			}
			else
			{
				this.Panes[i].divObj.style.display = "none";
			}
		}

	//	5 tabs, 3 are displayed. First tab is highlighted by default, and first panel is showing.
	//	I click next, tab 2 is highlighted, and panel 2 is activated
	//	i click next, tab 3 is highlighted and panel 3 is activated
	//	I click next, the tabs now show tabs 2, 3, 4. tab 4 is highlighted, and panel 4 is showing.
	//	I click previous, tabs 2, 3, 4 are showing, tab 3 is highlighted, and panel 3 is activated
	//	i click previous again, tabs 2, 3, 4 are showing, tab 2 is highlighted, and panel 2 is activated

	//	i click previous again, tabs 1, 2, 3 are showing, tab 1 is highlighted, and panel 1 is activated

	//  if  start from the begining and tabs 1,2,3 are showing, and when i press next from panel 1 and it goes to 4, then
	//	tab 2,3,4 shown up. The minimum amount of scrolling to show the hignlighted tab.
	//	So, if you shift 1,2,3 right once, you get 2,3,4, and tab 4 shows, so you are OK.
		if(jump){
			this.startIndex = this.selectedTab-this.tabSize+1;

		}else if(this.selectedTab>(this.startIndex+this.tabSize-1)){
			this.startIndex++;
		}else if(this.selectedTab<this.startIndex)
		{

			this.startIndex = ((this.selectedTab-this.tabSize+1)>=1)?(this.selectedTab-this.tabSize+1):1;
		}
		if(this.startIndex<=0) this.startIndex = 1;

		var tabTable = document.getElementById(this.containerDiv.id+this.styleClass+"tabtable");

		this.showHideTab(tabTable);

		this.setButtonIds();

		if(redrawPaneContents){

			try{
				if(typeof(hX_3)!="undefined" && hX_3!=null){
					//for jsf other controls which is not in the first panel, has to redraw when change panel.
					hX_3.redraw();
				}
				if(!isIE())
				{//NS graph inside tabpanel has redraw problem, has to use timer right now. Need to find a good solution.

					odcGraphTimeObj = window.setTimeout(odcGraphRedraw, 1);

				}
			}catch(e){
				//maybe user dosen't have hXclient.js included.
			}


		}

		this.selectedTabID = showID;


	}

	/**
	* @method private setButtonIds
	*   The function is used to determine which button is disabled or enabled
	* @param Object obj
	*	The tab control
	**/
	this.setButtonIds = function() {

		if (this.previousButton == null)
			return

		if(this.selectedTab ==1){
			this.previousButton.disabled = true;
		}else{

			this.previousButton.disabled = false;
			this.previousButton.name = this.Panes[this.selectedTab-1].divObj.DIVID ;

		}

		if(this.selectedTab == this.Panes.length-1)
		{
			this.nextButton.disabled = true;


		}else{

			this.nextButton.disabled = false;
			this.nextButton.name = this.Panes[this.selectedTab+1].divObj.DIVID ;

		}

		if(this.enableFinish){
			if (null != this.enableFinishButtonArray ) {

				for (var i = 0; i < this.enableFinishButtonArray.length; i++) {

					if (this.Panes[this.selectedTab].divObj.DIVID == this.enableFinishButtonArray[i]) {

						//RATLC01082051 - need to check if the id is in the disable cancel or finish array for each button
						this.cancelButton.disabled = false;
						if(this.disableCancelButtonArray != null){
							var length = this.disableCancelButtonArray.length;
							for(var x = 0; x < length; x++){
								if(this.disableCancelButtonArray[x] == this.enableFinishButtonArray[i]) {
									this.cancelButton.disabled = true;
									break;
								}							
							}
						}
						this.finishButton.disabled = false;
						if(this.disableFinishButtonArray != null) {
							var length = this.disableFinishButtonArray.length;
							for(var x = 0; x < length; x++){
								if(this.disableFinishButtonArray[x] == this.enableFinishButtonArray[i]) {
									this.finishButton.disabled = true;
									break;
								}							
							}
						}

/*
						this.cancelButton.style.display = "inline";

						this.cancelButton.onCancel = this.onCancel;
						this.cancelButton.onclick= this.OnCancelButtonClick;


						this.finishButton.style.display = "inline";

						this.finishButton.onFinish = this.onFinish;
						this.finishButton.onclick= this.OnFinishButtonClick;
*/

						break;
					}else{
						this.cancelButton.disabled = true;
						this.finishButton.disabled = true;


					}
			//		this.cancelButton.style.display = "none";
			//		this.finishButton.style.display = "none";
				}
			}
		}
	}





// Private:  Style the HTML of a 4-cell table to make it look like a tab
	this.styleTabHTML = function(tabobj, td, bFirst, tabclass,linkclass, nipLeft, nipRight, bottomline, index) {

		if (td.style.display == "none") return -1;

		var table = this.getFirstChild(td);
		var tr  = table.rows[0];
		var tdZ = tr.cells[0];
		var tdL = tr.cells[1];
		var tdM = tr.cells[2];
		var tdR = tr.cells[3];

		// Style the center td, first clearing anything we may have set previously
		tdM.style.borderLeftWidth = tdM.style.borderRightWidth = "";
		tdM.className = tabclass;
		tdM.name = tabobj.DIVID;
		tdM.id   = this.containerDiv.id+this.styleClass+"tabTdM_" +index;
		tdM.controlName   = this.Name;


		var tablabel = document.createElement("label");
		//tablabel.name = tabobj.DIVID;
		tablabel.id   = this.containerDiv.id+this.styleClass+"tablable_" +index;
		//tablabel.controlName   = this.Name;


		tablabel.innerHTML = tabobj.name;

		while(tdM.hasChildNodes())
			tdM.removeChild(tdM.lastChild);


		//this is mainly for accessbility issue.
		var a = document.createElement("a");
		//a.name = tabobj.DIVID;
		a.id   = this.containerDiv.id+this.styleClass+"hyperlink_" +index;
		//a.controlName   = this.Name;
		a.className = linkclass;
		a.setAttribute("href", "javascript:;");

		a.appendChild(tablabel);
		tdM.appendChild(a);




		//	tdM.innerHTML = tabobj.name;

		var borderT = getBorderSize (tdM, "top", 1);
		var borderB = getBorderSize (tdM, "bottom", 1);
		var borderL = getBorderSize (tdM, "left", 1);
		var borderR = getBorderSize (tdM, "right", 1);


		var backColor = getEffectiveStyle(tdM,"background-color");
		var borderTopStyle = getEffectiveStyle(tdM,"border-top-style");
		var borderTopColor = getEffectiveStyle(tdM,"border-top-color");
		var borderRightColor = getEffectiveStyle(tdM,"border-right-color");
		var borderLeftColor = getEffectiveStyle(tdM,"border-left-color");
		var borderBottomColor = getEffectiveStyle(tdM,"border-bottom-color");
		tdM.style.borderLeftWidth = tdM.style.borderRightWidth = "0px";


		if (bFirst)
			this.tabHeight = (tdM.offsetHeight>0)?tdM.offsetHeight:1;
		if (tdL.height != this.tabHeight) {
			tdL.height = this.tabHeight;
			tdM.height = this.tabHeight;
			tdR.height = this.tabHeight;
		}

		var cssVOffset = 0;
		var cssHOffset = 0;
		if (!isIE()) {
			cssVOffset = borderT + ((borderT > 1) ? 1 : 0);
			cssHOffset = 2;			
		}

		// Style the left td
		var color, sdiv, div, nip, xtra;
		div = this.getFirstChild(tdL);
		if (!div) div = this.createDiv(tdL,0);
		if (tdL.className != tdM.className)	tdL.className = tdM.className;
		if (tdL.style.textAlign != "left")  tdL.style.textAlign = "left";
		sdiv = this.getFirstChild(div);
		if (nipLeft == 0) {
			if (tdL.width != "1") {
				tdL.setAttribute("width", "1");
				tdL.style.borderLeftWidth = "";
				tdL.style.borderTopWidth = "";
				tdL.style.backgroundColor = "";
			}
		} else {
			nip = nipLeft + borderL;
			xtra = (borderT > 1) ? 1 : 0;
			nip = (nip > this.tabHeight - borderB + xtra) ? this.tabHeight - borderB + xtra - cssVOffset: nip;
			if (tdL.width != nip+"") {
				tdL.setAttribute("width", nip+"");
				tdL.style.borderLeftWidth = "0px";
				tdL.style.borderTopWidth = "0px";
				tdL.style.backgroundColor = "transparent";
			}
			for (var k = 1; k <= nip; k++) {
				if (!sdiv) sdiv = this.createDiv(div,1);
				color = (k <= borderL) ? borderLeftColor : backColor;
				// decfahey: Nip is to be drawn backwards when using rtl.
				if (document.documentElement.dir != "rtl") {
					this.vertLine (sdiv, color, k-1, (nip-k+1-xtra), this.tabHeight - borderB - (nip-k+1) + xtra - cssVOffset, borderT+xtra, borderTopStyle, borderTopColor);
				}
				else {
					if(isIE()) {
						this.vertLine (sdiv, color, nip-k-1, (nip-k+1-xtra), this.tabHeight - borderB - (nip-k+1) + xtra - cssVOffset, borderT+xtra, borderTopStyle, borderTopColor);
					}
					else {
						this.vertLine (sdiv, color, nip-k-5, (nip-k+1-xtra), this.tabHeight - borderB - (nip-k+1) + xtra - cssVOffset, borderT+xtra, borderTopStyle, borderTopColor);
					}
				}
				sdiv = sdiv.nextSibling;
			}
		}

		while (sdiv) {

			sdiv.style.display = "none";
			sdiv = sdiv.nextSibling;
		}

		// Style the right td
		div = this.getFirstChild(tdR);
		if (!div) div = this.createDiv(tdR,0);
		if (tdR.className != tdM.className) tdR.className = tdM.className;
		if (tdR.style.textAlign != "left") tdR.style.textAlign = "left";
		
		sdiv = this.getFirstChild(div);
		if (nipRight == 0) {
			if (tdR.width != "1") {
				tdR.setAttribute("width", "1");
				tdR.style.borderRightWidth = "";
				tdR.style.borderTopWidth = "";
				tdR.style.backgroundColor = "";
			}
		} else {

			nip = nipRight + borderR;
			xtra = (borderT > 1) ? 1 : 0;
			nip = (nip > this.tabHeight - borderB + xtra) ? (this.tabHeight - borderB + xtra - cssVOffset) : nip;

			if (tdR.width != nip+"") {
				tdR.setAttribute("width", nip+"");
				tdR.style.borderRightWidth = "0px";
				tdR.style.borderTopWidth = "0px";
				tdR.style.backgroundColor = "transparent";
			}
			for (var k = 1; k <= nip; k++) {
				if (!sdiv) sdiv = this.createDiv(div,1);
				color = (k > nip-borderR) ? borderRightColor : backColor;
				// decfahey: Nip is to be drawn backwards when using rtl.
				if (document.documentElement.dir != "rtl") {
					this.vertLine (sdiv, color, k-1, k-xtra, this.tabHeight - borderB - k + xtra - cssVOffset, borderT+xtra, borderTopStyle, borderTopColor);
				} 
				else {
					if (isIE()) {
						this.vertLine (sdiv, color, nip-k, k-xtra, this.tabHeight - borderB - k + xtra - cssVOffset, borderT+xtra, borderTopStyle, borderTopColor);
					}
					else {
						this.vertLine (sdiv, color, nip-k-4, k-xtra, this.tabHeight - borderB - k + xtra - cssVOffset, borderT+xtra, borderTopStyle, borderTopColor);
					}
				}
				sdiv = sdiv.nextSibling;
			}
		}
		while (sdiv) {
			sdiv.style.display = "none";
			sdiv = sdiv.nextSibling;
		}

		// If this is the active tab, we may need to "overwrite" the line at the bottom of the tab
		// Tab is active, its bottom border color is same as the tab color, and the border width is zero
		div = this.getFirstChild(tdZ);
		if (!div) div = this.createDiv(tdZ,1);
		sdiv = this.getFirstChild(div);
		if (!sdiv) sdiv = this.createDiv(div,1);
		if (div.style.display != "none") div.style.display = "none";

		if (bottomline) {

			if (borderBottomColor == backColor && borderB == 0) {
				var wn = (tr.offsetWidth) - (borderR+borderL);
				var	w = wn + "px";
				var h = (getBorderSize (td, "bottom", 1)) + "px";
				if (div.style.display != "block")		div.style.display = "block";
				if (div.style.overflow != "visible") 	div.style.overflow = "visible";
				if (div.style.zIndex != "100000") 	 	div.style.zIndex = "100000";
				if (sdiv.style.display != "block")		sdiv.style.display = "block";
				if (sdiv.style.overflow != "hidden")	sdiv.style.overflow = "hidden";
				// decfahey: again left reference breaking rtl.
				if ((sdiv.style.left != borderL + "px") && (document.documentElement.dir != "rtl"))	sdiv.style.left = borderL + "px";
				if (sdiv.style.top  != this.tabHeight + "px")	sdiv.style.top = this.tabHeight + "px";
				if (sdiv.style.width!= w&&wn>0)				sdiv.style.width = w;
				if (sdiv.style.height != h)				sdiv.style.height = h;
				sdiv.style.backgroundColor = backColor;
			}
		}

		return (tr.offsetWidth + (borderL+borderR) + cssHOffset);
	}

// Implementation: Helper for creating divs used to paint pixels
	this.createDiv = function(p, t) {
		var d = document.createElement("div");
		if (t==0) {
			d.style.position = "relative";
			d.style.top = "0px";
			d.style.left = "0px";
		} else {
			d.style.position = "absolute";
			d.style.overflow = "hidden";
		}
		d.style.backgroundColor = "transparent";
		d.style.fontSize= "0px";
		d.style.width = "1px";	d.style.height = "1px";
		d.style.borderWidth = "0px";
		d.style.padding = "0px";
		d.style.margin = "0px";
		p.appendChild(d);
		return d;
	}

// Implementation: Helper for "drawing" a line using a div to draw the line
	this.vertLine = function(sdiv, color, hOffset, vOffset, height, borderT, borderTopStyle, borderTopColor) {
		var h = ((height <= 1) ? 1 : height) + "px";
		sdiv.style.backgroundColor = color;
		sdiv.style.borderTopStyle = borderTopStyle;
		sdiv.style.borderTopColor = borderTopColor;
		sdiv.style.borderTopWidth = borderT;
		if (sdiv.style.display!= "block")		sdiv.style.display="block";
		if (sdiv.style.height != h)				sdiv.style.height = h;
		if (sdiv.style.width  != "1px")			sdiv.style.width  = "1px";
		if (sdiv.style.top    != vOffset+"px")	sdiv.style.top    = vOffset + "px";
		if (sdiv.style.left   != hOffset+"px")	sdiv.style.left	  = hOffset + "px";
	}

// Private:  Make a table used to draw a tab. Each table is a one row with 4 cells in it.
//			 Leftmost cell is a "spacer", next cell the "slant left", then the label cell, then the right slant cell
//			 i.e., ./label\
	this.emitTabHTML = function(td, tabobj, index) {
		td.valign = "top";
		var table = document.createElement("table");
		table.border = "0";
		table.cellSpacing = "0";
		table.cellPadding = "0";
		table.onclick = this.onTabSelected;
		table.onkeydown = this.onTabSelected;
		table.name = tabobj.DIVID;
		table.id   = this.containerDiv.id+this.stylClass+"hyperlink_" +index;
		table.controlName   = this.Name;

		if(!isodctpIE5)
			table.style.cursor = "pointer";

		td.appendChild(table);
		var tr = table.insertRow(0);

		// First cell is "invisible" -- used only to position things
		var td = tr.insertCell(0);
		td.setAttribute("width", "0");
		td.vAlign = "top";
		td.noWrap = "true";
		td.style.padding="0px";
		td.style.margin="0px";
		td.style.borderWidth = "0px";

		// Second cell is the "left" slant
		td = tr.insertCell(1);
		td.vAlign = "top";
		td.noWrap = "true";
		td.style.padding="0px";
		td.style.margin="0px";
		td.style.borderRightWidth = "0px";

		// Third cell is the label
		td = tr.insertCell(2);
		td.vAlign = "top";
		td.noWrap = "true";
		td.style.margin ="0px";

		// Fourth cell is the "right" slant
		td = tr.insertCell(3);
		td.vAlign = "top";
		td.noWrap = "true";
		td.style.padding="0px";
		td.style.margin="0px";
		td.style.borderLeftWidth = "0px";

		return table;
	}

	// Private:  The first child of a cell/div can be a "text node" in Netscape
	this.getFirstChild = function (cell) {
		r = (cell) ? cell.firstChild : null;
		while (r && r.nodeType != 1) r = r.nextSibling;
		return r;
	}
}

/**
 * @class public Pane
 * 	This class is to construct a panel object in the tab
 * @constructor Pane
 *   The constructor takes no parameters
**/
function Pane () {

	var divObj = null;
	var Name = null;


}




/*
OnExit()

// process here to find out what pane to show, and hide

paneSelected(show, hide)
	- global.OnExit
	- local.onpaneselect

showPane / showTab
	- global.OnExit
	- local.OnExit


*/
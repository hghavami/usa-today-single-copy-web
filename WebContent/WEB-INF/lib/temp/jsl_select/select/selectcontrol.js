//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003.  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//==================================================================
//-------------------------------------------------------
// SelectControl.js
// @author Kaushal Kurapati
//-------------------------------------------------------
 /**
  * @class public SelectControl
  * SelectControl class is associated with the HTML select menu.
 **/

 /**
  * @method SelectControl
  * Constructor for the class SelectControl.
 **/
function SelectControl() 
{
	// Variable Declarations & initializations
	this.Adapter   = null;
	this.selectObj = null;
	this.originalValueArray = new Array();
	this.originalTextArray  = new Array();
	this.optionValueArray = null;
	this.optionTextArray = null;
	
   /** 
     * @method public setValue
	 * Method setValue() calls refreshDataSet() to load up new select value / text data.
	 * If the eObject is null--in this case the refreshDataSet() function will pick up
	 * the parentEObject and get the relevant data to be displayed in the select menu from there.
	**/
	this.setValue = function()
	{
		this.Adapter.refreshDataSet(null);
		this.updateControl();
	}

    /**
	 * @method public setOriginalOptions
	 * Method to set any user-provided option values and text into a separate array called
	 * originalValueArray and originalTextArray. These original options will not be touched later on
	 * by the updateControl.
	**/	
	this.setOriginalOptions = function()
	{
		for(var i = 0; i < this.originalValueArray.length; i++)
		{
			this.selectObj.options[i] = new Option(this.originalTextArray[i], this.originalValueArray[i]);
		}
	}

	/**
	  * @method public updateControl
	  * Method updateControl() actually updates the options in the Select menu control.
	  * It first sets the options to the number of original options
	  * and then ADDS new option data to the original options.
	 **/	
	this.updateControl = function() 
	{	
		// First set the options array length to the original array length
		this.selectObj.options.length = this.originalValueArray.length;
		var startIndex = this.selectObj.options.length;
		var endIndex   = this.selectObj.options.length + this.optionValueArray.length;
		
		// Now create new option objects (got from WDO, say) and ADD to the original Option values in select control
		for(var i = startIndex; i < endIndex; i++)
		{
			this.selectObj.options[i] = new Option(this.optionTextArray[i-startIndex], this.optionValueArray[i-startIndex]);
		}
	}
}
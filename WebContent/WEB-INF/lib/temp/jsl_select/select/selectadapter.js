//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003.  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//==================================================================
//-------------------------------------------------------
// SelectAdapter.js
// @author Kaushal Kurapati
//-------------------------------------------------------
/**
  * @class public SelectAdapter
  * SelectAdapter operates in conjunction with SelectControl and binds the control
  * to a WDO.
 **/

/**
  * @method public SelectAdapter
  * Constructor for the class SelectAdapter.
 **/
function SelectAdapter(selectCtrl)
{
	this.selectControl = selectCtrl;
	this.propertyName= "";
	this.parentEObject = null;
	this.attributeName = "";

	this.selectControl.Adapter = this;

	/**
	  * @method bind
	  * Bind basically delegates the functionality to activateDataSet(), which sets the property binders.
	 **/
	this.bind = function()
	{
		this.activateDataSet(this.parentEObject);
	}

	/**
	 * @method public activateDataSet
	 * Method activateDataSet sets the property binder and calls refreshDataSet(), which
	 * in turn refreshes the data set in the control.
	 * @param EObject eObj		eObject which is bound to the selectControl via a PropertyBinder.
	**/
	this.activateDataSet = function(eObj)
	{
		if (null == eObj)
		{
			alert("in activateDataSet, eObject is null");
			return;
		}
		var pb = new PropertyBinder(eObj,this.propertyName,this.selectControl,null,null,null);
		pb.ControlSetFunction = "setValue";
		pb.DataBind();

		//var EObjectsArray = eObj.eGet(this.propertyName);
		// Array of property binders; setValue() is the callback function that is set up
		// to process the changes in the cells of the datagrid, say.
		//for (i=0; i< EObjectsArray.length; i++)
		//{
		//	var pb = new PropertyBinder(EObjectsArray[i],this.attributeName,this.selectControl,null,null,null);
		//	pb.ControlSetFunction = "setValue";
		//	pb.DataBind();
		//	alert("set up property binders");
		//}
		this.refreshDataSet(eObj);
	}

	/**
	 * @method public refreshDataSet
	 * Method refreshDataSet retrieves a bunch of new select option values from the eObject
	 * and populates the optionValueArray and optionTextArray datastructures in the SelectControl.
	 * @param EObject eObj	eObject that has the new option values.
	**/
	this.refreshDataSet = function(eObj)
	{
		var refArray = new Array();
		var newOptionValues = new Array();

		// if eObject is null (when called from setValue--say this function is called
		// the first time a page is loaded), pick up the parentEObject in the adapter.
		// If it is not null (when called from say a callback function in the page, which
		// calls the activateDataSet() directly with eObject as a parameter--providing the
		// changed eObject), get the latest eObject and process it for new data.
		if(eObj != null)
		{
			this.parentEObject = eObj;
			refArray = eObj.eGet(this.propertyName);
		}
		else
		{
			refArray = this.parentEObject.eGet(this.propertyName);
		}

		if(refArray != null)
		{
			for(var i = 0; i < refArray.length; i++)
			{
				var tempNewOptionValue = "";
				tempNewOptionValue = refArray[i].eGet(this.attributeName);
				newOptionValues[i] = tempNewOptionValue;
			}
		}
		this.selectControl.optionValueArray = newOptionValues;
		this.selectControl.optionTextArray  = newOptionValues;
	}

	/**
	 * @method refresh
	 * Method refresh is used to call the updateControl function in SelectControl
	 * to update the select option values with new ones.
	**/
	this.refresh = function()
	{
		this.selectControl.updateControl();
		this.selectControl.selectObj.selectedIndex = 0;
		this.selectControl.selectObj.onclick();
	}
}

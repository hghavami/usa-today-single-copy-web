//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003.  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//=================================================================


/**
 * @class public DataGrid
 * @constructor DataGrid
 * 	This class is DataGrid control which will render the datagrid
 * 	table based on the data set which is getting from the datagrid
 *	adapter.
 * @param Object parent
 *	The object which is point to place where the datagrid will be
 *	put in the html page
 * @param Array columnTitles
 *	The Array which has all the displayed name of the table header.
 *	If null, using the default EClass name.
 * @param String UrlPrefix
 *		The path relative to "jsl/datagrid/icons/" of the datagrid images
**/

function DataGrid(parent,columnTitles,UrlPrefix)
{

	if (parent != null) {
		while (parent.hasChildNodes())
			parent.removeChild(parent.lastChild);
	}

// 	this.IconLib = new dgIconLibrary(UrlPrefix);
	this.ID = "id" + Math.random()*10;

	this.datagridIcons = new Array();
	this.getDefaultSystemIcon(UrlPrefix);


	this.ColumnTitles = columnTitles;	// public;
	this.selectedItem = new Array();//for checkbox selected items info
	this.headArray = new Array; //for jsf regular column image change.

	this.HTMLTable = document.createElement("table");


	this.HTMLTable.GridControl = this;
	this.HTMLTable.setAttribute("id",this.ID);
	this.HTMLTable.border = 0;
	this.HTMLTable.cellPadding = 0;
	this.HTMLTable.cellSpacing = 0;
	parent.appendChild(this.HTMLTable);

	this.parentDiv = parent;

	this.EObjects = new Array();
	this.DataArray = new Array();
	this.eventArray = new Array;
	this.checkedEObjectArray = new Array(); //keep all the selected eobjects for event object.
	this.tdConverter = new Array();
//	this.tdWidth= new Array();
	this.tdAlign= new Array(); //keep the td width and align information for the new added line.
//	this.readOnlyTemplate = new Array();
//	this.ColumnSizes = new Array();
	this.selAll = false; //to remember select all/unselect all action.
	this.UrlPrefix = UrlPrefix;
	this.width = "100%";
}

	DataGrid.prototype.type = "DataGrid";
	DataGrid.prototype.pageId = "datagrid1";//default pageId which will be changed by ODCRegistry when do addElementInfo

	DataGrid.prototype.AllowPaging = false;
	DataGrid.prototype.PageSize = 5;
	DataGrid.prototype.StartIndex  = 0;	// private
	DataGrid.prototype.ReadOnly = false;	// public
	//DataGrid.prototype.ModifyOnly = false;	// public
	//DataGrid.prototype.PropertyName = "";

	DataGrid.prototype.totalRows = 1;
	DataGrid.prototype.totalPage = 1;

	DataGrid.prototype.metaData = null;	//mainly used to control the columns looking and feel.
	//Here is the example of metadata
	//ODCDataGridControlVar29.metaData = [["symbol", [true, "center", 160, false], converterObj]],...]
	//[[column Name, [ifeditable, alignment, width, ifunderline], converterObj]].
	DataGrid.prototype.styleClass = "";	//so two tables can have different style sheet.
	DataGrid.prototype.currentPage = 1;
	DataGrid.prototype.highlightItem = null; //for high lighted row info
	DataGrid.prototype.sltRdoItem = null; //for radio selected item info.
	DataGrid.prototype.RowFilter = null;


	DataGrid.prototype.sortOrder = null; //for jsf UIState of column, so each column has its owen sort state info

	DataGrid.prototype.sortState = null; //for restore UI state purpose. We only remeber the last sorted column.
//	this.selectSortOrder = null; //for jsf UIState, for select column

	DataGrid.prototype.selectHead = null; 	//for jsf select column image change.
	DataGrid.prototype.indexHead = null;    //for jsf index column image change.

	DataGrid.prototype.NavBarPosition = 0; //0:navigation bar at the bottom. 1:on the top. 2:both; -1: no navigation bar

//	DataGrid.prototype.pageChange =false; //to solve top navigation bar flash problem. add this flag to indicate there is no page change
							//so don't need to update navigation bar. once tried to use "currentPage", but didn't work.
	DataGrid.prototype.showRowIndex = null; //meta data for selection column. [name, checkobx/radio, align, width];
	DataGrid.prototype.selectCol = null; //show index column or not.
//	DataGrid.prototype.ParentEObject = null;

	DataGrid.prototype.NumberOfCols = 0;
	DataGrid.prototype.stateString = ""; //keep it because, for example, you have one row selected and one row highlight on page 1,
	// but your current page index 2, then when call restoreUIState, page 2 is displayed without highlight or selecte, if go to page 1
	//then all the highlighted or selected rows should restore to original state.

	//detect if the browser is IE5.5 so that don't display cursor style.
	var isodcdgIE5 = false;
	var browser = detectBrowser();

	if(browser.substring(browser.indexOf("MSIE")+5)<6){
		isodcdgIE5 = true;
	}


/**
* @method public DataGrid.prototype.getType
*   This function is return the type of the control
* @return String
*	Get the control's type "DataGrid"
**/
	DataGrid.prototype.getType = function(){
		return this.type;
	}


/**
* @method public DataGrid.prototype.getId
*   This function return the id of the control
* @return String
*  	The id of the control
**/
	DataGrid.prototype.getId = function(){
		return this.pageId;
	}


/**
* @method public DataGrid.prototype.setWidth
*   This function is used to set the tabpanel's width.
* @param string width
*	The user specified width
**/

DataGrid.prototype.setWidth = function(width)
{

		this.width = width;


}



/**
* @method private DataGrid.prototype.onRowRemoved
*   This is a call back function which is called by the datagrid
*	control when one row is deleted so the adapter will update the
*	data model.
* @param int rowIndex
*   The row number of the datagrid which is to be deleted
**/
	DataGrid.prototype.OnRowRemoved = function (rowIndex) {

		this.Adapter.parentEObject.eRemove(this.Adapter.propertyName,this.Adapter.EObjects[rowIndex]);
		//this.Adapter.obtainEObjects();
		/*
		this.Adapter.activateDataSet(this.Adapter.parentEObject);

		this.Adapter.refresh();
		*/
		
		this.activeEvent("onRowRemove");

	}
/**
* @method private DataGrid.prototype.getDataArray
*   This is a call back function which is called by the datagrid
*	control to get the current page's data set which will improve
*	the performance since only the data in that page will be constructed
**/
	DataGrid.prototype.refreshDataArray = function(){

		this.Adapter.getCurPageDataSet();
	}

/**
* @method DataGrid.prototype.private onAdd
*   This is a call back function which is called by the datagrid
*	control when one row is added so the adapter will update the
*	data model.
* @param Object tr
*   The new row object of the datagrid which is added
**/

	DataGrid.prototype.OnRowAdded = function (tr) {

		var newObject = this.Adapter.parentEObject.CreateEObject(this.Adapter.propertyName);

		var idx=0;
		if(this.selectCol)
			idx++;
		if(this.showRowIndex)
			idx++;


		for(var j=0; j<this.metaData.length; j++){

			for (var i = 0; i < newObject.Members.length; ++i) {
				var member = newObject.Members[i];
				var str = member.Name;

				if(str == this.metaData[j][0]){
					if (member.EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE)
						continue;

					if (member.EStructuralFeature.CLASSTYPE != EStructuralFeature.CLASSTYPE_EATTRIBUTECALCULATE){

						var value = newObject.eGet(member.Name);
						var td = tr.cells[idx];

						var input = td.firstChild;

						if(this.tdConverter[idx]&&this.tdConverter[idx]!='undefined')

							value = this.tdConverter[idx].stringToValue(input.value);
						else
							value = input.value;


						newObject.eSet(member.Name,value);

					}
					++idx;
					break;
				}else{
					if (member.EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTECALCULATE)

						member.EStructuralFeature.Active = false;


				}
			}
		}

		this.Adapter.parentEObject.eAdd(this.Adapter.propertyName,newObject);

		this.Adapter.activateDataSet(this.Adapter.parentEObject);

		this.Adapter.refresh();
		this.activeEvent("onRowAdd");		

	}



/**
* @method public DataGrid.prototype.addHandler
*   This function is used to add event handler to the control array
*	so that all the event can be queued
* @param String action
*	The action which the datagrid control has event handler associated,
*	like "OnHighlight", "OnSelect" etc.
* @param String handler
*   The function name of the event handler
**/
	DataGrid.prototype.addHandler = function(action, handler)
	{

		this.eventArray[this.eventArray.length] = new Array(action, handler);



	}


/**
* @method public DataGrid.prototype.setRowFilter
*   This function is used to set the new row filter
* @param String filter
*	new filter expression
**/
	DataGrid.prototype.setRowFilter = function(filter)
	{
 		//onFilter, which would be invoked when the Filter is updated, before filtering actually happens
		if(!this.activeEvent("onFilter"));
			return;
		this.RowFilter = filter;
		//Do I need to call adapter activiateDataset and refresh function?

	}


 /**
* @method public DataGrid.prototype.setstyleClass
*   This function is used to set the style sheet prefix for this
*	datagrid
* @param String prefix
*	the prefix which is added before the style sheet name. Like
* 	"dg1", so the css name will be "dg1_wpsTableSelectedRow".
**/
 DataGrid.prototype.setStyleClass = function(prefix)
 {
	this.styleClass = prefix + "_";

	this.normalStyleClass = this.styleClass+"wpsTableNrmRow" +" "+ this.styleClass+"wpsCheckBoxRow";

	this.highlightStyleClass = this.styleClass+"wpsTableNrmRow" +" " +this.styleClass+"wpsTableHighlightedRow"+" "+ this.styleClass+"wpsCheckBoxRow";

	this.selectStyleClass = this.styleClass+"wpsTableNrmRow" +" " +this.styleClass+"wpsTableSelectedRow" +" "+ this.styleClass+"wpsCheckBoxRow";

	 this.parentDiv.className = this.styleClass+"classForDatagridSysIcons";

	 var icons = new iconLibrary(this.parentDiv,"list-style-image");
	 for(var key in icons){
		 if(icons[key].indexOf("http")>-1){
			 this.datagridIcons[key] = icons[key];
		 }else{
		 	this.datagridIcons[key] = this.UrlPrefix+icons[key];
		}
	 }


 }

 /**
* @method private DataGrid.prototype.getDefaultSystemIcon
*   This function is used to get the default icons if the user didn't
*	specify it in the style sheet file
* @param String prefix
*	the prefix which is added before the style sheet name. Like
* 	"dg1", so the css name will be "dg1_wpsTableSelectedRow".
**/
DataGrid.prototype.getDefaultSystemIcon = function(UrlPrefix)
{

	this.datagridIcons["go"]      = UrlPrefix+"jsl/datagrid/icons/Buttons_Go.gif";       // jump to the selected page
    this.datagridIcons["previous"]= UrlPrefix+"jsl/datagrid/icons/Buttons_Left.gif"; // previous page
   // this.datagridIcons["disprevious"] = UrlPrefix+"jsl/datagrid/icons/previous_disabled.gif";
    this.datagridIcons["next"]     = UrlPrefix+"jsl/datagrid/icons/Buttons_Right.gif";        //  next page
   // this.datagridIcons["disnext"] = UrlPrefix+"jsl/datagrid/icons/next_disabled.gif";
    this.datagridIcons["start"] = UrlPrefix+"jsl/datagrid/icons/Buttons_First.gif";  // to the first page
   // this.datagridIcons["disstart"] = UrlPrefix+"jsl/datagrid/icons/start_disabled.gif";
    this.datagridIcons["end"]       = UrlPrefix+"jsl/datagrid/icons/Buttons_Last.gif";        // to the last page
   // this.datagridIcons["disend"] = UrlPrefix+"jsl/datagrid/icons/end_disabled.gif";
    this.datagridIcons["sortup"] = UrlPrefix+"jsl/datagrid/icons/n-sort-a-z.gif";
    this.datagridIcons["sortdown"] = UrlPrefix+"jsl/datagrid/icons/n-sort-z-a.gif";
    this.datagridIcons["clear"] = UrlPrefix+"jsl/datagrid/icons/clear.gif";
    this.datagridIcons["selectall"] = UrlPrefix+"jsl/datagrid/icons/select_all.gif";
    this.datagridIcons["unselectall"] = UrlPrefix+"jsl/datagrid/icons/select_none.gif";
    this.datagridIcons["addrow"] = UrlPrefix+"jsl/datagrid/icons/add_row.gif";
	this.datagridIcons["delrow"] = UrlPrefix+"jsl/datagrid/icons/remove_row.gif";
	this.datagridIcons["acceptrow"] = UrlPrefix+"jsl/datagrid/icons/ok.gif";
	this.datagridIcons["cancelrow"] = UrlPrefix+"jsl/datagrid/icons/cancel.gif";
	this.datagridIcons["wpsPagingTableHeaderEnd"] = UrlPrefix+"jsl/datagrid/icons/PagingTableTopRight.gif";
	this.datagridIcons["wpsPagingTableFooterEnd"] = UrlPrefix+"jsl/datagrid/icons/PagingTableBottomRight.gif";
	this.datagridIcons["wpsPagingTableHeaderStart"] = UrlPrefix+"jsl/datagrid/icons/tableleft.gif";
 }




/**
* @method private DataGrid.prototype.OnError
*   This is event function which is called when datagrid control has
*	problem on constructing the datagrid table. It will pop up alert
*	message to the user.
* @param String action
*	The action which the datagrid control has problem, like "BUILD ROW",
*	"DELETE ROW", "ADD ROW" etc.
* @param Object exception
*   The exception catched by the code
**/
	DataGrid.prototype.OnError = function(action,exception)	// public
	{
		var args = new Array;
		args[0] = action;
		args[1] = exception.description;

		alert(NlsFormatMsg(error_datagrid_action, args));

	}
	this.OnRowChanged = null;
	this.OnRowAdded = null;
	this.OnRowRemoved = null;
	this.OnSelect = null;
	this.OnUnSelect = null;
	this.OnHighlight = null;

	this.refreshDataArray = null;

/**
* @method private SetupDataGridEventObject
*   This function is used to construct an window.event object for
*	Netscape so that this "window.event" object can be used cross IE
*	and NS
* @param Object e
*	Netscape event object
**/
	function SetupDataGridEventObject(e) // private
	{
		if (e==null) return; // IE returns
		window.event = e;
		window.event.fromElement = e.target;
		window.event.toElement = e.target;
		window.event.srcElement = e.target;
		window.event.x = e.x;
		window.event.y = e.y;
	}

/**
* @method private DataGrid.prototype.selectRow
*   This function is used to change the color of the row which is be
*	selected by checking the checkboxusing style sheet.
* @param Object obj
*	The row which is be selected
**/
	DataGrid.prototype.selectRow = function(obj)
	{
		var id = obj.eobject.ID;

	//	var cssStyle = this.styleClass+"wpsTableSelectedRow" + " " + this.styleClass+ "wpsCheckBoxRow";
	//	var cssStyle= this.styleClass+"wpsTableNrmRow" +" " +this.styleClass+"wpsTableSelectedRow" +" "+ this.styleClass+" wpsCheckBoxRow";
		var cssStyle = this.selectStyleClass;
;
		if(this.highlightItem&&this.highlightItem[id]){

			//highlight row is the same as checked row.

			if(this.selectedItem[id] == null)
			//if it is NOT already selected and then user click "select all"button.
				this.selectedItem[id] =this.highlightItem[id][1];

			this.highlightItem[id][1] = cssStyle;


		}else if(this.selectedItem[id]==null){

			//if it is not selected,
			this.selectedItem[id] =obj.className;

			obj.className = cssStyle;


		}else{
			//if it is already selected, and then user click "select all" button.
			obj.className = cssStyle;
		}

	//	this.checkedEObjectArray = new Array();
		this.checkedEObjectArray[id] = obj.eobject;


	}
/**
* @method private DataGrid.prototype.unSelectRow
*   This function is used to restore the color of the row which is
*	be deselected by uncheck the check box using style sheet.
* @param Object obj
*	The row which is be deselected
**/
	DataGrid.prototype.unSelectRow = function(obj)
	{

		if(obj.eobject!=null){
			var id = obj.eobject.ID;

			if(this.highlightItem&&this.highlightItem[id]){
				//highlight row is the same as unchecked row.
				this.highlightItem[id][1] = this.selectedItem[id];

			}else{
				if(this.selectedItem[id])
					obj.className = this.selectedItem[id];


			}

			this.selectedItem[id]=null;
			//array of eobject whose checkbox is checked.
			this.checkedEObjectArray[id] = null;
		}

	}

/**
* @method private DataGrid.prototype.sltRdoRow
*   This function is used to change the color of the row which is be
* 	selected by click the radio button if it is not be selected before
* @param Object obj
*	The row which is be deselected
**/

	DataGrid.prototype.sltRdoRow = function(obj)
	{


		var id = obj.eobject.ID;

		if(this.sltRdoItem){
			for(var key in this.sltRdoItem){
				if(this.curPageIDArray[key])
				{
					if(this.sltRdoItem[key]){
						this.unSltRdoRow(this.sltRdoItem[key][0]);
						break;
					}


				}else{
					//if not on the same page
					this.sltRdoItem[key] = null;
					this.checkedEObjectArray[key] = null;

				}


			}

		}else{
			this.sltRdoItem = new Array();
		}



		this.checkedEObjectArray[id] = obj.eobject;
		var style;

		if(this.highlightItem&&this.highlightItem[id]){
			//highlightITem is the same as the select item. don't change highlight color.
			style = this.highlightItem[id][1];

		}else{
			style = obj.className;
			obj.className = this.selectStyleClass;

		}


		this.sltRdoItem[id] = new Array(obj, style, obj.eobject);
	}

/**
* @method private DataGrid.prototype.unSltRdoRow
*   This function is used to restore the color of the row
* @param Object obj
*	The row which is be deselected
**/
	DataGrid.prototype.unSltRdoRow = function(obj)
	{

		if(obj!=null&&obj!='undefined'){
			obj.checked = false;
		}

		var id = obj.eobject.ID;

		if(this.sltRdoItem[id]){
			if(this.highlightItem&&this.highlightItem[id]){
				//highlight item is the same as the selected item;

				this.highlightItem[id][1] = this.sltRdoItem[id][1];


			}else{
				obj.className = this.sltRdoItem[id][1];


			}

		}
		this.sltRdoItem[id] = null;
		this.checkedEObjectArray[id] = null;
	}


/**
* @method private DataGrid.prototype.lightRow
*   This function is used to using highlight style sheet to indicate
*	this row is highlighted
* @param Object obj
*	The row which is be highlighted
**/
	DataGrid.prototype.lightRow = function(obj)
	{

		if(this.highlightItem!=null){
			for(var key in this.highlightItem)
			{
				if(this.curPageIDArray[key])
					this.unLightRow(key);
					break;

			}

		}
		this.highlightItem = new Array();

		this.highlightItem[obj.eobject.ID] = new Array(obj, obj.className, obj.eobject);

		obj.className =  this.highlightStyleClass;
	}

/**
* @method private DataGrid.prototype.unLightRow
*   This function is used to restore the color of the once highlighted
*	row when other row is highlighted
* @param String id
*	The eobject id
**/
	DataGrid.prototype.unLightRow = function(id)
	{


		if(this.highlightItem!=null){


			this.highlightItem[id][0].className = this.highlightItem[id][1];

			this.highlightItem = null;
		}

	}




/**

 * @method private DataGrid.prototype.activeEvent
 * 	This method is used to active the corresponding event when user
 *	select or highlight a tree node.
 * @param String action
 *	the actions tree supports, like onHighlight, onSelect
 * @param EObject/String eobject
 *	the selected row's eobject. or sort order etc.
 * @param Object/String extrainfo
 *  extrainfo will be propertyName in onHighlight, other case will be
 *  the selected object.
**/
	DataGrid.prototype.activeEvent = function(action, info, extrainfo)
	{

			if(this.eventArray.length>0){

					for(var i=0; i<this.eventArray.length;i++)
					{

						if(this.eventArray[i][0].toUpperCase() == action.toUpperCase()){

							var handler = this.eventArray[i][1];

							 var e = new ODCEvent(action);

							switch (action.toUpperCase()) {
								case "ONHIGHLIGHT":
									e.eobject = info;
									e.propertyName = extrainfo;
									break;

								case "ONSELECT": case "ONUNSELECT":
									e.eobject=info;
									e.checkedItemsArray = this.compactCheckedItemsArray(extrainfo);
									break;

								case "ONSORT":
									e.sortOrder = info;
									e.propertyName = extrainfo;
									break;

								case "ONPAGE":
									e.pageSelectChoice = info;
									e.pageStartIndex = extrainfo;
									break;

								case "ONFILTER":
									e.filterExpression = this.RowFilter;
									break;

								case "ONSELECTALL":
									e.allEobjs  = this.Adapter.EObjects;
									e.toBeSelectedEobjs = this.compactCheckedItemsArray(info);
									break;
								case "ONUNSELECTALL":
									e.allEobjs = this.Adapter.EObjects;
									e.toBeUnselectedEobjs = this.compactCheckedItemsArray(this.checkedEObjectArray);
									break;
								case "ONROWADD":
									break;
								case "ONROWREMOVE":
									break;										
							}

							  if (eval(handler+".call(this,this, e);") == false)
							   {
								 return false;
							   }



						}
					}

				}
				return true;

	}

/**
 * @method private DataGrid.prototype.compactCheckedItemsArray
 * 	This method is used to clean up the checked items array so that
 *	the event object will only carry the checked node's eobjects.
 * @param Array checkedItems
 *	The array of the original checked item array.
 * @return Array
 *	The compacted Array.
**/
	DataGrid.prototype.compactCheckedItemsArray = function(checkedItems)
	{

		var rtArray = new Array();
		for(var k in checkedItems)
		{
			if(checkedItems[k] != null)
			{
				rtArray[rtArray.length] = checkedItems[k];
			}

		}
		return rtArray;
	}

/**
* @method private OnInternalActivate
*   This function is used to hanlde onHighlight , onSelect, onUnSelect
*	event. Right now, datagrid support "check box" and "radio" button,
*	which behave differently. User can select mulitple rows uisng check
*	box but only one row in radio button. So they are handled differently.
*	Although onHighligh can only highlight one row as radio button but
*	the highlighted row can only be unhighlighted by highlight another row.
*	In radio button case, user can click the radio button again to unselect
*	the selected row.
* @param Object	e
*	window event object.
**/
	DataGrid.prototype.OnInternalActivate = function(e) // private
	{

		try{
			SetupDataGridEventObject(e);

			var rowObj = window.event.srcElement;
			while(!rowObj.dg){
				rowObj = rowObj.parentNode;
			}

			var datagrid = rowObj.dg;
			var eobj = rowObj.eobject;
			var propName = rowObj.propName;

			if(datagrid==null||datagrid=='undefined')
				return;


			var src = window.event.srcElement;
			if(src.type == "checkbox"){
					var id = src.value;
					if(datagrid.selectedItem&&datagrid.selectedItem[id]){
						//uncheck the check box

							if(!datagrid.activeEvent("onUnselect", eobj, datagrid.checkedEObjectArray))
								return;
							datagrid.unSelectRow(rowObj);


					}else{

							//check the checkbox

							if(!datagrid.activeEvent("onSelect", eobj, datagrid.checkedEObjectArray))
								return;

							datagrid.selectRow(rowObj);

					}




			}else if(src.type == "radio")
			{
					var id = src.value;

					if(datagrid.sltRdoItem&&datagrid.sltRdoItem[id]){
						//unselect the select radio if user click the select radio again.

							if(!datagrid.activeEvent("onUnselect", eobj, datagrid.checkedEObjectArray))
									return;

							datagrid.unSltRdoRow(rowObj);

					}else{
						//select the radio button

							if(!datagrid.activeEvent("onSelect", eobj, datagrid.checkedEObjectArray))
								return;

							datagrid.sltRdoRow(rowObj);

				}





			}
			else{
					//IBM home page reader is getting text as source element, IE browser or click on HPR is getting <a> as source element.
					//so in the first situation, I have to get one more level up to get the row object.

					if(!datagrid.activeEvent("onHighlight", eobj, propName))
						return;

					datagrid.lightRow(rowObj);


				}
		}catch(e){
		}




	}


/**
* @method private DataGrid.prototype.getImageLink
*   This function is used to get the navigation bar's linked image object
* @param integer type
*	the type of the image.
*	0-transparent skip to main content image
*	1-previous image
*   2-next image
*	3-first page image
*	4-last page image
*	5-go to image
*   6-select/unselect image
*	7-add new row
*	8-delete row
*	9-accept new row
*	10-cancel new row
* @return object
*	The html a link object
**/

	DataGrid.prototype.getImageLink = function(type,pos)
	{
		var a = document.createElement("a");
		var img = document.createElement("img");
		img.border = 0;
		//img.className = this.styleClass+"wpsPagingTableHeaderIcon";

		switch (type) {

			case 0:

				img.setAttribute("src",  this.datagridIcons["clear"]);
				img.setAttribute("alt", odcDGSkipToMainContent);
				a.setAttribute("href","#navskip"+this.ID+this.currentPage);
				a.appendChild(img);
				break;

			case 1:

				if (this.GetPreviousStartIndex() != -1) {
					img.setAttribute("src", this.datagridIcons["previous"]);
					img.setAttribute("alt", odcDGImgPrevious);
					var code = "window.document.getElementById('" + this.ID + "').GridControl.PreviousPage();";
					a.setAttribute("href","javascript:" + code);
					a.appendChild(img);
				}
				else{
					//img.setAttribute("src", this.datagridIcons["disprevious"]);
					img.setAttribute("src", this.datagridIcons["clear"]);
					a = img;
				}
				break;

			case 2:

				if (this.GetNextStartIndex() != -1) {
					img.setAttribute("src", this.datagridIcons["next"]);
					img.setAttribute("alt", odcDGImgNext);
					var code = "window.document.getElementById('" + this.ID + "').GridControl.NextPage();";
					a.setAttribute("href","javascript:" + code);
					a.appendChild(img);
				}
				else{
					//img.setAttribute("src", this.datagridIcons["disnext"]);
					img.setAttribute("src", this.datagridIcons["clear"]);
					a = img;
				}
				break;

			case 3:

					if (this.GetFirstPageStartIndex() != -1) {
						img.setAttribute("src", this.datagridIcons["start"]);
						img.setAttribute("alt", odcDGImgFirst);
						a= document.createElement("a");
						var code = "window.document.getElementById('" + this.ID + "').GridControl.FirstPage();";
						a.setAttribute("href","javascript:" + code);
						a.appendChild(img);
					}
					else{
						//img.setAttribute("src", this.datagridIcons["disstart"]);
						img.setAttribute("src", this.datagridIcons["clear"]);
						a = img;
					}

				break;

			case 4:


					if (this.GetLastPageStartIndex() != -1) {
						img.setAttribute("src", this.datagridIcons["end"]);
						img.setAttribute("alt", odcDGImgLast);
						a = document.createElement("a");
						var code = "window.document.getElementById('" + this.ID + "').GridControl.LastPage();";
						a.setAttribute("href","javascript:" + code);


						a.appendChild(img);
					}
					else{
						//img.setAttribute("src", this.datagridIcons["disend"]);
						img.setAttribute("src", this.datagridIcons["clear"]);

						a = img;
					}
				break;

			case 5:

				img.style.marginLeft = "6px";
				img.setAttribute("src", this.datagridIcons["go"]);
				img.setAttribute("alt", odcDGImgGo);




				code = "window.document.getElementById('" + this.ID + "').GridControl.selectPage(-1)";
				a.setAttribute("href","javascript:"+code);

				a.appendChild(img);

				break;

			case 6:


				if(this.selAll){
					//already selected all so the select icon is should be unselect.
					img.setAttribute("src", this.datagridIcons["unselectall"]);
					img.setAttribute("alt", odcDGImgUnSelectAll);
					code = "window.document.getElementById('" + this.ID + "').GridControl.selectAll()";

				}else{


					img.setAttribute("src", this.datagridIcons["selectall"]);
					img.setAttribute("alt", odcDGImgSelectAll);
					code = "window.document.getElementById('" + this.ID + "').GridControl.selectAll(true)";

				}

				img.id = this.ID+"dgimgselectall"+pos;

				//img.className = this.styleClass+"wpsPagingTableToolIcon";

				a.setAttribute("href","javascript:"+code);

				a.appendChild(img);

				break;
			case 7:

				//for add new row button on navigation bar
				img.setAttribute("src", this.datagridIcons["addrow"]);
				img.setAttribute("alt", odcDGImgAddRow);
				code = "window.document.getElementById('" + this.ID + "').GridControl.SetAddMode()";

				a.setAttribute("href","javascript:"+code);

				a.appendChild(img);

				break;

			case 8:

				//for remove icon
				img.setAttribute("src", this.datagridIcons["delrow"]);
				img.setAttribute("alt", odcDGImgDelRow);
				//img.className = "";
				code = "window.document.getElementById('" + this.ID + "').GridControl.RemoveRow(" + pos + ");";

				a.setAttribute("href","javascript:"+code);

				a.appendChild(img);

				break;
			case 9:

				//for accept icon
				img.setAttribute("src", this.datagridIcons["acceptrow"]);
				img.setAttribute("alt", odcDGImgAcceptRow);

				code = "window.document.getElementById('" + this.ID + "').GridControl.AddRow();";

				a.setAttribute("href","javascript:"+code);

				a.appendChild(img);

				break;
			case 10:

				//for cancel icon
				img.setAttribute("src", this.datagridIcons["cancelrow"]);
				img.setAttribute("alt", odcDGImgCancelRow);

				code = "window.document.getElementById('" + this.ID + "').GridControl.CancelAddMode();";

				a.setAttribute("href","javascript:"+code);

				a.appendChild(img);

				break;


			default:
				break;
		}

		return a;



	}



/**
* @method private DataGrid.prototype.constructNavBar
*   This function is used to construct the datagrid's navigation bar
* 	to navigate the different pages of the datagrid table
* @param Object td
*	a html td object
* @param String pos
*	the position of the navigation bar
**/

	DataGrid.prototype.constructNavBar = function(td,pos)
	{
		/*
			the sample html code from web develper guide
		          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="wpsHeaderTable-lotus">
		            <tr>
		              <td class="wpsPagingTableHeaderStart-lotus" width="100%" nowrap="nowrap">&nbsp;</td>

		              <td class="wpsPagingTableHeaderMiddle-lotus"><a href="#"><img class="wpsPagingTableHeaderIcon-lotus" src="FirstPage.gif" alt="Go to first page" border="0" /></a></td>

		              <td class="wpsPagingTableHeaderMiddle-lotus"><a href="#"><img class="wpsPagingTableHeaderIcon-lotus" src="PreviousPage.gif" alt="Go to previous page" border="0" /></a></td>

		              <td class="wpsPagingTableHeaderMiddle-lotus" nowrap="nowrap">Page 1 of 6</td>

		              <td class="wpsPagingTableHeaderMiddle-lotus"><a href="#"><img class="wpsPagingTableHeaderIcon-lotus" src="NextPage.gif" alt="Go to next page" border="0" /></a></td>

		              <td class="wpsPagingTableHeaderMiddle-lotus"><a href="#"><img class="wpsPagingTableHeaderIcon-lotus" src="LastPage.gif" alt="Go to last page" border="0" /></a></td>

		              <td class="wpsPagingTableHeaderMiddle-lotus" nowrap="nowrap">Jump to page:</td>

		              <td class="wpsPagingTableHeaderMiddle-lotus"><input class="wpsEditField-lotus" type="text" name="page" size="3" /></td>

		              <td class="wpsPagingTableHeaderMiddle-lotus"><a href="#"><img class="wpsPagingTableHeaderIcon-lotus" src="Go.gif" alt="Jump to page" border="0" /></a></td>

		              <td class="wpsPagingTableHeaderEnd-lotus">&nbsp;</td>
		            </tr>
          </table>
		*/
			this.totalPage = Math.ceil(this.totalRows/this.PageSize);
			if(this.totalPage==0)
				return;

			var cellnum = 0;

			var navtable = document.createElement("table");
			navtable.id = this.ID+"navtable"+pos;

			navtable.style.width = "100%";
			navtable.border = "0";
			navtable.cellSpacing = "0";
			navtable.cellPadding = "0";

			navtable.className = this.styleClass+((pos=="top")?"wpsHeaderTable":"wpsFooterTable");

			//get all the linked image object
			var imageArray = new Array();
			for(var i=0; i<8; i++)
			{
				imageArray[i] = this.getImageLink(i,pos);
			}


			var textnode = document.createElement("label");
			textnode.innerHTML = "&nbsp;";

			var tr1 = navtable.insertRow(cellnum++);

			var td1 = tr1.insertCell(0);


			td1.className =this.styleClass+((pos=="top")?"wpsPagingTableHeaderStart":"wpsPagingTableFooterStart");

			if(pos=="top"&& this.styleClass == "tivoli_")
				td1.style.backgroundImage="url(" + this.datagridIcons["wpsPagingTableHeaderStart"] + ")";


			td1.appendChild(textnode);

			var middleClass = this.styleClass+((pos=="top")?"wpsPagingTableHeaderMiddle":"wpsPagingTableFooterMiddle");

			//for select all icon
			var td12 = tr1.insertCell(cellnum++);
			td12.noWrap = "true";
			td12.className = middleClass;

			var navNode12;
			if(this.selectCol&&this.selectCol[1]=="checkbox"){
				//add select all button
				navNode12 = imageArray[6];


			}else{

				navNode12 = document.createElement("label");
				navNode12.innerHTML = "&nbsp;";
			}



			td12.appendChild(navNode12);

			//for add new row icon
			var td13 = tr1.insertCell(cellnum++);
			td13.style.width = "100%";
			td13.noWrap = "true";
			td13.className = middleClass;

			var navNode13;
			if(!this.ReadOnly){
				navNode13 = imageArray[7];


			}else{

				navNode13 = document.createElement("label");
				navNode13.innerHTML = "&nbsp;";

			}

			td13.appendChild(navNode13);

			var td2 = tr1.insertCell(cellnum++);
			td2.className=middleClass;
			td2.appendChild(imageArray[0]);


			var td3 = tr1.insertCell(cellnum++);
			td3.className=middleClass;
			td3.appendChild(imageArray[3]);


			var td4 = tr1.insertCell(cellnum++);
			td4.className=middleClass;
			td4.appendChild(imageArray[1]);


		//	if(this.totalPage==1)
				this.totalPage = Math.ceil(this.totalRows/this.PageSize);
			var args = new Array;
			args[0] = this.currentPage;
			args[1] = this.totalPage;

			var Msg = NlsFormatMsg(odcDGNavigationPage, args);

			var td5 = tr1.insertCell(cellnum++);
			td5.noWrap = "true";
			td5.className=middleClass;
			td5.appendChild(document.createTextNode(Msg));


			var td6 = tr1.insertCell(cellnum++);
			td6.className=middleClass;
			td6.appendChild(imageArray[2]);

			var td7 = tr1.insertCell(cellnum++);
			td7.className=middleClass;
			td7.appendChild(imageArray[4]);




			if (this.totalPage > 1) {

				var td8 = tr1.insertCell(cellnum++);

				td8.noWrap = "true";
				td8.className=middleClass;
				td8.appendChild(document.createTextNode(odcDGNavigationJump));

				var inputObj = null;

				var code = "window.document.getElementById('" + this.ID + "').GridControl.selectPage(this.value)";

				//"onKeyPress" don't work if don't write seperate for IE and NS.
				var cssStyle = this.styleClass+"wpsEditField";


				if(isIE()){

					var inputStr = "<INPUT type='text' class='"+cssStyle+"' id='goPageNum_"+pos+this.ID+"' size='3' onKeyPress = 'checkEnter1(this,event)'>";

					inputObj = document.createElement(inputStr);

				}else{
					inputObj = document.createElement("input");
					inputObj.setAttribute("type", "text");
					inputObj.setAttribute("id", "goPageNum_"+pos+this.ID);
					inputObj.setAttribute("class", cssStyle);
					inputObj.setAttribute("size", "3");
					inputObj.setAttribute("onKeyPress", "checkEnter1(this,event)");
					//inputObj.setAttribute("onchange", code);

				}

				var td9 = tr1.insertCell(cellnum++);
				td9.className=middleClass;
				td9.appendChild(inputObj);

				var td10 = tr1.insertCell(cellnum++);
				td10.className=middleClass;
				td10.appendChild(imageArray[5]);

			}




			var td11 = tr1.insertCell(cellnum++);

			td11.className = this.styleClass+((pos=="top")?"wpsPagingTableHeaderEnd":"wpsPagingTableFooterEnd");
			if(this.styleClass == "dataGrid_"|| this.styleClass == "lotus_"){
				if(pos=="top")
					td11.style.backgroundImage="url(" + this.datagridIcons["wpsPagingTableHeaderEnd"] + ")";

				else

					td11.style.backgroundImage="url(" + this.datagridIcons["wpsPagingTableFooterEnd"] + ")";
			}

			var textnode2 = document.createElement("label");
			textnode2.innerHTML = "&nbsp;";


			td11.appendChild(textnode2);

			td.appendChild(navtable);


}

/**
* @method private checkEnter1
*   This function is used to active the select page if the user hit
* 	enter key after specifing the page number he want to go.
* @param Object td
*	a html td object
* @param Object e
*	the window event
**/
function checkEnter1(cell, e)
{
	var charCode=(isIE())?e.keyCode:e.which;
	if(charCode== 13)
	{
		var id = cell.id.substring(13);

		window.document.getElementById(id).GridControl.selectPage(cell.value);
	}


	return false;
}


/**
* @method private DataGrid.prototype.setupHead
*   This function is used to setup the header of the datagrid table
*	and pass the columns number and column size to datagrid control.
* @param Object tr
*	The html tr object
**/
	DataGrid.prototype.setupHead = function(tr) {

/*
            <tr class="wpsTableHead-lotus">
              <!-- class="table-lotus-header-link"  -->

              <th title="Click to sort ascending" class="wpsTableHeadStart-lotus wpsDataAlignCenter-lotus" style="cursor: pointer;" onclick="alert('This would sort the column.')">Select<img src="n-sort-trans.gif" width="9" height="5" alt="" hspace="3" border="0" /></th>

              <th title="Click to sort ascending" class="wpsTableHeadMiddle-lotus wpsDataAlignRight-lotus" style="cursor: pointer;">Number<img src="n-sort-trans.gif" width="9" height="5" alt="" hspace="3" border="0" /></th>

              <th title="Click to sort ascending" class="wpsTableHeadMiddle-lotus" style="cursor: pointer;">Date<img src="n-sort-trans.gif" width="9" height="5" alt="" hspace="3" border="0" /></th>

              <th title="Click to sort ascending" class="wpsTableHeadMiddle-lotus" style="cursor: pointer;">First Name<img src="n-sort-trans.gif" width="9" height="5" alt="" hspace="3" border="0" /></th>

              <th title="Click to sort descending" class="wpsTableHeadMiddle-lotus" style="cursor: pointer;">Last Name<img src="n-sort-a-z.gif" width="9" height="5" alt="Click to sort descending" hspace="3" border="0" /></th>

              <th title="Click to sort ascending" class="wpsTableHeadEnd-lotus wpsDataAlignCenter-lotus" style="cursor: pointer;">Exempt<img src="n-sort-trans.gif" width="9" height="5" alt="" hspace="3" border="0" /></th>
            </tr><!-- End column headers -->


*/
		this.readOnlyTemplate = new Array();

		if(this.metaData ==null)
			return;

		var newObject = (this.Adapter.EObjects!=null)?this.Adapter.EObjects[0]:null;
		if(!newObject) newObject = this.Adapter.previousEobject; //so use the previous eobject to construct the head since head may change.


		try{


			if(this.selectCol)
			{
				var selectionheader = null;
				//Use header passed in
				if(this.selectCol[0])
				{
				   selectionheader =  this.selectCol[0];
				}
				//use the default header from odysseyMessage_xx.js
				else
				{
					selectionheader = odcDGSelection
				}
				txt = document.createTextNode(selectionheader);

				this.constructCell(tr,"0", txt);
				this.NumberOfCols++;
			}

			if(this.showRowIndex)
			{
				var txt = document.createTextNode(NlsFormatMsg(odcdgindexhead,null));

				this.constructCell(tr,"1", txt);
				this.NumberOfCols++;

			}


			var ContentCols = 0;


			for(var j=0; j<this.metaData.length; j++){

				var str = this.metaData[j][0];
				var member = (newObject)?newObject.GetMember(str):null;

				if (this.ColumnTitles != null && ContentCols < this.ColumnTitles.length)
					str = this.ColumnTitles[ContentCols];

				var size = this.metaData[j][1][2];

				txt = document.createTextNode(str);
				this.constructCell(tr,"2", txt, member, this.metaData[j][1][1]);

				this.NumberOfCols++;

				++ContentCols;



			}

//for the editable table, the last col will be the delete column, so the header will be blank.

			if(!this.ReadOnly)
			{
				var th = document.createElement("th");
				tr.appendChild(th);
				th.className = th.className = this.styleClass+"wpsTableHeadEnd";

			}


		}catch(e)
		{
				if (this.OnError != null)
				this.OnError(action_datagrid_buildrow,e);

				return;

		}
	//	this.NumberOfCols = ContentCols;

	}



/**
* @method private DataGrid.prototype.constructCell
*   This function is used to construct the table head cell
* @param object tr
*	The html tr object
* @param String type
*   The type can be "0", "1" or "2", "0" means this is a select
*	column, "1" means this is index column. "2" means tthis is regular
*	column
* @param String txt
*   The title of the head
* @param String memberName
*   The name of the member which is used to sort the regular column
* @param String align
*	The alignment of this cell.
**/
DataGrid.prototype.constructCell = function(tr, type, txt, member,align)
{


			var th = document.createElement("th");
			tr.appendChild(th);


			if(this.NumberOfCols ==0)
				th.className = this.styleClass+"wpsTableHeadStart";

			else if(!this.ReadOnly){
				th.className = this.styleClass+"wpsTableHeadMiddle";

			}else if(this.selectCol&&this.showRowIndex&&(this.NumberOfCols-2 == this.metaData.length-1))
			{
				//to detect if this column is the last column so that use different style sheet.
				th.className = this.styleClass+"wpsTableHeadEnd";


			}else if((this.selectCol||this.showRowIndex)&&(this.NumberOfCols==this.metaData.length))
			{
				th.className = this.styleClass+"wpsTableHeadEnd";
			}else
				th.className = this.styleClass+"wpsTableHeadMiddle";



			//for accessibility reason, use <a> tag for IE and NS.
			var alink = document.createElement("a");
			//NS didn't work if you don't specify "javascript:;".
			alink.setAttribute("href", "javascript:;");
			alink.className = this.styleClass+"wpsDataAlink";

			alink.appendChild(txt);

			var imgSrc = this.datagridIcons["clear"]

			var img = document.createElement("img");
			img.style.marginLeft = "4px";

			img.setAttribute("src", imgSrc);
			img.setAttribute("class", this.styleClass+"wpsTableHeaderSortIcon");
			img.setAttribute("border", 0);
			img.id = this.ID+type;

			alink.appendChild(img);
			th.appendChild(alink);
			if(!isodcdgIE5)
				th.style.cursor = "pointer";

			th.onclick = onHeaderSelected;
			th.DataGrid = this;
			th.type = type;


			switch (type) {
				case "0":
					this.selectHead = th;
					this.readOnlyTemplate[this.NumberOfCols] = true;

					var mcss;
					if(this.selectCol[2]&&this.selectCol[2].toUpperCase()=="CENTER")
						mcss = this.styleClass+"wpsDataAlignCenter";

					if(this.selectCol[2]&&this.selectCol[2].toUpperCase()=="RIGHT")
						mcss = this.styleClass+"wpsDataAlignRight";

					th.className = th.className + " " + mcss;

					th.membername = "select column";//this is selection column

					break;

				case "1":
					this.indexHead = th;
					th.className = th.className  + " "+ this.styleClass+" wpsDataAlignCenter";
					this.readOnlyTemplate[this.NumberOfCols] = true;
					th.membername = "index column";//this is index column

					break;

				case "2":
					this.headArray[this.headArray.length] = th;
					if(member){
						this.readOnlyTemplate[this.NumberOfCols] = (member.EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTECALCULATE)?true:false;
						img.id = this.ID + type + member.Name;
						th.membername = member.Name;
					}else
						//what kind of case?
						this.readOnlyTemplate[this.NumberOfCols] = true;

					var mcss;
					if(align&&align.toUpperCase()=="CENTER")
						mcss = this.styleClass+"wpsDataAlignCenter";

					if(align&&align.toUpperCase()=="RIGHT")
						mcss = this.styleClass+"wpsDataAlignRight";

					th.className = th.className + " " + mcss;



					break;

				default:

					break;
			}




}


/**
* @method private onHeaderSelected
*   The call back function is called when datagrid header is clicked.
* @param Event evt
*	window event object.
**/
function onHeaderSelected(evt) {



	var sourceObj 	= (evt!=null)?evt.currentTarget:window.event.srcElement;
	if(isIE()){
		sourceObj = getdatagridCtrl(sourceObj);

	}

	var datagrid = sourceObj.DataGrid;
	var membername = sourceObj.membername;

	var sort;
	switch (sourceObj.type) {

		case "0":

			if(datagrid.sortOrder){
				if(datagrid.sortOrder["odcselect"]){
					sort = (datagrid.sortOrder["odcselect"]=='asc')?'desc':'asc';
				}else{
					sort = datagrid.sortOrder["odcselect"]='asc';
				}
			}else{
				datagrid.sortOrder = new Array();
				sort = "asc";
			}

			//add onSort event(with information about which column is being sorted, and whether sorting is up or down)
			//called before sorting actually occurs

			if(!datagrid.activeEvent("onSort", sort, membername))
				return;

			datagrid.Adapter.SortSel(sort);
			datagrid.sortState = "odcselect " + sort;
			datagrid.sortOrder["odcselect"] = sort;



			break;

		case "1":

			if(datagrid.sortOrder){
				if(datagrid.sortOrder["odcindex"]){
					sort = (datagrid.sortOrder["odcindex"]=='asc')?'desc':'asc';
				}else{

					sort = datagrid.sortOrder["odcindex"]='asc';
				}

			}else{
				datagrid.sortOrder = new Array();
				sort = "asc";
			}


			//add onSort event(with information about which column is being sorted, and whether sorting is up or down)
			//called before sorting actually occurs

			if(!datagrid.activeEvent("onSort", sort, membername))
				return;

			datagrid.Adapter.SortIndex(sort);
			datagrid.sortState = "odcindex " + sort;
			datagrid.sortOrder["odcindex"] = sort;


			break;

		case "2":

			if(datagrid.sortOrder){

				if(datagrid.sortOrder[sourceObj.membername]){
					sort = (datagrid.sortOrder[sourceObj.membername]=='asc')?'desc':'asc';
				}else{
					sort = datagrid.sortOrder[sourceObj.membername]='asc';
				}

			}else{
				datagrid.sortOrder = new Array();
				sort = "asc";
			}


			//add onSort event(with information about which column is being sorted, and whether sorting is up or down)
			//called before sorting actually occurs

			if(!datagrid.activeEvent("onSort", sort, membername))
				return;


			datagrid.Adapter.Sort(sourceObj.membername+ ' '+ sort);
			datagrid.sortState = sourceObj.membername + " " + sort;
			datagrid.sortOrder[sourceObj.membername] = sort;


			break;

		default:
			break;
	}
	sourceObj.so = sort;
	datagrid.ChangeSortImg(sourceObj);

	return false;



}

/**
* @method private ChangeSortImg
*   This function is used to change the image to "desc" from "asc"
*	if the column is sorted as "asc" and clicked again to "desc"
* 	or other column is clicked so that this column image will be
*	transparent, and other column will display "asc" or "desc" based
*	on the previous action
* @param Object cellObj
*   The column's head which is clicked to sort
**/
	DataGrid.prototype.ChangeSortImg = function(cellObj)
	{


		if(cellObj == null) return;

		if(this.preSelHead)
		{
			var img = this.changSortImgInternal(this.preSelHead);

			img.setAttribute("src", this.datagridIcons["clear"]);


		}


		var contentCell = cellObj.firstChild;

		var img = this.changSortImgInternal(cellObj);

		var imgSrc = (cellObj.so == "asc") ? this.datagridIcons["sortup"]:this.datagridIcons["sortdown"];

		img.setAttribute("src", imgSrc);

		//alert(img.src);
		var sortAlt = (cellObj.so == "asc") ? odcDGAscending:odcDGDescending;

		img.setAttribute("alt", sortAlt);

		this.preSelHead = cellObj;


	}


	DataGrid.prototype.changSortImgInternal = function(cellobj)
	{
			var contentCell = cellobj.firstChild;

			var id;
			var name = cellobj.membername;

			if(name&&name!="select column" &&name!="index column")
				id = this.ID+cellobj.type+cellobj.membername;
			else
				id = this.ID+cellobj.type;


			return document.getElementById(id);

	}



/**
* @method private getdatagridCtrl
*   This function is used to get datagrid control from the clicked
*	header. Only for IE since IE return alink or image object instead
*	of cell when the cell is clicked.
* @param Object srcObj
*	IE source object.
**/
function getdatagridCtrl(srcObj)
{

	var datagrid = srcObj.DataGrid;

	while(!datagrid)
	{
		srcObj = srcObj.parentNode;
		datagrid = srcObj.DataGrid;


	}
	return srcObj;


}


/**
* @method public DataGrid.prototype.show
*   This function is used to construct the datagrid table which not
*	only has the data content also include one column of "delete"
*	link to deleted that row, a "add row" link to add a new row.
*	and a navigation bar to navigate the different pages of the
*	datagrid table
**/

	DataGrid.prototype.show = function()		// public
	{
		this.ShowGrid();

	}



/**
* @method private DataGrid.prototype.ShowGrid
*   This function is used to construct the datagrid table which not
*	only has the data content also include one column of "delete"
*	link to deleted that row, a "add row" link to add a new row.
*	and a navigation bar to navigate the different pages of the
*	datagrid table
* @param boolean update
*	This flag is used to decide if display or refresh the datagrid
**/

	DataGrid.prototype.ShowGrid = function(update)		// public
	{
/*
		//if the datagrid is not bound to any datamodel, then pop up alert msg and display nothing.
		if(this.Adapter.EObjects==null)
		{
			alert(action_obfcontrolrender_errormsg);
			return;

		}
*/

		//get data
		this.curPageIDArray = new Array();

		if(this.DataArray == null)
			this.refreshDataArray();

/*
//this is the format of the datagrid table which is from Tom Spine's web developement guide.
    <table width="100%" border="1" cellspacing="0" cellpadding="0" class="wpsPagingTable-lotus">
      <!-- Begin paging header -->

       <tr>
        <td class="wpsPagingTableHeader-lotus">
			<table>
			************table of navigation bar*******************
			</table>
        </td>
       </tr>
       <tr>
        <td>
        	<table>
        	************table of content**************************
        	</table>
        </td>
       </tr>
       <tr>
        <td>
        	<table>
        	************table of foot which is navigation bar again*******
        	</table>
        </td>
       </tr>
     </table>

*/
	if(!update){

		if(this.Adapter.EObjects == null){
			this.NavBarPosition = -1;
		}else{

			if(this.NavBarPosition <0){
				//in case user set no navigation bar, but the page size is smaller than the data size.automatically add navigation bar.
				if(this.Adapter.EObjects.length>this.PageSize)
					this.NavBarPosition = 0;
			}
			/**
			* Removed by Graham 20-jan-2005 to resolve defect RATLC00978407, navigation bars not
			* displayed when data updated from outside datagrid and add/remove rows option not selected.
			* else{
			* 	//in case no data on that page, so don't display navigation bar
			* 	if(this.Adapter.EObjects.length<this.PageSize&&this.ReadOnly)
			* 		this.NavBarPosition = -1;
			* }
			**/
		}



		this.HTMLTable.className = this.styleClass+"wpsPagingTable";
		this.HTMLTable.style.width = this.width;
		this.HTMLTable.border = "0";
		this.HTMLTable.cellSpacing = "0";
		this.HTMLTable.cellPadding = "0";

		var dgi=0;
		if(this.AllowPaging&&(this.NavBarPosition==1||this.NavBarPosition==2))
		{
			var headtr = this.HTMLTable.insertRow(dgi);
			var headtd = headtr.insertCell(0);
			headtd.id = this.ID+"headtd";
			headtd.className = this.styleClass+"wpsPagingTableHeader";

			this.constructNavBar(headtd,"top");
			dgi++;
		}

		var bodytr = this.HTMLTable.insertRow(dgi);
		var bodytd = bodytr.insertCell(0);
		bodytd.id = this.ID+"bodytd";
		bodytd.className = this.styleClass+"wpsPagingTableBody";

		this.constructBody(bodytd);
		dgi++;

		if(this.AllowPaging&&(this.NavBarPosition==0||this.NavBarPosition==2)){

			var foottr = this.HTMLTable.insertRow(dgi);
			var foottd = foottr.insertCell(0);
			foottd.id = this.ID + "foottd";
			foottd.className = this.styleClass+"wpsPagingTableFooter";

			this.constructNavBar(foottd,"bot");
		}
	}else{
		this.updateDatagrid();
	}


}
/**
* @method private DataGrid.prototype.updateDatagrid
*   This function is used to refresh the datagrid using the new data
**/
DataGrid.prototype.updateDatagrid = function()
{

		var headtd = window.document.getElementById(this.ID+"headtd");
		if(headtd)
		{

				while (headtd.hasChildNodes())
					headtd.removeChild(headtd.lastChild);
				this.constructNavBar(headtd, "top");

		}


		var bodytd = window.document.getElementById(this.ID+"bodycontenttd");
		if(bodytd)
		{
				this.setupContent(bodytd,true);
		}

		var foottd = window.document.getElementById(this.ID+"foottd");
		if(foottd)
		{
				while (foottd.hasChildNodes())
					foottd.removeChild(foottd.lastChild);
				this.constructNavBar(foottd, "bot");

		}


}

/**
* @method private DataGrid.prototype.constructBody
*   This function is used to construct the body part of the datagrid
* @param Object td
*	The html td object
**/
DataGrid.prototype.constructBody = function(td)
{
	/*
	          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="wpsTable-lotus">
	            <!-- Begin column headers -->

	            <tr class="wpsTableHead-lotus">
	              <!-- class="table-lotus-header-link"  -->

	              <th title="Click to sort ascending" class="wpsTableHeadStart-lotus wpsDataAlignCenter-lotus" style="cursor: pointer;" onclick="alert('This would sort the column.')">Select<img src="n-sort-trans.gif" width="9" height="5" alt="" hspace="3" border="0" /></th>

	              <th title="Click to sort ascending" class="wpsTableHeadMiddle-lotus wpsDataAlignRight-lotus" style="cursor: pointer;">Number<img src="n-sort-trans.gif" width="9" height="5" alt="" hspace="3" border="0" /></th>

	              <th title="Click to sort ascending" class="wpsTableHeadMiddle-lotus" style="cursor: pointer;">Date<img src="n-sort-trans.gif" width="9" height="5" alt="" hspace="3" border="0" /></th>

	              <th title="Click to sort ascending" class="wpsTableHeadMiddle-lotus" style="cursor: pointer;">First Name<img src="n-sort-trans.gif" width="9" height="5" alt="" hspace="3" border="0" /></th>

	              <th title="Click to sort descending" class="wpsTableHeadMiddle-lotus" style="cursor: pointer;">Last Name<img src="n-sort-a-z.gif" width="9" height="5" alt="Click to sort descending" hspace="3" border="0" /></th>

	              <th title="Click to sort ascending" class="wpsTableHeadEnd-lotus wpsDataAlignCenter-lotus" style="cursor: pointer;">Exempt<img src="n-sort-trans.gif" width="9" height="5" alt="" hspace="3" border="0" /></th>
	            </tr><!-- End column headers -->
	            <!-- Begin data rows -->

	            <tr class="wpsTableNrmRow-lotus wpsCheckBoxRow-lotus">
	              <td class="wpsTableDataStart-lotus wpsDataAlignCenter-lotus"><input type="checkbox" name="row1" value="" /></td>

	              <td class="wpsTableDataMiddle-lotus wpsDataAlignRight-lotus">125,864</td>

	              <td class="wpsTableDataMiddle-lotus">July 8, 2004</td>

	              <td class="wpsTableDataMiddle-lotus">John</td>

	              <td class="wpsTableDataMiddle-lotus">Able</td>

	              <td class="wpsTableDataEnd-lotus wpsDataAlignCenter-lotus"><img src="tivoli-checkmark.gif" width="14" height="14" alt="" border="0" /></td>
            </tr>
*/


			var bodytable = document.createElement("table");
			bodytable.id = this.ID +"bodycontenttd";
			td.appendChild(bodytable);
			bodytable.style.width = "100%";
			bodytable.border = "1";
			bodytable.cellSpacing = "0";
			bodytable.cellPadding = "0";
			bodytable.className = this.styleClass+"wpsTable";


			var tr1 = bodytable.insertRow(0);
			tr1.className = this.styleClass+"wpsTableHead";


			this.setupHead(tr1);


			this.setupContent(bodytable);



}

/**
* @method private DataGrid.prototype.setupContent
*   This function is used to construct the body content of the datagrid
* @param Object bodytable
*	The html table object
* @param boolean update
*	boolean to decide to display or refresh the datagrid
**/
DataGrid.prototype.setupContent=function(bodytable,update)
{

		//construct the content of table

		for (var i = 0; i < this.PageSize; ++i) {
			var singleRowContent = (i < this.DataArray.length) ? this.DataArray[i] : null;


			var tr = (!update)?bodytable.insertRow(i+1):bodytable.rows[i+1];

			tr.className = this.normalStyleClass;

			this.setupColumns(tr,singleRowContent,i);


		}


		//construct one more line for new line if read only is false;
		if(!this.ReadOnly&&isIE()&&!update){
			tr = bodytable.insertRow(i+1);

			tr.id = this.ID+"newrow";

			tr.style.display ="none";


		}

}

/**
* @method private setupColumns
*   This function is used to construct the datagrid table content using the data set which is from data
*	grid adapter.Here three different cases need to be considered since we use the same tr and td object
*	across different page.
*		1. for example, the first page has 5 rows and the second page has 3 rows, so the last two rows content
*			has to be wiped out.
*		2. If the user set the page size as 5, but the data set only has 3 rows, so two blank rows need to
*			constructed.
*		3. normal situation, just wiped out the old content using the new one.
* @param Object	tr
*	The tr object which is used to construct that row.
* @param Array	rowContentArray
*	The data set array which has all the data need to be displayed.
* @param int index
*	The position of this to be constructed row in the whole table.
**/

DataGrid.prototype.setupColumns=function(tr,rowContentArray, index)
{
		if (null == tr)
			return;
		var height = tr.offsetHeight;
		if (null == rowContentArray || 'undefined' == typeof(rowContentArray)) {
			//update page
			//overwrite the previous content with blank
			if(tr.cells.length >0){
				for(var i=0; i<tr.cells.length; i++){
					var width = tr.cells[i].offsetWidth;
					while (tr.cells[i].hasChildNodes())
						tr.cells[i].removeChild(tr.cells[i].lastChild);


					var child = document.createElement("label");
					child.innerHTML = ".";
					child.style.visibility = "hidden";
					tr.id = null;
					tr.dg = null;
					tr.eobject = null;
					tr.propName = null;
					if(height>0)
						tr.height = height;

					tr.cells[i].appendChild(child);


			     }
			}else{


				//new page
				//if the first page's rows less than the page size.

				var cols = (this.ReadOnly)?this.NumberOfCols:this.NumberOfCols+1;
				for(var j=0; j<cols; j++){
					var child = document.createElement("label");
					child.innerHTML = ".";
					child.style.visibility = "hidden";
					var td = document.createElement("td");
					td.appendChild(child);
					tr.appendChild(td);

				}
			}
			return;
		}

		var eobj = this.Adapter.EObjects[(this.currentPage-1)*this.PageSize+index];
		Log.debug("datagrid.setupcolumns()", eobj.toStr());
		var id = eobj.ID;
		var signature = eobj.getSignature();

		tr.id = eobj.ID;
		tr.dg = this;
		tr.eobject = eobj;

		if(!(this.curPageIDArray&&this.curPageIDArray[id])) this.curPageIDArray[id] = signature;

		var cols = 0;
		//for selection column
		if(this.selectCol){
			var td = getTD(tr,cols);

			if(this.selectCol[2]&&this.selectCol[2].toUpperCase()=="CENTER")
				td.className = this.styleClass+"wpsTableDataStart" + " "+ this.styleClass +"wpsDataAlignCenter";
			if(this.selectCol[2]&&this.selectCol[2].toUpperCase()=="RIGHT")
				td.className = this.styleClass+"wpsTableDataStart" + " "+ this.styleClass + "wpsDataAlignRight";


			if(this.selectCol[3]>0) td.width = this.selectCol[3];

			//when page switch back to the previous page, the selected row need to be selected.
			if((this.selectedItem&&this.selectedItem[id])||(this.sltRdoItem&&this.sltRdoItem[id])){

					if(isIE())
						//checked property doesn't work in IE, so use defaultChecked property.NS opposite.
						rowContentArray[cols].defaultChecked = true;
					else
						rowContentArray[cols].checked = true;

					tr.className = this.selectStyleClass;

			}
/*
			//restore UI state
			if(this.stateString!="")
			{

					//stateStr will looks like: ("2;Amount asc; Position12; Position13,Position14,Position15; null)
					//where column sort can not be coexisted with select column sort
					//radio button onSelect can not be coexisted with check box onSelect.

					var stateArray = this.stateString.split(";");

					var check = stateArray[3].indexOf(signature);
					var radio = stateArray[4].indexOf(signature);
					if(check>-1||radio>-1)
					{
						//found the checkbox selected row
						if(isIE())

							rowContentArray[cols].defaultChecked = true;
						else
							rowContentArray[cols].checked = true;

						if(check>-1) this.selectRow(tr);
						if(radio>-1) this.sltRdoRow(tr);
					}

			}
*/

			td.onclick = this.OnInternalActivate;
			td.appendChild(rowContentArray[cols]);

			cols++;
		}

		//this logic handle index column
		if(this.showRowIndex){
			var td = getTD(tr,cols);
			td.className=(cols>0)?(this.styleClass+"wpsTableDataMiddle wpsDataAlignCenter"):this.styleClass+"wpsTableDataStart wpsDataAlignCenter";

			//for accessibility, we have to use <a> tag so that IE and NS can works.
			var a = document.createElement("a");
			a.setAttribute("href", "javascript:;");
			a.appendChild(rowContentArray[cols]);

			//td.align = "center";
			td.onclick = this.OnInternalActivate;

			if(!isodcdgIE5)
				td.style.cursor = "pointer";

			td.appendChild(a);
			cols++;


		}

		var firstCell = cols;

		//for regular case
		for(i=cols; i<rowContentArray.length;++i)
		{
			var td = getTD(tr,i);

			td.className = (i>0)?((i==rowContentArray.length-1&&this.ReadOnly)?(this.styleClass+"wpsTableDataEnd"):(this.styleClass+"wpsTableDataMiddle")):(this.styleClass+"wpsTableDataStart");


			var j,mcss;
			for(var j=0; j<this.metaData.length; j++){
				if(rowContentArray[i].name == this.metaData[j][0]){

					mcss = this.metaData[j][1][1];
					break;
				}
			}

			if(mcss&&mcss.toUpperCase()=="CENTER")
				td.className = td.className + " "+ this.styleClass +"wpsDataAlignCenter";
			if(mcss&&mcss.toUpperCase()=="RIGHT")
				td.className = td.className  + " "+ this.styleClass + "wpsDataAlignRight";



				//for accessibility, we have to use <a> tag so that IE and NS can works.
				var a = document.createElement("a");

				a.setAttribute("href", "javascript:;");

				if(!this.metaData[j][1][3]){
					a.className = this.styleClass+"wpsDataAlink"; //so this cell will show underline
				}

				//for accessiblity so that user can jump to the first content cell
				if(index==0&&firstCell==cols){

					a.setAttribute("id", "navskip"+this.ID+this.currentPage);
				}

				//so that the input text field can be keyboard acess. Laurent found the bug.

				if(this.metaData[j][1][0]){
					a.appendChild(rowContentArray[i]);
					td.appendChild(a);
				}else
					td.appendChild(rowContentArray[i]);


				if(!isodcdgIE5)
					td.style.cursor = "pointer";
				td.onclick = this.OnInternalActivate;

				//save the width and align information for later use.
				if(this.tdAlign[cols]==null){
					//this.tdWidth[cols] = td.offsetWidth;

					this.tdAlign[cols] = td.align;
				}
			cols++;
		}


		//handle highlight situation
		if(this.highlightItem)
		{
			//back from other page
			if(this.highlightItem[id])
				tr.className = this.highlightStyleClass;


		}else{
/*
			//restore UI State
			if(this.stateString!="")
			{
				var stateArray = this.stateString.split(";");
				if(stateArray[2].indexOf(signature)>-1)
					this.lightRow(tr);
			}

*/
		}





		// create delete link column
		if (!this.ReadOnly) {
			var td = getTD(tr,cols);
			//td.className = this.styleClass+"wpsTableDataEnd";

			if (rowContentArray != null) {

				var img = this.getImageLink(8,index);
				td.appendChild(img);
			}
			else {
				var child = document.createElement("label");
				child.innerHTML = ".";
				child.style.visibility = "hidden";
				td.appendChild(child);

			}
		}


}


/**
* @method private getTD
*   This function is used to get the html td object with the tr object
* @param object tr
* 	the html tr object
* @param int colId
*	The index of the cell.
**/
function getTD(tr,colId)
{
	if (colId < tr.cells.length) {
		td = tr.cells[colId];
		while (td.hasChildNodes())
			td.removeChild(td.lastChild);
	}else
		td = tr.insertCell(colId);
	return td;

}

/**
* @method private DataGrid.prototype.SetAddMode
*   This function is called when user click "add row" link so that
*	one more row will be constructed with blank label if that column
*	is not editable or an input text field if that column can be
*	edited.And also have "accept" or "cancel" link in the last cell.
**/
	DataGrid.prototype.SetAddMode = function()
	{
		var tr1;

		if(isIE()){
			var tr1 = document.getElementById(this.ID+"newrow");
			if(tr1.style.display=="inline")
				return;
			tr1.style.display = "inline";

		}else{
			if(document.getElementById(this.ID+"newrow"))
				return;

				var bodytable = document.getElementById(this.ID +"bodycontenttd");
				tr1 = document.createElement("tr");
				tr1.id = this.ID+"newrow";
				bodytable.appendChild(tr1);
		}

		var i = 0;
		if(this.selectCol){
				tr1.insertCell(i);
			i++;

		}

		if(this.showRowIndex){
				tr1.insertCell(i);
			i++;
		}

		var cssStyle =  this.styleClass+"wpsEditField";

//get the column size
			var bodytable = document.getElementById(this.ID +"bodycontenttd");
			var firsttr = bodytable.rows[1];



		for (; i < this.NumberOfCols; i++) {

			 tdcol = tr1.insertCell(i);
			 var width = firsttr.cells[i].offsetWidth-2;
			 tdcol.width = width;

			if (!this.readOnlyTemplate[i]) {
				var input=inputstr = null;
				if(isIE()){
					if(this.tdConverter[i]){

							inputstr  = "<INPUT TYPE='text' class='"+cssStyle+"' style='text-align:" + this.tdAlign[i]+"' style='width:" + width+"px' onchange = formatValue(this,"+i+",'"+this.ID+"')>";
					}else{
							inputstr = "<INPUT TYPE='text'  class='"+cssStyle+"' style='text-align:" + this.tdAlign[i]+"' style='width:" + width+"px'>";
					}
					input = document.createElement(inputstr);
				}else{
					input = document.createElement("input");
					input.setAttribute("type", "text");
					input.setAttribute("class", cssStyle);
					input.setAttribute("style", "text-align:"+this.tdAlign[i]);

					input.setAttribute("style", "width:"+width+"px");

					if(this.tdConverter[i]){

						input.setAttribute("onchange", "formatValue(this,"+i+",'"+this.ID+"')");
					}

				}

				tdcol.appendChild(input);
			}
		}

		var td2 = tr1.insertCell(i);

		var acceptimg = this.getImageLink(9);
		var cancelimg = this.getImageLink(10);
		td2.appendChild(acceptimg);
		td2.appendChild(cancelimg);


	}
/**
* @method private formatValue
*   This function is used to format the cell content
* @param object input
* 	the html input object
* @param int index
*	the index to get the format converter in the converter array
* @param String id
*	the id to get the datagrid control
**/
	function formatValue(input, index, id)
	{
		try{
			input.value = window.document.getElementById(id).GridControl.tdConverter[index].valueToString(input.value);
		}catch(e){
		}

	}



/**
* @method private DataGrid.prototype.AddRow
*   This function is called when user click "accept" link in the new
*	created row so that the content the user typed in will be saved to
*	the data model using call back mechanism.
**/
	DataGrid.prototype.AddRow = function()
	{

		var tr = document.getElementById(this.ID+"newrow");

		if (null == tr) {
			this.CancelAddMode();
			return;
		}


		try {
			// event handling
			if (this.OnRowAdded != null){

				this.OnRowAdded(tr);
			}

		}
		catch (e)
		{
			if (this.OnError != null)
				 this.OnError(action_datagrid_addrow,e);

			return;
		}

		this.CancelAddMode();

	}



/**
* @method private DataGrid.prototype.CancelAddMode
*   This function is called when user click "cancel" link in the new
*	created row so that content the user typed in will not be saved
*	to the data model. No call back here.
**/
	DataGrid.prototype.CancelAddMode = function()
	{
		var tr = document.getElementById(this.ID+"newrow");
		if(isIE()){

			tr.style.display = "none";
			while (tr.cells.length > 0)
				tr.deleteCell(tr.cells.length - 1);


		}else{
			var bodytable = document.getElementById(this.ID +"bodycontenttd");

			bodytable.removeChild(tr);

		}

	//	this.ShowGrid(true);
	}
/**
* @method private DataGrid.prototype.RemoveRow
*   This function is called when user click "delete" link in the row
*	he/she want to delete data model will be changed using call back
*	mechanism.
* @param int index
*	The position of the row which is to be deleted.
**/
	DataGrid.prototype.RemoveRow = function(index)
	{
		try{
			if (index >= 0 && index < this.DataArray.length) {

				if (this.OnRowRemoved != null)
					this.OnRowRemoved(index+this.StartIndex);
			}
		}catch(e){
				if (this.OnError != null)
					this.OnError(action_datagrid_removerow,e);

				return;

		}

	}
/**
* @method private DataGrid.prototype.GetPreviousStartIndex
*   This function is used to get the previous page's start index
**/
	DataGrid.prototype.GetPreviousStartIndex = function()
	{
		var nRows = 0;
		var prevIndex = -1;
		for (var i = this.StartIndex-1; i >= 0 && nRows < this.PageSize; --i) {
			prevIndex = i;
			++nRows;
		}
		if (nRows != this.PageSize)
			return -1;

		return prevIndex;
	}
/**
* @method private DataGrid.prototype.GetNextStartIndex
*   This function is used to get the next page's start index
**/

	DataGrid.prototype.GetNextStartIndex = function()
	{
		var nRows = 0;
		var nextIndex = -1;


		for (var i = this.StartIndex+1; i < this.totalRows && nRows < this.PageSize;++i) {

			nextIndex = i;
			++nRows;
		}
		if (nRows != this.PageSize)
			return -1;

		return nextIndex;
	}
/**
* @method private DataGrid.prototype.GetFirstPageStartIndex
*   This function is used to get the first page's start index
**/
	DataGrid.prototype.GetFirstPageStartIndex = function()
	{
		if(this.StartIndex ==0) return -1;


	}

/**
* @method private DataGrid.prototype.GetLastPageStartIndex
*   This function is used to get the last page's start index
**/
	DataGrid.prototype.GetLastPageStartIndex = function()
	{
		if(this.totalPage ==1)
			this.totalPage = Math.ceil(this.totalRows/this.PageSize);
		if(this.currentPage == this.totalPage) return -1;

	}
/**
* @method public DataGrid.prototype.selectPage
*   This function is called to go the page the user specified
* @param int value
*	The page the user want to go
**/
	DataGrid.prototype.selectPage = function(value)
	{

		if(value==-1){
			var idtop = "goPageNum_top"+this.ID;
			var idbottom = "goPageNum_bot"+this.ID;

			switch(this.NavBarPosition){
				case 0:
					value = document.getElementById(idbottom).value;
					break;
				case 1:
					value = document.getElementById(idtop).value;
					break;
				case 2:
					value = document.getElementById(idbottom).value;
					if(value=="")
						value = document.getElementById(idtop).value;
					break;
				default:
					value = 1;
					break;
			}

		}

		//var totalPage = Math.ceil(this.totalRows/this.PageSize);
		if(isNaN(value)) value = 1;
		if(value<=0) value = 1;
		if(value>this.totalPage) value = this.totalPage;
		var startindex = (value-1)*this.PageSize;

		//onPage (with information about whether it is next, previous, first or last, of a jump to a specified page) before paging occurs
		if(!this.activeEvent("onPage", "jump", startindex+1))
			return;

		this.currentPage = value;
		this.StartIndex = startindex;
		this.DataArray = null;

		this.ShowGrid(true);

	}

/**
* @method public DataGrid.prototype.FirstPage
*   This function is called to go the first page
**/
	DataGrid.prototype.FirstPage = function()
	{

		//onPage (with information about whether it is next, previous, first or last, of a jump to a specified page) before paging occurs
		if(!this.activeEvent("onPage", "first", 1))
			return;

		this.StartIndex =0;
		this.currentPage = 1;
	//	this.pageChange = false;
		this.DataArray = null;
	//	this.unSelectRow();
		this.ShowGrid(true);


	}
/**
* @method public DataGrid.prototype.LastPage
*   This function is called to go the last page
**/
	DataGrid.prototype.LastPage = function()
	{
		//var totalPage = Math.ceil(this.totalRows/this.PageSize);
		var startindex = (this.totalPage-1)*this.PageSize;

		//onPage (with information about whether it is next, previous, first or last, of a jump to a specified page) before paging occurs
		if(!this.activeEvent("onPage", "last", startindex+1))
			return;


		this.StartIndex =startindex;

		this.currentPage = this.totalPage;
	//	if(this.currentPage!=1)
	//		this.pageChange = true;
		this.DataArray = null;
	//	this.unSelectRow();
		this.ShowGrid(true);


	}

/**
* @method public DataGrid.prototype.PreviousPage
*   This function is called to go the previous page
**/

	DataGrid.prototype.PreviousPage = function()
	{
		var previousIndex = this.GetPreviousStartIndex();
		if (previousIndex == -1)
			return;

		//onPage (with information about whether it is next, previous, first or last, of a jump to a specified page) before paging occurs
		if(!this.activeEvent("onPage", "previous", previousIndex+1))
			return;

		this.StartIndex = previousIndex;
		this.currentPage--;
	//	if(this.currentPage!=1)
	//		this.pageChange = true;

		this.DataArray = null;
	//	this.unSelectRow();
		this.ShowGrid(true);
	}
/**
* @method public DataGrid.prototype.NextPage
*   This function is called to go the next page
**/
	DataGrid.prototype.NextPage = function()
	{

		var nextIndex = this.GetNextStartIndex();
		if (nextIndex == -1)
			return;

		//onPage (with information about whether it is next, previous, first or last, of a jump to a specified page) before paging occurs
		if(!this.activeEvent("onPage", "next", nextIndex+1))
			return;


		this.StartIndex = nextIndex;
		this.currentPage++;

	//	if(this.currentPage!=1)
	//		this.pageChange = true;
		this.DataArray =null;
		this.ShowGrid(true);

	}


/**
* @method private DataGrid.prototype.selectAll
*   This function is called to check all the row's checkbox
**/

	DataGrid.prototype.selectAll = function (selectFlag)
	{
		this.selAll = selectFlag;
		if(selectFlag){

			var unselArr = new Array();
			var allArr = new Array();
			var checkArr = new Array();

			//select all
			//set all the objected as selected in the selected array
			for(var i=0; i<this.Adapter.EObjects.length; i++)
			{
				var eobj = this.Adapter.EObjects[i]
				var id = eobj.ID;

				if(!this.selectedItem[id]){
					unselArr[id] = eobj;
				}
				allArr[id] = this.normalStyleClass;
				checkArr[id] = eobj;

			}

			//call "onselectall" event handler.
			if(!this.activeEvent("onSelectAll", unselArr))
				return;

			this.checkCurPage();

			for(var key in allArr)
			{
				if(!this.selectedItem[key]){

					this.selectedItem[key] = this.normalStyleClass;
					this.checkedEObjectArray[key] = checkArr[key];

				}
			}

		}else{


			//call "onunselectall" event handler.

			if(!this.activeEvent("onUnselectAll", this.checkedEObjectArray))
				return;


			this.uncheckCurPage();
			this.selectedItem = new Array();
			this.checkedEObjectArray = new Array();
			//this.stateString = this.generateUIStateString();

		}

		//change "select all" to "deselect all";
		var imgobjs = new Array();
		imgobjs[0] = document.getElementById(this.ID+"dgimgselectalltop");
		imgobjs[1] = document.getElementById(this.ID+"dgimgselectallbot");


		for(var i=0; i<imgobjs.length;i++){
			if(imgobjs[i]){
				if(selectFlag){
					//select all change to unselect all
					imgobjs[i].setAttribute("src", this.datagridIcons["unselectall"]);
					imgobjs[i].setAttribute("alt", odcDGImgUnSelectAll);
					code = "window.document.getElementById('" + this.ID + "').GridControl.selectAll()";
				}else{
					//unselect all change to select all

					imgobjs[i].setAttribute("src", this.datagridIcons["selectall"]);
					imgobjs[i].setAttribute("alt", odcDGImgSelectAll);
					code = "window.document.getElementById('" + this.ID + "').GridControl.selectAll(true)";
				}
				imgobjs[i].parentNode.setAttribute("href","javascript:"+code);
			}
		}


	}


/**
* @method private DataGrid.prototype.getCurPageRows
*   This function is used to get all the current page rows html tr
*	object
**/
	DataGrid.prototype.getCurPageRows = function(){

			var tr = new Array();

			var bodytable = document.getElementById(this.ID +"bodycontenttd");

			for(var i=1; i<bodytable.rows.length; i++)
			{
				var rowObj = bodytable.rows[i];

				tr[rowObj.id] = rowObj;
			}

			return tr;
	}
/**
* @method private DataGrid.prototype.checkCurPage
*   This function is used to check all the current page checkbox
*	object
**/
	DataGrid.prototype.checkCurPage = function(){


		var tr = this.getCurPageRows();

		for(var key in tr){

			if(tr[key].cells[0]&&tr[key].cells[0].firstChild&&tr[key].eobject&&!this.selectedItem[key]){
				tr[key].cells[0].firstChild.checked = true;
				this.selectRow(tr[key]);
			}
		}

	}

/**
* @method private DataGrid.prototype.uncheckCurPage
*   This function is used to uncheck all the current page checkbox
*	object
**/

	DataGrid.prototype.uncheckCurPage = function(){

		var tr = this.getCurPageRows();

		for(var key in tr){

			if(tr[key].cells[0]&&tr[key].cells[0].firstChild){
				tr[key].cells[0].firstChild.checked = false;
				this.unSelectRow(tr[key]);
			}
		}

	}



/**
* @method public DataGrid.prototype.getRowNum
*   This function is called get the row number. When the method is
*	called,the datagrid will search for the first object with the
*	matching attribute value, get the row number.
* @param String attributeName
*   The name of the attribute
* @param String attributeValue
*   The value of the attribute
* @return int
*	the row number of the searching row.
**/
	DataGrid.prototype.getRowNum = function(attributeName, attributeValue)
	{
		var objs = this.Adapter.EObjects;

		for(var i=0; i<objs.length; i++)
		{
			obj = objs[i];
			var value = obj.eGet(attributeName);
			if(value&&value==attributeValue)
			{
				return i+1;

			}

		}



		return 0;//if couldn'd find the row


	}



/**
* @method public DataGrid.prototype.getSelectedItems
*   This function is returning an array of selected item's eobjets. If none
*	of items is selected, then return empty array
* @return Array
*	An array of selected item's eobjects
**/
	DataGrid.prototype.getSelectedItems = function()
	{

		return this.compactCheckedItemsArray(this.checkedEObjectArray);
	}


/**
* @method public DataGrid.prototype.getHighlightedItem
*   This function is returning the highlighted item's eobject. If none of items
*	is highlighted, then return null.
* @return eObject
*	the eObject of highlighted item.
**/
	DataGrid.prototype.getHighlightedItem = function()
	{

		if(this.highlightItem){
			for(var key in this.highlightItem)
			{
				if(this.highlightItem[key])
				{
					return this.highlightItem[key][2];

				}
			}

		}else
			return null;

	}


/**
* @method public DataGrid.prototype.getTotalPageNum
*   This function is returning the total number of the pages.
* @return int
*	The total number of pages
**/
	DataGrid.prototype.getTotalPageNum = function()
	{
		return this.totalPage;
	}


/**
* @method public DataGrid.prototype.highlightRowNum
*   This function is called to highlight the selected row.
* @param String rowNumber
*   The row number of the highlighted row
**/
	DataGrid.prototype.highlightRowNum = function(rowNum)
	{
		var obj = this.Adapter.EObjects[rowNum-1];
		var curPage = Math.ceil((rowNum)/this.PageSize);
		var signature = obj.getSignature();

		if(signature){


			if(!this.activeEvent("onHighlight", obj))
				return;

			var stateStr = curPage + ";null;"+signature+";null;null";
			this.restoreUIState(stateStr);


		}

	}






/**
* @method public DataGrid.prototype.highlightRow
*   This function is called to highlight the selected row.When the
*	method is called,the datagrid will search for the first object
*	with the matching attribute value, show that page, and generate
*	a highlight event on it.
* @param String attributeName
*   The name of the attribute
* @param String attributeValue
*   The value of the attribute
**/
	DataGrid.prototype.highlightRow = function(attributeName, attributeValue)
	{
		var objs = this.Adapter.EObjects;
		var curPage = 1;
		var signature = obj = null;

		for(var i=0; i<objs.length; i++)
		{
			obj = objs[i];
			var value = obj.eGet(attributeName);
			if(value&&value==attributeValue)
			{

				curPage = Math.ceil((i+1)/this.PageSize);
				signature = obj.getSignature();


				break;
			}

		}

		if(signature){

			if(!this.activeEvent("onHighlight", obj, attributeName))
				return;

			var stateStr = curPage + ";null;"+signature+";null;null";
			this.restoreUIState(stateStr);


		}


	}



/**
* @method public DataGrid.prototype.generateUIStateString
*   This function is called by pageControl to get the current
*	datagrid's UI state
* @return String UIStateStr
*	stateStr will looks like: ("2;Amount asc;null; Position12;
*	Position13, Position14, Position15; null).format:("currentpage;
*	column sort order; highighted item; selected checkbox items;
*	selected radio item); where column sort can not be coexisted
*	with select column sort radio button onSelect can not be
*	coexisted with check box onSelect.
**/

	DataGrid.prototype.generateUIStateString = function()
	{

		var UIStateStr = "";

		UIStateStr +=this.currentPage+";";


		if(this.sortState!=null)
			UIStateStr += this.sortState+";";
		else
			UIStateStr += "null;";


		if(this.highlightItem){
			//alert(this.highlightItem[0])
			//alert(this.getEObjectSignatureFromID(this.highlightItem[0]));
			for(var key in this.highlightItem)
			{
				//alert("key " + key);
				if(this.highlightItem[key])
				{
					//alert(this.highlightItem[key]);
					//alert(this.highlightItem[key][2].toStr());

					UIStateStr += this.highlightItem[key][2].getSignature()+";";

				}
			}

		}else
			UIStateStr += "null;";


		var selected = false;
		for(id in this.selectedItem){
			if(this.selectedItem[id]){
				selected = true;
		 		UIStateStr += this.getEObjectSignatureFromID(id)+",";
			}
		}
		if(selected){
			UIStateStr = Trim(UIStateStr) + ";";
		}else
			UIStateStr += "null;";



		if(this.sltRdoItem)
			for(var key in this.sltRdoItem)
			{
				if(this.sltRdoItem[key])
					UIStateStr += this.sltRdoItem[key][2].getSignature();
			}
		else
			UIStateStr += "null";

		return UIStateStr;


	}
/**
* @method private DataGrid.prototype.getEObjectSignature
*   This function is used to get the eobject signature which is
*	persistent between calls
* @param String eobjectID
*	The xmi ID of the EOjbect.
* @return String
*	The sigature string of the searched eobject
**/

	DataGrid.prototype.getEObjectSignatureFromID = function(id)
	{

		for(var i=0; i<this.Adapter.EObjects.length; i++)
		{
			if(this.Adapter.EObjects[i].ID == id)
				return this.Adapter.EObjects[i].getSignature();



		}



	}

/**
* @method private DataGrid.prototype.getEObjectIDFromSignature
*   This function is used to get the eobject xmi id from the
*	signature which is persistent between calls
* @param String signature
*	The signature of the EOjbect.
* @return String id
*	The eojbect id;
**/

	DataGrid.prototype.getEObjectIDFromSignature = function(signature)
	{

		for(var i=0; i<this.Adapter.EObjects.length; i++)
		{
			if(this.Adapter.EObjects[i].getSignature() == signature)
				return this.Adapter.EObjects[i].ID;



		}


	}


/**
* @method public DataGrid.prototype.restoreUIState
*   This function is called by jsf page to restore the previous
*	datagrid's UI state, after using this method, performance is
*	down.
* @param String stateStr
*	A string which has all the previous UI state information
**/
	DataGrid.prototype.restoreUIState = function(stateStr)
	{

		//stateStr will looks like: ("2;Amount asc; Position12; Position13, Position14, Position15; null)
		//where column sort can not be coexisted with select column sort
		//radio button onSelect can not be coexisted with check box onSelect.
		if(stateStr!=null&&stateStr!=""){
			//this.stateString = stateStr;

			var stateArray = stateStr.split(";");


			//for column sort
			if(stateArray[1]!="null"){
				this.sortOrder = new Array();


				var sortArray = stateArray[1].split(" ");

				if(sortArray[0] == "odcselect"){
					this.Adapter.SortSel(sortArray[1]);
					this.selectHead.so = sortArray[1];
					this.ChangeSortImg(this.selectHead);
					this.sortOrder["odcselect"] = sortArray[1];

				}else if(sortArray[0] == "odcindex"){
					this.Adapter.SortIndex(sortArray[1]);
					this.indexHead.so = sortArray[1];
					this.ChangeSortImg(this.indexHead);
					this.sortOrder["odcindex"] = sortArray[1];
				}else{

					this.Adapter.Sort(stateArray[1]);

					for(var i=0; i<this.headArray.length;i++)
					{	//find out the column to change the image
						if(sortArray[0] == this.headArray[i].membername)
						{
							this.headArray[i].so = sortArray[1];
							this.sortOrder[sortArray[0]] = sortArray[1];
							this.ChangeSortImg(this.headArray[i]);
							break;

						}

					}
				}
			}




			//for page select
			if(stateArray[0]!="1")
				this.selectPage(Number(stateArray[0]));



			var tr = this.getCurPageRows();

//for onselect checkbox

			if(stateArray[3]!="null"){
				var selectArr = stateArray[3].split(",");

				for(var j=0; j<selectArr.length; j++)
				{
					if(selectArr[j]&&selectArr!="")
					{


						var Objid = this.getEObjectIDFromSignature(selectArr[j]);

						//this.selectedItem[Objid] =oneRowArr[1];
						if(tr[Objid])
						{
							//if(isIE()){


								//tr[Objid].cells[0].firstChild.defaultChecked = true;
								tr[Objid].cells[0].firstChild.checked = true;
							//}else
							//	tr[Objid].cells[0].firstChild.checked = true;

							this.selectRow(tr[Objid]);


						}
					}
				}
			}


			//for OnSelect radio button
			if(stateArray[4]!="null")
			{


				var Objid = this.getEObjectIDFromSignature(stateArray[4]);

				if(tr[Objid]){
					this.sltRdoRow(tr[Objid]);

					if(isIE())
						tr[Objid].cells[0].firstChild.defaultChecked = true;

					else
						tr[Objid].cells[0].firstChild.checked = true;


				}
			}


			if(stateArray[2]!="null")
			{


				var Objid = this.getEObjectIDFromSignature(stateArray[2]);


				//for Highlight
				if(tr[Objid])
				{
						this.lightRow(tr[Objid]);



				}
			}







		}

	}







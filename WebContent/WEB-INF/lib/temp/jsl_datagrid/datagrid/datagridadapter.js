//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003.  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//=================================================================


/**
 * @class public DatagridAdapter
 * @constructor DatagridAdapter
  * This adapter interfaces with the Datagrid Control object, and is
  * to it at runtime.It acts as an middle layer between datamodel and
  * the datagrid control
 * @param DataGrid gridCtrl
 *		The Datagrid control which the datagrid adapter want to bind.
**/

function DatagridAdapter(gridCtrl) {

	this.gridControl = gridCtrl;
	this.activeBinders = new Array();

}


	DatagridAdapter.prototype.xmiModel = null;
	DatagridAdapter.prototype.activeDataSet = 0;
	DatagridAdapter.prototype.activeEObject = null;
	DatagridAdapter.prototype.EObjects = null;
	DatagridAdapter.prototype.loaderInitialized = false;
	DatagridAdapter.prototype.parentEObjectBinder = null;
	DatagridAdapter.prototype.parentEObject = null;
	DatagridAdapter.prototype.propertyName= "";
	DatagridAdapter.prototype.parentEObjectArrayBinder = null;
	DatagridAdapter.prototype.previousEobject = null;




/**
* @method public DatagridAdapter.prototype.setModel
*   Method to set the the data mode whichthe datagrid control will render
* @param EObject model
*   The data model which the datagrid control will render
**/
	DatagridAdapter.prototype.setModel = function(model) {

		this.xmiModel = model;
	}


/**
* @method public DatagridAdapter.prototype.bind
*   Function to establish linkages between the adapter and the control
*	Only call this method once when setup datagrid adapter and control.
**/
	DatagridAdapter.prototype.bind = function() {


		this.gridControl.Adapter = this;

		// populate the EObjects array with all the info from the mapping
		try{

			this.activateDataSet(this.parentEObject);
		}catch(e)
		{
			//alert(e.description);
			return false;
		}



		// create the columns
	//	this.setupColumns();


	}

/**
* @method public DatagridAdapter.prototype.refresh
*   This function is used to redisplay the datagrid
**/
	DatagridAdapter.prototype.refresh = function() {
		//this.gridControl.setupColumns();
		this.gridControl.ShowGrid(true);

	}

/**
* @method private DatagridAdapter.prototype.resetIndex
*   This function is used to reset the indexes to the first page and
*   the first row of the datagrid
**/
	DatagridAdapter.prototype.resetIndex = function(){
		//this.gridControl.NumberOfCols = 0;
		this.gridControl.StartIndex = 0;
		this.gridControl.currentPage = 1;


	}
/**
* @method private DatagridAdapter.prototype.resetIndex
*   This function is used to reset the indexes to the first page
*   and the first row of the datagrid
**/
	DatagridAdapter.prototype.resetArray = function(){

		this.gridControl.checkedEObjectArray = new Array();
		this.gridControl.selectedItem = new Array();
		this.gridControl.highlightItem = null;
		this.gridControl.sltRdoItem = null;
	}





/**
* @method private DatagridAdapter.prototype.Sort
*   This function is used to sort the column data when the user click
*   the table header
* @param String sortOrder
*   The order which can be "desc" and "asc".
**/
	DatagridAdapter.prototype.Sort = function(sortOrder)
	{

		if (this.EObjects==null||this.EObjects.length <=1)
			return;

		//this.gridControl.selectSortOrder = null; //since only one column can be sort criteria
		//alert(" sortOrder " + sortOrder + "this.propertyName " + this.propertyName);

	var sortArray = SortRows(sortOrder,this.EObjects);

	rows = new Array();
	for (var i = 0; i < sortArray.length; ++i)
	{
		rows[rows.length] = sortArray[i].m_row;
	}

	this.EObjects = rows;

	this.resetIndex();

	this.getCurPageDataSet();
	this.gridControl.ShowGrid(true);

	}







/**
* @method private DatagridAdapter.prototype.SortIndex
*   This function is used to sort the index column
* @param String sortOrder
*   The order which can be "desc" and "asc".
**/

DatagridAdapter.prototype.SortIndex = function(sortOrd)
{
	if(this.EObjects == null) return;

	var sortedArray = new Array();
	for(i=0; i<this.EObjects.length; i++)
	{
		sortedArray[i] = this.EObjects[this.EObjects.length-1-i];
	}
	this.EObjects = sortedArray;

	this.resetIndex();

	this.getCurPageDataSet();
	this.gridControl.ShowGrid(true);
}


/**
* @method private DatagridAdapter.prototype.SortSel
*   This function is used to sort the selection column so that all
*   the selected rows will be grouped to the beginning if sortOrd is
*   "desc" or to the end if sortOrd is "asc"
* @param String sortOrder
*   The order which can be "desc" and "asc".
**/
	DatagridAdapter.prototype.SortSel = function(sortOrd)
	{

		if(this.EObjects == null) return;

		var selArr = new Array();
		var unSelArr = new Array();
		var idStr = "";

		if(this.gridControl.selectedItem !=null){

				for(var a in this.gridControl.selectedItem){
					idStr += a;
				}
		}

		if(idStr==""&&this.gridControl.sltRdoItem!=null){

			idStr = this.gridControl.sltRdoItem[0];

		}

		for(var i=0; i<this.EObjects.length; i++){
			if(idStr.indexOf(this.EObjects[i].ID)!=-1){

				selArr[selArr.length] = this.EObjects[i];
			}else{
				unSelArr[unSelArr.length] = this.EObjects[i];
			}



		}

		if(sortOrd=='asc')
			this.EObjects = selArr.concat(unSelArr);
		else
			this.EObjects = unSelArr.concat(selArr);

		this.resetIndex();

		this.getCurPageDataSet();
		this.gridControl.ShowGrid(true);
	}




/**
* @method private DatagridAdapter.prototype.unbindAll
*   This function is used to remove all the binding relations with
*   the datagrid
**/

	DatagridAdapter.prototype.unbindAll = function() {

		for (var i = 0; i < this.activeBinders.length; i++) {

			this.activeBinders[i].DataUnbind();

		}

		this.activeBinders = new Array();

	}


/**
* @method private DatagridAdapter.prototype.findEObjectInArray
*   This function is used to find the eobject using the object id in
*   the binding eobject list.
* @param String Id
*   The eobject id
* @param Array arr
*   The eobject array
* @return Eobject
*   The eobject to be found
**/
	DatagridAdapter.prototype.findEObjectInArray = function(id, arr) { // private

		for (var j = 0; j < arr.length; j++) {

			if (id == arr[j].ID) {

				return arr[j];

			}

		}
		return null;

	}
/**
* @method private obtainEObjects
*   This function is used to get all the children eobjects
**/
	DatagridAdapter.prototype.obtainEObjects = function () {

		if(this.EObjects&&this.EObjects.length)
			this.previousEobject = this.EObjects[0]; //still get the last time object in case this new dataset is null.

		this.EObjects = this.parentEObject.eGet(this.propertyName);

		if (null == this.EObjects || typeof(this.EObjects) != "object"){
			this.EObjects = new Array();
		}else {
			this.previousEobject = null;
			if (this.EObjects.length == null) {
				var arr = new Array();
				arr[arr.length] = this.EObjects;
				this.EObjects = arr;
			}
		}

	}
/**
* @method private getCurPageDataSet
*   this function is called get the current page's data set which
*   will improve the performance since only the data in that page will
*   be constructed
**/
	DatagridAdapter.prototype.getCurPageDataSet = function()
	{


		this.gridControl.DataArray = new Array();
		//for (var i = 0; i < this.EObjects.length; i++) {
		for (var i = this.gridControl.StartIndex; i < (this.gridControl.StartIndex + this.gridControl.PageSize); i++) {
		Log.debug("activateDataSet() in datagridadapter.js", "enter each createTabularContent() i:" + i);


				var TableRowArray = this.createTabularContent(this.EObjects[i],i+1);


				//var TableRowArray = this.createTabularContent(this.EObjects[i]);
				if (null != TableRowArray) {

					this.gridControl.DataArray[this.gridControl.DataArray.length] = TableRowArray;
				}



		}



	}
/**
* @method public DatagridAdapter.prototype.activateDataSet
*   this function is used to get all the children eobjects of the
*   passed eobject and filter out the rows based on the filter
*   expression if existing. and Then call getCurPageDataSet to
*   construct the dataset.
* @param EObject eobject
*	the eobject which need to be rendered in datagrid control.
**/
	DatagridAdapter.prototype.activateDataSet = function(eobject) {

      Log.debug("activateDataSet() in datagridadapter.js", "entering activateDataSet()");

		if (null == eobject) {
			return;
		}

		// unbind all the binders attached to the existing table
		if (null != this.parentEObjectBinder ) {

			this.parentEObject.PropertyBinders.Remove(this.parentEObjectBinder);

		}

		if (null != this.parentEObjectArrayBinder) {


			this.parentEObject.PropertyArrayBinders.Remove(this.parentEObjectArrayBinder);

		}


		//remove all the cells binder

		if(this.activeBinders.length>0){

			this.unbindAll();
		}

		//remove all the checked
		this.resetArray();

		this.resetIndex();
		Log.debug("activateDataSet() in datagridadapter.js", eobject.toStr());

		this.parentEObject = eobject;

		this.obtainEObjects();


      	var rowFilter = this.gridControl.RowFilter;
        rowFilter = convertXMLEscapingtoJS(rowFilter);
    	if (null != rowFilter && rowFilter.length > 0) {
			var filteredRows = new Array();
			for(var i=0; i<this.EObjects.length; i++)
			{
					var rowFilterResult = true;

					if(this.EObjects[i]!=null){
						//var rowFilter = this.gridControl.RowFilter;

							//var str = this.EObjects[i].CreateEvalContext();

							rowFilter = rowFilter.replace(/this.get/g, "this.EObjects[i].get");
					  		rowFilter = rowFilter.replace(/this.eGet/g, "this.EObjects[i].eGet");
							var str = "rowFilterResult = (true == (" + rowFilter + ")) ? true : false;";
							try {
								eval(str);
							}
							catch (e) {

								var args = new Array;
								args[0] = rowFilter;
								args[1] = e.description;

								var Msg = NlsFormatMsg(invalid_filter_expression, args);

								alert(Msg);

								return;

							}
					}

						if (rowFilterResult)
							filteredRows[filteredRows.length] = this.EObjects[i];
				}

			this.EObjects = filteredRows;

		}




		if(this.gridControl.PageSize <=0){

			this.gridControl.PageSize = this.EObjects.length;
		}
		this.gridControl.totalRows = this.EObjects.length;
	//	this.gridControl.totalRows = filteredRows.length;
      Log.debug("activateDataSet() in datagridadapter.js", "before createTabularContent loop");

		if (!this.gridControl.AllowPaging) {
			this.gridControl.StartIndex = 0;
			//this.gridControl.PageSize = this.EObjects.length;
			this.gridControl.PageSize = filteredRows.length;
		}
		else {
			if (this.gridControl.StartIndex < 0)
				this.gridControl.StartIndex = 0;
		}
		// bind
		if (null != this.parentEObject) {
			// binder to capture existing model changes
			var pb = new PropertyBinder(this.parentEObject,this.propertyName,this);
			pb.ControlSetFunction = "onModelChange";
			//this.parentEObject.PropertyBinders.Add(pb);
			this.parentEObjectBinder = this.parentEObject.PropertyBinders.Add(pb);
/*
			if( !this.gridControl.ReadOnly) {
				// binder to capture structural changes
				var pab = new PropertyArrayBinder(this,this.parentEObject,this.propertyName,this.onModelAdd,this.onModelRemove);
				//this.parentEObject.PropertyArrayBinders.Add(pab);
				this.parentEObjectArrayBinder = this.parentEObject.PropertyArrayBinders.Add(pab);
			}
*/

		}
	//	try{

			this.getCurPageDataSet();


	}

/**
* @method private DatagridAdapter.prototype.onModelChange
*   this function will be fired by the Eobject when there is a change
*   relating to the "propertyName"
* @param boolean rows
*	if rows is true, then rebind to the new parent eobject.
**/
   DatagridAdapter.prototype.onModelChange = function(rows)
   {
		  if (rows) {

				this.activateDataSet(this.parentEObject);
				this.refresh();
			}

   }


/**
* @method private DatagridAdapter.prototype.createTabularContent
*   this function is used to construct the dataset of one row, in
*   datagrid, every single cell bind to the data model
* @param EObject eobject
*	the eobject which has all the information to be displayed in datagrid.
* @param int index
*	the index of the row.
* @return Object
*   the constructed one row of dataset.
**/
	DatagridAdapter.prototype.createTabularContent = function (eobj,index) {

      //Log.debug("createTabularContent() in datagridadapter.js", "entering");

		if(eobj == null)
			return null;
		var contentArray = new Array();
		var member = null;
		var cols = 0;


		if(this.gridControl.selectCol!=null&&this.gridControl.selectCol!='undefined'){

				var elem = null;
				if(this.gridControl.selectCol[1]=="checkbox"){

						if(isIE()){

							var inputStr = "<INPUT TYPE='checkbox' name='ItemSelect' value='"+eobj.ID+"'>";
							elem = document.createElement(inputStr);
						}else{
							elem = document.createElement("input");
							elem.setAttribute("type","checkbox");

							elem.setAttribute("value", eobj.ID);
							elem.setAttribute("name", "ItemSelect");





						}

				}else{
						if(isIE()){

							var inputStr = "<INPUT TYPE='radio' name='ItemSelect' value='"+eobj.ID+"'>";
							elem = document.createElement(inputStr);
						}else{
							elem = document.createElement("input");
							elem.setAttribute("type","radio");

							elem.setAttribute("value", eobj.ID);
							elem.setAttribute("name", "ItemSelect");





						}
				}
					//elem.dg = this.gridControl;
					elem.eobject = eobj;
					elem.checked = false;
					contentArray[cols] = elem;
					++cols;

		}

		if(this.gridControl.showRowIndex)
		{

			var child = null;
						try {
								var browser = detectBrowser();
								//netscape 7 or mozilla 1.0.2 's label don't have the object. so using text node.
								if(browser == "Netscape 7.02" || browser == "Mozilla 1"){

									child = document.createTextNode(index);
									child.name = "itemIndex";

								}else{

									child = document.createElement("label");

									child.name = "itemIndex";

									child.innerHTML = index;


								}


							} catch (e) {
								// if complications arise (potentially due to masking), then leave the label blank
								contentArray[cols] = child;
								++cols;
						}
					//child.dg = this.gridControl;
					child.eobject = eobj;
					contentArray[cols] = child;
					++cols;



		}

		var cssStyle = this.gridControl.styleClass+"jsldgEditField";
		for(var j=0; j<this.gridControl.metaData.length; j++){

			// iterate through all members of the EObject
		//	for (var i = 0; i < eobj.Members.length; i++) {

				// obtain member
		//		member = eobj.Members[i];

				// if EReference, pass on it
		//		if (member.EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE)
				//if (member.EStructuralFeature.CLASSTYPE == "EReference")
		//			continue;
		//		var str = member.Name;
				var str = this.gridControl.metaData[j][0];
				var value =  eobj.eGet(str);


				//if(str == this.gridControl.metaData[j][0]){
					member = eobj.GetMember(str);


					var propName = null;
					var propEvt = null;
					var child = null;

					if(value==null||value=='undefined'||(typeof(value) == 'string' &&Trim(value)==""))
						value = "&nbsp;";


					// create the input/
//					if (this.gridControl.ReadOnly || member.EStructuralFeature.CLASSTYPE == "EAttributeCalculate"||this.gridControl.metaData[j][1][0]) {
					if (member.EStructuralFeature.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTECALCULATE||this.gridControl.metaData[j][1][0]) {


						try {
							var browser = detectBrowser();
							//netscape 7 or mozilla 1.0.2 's label don't have the object. so using text node.
							if(browser == "Netscape 7.02" || browser == "Mozilla 1"){

								child = document.createTextNode(value);
								child.name = str;
								propName = "nodeValue";
							}else{

								child = document.createElement("label");

								child.name = str;
								propName = "innerHTML";
								child.innerHTML = value;



							}


						} catch (e) {
							// if complications arise (potentially due to masking), then leave the label blank
							contentArray[cols] = child;
							++cols;
							continue;
						}
					}
					else {


						if(isIE()){

							if(this.gridControl.metaData[j][1][2]>0)

								inputStr = "<INPUT TYPE='text' class='"+cssStyle+"' name='"+ str+"'VALUE='"+value+"' style='text-align:" + this.gridControl.metaData[j][1][1]+"' style='width:" + this.gridControl.metaData[j][1][2]+"px"+"'>";
							else
								inputStr = "<INPUT TYPE='text' class='"+cssStyle+"' name='"+ str+"'VALUE='"+value+"' style='text-align:" + this.gridControl.metaData[j][1][1]+"'>";
								child = document.createElement(inputStr);
						}else{
							child = document.createElement("input");
							child.setAttribute("type","text");
							//child.value = eobj.eGet(member.Name);
							child.setAttribute("value", value);
							child.setAttribute("name", str);
							child.setAttribute("class", cssStyle);
							child.size = this.gridControl.metaData[j][1][2];

							child.setAttribute("style", "text-align:"+this.gridControl.metaData[j][1][1]);
						//	child.setAttribute("onKeyPress", "checkEnter(this,event)");
							if(this.gridControl.metaData[j][1][2]>0)
								child.setAttribute("style", "width:"+this.gridControl.metaData[j][1][2]+"px");


						}
						propName = "value";
						propEvt = "onchange";



					}

					child.className = cssStyle;
					child.eobject = eobj;
					child.propertyName= member.Name;
					//child.dg = this.gridControl;

		   			var pb = new PropertyBinder(eobj,member.Name,child,propName,propEvt);

					var converter = this.gridControl.metaData[j][2];

					if(converter){

						pb.setConverter(converter);
						this.gridControl.tdConverter[cols] = converter;
					}


					//child.PropertyBinder.setMetaData(this.gridControl.metaData);
					pb.OnError = this.gridControl.OnError;




					pb.DataBind();

					this.activeBinders[this.activeBinders.length] = pb;

					contentArray[cols] = child;


					++cols;

			//	}//if(str == this.gridControl.metaData[j].name)

		//	}//for (var i = 0; i < eobj.Members.length; i++) {
		}//for(var j=0; j<this.gridControl.metaData.length; j++){

      //Log.debug("createTabularContent() in datagridadapter.js", "exiting");

		return contentArray;

	}







// Check:
// - CK: ie. is this needed / does absence cause a problem
// - FIX: done quick&dirty, need to go back and elegantly recode this.

// Notes:
// - OnErrors are delegated to the Control from the Adapter, as control deals with the User Interface
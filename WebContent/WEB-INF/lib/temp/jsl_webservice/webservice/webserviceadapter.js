/** ==================================================================
 *  Licensed Materials - Property of IBM
 *  (c) Copyright IBM Corp. 2003.  All rights reserved.
 *
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM
 *  Corp.
 *
 *  DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
 *  sample code created by IBM Corporation.  This sample code is
 *  not part of any standard or IBM product and is provided to you
 *  solely for the purpose of assisting you in the development of
 *  your applications.  The code is provided "AS IS", without
 *  warranty of any kind.  IBM shall not be liable for any damages
 *  arising out of your use of the sample code, even if they have
 *  been advised of the possibility of such damages.
 * ==================================================================
 **/

/**
 * ------------------------------------------------------------
 * WebServiceAdapter.js
 * @author Rajesh , Manu
 * ------------------------------------------------------------
 **/

 /**
  * @class WebServiceAdapter
  *
  * This JS class interact with the WDO objects providing the data values
  * at runtime.
  **/

 /**
  * @method WebServiceAdapter
  * Constructor for the class.
  * @param ws_ctrl the webservice control object , which hides the webservice access
  * @param callback type String - the callback method name. The flash control will
  *  invoke this java script method for passing the results.
  * @param operation type String - The webservice method name
  * @param friendlyName type String - The webservice alias name
  * @param wsdl type String - The webservice end point URL
  * @param mdlRoot  - The WDO root object
  *			.
  **/
function WebServiceAdapter(ws_ctrl,callback,wsdl,friendlyName,Operation){

	this.ws            = ws_ctrl;
	this.wsdl	   = wsdl;
	this.friendlyName  = friendlyName;
	this.operation     = Operation;
	/**[eObjectId,eAttributeName,modelRoot,responseElementName,attributeName]**/
	this.OutputParams  = new Array();
	/** [eObjectId,eAttributeName,modelRoot,requestElementName,attributeName,useAsList]**/
	this.wsInput       =new Array();
	this.ret 	   = new Array();
	this.callback	   = callback;
	this.outputmapped  = "false";


// This following section is added by Qun Zhou to make sure that all ODC controls have the type and page id information
// for other controls, these information are added in control class. The reason they are added here because, in ODCRegistry
// addElementInfo call, the adapter instead of control is registered as control.
// Laurent required. Any qustion, please contact Qun or Laurent.
  this.type = "WebService";
  this.pageId = "webservice1";//default pageId which will be changed by ODCRegistry when do addElementInfo


  	// Methods

  /**
  * @method public getType
  *   This function is return the type of the control
  **/
  	this.getType = function(){
  		return this.type;
  	}


  /**
  * @method public getId
  *   This function return the id of the control
  **/
  	this.getId = function(){
  		return this.pageId;
  	}
//End of section. Qun



/**
 * The method that setup the input out put parameters for the webservice
 * @param wsInput - WS input array
 * @param wsOutput - WS output array
 * @param isOutputList - Indicates that whether the output is of the type List
 */
this.setupParams = function(wsInput,wsOutput) {
    this.wsInput=wsInput;
    this.OutputParams = wsOutput;
}
/**
 * The execution point. Refer's InvokeWS() in webservice.js. This method
 * is called from the page control
 */
this.Execute = function() {
    var OutputParams       = this.OutputParams;
    var eobj   = null;
    var eobjID = null;

    /** Preparing the output mapping **/
    try {
	    for (var i = 0; i < this.OutputParams.length; ++i)   {
		/** Mapping is already provided by the user **/
		/** this.OutputParams[i][3]--> RequestElementName**/
	        if(!(("null" == this.OutputParams[i][3]) || (this.OutputParams[i][3] ==""))){
	            this.outputmapped = "true";
	            this.ret[i] = this.OutputParams[i][3];
	        }
	    }
	} catch (Exception){
	    var Msg = NlsFormatMsg(error_mapping_output);
        Log.error("Execute", Msg);
        //alert(Msg);
        return;
	}
    /** Preparing the input mapping **/
    var eobji   = null;
    var eobjIDi = null;
    var paramXML = "<p>";
    var inputParams="";
     try {
	    for (var i = 0; i < this.wsInput.length; ++i) {
	          /** Handle constant **/
	        if(this.wsInput[i][0] == 'null') {  //eObjectId== null
	           if(this.wsInput[i][3] !="null") {  //requestElementName
	             attributeName = this.wsInput[i][3];
	           } 
	           else
	           {
	             /**
	             #Satish:put these two lines within braces coz without this it results in a bug.
		     If input  data to a web service component is of type "Simple Complex " (e.g. 
		     like Stock in portfolio demo)  and we use the input parameter mapping.
		     if the value for the very first element is constant then input parameters 
		     doesn't get encoded properly.
		     **/

	             attributeName = 'none';
	             paramXML += this.encodeSimpleType("",this.wsInput[i][1],attributeName);
	           }
	        } /** Handle specific attribue mapping for simple complex **/
	        else if("null" != this.wsInput[0][3]){ //requestElementName
	           paramXML += this.handleCmplxAttrMapping();
	           break;
	        } /** Handle simple array mapping - special case **/
	        else if("null" !=this.wsInput[0][4]){ //attributeName
	           var X = this.wsInput[i][0]; //eObjectId
		   if (X != eobjIDi) {
		       eobji = findEObjectByXMIID(this.wsInput[i][2], X);
		       eobjIDi = X;
		   }
	           paramXML += this.handleSimpleListMapping("",eobji.eGet(this.wsInput[i][1]),this.wsInput[i][4]);
	        }else {
		   var X = this.wsInput[i][0];
		   if (X != eobjIDi) {
		       eobji = findEObjectByXMIID(this.wsInput[i][2], X);
		       eobjIDi = X;
		   }
		   var attribute = this.splitNameAndIndex(this.wsInput[i][1]);
	           paramXML += this.prepareInputXML("",eobji,attribute,this.wsInput[i][5]);
		}
	    }
	    inputParams = paramXML + "</p>";
	 } catch (Exception){
        var Msg = NlsFormatMsg(error_mapping_input);
        Log.error("Execute", Msg);
  	 	//alert(Msg);
  	 	return;
	 }
	//alert(inputParams);
    /** Invoke the webservice **/
    this.ws.invokeWS(this.callback, this.operation,this.wsdl,inputParams);
}

/**
 * Utility function to extract the attributeName and index from the
 * last token of the VBL
 */
this.splitNameAndIndex = function(featureName){
   var attribute = new Array();
   var tempIndex= featureName.indexOf("[");
   if(tempIndex!=-1){
        var attribName = featureName.substring(0,tempIndex);
        var index = featureName.substring(tempIndex+1, featureName.length-1);
        attribute[0] = attribName;
        attribute[1] = index;
   }else{
        attribute[0] = featureName;
   }
   return attribute;
}


/**
 * method for Complex Object and user as Done Attribute Mapping
 * @param paramXML - The OP XML
 */
this.handleCmplxAttrMapping = function() {
     var paramXMLCM ="<c name=\"none\">";
     var eobji   = null;
     var eobjIDi = null;
     var value;

     for (var i = 0; i < this.wsInput.length; ++i) {
	if(this.wsInput[i][0] == "null") {
	   value = this.wsInput[i][1];
	} else {
	    var X = this.wsInput[i][0];
	    if (X != eobjIDi) {
	       eobji = findEObjectByXMIID(this.wsInput[i][2], X);
	       eobjIDi = X;
	    }
	    var eattrname = this.wsInput[i][1];
	    value = eobji.eGet(eattrname);
	}

	var requestName=this.wsInput[i][3];
	if(requestName=="null"){
        var Msg = NlsFormatMsg(error_mapping_input);
        Log.error("Execute", Msg);
        throw new EObjectError(Msg);
	}else{
  	    paramXMLCM += this.encodeSimpleType("",value,requestName);
	}
    }
   return paramXMLCM+"</c>";
}

/**
 * method for List of Object and user as Done Attribute Mapping
 * @param paramXML - The OP XML
 */
this.handleSimpleListMapping = function(paramXML,eRefArr,attribName) {
    var paramXMLLM ="<l name=\"none\" >";
    for (k in eRefArr){
	paramXMLLM = paramXMLLM + this.encodeSimpleType(paramXML,eRefArr[k].eGet(attribName));
    }
    return paramXMLLM+"</l>";
}

/**
 * method for converting WDO4JSObject to Input XML
 * @param eObject - The EObject
 * @param eAttribute - The Attribute name.
 * @param isList - indicates that the eReference is a List
 */
this.prepareInputXML = function(pXML,eObjectP,eAttributeP,useAsList) {
   var paramXML =pXML;
   var eAttribute = "";
   var index;
   /** getting the correct attribute name,**/
   if(typeof eAttributeP != "string") {
       eAttribute = eAttributeP[0];
       index = eAttributeP[1];
   } else
      eAttribute = eAttributeP;
   var metadata	= eObjectP.EClass.getEStructuralFeature(eAttribute);
   var tempClass = eObjectP.eGet(eAttribute);
   /* If classtype is reference it means this is a complex object/List */
   if(metadata.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE){
      if(useAsList == "true") {// if(this.isListType(metadata)){
       	    paramXML=paramXML+this.encodeArrayType(paramXML,eObjectP,eAttribute);
       }else{
           if(this.isListType(metadata)){
               eObjectP = tempClass[index];
               eAttribute = null;
           }
           paramXML=paramXML+this.encodeObjectType(paramXML,eObjectP,eAttribute);
       }
   }
   /* else this will be Simple List or simple data type */
   else {
	if(useAsList == "true") { //this.isListType(metadata)) {
	    paramXML=paramXML+this.encodeArrayType(paramXML,eObjectP,eAttribute);
	}else{
	    if(this.isListType(metadata)){
                tempClass = tempClass[Number(index)];
            }
	    paramXML=paramXML+this.encodeSimpleType(paramXML,tempClass,eAttribute);
	}
   }
   return  paramXML ;
}

/**
 * method for encoding the Object type argument
 * paramXML -The XML holding string
 * eObject - The EObject
 * eAttribute - The Attribute name.
 */
this.encodeObjectType = function(paramXML,eObjectParam,eAttributeParam){
    /** Name is hardcoded. Will change this for nested types **/
    var paramXMLObj;
    var tempClass;
    if(eAttributeParam!=null){
         tempClass = eObjectParam.eGet(eAttributeParam);
    }else{
  	 tempClass = eObjectParam;
  	 eAttributeParam="none";

    }
    paramXMLObj="<c name=\"" +eAttributeParam+"\">";
    var attribs = this.mergeArray(tempClass.EClass.getEAllAttributes(),tempClass.EClass.getEAllReferences());
    var j;
    for (j in attribs){
	var attribName = attribs[j].Name;
	/** To ignore the common WDO4JS Object **/
		if(typeof attribName != 'undefined' && attribName !="xmi:id"){var useAsList ="false";
		   if(this.isListType(attribs[j])){
		      useAsList ="true";
		   }
		   paramXMLObj=paramXMLObj+this.prepareInputXML(paramXML,tempClass,attribName,useAsList);
		}
    }
    return paramXMLObj + "</c>";
}

/**
 * method for encoding the Array type argument
 * paramXML -The XML holding string
 * eObjectParam - The EObject
 * eAttribute - The Attribute name.
 */
this.encodeArrayType = function(paramXML,eObjectParam,eAttributeParam){
    if(eAttributeParam == null){
        eAttributeParam = "none";
    }
    var paramXMLArray = "<l name=\"" +eAttributeParam+"\">";
    var tempClass;
    tempClass = eObjectParam.eGet(eAttributeParam);

    if(((tempClass && typeof tempClass == 'object') || typeof tempClass == 'function') && tempClass.constructor == Array){
        var k;
        for (k in tempClass){
	    var tempClassins = tempClass[k];
            if(typeof tempClassins == "string"){
	        paramXMLArray=paramXMLArray+this.encodeSimpleType(paramXML,tempClassins);
	    }else{
	        paramXMLArray=paramXMLArray+this.encodeObjectType(paramXML,tempClassins,null);
	    }
        }
    }
    return paramXMLArray + "</l>";
}

/**
 * Method for checking complex List
 * paramXML -The XML holding string
 */
this.isListType = function(param){
    return (param.getUpperBound()>1||param.getUpperBound()== -1);
}


/**
 * Method for encoding the simple types
 * paramXML -The XML holding string
 * argsValue - The Attribute value.
 * argType - The Attribute type.
 * argName - The Attribute name.
 */
this.encodeSimpleType = function(paramXML,argsValue,argName){
    if(typeof(argName) == 'undefined') {
	argName = "none";
    }
    return "<s name=\""+argName+"\" value=\""+argsValue+"\" />";
}

/**
 * wrapper method for checking the list type
 */
this.isList = function(eObj,eAttrib){
    var oput = eObj.EClass.getEStructuralFeature(eAttrib);
    if(oput.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE || oput.CLASSTYPE == EStructuralFeature.CLASSTYPE_EATTRIBUTE){
	return this.isListType(oput);
    }
    return false;
}

/**
 * method merges array1+ array2
 * array1 -The array1
 * array2 - The array2
 */
this.mergeArray = function(array1,array2){
	var arrNew=new Array();
	if(array1==null && array2==null)
		return null;
	if(array1==null)
		return array2;
	if(array2==null)
		return array1;
	for(var id=0;id<array2.length;id++){
		arrNew[id]=array2[id];
	}
	for(var id=0;id<array1.length;id++){
		arrNew[array2.length+id]=array1[id];
	}

	return arrNew;
}

 /**
  * a deep copy of the object , copy all from source to target
  * @param source - Source eObject
  * @param target - Target eObject
  */

 /* this.ePrint = function (eObject,header,pheader){
  *
  *  if(str == null) {
  *  var str = "";
  *  }
  *  if(header== null) {
  *      pheader ="";
  *      header = "";
  *  }
  *  var sourceAttribs = eObject.EClass.getEAllAttributes();
  *  for(var i=0; i< sourceAttribs.length; ++i)  {
  *       var attribName = sourceAttribs[i].Name;
  *  	if(typeof attribName != 'undefined' && attribName !="xmi:id"){
  *   	   var value = eObject.eGet(attribName);
  *  	   str += "\n" + header + attribName + " : " + value;
  *  	}
  *  }
  *
  *  var sourceRefs = eObject.EClass.getEAllReferences();
  *
  *  pheader = header;
  *  header += "      ";
  *  for(var i=0; i< sourceRefs.length; ++i)  {
  *       var eRef = sourceRefs[i];
  *       if(this.isList(eObject,eRef.Name)){
  *           var eRefArr = eObject.eGet(eRef.Name);
  *           for(var j=0; j< eRefArr.length; ++j)  {
  *              str += "\n" + pheader + "Obj :   " + eRef.Name + "[ " + j +" ] ";
  *              str += this.ePrint(eRefArr[j],header,pheader);
  *           }
  *       } else {
  *          str += "\n" +pheader + "Obj :   " + eRef.Name;
  *          str += this.ePrint(eObject.eGet(eRef.Name),header,pheader);
  *       }
  *  }
  *  return str;
  *  }
  */
 /**
  * This method handles the output if List type
  * @param root - the js object given by flash
  * @param eObj - the parent eObject for this result
  * @eAttrib - the attribute name
  */
 this.handleListGraph = function(root,eobj,eAttrib){
    var metadata = eobj.EClass.getEStructuralFeature(eAttrib);
    if(metadata.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE) {
         /** Handle complex List **/
         var newEObj = new Array();
         for(var i=0; i< root.length; i++){
            newEObj[i] = eobj.CreateEObject(eAttrib);
            this.buildEObjectGraph(newEObj[i],root[i]);
            eobj.eAdd(eAttrib,newEObj[i]);
         }
    } else {
       eobj.eSet(""+eAttrib, root);
    }
}


 /**
  * The build eObj method do a deep copy of the java script object to the
  * eObject.
  */
  this.buildEObjectGraph = function (eObj,Obj){
     var attrib;
     for (attrib in Obj) {
        var NewObj = Obj[""+attrib];
        if (this.isArray(NewObj) == true) {
           this.handleListGraph(NewObj,eObj,attrib);
        }else if(this.isObject(NewObj) == true) {
	       var innerEObj = eObj.CreateEObject(attrib);
           this.buildEObjectGraph(innerEObj,NewObj);
           eObj.eSet(""+attrib,innerEObj);
        }else {
           eObj.eSet(""+attrib,NewObj);
       }
     }
 }


 this.isObject = function(a) {
    return ((a && typeof a == 'object') || typeof a == 'function');
  }
 this.isArray = function(a) {
    return (((a && typeof a == 'object') || typeof a == 'function') && a.constructor == Array);
 }

this.handleOutputList = function(root,eobj,eAttrib){
   var metadata = eobj.EClass.getEStructuralFeature(eAttrib);
   if(metadata.CLASSTYPE == EStructuralFeature.CLASSTYPE_EREFERENCE) {
        /** Handle complex List **/
        var newEObj = new Array();
        for(var i=0; i< root.length; i++){
           newEObj[i] = eobj.CreateEObject(eAttrib);
           this.buildEObjectGraph(newEObj[i],root[i]);
        }
        mergeData(newEObj,eobj,eAttrib);
   } else {
      eobj.eSet(""+eAttrib, root);
   }
}

/**
 * The callback method for handling the result.The WebService Emitter
 * emits a peice of code that calls this method
 */
this.resultHandler = function(){

    var eobj   = null;
    var eobjID = null;
    var resultString  = this.ws.getResultSet();
    //alert(resultString);
    eval(resultString);

    try {
	if(root == 'undefined') {
	   return;
	}
	/** Handle simple array type, if attributeName is present **/
	if(this.OutputParams[0][4] != "null"){
	    eobj = findEObjectByXMIID(this.OutputParams[0][2], this.OutputParams[0][0]);
	    var arrayRef = eobj.eGet(this.OutputParams[0][1]);
	    if(arrayRef != null && arrayRef.length == root.length){
	        for(var i=0;i<arrayRef.length;i++){
	              arrayRef[i].eSet(""+this.OutputParams[0][4],root[i]);
	        }
	    } else {
	    	var Msg = NlsFormatMsg(error_mapping_output);
		Log.error("ResultHandler", Msg);
                //alert(Msg);
	    }
	    return;
	}
	/** Handle Array of Simple or Complex **/
	if(this.isArray(root)) {
	    /** Handle List **/
	    eobj = findEObjectByXMIID(this.OutputParams[0][2], this.OutputParams[0][0]);
	    this.handleOutputList(root,eobj,this.OutputParams[0][1]);
	} else if(this.isObject(root)) {
	   /** Handle complex and nested complex **/
	   if(this.outputmapped == "true") {
	       for (var i = 0; i < this.OutputParams.length; ++i) {
	           var X = this.OutputParams[i][0];
	           if (X != eobjID) {
	              eobj = findEObjectByXMIID(this.OutputParams[i][2], X);
	              eobjID = X;
	           }
	           eobj.eSet(""+this.OutputParams[i][1], root[this.ret[i]]);
	       }
	   } else {
	       eobj = findEObjectByXMIID(this.OutputParams[0][2], this.OutputParams[0][0]);
	       var attribute = this.splitNameAndIndex(this.OutputParams[0][1]);
	       var eattrname = attribute[0];
	       var newGraph = eobj.CreateEObject(eattrname);
	       this.buildEObjectGraph(newGraph,root);
	       mergeData(newGraph,eobj,eattrname);
 	  }
       } else {
	  /** Handle simple types**/
      	      eobj = findEObjectByXMIID(this.OutputParams[0][2], this.OutputParams[0][0]);
	      /** will change the following statement later **/
	      var attribute = this.splitNameAndIndex(this.OutputParams[0][1]);
	      eobj.eSet(""+attribute[0], root);
       }
    } catch (e){
        var Msg = NlsFormatMsg(error_mapping_output);
        Log.error("ResultHandler", Msg);
        //alert(Msg);
        return;
    }
 }

} //end



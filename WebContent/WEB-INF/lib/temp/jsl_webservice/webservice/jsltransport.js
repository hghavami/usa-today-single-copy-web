//
// Webservice Transport Layer
// - Jobi
//


function JSLHttpRequest()
{
	//	alert('JSLHttpRequest');
	var xmlHttp= new Object();
	if (navigator.appName == "Netscape")
		xmlHttp= new XMLHttpRequest();
	else
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");

	this.responseText="";
	this.responseXML="";
	this.readyState=1;

	this.open= function(prot,url,flag)
	{	
		if (navigator.appName == "Netscape")
			netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
		xmlHttp.open(prot,url,flag);
	}
	
	this.onnsreadystatechange= function()
	{
		this.jsdoobj.readyState=xmlHttp.readyState;
		this.jsdoobj.responseText=xmlHttp.responseText;
		this.jsdoobj.responseXML=xmlHttp.responseXML;
		this.jsdoobj.onreadystatechange();
	}
	
	this.setRequestHeader = function (key, value)
	{
		xmlHttp.setRequestHeader(key, value);
	}
	
	this.send = function(param)
	{
		jsdoSelf=this;
		oniereadystatechange= function()
		{
			jsdoSelf.readyState=xmlHttp.readyState;
			if (xmlHttp.readyState == 4)
			{
				// alert(xmlHttp.responseText);
				jsdoSelf.responseText=xmlHttp.responseText;
				jsdoSelf.responseXML=xmlHttp.responseXML;
				jsdoSelf.documentElement=xmlHttp.responseXML.documentElement;
			}
			jsdoSelf.onreadystatechange();
		}	
		if (navigator.appName == "Netscape")
		{
			this.onnsreadystatechange.jsdoobj = this;
			xmlHttp.onreadystatechange= this.onnsreadystatechange;
			xmlHttp.send(param);
		}
		else
		{
			xmlHttp.onreadystatechange= oniereadystatechange;
			xmlHttp.send(param);
		}
	}
}

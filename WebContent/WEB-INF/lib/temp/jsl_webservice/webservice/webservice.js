/** ==================================================================
 *  Licensed Materials - Property of IBM
 *  (c) Copyright IBM Corp. 2003.  All rights reserved.
 *
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM
 *  Corp.
 *
 *  DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
 *  sample code created by IBM Corporation.  This sample code is
 *  not part of any standard or IBM product and is provided to you
 *  solely for the purpose of assisting you in the development of
 *  your applications.  The code is provided "AS IS", without
 *  warranty of any kind.  IBM shall not be liable for any damages
 *  arising out of your use of the sample code, even if they have
 *  been advised of the possibility of such damages.
 * ==================================================================
 **/

/**
 * ------------------------------------------------------------
 * WebService.js
 * @author Rajesh , Manu 
 * ------------------------------------------------------------
 **/
 
 /**
  * @class WebServiceControl
  * 
  * This JS class acts as a bridge between the webservice odc component and the 
  * flash component. It delegates the webservice call to the flash action script.
  **/

 /**
  * @method WebServiceControl
  * Constructor for the class. Receives the flash component and wrap it from the
  * adapter
  * @param FlashObject flashComp
  *			.
  **/
 function WebServiceControl(flashComp) {
        if(flashComp == null) {
           var Msg = NlsFormatMsg(no_flash_component);
           Log.error("resultHandler", Msg);
           throw new EObjectError(Msg);          
        }
	this.flashWS =   flashComp; 	

/**
 * The main function that invokes the flash webservice
 * @param callback type String - the callback method name. The flash control will
 * invoke this java script method for passing the results.
 * @param operation type String - The webservice method name 
 * @param wsdl type String - The webservice end point URL 
 * @param paramXML type String - The XML formatted arguments for this WS
 * @param ret type Array - An array of Strings, the WSDL element names for retrieving
 * the result
 */
this.invokeWS = function(callback,operation,wsdl,paramXML) {
	try {
		/** Getting the flash component **/
		flashWS = this.flashWS;
		/**
		 * Passing the webservice URL, Operation name and callback method
		 * name to the flash control. Upon completeing the call the flash will
		 * invoke the callback method.
		 */
		flashWS.SetVariable("wsdlURL",wsdl);
		flashWS.SetVariable("operationName",operation);
		flashWS.SetVariable("callbackMethod",callback);
		flashWS.SetVariable("xmlString",paramXML);
		/** The attributes names of the return values **/
		//this.registerRetVal(flashWS,ret);
		/** Calling the update frame of the flash control **/
		flashWS.TCallLabel("/","update");	
	} catch (e){
        var Msg = NlsFormatMsg(error_accessing_flash);
        Log.error("Invoke", Msg);
        alert(Msg);    
	}
}

/**
 * This method retrieves the result from flash
 */ 
this.getResultSet =  function(){
     return this.flashWS.GetVariable("returnValues");
}

}


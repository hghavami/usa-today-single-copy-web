7/1/2004//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003.  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//==================================================================
//-------------------------------------------
// GraphDrawControl.js
// @author Kaushal Kurapati,  U.Dinesh
//-------------------------------------------

/**
 * @class public GraphDrawControl
 * GraphDrawControl allows developers to render data in 3 forms: pie, bar, or line
 * using a Macromedia flash object.
**/

/**
 * @variable public ChartCheck Boolean
**/

var ChartCheck=null;



/**
 * The following setion is used to make graph work fine in mozilla/graph/tabbedpanel case.
**/
var odcGraphtimer = 1;
var odcGraphTimeObj = null;
if(!isIE())
{
	window.addEventListener('load',function(){odcGraphTimeObj = window.setTimeout(odcGraphRedraw,odcGraphtimer);} ,false);
}


function odcGraphRedraw() {
	var anyFailer = false;
	var obj = null;
	for(var key in ODCRegistry.Elements) {
		try{
			obj = ODCRegistry.Elements[key].JSObject;
			if(obj instanceof GraphDrawControl){
				if(obj.chartObj.GetVariable("pointData") == null || obj.chartObj.GetVariable("pointData") == "")
					obj.ShowGraph();
				else
					continue;
				if(obj.chartObj.GetVariable("pointData") == null || obj.chartObj.GetVariable("pointData") == "")
					anyFailer = true;
			}
		}catch(e){
			continue;
		}
	}
	odcGraphtimer += 10;
	//in case the graph draw keep redrawing when the passed in dataset is empty.So the threhold is 1 min.
	if(anyFailer&&odcGraphtimer<60000){
		odcGraphTimeObj = window.setTimeout(odcGraphRedraw, odcGraphtimer);
	}else{
		//clear timer
		window.clearTimeout(odcGraphTimeObj);
	}
}

/**
 * end of this section
**/



/**
 * @method GraphDrawControl
 * Constructor for the GraphDrawControl class.
 * Initializes class vars and loads up a color data array of 31 most used web colors.
 * If there are more than 31 points in the data to be rendered, the colors will run out.
 *
**/
function GraphDrawControl()
{
       // Variable Declarations & initializations
       this.chartTitle = "";
       this.xAxisTitle = "";
       this.yAxisTitle = "";
       this.yAxisNLData = ""; //Formatted data points after calculating the min, max y-axis values
	   this.seriesSelectorsNLData = "";//Formatted data points after calculating the min, max y-axis values
       this.labelNLData = ""; //NL Label data points after invoking  the number converters

	   this.normalizedTitle= new Array();//"";
	   this.commonNormalizedTitle= "";//"";

       this.dataSeriesNames = "";

       this.lblPie = chartdraw_lblPie;
       this.lblBar = chartdraw_lblBar;
       this.lblLine = chartdraw_lblLine;
       this.tooltipShare = chartdraw_tooltipShare;
       this.tooltipSeries = chartdraw_tooltipSeries;
       this.tooltipLabel = chartdraw_tooltipLabel;
       this.tooltipValue = chartdraw_tooltipValue;
       this.msgPieChartError = chartdraw_msgPieChartError;

	   this.lblShowAll = chartdraw_lblShowAll;
	   this.lblChartType = chartdraw_lblChartType;
	   this.lblSeries = chartdraw_lblSeries;

       this.yAxisDivisions="";
   	   var graphcsspath="/theme/graph.css";
	   this.css_url = "";


       this.defaultChartType = "Pie"; // could be Pie or Bar or Line; Pie by default
       this.showLabel; // true - show labels; false - don't show labels
       this.showPie; // true - show pie chart; false - don't show pie chart
       this.showBar; // true - show bar chart; false - don't show bar chart
       this.showLine; // true - show line chart; false - don't show line chart
       this.showLegend; // true - show legands; false - don't show legands

	   this.styleClass="";
	   this.selectedSeries = -1;

	   this.showHorizontalLinesBar; // true - show horizontal lines in the bar chart; false - don't show horizontal lines
	   this.showHorizontalLinesLine; // true - show horizontal lines in the line chart; false - don't show horizontal lines
	   this.showVerticalLinesLine; // true - show vertical lines in the line chart; false - don't show vertical lines


       this.pointData = null;
       this.pointNLData = null;

       this.seriesColorData = "";
       this.pieChartColorData = "";
       this.chartObj  = null; // this should be set to whatever is the chart id in object embed
       this.Adapter   = null;

       this.dataSeparator = "`";
       this.seriesSeparator="*";
       this.splitYAxis = false;

	   this.noGroupOpSepcified="";
       this.invalidgroupOperation="";

 	   this.label_converter_exception = "";
	   this.data_converter_exception = "";
	   this.yaxis_converter_exception = "";

 	   var data_exception_shown = false;
	   var label_exception_shown = false;
	   var data_exception_shown = false;
	   var yaxis_exception_shown = false;
	   this.data_converter_exception = "";

	   this.show_no_graph = false;

       var tempSeriesNames = "";
       var retvalue="";

       this.Flash_SeriesColor_Array =[
							       ["6868D8","blue pens"],
								   ["F8C800","goldenrod pens"],
								   ["F83800","red pens"],
						           ["209800","leaf grean pens"],
								   ["F88800","orange pens"],
                                   ["0090B0","deep sky blue pens"],
								   ["A0A0A0","light grey pens"],
           					       ["33338C","navy blue pens"],
  							       ["E104D9","medium violet red pens"],
								   ["04E132","lime green pens"]
 		                           ];

	   this.Flash_PieSectionColor_Array =[
		                           ["6868D8","blue pens"],
                                   ["F8C800","goldenrod pens"],
							       ["F83800","red pens"],
                                   ["209800","leaf grean pens"],
                                   ["F88800","orange pens"],
                                   ["0090B0","deep sky blue pens"],
		                           ["A0A0A0","light grey pens"],
                                   ["33338C","navy blue pens"],
							       ["E104D9","medium violet red pens"],
                                   ["04E132","lime green pens"],
	                               ["474747","dark grey"],
                                   ["0BE6DE","cyan pens"],
                                   ["816940","saddle brown pens"],
                                   ["F5FC33","yellow pens"],
                                   ["F56AB2","viiolet red pens"],
                                   ["145A01","spring green pens"],
                                   ["FDB663","sienna pens"],
                                   ["9B9351","light orange pens"],
                                   ["C5C5C5","honey dew pens"],
                                   ["9205C6","blue violet pens"],
								   ["C6BF05","orange pens"],
                                   ["A3BDFA","medium purple pens"],
                                   ["C3ED7B","pale green pens"],
                                   ["D46F03","fire brick  pens"],
                                   ["F9F890","light yellow pens"],
					               ["CC9189","indian red pens"],
                                   ["A9BC9D","grey pens"],
								   ["419189","green pens"],
                                   ["914161","light red pens"],
								   ["9398B5","dark white pens"]
	   ];

		this.isRendered = false;

		this.type = "GraphDraw";
		this.pageId = "graphdraw1";//default pageId which will be changed by ODCRegistry when do addElementInfo


		// Methods

	/**
	* @method public getType
	*   This function is return the type of the control
	**/
		this.getType = function(){
			return this.type;
		}


	/**
	* @method public getId
	*   This function return the id of the control
	**/
		this.getId = function(){
			return this.pageId;
		}


       /**
         * @method public setValue
         * Method calls refreshDataSet() in Adapter to load up pointdata to be displayed in the graph
         * If the eObject is null--in this case the refreshDataSet() function will pick up
         * the parentEObject in the Adapter and get the relevant data to be rendered from there.
         * Method calls loadColorData() to load color data array and calls ShowGraph() to actually
         * update the values in the chart object.
        **/
       this.setValue = function()
       {
			this.Adapter.refreshDataSet(null);

 			if(this.Adapter.invalidLabelAttributeName) return;

			if(this.invalidgroupOperation.substring(0,4) == "true") return;

			if(this.show_no_graph == true) return;

 			if (this.Adapter.nullDataValue.substring(0,4) =="true") return;

			if (this.Adapter.invalidDatatype.substring(0,4) =="true") return;

			this.separateSeries();

			if(this.isRendered == true)
			{
				this.Adapter.refreshDataSet(null);
				this.loadColorData();
				this.ShowGraph();
				this.separateSeries();
		    }

         }

       /**
            * @method public seperateSeries
          * parses the string using dataSeparator as the delimiter into multiple series using the seriesSeperator
       **/
       this.separateSeries = function()
       {
              for( var r = 0 ; r < this.dataSeriesNames.length;  r++)
              {
                     if (r==0) {
                            tempSeriesNames = this.dataSeriesNames[r];
                     } else {
                            tempSeriesNames = tempSeriesNames + this.seriesSeparator + this.dataSeriesNames[r];
                     }
					 if (this.normalizedTitle[r] != "undefined" && typeof(this.normalizedTitle[r]) != "undefined"){
 					 tempSeriesNames = tempSeriesNames + this.dataSeparator + this.normalizedTitle[r];
 					 }
              }
       }


       /**
         * @method public show
         * Method to update the chart object.
         * This function waits for the chartObject to be loaded; Netscape has a problem
         * in loading it and so we keep polling to see if the object has been
         * acquired. Once it has we call the updateChart() to do the setting of params
         * This solution is not working on Mozilla though. It is working on Netscape.
         * If navigator is NOT Netscape (IE), updateChart() is called directly.
        **/
       this.show = function()
       {
		   this.ShowGraph();
	   }



		/**
		* @method public getSelectedItems
		*   To make all the data bound controls api consistent, provide this function for graphdraw, actually it is
		*	just return an empty array.
		* @return Array
		*	An empty array
		**/
			this.getSelectedItems = function()
			{

				return new Array();
			}


		/**
		* @method public getHighlightedItem
		*   To make all the data bound controls api consistent, provide this function for graphdraw, actually it is
		*	just return a null object.
		* @return object
		*	null object
		**/
			this.getHighlightedItem = function()
			{
				return 	null;

			}


       /**
         * @method private ShowGraph
         * Method to update the chart object.
         * This function waits for the chartObject to be loaded; Netscape has a problem
         * in loading it and so we keep polling to see if the object has been
         * acquired. Once it has we call the updateChart() to do the setting of params
         * This solution is not working on Mozilla though. It is working on Netscape.
         * If navigator is NOT Netscape (IE), updateChart() is called directly.
        **/
       this.ShowGraph = function()
       {
 			//If a data Series has no attribute name specified, Log and alert the msg
			if (this.Adapter.emptyDataAttributeName.substring(0,4) =="true"){
					var arg = new Array();

					var tempInvAttname = this.Adapter.emptyDataAttributeName;
					var Sername = tempInvAttname.substring(5,tempInvAttname.length-1);
					arg[0] = this.dataSeriesNames[Sername];
					var Msg = NlsFormatMsg(no_data_atttribute,arg);
					Log.error("Execute", Msg);
					alert(Msg);
					return;
 			   }

			//If Invalid Data Series is specified, Log and alert the msg
			if (this.Adapter.invalidDataAttributeName.substring(0,4) =="true"){
					var arg = new Array();

					var tempInvAttname = this.Adapter.invalidDataAttributeName;
					var Sername = tempInvAttname.substring(5,tempInvAttname.length-1);
					arg[0] = this.dataSeriesNames[Sername];
					var Msg = NlsFormatMsg(invalid_data_atttribute,arg);
					Log.error("Execute", Msg);
					alert(Msg);
					return;
 			   }

			//If No Data Series are specified, Log and alert the msg
			if (this.Adapter.noDataSeriesAttributes){
					var Msg = NlsFormatMsg(no_data_series);
					Log.error("Execute", Msg);
					alert(no_data_series);
					return;
			   }

 			//If label attribute name is empty, Log and alert the msg
			   if (this.Adapter.emptyLabelAttributeName) {
					var Msg = NlsFormatMsg(no_label_attribute);
					Log.error("Execute", Msg);
					alert(no_label_attribute);
					return;
 			   }

 			//If label attribute name is invalid, Log and alert the msg
			   if (this.Adapter.invalidLabelAttributeName) {
					var Msg = NlsFormatMsg(invalid_label_attribute);
					Log.error("Execute", Msg);
					alert(invalid_label_attribute);
					return;
 			   }

			//No grouping operation is specified, Log and alert the msg
  				var StrNoOper = "";
 				if (this.noGroupOpSepcified.length !=0){
				for (var i=0;i<this.noGroupOpSepcified.length;i++){
					if (i > 0) {
						StrNoOper += ", ";
					}
						StrNoOper +=this.dataSeriesNames[this.noGroupOpSepcified.charAt(i)];
				}

				var arg = new Array();
				arg[0] = StrNoOper;
				var Msg = NlsFormatMsg(no_group_operation_specified,arg);
				Log.error("Execute", Msg);
				alert(Msg);
				return;
 				}

			//If Invalid grouping operation is specified, Log and alert the msg

			var tempInvGrpOper = this.invalidgroupOperation;
			if(tempInvGrpOper.substring(0,4) == "true") {
			   var arg = new Array();
			   var StrInvOper = this.dataSeriesNames[parseInt(tempInvGrpOper.charAt(4))];
			   var StrInv = tempInvGrpOper.substring(5,tempInvGrpOper.length);
				arg[0] = StrInvOper//retvalue.substring(4,retvalue.length);
				arg[1] = StrInv//retvalue.substring(4,retvalue.length);
				var Msg = NlsFormatMsg(invalid_group_operation,arg);
				Log.error("Execute", Msg);
				alert(Msg);
				return;
 		  }


		    //If any of the data values contains null, Log and alert the msg

  			if (this.Adapter.nullDataValue.substring(0,4) =="true"){
 					var arg = new Array();
 					var nullData = this.dataSeriesNames[parseInt(this.Adapter.nullDataValue.charAt(4))];
  					arg[0] = nullData;
					var Msg = NlsFormatMsg(null_data_value,arg);
					Log.error("Execute", Msg);
					alert(Msg);
					return;
 			   }

		  //If Invalid data type is specified, Log and alert the msg

  			if (this.Adapter.invalidDatatype.substring(0,4) =="true"){
 					var arg = new Array();
 					var StrInvDatatype = this.dataSeriesNames[parseInt(this.Adapter.invalidDatatype.charAt(4))];
  					arg[0] = StrInvDatatype;
					var Msg = NlsFormatMsg(invalid_series_attribute_data_type,arg);
					Log.error("Execute", Msg);
					alert(Msg);
					return;
 			   }


		  if(this.show_no_graph == true) return;

			if ((this.label_converter_exception != "") && (label_exception_shown == false))	{
				alert(this.label_converter_exception);
				label_exception_shown =  true;
			}


			if ((this.data_converter_exception != "") && (data_exception_shown == false))	{
 			alert(this.data_converter_exception);
			data_exception_shown =  true;
			}

			if ((this.yaxis_converter_exception != "") && (yaxis_exception_shown == false))	{
			alert(this.yaxis_converter_exception);
			yaxis_exception_shown =  true;
			}


		  obj = this;
		  if (navigator.appName == "Netscape") {
			 if (this.chartObj.SetVariable != undefined)
			 {   this.updateChart();       }
			 else
			 {       window.setTimeout(function () {obj.ShowGraph()}, 10);   }
		  } else
			  	this.updateChart();


        this.isRendered= true;

       }


       /**
         * @method public updateChart
         * Function to actually set the chart title, type, point, color and label data
        **/
       this.updateChart = function()
       {

              if (this.chartTitle!="" && typeof(this.chartTitle)!="undefined") this.chartObj.SetVariable("chartTitle", this.chartTitle);
              if (this.xAxisTitle!="" && typeof(this.xAxisTitle)!="undefined") this.chartObj.SetVariable("xAxisTitle", this.xAxisTitle);

 			  if (this.commonNormalizedTitle != undefined){
 				  if (typeof(this.yAxisTitle)!="undefined") this.chartObj.SetVariable("yAxisTitle", this.yAxisTitle + this.dataSeparator + this.commonNormalizedTitle);
			  }else{
 				  if (typeof(this.yAxisTitle)!="undefined") this.chartObj.SetVariable("yAxisTitle", this.yAxisTitle);
			  }

              if (this.dataSeriesNames!="" && typeof(this.dataSeriesNames)!="undefined") this.chartObj.SetVariable("seriesNames", tempSeriesNames);
              if (this.yAxisDivisions!="" && typeof(this.yAxisDivisions)!="undefined") this.chartObj.SetVariable("yAxisDivisions", this.yAxisDivisions);

			 //The chart type should not change during data refresh.
				var obj = null;
				for(var key in ODCRegistry.Elements) {
					try{
						obj = ODCRegistry.Elements[key].JSObject;
						if(obj instanceof GraphDrawControl && obj.chartObj == this.chartObj){
							currentgraph = obj.chartObj.GetVariable("defaultChartType");
 							if (currentgraph !=  "null" || currentgraph != null || currentgraph != 'undefined')	{
								this.defaultChartType = obj.chartObj.GetVariable("defaultChartType");
								this.selectedSeries = parseInt(obj.chartObj.GetVariable("selectedSeries"));
 							}
						}
					}catch(e){
						continue;
					}
				}


              if (this.defaultChartType!="" && typeof(this.defaultChartType)!="undefined") this.chartObj.SetVariable("defaultChartType", this.defaultChartType);
			  if (this.selectedSeries!="" && typeof(this.selectedSeries)!="undefined")  this.chartObj.SetVariable("selectedSeries", this.selectedSeries);
              if (this.showLabel!="" && typeof(this.showLabel)!="undefined") this.chartObj.SetVariable("showLabel", this.showLabel);
              if (this.showPie!="" && typeof(this.showPie)!="undefined") this.chartObj.SetVariable("showPie", this.showPie);
              if (this.showBar!="" && typeof(this.showBar)!="undefined") this.chartObj.SetVariable("showBar", this.showBar);
              if (this.showLine!="" && typeof(this.showLine)!="undefined") this.chartObj.SetVariable("showLine", this.showLine);
			  if (this.styleClass!="" && typeof(this.styleClass)!="undefined") this.chartObj.SetVariable("styleClass", this.styleClass);
              if (this.showLegend!="" && typeof(this.showLegend)!="undefined") this.chartObj.SetVariable("showLegend", this.showLegend);
			  if (this.showHorizontalLinesBar!="" && typeof(this.showHorizontalLinesBar)!="undefined") this.chartObj.SetVariable("showHorizontalLinesBar", this.showHorizontalLinesBar);
			  if (this.showHorizontalLinesLine!="" && typeof(this.showHorizontalLinesLine)!="undefined") this.chartObj.SetVariable("showHorizontalLinesLine", this.showHorizontalLinesLine);
			  if (this.showVerticalLinesLine!="" && typeof(this.showVerticalLinesLine)!="undefined") this.chartObj.SetVariable("showVerticalLinesLine", this.showVerticalLinesLine);
              if (this.labelNLData!="" && typeof(this.labelNLData)!="undefined") this.chartObj.SetVariable("labelNLData", this.labelNLData);
              if (this.pointData!="" && typeof(this.pointData)!="undefined") this.chartObj.SetVariable("pointData", this.pointData);
              if (this.yAxisNLData!="" && typeof(this.yAxisNLData)!="undefined") this.chartObj.SetVariable("yAxisNLData", this.yAxisNLData);
 			  if (this.seriesSelectorsNLData!="" && typeof(this.seriesSelectorsNLData)!="undefined") this.chartObj.SetVariable("seriesSelectorsNLData", this.seriesSelectorsNLData);
              if (this.pointNLData!="" && typeof(this.pointNLData)!="undefined") this.chartObj.SetVariable("pointNLData", this.pointNLData);
			  if (this.singlePointNLData!="" && typeof(this.singlePointNLData)!="undefined") this.chartObj.SetVariable("singlePointNLData", this.singlePointNLData);
              if (this.splitYAxis!="" && typeof(this.splitYAxis)!="undefined") this.chartObj.SetVariable("splitYAxis", this.splitYAxis);
              if (this.seriesColorData!="" && typeof(this.seriesColorData)!="undefined") this.chartObj.SetVariable("seriesColorData", this.seriesColorData);
              if (this.pieChartColorData!="" && typeof(this.pieChartColorData)!="undefined") this.chartObj.SetVariable("pieChartColorData", this.pieChartColorData);
              if (this.dataSeparator !="" && typeof(this.dataSeparator)!="undefined") this.chartObj.SetVariable("dataSeparator", this.dataSeparator);
              if (this.seriesSeparator !="" && typeof(this.seriesSeparator)!="undefined") this.chartObj.SetVariable("seriesSeparator", this.seriesSeparator);
              if (typeof(this.lblPie) !="undefined") this.chartObj.SetVariable("lblPie", this.lblPie);
              if (typeof(this.lblBar)!="undefined") this.chartObj.SetVariable("lblBar", this.lblBar);
              if (typeof(this.lblLine)!="undefined") this.chartObj.SetVariable("lblLine", this.lblLine);
			  if (typeof(this.lblShowAll)!="undefined") this.chartObj.SetVariable("lblShowAll", this.lblShowAll);
			  if (typeof(this.lblChartType)!="undefined") this.chartObj.SetVariable("lblChartType", this.lblChartType);
			  if (typeof(this.lblSeries)!="undefined") this.chartObj.SetVariable("lblSeries", this.lblSeries);
              if (this.tooltipSeries!="" && typeof(this.tooltipSeries)!="undefined") this.chartObj.SetVariable("tooltipSeries", this.tooltipSeries);
              if (this.tooltipLabel!="" && typeof(this.tooltipLabel)!="undefined") this.chartObj.SetVariable("tooltipLabel", this.tooltipLabel);
              if (this.tooltipShare!="" && typeof(this.tooltipShare)!="undefined") this.chartObj.SetVariable("tooltipShare", this.tooltipShare);
              if (this.tooltipValue!="" && typeof(this.tooltipValue)!="undefined") this.chartObj.SetVariable("tooltipValue", this.tooltipValue);
              if (this.msgPieChartError!="" && typeof(this.msgPieChartError)!="undefined") this.chartObj.SetVariable("msgPieChartError", this.msgPieChartError);


			  //To send the graph style sheet path to flash
 			  var isSlash = URL_REWRITER_PREFIX.charAt(0);
			  if (isSlash == "/")  {
 					var projectName = URL_REWRITER_PREFIX.substring(1,URL_REWRITER_PREFIX.lastIndexOf("/",URL_REWRITER_PREFIX.length - 2));
					this.css_url = "/" + projectName + graphcsspath;
			  }
			  else{
				  var projectName = URL_REWRITER_PREFIX.substring(0,URL_REWRITER_PREFIX.lastIndexOf("/",URL_REWRITER_PREFIX.length - 2));
				  this.css_url = projectName + graphcsspath;
			  }
 
 			  if (this.css_url !="" && typeof(this.css_url)!="undefined") this.chartObj.SetVariable("css_url", this.css_url);


              //this is a dirty fix for the live script communication error that occurs when updating the graph draw for the first time
              //the graphdraw seems to load fine despite this error...do nothing about this right now
               try{
                     this.chartObj.TCallLabel("/","update");
              }
              catch(e){

              }
       }

       /**
         * @method public setChartType
         * Method to be able to set the chart type directly
        **/
       this.setChartType = function(chart_type)
       {
              this.defaultChartType = chart_type;
              //this.chartObj.SetVariable("defaultChartType", chart_type);
       }

       /**
         * @method public loadColorData
         * Method to load up the same number of colors that there are data points.
         * Cycle through the Flash_ColorLabel_Array in GraphDrawControl and
         * construct a seriesColorData string to be fed to updateChart().
        **/
		this.loadColorData = function()
 		{
			this.seriesColorData = "";
			this.pieChartColorData = "";
  	 		if (this.pointData.length == "null" || this.pointData.length  == null || this.pointData.length  == 'undefined'){
 				this.seriesColorData += this.Flash_SeriesColor_Array[0][0];
				this.pieChartColorData += this.Flash_PieSectionColor_Array[1][0];
			}
			else
			{
			var points = this.pointData.split(this.dataSeparator);
 			if(points.length!='undefined')
            {
				var maxSeriesColors = this.Flash_SeriesColor_Array.length;
				var maxSectionColors = this.Flash_PieSectionColor_Array.length;

 				//loop through the data series, and pick the next color off the series color list for that series
				//if the number of series has been exceeded, loop back and start from the beginning
				var j = 0;
				for (var i=0; i<this.dataSeriesNames.length ;i++)
				{
					if (i > 0) {
						this.seriesColorData += this.seriesSeparator;
					}
					if (i >= maxSeriesColors) {
 	 					this.seriesColorData += this.Flash_SeriesColor_Array[j++][0];
 						if (j>=maxSeriesColors){
							j = 0;
						}

					}
					else {
		 					this.seriesColorData += this.Flash_SeriesColor_Array[i][0];
					}
				}

				//loop through the data points, and pick the next color off the points color list
				//if the number of points has been exceeded, loop back and start from the beginning
				var j = 1;
				for(var i=0;i<points.length; i++)
                {
 					if (i > 0) {
						this.pieChartColorData += this.seriesSeparator;
					}
					if (i >= maxSectionColors) {
 						this.pieChartColorData+= this.Flash_PieSectionColor_Array[j++][0];
 						if (j>=maxSectionColors){
							j = 1;
						}
					}
					else {
 						this.pieChartColorData+= this.Flash_PieSectionColor_Array[i][0];
					}
				}
              }
		} //end else
         }
}

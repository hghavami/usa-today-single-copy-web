//==================================================================
// Licensed Materials - Property of IBM
// (c) Copyright IBM Corp. 2003.  All rights reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with IBM
// Corp.
//
// DISCLAIMER OF WARRANTIES.  The following [enclosed] code is
// sample code created by IBM Corporation.  This sample code is
// not part of any standard or IBM product and is provided to you
// solely for the purpose of assisting you in the development of
// your applications.  The code is provided "AS IS", without
// warranty of any kind.  IBM shall not be liable for any damages
// arising out of your use of the sample code, even if they have
// been advised of the possibility of such damages.
//==================================================================
//------------------------------------------------------------
// GraphDrawAdapter.js
// @author Kaushal Kurapati, U.Dinesh
//------------------------------------------------------------

/**
  * @class GraphDrawAdapter
  * Adapter class that facilitates a refresh of the data set for rendering data
  * using the GraphDrawControl (pie, bar, or line charts).
  * @constructor GraphDrawAdapter
  * Constructor for GraphDrawAdapter class. Adapter sets up property binders between
  * WDO and corresponding GraphDrawControl so that control can respond to new data from WDO.
  * @param GraphDrawControl graphDrawCtrl
  *         Graph Draw Control object being passed to the adapter.
 **/
function GraphDrawAdapter(graphDrawCtrl)
{
    //---------------------------------------------------
    // Variable Declarations
    // parentEObject --> this adapter responds/processes the changes in this object; E.g: portfolio
    // propertyName  --> the name of eRefernece in the object; E.g: position
    // dataAttributeNames --> the name of attribute value which is displayed in the graph; E.g: total (for total portfolio value)
    // labelAttributeName--> the label to be associated with the graphed values; E.g: symbol (stock)
    //---------------------------------------------------

    this.graphDrawControl = graphDrawCtrl;
    this.propertyName= "";
    this.parentEObject = null;
    this.dataAttributeNames = new Array();
    this.labelAttributeName = "";

    this.labelFormat = "";
 	this.dataFormat="";
	
    this.labelConverter="";
 	this.dataConverter="";

    this.yAxisDivisions = "";

    this.groupOperations = new Array();
    this.grouped = false;
	this.seriesSelectorsData = new Array();

 	this.invalidDataAttributeName="";
	this.invalidDatatype="";
	this.nullDataValue="";
	this.noDataSeriesAttributes = false;

 	this.invalidLabelAttributeName = false;
    this.emptyLabelAttributeName = false;
	this.emptyDataAttributeName="";
 
	var check=false;

    var nPointData = new Array();
	var onlyLabelData = new Array();
    var finalData = new Array();

     // Point the adapter in the graphdraw control to this adapter
    this.graphDrawControl.Adapter = this;

    /**
      * @method public bind
      * Method calls activateDataSet in turn, which does most of the work of setting up property binders.
     **/
    this.bind = function()
    {
	    //If No Data Series are specified, Log and alert the msg
   		if (this.dataAttributeNames.length == 0) {
 			this.noDataSeriesAttributes = true;
			return;
		}

  		var eObject = this.parentEObject;
		if (eObject == null) {return;}

		//Test for label attribute name
   		if (this.labelAttributeName == "null" || this.labelAttributeName == null || this.labelAttributeName == 'undefined'){
			this.invalidLabelAttributeName = true;
			return;
		}

		try
		{
			var EObjectsArray = eObject.eGet(this.propertyName);
			if (EObjectsArray.length > 0){
				var obj = EObjectsArray[0];
				obj.eGet(this.labelAttributeName);
			}
		}
		catch(exception) {
			this.invalidLabelAttributeName = true;
			return;
		}


		//Test for data attribute name
		var EObjectsArray = eObject.eGet(this.propertyName);
		if (EObjectsArray.length > 0){
			var obj = EObjectsArray[0];
			for(var i=0;i<this.dataAttributeNames.length;i++){
 					if (this.dataAttributeNames[i] == "null" || this.dataAttributeNames[i] == null || this.dataAttributeNames[i] == 'undefined'){
						this.invalidDataAttributeName="true"+i;
						return;
					}
					try
					{
						obj.eGet(this.dataAttributeNames[i]);
					}
					catch(exception) {
						this.invalidDataAttributeName="true"+i;
						return;
					}
			}
		}
 
        this.activateDataSet(this.parentEObject);
    }

    /**
      * @method public activateDataSet
      * Method activateDataSet() sets up the property binders to the objects bound to graph control
      * In the case of the data grid elements being bound to the graph draw, each cell has to
      * be bound to the graph draw, because a change in any cell should prompt the graphdraw to redraw.
      * The method also calls refreshDataSet to load up new data from the object into the control.
      * @param EObject eObject
      *         eObject that contains the data to be fed into the control.

     **/
    this.activateDataSet = function(eObject)
    {
        if (null == eObject)
        {
            return;
        }
        var EObjectsArray = eObject.eGet(this.propertyName);

		// Array of property binders; setValue() is the callback function that is set up
        // to process the changes in the cells of the datagrid, say.
        for (var j=0; j< this.dataAttributeNames.length; j++){                 
			if(this.dataAttributeNames[j] != "null") {		         
				for (var i=0; i< EObjectsArray.length; i++){
					var pb = new PropertyBinder(EObjectsArray[i],this.dataAttributeNames[j],this.graphDrawControl,null,null,null);
					pb.ControlSetFunction = "setValue";				 
					pb.DataBind();				 						
				}
			}	
		}
	
        this.refreshDataSet(eObject);
    }


    /**
      * @method public refreshDataSet
      * Method refreshDataSet() takes in an eObject and retrieves the latest data from the object
      * to populate the pointData data structure in the GraphDrawControl.
      * @param EObject eObj
      *         eObject that contains the data to be fed into the control.
     **/
    this.refreshDataSet = function(eObj)
    {
        // Cycle through the eObj, get the eRefs, and
        // access the eAttr, in each eReference. The attribute
        // contains the data to be rendered in the flash chart.
        // Example: Want to display file sizes in a directory tree in a pie chart.
        // eObject --> selected_object in the tree, say.
        // eRef    --> childrenfiles (reference to array of children files)
        // eAttr   --> size (attribute in file class, which holds the data for pie chart)
        // eAttrLabel--> name (attribute in file class)

        var refArray = new Array();
        var newPointData = new Array();
		var SinglePointData = new Array();
		var plottedPointData = new Array();
        var newLabelData = new Array();

 

        var tempPointNLData="";
		var tempSinglePointNLData ="";
        var tempLabelNLData="";
        var pointNLData = "";
        this.dataSeparator = "`";
        this.seriesSeparator="*";
		var seriesPointdata ="";


 
        // if eObject is null (when called from setValue--say this function is called
        // the first time a page is loaded), pick up the parentEObject in the adapter.
        // If it is not null (when called from say a callback function in the page, which
        // calls the activateDataSet() directly with eObject as a parameter--providing the
        // changed eObject), get the latest eObject and process it for new data.

        if(eObj != null) //activate data set is called
        {
            this.parentEObject = eObj;
            refArray = eObj.eGet(this.propertyName);
        }
        else //eobj is null, first time page loads
        { 
            refArray = this.parentEObject.eGet(this.propertyName);
        }
 
 
        //PARSING OF LABEL AND DATA INFORMATION----------------------------------------------------
		//Validation for invalid/no label attribute name.
		if (this.labelAttributeName == null || this.labelAttributeName == 'undefined' || this.labelAttributeName == "null"){
 			this.graphDrawControl.invalidLabelAttributeName = true;
 			return;
		}

        if(refArray != null)
        {       
            //PARSING OF LABEL AND DATA POINT INFORMATION-----------------------------------
            for(var i = 0; i < refArray.length; i++)
            {
                var tempNewPointData = new Array(); 
                var tempNewLabelData = new Array(); 

                for(var j = 0; j < this.dataAttributeNames.length; ++j) { //parse the data points for the total number of attributes

                    tempNewPointData[j] = refArray[i].eGet(this.dataAttributeNames[j]);                     
                    tempNewLabelData[j] = refArray[i].eGet(this.labelAttributeName);

					
					//check if the data values has any null values
					if(tempNewPointData[j]==null){
 						this.nullDataValue = "true"+j;
						return;
					}

					//check if the dataseries selected is only number/datetime
 					if (isNaN(tempNewPointData[j]) || (typeof(tempNewPointData[j])=="string")){
						this.invalidDatatype = "true"+j;
 						return;
 					}

                    if(i>0)
                    {
                        newLabelData[j] = newLabelData[j] + this.dataSeparator + tempNewLabelData[j];
                        newPointData[j] = newPointData[j] + this.dataSeparator + tempNewPointData[j];                           
                    }
                    else
                    {
                        newPointData[j] = tempNewPointData[j];
                        newLabelData[j] = tempNewLabelData[j];
                    }
                }
            }
        }//end of refArray


		//validate if split y-axis is enabled if and only if two series is present
		if (newPointData.length < 2 || newPointData.length >2 )  {
			   if (this.graphDrawControl.splitYAxis == true) {
				this.graphDrawControl.splitYAxis = false;
			   }
		}

        //Build Accessibility String
		//fix for Accessibility - LabelData being passed to this function in v6.
		this.accessibility = function(AccPointData,LabelData)
		{
				var accessText = "";
  				for(var series = 0; series < AccPointData.length;  series++) {
 					try {
					accessText += chartdraw_tooltipSeries + eval(series+1) + " "; 
					accessText += this.dataAttributeNames[series] + " "; 
					}
					catch(e){
						alert(e.description);
					}

 					var st = new Array();

					try{
 						var muldata = AccPointData[series];
 						var acc_data = muldata.toString().indexOf(this.dataSeparator);
 

						if (acc_data == -1){
								st = muldata;
						}
						else{
 							st  = muldata.split(this.dataSeparator);
  						}
 
						var mullabel = LabelData.toString().indexOf(this.dataSeparator);
 						var s1 = "";

						if (mullabel == -1){
 							var sl  = LabelData;
 							accessText += sl + " ";                  
							accessText += st + " ";
						} 
 
						else {
 							if (this.grouped == false){
								sl  = LabelData[0].toString().split(this.dataSeparator);
							}
							else {
	   							sl  = LabelData.toString().split(this.dataSeparator);
							}
							for(var point = 0 ; point < sl.length;  point++){
									accessText += sl[point] + " ";                  
									accessText += st[point] + " ";
								}
						}
 					}//end try
					catch (e){
						alert(e.description);
					}
			}//end for
  				this.accessibleTextContainer.innerHTML = accessText;
    		}//end for accessibility string
 


		//for finding the NL data for single series 
		for (var i=0;i<newPointData.length;i++ ){
			SinglePointData[i]=newPointData[i];
			plottedPointData[i]=newPointData[i];
		}

  
		//If group operation is false
        if (this.grouped == false) {
  			this.accessibility(newPointData,newLabelData);
 			var tonormalize="";
			var twoyaxis_normalize = "";
			var series_normalize = "";

		    //GET THE FORMAT INFO AND PARAMS FOR THE SERIES
			var dFormat = this.dataFormat; //format contains "STRING" or "NUMBER"
			//invoke calculate_yaxis and find the y-axis scale.
			//Also find if the yaxis needs to be normalized. If so, then find the median also.

				if ((this.graphDrawControl.splitYAxis == true)){
					twoyaxis_normalize =  this.calculate_split_yaxis(newPointData,dFormat);
				}
				else{
 					tonormalize =  this.calculate_yaxis(newPointData,dFormat);
  				}


 
             //FORMATTING DATA FOR EACH SERIES  
	 		for(var series = 0; series < newPointData.length;  series++){
 				var series_normalize = "";
				var series_normalize = this.single_series_median(newPointData[series],dFormat,series);
   
				if ((this.graphDrawControl.splitYAxis == true)){
					tonormalize =  twoyaxis_normalize[series];
 				}

				//append the series seperator if more than one series is present
                if (series>0) {
                    tempPointNLData = tempPointNLData + this.seriesSeparator;
					tempSinglePointNLData = tempSinglePointNLData + this.seriesSeparator;
					seriesPointdata = seriesPointdata + this.seriesSeparator;
  		         }
	        if (dFormat == "STRING") {
			//if only one dataseries is specified and only one row of data is present
			if (newPointData[series].length == "null" || newPointData[series].length == null || newPointData[series].length == 'undefined'){
 				//If normalization is required, check if the dataseries is of instance numberConverter or the data formatting is string.
				if ((typeof(tonormalize) != "undefined") && tonormalize != "" && (dFormat == "NUMBER" || dFormat == "STRING")){
					//divide this datapoint by the median value
					newPointData[series] = parseFloat(newPointData[series]/tonormalize);
 				}
 				tempPointNLData += newPointData[series];
 				seriesPointdata += plottedPointData[series];

				//Single Series - If normalization is required, check if the dataseries is of instance numberConverter or the data formatting is string.
				if ((typeof(series_normalize) != "undefined") && series_normalize != "" && (dFormat == "NUMBER" || dFormat == "STRING")){
					//divide this datapoint by the median value
 					SinglePointData[series] = parseFloat(SinglePointData[series]/series_normalize);
				}
 				tempSinglePointNLData += SinglePointData[series];

 			}
			else{
				var st = new Array();
				var st_single = new Array();
				var st_plotted = new Array();
				st = newPointData[series].split(this.dataSeparator);
				st_single = SinglePointData[series].split(this.dataSeparator);
				st_plotted = plottedPointData[series].split(this.dataSeparator);
 				for(var point = 0 ; point < st.length;  point++){
					if ((typeof(tonormalize) != "undefined") && tonormalize != ""  && (dFormat == "NUMBER" || dFormat == "STRING")){
					st[point] = parseFloat(st[point]/tonormalize);
 				}
					if ((typeof(series_normalize) != "undefined") && series_normalize != ""  && (dFormat == "NUMBER" || dFormat == "STRING")){
 					st_single[point] = parseFloat(st_single[point]/series_normalize);
				}
 
				if (point==0) {
					tempPointNLData = tempPointNLData + st[point];
					tempSinglePointNLData = tempSinglePointNLData + st_single[point];
					seriesPointdata = seriesPointdata + st_plotted[point];
 					}else {
					tempPointNLData = tempPointNLData + this.dataSeparator + st[point];
					tempSinglePointNLData = tempSinglePointNLData + this.dataSeparator + st_single[point];
					seriesPointdata = seriesPointdata + this.dataSeparator + st_plotted[point];
	 				}
				}//end for
  			}//end else

 		}//end format - String 
        else {//if data format is either number/datetime
			var dataSeriesConverter = this.dataConverter;
			var convertedValue="";
			var convertedValue_single="";
			//if only one dataseries is specified and only one row of data is present
 			if (newPointData[series].length == "null" || newPointData[series].length == null || newPointData[series].length == 'undefined'){
				if ((typeof(tonormalize) != "undefined") && tonormalize != "" && (dFormat == "NUMBER" || dFormat == "STRING")){
					newPointData[series] = parseFloat(newPointData[series]/tonormalize);
 				}
				if ((typeof(series_normalize) != "undefined") && series_normalize != "" && (dFormat == "NUMBER" || dFormat == "STRING")){
 					SinglePointData[series] = parseFloat(SinglePointData[series]/series_normalize);
				}

				try{
	 				convertedValue = dataSeriesConverter.valueToString(modelValueToObject(dataSeriesConverter, newPointData[series]));
					convertedValue_single = dataSeriesConverter.valueToString(modelValueToObject(dataSeriesConverter, SinglePointData[series]));
				}catch(e){
	 				this.graphDrawControl.data_converter_exception = e.description;
				}
				tempPointNLData += convertedValue;
				tempSinglePointNLData += convertedValue_single;
				seriesPointdata += plottedPointData[series];
  	 		}
			else{
				var st = new Array();
				var st_single = new Array();
				st = newPointData[series].split(this.dataSeparator);
				st_single = SinglePointData[series].split(this.dataSeparator);
				st_plotted = plottedPointData[series].split(this.dataSeparator);
                for(var point = 0 ; point < st.length;  point++){
					var nextPoint = parseFloat(st[point]);
					var nextPoint_single = parseFloat(st_single[point]);
					var nextPoint_plotted = parseFloat(st_plotted[point]);
  					if ((typeof(tonormalize) != "undefined") && tonormalize != "" && ( dFormat == "NUMBER" || dFormat == "STRING")){
 						var nextPoint = parseFloat(nextPoint/tonormalize);
					}
  					if ((typeof(series_normalize) != "undefined") && series_normalize != "" && ( dFormat == "NUMBER" || dFormat == "STRING")){
 						var nextPoint_single = parseFloat(nextPoint_single/series_normalize);
					}

					try{
                        convertedValue = dataSeriesConverter.valueToString(modelValueToObject(dataSeriesConverter, nextPoint));
						convertedValue_single = dataSeriesConverter.valueToString(modelValueToObject(dataSeriesConverter, nextPoint_single));
 
					}catch (e) {
							this.graphDrawControl.data_converter_exception = e.description;
 					}

 
					//if the JS converter is returning empty, assign the original value 
                    if (convertedValue == "") convertedValue = st[point];
					if (convertedValue_single == "") convertedValue_single = st_single[point];
                    if (point==0) {
                            tempPointNLData = tempPointNLData + convertedValue;
							tempSinglePointNLData = tempSinglePointNLData +  convertedValue_single;
							seriesPointdata = seriesPointdata +  nextPoint_plotted;
                         } else {
                            tempPointNLData = tempPointNLData + this.dataSeparator + convertedValue;
							tempSinglePointNLData = tempSinglePointNLData + this.dataSeparator +  convertedValue_single;
							seriesPointdata = seriesPointdata + this.dataSeparator +  nextPoint_plotted;
                         }
		            }//end for
	            }//end else  
 			}//end format - number/datetime
          }//end for the series




		var total_single_data = "";
		for (var i=0;i<this.seriesSelectorsData.length;i++ ){
               if (i > 0) {
                    total_single_data = total_single_data + this.seriesSeparator;
			   }
			    total_single_data += this.seriesSelectorsData[i];
		}

 		this.graphDrawControl.seriesSelectorsNLData = total_single_data; 
 

		//FORMATTING LABELS ---------------------------------------------------------------
		if (this.labelFormat=="STRING") 
		{
			for( var i = 0 ; i < newPointData.length; i++)
                {
                    var dtseries = newLabelData[i];
	 				if (dtseries.length == "null" || dtseries.length == null || dtseries.length == 'undefined'){//fix for x-axis format of one data point point in V6
						tempLabelNLData +=dtseries;
 					}

					else{
	                    var dt = new Array();
		                dt =  dtseries.split(this.dataSeparator);
	                    for( var j = 0 ; j < dt.length;  j++){
	                        if (j==0){
	                            tempLabelNLData = dt[j];
		                    } 
	                        else{
		                        tempLabelNLData = tempLabelNLData + this.dataSeparator + dt[j];
				                }
							}//end for
	 	                }//end else
				}//end for newPointData
		}//end for label format string
        else {//if label format is either number/datetime/mask
			for( var i = 0 ; i < newPointData.length; i++){
				var dtseries = newLabelData[i];
 				if (dtseries.length == "null" || dtseries.length == null || dtseries.length == 'undefined'){//fix for x-axis format of one data point point in V6
						var conv="";
						try	{
							conv = this.labelConverter.valueToString(modelValueToObject(this.labelConverter,parseFloat(dtseries)));
						} catch (e) {
							this.graphDrawControl.label_converter_exception = e.description;
 						}
						tempLabelNLData +=conv;
				}
				else{
						var dt = new Array();                    
	                    dt =  dtseries.split(this.dataSeparator);
                        for( var j = 0 ; j < dt.length;  j++){
 						var conv="";
						var checkNaN="";
						var nextLabelValue ="";
						try	{
							nextLabelValue =  parseFloat(dt[j]);
							conv = this.labelConverter.valueToString(modelValueToObject(this.labelConverter,nextLabelValue));
							checkNaN = conv.indexOf("NaN");
						} catch (e) {
							this.graphDrawControl.label_converter_exception = e.description;
 						}
			
						//if unable to convert, assign the original value itself
 						if (conv == "" || conv == "undefined" || conv == "null"  || conv == null || typeof(conv)=="undefined" || checkNaN != -1) {
							conv = dt[j];
						}

                        if (j==0) {
		                        tempLabelNLData = conv;
						        } else {
								tempLabelNLData = tempLabelNLData + this.dataSeparator + conv;
		                    }
	                    }//end for
	                }//end else
				}//end for newPointData
            }//end label number/datetime/mask format

             
            //SPLIT FORMATTED LABELS AND DATA INTO INDIVIDUAL SERIES ---------------------
            //Break the series with comma seperated values


			var tempseriesData = new Array();
			if (seriesPointdata == "null" || seriesPointdata == null || seriesPointdata == 'undefined'){
				tempseriesData = spointdata;
			}
			else{
				tempseriesData = seriesPointdata.split(this.seriesSeparator);
			}


            var seriesData = tempseriesData[0];
              for(var i = 1; i < tempseriesData.length;  i++) {
                seriesData += this.seriesSeparator + tempseriesData[i];
            }

 
        } //end of grouped = false

        else {
			//No grouping operation is specified, Log and alert the msg
			if (!check)	{
				for (var gt=0;gt<this.groupOperations.length;gt++){
					if (this.groupOperations[gt]=="NONE") this.graphDrawControl.noGroupOpSepcified +=gt.toString();
					check = true;
  				}						
			}
            //check if the group operations specified are valid
            for (var gt=0;gt<this.groupOperations.length;gt++){
                if ((this.groupOperations[gt] != "SUM") && (this.groupOperations[gt] != "AVG") && (this.groupOperations[gt] != "FIRST") && (this.groupOperations[gt] != "LAST") && (this.groupOperations[gt] != "COUNT") &&  (this.groupOperations[gt] != "MIN") && (this.groupOperations[gt] != "MAX")) {
                      this.graphDrawControl.invalidgroupOperation = "true" + gt+ this.groupOperations[gt];
 					  return;
                }
            }
			//nLabelData and tLabelData contain the x-axis labels
            var nLabelData = new Array();
            var tLabelData = new Array();
			//valPointData consists of datapoints to be plotted
            var valPointData = new Array();

            //CALLING THE GROUPING FUNCTION
            for(var series = 0 ; series < newPointData.length; series++){
                var dt = new Array();
                var dtseries = newLabelData[series]; 

				//fix for x-axis format of one data point point in V6
				if (dtseries.length == "null" || dtseries.length  == null || dtseries.length  == 'undefined'){
					nLabelData = dtseries; 
				}
				else{
	                 nLabelData = dtseries.split(this.dataSeparator); 
 				     for (var j=0;j<newPointData.length;j++){
			            var spointdata = newPointData[j];
						//if only one dataseries is specified and only one row of data is present
						if (spointdata.length == "null" || spointdata.length  == null || spointdata.length  == 'undefined'){
 							nPointData[j] = spointdata;
							}else{
								nPointData[j] = spointdata.split(this.dataSeparator);
							}
		            }//end for
				}//end else

				//fix for x-axis format of one data point point in V6
				if (nLabelData.length == "null" || nLabelData.length  == null || nLabelData.length  == 'undefined'){
						tLabelData[0]=nLabelData;	
				}
				else{
					//store duplicate labelData in an Array
					for (var k=0;k<nLabelData.length;k++){
						tLabelData[k]=nLabelData[k];
					}
				}

				 //call the grouping function for each series
                 this.getGrouping(nPointData,tLabelData,nLabelData,this.groupOperations);
               }//end for all series

			var label_data_accessibility = "";

             //FORMATTING LABELS
            if (this.labelFormat=="STRING") {
				//onlyLabelData contains the labels after calculating group operations of all the series
                 for( var j = 0 ; j < onlyLabelData.length;  j++){
                    if(onlyLabelData[j] !=null) {
                        if (j==0) {
                            tempLabelNLData = onlyLabelData[j];
							label_data_accessibility = onlyLabelData[j];
                        } else {
                            tempLabelNLData = tempLabelNLData + this.dataSeparator + onlyLabelData[j];
							label_data_accessibility = label_data_accessibility + this.dataSeparator + onlyLabelData[j];
                        }
                    }
                }
            } 
            else {
                for( var j = 0 ; j < onlyLabelData.length;  j++){
		          if(onlyLabelData[j] !=null) {//to eliminate undefined
					var conv = "";
					var checkNaN = "";
					var nextLabelValue ="";

					try{
						nextLabelValue = parseFloat(onlyLabelData[j]);
	 					conv = this.labelConverter.valueToString(modelValueToObject(this.labelConverter,nextLabelValue));
						checkNaN = conv.indexOf("NaN");
 					}catch(e){
							this.graphDrawControl.label_converter_exception = e.description;
					}

					//if unable to convert, assign the original value itself
	 				if (conv == "" || conv == "undefined" || conv == "null"  || conv == null  || typeof(conv)=="undefined"  || checkNaN != -1) {
 							 conv = onlyLabelData[j];
							 nextLabelValue = onlyLabelData[j];
					}
   	 
                    if (j==0) {
                        tempLabelNLData = conv;
						label_data_accessibility = nextLabelValue;
                    } else {
                        tempLabelNLData = tempLabelNLData + this.dataSeparator + conv;
						label_data_accessibility = label_data_accessibility + this.dataSeparator + conv;
                    }
				}//end check for null
             }//end for
            }//end else

          
            //FORMATTING DATA FOR EACH SERIES 
            var ndConverter;
            for(var series = 0; series < newPointData.length;  series++){
                
                //GET THE FORMAT INFO AND PARAMS FOR THE SERIES
 				var dFormat = this.dataFormat; 
				
                //CONVERT THE GROUPED DATA INTO A `-SEPARATED ARRAY
                var fPointData = new Array();
                var fP = new Array();
                var temp="";
                var st="";

                 for(r=0;r<finalData.length;r++){    //make the , seperated array as ` seperated
                    if (finalData[r] != null){
                        fP = finalData[r];
				 		if (fP.length == "null" || fP.length  == null || fP.length  == 'undefined'){
                            st=fP;
							fPointData[r]=fP;
						}
						else{
	                        for (j=0;j<fP.length;j++){
		                         if (fP[j]!=null){
			                        if (j==0){
				                        st=fP[j];
					                }
						            else st = st +"`"+fP[j];
							    }//if
	                        }//for
	                        fPointData[r] = st;
		                    st="";
			                }//end else
		             }//end if
				}//for


			//copy the contents of fPointData to SinglePointData - For sending NL data if single series is selected
			 for (var i=0;i<fPointData.length;i++ ){
				SinglePointData[i]=fPointData[i];
				plottedPointData[i]=fPointData[i];
			}

	            if (series>0) {
		            tempPointNLData = tempPointNLData + this.seriesSeparator;
					tempSinglePointNLData = tempSinglePointNLData + this.seriesSeparator;
					seriesPointdata = seriesPointdata + this.seriesSeparator;
                }



				var tonormalize="";
				var twoyaxis_normalize = "";
				//Invoke calculate_yaxis and find the y-axis scale.
				//Also find if the yaxis needs to be normalized. If so, then find the median also.

   				if ((this.graphDrawControl.splitYAxis == true)){
					twoyaxis_normalize =  this.calculate_split_yaxis(fPointData,dFormat);
					tonormalize =  twoyaxis_normalize[series];
 				}
				else{
 					tonormalize =  this.calculate_yaxis(fPointData,dFormat);
   				}

				var series_normalize = this.single_series_median(fPointData[series],dFormat,series);

 

                if (dFormat == "STRING") {
				if (fPointData[series].length == "null" || fPointData[series].length == null || fPointData[series].length == 'undefined'){
 					if ((typeof(tonormalize) != "undefined") && (tonormalize) != "" &&  (dFormat == "NUMBER" || dFormat == "STRING")){
 						fPointData[series] = parseFloat(fPointData[series]/tonormalize);
					}
					tempPointNLData +=fPointData[series];
					seriesPointdata += plottedPointData[series];

					if ((typeof(series_normalize) != "undefined") && (series_normalize) != "" &&  (dFormat == "NUMBER" || dFormat == "STRING")){
							SinglePointData[series] = parseFloat(SinglePointData[series]/series_normalize);
					}
					tempSinglePointNLData +=SinglePointData[series];
 
 				}else{
 	                    var st = new Array();
						var st_single = new Array();
						var st_plotted = new Array();
		                st = fPointData[series].split(this.dataSeparator);
						st_single = SinglePointData[series].split(this.dataSeparator);
						st_plotted = plottedPointData[series].split(this.dataSeparator);
						for(var point = 0 ; point < st.length;  point++) {
 							//if yaxis normalization and is a NumberConverter, divide each datapoint by the median value
 							if ((typeof(tonormalize) != "undefined") && tonormalize != "" && (dFormat == "NUMBER" || dFormat == "STRING")){
									st[point] = parseFloat(st[point]/tonormalize);
							}
							if ((typeof(series_normalize) != "undefined") && series_normalize != "" && (dFormat == "NUMBER" || dFormat == "STRING")){
								st_single[point] = parseFloat(st_single[point]/series_normalize);
							}

							if (point==0) {
								tempPointNLData = tempPointNLData + st[point];
								tempSinglePointNLData = tempSinglePointNLData + st_single[point];
								seriesPointdata = seriesPointdata + st_plotted[point];
							} else {
								tempPointNLData = tempPointNLData + this.dataSeparator + st[point];
								tempSinglePointNLData = tempSinglePointNLData + this.dataSeparator + st_single[point];
								seriesPointdata = seriesPointdata +this.dataSeparator + st_plotted[point];
							}
						}//end for
					}//end else
                }//end for String format
                else {//if the formating is datetime/number
					var convertedValue="";
					var convertedValue_single="";
		            var dataSeriesConverter = this.dataConverter;
					//if only one dataseries is specified and only one row of data is present
					if (fPointData[series].length == "null" || fPointData[series].length == null || fPointData[series].length == 'undefined'){
   						if ((typeof(tonormalize) != "undefined") && tonormalize != "" && (dFormat == "NUMBER" || dFormat == "STRING")){
 							fPointData[series] = parseFloat(fPointData[series]/tonormalize);
						}

   						if ((typeof(series_normalize) != "undefined") && series_normalize != "" && (dFormat == "NUMBER" || dFormat == "STRING")){
 							SinglePointData[series] = parseFloat(SinglePointData[series]/series_normalize);
						}


						try{
			                convertedValue = dataSeriesConverter.valueToString(modelValueToObject(dataSeriesConverter, parseFloat(fPointData[series])));
							convertedValue_single = dataSeriesConverter.valueToString(modelValueToObject(dataSeriesConverter, parseFloat(SinglePointData[series])));
			 			}catch(e){
 			 				this.graphDrawControl.data_converter_exception = e.description;
						}
						tempPointNLData +=convertedValue;
						tempSinglePointNLData +=convertedValue_single;
						seriesPointdata += plottedPointData[series];
 					}else{
	                var st = new Array();
					var st_single = new Array();
					var st_plotted = new Array();
                    st = fPointData[series].split(this.dataSeparator);
					st_single = SinglePointData[series].split(this.dataSeparator);
					st_plotted = plottedPointData[series].split(this.dataSeparator);
                    for(var point = 0 ; point < st.length;  point++) {
						var nextPoint = parseFloat(st[point]);
						var nextPoint_single = parseFloat(st_single[point]);
						var nextPoint_plotted = parseFloat(st_plotted[point]);
						//if yaxis normalization and is a NumberConverter, divide each datapoint by the median value
						if ((typeof(tonormalize) != "undefined") && tonormalize != "" && (dFormat == "NUMBER" || dFormat == "STRING")){
							var nextPoint = parseFloat(nextPoint/tonormalize);
						}

						if ((typeof(series_normalize) != "undefined") && series_normalize != "" && (dFormat == "NUMBER" || dFormat == "STRING")){
 							var nextPoint_single = parseFloat(nextPoint_single/series_normalize);
						}


 						try{
                        convertedValue = dataSeriesConverter.valueToString(modelValueToObject(dataSeriesConverter, nextPoint));
						convertedValue_single = dataSeriesConverter.valueToString(modelValueToObject(dataSeriesConverter, nextPoint_single));
						}catch(e){
							this.graphDrawControl.data_converter_exception = e.description;
						}
						
 						if (convertedValue == "") convertedValue = st[point];
						if (convertedValue_single == "") convertedValue_single = st_single[point];
 						if (point==0) {
                            tempPointNLData = tempPointNLData + convertedValue;
							tempSinglePointNLData = tempSinglePointNLData + convertedValue_single;
							seriesPointdata = seriesPointdata +  nextPoint_plotted; 
                        } else {
                            tempPointNLData = tempPointNLData + this.dataSeparator + convertedValue;
							tempSinglePointNLData = tempSinglePointNLData + this.dataSeparator + convertedValue_single;
							seriesPointdata = seriesPointdata + this.dataSeparator + nextPoint_plotted;
                        }
                    }//end for
				}//end else
              }//end for number/datetime formating
            }//end for series

 
 
			var total_single_data = "";
			for (var i=0;i<this.seriesSelectorsData.length;i++ ){
						   if (i > 0) {
								total_single_data = total_single_data + this.seriesSeparator;
						   }
							total_single_data += this.seriesSelectorsData[i];
			}
				this.graphDrawControl.seriesSelectorsNLData = total_single_data; 



			//SPLIT FORMATTED LABELS AND DATA INTO INDIVIDUAL SERIES ---------------------
            //Break the series with comma seperated values


			var tempseriesData = new Array();
			if (seriesPointdata == "null" || seriesPointdata == null || seriesPointdata == 'undefined'){
				tempseriesData = seriesPointdata;
			}
			else{
				tempseriesData = seriesPointdata.split(this.seriesSeparator);
			}

			var seriesData = tempseriesData[0];
			  for(var i = 1; i < tempseriesData.length;  i++) {
				seriesData += this.seriesSeparator + tempseriesData[i];
			}

			//call the accessibility for grouping - true
 			this.accessibility(tempseriesData,label_data_accessibility);


        } // end of grouped = true

		//If grouped is false the final data is in newPointData.  If grouped is true the final data is in fPointData.

		var ydiv = this.yAxisDivisions-1;
        this.graphDrawControl.yAxisDivisions = ydiv;
        this.graphDrawControl.pointData =  seriesData;
        this.graphDrawControl.pointNLData = tempPointNLData;
		this.graphDrawControl.singlePointNLData = tempSinglePointNLData;
        this.graphDrawControl.labelNLData = tempLabelNLData;


 

        // Load up the appropriate number of colors from the color label array in GraphDrawControl
        // loadColorData() takes the current point set in control and associates colors
        this.graphDrawControl.loadColorData();
	}//end refresh data

        //Y AXIS LABELS ---------------------------------------------------------------
        //Formatting to calculate the Y-axis scale


	this.calculate_split_yaxis = function(yaxisPointData,dFormat)
	{

		var tempinp = yaxisPointData[0];
		for( var i = 1 ; i < yaxisPointData.length;  i++) 
		tempinp += this.seriesSeparator + yaxisPointData[i];
 
        var v_max = 0;
        var v_min = 0;

        var tempArray = new Array();
        var tempArrayNL = new Array();
    
 		if (tempinp.length == "null" || tempinp.length  == null || tempinp.length  == 'undefined'){
			tempArray = tempinp;
 		}
		else{
	        tempArray = tempinp.split(this.seriesSeparator);
 		}

		if (tempArray.length == "null" || tempArray.length  == null || tempArray.length  == 'undefined'){
			var pointArray = tempArray;
		}else{
	         var pointArray = new Array(tempArray.length);
		}

 		if (tempArray.length == "null" || tempArray.length  == null || tempArray.length  == 'undefined'){
			pointArray = tempArray;
			v_min =0;
			v_max=pointArray;
		}else{
	        for (var i in tempArray) {
            pointArray[i] = new Array();
            pointArray[i] = tempArray[i].split(this.dataSeparator);
		    }
    
			for (var i in pointArray)  {
				for (var j in pointArray[i])  {
					pointArray[i][j] = Number(pointArray[i][j]);
					}
				}
			}

 			var finalyaxis = "";
			var norm = new Array();

				for (var p=0;p<pointArray.length;p++) { //for each series find the min and max
				if (p>0){
					finalyaxis +=  this.seriesSeparator;
				}
				for(var j=0; j<pointArray[p].length; j++) {
					if (v_max<pointArray[p][j])
						 v_max=pointArray[p][j];
					if(v_min > pointArray[p][j])
						v_min = pointArray[p][j];
				}

 

				var ydiv = this.yAxisDivisions-1;
				ydiv = ydiv <= 0 ? 5: ydiv;
				var intervals = Math.round(ydiv);

				var v_min2 = 0;
				if (v_min != 0 && ((Math.abs(v_min) > 1))) {
					//calculate a rounded up min
					var digits_min = Math.floor(Math.log(Math.abs(v_min))/Math.log(10));
					var place_min = Math.pow(10, digits_min);
					v_min2 = Math.round(v_min/place_min)*place_min;
						if (v_min2 > v_min) {
							v_min2 = v_min2 - place_min;
							}
					}

 
				if (Math.abs(v_min) <= 1) {
					v_min2 = 0;
				}

				//calculate the step
				var tStep = (v_max - v_min2)/intervals;
				tStep = Math.abs(tStep);
				tStep = tStep == 0 ? 1 : tStep;
				var digits = Math.floor(Math.log(tStep)/Math.log(10));
				var place = Math.pow(10, digits);
				var step = Math.round(tStep/place)*place;
		 
 
				if (Math.abs(v_min) <= 1 && v_min < 0) {
					if (step > 1) {
						v_min2 = -1 * step;
					} else {
					v_min2 = -1;
					}
				}

				//calculate the step
				tStep = (v_max - v_min2)/intervals;
				tStep = Math.abs(tStep);
				tStep = tStep == 0 ? 1 : tStep;
 				digits = Math.floor(Math.log(tStep)/Math.log(10));
				place = Math.pow(10, digits);
				step = Math.round(tStep/place)*place;

				if (Math.abs(v_min) <= 1 && v_min < 0) {
					if (step > 1) {
						v_min2 = -1 * step;
					} else {
						v_min2 = -1;
					}
				}

 
				//If auto option is selected in Tooling
				if (this.yAxisDivisions <= 0){
					while((step*(intervals+1)+v_min2) < v_max) intervals++;
					this.yAxisDivisions = intervals;
				}
				else{
					//round up the step if projected max is less than actual
					var projectedMax = eval(step*(intervals+1)+eval(v_min2));
					if (projectedMax < v_max) {
						var roundUpTo = place/2;
						step = step + roundUpTo;
					}
				}


				if (step < 0.0001) {
					step = 0.0001;
				}


				var doubleyaxis = "";
				var convertedValue = "";
 				var yaxis_normalize = false;

				var tempmedian = new Array();

				for(var i=0;i<intervals+2;i++) {                 
 					var nextAxisValue = Math.round((eval(v_min2) + eval(i)*eval(step))*10000)/10000;
 					if (((Math.abs(parseInt(nextAxisValue))).toString(10).length) >=4)  yaxis_normalize = true;
 					tempmedian[i] = nextAxisValue;

				}

				var axisConverter = this.dataConverter; //one-converter-for-all-data
				var medianvalue = this.find_median(tempmedian);


 
	 			if ((yaxis_normalize) && (((Math.abs(parseInt(medianvalue))).toString(10).length) >=4) && (dFormat == "NUMBER" || dFormat == "STRING")){
	  				for(var i=0;i<Scales.length;i++) {
 	 				if (Math.abs(parseInt(medianvalue))/Scales[i][0] >= 1)
					{
						var tonormalize = Scales[i][0];
						this.graphDrawControl.normalizedTitle[p] = "("+Scales[i][1]+")";
 						break;
					}
				}
 				for(i=0;i<intervals+2;i++) {                 
 					var nextAxisValue = Math.round((eval(v_min2) + eval(i)*eval(step))*10000)/10000;
					if (dFormat == "STRING") {
						var norm_data = nextAxisValue/tonormalize;
	 						convertedValue = norm_data
					} else {
						try{
							var norm_data = nextAxisValue/tonormalize;
 							convertedValue = axisConverter.valueToString(modelValueToObject(axisConverter, norm_data));
						}catch(e){
									this.graphDrawControl.yaxis_converter_exception = e.description;
							}

					}
					if (i==0) {
						doubleyaxis = convertedValue;
					} else {
									doubleyaxis = doubleyaxis + this.dataSeparator + convertedValue;
					}
				}
 			}//end y-axis normalization
			else //Original Data
			{
  				for(i=0;i<intervals+2;i++) {                 
 					var nextAxisValue = Math.round((eval(v_min2) + eval(i)*eval(step))*10000)/10000;
 					if (dFormat == "STRING") {
						convertedValue = nextAxisValue;
					} else {
						try{
							convertedValue = axisConverter.valueToString(modelValueToObject(axisConverter, nextAxisValue));
						}catch(e){
									this.graphDrawControl.yaxis_converter_exception = e.description;
							}

					}
					if (i==0) {
						doubleyaxis = convertedValue;
					} else {
									doubleyaxis = doubleyaxis + this.dataSeparator + convertedValue;
					}
				}
			}//end else 



			finalyaxis +=  doubleyaxis;
			v_max = 0;
			v_min = 0;
			norm[p] = tonormalize;
			tonormalize = "";
		}//end for series

 			this.graphDrawControl.yAxisNLData  = finalyaxis;
			return norm;
  	}//end calculate_split_yaxis



	//In case of single series selection find the median value.
	this.single_series_median = function(yaxisPointData,dFormat,series)
	{
  		var tempinp = yaxisPointData;
        var v_max = 0;
        var v_min = 0;

        var tempArray = new Array();
        var tempArrayNL = new Array();

  		if (tempinp.length == "null" || tempinp.length  == null || tempinp.length  == 'undefined'){
			tempArray = tempinp;
 		}
		else{
 	        tempArray = tempinp.split(this.seriesSeparator);
 		}

		if (tempArray.length == "null" || tempArray.length  == null || tempArray.length  == 'undefined'){
			var pointArray = tempArray;
		}else{
 	         var pointArray = new Array(tempArray.length);
		}

		
  		if (tempArray.length == "null" || tempArray.length  == null || tempArray.length  == 'undefined'){
			pointArray = tempArray;
			v_min =0;
			v_max=pointArray;
		}else{
	        for (var i in tempArray) {
            pointArray[i] = new Array();
            pointArray[i] = tempArray[i].split(this.dataSeparator);
		    }
    
			for (var i in pointArray)  {
				for (var j in pointArray[i])  {
					pointArray[i][j] = Number(pointArray[i][j]);
					}
				}
			}

	 if (pointArray.length == "null" || pointArray.length  == null || pointArray.length  == 'undefined'){
			if (pointArray<0){
				v_min=pointArray;
				v_max = 0;
			}
			else {
				v_min = 0;
				v_max=pointArray;
			}
		}
		else {
				for(var j=0; j<pointArray[0].length; j++) {
						if (v_max<pointArray[0][j])
							 v_max=pointArray[0][j];
						if(v_min > pointArray[0][j])
							v_min = pointArray[0][j];
					}

			}


				var ydiv = this.yAxisDivisions-1;
				ydiv = ydiv <= 0 ? 5: ydiv;
				var intervals = Math.round(ydiv);

				var v_min2 = 0;
				if (v_min != 0 && ((Math.abs(v_min) > 1))) {
					//calculate a rounded up min
					var digits_min = Math.floor(Math.log(Math.abs(v_min))/Math.log(10));
					var place_min = Math.pow(10, digits_min);
					v_min2 = Math.round(v_min/place_min)*place_min;
						if (v_min2 > v_min) {
							v_min2 = v_min2 - place_min;
							}
					}

				if (Math.abs(v_min) <= 1) {
					v_min2 = 0;
				}

				//calculate the step
				var tStep = (v_max - v_min2)/intervals;
				tStep = Math.abs(tStep);
				tStep = tStep == 0 ? 1 : tStep;
				var digits = Math.floor(Math.log(tStep)/Math.log(10));
				var place = Math.pow(10, digits);
				var step = Math.round(tStep/place)*place;
		 
				if (Math.abs(v_min) <= 1 && v_min < 0) {
					if (step > 1) {
						v_min2 = -1 * step;
					} else {
						v_min2 = -1;
					}
				}

				//calculate the step
				tStep = (v_max - v_min2)/intervals;
				tStep = Math.abs(tStep);
				tStep = tStep == 0 ? 1 : tStep;
 				digits = Math.floor(Math.log(tStep)/Math.log(10));
				place = Math.pow(10, digits);
				step = Math.round(tStep/place)*place;


 				if (Math.abs(v_min) <= 1 && v_min < 0) {
					if (step > 1) {
						v_min2 = -1 * step;
					} else {
						v_min2 = -1;
					}
				}


				//If auto option is selected in Tooling
				if (this.yAxisDivisions <= 0){
					while((step*(intervals+1)+v_min2) < v_max) intervals++;
					this.yAxisDivisions = intervals;
				}
				else{
					//round up the step if projected max is less than actual
					var projectedMax = eval(step*(intervals+1)+eval(v_min2));
					if (projectedMax < v_max) {
						var roundUpTo = place/2;
						step = step + roundUpTo;
					}
				}


				if (step < 0.0001) {
					step = 0.0001;
				}

  

			var doubleyaxis = "";
			var convertedValue = "";
			var yaxis_normalize = false;

			var tempmedian = new Array();

			for(var i=0;i<intervals+2;i++) {      
 				var nextAxisValue = Math.round((eval(v_min2) + eval(i)*eval(step))*10000)/10000;
 				if (((Math.abs(parseInt(nextAxisValue))).toString(10).length) >=4)  yaxis_normalize = true;
				tempmedian[i] = nextAxisValue;
			}
			
			var axisConverter = this.dataConverter; //one-converter-for-all-data
 			var medianvalue = this.find_median(tempmedian);


			if ((yaxis_normalize) && (((Math.abs(parseInt(medianvalue))).toString(10).length) >=4) && (dFormat == "NUMBER" || dFormat == "STRING")){
				for(var i=0;i<Scales.length;i++) {
					if (Math.abs(parseInt(medianvalue))/Scales[i][0] >= 1) {
						var tonormalize = Scales[i][0];
						this.graphDrawControl.normalizedTitle[series]= "("+Scales[i][1]+")";
						break;
					}
				}

 				for(i=0;i<intervals+2;i++) {                 
 					var nextAxisValue = Math.round((eval(v_min2) + eval(i)*eval(step))*10000)/10000;
 					if (dFormat == "STRING") {
						var norm_data = nextAxisValue/tonormalize;
	 						convertedValue = norm_data
					} else {
						try{
							var norm_data = nextAxisValue/tonormalize;
 							convertedValue = axisConverter.valueToString(modelValueToObject(axisConverter, norm_data));
						}catch(e){
									this.graphDrawControl.yaxis_converter_exception = e.description;
							}

					}
					if (i==0) {
						doubleyaxis = convertedValue;
					} else {
									doubleyaxis = doubleyaxis + this.dataSeparator + convertedValue;
					}
				}
			}//end if yaxis_normalize

			else //Original Data
			{
   				for(i=0;i<intervals+2;i++) {                 
 					var nextAxisValue = Math.round((eval(v_min2) + eval(i)*eval(step))*10000)/10000;
 					if (dFormat == "STRING") {
 						convertedValue = nextAxisValue;
					} else {
 						try{
							convertedValue = axisConverter.valueToString(modelValueToObject(axisConverter, nextAxisValue));
						}catch(e){
									this.graphDrawControl.yaxis_converter_exception = e.description;
							}

					}
					if (i==0) {
						doubleyaxis = convertedValue;
					} else {
									doubleyaxis = doubleyaxis + this.dataSeparator + convertedValue;
					}
				}
			}//end else 



			//finalyaxis +=  doubleyaxis;
		this.seriesSelectorsData[series] = doubleyaxis
 		return tonormalize;
 	}//end series median

 
 	this.calculate_yaxis = function(yaxisPointData,dFormat)
	{
		var tempinp = yaxisPointData[0];
		for( var i = 1 ; i < yaxisPointData.length;  i++) 
		tempinp += this.seriesSeparator + yaxisPointData[i];
 
        var v_max = 0;
        var v_min = 0;

        var tempArray = new Array();
        var tempArrayNL = new Array();
    
 		if (tempinp.length == "null" || tempinp.length  == null || tempinp.length  == 'undefined'){
			tempArray = tempinp;
 		}
		else{
	        tempArray = tempinp.split(this.seriesSeparator);
 		}

		if (tempArray.length == "null" || tempArray.length  == null || tempArray.length  == 'undefined'){
			var pointArray = tempArray;
		}else{
	         var pointArray = new Array(tempArray.length);
		}

 		if (tempArray.length == "null" || tempArray.length  == null || tempArray.length  == 'undefined'){
			pointArray = tempArray;
			v_min =0;
			v_max=pointArray;
		}else{
	        for (var i in tempArray) {
            pointArray[i] = new Array();
            pointArray[i] = tempArray[i].split(this.dataSeparator);
		    }
    
			for (var i in pointArray)  {
				for (var j in pointArray[i])  {
					pointArray[i][j] = Number(pointArray[i][j]);
					}
				}
			}

			//find the min and max for the total number for series
 			  for (var i=0;i<pointArray.length;i++) {
				for(var j=0; j<pointArray[i].length; j++) {
					if (v_max<pointArray[i][j])
						 v_max=pointArray[i][j];
					if(v_min > pointArray[i][j])
						v_min = pointArray[i][j];
				}
			}

 
				var ydiv = this.yAxisDivisions-1;
 				ydiv = ydiv <= 0 ? 5: ydiv;
				var intervals = Math.round(ydiv);

				var v_min2 = 0;
				if (v_min != 0 && ((Math.abs(v_min) > 1))) {
					//calculate a rounded up min
					var digits_min = Math.floor(Math.log(Math.abs(v_min))/Math.log(10));
					var place_min = Math.pow(10, digits_min);
					v_min2 = Math.round(v_min/place_min)*place_min;
						if (v_min2 > v_min) {
							v_min2 = v_min2 - place_min;
							}
					}

				if (Math.abs(v_min) <= 1) {
					v_min2 = 0;
				}

				//calculate the step
				var tStep = (v_max - v_min2)/intervals;
				tStep = Math.abs(tStep);
				tStep = tStep == 0 ? 1 : tStep;
				var digits = Math.floor(Math.log(tStep)/Math.log(10));
				var place = Math.pow(10, digits);
				var step = Math.round(tStep/place)*place;
		 
				if (Math.abs(v_min) <= 1 && v_min < 0) {
					if (step > 1) {
						v_min2 = -1 * step;
					} else {
					v_min2 = -1;
					}
				}

				//calculate the step
				tStep = (v_max - v_min2)/intervals;
				tStep = Math.abs(tStep);
				tStep = tStep == 0 ? 1 : tStep;
 				digits = Math.floor(Math.log(tStep)/Math.log(10));
				place = Math.pow(10, digits);
				step = Math.round(tStep/place)*place;


 				if (Math.abs(v_min) <= 1 && v_min < 0) {
					if (step > 1) {
						v_min2 = -1 * step;
					} else {
						v_min2 = -1;
					}
				}
 

				//If auto option is selected in Tooling
				if (this.yAxisDivisions <= 0){
					while((step*(intervals+1)+v_min2) < v_max) intervals++;
					this.yAxisDivisions = intervals;
				}
				else{
					//round up the step if projected max is less than actual
					var projectedMax = eval(step*(intervals+1)+eval(v_min2));
					if (projectedMax < v_max) {
						var roundUpTo = place/2;
						step = step + roundUpTo;
					}
				}

			if (step < 0.0001) {
				step = 0.0001;
			}
 		
			var nlyaxis = "";
			var convertedValue = "";
			var axisConverter = this.dataConverter; //one-converter-for-all-data
			var yaxis_normalize = false;

  			var tempmedian = new Array();

 			for(var i=0;i<intervals+2;i++) {                 
 				var nextAxisValue = Math.round((eval(v_min2) + eval(i)*eval(step))*10000)/10000;
				if (((Math.abs(parseInt(nextAxisValue))).toString(10).length) >=4)  yaxis_normalize = true;
  				tempmedian[i] = nextAxisValue;
				
			}

 
  	  		var medianvalue = this.find_median(tempmedian);
			
			
									 
  			if ((yaxis_normalize) && (((Math.abs(parseInt(medianvalue))).toString(10).length) >=4) && (dFormat == "NUMBER" || dFormat == "STRING")) {
 				for(var i=0;i<Scales.length;i++) {
					if (Math.abs(parseInt(medianvalue))/Scales[i][0] >= 1)
 					{
 						var tonormalize = Scales[i][0];
 						this.graphDrawControl.commonNormalizedTitle = "("+Scales[i][1]+")";
  						break;
					}
				}
 
				for(var i=0;i<intervals+2;++i) {                 
 					var nextAxisValue = Math.round((eval(v_min2) + eval(i)*eval(step))*10000)/10000;
 					if (dFormat == "STRING") {
						var norm_data = nextAxisValue/tonormalize;
 						convertedValue = norm_data;
					} else {
  						try{
  							var norm_data = nextAxisValue/tonormalize;
   							convertedValue = axisConverter.valueToString(modelValueToObject(axisConverter, norm_data));
   							}catch(e){
									this.graphDrawControl.yaxis_converter_exception = e.description;
							}
					}
					if (i==0) {
 							nlyaxis = convertedValue;
 					} else {
							nlyaxis = nlyaxis + this.dataSeparator + convertedValue;
 					}
 				}//end for
 			}//end for yaxis normalization
			else{	//Original data (String length is < 4
	 			for(var i=0;i<intervals+2;i++) {                 
 				var nextAxisValue = Math.round((eval(v_min2) + eval(i)*eval(step))*10000)/10000;
   				if (dFormat == "STRING") {
						convertedValue = nextAxisValue;
				} else {
						try{
							convertedValue = axisConverter.valueToString(modelValueToObject(axisConverter, nextAxisValue));
						}catch(e){
									this.graphDrawControl.yaxis_converter_exception = e.description;
							}
					}
					if (i==0) {
  						nlyaxis = convertedValue;
					} else {
						nlyaxis = nlyaxis + this.dataSeparator + convertedValue;
					}
				}//end for
			}//end for else (original values)

 			this.graphDrawControl.yAxisNLData = nlyaxis;
 			return tonormalize;
		}//end calculate_yaxis

	  
		this.find_median = function(tempmedian)
		{
 			var sortmedian = new Array();
			var median = "";
  			sortmedian = tempmedian.sort(compareNumbers);
			var mid = parseInt(sortmedian.length/2);
			var isprime =sortmedian.length%2;

			if (isprime == 1){
				median = sortmedian[mid];
				//In case if median is exactly 0
 				if (median ==0)	{
					var sum= sortmedian[mid-1] + sortmedian[mid];
					median = parseFloat(sum/2);
				}
 			}else{
				var sum= sortmedian[mid-1] + sortmedian[mid];
				median = parseFloat(sum/2);
 		 	}
 			return median;
 		}

 
    //Grouping function which forms the  finalData and finalLabel array based on the group operation.
    this.getGrouping = function(nPointData,tLabelData,nLabelData,groupType)
    {
		//nPointData -->Data points specified for all the series
		//tLabelData -->Label data specified for all the series
		//tLabelData will be manipulatred inside the for loop for each series. Re-assign the values of nLabelData to tLabelData and the end of each series
 		//nLabelData -->Label data specified for all the series
		//groupType --> Group type specified for all the series

        var countData = new Array();
        var averageName = new Array();
        var averageData = new Array();
        var minMaxName = new Array();
        var maxName = new Array();
        var avgDataSeries = new Array();

        var min = 0;
        var minCount = 0;
        var counter = 0;
        var average = 0;
        var iCnt = 0;
        var first;
        var Count = 0;
        var last;
        var lastVal = 0;
        var flag = false;
        var bCheckName = false;
 
		for (var t=0;t<nPointData.length;t++)   
        {
			//Take the Point Data for each series in an Array and store
			valPointData = nPointData[t];  
			//if only one series and only one row is present
			if (valPointData.length == "null" || valPointData.length  == null || valPointData.length  == 'undefined'){
				if(groupType[t] != "COUNT"){
			        finalData[t] = valPointData;
	 				continue;
				}
				else{
					finalData[t]=1;
					continue;
				}
			}
			else
			{
			//re-initialize these values for each series
            var firstData = new Array();
            var lastData = new Array();
            var minArray = new Array();
            var maxArray = new Array();
			var SumDataSeries = new Array();
            first = 0;
            last = 0;
			//Loop through total label datas available for each series
            for(var i=0;i<tLabelData.length;i++) {
                var minMaxData = new Array();
                iCnt = 0;
                var foundrep = false;  //if match is found
				//start the loop from i+1 (Label) and find if i and j matches, If so grouping needs to
				//be performed for this Label
                for(j=(i+1);j<tLabelData.length;j++) {

				 if((typeof(tLabelData[i]) != 'undefined') && (tLabelData[i] == tLabelData[j])){
                        foundrep = true;
                        iCnt++;

                         if(!flag) {//find the sum (i+j) if grouptype is SUM and store in Count
                            Count += parseFloat(valPointData[j]) + parseFloat(valPointData[i]);
                             flag = true;
        
                            //to get first Name & Data
                            if(groupType[t] == "FIRST"){
                                firstData[first] = valPointData[i];
                                first++;
                            }
                            //adding Name for Min and Max Data
                            if(!bCheckName) {
                                minMaxName[min] = tLabelData[i];
                                bCheckName = true;
                                min++;
                            }
        
                            //adding data to find out min and max
                            if(valPointData[j] !=null && valPointData[i] !=null) {
                                minMaxData[minCount] = valPointData[i];
                                minCount++;
                                minMaxData[minCount] = valPointData[j];
                                minCount++;
                            }
        
                        } else {//for SUM, if flag is false then henceforth just add the j data to count.
                            Count += parseFloat(valPointData[j]);
                            if(valPointData[j] !=null || valPointData[j] !='') {
                                minMaxData[minCount] = valPointData[j];
                                minCount++;
                            }
                        }
                        if(groupType[t] == "LAST"){
                            lastVal = valPointData[j];
                          }

 						//delete the parsed j point data, so that when i is incrementetd next
						//time it should not take the current j positions value
                        delete valPointData[j];	
                        delete tLabelData[j];	//delete the parsed j label data
						delete onlyLabelData[j]; //For x-axis labels

                    }//end for matching
                } // end of j loop


		         //if i and j labels are matched store the values
				 if (foundrep){
					 if(groupType[t] == "SUM"){
 						SumDataSeries[i] =  parseFloat(Count);	//for SUM
					 }

					if((groupType[t] == "AVG") && (foundrep)) {
	                    averageName[average] = tLabelData[i];
 						averageData[average] = parseFloat(parseFloat(Count)/parseFloat(iCnt+1));
				        average++;
					}

                     if((groupType[t] == "LAST") && (foundrep)) {
                        lastData[last] = lastVal;
                        last++;
                    }
				 }

				//if i and j labels do not match, case may be unique data labels.
                if (!foundrep){
                    if(groupType[t] == "FIRST"){
                            firstData[first] = valPointData[i];
                            first++;
                     }
                    if(groupType[t] == "LAST"){
                         lastData[last] = valPointData[i];
                        last++;
                    }
                    if(groupType[t] == "MIN" || groupType[t] == "MAX"){
                        if(tLabelData[i] != null && valPointData[i]!= null){
                            minMaxName[min++] = tLabelData[i];
                            minMaxData[minCount++] = valPointData[i];
                        }
                    }
					if(groupType[t] == "SUM"){
						SumDataSeries[i] =  valPointData[i];	//for SUM
					}
					if(groupType[t] == "AVG"){
                    averageName[average] = tLabelData[i];
                    averageData[average] = valPointData[i];
                    average++;
					}
                } 
        
  							
  
                //Print Min and Max Data for the Person
                if(groupType[t] == "MIN" || groupType[t] == "MAX"){
                    for(var g=0;g<min;g++) {
                        if(minMaxName[g] !=null && minMaxName[g] == tLabelData[i]) {
                            var str = new Array();

						//fix begin for min/max in mozilla/netscape for V6
 							var tempsortminmax = new Array();
							var ts=0;
							for (var s=0;s<minMaxData.length;s++){
 								if (minMaxData[s] != "null" && minMaxData[s] != "undefined" && minMaxData[s] != undefined){
 								tempsortminmax[ts] = minMaxData[s];
								ts++;
								}
							}

							str = tempsortminmax.sort(compareNumbers);

						//fix end  for min/max in mozilla/netscape V6
 
                            if(groupType[t] == "MIN"){
                                minArray[g] = str[0];
								break;//added for V6
                            }
                            if(groupType[t] == "MAX"){
                                for(var jj = str.length-1 ; jj >= 0 ; jj--){
                                    if(str[jj] != null){
                                        maxArray[g] = str[jj];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                Count=0;
                flag = false;
                bCheckName = false;
 
                if(tLabelData[i] != null){
                    countData[counter] = iCnt+1;
                    counter++;
                }
 
            } // end of i loop
    
            var tempAvgName = new Array();
            var tempAvgData = new Array();
            for(var v=0, i = 0 ;v<average;v++) {
                 if(averageName[v] != null || averageName[v] !='undefined'){
                    tempAvgName[i] = averageName[v];
                    tempAvgData[i] = averageData[v];
                    i++;
                }
            }
    
            if (groupType[t]=="AVG"){
                if(tempAvgName.length >0){
                    avgDataSeries[t]=tempAvgData;
                }
            }
            else if (groupType[t]=="SUM"){
  				  avgDataSeries[t] = SumDataSeries;
               }
            else if (groupType[t]=="FIRST"){
                 avgDataSeries[t]=firstData;
             }
            else if (groupType[t]=="LAST"){
                 avgDataSeries[t]=lastData;
             }
            else if (groupType[t]=="COUNT"){
                 avgDataSeries[t]=countData;
            }
            else if (groupType[t]=="MIN"){
                 avgDataSeries[t]=minArray;
            }
            else if (groupType[t]=="MAX"){
                 avgDataSeries[t]=maxArray;
             }
            //reset the values to 0 for the next series            
            average = 0;    
            min = 0;
            minCount = 0;
 			counter = 0;
			//onlyLabelData for storing the finalLabelData 
			 for (var k=0;k<tLabelData.length;k++){
				 onlyLabelData[k] = tLabelData[k];
				}

			//Re-initialize the nLabelData to tLabelData for each series 
			for (var k=0;k<nLabelData.length;k++){
				 tLabelData[k] = nLabelData[k];
			}
	 
       }//end else part

 		if (valPointData.length != "null" || valPointData.length  != null || valPointData.length  != 'undefined'){
         finalData = avgDataSeries;
		}

 		}//end series loop -  t       

      }//end of grouping function


    /**
      * @method public compare which is used by sort
     **/
    
    function compareNumbers(a, b) {
       return a - b
    }

    /**
      * @method public setChartType
      * Method to set the chart type in the control.
      *
     **/
    this.setChartType = function(new_chart_type)
    {
        this.graphDrawControl.chartType = new_chart_type;
    }

    /**
      * @method public refresh
      * Method refresh is used to call the ShowGraph() function in the GraphDrawControl
      * to render the graph.
     **/
    this.refresh = function()
    {
        this.graphDrawControl.ShowGraph();
    }
}
// commonValidations.js

// newFunction

//
// Checks if str contains digits and characters
//
function allDigitsAndChars(str)
{
	return inValidCharSet(str,"0123456789abcdefghjklmnprstuvwxyABCDEFGHJKLMNPRSTUVWXY");
}

//
// Checks if str contains digits only
//
function allDigits(str) {
	return inValidCharSet(str,"0123456789");
}

// checks if all chars are in the set
function inValidCharSet(str,charset)
{
	var result = true;
	
	for (var i=0;i<str.length;i++)
		if (charset.indexOf(str.substr(i,1))<0)
		{
			result = false;
			break;
		}
	
	return result;
}

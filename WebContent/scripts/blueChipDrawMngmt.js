// blueChipDrawMngmt.js

var buttonObj = null;
var discardChangesButton = null;
var submitButtonObj = null;

var editableDrawFields = null;

function disableButton() {
	buttonObj.disabled = true; 
}

///////////////////////////////////////////////////
function func_discardClicked(thisObj, thisEvent) {
	
	if (submitButtonObj == null) {
		var submitArray = $$('input.submitChangesStyle');
		if (submitArray != null && submitArray[0] != null) {
			submitButtonObj = submitArray[0];
		}
	}
	
	// disable the submit button if discarding.
	if (submitButtonObj != null) {
		submitButtonObj.disabled = true;
	}
}
 
function func_SaveClicked(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	buttonObj = thisObj;
	var origText = thisObj.value;
	thisObj.value = "Please Wait...";
	
	if (discardChangesButton == null) {
		var cancelArray = $$('input.cancelChangesStyle');
		if (cancelArray != null && cancelArray[0] != null) {
			discardChangesButton = cancelArray[0];
		}
	}
	
	var formErrorsExist = false;
	
//	formErrorsExist = formHasErrors();
	for(i = 0; i < editableDrawFields.length; i++) {
		if ($(editableDrawFields[i]).hasClassName('inputText_Error')) {
			thisObj.value = origText;
			setTimeout('alert("There are errors on the page. Please fix all errors and resubmit")', 100);
			formErrorsExist = true;
			editableDrawFields[i].focus();
			editableDrawFields[i].select();
			break;
		}
	}

	
	if (!formErrorsExist) {
		if (discardChangesButton != null) {
			discardChangesButton.disabled = true;
		}	
		setTimeout('disableButton()',2000);
	}
	
	// return true only if there are no errors
	if (formErrorsExist) {
		return false;
	} 
	else {
		return true;
	}
}

function formHasErrors() {
	var errorsExist = false;
	
	for(i = 0; i < editableDrawFields.length; i++) {
		if ($(editableDrawFields[i]).hasClassName('inputText_Error')) {
			thisObj.value = origText;
			setTimeout('alert("There are errors on the page. Please fix all errors and resubmit")', 100);
			errorsExist = true;
			editableDrawFields[i].focus();
			editableDrawFields[i].select();
			break;
		}
	}
	return errorsExist;
}

function func_SaveDrawKeyPress(thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	

	if(thisEvent.keyCode == Event.KEY_RETURN || thisEvent.keyCode == Event.KEY_DOWN) {
		if(firstField != null) {
			firstField.focus();
			firstField.select();
		}
		thisEvent.stop();
	}
	return false;
}

/////////////////////////////////////////////////////////
//  Cut off time countdown
/////////////////////////////////////////////////////////
var countDownMinutes = 30;
var countDownInfoThreshold = 15;
var countDownWarningThreshold = 10;
var countDownDangerThreshold = 3;
var countDownToCutOff = true;

function updateBlueChipCountdown() {

	if (countDownToCutOff == false || countDownToCutOff == 'false') {
		return;
	}
	
	var divArea = $('countdownMSG');
	if (divArea == null) {
		countDownToCutOff = false;
		return;
	}

	if (countDownMinutes > countDownInfoThreshold) {
		if (divArea.visible()) {
			divArea.hide();
		}
	}
	else {	
		if (!divArea.visible()) {
			divArea.show();
		}
		if (countDownMinutes > countDownWarningThreshold && countDownMinutes <= countDownInfoThreshold ) {
			divArea.innerHTML = '<span class="outputOKArea">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;You have <b>' + countDownMinutes + '</b> minutes until the order data becomes outdated and must be refreshed.';
		}
		else if (countDownMinutes > countDownDangerThreshold && countDownMinutes <= countDownWarningThreshold) {
			divArea.innerHTML = '<span class="outputWarningArea">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;Warning: You have <b>' + countDownMinutes + '</b> minutes until the order data becomes outdated and must be refreshed.';	
		}
		else if (countDownMinutes > 0) {
			divArea.innerHTML = '<span class="outputDangerArea">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;WARNING: You only have <b>' + countDownMinutes + '</b> minutes until the order data becomes outdated and must be refreshed. Any UnSaved Changes will be lost.';
		}
		else {
			divArea.innerHTML = '<span class="outputDangerArea">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;WARNING: The order data has become outdated and must be refreshed. Any UnSaved Changes will be lost. Click Save Changes or Discard Changes to refresh the data.';
		}
	}
	if (countDownMinutes <= countDownDangerThreshold) {
		if (divArea.hasClassName('outputTextAltDanger') == false) {
			divArea.addClassName('outputTextAltDanger');
		}
	}
	
	if (countDownMinutes > 0) {
		countDownMinutes = countDownMinutes - 1;
		setTimeout('updateBlueChipCountdown()', 60000);
	}
	else {
		countDownToCutOff = false;
	}
}

////////////////////////////////////////////////////////////////////////
//
//  Function to validate draw value on client side
//
////////////////////////////////////////////////////////////////////////
function blueChipDrawValidation(thisObj, thisEvent, maxDraw, minDraw, originalDrawValue, marketPhone) {

	var valStr = thisObj.value;
	valStr = valStr.strip();
	thisObj.value = valStr;

	var currentDrawValueInt = -2;
	var drawField = $(thisObj);
	
	if (valStr.length > 0 && allDigits(valStr)) { 
		currentDrawValueInt =  parseInt(valStr, 10);
	}
	
	if (currentDrawValueInt == originalDrawValue) {		// remove and server side errror flags
		$(thisObj).removeClassName('inputText_Error');
		return;
	}

	if ((drawField.value.length == 0) || (allDigits(drawField.value) == false) ||
	     (currentDrawValueInt < minDraw)  || (currentDrawValueInt > maxDraw)) {
	
		if ( !drawField.hasClassName('inputText_Error') ) {
			drawField.addClassName('inputText_Error');
			var alertStr = "Quantity must be set to a positive integer.     Minimum Value: " + minDraw + "  Maximum Value: " + maxDraw + ". Contact your regional USA TODAY office for assistance in entering quantities outside of this range. USA TODAY Office Phone:  " + marketPhone;
			thisObj.focus();
			thisObj.select();
			setTimeout('alert("' + alertStr + '")', 100);
		}
		return;
	}

	// remove and server side errror flags
	drawField.removeClassName('inputText_Error');
}
// windowutility.js
var helpWindow = null;
var rdlWindow = null;
var weeklyRDLWindow = null;
var printWindow = null;

function fullScreen(theURL, thePageName) {
	return window.open(theURL, thePageName, 'fullscreen=yes, scrollbars=auto');
}

function helpScreen(theURL) {
	if (helpWindow == null || helpWindow.closed) {
		helpWindow = window.open(theURL, 'helpWin', 'width=615,height=650,status=no,scrollbars=yes,resizable=yes');
		// retrieve user's available screen resolution 
		var w = window.screen.availWidth; 
		var h = window.screen.availHeight; 
		var delta = w - 620;
		if (delta > 0) {
			helpWindow.moveTo(delta, 20);
		}
	}
	else {
		helpWindow.document.location = theURL;
		setTimeout('helpWindow.focus()', 500);
	}	
	return helpWindow;
}


// functions for confirming page unload to use add a body onbeforeunload event to 
// call the confirmUnload function.
  
var submitClicked = false;
var forceClose = false; 

function sClicked () {
	submitClicked = true;
}

function sUnClicked () { 
	submitClicked = false;
}

function setForceClose() {
	forceClose = true;
}

function confirmUnload(thisObj, thisEvent) {

	var messageText = "Changes will be lost.";

	if (forceClose == false && !submitClicked) { 
		// want to determine if they clicked the X or a button, 
		// if button then just return.
		thisEvent.returnValue = messageText;
		submitClicked = false;
	}
	else {
		submitClicked = false;
		return;
	}
} 

function setWeeklyRDLWindow(rdlWin) {
	weeklyRDLWindow = rdlWin;
}

function setRDLWindow(rdlWin) {
	rdlWindow = rdlWin;
}

function setHelpWindow(hWin) {
	helpWindow = hWin;
}

function setPrintWindow(hWin) {
	printWindow = hWin;
}

// this function is called by d4day.jsp to update the main window,
// if the user moves around in the main window or closes the main window.
function confirmParentOpenDaily() {

	try {
		if (!window.opener || !window.opener.open || window.opener.closed) {
			try {
				window.setForceClose();
			}
			catch (ee) {
			} 
			window.close();
		}
		else {
			try {
				window.opener.setRDLWindow(window);
			}
			catch (e) {
				window.setForceClose();
				window.close();
			}
			
			setTimeout('confirmParentOpenDaily()', 10000);
		}
	}
	catch (outere) {
		window.setForceClose();
		window.close();
	}
}

// this function is called by d4week.jsp to update the main window,
// if the user moves around in the main window or closes the main window.
function confirmParentOpenWeekly() {

	try {
		if (!window.opener || !window.opener.open || window.opener.closed) {
			try {
				window.setForceClose();
			}
			catch (ee) {
			}
			window.close();
		}
		else {
			try {
				window.opener.setWeeklyRDLWindow(window);
			}
			catch (e) {
			}
		}
		setTimeout('confirmParentOpenWeekly()', 10000);
	}
	catch (outere) {
		window.setForceClose();
		window.close();
	}
}

// this function is called by the help jsps to update the main window,
// if the user moves around in the main window or closes the main window.
function confirmParentOpenHelpWin() {

	try {
		if (!window.opener || !window.opener.open || window.opener.closed) {
			window.close();
		}
		else {
			try {
				window.opener.setHelpWindow(window);
			}
			catch (e) {
			}
			setTimeout('confirmParentOpenHelpWin()', 8000);
		}
	}
	catch (outere) {
		window.close();
	}
} 

// this function is called by the help jsps to update the main window,
// if the user moves around in the main window or closes the main window.
function confirmParentOpenPrintWin() {

	try {
		if (!window.opener || !window.opener.open || window.opener.closed) {
			window.close();
		}
		else {
			try {
				window.opener.setPrintWindow(window);
			}
			catch (e) {
			}
			setTimeout('confirmParentOpenPrintWin()', 6000);
		}
	}
	catch (outere) {
		window.close();
	}
}

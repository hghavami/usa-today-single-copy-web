// drawmngmt.js
// the set of input text fields
var textInputFields = null;
var editableDrawFields = null;
var editableReturnFields = null;

var currentZDRField = '';
var currentHiddenField  = '';
var validatingField = false;

var currentField = null;
var previousField = null;

var lastSelectedElement = null;


//////////////////////////////////////////
// Following method is used to prevent the 
// links on the product types and such from
// taking focus. (kind of misnamed)
/////////////////////////////////////////
function autoTab(thisObj, thisEvent) {
	if (lastSelectedElement) {
		lastSelectedElement.focus();
		lastSelectedElement.select();
	}
}

// performs draw field validation on the draw fields in a weekly view
function drawValueChangedWeekly(thisObj, thisEvent, maxDraw, originalDrawValue, zdrFieldID, originalZDRValue, zdrImageIndicatorID, zdrImageIndicatorDiv, locationID, locationName, poid, pubCode, returnFieldID, dow) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'

	// if ZDR window visible don't do anything but eat the event
	if ($('ZeroDrawReasonChooser').visible()) {
		return;
	}
	
	var valStr = thisObj.value;
	valStr = valStr.strip();
	thisObj.value = valStr;

	currentZDRField = thisObj;

	// need to get the current table row so we can then get all input fields in that row
	currentHiddenField = $(zdrFieldID);

	var hiddenField = $(zdrFieldID);
	
	var zdrImageIndicator = $(zdrImageIndicatorID);
	var zdrImageIndicatorDiv = $(zdrImageIndicatorDiv);

	var currentDrawValueInt = -1;

	validatingField = true;
	
	if (submitClicked) {
		validatingField = false;
		return;
	}
		
	// remove and server side errror flags
	var currentReturnField = $(returnFieldID);

	// check for negative numbers and non numerics
	if ((currentZDRField.value.length == 0) || (allDigits(currentZDRField.value) == false) ) {
		if ( !$(thisObj).hasClassName('inputText_Error') ) {
			if (currentReturnField.disabled == false) {
				$(currentReturnField).addClassName('inputText_Error');
			}
			$(thisObj).addClassName('inputText_Error');
			var alertStr = "Draw must be set to a positive integer or 0.";
			setTimeout('alert("' + alertStr + '")', 100);
			thisObj.focus();
			thisObj.select();
			previousField = thisObj;
		}
		validatingField = false;
		return;
	}
	
	currentDrawValueInt = parseInt(thisObj.value, 10);
	
	var currentReturnFieldValueInt = -1;
	
	if (currentReturnField && (currentReturnField.value.length > 0) && (allDigits(currentReturnField.value)==true) ) {
		currentReturnFieldValueInt = parseInt(currentReturnField.value, 10);
	}

	// draw value is valid, make sure it does not cause the 
	// corresponding return field value to become invalid (returns can't be more than draw)
	if (currentDrawValueInt < currentReturnFieldValueInt) {

		if ( !$(thisObj).hasClassName('inputText_Error') ) {
			if (currentReturnField.disabled == false) {
				$(currentReturnField).addClassName('inputText_Error');
			}
			$(thisObj).addClassName('inputText_Error');
			var alertStr = "Draw value cannot be lower than returns. Please update the returns field before reducing draw.";
			setTimeout('alert("' + alertStr + '")', 100);
			if (currentDrawValueInt == 0) {
				// can't allow a zero draw here because it creates a whole
				// with the requirement that we must have a zero draw reason.
				thisObj.value = originalDrawValue;
			}
			thisObj.focus();
			thisObj.select();
			$(thisObj).addClassName('inputText_Error');
			previousField = thisObj;
		}
		validatingField = false;
		return;
	}

	// check for zero draw value 
	if (currentDrawValueInt == 0) {

		// if hidden field value is non zero length then ZDR indicator already displayed.
		if (currentHiddenField.value.length == 0 && !zdrImageIndicatorDiv.visible()) {
			zdrImageIndicatorDiv.show();

			showZRDPanel(thisObj, zdrFieldID, locationID, locationName, poid, pubCode, dow, 'true', originalZDRValue, originalDrawValue);
		}
	}
	else {
		// clear any previosly set zero draw reason codes
		currentHiddenField.value = "";
		zdrImageIndicatorDiv.hide();
		
		// check if value exceeds max draw
		if (currentDrawValueInt > maxDraw) {
			if ( !$(thisObj).hasClassName('inputText_Error') ) {
				var alertStr = "Draw value cannot exceed maximum value of " + maxDraw + ". Contact your market for assistance.";
				setTimeout('alert("' + alertStr + '")', 100);
				$(thisObj).addClassName('inputText_Error');		
				thisObj.focus();
				thisObj.select();
			}
			validatingField = false;
			return;
		}
	}
	$(thisObj).removeClassName('inputText_Error');			
	validatingField = false;
}

//////////////////////////////////////////////////////
//    Daily Draw Value changed
///////////////////////////////////////////////////////
function drawValueChangedDaily(thisObj, thisEvent, maxDraw, originalDrawValue, zdrFieldID, originalZDRValue, zdrImageIndicatorID, zdrImageIndicatorDiv, locationID, locationName, poid, pubCode, drawDayReturns) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'

	// if ZDR window visible don't do anything but eat the event
	if ($('ZeroDrawReasonChooser').visible()) {
		return;
	}
	// need to get the current table row so we can then get all input fields in that row
	currentHiddenField = $(zdrFieldID);

	var hiddenField = $(zdrFieldID);
	
	var zdrImageIndicator = $(zdrImageIndicatorID);
	var zdrImageIndicatorDiv = $(zdrImageIndicatorDiv);

	var currentDrawValueInt = -1;
	
	var valStr = thisObj.value;
	valStr = valStr.strip();
	thisObj.value = valStr;
	valInt = -2;
	if (valStr.length > 0 && allDigits(valStr)) { 
		valInt =  parseInt(valStr, 10);
	}
	
	// don't need to validate if value as was loaded originally
	if (valInt == originalDrawValue) {		// remove and server side errror flags
		$(thisObj).removeClassName('inputText_Error');
		currentDrawValueInt = parseInt(thisObj.value, 10);	
		if (currentDrawValueInt == 0) {
			zdrImageIndicatorDiv.show();
			hiddenField.value = originalZDRValue;
		}
		else {
			hiddenField.value = '';
			zdrImageIndicatorDiv.hide();
		}
		validatingField = false;
		return;
	}

	currentZDRField = thisObj;
	 
	validatingField = true;
	
	if (submitClicked) {
		validatingField = false;
		return;
	}
		
	if ((currentZDRField.value.length == 0) || (allDigits(currentZDRField.value) == false) ) {
	
		if ( !$(thisObj).hasClassName('inputText_Error') ) {
			$(thisObj).addClassName('inputText_Error');
			var alertStr = "Draw must be set to a positive integer or 0.";
			setTimeout('alert("' + alertStr + '")', 100);
			thisObj.focus();
			thisObj.select();
			previousfield = thisObj;
		}
		validatingField = false;
		return;
	}
	
	currentDrawValueInt = parseInt(thisObj.value, 10);	
	if (currentDrawValueInt < drawDayReturns) {
		if ( !$(thisObj).hasClassName('inputText_Error') ) {
			$(thisObj).addClassName('inputText_Error');
			var alertStr = "Draw value cannot be less than returns value of " + drawDayReturns + ".";
			setTimeout('alert("' + alertStr + '")', 100);
			thisObj.focus();
			thisObj.select();
			previousfield = thisObj;
		}
		validatingField = false;
		return;
	}	
	
	
	// perform further draw value validations
	// check for zero draw value 
	if (currentDrawValueInt == 0) {
		
		if (currentHiddenField.value.length == 0 && !zdrImageIndicatorDiv.visible()) {
			zdrImageIndicatorDiv.show();

			showZRDPanel(thisObj, zdrFieldID, locationID, locationName, poid, pubCode, null, 'true', originalZDRValue, originalDrawValue);				
		}
	}
	else {
		// clear any previosly set zero draw reason codes
		currentHiddenField.value = "";
		zdrImageIndicatorDiv.hide();
		
		// check if value exceeds max draw
		if (currentDrawValueInt > maxDraw) {
			if ( !$(thisObj).hasClassName('inputText_Error') ) {
				$(thisObj).addClassName('inputText_Error');
				var alertStr = "Draw value cannot exceed maximum value of " + maxDraw + ". Contact your market for assistance.";
				setTimeout('alert("' + alertStr + '")', 100);
				thisObj.focus();
				thisObj.select();
				previousField = thisObj;
			}
			validatingField = false;
			return;
		}
		validatingField = false;
	}

	// remove and server side errror flags
	$(thisObj).removeClassName('inputText_Error');
	validatingField = false;
}


////////////////////////////////////////////////
//   Show ZDR 
///////////////////////////////////////////
function showZRDPanel(thisObj, zdrFieldId, locationID, locationName, poid, pubCode, dow, allowChanges, originalZDRCode, originalDrawValue) {

	if (allowChanges != 'true') { 
		alert('VIEW ONLY: Hover your mouse over the ZDR icon to view the Zero Draw Reason.');
		return;
	}
	
	$('tempLocID').value = locationID;
	$('tempLocName').innerHTML = '<SPAN class="inputText_disabledV2">' + locationName + ' </SPAN>';
	$('tempPOID').value = poid;
	$('tempPubCode').value = pubCode;
	$('zdrFieldID').value = zdrFieldId;
	if (dow && $('tempDayOfWeek')) {
		$('tempDayOfWeek').innerHTML = '<SPAN class="inputText_disabledV2">' + dow + ' </SPAN>';
	}
	
	var dform = $('formManageDraw');
	currentHiddenField = $(zdrFieldId);
	
	Position.clone(dform, 'ZeroDrawReasonChooser');
				  
	Effect.Appear('ZeroDrawReasonChooser', {duration:0.1});
				
	var selectBox = $('ZeroDrawReasonSelector');

	selectBox.selectedIndex = 0;
	
	for (i=0; i<selectBox.options.length; i++){
		if(selectBox.options[i].value == currentHiddenField.value) {
			selectBox.selectedIndex = i;
		}
	}
	
	// if original ZDR value was not set then show cancel button
	if ((originalZDRCode.length == 0) &&
		 (originalDrawValue == '0')) {
		$('ZDRCodeCancel').setStyle({display: 'inline'});
	}
	else {
		$('ZDRCodeCancel').setStyle({display: 'none'});
	}
	
}

////////////////////////////////////////////////
//   ZDR Selected
///////////////////////////////////////////
function zeroDrawValueSelected(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	var selectBox = $('ZeroDrawReasonSelector');
	
	if (!$('ZDRCodeCancel').visible()) {
		if (selectBox.selectedIndex == 0) {
			alert( 'Please choose a Zero Draw Reason from the options in the drop down list.');
			return false;
		}
	}
	
	var tempzdrFieldID = $('zdrFieldID').value;
	var hiddenField = $(tempzdrFieldID)
	
	hiddenField.value = selectBox.value;
	
	// hide this div
	Effect.Fade('ZeroDrawReasonChooser', {duration:0.1});
	
	$('zdrFieldID').value = "";
	
	currentZDRField = '';
	currentHiddenField = '';
	
	return true;
	
}

// handle a cancle of ZDR entry
function zeroDrawValueCanceled(thisObj, thisEvent) {
	// hide this div
	Effect.Fade('ZeroDrawReasonChooser', {duration:0.1});
	
	currentZDRField = '';
	currentHiddenField = '';
	
	return true;

}


///////////////////////////////////////////////
//  Weekly RETURNS Edits.
//  NOTE: Pandora's box method, use extreme caution when altering.
//        Any rules that apply to the daily view should be updated in the corresponding
//        daily method.
//////////////////////////
function returnValueChangedWeekly(thisObj, thisEvent, originalValue, drawFieldID) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	
	validatingField = true;
	
	// if zero draw reason window is open ignore this event.
	if ($('ZeroDrawReasonChooser').visible()) {
		Event.stop(thisEvent);
		validatingField = false;
		return;
	}
	
	window.status = '';

	// it's ok to clear returns (set to blank)
	if (thisObj.value.length == 0 || inValidCharSet(thisObj.value, ' ')) {
		thisObj.value = '';
		validatingField = false;
		$(thisObj).removeClassName('inputText_Error');
		return;		
	}

	var drawField = null;
	if (drawFieldID != null && drawFieldID.length > 0) {
		drawField = $(drawFieldID);
	}
	
	// catch negative values of non numerics
	if ((thisObj.value.length > 0) && (allDigits(thisObj.value) == false) ) {
		
		if ( !$(thisObj).hasClassName('inputText_Error') ) {
			var alertStr = "Returns must be set to a positive integer, 0 or blank.";
			$(thisObj).addClassName('inputText_Error');
			setTimeout('alert("' + alertStr + '")', 100);
			if (!drawField.hasClassName('inputText_Error')) {
				thisObj.focus();
				thisObj.select();
			}
		}
				
		Event.stop(thisEvent);
		
		validatingField = false;
		return false;
	}	
		
	var thisObjValueInt = parseInt(thisObj.value, 10);
	
	// make sure return is not greater than draw field
	var drawFieldInt;
	// need to get the current table row so we can then get all input fields in that row
	if (drawField != null) {
		if ( !isNaN(drawField.value) ) {
			drawFieldInt = parseInt(drawField.value, 10);
		}			
	}
	
	if (drawFieldInt < 0) {
		// if draw field is less than zero then this value is invalid too
		// simply set to red but don't request focus
		if ( !$(thisObj).hasClassName('inputText_Error') ) {
			$(thisObj).addClassName('inputText_Error');
		}
		Event.stop(thisEvent);
		validatingField = false;
		return;
	}
	else if (thisObjValueInt > drawFieldInt) {
		// use of the setTimeout is to get around a bug in Mozilla that would interfere with the onkeydown event
		// when an alert box is displayed directly from this method.
		if ( !$(thisObj).hasClassName('inputText_Error') ) {
			var alertStr = "Return value cannot exceed draw value of " + drawFieldInt;
			$(thisObj).addClassName('inputText_Error');
			setTimeout('alert("' + alertStr + '")', 100);

			// only request focus if draw field not in error
			if (!drawField.hasClassName('inputText_Error')) {					
				thisObj.focus();
				thisObj.select();
			}
		}

		Event.stop(thisEvent);
		validatingField = false;
		return;
	}
	$(thisObj).removeClassName('inputText_Error');
	validatingField = false;
	
}

///////////////////////////////////////////////
//  Daily RETURNS Edits.
//  NOTE: Pandora's box method, use extreme caution when altering.
//        Any rules that apply to the daily view should be updated in the corresponding
//        weekly method.
//////////////////////////
function returnValueChangedDaily(thisObj, thisEvent, previousDayDrawValue) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	
	validatingField = true;
	
	// if zero draw reason window is open ignore this event.
	if ($('ZeroDrawReasonChooser').visible()) {
		Event.stop(thisEvent);
		validatingField = false;
		return;
	}
	
	// it's ok to clear returns (set to blank)
	if (thisObj.value.length == 0 || inValidCharSet(thisObj.value, ' ')) {
		thisObj.value = '';
		validatingField = false;
		$(thisObj).removeClassName('inputText_Error');
		return;		
	}

	// catch negative values of non numerics
	if ((thisObj.value.length > 0) && (allDigits(thisObj.value) == false) ) {
		
		if ( !$(thisObj).hasClassName('inputText_Error') ) {
			var alertStr = "Returns must be set to a positive integer, 0 or blank.";
			$(thisObj).addClassName('inputText_Error');
			setTimeout('alert("' + alertStr + '")', 100);
			thisObj.focus();
			thisObj.select();
		}
				
		Event.stop(thisEvent);
		
		validatingField = false;
		return false;
	}	
		
	var thisObjValueInt = parseInt(thisObj.value, 10);
	
	// make sure return is not greater than draw field
	var drawFieldInt = previousDayDrawValue;
	
	if (drawFieldInt < 0) {
		// if draw field is less than zero then this value is invalid too
		// simply set to red but don't request focus
		if ( !$(thisObj).hasClassName('inputText_Error') ) {
			var alertStr = "Previous Draw Value not set so this field must remain blank. ";
			setTimeout('alert("' + alertStr + '")', 100);
			$(thisObj).value = '';
		}
		Event.stop(thisEvent);
		validatingField = false;
		return;
	}
	else if (thisObjValueInt > drawFieldInt) {
		// use of the setTimeout is to get around a bug in Mozilla that would interfere with the onkeydown event
		// when an alert box is displayed directly from this method.
		if ( !$(thisObj).hasClassName('inputText_Error') ) {
			var alertStr = "Return value cannot exceed draw value of " + drawFieldInt;
			$(thisObj).addClassName('inputText_Error');
			setTimeout('alert("' + alertStr + '")', 100);
			thisObj.focus();
			thisObj.select();
		}

		Event.stop(thisEvent);
		validatingField = false;
		return;
	}
	$(thisObj).removeClassName('inputText_Error');
	validatingField = false;
}

////////////////////////////////////
//  KEY PRESSED                  //
///////////////////////////////////

function aKeyPressed(anEvent) {

		// if ZDR window visible don't do anything but eat the event
		if ($('ZeroDrawReasonChooser').visible()) {
			if (BrowserDetect.browser == 'Explorer') {	
				Event.stop(anEvent);
				return;
			}
			else {
				return false;
			}
		}
		
		if (anEvent.keyCode == Event.KEY_RETURN) {
		
			var elt = Event.element(anEvent);

			if (elt.tagName.toLowerCase() == 'input' && ['text', 'hidden'].include(elt.type)) {
						
				if (BrowserDetect.browser == 'Explorer') {
					// following works in IE
					if (elt.tagName.toLowerCase() == 'input' && ['button', 'submit'].include(elt.type)) {
						return anEvent.keyCode;
					}
					else {
						var nextElt = $(elt).next();
						while( nextElt && nextElt.type != 'text' && nextElt.disabled) {
							nextElt = $(nextElt).next();
						}
						if (!nextElt) {
							nextElt = $(elt).next();
						}
						if( nextElt && nextElt.tagName.toLowerCase() == 'input' && ['button', 'submit'].include(nextElt.type)) {				
							// if we are on the last input field then stop the auto tab
							//alert ('stopping event');
							Event.stop(anEvent);
							return;
						}
						else {
							anEvent.keyCode = Event.KEY_TAB;
							return anEvent.keyCode;
						}
					}
				} // end if IE browser
				else {
				
					var nextElt = $(elt);
					var found = false;
					// get the list of text inputs from global holder
					var textInputs = null;
					if (textInputFields != null) {
						textInputs = textInputFields;
					}
					else {
						textInputFields = $$('input.dmEditField');
						textInputs = textInputFields;
					}
					var currentIndex = 1;
					var z;
					for (z = 0; z < textInputs.length; z++) {
						if (textInputs[z] == nextElt) {
							break;
						}
						else {
							currentIndex++;
						}
					}
					if (currentIndex < textInputs.length) {
						nextElt = textInputs[currentIndex];
					}
					else {
						nextElt = null;
					}
					while( found == false) {
						
						if (nextElt != null && nextElt != undefined) {
							if (nextElt.disabled == false) {
								found = true;
							}
							else {
								currentIndex = currentIndex + 1;
								if (currentIndex < textInputs.length) {
									nextElt = textInputs[currentIndex];
								}
								else {
									found = true;
								}
							}
						}
						else {
							found = true;
						}
					}
					
					if (nextElt != null && nextElt.tagName.toLowerCase() == 'input' && ['button', 'submit'].include(nextElt.type)) {
						return true;
					}
					else {
						try {
							if (nextElt != null && typeof(nextElt) != 'undefined' && typeof(nextElt.focus) == 'function') {
								nextElt.focus();
								nextElt.select();
							}
						} catch (e) {
							; // ignore
						}
						
						//Event.stop(anEvent);
						return false;
					}
					
				} // end else not IE browser
			
			} // end if current element an input type of text or hidden.
			else {
				return; //anEvent.keyCode;
			}
		} // end if Enter key pressed
}

//////////////////////////////////////////////////////
// This method is required to reverse the "Enter Key" for Tab event 
// that will happen even when the field prior was in error and needs focus
///////////////////
function focusGainedWeekly(thisObj, thisEvent) {
	
	// if curent field not in error then check previous field for error.
	if (!$(thisObj).hasClassName('inputText_Error')) {
		if (previousField != null && previousField.hasClassName('inputText_Error')) {
			var temp = previousField;
			previousField = null;
			temp.select();
			temp.focus();
		}
		else {
			if (currentField != null) {
				previousField = $(currentField);
			}
			currentField = $(thisObj);
		}
	}
	else {
		// if current field is in error we only go back if this is a return field.
		if (previousField != null && previousField.hasClassName('inputText_Error')) {
				// only go back if we are a returns field. assume draw field is open
				var row = thisObj.parentNode;
				while(row.nodeName != "TR") {
					row = row.parentNode;
				}
				
				// capture the current draw field
				var rowFields = row.getElementsByTagName("input");
				for (i = 0; i < rowFields.length; i++) {
					if (rowFields[i].type == "text" && rowFields[i] != thisObj && rowFields[i] == previousField) {
						var temp = previousField;
						previousField = null;
						temp.select();
						temp.focus();
					}
				}
		}
	}
}

//////////////////////////////////////////////////////
// This method is required to reverse the "Enter Key" for Tab event 
// that will happen even when the field prior was in error and needs focus
///////////////////
function focusGainedDaily(thisObj, thisEvent) {
	
	// if curent field not in error then check previous field for error.
	if (!$(thisObj).hasClassName('inputText_Error')) {
		if (previousField != null && previousField.hasClassName('inputText_Error')) {
			var temp = previousField;
			previousField = null;
			temp.select();
			temp.focus();
		}
		else {
			if (currentField != null) {
				previousField = $(currentField);
			}
			currentField = $(thisObj);
		}
	}
}


// validates the weekly draw form prior to submission
// really just checks to see if any fields are in error.
function validateDrawMngmtForm(thisObj, thisEvent) {

	if (validatingField ) {
		submitClicked = false;
		return false;
	}
	
	$('StatusMessageField').innerHTML = 'Please Wait....<IMG border="0" src="/scweb/images/squaresAnimated.gif" width="80"	height="10">';
	
	// if user has disabled fields then turn them on
	enableCheckBoxes();

	var textInputs = null;
	if (textInputFields != null) {
		textInputs = textInputFields;
	}
	else {
		textInputFields = $$('input.dmEditField');
		textInputs = textInputFields;
	}
	
	var formPassed = true;
	
	for(i = 0; i < textInputs.length; i++) {
		if ($(textInputs[i]).hasClassName('inputText_Error')) {
			submitClicked = false;
			alert('There are errors on the page. Please fix all errors and resubmit');
			formPassed = false;
			textInputs[i].focus();
			textInputs[i].select();
			$('StatusMessageField').innerHTML = '&nbsp;';
			submitClicked = false;
			break;
		}
	}

	if ($('ZeroDrawReasonChooser').visible() ) {
		submitClicked = false;
		$('StatusMessageField').innerHTML = '&nbsp;';
		return false;
	}
 
	submitClicked = formPassed;
	
	return formPassed;
}

var resetZDRImagesFlag = false;

//////////////////////////////////////////////////////////////////////////
function resetZDRImages() {

	if (resetZDRImagesFlag == true) {
		resetZDRImagesFlag = false;
	}
	else {
		return;
	}
	
	var drawInputArray = $('formManageDraw').getElementsByClassName('drawInputText');
	
	for (i = 0; i < drawInputArray.length; i++) {
		if (drawInputArray[i].disabled == false ) {
			var intDrawVal = parseInt(drawInputArray[i].value, 10);
			if (intDrawVal > 0) {
				// locate and hide ZDR 
				var drawFieldID = new String(drawInputArray[i].id);
				var indexOfLastDelim = drawFieldID.lastIndexOf(':');
				var zdrImageDivID = drawFieldID.substring(0, indexOfLastDelim);
				var zdrValueID = zdrImageDivID;
				zdrImageDivID += ":zdrIndDiv";
				zdrValueID += ":ZeroDrawReasonCode";
				
				var zdrInd = $(zdrImageDivID);
				if (zdrInd) {
					 zdrInd.hide();
					 $(zdrValueID).value = '';
				}
			}
			else if (intDrawVal == 0) {
				var drawFieldID = new String(drawInputArray[i].id);
				var indexOfLastDelim = drawFieldID.lastIndexOf(':');
				var zdrImageDivID = drawFieldID.substring(0, indexOfLastDelim);
				zdrImageDivID += ":zdrIndDiv";
				
				var zdrInd = $(zdrImageDivID);
				if (zdrInd) {
					 zdrInd.show();
				 }
			}
		}
	}	
}

////////////////////////////////////////
//  Confirms reset of page
/////////////////////////////////////////
function confirmDrawReset(thisObj,thisEvent) {
		var rvalue=confirm('This will reset any changes you have made, returning them to the values that existed when the page loaded. Are you sure you want to reset the values?');
		if (rvalue) {
			$('StatusMessageField').innerHTML = 'Please Wait....<IMG border="0" src="/scweb/images/squaresAnimated.gif" width="80"	height="10">';
			// call function to clear any ZDR Image indicators/fields
			resetZDRImagesFlag = true; 

			enableCheckBoxes();
			// revalidate draw returns without alerts
			setTimeout("resetZDRImages()", 300);
			setTimeout("scanFieldsForErrors()", 600);
			return;
		}   
		else  {
			return false;	
		}
}

///////////////////////////////////////////
// removes any errof fields from a form after a user
// clicks the reset button.
//////////////////////////////////////////////////
function scanFieldsForErrors() {
	if (textInputFields != null) {
		var cField = null;
		var lastEnabled = null;
		for (i = textInputFields.length; i > 0; i--) {
			cField = $(textInputFields[i-1]);
			if (cField.disabled == false) {
				lastEnabled = cField;
				if (cField.hasClassName('inputText_Error')) {
					cField.focus();
				}
			}
		}
		if (lastEnabled != null) {
			lastEnabled.focus();
		}
	}
	$('StatusMessageField').innerHTML = '&nbsp;';	
	
}

////////////////////////////////////////////////////////
// toggles all draw fields
////////////////////////////////////////////////////////
function func_toggleDrawFields(thisObj, thisEvent) {
	if (editableDrawFields == null) {
		editableDrawFields = $$('input.drawInputText');
	}

	if (thisObj.checked == true) {
		reenableDrawFields();
	}
	else {
		disableDrawFields();
	}
}


////////////////////////////////////////////////////////
// disables all enabled draw fields
////////////////////////////////////////////////////////
function disableDrawFields() {
	if (editableDrawFields == null || editableDrawFields.length == 0) {
		return;
	}
	for (i = 0; i < editableDrawFields.length; i++) {
		editableDrawFields[i].disabled = true;
		editableDrawFields[i].addClassName('inputText_disabled');		
	}
	
	// Attempt to highlight a field
	if (lastSelectedElement != null) {
		if(lastSelectedElement.disabled) {
			lastSelectedElement.blur();
			var drawCheckBox = $('disableEditableReturnFields');
			if (drawCheckBox.checked) {
				if (editableReturnFields != null && editableReturnFields.length > 0) {
					editableReturnFields[0].focus();
					editableReturnFields[0].select();
				}
			}
		}
		else {
			lastSelectedElement.focus();
			lastSelectedElement.select();
		}
	}

}

////////////////////////////////////////////////////////
// enables all enabled draw fields
////////////////////////////////////////////////////////
function reenableDrawFields() {
	if (editableDrawFields == null || editableDrawFields.length == 0) {
		return;
	}
	for (i = 0; i < editableDrawFields.length; i++) {
		editableDrawFields[i].disabled = false;
		editableDrawFields[i].removeClassName('inputText_disabled');		
	}
	
	var drawCheckBox = $('disableEditableReturnFields');
	// if draw was disabled then set focus to first return field
	if (lastSelectedElement && lastSelectedElement.disabled == false) {	
		lastSelectedElement.focus();
		lastSelectedElement.select();
	}
	else if (drawCheckBox.checked == false) {
			if (editableDrawFields != null && editableDrawFields.length > 0) {
				editableDrawFields[0].focus();
				editableDrawFields[0].select();
			}
	}
	
}
////////////////////////////////////////////////////////
// toggles all return fields
////////////////////////////////////////////////////////
function func_toggleReturnFields(thisObj, thisEvent) {
	if (editableReturnFields == null) {
		editableReturnFields = $$('input.returnInputText');
	}

	if (thisObj.checked == true) {
		reenableReturnFields();
	}
	else {
		disableReturnFields();
	}
}

////////////////////////////////////////////////////////
// disables all enabled draw fields
////////////////////////////////////////////////////////
function disableReturnFields() {
	if (editableReturnFields == null || editableReturnFields.length == 0) {
		return;
	}
	for (i = 0; i < editableReturnFields.length; i++) {
		editableReturnFields[i].disabled = true;
		editableReturnFields[i].addClassName('inputText_disabled');		
	}
	
	// Attempt to highlight a field
	if (lastSelectedElement != null) {
		if(lastSelectedElement.disabled) {
			lastSelectedElement.blur();
			var drawCheckBox = $('disableEditableDrawFields');
			if (drawCheckBox.checked) {
				if (editableDrawFields != null && editableDrawFields.length > 0) {
					editableDrawFields[0].focus();
					editableDrawFields[0].select();
				}
			}
		}
		else {
			lastSelectedElement.focus();
			lastSelectedElement.select();
		}
	}
}

////////////////////////////////////////////////////////
// enables all enabled draw fields
////////////////////////////////////////////////////////
function reenableReturnFields() {
	if (editableReturnFields == null || editableReturnFields.length == 0) {
		return;
	}
	for (i = 0; i < editableReturnFields.length; i++) {
		editableReturnFields[i].disabled = false;
		editableReturnFields[i].removeClassName('inputText_disabled');		
	}
	var drawCheckBox = $('disableEditableDrawFields');
	// if draw was disabled then set focus to first return field
	if (lastSelectedElement && lastSelectedElement.disabled == false) {	
		lastSelectedElement.focus();
		lastSelectedElement.select();
	}
	else if (drawCheckBox.checked == false) {
			if (editableReturnFields != null && editableReturnFields.length > 0) {
				editableReturnFields[0].focus();
				editableReturnFields[0].select();
			}
	}
}

////////////////////////////////////////////////////////////
function enableCheckBoxes() {
	var checkBox = $('disableEditableReturnFields');
	if (checkBox != null && !checkBox.checked) {
		checkBox.checked = true;
		reenableReturnFields();
	}
	
	checkBox = $('disableEditableDrawFields');
	if (checkBox != null && !checkBox.checked) {
		checkBox.checked = true;
		reenableDrawFields();
	}
}

// keepalive.js

//apporximately 30 minutes for each keep alive 3= 1.5 hours
var MAX_KEEP_ALIVES = 7;  
var keepAliveCount = 1;

function startKeepAlive() {
	keepAlive();
}


// newFunction
// following method is used to keep the session alive.
// It is called whenever a draw managment window is opened.
function keepAlive() {

	var url = "/scweb/util/keepAlive.do";
	var ajaxReq = initRequest(url);
	ajaxReq.onreadystatechange = function() {
		
		if (ajaxReq.readyState == 4) {
			if (ajaxReq.status == 200) {
			
				parseKeepAliveMessages(ajaxReq.responseXML);
				
				// reset timer for 10 minutes 1000ms * 60 * 10 = 600000
				if (keepAliveCount < MAX_KEEP_ALIVES) {
					setTimeout('keepAlive()', 600000);
					keepAliveCount++;
				}
			}
		}
	};

    ajaxReq.open("GET", url, true);
	ajaxReq.send(null);
}

var countDownMinutes = 80;
var countDownWarningThreshold = 10;
var countDownDangerThreshold = 5;

function updateCountdown() {

	if (countDownMinutes > countDownWarningThreshold) {
		$('countdownMSG').innerHTML = '<span class="outputOKArea">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;You have about <b>' + countDownMinutes + '</b> minutes to submit your changes before your web session will expire due to inactivity.';
	}
	else if (countDownMinutes > countDownDangerThreshold) {
		$('countdownMSG').innerHTML = '<span class="outputWarningArea">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;Warning: You have about <b>' + countDownMinutes + '</b> minutes to submit your changes before your web session will expire due to inactivity.';	
	}
	else if (countDownMinutes > 0) {
		$('countdownMSG').innerHTML = '<span class="outputDangerArea">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;Danger: You have about <b>' + countDownMinutes + '</b> minutes to submit your changes before your web session will expire due to inactivity.';	
	}
	else {
		$('countdownMSG').innerHTML = 'Warning: Your session may have expired due to inactivity. Any changes you have made are likely lost.';
	}
	
	if (countDownMinutes == countDownDangerThreshold) {
		$('countdownMSG').addClassName('outputTextAltDanger');
	}
	
	if (countDownMinutes > 0) {
		countDownMinutes = countDownMinutes - 1;
		setTimeout('updateCountdown()', 60000);
	}
	
}

function initRequest(url) {
   if (window.XMLHttpRequest) {
       return new XMLHttpRequest();
   } else if (window.ActiveXObject) {
       return new ActiveXObject("Microsoft.XMLHTTP");
   }
}


function parseKeepAliveMessages(respMessage) {
	
	var outerRes = respMessage.getElementsByTagName("KeepAlive")[0];
	var response = outerRes.getElementsByTagName("GlobalMesssageActive")[0];
    var responseDetail = outerRes.getElementsByTagName("GlobalMesssageDetail")[0]; 
    var maintArea = $('globalMessageDivArea');
 
       if (maintArea && response && response.childNodes[0].nodeValue.length > 0 && response.childNodes[0].nodeValue == "true") {
			if (responseDetail && responseDetail.childNodes[0].nodeValue.length > 0 ) {
				
				maintArea.innerHTML = responseDetail.childNodes[0].nodeValue + '&nbsp;&nbsp;&nbsp;&nbsp;<div id="hideMessageArea" onclick="hideMaintArea();" style="font-size: 8pt; cursor: pointer;">Click To Hide</div> ';
				maintArea.removeClassName('globalMessageDivStyleHiddenNoTemplate');
				maintArea.show();
			}
			else {
				maintArea.innerHTML = "";
				maintArea.addClassName('globalMessageDivStyleHiddenNoTemplate');
				maintArea.hide();
			}
			
       }
       else {
       		if (maintArea) {
	       		maintArea.hide();
	       	}
       }
}

function hideMaintArea() {
	    var maintArea = $('globalMessageDivArea');
	    if (maintArea) {
			maintArea.hide();
		}
}
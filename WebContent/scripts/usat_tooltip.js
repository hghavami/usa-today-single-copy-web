// usat_tooltip.js
xOffset = -235;

yOffset = 10;

enableTip = false;

toolTip = document.getElementById("tooltip");





function browserName() {

browserAgent = navigator.userAgent;

var patternFirefox = /firefox/i;

var patternNetscape = /netscape/i;

var patternOpera = /opera/i;

var patternSafari = /safari/i;

var patternIE = /msie/i;



if (patternFirefox.test(browserAgent))

	return "Firefox";

else if (patternNetscape.test(browserAgent))

	return "Netscape";

else if (patternOpera.test(browserAgent))

	return "Opera";

else if (patternSafari.test(browserAgent))

	return "Safari";

else if (patternIE.test(browserAgent))

	return "IE";

else

	return "Other";

}

////////////////////////////////////////////
function isWhitespace(FValue)

{

var pattern = /^\s*$/;

if (pattern.test(FValue))

	return true;

else

	return false;

}


////////////////////////////////////////////
function showTip(FText,FWidth)

{
//toolTip = document.getElementById("tooltip");

if (browserName() == "MacIE")

	return false;

if (!isWhitespace(FText))

{

	//var htmlText = 	'<TABLE border="0" cellpadding="0" cellspacing="1"><TBODY><TR><TD>' + FText + '</TD></TR></TBODY></TABLE>';

	toolTip.innerHTML = FText;
	//toolTip.innerHTML = htmlText;

	if (typeof FWidth != "undefined")

	{

		toolTip.style.width = FWidth + "px";

		if ((toolTip.scrollWidth ) > FWidth)

			toolTip.style.width = (toolTip.scrollWidth) + "px";

	}

	enableTip = true;

}

return false;

}



function positionTip(e){


userBrowser = browserName();

if (enableTip)

{

	if (userBrowser == "IE" || userBrowser == "Opera")

	{

		curX = event.clientX +  document.body.scrollLeft;

		curY = event.clientY +  document.body.scrollTop;

	}

	else

	{

		curX = e.clientX;

		curY = e.clientY;

		if (userBrowser == "Safari" && parent != self)

		{

			curX -= window.scrollX;

			curY -= window.scrollY;

		}

		else if (userBrowser != "Safari" && parent == self)

		{

			curX += window.scrollX;

			curY += window.scrollY;

		}

	}

	//alert ('cur x = ' + curX + '    cur y = ' + curY);

	tipWidth = toolTip.offsetWidth;

	tipHeight = toolTip.offsetHeight;



	leftEdge = curX + xOffset;

	rightEdge = curX + xOffset + tipWidth;

	topEdge = curY + yOffset;

	bottomEdge = curY + yOffset + tipHeight;



	if (userBrowser == "IE")

	{

		windowWidth =  document.body.clientWidth;

		windowHeight =  document.body.clientHeight;

		if (parent == self)

		{

			windowWidth +=  document.body.scrollLeft;

			windowHeight +=  document.body.scrollTop;

		}

	}

	else

	{

		windowWidth =  window.innerWidth;

		windowHeight =  window.innerHeight;

		if (parent == self)

		{

			windowWidth += window.scrollX;

			windowHeight += window.scrollY;

		}

	}



	if (leftEdge < 0)

		toolTip.style.left = "5px";

	else if (rightEdge > windowWidth)

		toolTip.style.left = (windowWidth - tipWidth - 20) + "px";

	else

		toolTip.style.left = leftEdge + "px";

	if ((parseInt(toolTip.style.left) + tipWidth + 5) < curX)

		toolTip.style.left = (curX - tipWidth - 5) + "px";



	if (bottomEdge > windowHeight)

	{

		if ((curY - yOffset - tipHeight) > 0)

			toolTip.style.top = (curY - yOffset - tipHeight) + "px";

		else

		{

			toolTip.style.top = "0px";

			leftEdge = parseInt(toolTip.style.left);

			rightEdge = leftEdge + tipWidth;

			if (leftEdge <= curX && rightEdge >= curX)

			{

				if ((curX - 5 - tipWidth) > 0)

					toolTip.style.left = (curX - 5 - tipWidth) + "px";

				else

					toolTip.style.left = (curX + 5) + "px";

			}

		}

	}

	else

		toolTip.style.top = topEdge + "px";



	toolTip.style.visibility = "visible";

}

}



function hideTip()

{

if (browserName() == "MacIE")

	return false;

enableTip = false;

toolTip.style.visibility = "hidden";

toolTip.style.left = "-1000px";

toolTip.style.backgroundColor = "";

toolTip.style.width = "";

return false;

}



document.onmousemove = positionTip;
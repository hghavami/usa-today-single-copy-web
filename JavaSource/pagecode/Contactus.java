/*
 * Created on Aug 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode;

import com.usatoday.champion.handlers.MarketsHandler;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import com.ibm.odc.jsf.components.components.rte.UIRichTextEditor;
import com.ibm.faces.component.html.HtmlJspPanel;
import javax.faces.component.html.HtmlPanelGrid;
import com.ibm.faces.component.html.HtmlPanelBox;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.UIColumn;
import javax.faces.component.html.HtmlDataTable;
/**
 * @author aeast
 * @date Aug 2, 2007
 * @class pagecodeContactus
 * 
 * 
 * 
 */
public class Contactus extends PageCodeBase {

    protected MarketsHandler marketsHandler;
    protected HtmlScriptCollector scriptCollector1;
    protected HtmlForm form1;
    protected UIRichTextEditor richTextEditor1;
    protected HtmlJspPanel jspPanel2;
    protected HtmlPanelGrid grid1;
    protected HtmlJspPanel jspPanel1;
    protected HtmlPanelBox box1;
    protected HtmlOutputText text2;
    protected HtmlOutputText text100;
    protected HtmlJspPanel jspPanelRightColumn;
    protected HtmlOutputLinkEx linkEx1;
    protected HtmlOutputText textSendCommentQuestion;
    protected UIColumn column3;
    protected HtmlOutputText text4;
    protected HtmlOutputText text6;
    protected HtmlDataTable tableMarketInfo;
    protected HtmlOutputText text5;
    protected UIColumn column4;
    protected HtmlOutputText text7;
 
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlForm getForm1() {
        if (form1 == null) {
            form1 = (HtmlForm) findComponentInRoot("form1");
        }
        return form1;
    }
    protected UIRichTextEditor getRichTextEditor1() {
        if (richTextEditor1 == null) {
            richTextEditor1 = (UIRichTextEditor) findComponentInRoot("richTextEditor1");
        }
        return richTextEditor1;
    }
    protected HtmlJspPanel getJspPanel2() {
        if (jspPanel2 == null) {
            jspPanel2 = (HtmlJspPanel) findComponentInRoot("jspPanel2");
        }
        return jspPanel2;
    }
    protected HtmlPanelGrid getGrid1() {
        if (grid1 == null) {
            grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
        }
        return grid1;
    }
    protected HtmlJspPanel getJspPanel1() {
        if (jspPanel1 == null) {
            jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
        }
        return jspPanel1;
    }
    protected HtmlPanelBox getBox1() {
        if (box1 == null) {
            box1 = (HtmlPanelBox) findComponentInRoot("box1");
        }
        return box1;
    }
    protected HtmlOutputText getText2() {
        if (text2 == null) {
            text2 = (HtmlOutputText) findComponentInRoot("text2");
        }
        return text2;
    }
    protected HtmlOutputText getText100() {
        if (text100 == null) {
            text100 = (HtmlOutputText) findComponentInRoot("text100");
        }
        return text100;
    }
    protected HtmlJspPanel getJspPanelRightColumn() {
        if (jspPanelRightColumn == null) {
            jspPanelRightColumn = (HtmlJspPanel) findComponentInRoot("jspPanelRightColumn");
        }
        return jspPanelRightColumn;
    }
    protected HtmlOutputLinkEx getLinkEx1() {
        if (linkEx1 == null) {
            linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
        }
        return linkEx1;
    }
    protected HtmlOutputText getTextSendCommentQuestion() {
        if (textSendCommentQuestion == null) {
            textSendCommentQuestion = (HtmlOutputText) findComponentInRoot("textSendCommentQuestion");
        }
        return textSendCommentQuestion;
    }
    protected UIColumn getColumn3() {
        if (column3 == null) {
            column3 = (UIColumn) findComponentInRoot("column3");
        }
        return column3;
    }
    protected HtmlOutputText getText4() {
        if (text4 == null) {
            text4 = (HtmlOutputText) findComponentInRoot("text4");
        }
        return text4;
    }
    protected HtmlOutputText getText6() {
        if (text6 == null) {
            text6 = (HtmlOutputText) findComponentInRoot("text6");
        }
        return text6;
    }
    protected HtmlDataTable getTableMarketInfo() {
        if (tableMarketInfo == null) {
            tableMarketInfo = (HtmlDataTable) findComponentInRoot("tableMarketInfo");
        }
        return tableMarketInfo;
    }
    protected HtmlOutputText getText5() {
        if (text5 == null) {
            text5 = (HtmlOutputText) findComponentInRoot("text5");
        }
        return text5;
    }
    protected UIColumn getColumn4() {
        if (column4 == null) {
            column4 = (UIColumn) findComponentInRoot("column4");
        }
        return column4;
    }
    protected HtmlOutputText getText7() {
        if (text7 == null) {
            text7 = (HtmlOutputText) findComponentInRoot("text7");
        }
        return text7;
    }
	/** 
	 * @managed-bean true
	 */
	protected MarketsHandler getMarketsHandler() {
		if (marketsHandler == null) {
			marketsHandler = (MarketsHandler) getManagedBean("marketsHandler");
		}
		return marketsHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setMarketsHandler(MarketsHandler marketsHandler) {
		this.marketsHandler = marketsHandler;
	}
}
/*
 * Created on Jul 10, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.faq;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlCommandLink;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlOutputSeparator;
import com.ibm.faces.component.html.HtmlJspPanel;
/**
 * @author aeast
 * @date Jul 10, 2007
 * @class faqFaq
 * 
 * 
 * 
 */
public class Faq extends PageCodeBase {

    protected HtmlScriptCollector scriptCollector2;
    protected HtmlForm formLogout;
    protected HtmlOutputFormat formatWelcomeMessage;
    protected HtmlOutputText textLogoutLink;
    protected HtmlCommandLink linkLogout;
    protected HtmlPanelBox QuestionBoxHeader;
    protected HtmlOutputLinkEx linkExBTP;
    protected HtmlOutputLinkEx linkEx2;
    protected HtmlOutputText textQ1H;
    protected HtmlOutputLinkEx linkEx1;
    protected HtmlOutputText textQ2H;
    protected HtmlOutputLinkEx linkExQ3;
    protected HtmlOutputText textQ3H;
    protected HtmlOutputLinkEx linkExQ4;
    protected HtmlOutputText textQ4H;
    protected HtmlOutputSeparator separator1;
    protected HtmlPanelBox boxAnswers;
    protected HtmlOutputText text3;
    protected HtmlOutputText text10;
    protected HtmlOutputText textBTP3;
    protected HtmlOutputText textQ1;
    protected HtmlOutputText textA1;
    protected HtmlOutputLinkEx linkEx3;
    protected HtmlOutputText textQ2;
    protected HtmlOutputText textA2;
    protected HtmlOutputLinkEx linkEx4;
    protected HtmlOutputText textQ3;
    protected HtmlOutputText textA3;
    protected HtmlOutputLinkEx linkExBTP3;
    protected HtmlOutputLinkEx linkExQ5;
    protected HtmlOutputText textQ5H;
    protected HtmlScriptCollector scriptCollector1;
    protected HtmlOutputLinkEx linkEx5;
    protected HtmlOutputText text1;
    protected HtmlForm form1;
    protected HtmlJspPanel jspPanel3;
    protected HtmlJspPanel jspPanel4;
    protected HtmlJspPanel jspPanel5;
    protected HtmlJspPanel jspPanel6;
    protected HtmlOutputText textQ4;
    protected HtmlOutputText textA4;
    protected HtmlOutputText textBTP4;
    protected HtmlOutputLinkEx linkExBTP4;
    protected HtmlJspPanel jspPanel12;
    protected HtmlOutputText textQ5;
    protected HtmlOutputText textA5;
    protected HtmlOutputText textBTP5;
    protected HtmlOutputLinkEx linkExBTP5;
    protected HtmlScriptCollector getScriptCollector2() {
        if (scriptCollector2 == null) {
            scriptCollector2 = (HtmlScriptCollector) findComponentInRoot("scriptCollector2");
        }
        return scriptCollector2;
    }
    protected HtmlForm getFormLogout() {
        if (formLogout == null) {
            formLogout = (HtmlForm) findComponentInRoot("formLogout");
        }
        return formLogout;
    }
    protected HtmlOutputFormat getFormatWelcomeMessage() {
        if (formatWelcomeMessage == null) {
            formatWelcomeMessage = (HtmlOutputFormat) findComponentInRoot("formatWelcomeMessage");
        }
        return formatWelcomeMessage;
    }
    protected HtmlOutputText getTextLogoutLink() {
        if (textLogoutLink == null) {
            textLogoutLink = (HtmlOutputText) findComponentInRoot("textLogoutLink");
        }
        return textLogoutLink;
    }
    protected HtmlCommandLink getLinkLogout() {
        if (linkLogout == null) {
            linkLogout = (HtmlCommandLink) findComponentInRoot("linkLogout");
        }
        return linkLogout;
    }
    protected HtmlOutputLinkEx getLinkExBTP() {
        if (linkExBTP == null) {
            linkExBTP = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP");
        }
        return linkExBTP;
    }
    protected HtmlOutputLinkEx getLinkEx2() {
        if (linkEx2 == null) {
            linkEx2 = (HtmlOutputLinkEx) findComponentInRoot("linkEx2");
        }
        return linkEx2;
    }
    protected HtmlOutputText getTextQ1H() {
        if (textQ1H == null) {
            textQ1H = (HtmlOutputText) findComponentInRoot("textQ1H");
        }
        return textQ1H;
    }
    protected HtmlOutputLinkEx getLinkEx1() {
        if (linkEx1 == null) {
            linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
        }
        return linkEx1;
    }
    protected HtmlOutputText getTextQ2H() {
        if (textQ2H == null) {
            textQ2H = (HtmlOutputText) findComponentInRoot("textQ2H");
        }
        return textQ2H;
    }
    protected HtmlOutputLinkEx getLinkExQ3() {
        if (linkExQ3 == null) {
            linkExQ3 = (HtmlOutputLinkEx) findComponentInRoot("linkExQ3");
        }
        return linkExQ3;
    }
    protected HtmlOutputText getTextQ3H() {
        if (textQ3H == null) {
            textQ3H = (HtmlOutputText) findComponentInRoot("textQ3H");
        }
        return textQ3H;
    }
    protected HtmlOutputLinkEx getLinkExQ4() {
        if (linkExQ4 == null) {
            linkExQ4 = (HtmlOutputLinkEx) findComponentInRoot("linkExQ4");
        }
        return linkExQ4;
    }
    protected HtmlOutputText getTextQ4H() {
        if (textQ4H == null) {
            textQ4H = (HtmlOutputText) findComponentInRoot("textQ4H");
        }
        return textQ4H;
    }
    protected HtmlOutputSeparator getSeparator1() {
        if (separator1 == null) {
            separator1 = (HtmlOutputSeparator) findComponentInRoot("separator1");
        }
        return separator1;
    }
    protected HtmlPanelBox getBoxAnswers() {
        if (boxAnswers == null) {
            boxAnswers = (HtmlPanelBox) findComponentInRoot("boxAnswers");
        }
        return boxAnswers;
    }
    protected HtmlOutputText getText3() {
        if (text3 == null) {
            text3 = (HtmlOutputText) findComponentInRoot("text3");
        }
        return text3;
    }
    protected HtmlOutputText getText10() {
        if (text10 == null) {
            text10 = (HtmlOutputText) findComponentInRoot("text10");
        }
        return text10;
    }
    protected HtmlOutputText getTextBTP3() {
        if (textBTP3 == null) {
            textBTP3 = (HtmlOutputText) findComponentInRoot("textBTP3");
        }
        return textBTP3;
    }
    protected HtmlOutputText getTextQ1() {
        if (textQ1 == null) {
            textQ1 = (HtmlOutputText) findComponentInRoot("textQ1");
        }
        return textQ1;
    }
    protected HtmlOutputText getTextA1() {
        if (textA1 == null) {
            textA1 = (HtmlOutputText) findComponentInRoot("textA1");
        }
        return textA1;
    }
    protected HtmlOutputLinkEx getLinkEx3() {
        if (linkEx3 == null) {
            linkEx3 = (HtmlOutputLinkEx) findComponentInRoot("linkEx3");
        }
        return linkEx3;
    }
    protected HtmlOutputText getTextQ2() {
        if (textQ2 == null) {
            textQ2 = (HtmlOutputText) findComponentInRoot("textQ2");
        }
        return textQ2;
    }
    protected HtmlOutputText getTextA2() {
        if (textA2 == null) {
            textA2 = (HtmlOutputText) findComponentInRoot("textA2");
        }
        return textA2;
    }
    protected HtmlOutputLinkEx getLinkEx4() {
        if (linkEx4 == null) {
            linkEx4 = (HtmlOutputLinkEx) findComponentInRoot("linkEx4");
        }
        return linkEx4;
    }
    protected HtmlOutputText getTextQ3() {
        if (textQ3 == null) {
            textQ3 = (HtmlOutputText) findComponentInRoot("textQ3");
        }
        return textQ3;
    }
    protected HtmlOutputText getTextA3() {
        if (textA3 == null) {
            textA3 = (HtmlOutputText) findComponentInRoot("textA3");
        }
        return textA3;
    }
    protected HtmlOutputLinkEx getLinkExBTP3() {
        if (linkExBTP3 == null) {
            linkExBTP3 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP3");
        }
        return linkExBTP3;
    }
    protected HtmlOutputLinkEx getLinkExQ5() {
        if (linkExQ5 == null) {
            linkExQ5 = (HtmlOutputLinkEx) findComponentInRoot("linkExQ5");
        }
        return linkExQ5;
    }
    protected HtmlOutputText getTextQ5H() {
        if (textQ5H == null) {
            textQ5H = (HtmlOutputText) findComponentInRoot("textQ5H");
        }
        return textQ5H;
    }
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlOutputLinkEx getLinkEx5() {
        if (linkEx5 == null) {
            linkEx5 = (HtmlOutputLinkEx) findComponentInRoot("linkEx5");
        }
        return linkEx5;
    }
    protected HtmlOutputText getText1() {
        if (text1 == null) {
            text1 = (HtmlOutputText) findComponentInRoot("text1");
        }
        return text1;
    }
    protected HtmlForm getForm1() {
        if (form1 == null) {
            form1 = (HtmlForm) findComponentInRoot("form1");
        }
        return form1;
    }
    protected HtmlJspPanel getJspPanel3() {
        if (jspPanel3 == null) {
            jspPanel3 = (HtmlJspPanel) findComponentInRoot("jspPanel3");
        }
        return jspPanel3;
    }
    protected HtmlJspPanel getJspPanel4() {
        if (jspPanel4 == null) {
            jspPanel4 = (HtmlJspPanel) findComponentInRoot("jspPanel4");
        }
        return jspPanel4;
    }
    protected HtmlJspPanel getJspPanel5() {
        if (jspPanel5 == null) {
            jspPanel5 = (HtmlJspPanel) findComponentInRoot("jspPanel5");
        }
        return jspPanel5;
    }
    protected HtmlJspPanel getJspPanel6() {
        if (jspPanel6 == null) {
            jspPanel6 = (HtmlJspPanel) findComponentInRoot("jspPanel6");
        }
        return jspPanel6;
    }
    protected HtmlOutputText getTextQ4() {
        if (textQ4 == null) {
            textQ4 = (HtmlOutputText) findComponentInRoot("textQ4");
        }
        return textQ4;
    }
    protected HtmlOutputText getTextA4() {
        if (textA4 == null) {
            textA4 = (HtmlOutputText) findComponentInRoot("textA4");
        }
        return textA4;
    }
    protected HtmlOutputText getTextBTP4() {
        if (textBTP4 == null) {
            textBTP4 = (HtmlOutputText) findComponentInRoot("textBTP4");
        }
        return textBTP4;
    }
    protected HtmlOutputLinkEx getLinkExBTP4() {
        if (linkExBTP4 == null) {
            linkExBTP4 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP4");
        }
        return linkExBTP4;
    }
    protected HtmlJspPanel getJspPanel12() {
        if (jspPanel12 == null) {
            jspPanel12 = (HtmlJspPanel) findComponentInRoot("jspPanel12");
        }
        return jspPanel12;
    }
    protected HtmlOutputText getTextQ5() {
        if (textQ5 == null) {
            textQ5 = (HtmlOutputText) findComponentInRoot("textQ5");
        }
        return textQ5;
    }
    protected HtmlOutputText getTextA5() {
        if (textA5 == null) {
            textA5 = (HtmlOutputText) findComponentInRoot("textA5");
        }
        return textA5;
    }
    protected HtmlOutputText getTextBTP5() {
        if (textBTP5 == null) {
            textBTP5 = (HtmlOutputText) findComponentInRoot("textBTP5");
        }
        return textBTP5;
    }
    protected HtmlOutputLinkEx getLinkExBTP5() {
        if (linkExBTP5 == null) {
            linkExBTP5 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP5");
        }
        return linkExBTP5;
    }
}
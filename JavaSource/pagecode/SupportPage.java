/*
 * Created on Aug 21, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pagecode;

import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlForm;
/**
 * @author aeast
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SupportPage extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlOutputText text1;
	protected HtmlOutputLinkEx linkExHomeLink;
	protected HtmlOutputText textSCRouteWarningMsg;
	protected HtmlForm form1;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}
	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}
	protected HtmlOutputLinkEx getLinkExHomeLink() {
		if (linkExHomeLink == null) {
			linkExHomeLink = (HtmlOutputLinkEx) findComponentInRoot("linkExHomeLink");
		}
		return linkExHomeLink;
	}
	protected HtmlOutputText getTextSCRouteWarningMsg() {
		if (textSCRouteWarningMsg == null) {
			textSCRouteWarningMsg = (HtmlOutputText) findComponentInRoot("textSCRouteWarningMsg");
		}
		return textSCRouteWarningMsg;
	}
	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}
	public String doButtonProceedToHomeAction() {
		// Type Java code that runs when the component is clicked

		return "success";
	}
	public String doButtonCloseWindowAction() {
		// Type Java code that runs when the component is clicked

		return "success";
	}
}
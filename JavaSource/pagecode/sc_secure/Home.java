/*
 * Created on Jan 19, 2007
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure;

import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlOutputText;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPagerWeb;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.champion.handlers.RouteHandler;
import com.usatoday.champion.handlers.RouteListHandler;
import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.singlecopy.model.bo.drawmngmt.RouteBO;

import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlOutputStatistics;
import com.ibm.faces.component.html.HtmlCommandExRowAction;
import javax.faces.component.UIParameter;
import com.ibm.faces.component.html.HtmlInputHelperKeybind;
import com.usatoday.champion.handlers.MessageHandler;
/**
 * @author aeast
 * @date Jan 19, 2007
 * @class wireframeHome
 * 
 * 
 * 
 */
public class Home extends PageCodeBase {

    protected RouteListHandler routeListHandler;
    protected HtmlCommandLink linkLogout;
    protected HtmlOutputText textLogoutLink;
    protected HtmlPagerWeb webRouteSummary11;
    protected HtmlCommandExButton buttonDoLogoutBtn;
    protected HtmlScriptCollector scriptCollector2;
    protected HtmlForm formLogout;
    protected HtmlOutputFormat formatWelcomeMessage;
    protected HtmlForm formHomePageMainForm;
    protected HtmlScriptCollector scriptCollectorHomePageCollector;
    protected HtmlGraphicImageEx imageExHelpIcon1;
    protected HtmlOutputText textBlueChipDrawText;
    protected HtmlOutputLinkEx linkEx1;
    protected HtmlOutputText text2;
    protected HtmlGraphicImageEx imageExHotelIcon;
	protected HtmlOutputText textBlueChipHomePageTxt;
	protected UserHandler userHandler;
	protected HtmlJspPanel jspPanelRouteSummary;
	protected RouteHandler route;
	protected HtmlJspPanel jspPanelNoRoutesAssigned;
	protected HtmlOutputText textNoRoutesHeader;
	protected HtmlOutputText textNoRoutesMessage;
	protected HtmlJspPanel jspPanelBlueChipPanel;
	protected HtmlJspPanel jspPanelHasRoutesAssigned;
	protected HtmlOutputText text1;
	protected HtmlDataTableEx tableExRouteListDataTable;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText text3;
	protected HtmlPanelBox box1;
	protected HtmlOutputText textDataTableHeaderText;
	protected HtmlOutputText text4;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText text5;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText text6;
	protected UIColumnEx columnEx4;
	protected HtmlOutputText textDataTableMarketIDVal;
	protected HtmlOutputText textRouteListDataTableDistIdVal;
	protected HtmlOutputText textDataTableRouteListRouteIDVal;
	protected HtmlOutputText textDataTableRouteListRteDesVal;
	protected HtmlPanelBox boxDataTableFooter;
	protected HtmlPagerWeb web1;
	protected HtmlOutputStatistics statistics1;
	protected HtmlCommandExRowAction rowAction1;
	protected UIColumnEx columnEx5;
	protected UIParameter param1;
	protected UIParameter param2;
	protected UIParameter param3;
	protected HtmlOutputText textOmnitureTrackingCode1;
	protected HtmlScriptCollector scriptCollectorTemplateScriptCollector1;
	protected HtmlOutputText textGlobalMessageText;
	protected HtmlInputHelperKeybind inputHelperKeybind1;
	protected MessageHandler globalMessage;
	protected HtmlScriptCollector templateScriptCollector;
	protected HtmlCommandLink getLinkLogout() {
        if (linkLogout == null) {
            linkLogout = (HtmlCommandLink) findComponentInRoot("linkLogout");
        }
        return linkLogout;
    }
    protected HtmlOutputText getTextLogoutLink() {
        if (textLogoutLink == null) {
            textLogoutLink = (HtmlOutputText) findComponentInRoot("textLogoutLink");
        }
        return textLogoutLink;
    }
    protected HtmlPagerWeb getWebRouteSummary11() {
        if (webRouteSummary11 == null) {
            webRouteSummary11 = (HtmlPagerWeb) findComponentInRoot("webRouteSummary11");
        }
        return webRouteSummary11;
    }
    protected HtmlCommandExButton getButtonDoLogoutBtn() {
        if (buttonDoLogoutBtn == null) {
            buttonDoLogoutBtn = (HtmlCommandExButton) findComponentInRoot("buttonDoLogoutBtn");
        }
        return buttonDoLogoutBtn;
    }
    protected HtmlScriptCollector getScriptCollector2() {
        if (scriptCollector2 == null) {
            scriptCollector2 = (HtmlScriptCollector) findComponentInRoot("scriptCollector2");
        }
        return scriptCollector2;
    }
    protected HtmlForm getFormLogout() {
        if (formLogout == null) {
            formLogout = (HtmlForm) findComponentInRoot("formLogout");
        }
        return formLogout;
    }
    protected HtmlOutputFormat getFormatWelcomeMessage() {
        if (formatWelcomeMessage == null) {
            formatWelcomeMessage = (HtmlOutputFormat) findComponentInRoot("formatWelcomeMessage");
        }
        return formatWelcomeMessage;
    }
    protected HtmlForm getFormHomePageMainForm() {
        if (formHomePageMainForm == null) {
            formHomePageMainForm = (HtmlForm) findComponentInRoot("formHomePageMainForm");
        }
        return formHomePageMainForm;
    }
    protected HtmlScriptCollector getScriptCollectorHomePageCollector() {
        if (scriptCollectorHomePageCollector == null) {
            scriptCollectorHomePageCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorHomePageCollector");
        }
        return scriptCollectorHomePageCollector;
    }
    protected HtmlGraphicImageEx getImageExHelpIcon1() {
        if (imageExHelpIcon1 == null) {
            imageExHelpIcon1 = (HtmlGraphicImageEx) findComponentInRoot("imageExHelpIcon1");
        }
        return imageExHelpIcon1;
    }
    protected HtmlOutputText getTextBlueChipDrawText() {
        if (textBlueChipDrawText == null) {
            textBlueChipDrawText = (HtmlOutputText) findComponentInRoot("textBlueChipDrawText");
        }
        return textBlueChipDrawText;
    }
    protected HtmlOutputLinkEx getLinkEx1() {
        if (linkEx1 == null) {
            linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
        }
        return linkEx1;
    }
    protected HtmlOutputText getText2() {
        if (text2 == null) {
            text2 = (HtmlOutputText) findComponentInRoot("text2");
        }
        return text2;
    }
    protected HtmlGraphicImageEx getImageExHotelIcon() {
        if (imageExHotelIcon == null) {
            imageExHotelIcon = (HtmlGraphicImageEx) findComponentInRoot("imageExHotelIcon");
        }
        return imageExHotelIcon;
    }
	protected HtmlOutputText getTextBlueChipHomePageTxt() {
		if (textBlueChipHomePageTxt == null) {
			textBlueChipHomePageTxt = (HtmlOutputText) findComponentInRoot("textBlueChipHomePageTxt");
		}
		return textBlueChipHomePageTxt;
	}
	/** 
	 * @managed-bean true
	 */
	protected RouteListHandler getRouteListHandler() {
		if (routeListHandler == null) {
			routeListHandler = (RouteListHandler) getManagedBean("routeListHandler");
		}
		return routeListHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setRouteListHandler(RouteListHandler routeListHandler) {
		this.routeListHandler = routeListHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUserHandler() {
		if (userHandler == null) {
			userHandler = (UserHandler) getManagedBean("userHandler");
		}
		return userHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUserHandler(UserHandler userHandler) {
		this.userHandler = userHandler;
	}
	protected HtmlJspPanel getJspPanelRouteSummary() {
		if (jspPanelRouteSummary == null) {
			jspPanelRouteSummary = (HtmlJspPanel) findComponentInRoot("jspPanelRouteSummary");
		}
		return jspPanelRouteSummary;
	}
	public String doRowAction1Action() {
		// TODO Auto-generated method
		//     Parameters are being passed to the next page automatically.
		//     If that is enough, nothing needs to be done here.
		//     Note that since the view would not be restored for this action, no components can be used in this method.
		//
		//     To get the parameter(s) value for the the selected row use:
		//     Object paramValue = getRequestParam().get("paramName");
		//
		//     Specify the return value (a string) which is used by the navigation map to determine
		//     the next page to display
	
    	@SuppressWarnings("unused")
		int row = getRowAction1().getRowIndex();
		Object routeIDParam = getRowAction1().getRowParameters().get("routeID");
		Object marketIDParam = getRowAction1().getRowParameters().get("marketID");
		Object districtIDParam = getRowAction1().getRowParameters().get("districtID");
		
		String routeIDKey = (String)marketIDParam + (String)districtIDParam + (String)routeIDParam;
		RouteListHandler rlh = this.getRouteListHandler();
		RouteBO r = rlh.getRoute(routeIDKey);
		RouteHandler rh = this.getRoute();
		
		rh.setRoute(r);
	
//		getSessionScope().remove("route");
//		getSessionScope().put("route", rh);

        return "success";
	
	}
	/** 
	 * @managed-bean true
	 */
	protected RouteHandler getRoute() {
		if (route == null) {
			route = (RouteHandler) getManagedBean("route");
		}
		return route;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setRoute(RouteHandler route) {
		this.route = route;
	}
	protected HtmlJspPanel getJspPanelNoRoutesAssigned() {
		if (jspPanelNoRoutesAssigned == null) {
			jspPanelNoRoutesAssigned = (HtmlJspPanel) findComponentInRoot("jspPanelNoRoutesAssigned");
		}
		return jspPanelNoRoutesAssigned;
	}
	protected HtmlOutputText getTextNoRoutesHeader() {
		if (textNoRoutesHeader == null) {
			textNoRoutesHeader = (HtmlOutputText) findComponentInRoot("textNoRoutesHeader");
		}
		return textNoRoutesHeader;
	}
	protected HtmlOutputText getTextNoRoutesMessage() {
		if (textNoRoutesMessage == null) {
			textNoRoutesMessage = (HtmlOutputText) findComponentInRoot("textNoRoutesMessage");
		}
		return textNoRoutesMessage;
	}
	protected HtmlJspPanel getJspPanelBlueChipPanel() {
		if (jspPanelBlueChipPanel == null) {
			jspPanelBlueChipPanel = (HtmlJspPanel) findComponentInRoot("jspPanelBlueChipPanel");
		}
		return jspPanelBlueChipPanel;
	}
	protected HtmlJspPanel getJspPanelHasRoutesAssigned() {
		if (jspPanelHasRoutesAssigned == null) {
			jspPanelHasRoutesAssigned = (HtmlJspPanel) findComponentInRoot("jspPanelHasRoutesAssigned");
		}
		return jspPanelHasRoutesAssigned;
	}
	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}
	protected HtmlDataTableEx getTableExRouteListDataTable() {
		if (tableExRouteListDataTable == null) {
			tableExRouteListDataTable = (HtmlDataTableEx) findComponentInRoot("tableExRouteListDataTable");
		}
		return tableExRouteListDataTable;
	}
	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}
	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}
	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}
	protected HtmlOutputText getTextDataTableHeaderText() {
		if (textDataTableHeaderText == null) {
			textDataTableHeaderText = (HtmlOutputText) findComponentInRoot("textDataTableHeaderText");
		}
		return textDataTableHeaderText;
	}
	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}
	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}
	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}
	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}
	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}
	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}
	protected HtmlOutputText getTextDataTableMarketIDVal() {
		if (textDataTableMarketIDVal == null) {
			textDataTableMarketIDVal = (HtmlOutputText) findComponentInRoot("textDataTableMarketIDVal");
		}
		return textDataTableMarketIDVal;
	}
	protected HtmlOutputText getTextRouteListDataTableDistIdVal() {
		if (textRouteListDataTableDistIdVal == null) {
			textRouteListDataTableDistIdVal = (HtmlOutputText) findComponentInRoot("textRouteListDataTableDistIdVal");
		}
		return textRouteListDataTableDistIdVal;
	}
	protected HtmlOutputText getTextDataTableRouteListRouteIDVal() {
		if (textDataTableRouteListRouteIDVal == null) {
			textDataTableRouteListRouteIDVal = (HtmlOutputText) findComponentInRoot("textDataTableRouteListRouteIDVal");
		}
		return textDataTableRouteListRouteIDVal;
	}
	protected HtmlOutputText getTextDataTableRouteListRteDesVal() {
		if (textDataTableRouteListRteDesVal == null) {
			textDataTableRouteListRteDesVal = (HtmlOutputText) findComponentInRoot("textDataTableRouteListRteDesVal");
		}
		return textDataTableRouteListRteDesVal;
	}
	protected HtmlPanelBox getBoxDataTableFooter() {
		if (boxDataTableFooter == null) {
			boxDataTableFooter = (HtmlPanelBox) findComponentInRoot("boxDataTableFooter");
		}
		return boxDataTableFooter;
	}
	protected HtmlPagerWeb getWeb1() {
		if (web1 == null) {
			web1 = (HtmlPagerWeb) findComponentInRoot("web1");
		}
		return web1;
	}
	protected HtmlOutputStatistics getStatistics1() {
		if (statistics1 == null) {
			statistics1 = (HtmlOutputStatistics) findComponentInRoot("statistics1");
		}
		return statistics1;
	}
	protected HtmlCommandExRowAction getRowAction1() {
		if (rowAction1 == null) {
			rowAction1 = (HtmlCommandExRowAction) findComponentInRoot("rowAction1");
		}
		return rowAction1;
	}
	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}
	protected UIParameter getParam1() {
		if (param1 == null) {
			param1 = (UIParameter) findComponentInRoot("param1");
		}
		return param1;
	}
	protected UIParameter getParam2() {
		if (param2 == null) {
			param2 = (UIParameter) findComponentInRoot("param2");
		}
		return param2;
	}
	protected UIParameter getParam3() {
		if (param3 == null) {
			param3 = (UIParameter) findComponentInRoot("param3");
		}
		return param3;
	}
	protected HtmlOutputText getTextOmnitureTrackingCode1() {
		if (textOmnitureTrackingCode1 == null) {
			textOmnitureTrackingCode1 = (HtmlOutputText) findComponentInRoot("textOmnitureTrackingCode1");
		}
		return textOmnitureTrackingCode1;
	}
	protected HtmlScriptCollector getScriptCollectorTemplateScriptCollector1() {
		if (scriptCollectorTemplateScriptCollector1 == null) {
			scriptCollectorTemplateScriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollectorTemplateScriptCollector1");
		}
		return scriptCollectorTemplateScriptCollector1;
	}
	protected HtmlOutputText getTextGlobalMessageText() {
		if (textGlobalMessageText == null) {
			textGlobalMessageText = (HtmlOutputText) findComponentInRoot("textGlobalMessageText");
		}
		return textGlobalMessageText;
	}
	protected HtmlInputHelperKeybind getInputHelperKeybind1() {
		if (inputHelperKeybind1 == null) {
			inputHelperKeybind1 = (HtmlInputHelperKeybind) findComponentInRoot("inputHelperKeybind1");
		}
		return inputHelperKeybind1;
	}
	/** 
	 * @managed-bean true
	 */
	protected MessageHandler getGlobalMessage() {
		if (globalMessage == null) {
			globalMessage = (MessageHandler) getManagedBean("globalMessage");
		}
		return globalMessage;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setGlobalMessage(MessageHandler globalMessage) {
		this.globalMessage = globalMessage;
	}
	protected HtmlScriptCollector getTemplateScriptCollector() {
		if (templateScriptCollector == null) {
			templateScriptCollector = (HtmlScriptCollector) findComponentInRoot("templateScriptCollector");
		}
		return templateScriptCollector;
	}
}
/*
 * Created on Mar 13, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.bcd;

import java.sql.Timestamp;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;

import org.joda.time.DateTime;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlInputHelperKeybind;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.champion.handlers.CommentQuestionHandler;
import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.singlecopy.model.bo.markets.IncidentBO;
import com.usatoday.singlecopy.model.bo.users.UserBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipLocationIntf;
import com.usatoday.singlecopy.model.interfaces.markets.MarketIntf;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;
import com.usatoday.singlecopy.model.so.MarketNotificationService;
import com.usatoday.singlecopy.model.util.IncidentMarketCache;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;
/**
 * @author aeast
 * @date Mar 13, 2008
 * @class sc_secureCommentquestions
 * 
 * 
 * 
 */
public class Commentquestions extends PageCodeBase {

    protected CommentQuestionHandler feedbackHandler;
    protected HtmlScriptCollector scriptCollector2;
    protected HtmlForm formLogout;
    protected HtmlOutputFormat formatWelcomeMessage;
    protected HtmlScriptCollector scriptCollector1;
    protected HtmlForm formFeedbackForm;
    protected HtmlPanelBox boxOuterListPanel;
    protected HtmlOutputText textFeedbackInstructions;
    protected HtmlCommandExButton buttonDoLogoutBtn;
    protected HtmlInputHelperKeybind inputHelperKeybind1;
    protected HtmlJspPanel jspPanelFormFields;
    protected HtmlMessages messages1;
    protected HtmlOutputText textNameLabel;
    protected HtmlInputText textName;
    protected HtmlMessage message2;
    protected HtmlOutputText textReplyToPhoneLabel;
    protected HtmlInputText textPhoneAreaCode;
    protected HtmlInputText textPhonePrefix;
    protected HtmlInputText textPhoneLineNumber;
    protected HtmlInputText textPhoneExtension;
    protected HtmlOutputText textReplyToEmailLabel;
    protected HtmlInputText textEmailAddress;
    protected HtmlOutputText textResponseRequestedLabel;
    protected HtmlSelectBooleanCheckbox checkboxResponseRequested;
    protected HtmlOutputText textCheckBoxExplain;
    protected HtmlOutputText textCategoryLabel;
    protected HtmlSelectOneMenu menuCategoryChooser;
    protected HtmlOutputText textCommentsLabel;
    protected HtmlInputTextarea textareaUserComments;
    protected HtmlMessage message1;
    protected HtmlSelectOneMenu menuLocationNameChooser;
    protected HtmlCommandExButton buttonSubmitFeedback;
	protected CommentQuestionHandler commentHandler;
	protected UserHandler userHandler;
 
    public String doButtonSubmitFeedbackAction() {
        // Type Java code that runs when the component is clicked

        CommentQuestionHandler cqh = this.getCommentHandler();
        
        String cqhName = cqh.getName();
        String cqhPhoneAreaCode = cqh.getPhoneAreaCode();
        String cqhPhonePrefix = cqh.getPhonePrefix();
        String cqhPhoneLineNumber = cqh.getPhoneLineNumber();
        String cqhPhoneLineExtension = cqh.getPhoneExtension();
        String cqhPhoneNumber = cqhPhonePrefix.trim() + cqhPhoneLineNumber.trim()
                                + cqhPhoneLineExtension.trim();
        String cqhLocationID = cqh.getLocationID();
        String cqhEmailAddress = cqh.getEmailAddress();
        boolean cqhResponseRequested = cqh.isResponseRequested();
        if (cqhResponseRequested) {
            if ((cqhPhoneNumber == null && cqhEmailAddress == null) || 
                    (cqhPhoneNumber.trim().equals("") && cqhEmailAddress.trim().equals(""))) {
                FacesContext context = FacesContext.getCurrentInstance();
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "When response is requested, " +
                        "need either the email address or phone number.", null);
                context.addMessage(null, message);
                return "failure";
            }
        }
        String cqhCategoryChooser = cqh.getCategory();
        String cqhUserComments = cqh.getComments();
        
        // Set up the email for user markets
        UserHandler uh =  this.getUserHandler();
        UserIntf user = uh.getUser();
        UserBO u = (UserBO) user;
        IncidentBO incident = new IncidentBO();
        IncidentMarketCache incidentMarketCache = IncidentMarketCache.getInstance();
        MarketIntf market = null;        
        MarketNotificationService marketNotify = new MarketNotificationService();
        BlueChipLocationIntf blueChipLoc = null;
        try {
            if (cqhLocationID != null) {
                blueChipLoc = u.getBlueChipLocations().get(cqhLocationID);
                market = blueChipLoc.getOwningMarket();
                long marketID = incidentMarketCache.getMarket(market.getID()).getID();
                // Prepare for iQuest incident insert
                incident.setMarketKey(marketID);
            }
            else {
                try {
                    incident.setMarketKey(incidentMarketCache.getMarket("99").getID());
                }
                catch (Exception e) {
                    ;
                }
            }
            
            incident.setNcsSiteKey(7);
            incident.setRefundRequest(false);
            incident.setIncidentType("Web Blue Chip Hotel");
            incident.setIncidentRequestType(cqhCategoryChooser);
            if (blueChipLoc != null) {
                incident.setIncidentLocationName(blueChipLoc.getLocationName());
            }
            Timestamp ts = new Timestamp(System.currentTimeMillis());
            DateTime insertTS = new DateTime(ts.getTime());
            incident.setIncidentDate(insertTS.toDate());
            incident.setUpdatedBy(user.getEmailAddress());
            incident.setEnteredBy(user.getEmailAddress());
            if (blueChipLoc != null) {
                incident.setIncidentAddress(blueChipLoc.getAddress1());
                incident.setCustomerAccountNumber(blueChipLoc.getLocationID());
            } 
            int index = cqhName.indexOf(' ');
            if (index != -1) {
                incident.setCustomerFirstName(cqhName.substring(0,index));
                incident.setCustomerLastName(cqhName.substring(index + 1));
            } else {
                incident.setCustomerFirstName(cqhName.trim());
                incident.setCustomerLastName("");
            }
            incident.setCustomerPhone(cqhPhoneAreaCode + cqhPhonePrefix + 
            cqhPhoneLineNumber + " " + cqhPhoneLineExtension);
            incident.setCustomerEmail(cqhEmailAddress);
            incident.setNotes(cqhUserComments.replaceAll("'", "''"));
            incident.setResponseRequested(cqhResponseRequested);
            // If flagged, populate data for iQuest incident insert
            if (USATApplicationConstants.iQuestUpdate) {                        
                try {
                    incident.save();                        
                } catch (Exception e) {
                    FacesContext context = FacesContext.getCurrentInstance();
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Failed to Save Incident. " + e.getMessage(), null);
                    context.addMessage(null, message);
                }
            }
            if (market != null && market.getEmailAddress() != null && !market.getEmailAddress().trim().equals("")) {                    
                marketNotify.notifyMarket(incident, market);
            }
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Failed to Process Form. " + e.getMessage(), null);
            context.addMessage(null, message);
            return "failure";
        }
        return "success";
    }
    
    
    protected HtmlScriptCollector getScriptCollector2() {
        if (scriptCollector2 == null) {
            scriptCollector2 = (HtmlScriptCollector) findComponentInRoot("scriptCollector2");
        }
        return scriptCollector2;
    }
    protected HtmlForm getFormLogout() {
        if (formLogout == null) {
            formLogout = (HtmlForm) findComponentInRoot("formLogout");
        }
        return formLogout;
    }
    protected HtmlOutputFormat getFormatWelcomeMessage() {
        if (formatWelcomeMessage == null) {
            formatWelcomeMessage = (HtmlOutputFormat) findComponentInRoot("formatWelcomeMessage");
        }
        return formatWelcomeMessage;
    }
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlForm getFormFeedbackForm() {
        if (formFeedbackForm == null) {
            formFeedbackForm = (HtmlForm) findComponentInRoot("formFeedbackForm");
        }
        return formFeedbackForm;
    }
    protected HtmlPanelBox getBoxOuterListPanel() {
        if (boxOuterListPanel == null) {
            boxOuterListPanel = (HtmlPanelBox) findComponentInRoot("boxOuterListPanel");
        }
        return boxOuterListPanel;
    }
    protected HtmlOutputText getTextFeedbackInstructions() {
        if (textFeedbackInstructions == null) {
            textFeedbackInstructions = (HtmlOutputText) findComponentInRoot("textFeedbackInstructions");
        }
        return textFeedbackInstructions;
    }
    protected HtmlCommandExButton getButtonDoLogoutBtn() {
        if (buttonDoLogoutBtn == null) {
            buttonDoLogoutBtn = (HtmlCommandExButton) findComponentInRoot("buttonDoLogoutBtn");
        }
        return buttonDoLogoutBtn;
    }
    protected HtmlInputHelperKeybind getInputHelperKeybind1() {
        if (inputHelperKeybind1 == null) {
            inputHelperKeybind1 = (HtmlInputHelperKeybind) findComponentInRoot("inputHelperKeybind1");
        }
        return inputHelperKeybind1;
    }
    protected HtmlJspPanel getJspPanelFormFields() {
        if (jspPanelFormFields == null) {
            jspPanelFormFields = (HtmlJspPanel) findComponentInRoot("jspPanelFormFields");
        }
        return jspPanelFormFields;
    }
    protected HtmlMessages getMessages1() {
        if (messages1 == null) {
            messages1 = (HtmlMessages) findComponentInRoot("messages1");
        }
        return messages1;
    }
    protected HtmlOutputText getTextNameLabel() {
        if (textNameLabel == null) {
            textNameLabel = (HtmlOutputText) findComponentInRoot("textNameLabel");
        }
        return textNameLabel;
    }
    protected HtmlInputText getTextName() {
        if (textName == null) {
            textName = (HtmlInputText) findComponentInRoot("textName");
        }
        return textName;
    }
    protected HtmlMessage getMessage2() {
        if (message2 == null) {
            message2 = (HtmlMessage) findComponentInRoot("message2");
        }
        return message2;
    }
    protected HtmlOutputText getTextReplyToPhoneLabel() {
        if (textReplyToPhoneLabel == null) {
            textReplyToPhoneLabel = (HtmlOutputText) findComponentInRoot("textReplyToPhoneLabel");
        }
        return textReplyToPhoneLabel;
    }
    protected HtmlInputText getTextPhoneAreaCode() {
        if (textPhoneAreaCode == null) {
            textPhoneAreaCode = (HtmlInputText) findComponentInRoot("textPhoneAreaCode");
        }
        return textPhoneAreaCode;
    }
    protected HtmlInputText getTextPhonePrefix() {
        if (textPhonePrefix == null) {
            textPhonePrefix = (HtmlInputText) findComponentInRoot("textPhonePrefix");
        }
        return textPhonePrefix;
    }
    protected HtmlInputText getTextPhoneLineNumber() {
        if (textPhoneLineNumber == null) {
            textPhoneLineNumber = (HtmlInputText) findComponentInRoot("textPhoneLineNumber");
        }
        return textPhoneLineNumber;
    }
    protected HtmlInputText getTextPhoneExtension() {
        if (textPhoneExtension == null) {
            textPhoneExtension = (HtmlInputText) findComponentInRoot("textPhoneExtension");
        }
        return textPhoneExtension;
    }
    protected HtmlOutputText getTextReplyToEmailLabel() {
        if (textReplyToEmailLabel == null) {
            textReplyToEmailLabel = (HtmlOutputText) findComponentInRoot("textReplyToEmailLabel");
        }
        return textReplyToEmailLabel;
    }
    protected HtmlInputText getTextEmailAddress() {
        if (textEmailAddress == null) {
            textEmailAddress = (HtmlInputText) findComponentInRoot("textEmailAddress");
        }
        return textEmailAddress;
    }
    protected HtmlOutputText getTextResponseRequestedLabel() {
        if (textResponseRequestedLabel == null) {
            textResponseRequestedLabel = (HtmlOutputText) findComponentInRoot("textResponseRequestedLabel");
        }
        return textResponseRequestedLabel;
    }
    protected HtmlSelectBooleanCheckbox getCheckboxResponseRequested() {
        if (checkboxResponseRequested == null) {
            checkboxResponseRequested = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxResponseRequested");
        }
        return checkboxResponseRequested;
    }
    protected HtmlOutputText getTextCheckBoxExplain() {
        if (textCheckBoxExplain == null) {
            textCheckBoxExplain = (HtmlOutputText) findComponentInRoot("textCheckBoxExplain");
        }
        return textCheckBoxExplain;
    }
    protected HtmlOutputText getTextCategoryLabel() {
        if (textCategoryLabel == null) {
            textCategoryLabel = (HtmlOutputText) findComponentInRoot("textCategoryLabel");
        }
        return textCategoryLabel;
    }
    protected HtmlSelectOneMenu getMenuCategoryChooser() {
        if (menuCategoryChooser == null) {
            menuCategoryChooser = (HtmlSelectOneMenu) findComponentInRoot("menuCategoryChooser");
        }
        return menuCategoryChooser;
    }
    protected HtmlOutputText getTextCommentsLabel() {
        if (textCommentsLabel == null) {
            textCommentsLabel = (HtmlOutputText) findComponentInRoot("textCommentsLabel");
        }
        return textCommentsLabel;
    }
    protected HtmlInputTextarea getTextareaUserComments() {
        if (textareaUserComments == null) {
            textareaUserComments = (HtmlInputTextarea) findComponentInRoot("textareaUserComments");
        }
        return textareaUserComments;
    }
    protected HtmlMessage getMessage1() {
        if (message1 == null) {
            message1 = (HtmlMessage) findComponentInRoot("message1");
        }
        return message1;
    }
    protected HtmlSelectOneMenu getMenuLocationNameChooser() {
        if (menuLocationNameChooser == null) {
            menuLocationNameChooser = (HtmlSelectOneMenu) findComponentInRoot("menuLocationNameChooser");
        }
        return menuLocationNameChooser;
    }
    protected HtmlCommandExButton getButtonSubmitFeedback() {
        if (buttonSubmitFeedback == null) {
            buttonSubmitFeedback = (HtmlCommandExButton) findComponentInRoot("buttonSubmitFeedback");
        }
        return buttonSubmitFeedback;
    }


	/** 
	 * @managed-bean true
	 */
	protected CommentQuestionHandler getCommentHandler() {
		if (commentHandler == null) {
			commentHandler = (CommentQuestionHandler) getManagedBean("commentHandler");
		}
		return commentHandler;
	}


	/** 
	 * @managed-bean true
	 */
	protected void setCommentHandler(CommentQuestionHandler commentHandler) {
		this.commentHandler = commentHandler;
	}


	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUserHandler() {
		if (userHandler == null) {
			userHandler = (UserHandler) getManagedBean("userHandler");
		}
		return userHandler;
	}


	/** 
	 * @managed-bean true
	 */
	protected void setUserHandler(UserHandler userHandler) {
		this.userHandler = userHandler;
	}
}
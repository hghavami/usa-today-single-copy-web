/**
 * 
 */
package pagecode.sc_secure.bcd;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import com.usatoday.champion.handlers.UserHandler;
import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.usatoday.champion.handlers.blueChipDraw.BlueChipProductOrderHistoryHandler;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlPanelBox;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.UIColumnEx;
import javax.faces.component.html.HtmlCommandLink;

/**
 * @author aeast
 *
 */
public class Order_history extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorPOHistoryCollector;
	protected HtmlForm formPOHistoryForm;
	protected UserHandler userHandler;
	protected HtmlMessages messagesGlobalErrors;
	protected HtmlGraphicImageEx imageExPrintIcon;
	protected HtmlGraphicImageEx imageExHelpIcon1;
	protected BlueChipProductOrderHistoryHandler blueChipPOHistoryHandler;
	protected HtmlPanelLayout layoutDataLayout1;
	protected HtmlDataTableEx tableExHistoryData;
	protected HtmlPanelBox box2;
	protected HtmlPanelBox box1;
	protected HtmlPanelGrid gridHeaderOuterGrid;
	protected HtmlOutputText text2;
	protected HtmlOutputText text3;
	protected HtmlOutputText text4;
	protected HtmlOutputText text5;
	protected HtmlOutputText text6;
	protected HtmlOutputText text7;
	protected HtmlOutputText text8;
	protected HtmlOutputText textPOWeekHeaderLabel;
	protected UIColumnEx columnExWeekEnding;
	protected HtmlOutputText textWeekEndingDate;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText text10;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText text11;
	protected UIColumnEx columnEx4;
	protected HtmlOutputText text12;
	protected UIColumnEx columnEx5;
	protected HtmlOutputText text13;
	protected UIColumnEx columnEx6;
	protected HtmlOutputText text15;
	protected UIColumnEx columnEx7;
	protected HtmlOutputText text14;
	protected UIColumnEx columnEx8;
	protected HtmlOutputText text16;
	protected HtmlPanelGrid gridLeftPanel1;
	protected HtmlOutputText textLocationName;
	protected HtmlOutputText textLocationAddress1;
	protected HtmlOutputText textLocationPhone;
	protected HtmlOutputText textLocationIDStr;
	protected HtmlOutputText text17;
	protected UIColumnEx columnExWeekTotal;
	protected HtmlOutputText text18;
	protected BlueChipProductOrderHistoryHandler bcPoOrderHistoryHandler;
	protected HtmlOutputText text1;
	protected HtmlOutputText textBackToDailyLinkLabel1;
	protected HtmlCommandLink linkBackToPrevious1;
	protected HtmlScriptCollector getScriptCollectorPOHistoryCollector() {
		if (scriptCollectorPOHistoryCollector == null) {
			scriptCollectorPOHistoryCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorPOHistoryCollector");
		}
		return scriptCollectorPOHistoryCollector;
	}

	protected HtmlForm getFormPOHistoryForm() {
		if (formPOHistoryForm == null) {
			formPOHistoryForm = (HtmlForm) findComponentInRoot("formPOHistoryForm");
		}
		return formPOHistoryForm;
	}

	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUserHandler() {
		if (userHandler == null) {
			userHandler = (UserHandler) getManagedBean("userHandler");
		}
		return userHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUserHandler(UserHandler userHandler) {
		this.userHandler = userHandler;
	}

	protected HtmlMessages getMessagesGlobalErrors() {
		if (messagesGlobalErrors == null) {
			messagesGlobalErrors = (HtmlMessages) findComponentInRoot("messagesGlobalErrors");
		}
		return messagesGlobalErrors;
	}

	protected HtmlGraphicImageEx getImageExPrintIcon() {
		if (imageExPrintIcon == null) {
			imageExPrintIcon = (HtmlGraphicImageEx) findComponentInRoot("imageExPrintIcon");
		}
		return imageExPrintIcon;
	}

	protected HtmlGraphicImageEx getImageExHelpIcon1() {
		if (imageExHelpIcon1 == null) {
			imageExHelpIcon1 = (HtmlGraphicImageEx) findComponentInRoot("imageExHelpIcon1");
		}
		return imageExHelpIcon1;
	}

	/** 
	 * @managed-bean true
	 */
	protected BlueChipProductOrderHistoryHandler getBlueChipPOHistoryHandler() {
		if (blueChipPOHistoryHandler == null) {
			blueChipPOHistoryHandler = (BlueChipProductOrderHistoryHandler) getManagedBean("blueChipPOHistoryHandler");
		}
		return blueChipPOHistoryHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setBlueChipPOHistoryHandler(
			BlueChipProductOrderHistoryHandler blueChipPOHistoryHandler) {
		this.blueChipPOHistoryHandler = blueChipPOHistoryHandler;
	}

	protected HtmlPanelLayout getLayoutDataLayout1() {
		if (layoutDataLayout1 == null) {
			layoutDataLayout1 = (HtmlPanelLayout) findComponentInRoot("layoutDataLayout1");
		}
		return layoutDataLayout1;
	}

	protected HtmlDataTableEx getTableExHistoryData() {
		if (tableExHistoryData == null) {
			tableExHistoryData = (HtmlDataTableEx) findComponentInRoot("tableExHistoryData");
		}
		return tableExHistoryData;
	}

	protected HtmlPanelBox getBox2() {
		if (box2 == null) {
			box2 = (HtmlPanelBox) findComponentInRoot("box2");
		}
		return box2;
	}

	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}

	protected HtmlPanelGrid getGridHeaderOuterGrid() {
		if (gridHeaderOuterGrid == null) {
			gridHeaderOuterGrid = (HtmlPanelGrid) findComponentInRoot("gridHeaderOuterGrid");
		}
		return gridHeaderOuterGrid;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected HtmlOutputText getTextPOWeekHeaderLabel() {
		if (textPOWeekHeaderLabel == null) {
			textPOWeekHeaderLabel = (HtmlOutputText) findComponentInRoot("textPOWeekHeaderLabel");
		}
		return textPOWeekHeaderLabel;
	}

	protected UIColumnEx getColumnExWeekEnding() {
		if (columnExWeekEnding == null) {
			columnExWeekEnding = (UIColumnEx) findComponentInRoot("columnExWeekEnding");
		}
		return columnExWeekEnding;
	}

	protected HtmlOutputText getTextWeekEndingDate() {
		if (textWeekEndingDate == null) {
			textWeekEndingDate = (HtmlOutputText) findComponentInRoot("textWeekEndingDate");
		}
		return textWeekEndingDate;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getText11() {
		if (text11 == null) {
			text11 = (HtmlOutputText) findComponentInRoot("text11");
		}
		return text11;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected HtmlOutputText getText12() {
		if (text12 == null) {
			text12 = (HtmlOutputText) findComponentInRoot("text12");
		}
		return text12;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected HtmlOutputText getText13() {
		if (text13 == null) {
			text13 = (HtmlOutputText) findComponentInRoot("text13");
		}
		return text13;
	}

	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}

	protected HtmlOutputText getText15() {
		if (text15 == null) {
			text15 = (HtmlOutputText) findComponentInRoot("text15");
		}
		return text15;
	}

	protected UIColumnEx getColumnEx7() {
		if (columnEx7 == null) {
			columnEx7 = (UIColumnEx) findComponentInRoot("columnEx7");
		}
		return columnEx7;
	}

	protected HtmlOutputText getText14() {
		if (text14 == null) {
			text14 = (HtmlOutputText) findComponentInRoot("text14");
		}
		return text14;
	}

	protected UIColumnEx getColumnEx8() {
		if (columnEx8 == null) {
			columnEx8 = (UIColumnEx) findComponentInRoot("columnEx8");
		}
		return columnEx8;
	}

	protected HtmlOutputText getText16() {
		if (text16 == null) {
			text16 = (HtmlOutputText) findComponentInRoot("text16");
		}
		return text16;
	}

	protected HtmlPanelGrid getGridLeftPanel1() {
		if (gridLeftPanel1 == null) {
			gridLeftPanel1 = (HtmlPanelGrid) findComponentInRoot("gridLeftPanel1");
		}
		return gridLeftPanel1;
	}

	protected HtmlOutputText getTextLocationName() {
		if (textLocationName == null) {
			textLocationName = (HtmlOutputText) findComponentInRoot("textLocationName");
		}
		return textLocationName;
	}

	protected HtmlOutputText getTextLocationAddress1() {
		if (textLocationAddress1 == null) {
			textLocationAddress1 = (HtmlOutputText) findComponentInRoot("textLocationAddress1");
		}
		return textLocationAddress1;
	}

	protected HtmlOutputText getTextLocationPhone() {
		if (textLocationPhone == null) {
			textLocationPhone = (HtmlOutputText) findComponentInRoot("textLocationPhone");
		}
		return textLocationPhone;
	}

	protected HtmlOutputText getTextLocationIDStr() {
		if (textLocationIDStr == null) {
			textLocationIDStr = (HtmlOutputText) findComponentInRoot("textLocationIDStr");
		}
		return textLocationIDStr;
	}

	protected HtmlOutputText getText17() {
		if (text17 == null) {
			text17 = (HtmlOutputText) findComponentInRoot("text17");
		}
		return text17;
	}

	protected UIColumnEx getColumnExWeekTotal() {
		if (columnExWeekTotal == null) {
			columnExWeekTotal = (UIColumnEx) findComponentInRoot("columnExWeekTotal");
		}
		return columnExWeekTotal;
	}

	protected HtmlOutputText getText18() {
		if (text18 == null) {
			text18 = (HtmlOutputText) findComponentInRoot("text18");
		}
		return text18;
	}

	/** 
	 * @managed-bean true
	 */
	protected BlueChipProductOrderHistoryHandler getBcPoOrderHistoryHandler() {
		if (bcPoOrderHistoryHandler == null) {
			bcPoOrderHistoryHandler = (BlueChipProductOrderHistoryHandler) getManagedBean("bcPoOrderHistoryHandler");
		}
		return bcPoOrderHistoryHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setBcPoOrderHistoryHandler(
			BlueChipProductOrderHistoryHandler bcPoOrderHistoryHandler) {
		this.bcPoOrderHistoryHandler = bcPoOrderHistoryHandler;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getTextBackToDailyLinkLabel1() {
		if (textBackToDailyLinkLabel1 == null) {
			textBackToDailyLinkLabel1 = (HtmlOutputText) findComponentInRoot("textBackToDailyLinkLabel1");
		}
		return textBackToDailyLinkLabel1;
	}

	public String doLinkBackToPreviousAction() {
		// Type Java code that runs when the component is clicked
	
		return "success";
	}

	protected HtmlCommandLink getLinkBackToPrevious1() {
		if (linkBackToPrevious1 == null) {
			linkBackToPrevious1 = (HtmlCommandLink) findComponentInRoot("linkBackToPrevious1");
		}
		return linkBackToPrevious1;
	}

}
/*
 * Created on Mar 14, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.bcd;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlOutputText;
/**
 * @author aeast
 * @date Mar 14, 2008
 * @class bcdQuestioncommentthankyou
 * 
 * 
 * 
 */
public class Commentquestionsthankyou extends PageCodeBase {

    protected HtmlScriptCollector scriptCollector1;
    protected HtmlOutputText CommenQuestionsThankYouText1;
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlOutputText getCommenQuestionsThankYouText1() {
        if (CommenQuestionsThankYouText1 == null) {
            CommenQuestionsThankYouText1 = (HtmlOutputText) findComponentInRoot("Commen");
        }
        return CommenQuestionsThankYouText1;
    }
}
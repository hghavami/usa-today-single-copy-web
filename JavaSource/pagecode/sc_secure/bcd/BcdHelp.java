/*
 * Created on May 13, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.bcd;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
/**
 * @author aeast
 * @date May 13, 2008
 * @class bcdBcdHelp
 * 
 * 
 * 
 */
public class BcdHelp extends PageCodeBase {

	protected HtmlOutputText textDMHelpHeader;
	protected HtmlOutputLinkEx linkExQH2;
	protected HtmlOutputText textQ2H;
	protected HtmlOutputLinkEx linkExQH3;
	protected HtmlOutputText textQ3H;
	protected HtmlOutputLinkEx linkExQH4;
	protected HtmlOutputText textQ4H;
	protected HtmlOutputLinkEx linkExQH5;
	protected HtmlOutputText textQ5H;
	protected HtmlOutputLinkEx linkExQH6;
	protected HtmlOutputText textQ6H;
	protected HtmlOutputLinkEx linkExQH7;
	protected HtmlOutputText textQ7H;
	protected HtmlOutputLinkEx linkExQH8;
	protected HtmlOutputText textQ8H;
	protected HtmlOutputLinkEx linkExQH9;
	protected HtmlOutputText textQ9H;
	protected HtmlOutputLinkEx linkExQH10;
	protected HtmlOutputText textQ10H;
	protected HtmlOutputLinkEx linkExQH11;
	protected HtmlOutputText textQ11H;
	protected HtmlPanelBox boxAnswers;
	protected HtmlOutputText textQ2;
	protected HtmlOutputText textBTP2;
	protected HtmlOutputText textBTP3;
	protected HtmlOutputText textBTP4;
	protected HtmlOutputText textBTP5;
	protected HtmlOutputText textBTP6;
	protected HtmlOutputText textBTP7;
	protected HtmlOutputText textBTP8;
	protected HtmlOutputText textBTP9;
	protected HtmlOutputText textBTP10;
	protected HtmlOutputText textBTP11;
	protected HtmlOutputText textA2;
	protected HtmlOutputLinkEx linkExBTP2;
	protected HtmlOutputText textQ3;
	protected HtmlOutputText textA3;
	protected HtmlOutputLinkEx linkExBTP3;
	protected HtmlOutputText textQ4;
	protected HtmlOutputText textA4;
	protected HtmlOutputLinkEx linkExBTP4;
	protected HtmlOutputText textQ5;
	protected HtmlOutputText textA5;
	protected HtmlOutputLinkEx linkExBTP5;
	protected HtmlOutputText textQ6;
	protected HtmlOutputText textA6;
	protected HtmlOutputLinkEx linkExBTP6;
	protected HtmlOutputText textQ7;
	protected HtmlOutputText textA7;
	protected HtmlOutputLinkEx linkExBTP7;
	protected HtmlOutputText textQ8;
	protected HtmlOutputText textA8;
	protected HtmlOutputLinkEx linkExBTP8;
	protected HtmlOutputText textQ9;
	protected HtmlOutputText textA9;
	protected HtmlOutputLinkEx linkExBTP9;
	protected HtmlOutputText textQ10;
	protected HtmlOutputText textA10;
	protected HtmlOutputLinkEx linkExBTP10;
	protected HtmlOutputText textQ11;
	protected HtmlOutputText textA11;
	protected HtmlOutputLinkEx linkExBTP11;
	protected HtmlOutputLinkEx linkExCloseWindowBottom;
	protected HtmlOutputText textCloseWindowBottom;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlOutputLinkEx linkExCloseWindowTop;
	protected HtmlOutputText textCloseWindowTop;
	protected HtmlOutputText textOmnitureTrackingCode1;

	protected HtmlOutputText getTextDMHelpHeader() {
		if (textDMHelpHeader == null) {
			textDMHelpHeader = (HtmlOutputText) findComponentInRoot("textDMHelpHeader");
		}
		return textDMHelpHeader;
	}

	protected HtmlOutputLinkEx getLinkExQH2() {
		if (linkExQH2 == null) {
			linkExQH2 = (HtmlOutputLinkEx) findComponentInRoot("linkExQH2");
		}
		return linkExQH2;
	}

	protected HtmlOutputText getTextQ2H() {
		if (textQ2H == null) {
			textQ2H = (HtmlOutputText) findComponentInRoot("textQ2H");
		}
		return textQ2H;
	}

	protected HtmlOutputLinkEx getLinkExQH3() {
		if (linkExQH3 == null) {
			linkExQH3 = (HtmlOutputLinkEx) findComponentInRoot("linkExQH3");
		}
		return linkExQH3;
	}

	protected HtmlOutputText getTextQ3H() {
		if (textQ3H == null) {
			textQ3H = (HtmlOutputText) findComponentInRoot("textQ3H");
		}
		return textQ3H;
	}

	protected HtmlOutputLinkEx getLinkExQH4() {
		if (linkExQH4 == null) {
			linkExQH4 = (HtmlOutputLinkEx) findComponentInRoot("linkExQH4");
		}
		return linkExQH4;
	}

	protected HtmlOutputText getTextQ4H() {
		if (textQ4H == null) {
			textQ4H = (HtmlOutputText) findComponentInRoot("textQ4H");
		}
		return textQ4H;
	}

	protected HtmlOutputLinkEx getLinkExQH5() {
		if (linkExQH5 == null) {
			linkExQH5 = (HtmlOutputLinkEx) findComponentInRoot("linkExQH5");
		}
		return linkExQH5;
	}

	protected HtmlOutputText getTextQ5H() {
		if (textQ5H == null) {
			textQ5H = (HtmlOutputText) findComponentInRoot("textQ5H");
		}
		return textQ5H;
	}

	protected HtmlOutputLinkEx getLinkExQH6() {
		if (linkExQH6 == null) {
			linkExQH6 = (HtmlOutputLinkEx) findComponentInRoot("linkExQH6");
		}
		return linkExQH6;
	}

	protected HtmlOutputText getTextQ6H() {
		if (textQ6H == null) {
			textQ6H = (HtmlOutputText) findComponentInRoot("textQ6H");
		}
		return textQ6H;
	}

	protected HtmlOutputLinkEx getLinkExQH7() {
		if (linkExQH7 == null) {
			linkExQH7 = (HtmlOutputLinkEx) findComponentInRoot("linkExQH7");
		}
		return linkExQH7;
	}

	protected HtmlOutputText getTextQ7H() {
		if (textQ7H == null) {
			textQ7H = (HtmlOutputText) findComponentInRoot("textQ7H");
		}
		return textQ7H;
	}

	protected HtmlOutputLinkEx getLinkExQH8() {
		if (linkExQH8 == null) {
			linkExQH8 = (HtmlOutputLinkEx) findComponentInRoot("linkExQH8");
		}
		return linkExQH8;
	}

	protected HtmlOutputText getTextQ8H() {
		if (textQ8H == null) {
			textQ8H = (HtmlOutputText) findComponentInRoot("textQ8H");
		}
		return textQ8H;
	}

	protected HtmlOutputLinkEx getLinkExQH9() {
		if (linkExQH9 == null) {
			linkExQH9 = (HtmlOutputLinkEx) findComponentInRoot("linkExQH9");
		}
		return linkExQH9;
	}

	protected HtmlOutputText getTextQ9H() {
		if (textQ9H == null) {
			textQ9H = (HtmlOutputText) findComponentInRoot("textQ9H");
		}
		return textQ9H;
	}

	protected HtmlOutputLinkEx getLinkExQH10() {
		if (linkExQH10 == null) {
			linkExQH10 = (HtmlOutputLinkEx) findComponentInRoot("linkExQH10");
		}
		return linkExQH10;
	}

	protected HtmlOutputText getTextQ10H() {
		if (textQ10H == null) {
			textQ10H = (HtmlOutputText) findComponentInRoot("textQ10H");
		}
		return textQ10H;
	}

	protected HtmlOutputLinkEx getLinkExQH11() {
		if (linkExQH11 == null) {
			linkExQH11 = (HtmlOutputLinkEx) findComponentInRoot("linkExQH11");
		}
		return linkExQH11;
	}

	protected HtmlOutputText getTextQ11H() {
		if (textQ11H == null) {
			textQ11H = (HtmlOutputText) findComponentInRoot("textQ11H");
		}
		return textQ11H;
	}

	protected HtmlPanelBox getBoxAnswers() {
		if (boxAnswers == null) {
			boxAnswers = (HtmlPanelBox) findComponentInRoot("boxAnswers");
		}
		return boxAnswers;
	}

	protected HtmlOutputText getTextQ2() {
		if (textQ2 == null) {
			textQ2 = (HtmlOutputText) findComponentInRoot("textQ2");
		}
		return textQ2;
	}

	protected HtmlOutputText getTextBTP2() {
		if (textBTP2 == null) {
			textBTP2 = (HtmlOutputText) findComponentInRoot("textBTP2");
		}
		return textBTP2;
	}

	protected HtmlOutputText getTextBTP3() {
		if (textBTP3 == null) {
			textBTP3 = (HtmlOutputText) findComponentInRoot("textBTP3");
		}
		return textBTP3;
	}

	protected HtmlOutputText getTextBTP4() {
		if (textBTP4 == null) {
			textBTP4 = (HtmlOutputText) findComponentInRoot("textBTP4");
		}
		return textBTP4;
	}

	protected HtmlOutputText getTextBTP5() {
		if (textBTP5 == null) {
			textBTP5 = (HtmlOutputText) findComponentInRoot("textBTP5");
		}
		return textBTP5;
	}

	protected HtmlOutputText getTextBTP6() {
		if (textBTP6 == null) {
			textBTP6 = (HtmlOutputText) findComponentInRoot("textBTP6");
		}
		return textBTP6;
	}

	protected HtmlOutputText getTextBTP7() {
		if (textBTP7 == null) {
			textBTP7 = (HtmlOutputText) findComponentInRoot("textBTP7");
		}
		return textBTP7;
	}

	protected HtmlOutputText getTextBTP8() {
		if (textBTP8 == null) {
			textBTP8 = (HtmlOutputText) findComponentInRoot("textBTP8");
		}
		return textBTP8;
	}

	protected HtmlOutputText getTextBTP9() {
		if (textBTP9 == null) {
			textBTP9 = (HtmlOutputText) findComponentInRoot("textBTP9");
		}
		return textBTP9;
	}

	protected HtmlOutputText getTextBTP10() {
		if (textBTP10 == null) {
			textBTP10 = (HtmlOutputText) findComponentInRoot("textBTP10");
		}
		return textBTP10;
	}

	protected HtmlOutputText getTextBTP11() {
		if (textBTP11 == null) {
			textBTP11 = (HtmlOutputText) findComponentInRoot("textBTP11");
		}
		return textBTP11;
	}

	protected HtmlOutputText getTextA2() {
		if (textA2 == null) {
			textA2 = (HtmlOutputText) findComponentInRoot("textA2");
		}
		return textA2;
	}

	protected HtmlOutputLinkEx getLinkExBTP2() {
		if (linkExBTP2 == null) {
			linkExBTP2 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP2");
		}
		return linkExBTP2;
	}

	protected HtmlOutputText getTextQ3() {
		if (textQ3 == null) {
			textQ3 = (HtmlOutputText) findComponentInRoot("textQ3");
		}
		return textQ3;
	}

	protected HtmlOutputText getTextA3() {
		if (textA3 == null) {
			textA3 = (HtmlOutputText) findComponentInRoot("textA3");
		}
		return textA3;
	}

	protected HtmlOutputLinkEx getLinkExBTP3() {
		if (linkExBTP3 == null) {
			linkExBTP3 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP3");
		}
		return linkExBTP3;
	}

	protected HtmlOutputText getTextQ4() {
		if (textQ4 == null) {
			textQ4 = (HtmlOutputText) findComponentInRoot("textQ4");
		}
		return textQ4;
	}

	protected HtmlOutputText getTextA4() {
		if (textA4 == null) {
			textA4 = (HtmlOutputText) findComponentInRoot("textA4");
		}
		return textA4;
	}

	protected HtmlOutputLinkEx getLinkExBTP4() {
		if (linkExBTP4 == null) {
			linkExBTP4 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP4");
		}
		return linkExBTP4;
	}

	protected HtmlOutputText getTextQ5() {
		if (textQ5 == null) {
			textQ5 = (HtmlOutputText) findComponentInRoot("textQ5");
		}
		return textQ5;
	}

	protected HtmlOutputText getTextA5() {
		if (textA5 == null) {
			textA5 = (HtmlOutputText) findComponentInRoot("textA5");
		}
		return textA5;
	}

	protected HtmlOutputLinkEx getLinkExBTP5() {
		if (linkExBTP5 == null) {
			linkExBTP5 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP5");
		}
		return linkExBTP5;
	}

	protected HtmlOutputText getTextQ6() {
		if (textQ6 == null) {
			textQ6 = (HtmlOutputText) findComponentInRoot("textQ6");
		}
		return textQ6;
	}

	protected HtmlOutputText getTextA6() {
		if (textA6 == null) {
			textA6 = (HtmlOutputText) findComponentInRoot("textA6");
		}
		return textA6;
	}

	protected HtmlOutputLinkEx getLinkExBTP6() {
		if (linkExBTP6 == null) {
			linkExBTP6 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP6");
		}
		return linkExBTP6;
	}

	protected HtmlOutputText getTextQ7() {
		if (textQ7 == null) {
			textQ7 = (HtmlOutputText) findComponentInRoot("textQ7");
		}
		return textQ7;
	}

	protected HtmlOutputText getTextA7() {
		if (textA7 == null) {
			textA7 = (HtmlOutputText) findComponentInRoot("textA7");
		}
		return textA7;
	}

	protected HtmlOutputLinkEx getLinkExBTP7() {
		if (linkExBTP7 == null) {
			linkExBTP7 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP7");
		}
		return linkExBTP7;
	}

	protected HtmlOutputText getTextQ8() {
		if (textQ8 == null) {
			textQ8 = (HtmlOutputText) findComponentInRoot("textQ8");
		}
		return textQ8;
	}

	protected HtmlOutputText getTextA8() {
		if (textA8 == null) {
			textA8 = (HtmlOutputText) findComponentInRoot("textA8");
		}
		return textA8;
	}

	protected HtmlOutputLinkEx getLinkExBTP8() {
		if (linkExBTP8 == null) {
			linkExBTP8 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP8");
		}
		return linkExBTP8;
	}

	protected HtmlOutputText getTextQ9() {
		if (textQ9 == null) {
			textQ9 = (HtmlOutputText) findComponentInRoot("textQ9");
		}
		return textQ9;
	}

	protected HtmlOutputText getTextA9() {
		if (textA9 == null) {
			textA9 = (HtmlOutputText) findComponentInRoot("textA9");
		}
		return textA9;
	}

	protected HtmlOutputLinkEx getLinkExBTP9() {
		if (linkExBTP9 == null) {
			linkExBTP9 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP9");
		}
		return linkExBTP9;
	}

	protected HtmlOutputText getTextQ10() {
		if (textQ10 == null) {
			textQ10 = (HtmlOutputText) findComponentInRoot("textQ10");
		}
		return textQ10;
	}

	protected HtmlOutputText getTextA10() {
		if (textA10 == null) {
			textA10 = (HtmlOutputText) findComponentInRoot("textA10");
		}
		return textA10;
	}

	protected HtmlOutputLinkEx getLinkExBTP10() {
		if (linkExBTP10 == null) {
			linkExBTP10 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP10");
		}
		return linkExBTP10;
	}

	protected HtmlOutputText getTextQ11() {
		if (textQ11 == null) {
			textQ11 = (HtmlOutputText) findComponentInRoot("textQ11");
		}
		return textQ11;
	}

	protected HtmlOutputText getTextA11() {
		if (textA11 == null) {
			textA11 = (HtmlOutputText) findComponentInRoot("textA11");
		}
		return textA11;
	}

	protected HtmlOutputLinkEx getLinkExBTP11() {
		if (linkExBTP11 == null) {
			linkExBTP11 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP11");
		}
		return linkExBTP11;
	}

	protected HtmlOutputLinkEx getLinkExCloseWindowBottom() {
		if (linkExCloseWindowBottom == null) {
			linkExCloseWindowBottom = (HtmlOutputLinkEx) findComponentInRoot("linkExCloseWindowBottom");
		}
		return linkExCloseWindowBottom;
	}

	protected HtmlOutputText getTextCloseWindowBottom() {
		if (textCloseWindowBottom == null) {
			textCloseWindowBottom = (HtmlOutputText) findComponentInRoot("textCloseWindowBottom");
		}
		return textCloseWindowBottom;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlOutputLinkEx getLinkExCloseWindowTop() {
		if (linkExCloseWindowTop == null) {
			linkExCloseWindowTop = (HtmlOutputLinkEx) findComponentInRoot("linkExCloseWindowTop");
		}
		return linkExCloseWindowTop;
	}

	protected HtmlOutputText getTextCloseWindowTop() {
		if (textCloseWindowTop == null) {
			textCloseWindowTop = (HtmlOutputText) findComponentInRoot("textCloseWindowTop");
		}
		return textCloseWindowTop;
	}

	protected HtmlOutputText getTextOmnitureTrackingCode1() {
		if (textOmnitureTrackingCode1 == null) {
			textOmnitureTrackingCode1 = (HtmlOutputText) findComponentInRoot("textOmnitureTrackingCode1");
		}
		return textOmnitureTrackingCode1;
	}
}
/*
 * Created on Mar 26, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.bcd;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIColumn;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import pagecode.PageCodeBase;

import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlInputHelperKeybind;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlOutputSeparator;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.champion.handlers.blueChipDraw.BlueChipDrawManagementHandler;
import com.usatoday.champion.handlers.blueChipDraw.BlueChipLocationHandler;
import com.usatoday.champion.handlers.blueChipDraw.BlueChipProductOrderHistoryHandler;
import com.usatoday.champion.handlers.blueChipDraw.BlueChipWeeklyProductOrderHandler;
import com.usatoday.singlecopy.model.bo.blueChip.BlueChipProductOrderUpdaterService;
import com.usatoday.singlecopy.model.bo.users.UserBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipLocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderHistoryIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;
/**
 * @author aeast
 * @date Mar 26, 2008
 * @class bcdCurrentDayDraw
 * 
 * 
 * 
 */
public class DistributionDayDraw extends PageCodeBase {

    protected BlueChipDrawManagementHandler blueChipDrawManagementHandler;
    protected HtmlOutputText textWeekViewLinkLabel;
    protected HtmlCommandLink linkWeekView;
    protected HtmlScriptCollector scriptCollector2;
    protected HtmlForm formLogout;
    protected HtmlOutputFormat formatWelcomeMessage;
    protected HtmlScriptCollector scriptCollectorDistributionDayDraw;
    protected HtmlForm formDistributionDatePOForm;
    protected HtmlCommandExButton buttonDoLogoutBtn;
	protected HtmlGraphicImageEx imageExHelpIcon1;
	protected HtmlMessages messagesGlobalErrors;
	protected HtmlOutputText textRestrictedAccessText;
	protected HtmlOutputText textLoginLinkLabel;
	protected HtmlOutputText textUpdateErrorsMessage;
	protected HtmlOutputLinkEx linkExRestrictedLogIn;
	protected HtmlGraphicImageEx imageExPrintIcon;
	protected UserHandler userHandler;
	protected HtmlOutputText textUnsavedChangesMsg;
	protected UIColumn column2;
	protected HtmlOutputText textHotelLocHeaderLabel;
	protected HtmlJspPanel jspPanel3;
	protected HtmlOutputText textLocationMarketID;
	protected HtmlOutputText textHotelOrderInfoHeaderLabel;
	protected HtmlDataTable table1;
	protected UIColumn column9;
	protected HtmlOutputText textProductName_A;
	protected HtmlOutputText textPODescHeaderLabel;
	protected HtmlOutputText textPOPubHeaderLabel;
	protected HtmlOutputText textPOQtyHeaderLabel;
	protected HtmlInputText textPODraw;
	protected HtmlOutputText textPODeliveryDateHeaderLabel;
	protected HtmlJspPanel jspPanel4;
	protected HtmlGraphicImageEx imageEx1;
	protected HtmlCommandExButton buttonSaveDrawEdits;
	protected HtmlPanelBox box1;
	protected HtmlPanelLayout layout1;
	protected HtmlJspPanel jspPanelCountDownMsg;
	protected HtmlDataTable tableProductOrderData;
	protected HtmlPanelGrid grid2;
	protected HtmlOutputText textLocationIDStr;
	protected HtmlOutputText textLocationName;
	protected HtmlOutputText textLocationAddress1;
	protected HtmlOutputText textLocationPhone;
	protected UIColumn columnOrderInformation;
	protected HtmlJspPanel jspPanelInternalPOData;
	protected HtmlOutputText textProductOrderID_A;
	protected UIColumn column8;
	protected HtmlOutputText textPODescription;
	protected UIColumn column10;
	protected HtmlJspPanel jspPanel1;
	protected HtmlGraphicImageEx imageExViewHist;
	protected UIColumn column11;
	protected HtmlOutputText textPODeliveryDate;
	protected UIColumn column12;
	protected HtmlOutputText textHistoryKey;
	protected HtmlCommandExButton buttonCancelChanges;
	protected HtmlOutputText textNoAccountsAssigned;
	protected HtmlOutputFormat formatDisclaimer;
	protected HtmlInputHidden minutesToStaleData;
	protected HtmlInputHelperKeybind inputHelperKeybind1;
	protected HtmlOutputText text1;
	protected UIColumnEx columnExCutOffTime;
	protected HtmlOutputText textPOCutOffTime;
	protected HtmlScriptCollector templateScriptCollector;
	protected HtmlOutputText textGlobalMessageText;
	protected HtmlPanelGrid gridActionsGrid;
	protected HtmlCommandLink linkEditWeekLink;
	protected HtmlOutputText textEditWeekLabel;
	protected HtmlCommandLink viewHistoryLink;
	protected HtmlOutputText textViewHistoryLabel;
	protected BlueChipWeeklyProductOrderHandler blueChipPOForWeek;
	protected BlueChipProductOrderHistoryHandler blueChipPOHistoryHandler;
	protected HtmlPanelGrid gridSeperateorGrid1;
	protected HtmlOutputSeparator separatorLink1;
	protected BlueChipProductOrderHistoryHandler bcPoOrderHistoryHandler;
	public String doButtonSaveDrawEditsAction() {
        // Type Java code that runs when the component is clicked

        BlueChipDrawManagementHandler bdh = this.getBlueChipDrawManagementHandler();
        
        boolean hasChanges = bdh.getHasUnsavedChanges();
        
        if (bdh.isPastStaleDataThreshold()) {
            // recache data
            bdh.refreshBlueChipLocations();
            if (hasChanges) {
	    		FacesContext context = FacesContext.getCurrentInstance( );
	            FacesMessage facesMsg = 
	                    new FacesMessage(FacesMessage.SEVERITY_INFO, "The order data was successfully refreshed from the database. Notice: Your changes were discarded because the data was no longer valid.", null);
	            
	            context.addMessage(null, facesMsg);
            }
            else {
	    		FacesContext context = FacesContext.getCurrentInstance( );
	            FacesMessage facesMsg = 
	                    new FacesMessage(FacesMessage.SEVERITY_INFO, "The order data was successfully refreshed from the database. ", null);
	            
	            context.addMessage(null, facesMsg);
            }
        }
        else if (hasChanges) {
            // clear any update errros from previous invocations
            bdh.clearUpdateErrors();
            try {
	            BlueChipProductOrderUpdaterService updateService = new BlueChipProductOrderUpdaterService();
	            UserHandler userH = this.getUserHandler();
	            
	            UserBO user = (UserBO) userH.getUser();
	            
	            updateService.saveChangedProductOrders(user);

	            // recache data
	            bdh.refreshBlueChipLocations();
	            
            }
            catch (Exception e) {
    			FacesContext context = FacesContext.getCurrentInstance( );
		        FacesMessage facesMsg = 
		                new FacesMessage(FacesMessage.SEVERITY_INFO, "Failed to Save Changes. " + e.getMessage(), null);
		        
		        context.addMessage(null, facesMsg);
		        return "failure";    		          
            }
    		FacesContext context = FacesContext.getCurrentInstance( );
            FacesMessage facesMsg = 
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "All Changes Saved Successfully. ", null);
            
            context.addMessage(null, facesMsg);
        }
                
        
        return "success";
    }
    protected HtmlOutputText getTextWeekViewLinkLabel() {
        if (textWeekViewLinkLabel == null) {
            textWeekViewLinkLabel = (HtmlOutputText) findComponentInRoot("textWeekViewLinkLabel");
        }
        return textWeekViewLinkLabel;
    }
    protected HtmlCommandLink getLinkWeekView() {
        if (linkWeekView == null) {
            linkWeekView = (HtmlCommandLink) findComponentInRoot("linkWeekView");
        }
        return linkWeekView;
    }
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public String doLinkWeekViewAction() {
        
        
        Map requestParams = this.getRequestParam();
        String locID = (String)requestParams.get("locationID");
        String poID = (String)requestParams.get("poid");
       // String weekHashKey = (String)requestParams.get("weekKey");
        
        BlueChipDrawManagementHandler bcdm = this.getBlueChipDrawManagementHandler();

        if (bcdm.isPastStaleDataThreshold()) {
    		FacesContext context = FacesContext.getCurrentInstance( );
            FacesMessage facesMsg = 
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "The order data was successfully refreshed from the database. Notice: Your changes were discarded because the data was no longer valid.", null);
            
            context.addMessage(null, facesMsg);
            return "failure";
        }
        
        Collection<BlueChipLocationHandler> locations = bcdm.getBlueChipLocations();
        Iterator<BlueChipLocationHandler> itr = locations.iterator();
        
        while (itr.hasNext()) {
            BlueChipLocationHandler locH = itr.next();
            if (locH.getLocationID().equalsIgnoreCase(locID)) {
                BlueChipLocationIntf loc = locH.getLocation();
                BlueChipMultiWeekProductOrderIntf mwpo = (BlueChipMultiWeekProductOrderIntf)loc.getProductOrder(poID);
                BlueChipProductOrderWeekIntf week = mwpo.getNextDistributionDateProductOrderWeek();
                
                if (week != null) {
                    BlueChipWeeklyProductOrderHandler weekHandler = new BlueChipWeeklyProductOrderHandler(week);
                    BlueChipWeeklyProductOrderHandler oldWeekHandler = (BlueChipWeeklyProductOrderHandler)this.getSessionScope().get("blueChipPOForWeek");
                    if (oldWeekHandler != null) {
                        oldWeekHandler.setWpo(null);
                        this.getSessionScope().remove("blueChipPOForWeek");
                    }
                    this.getSessionScope().put("blueChipPOForWeek", weekHandler);
                    return "success";
                }
            }
        }
        
        return "failure";
    }
    public String doButtonCancelChangesAction() {
        // Type Java code that runs when the component is clicked

        BlueChipDrawManagementHandler bcdm = this.getBlueChipDrawManagementHandler();
        

        if (bcdm.getMinutesToStaleData() < 5) {
            bcdm.refreshBlueChipLocations();
        }
        else {
            Collection<BlueChipProductOrderIntf> changedPOs = bcdm.getChangedProductOrders();
	        if (!changedPOs.isEmpty()) {
	            Iterator<BlueChipProductOrderIntf> itr = changedPOs.iterator();
	            while (itr.hasNext()) {
	                BlueChipProductOrderIntf po = itr.next();
	                
	                po.resetToOriginal();
	            }
	        }
        }
        return "success";
    }
    
    protected HtmlScriptCollector getScriptCollector2() {
        if (scriptCollector2 == null) {
            scriptCollector2 = (HtmlScriptCollector) findComponentInRoot("scriptCollector2");
        }
        return scriptCollector2;
    }
    protected HtmlForm getFormLogout() {
        if (formLogout == null) {
            formLogout = (HtmlForm) findComponentInRoot("formLogout");
        }
        return formLogout;
    }
    protected HtmlOutputFormat getFormatWelcomeMessage() {
        if (formatWelcomeMessage == null) {
            formatWelcomeMessage = (HtmlOutputFormat) findComponentInRoot("formatWelcomeMessage");
        }
        return formatWelcomeMessage;
    }
    protected HtmlScriptCollector getScriptCollectorDistributionDayDraw() {
        if (scriptCollectorDistributionDayDraw == null) {
            scriptCollectorDistributionDayDraw = (HtmlScriptCollector) findComponentInRoot("scriptCollectorDistributionDayDraw");
        }
        return scriptCollectorDistributionDayDraw;
    }
    protected HtmlForm getFormDistributionDatePOForm() {
        if (formDistributionDatePOForm == null) {
            formDistributionDatePOForm = (HtmlForm) findComponentInRoot("formDistributionDatePOForm");
        }
        return formDistributionDatePOForm;
    }
    protected HtmlCommandExButton getButtonDoLogoutBtn() {
        if (buttonDoLogoutBtn == null) {
            buttonDoLogoutBtn = (HtmlCommandExButton) findComponentInRoot("buttonDoLogoutBtn");
        }
        return buttonDoLogoutBtn;
    }
    public void onPageLoadBegin(FacesContext facescontext) {

        if (USATApplicationConstants.debug) {
            System.out.println("BC Current Day Draw Page Loading....");
        }
    
    	BlueChipDrawManagementHandler bcdh = this.getBlueChipDrawManagementHandler();
    	
    	boolean hasChanges = bcdh.getHasUnsavedChanges();
    	
    	if (!hasChanges) {
    	    if (bcdh.getMinutesToStaleData() < 2) {
    	        if (USATApplicationConstants.debug) {
    	            System.out.println("Refreshing Blue Chip Data....");
    	        }
    	        bcdh.refreshBlueChipLocations();
	    		FacesContext context = FacesContext.getCurrentInstance( );
	            FacesMessage facesMsg = 
	                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Since no changes have been made, the order data was automatically refreshed from the database.", null);
	            
	            context.addMessage(null, facesMsg);
    	    }
    	}
    	else {
    	    if (bcdh.isPastStaleDataThreshold()) {
	    		FacesContext context = FacesContext.getCurrentInstance( );
	            FacesMessage facesMsg = 
	                    new FacesMessage(FacesMessage.SEVERITY_INFO, "The order data was successfully refreshed from the database. Notice: Your changes were discarded because the data was no longer valid.", null);
	            
	            context.addMessage(null, facesMsg);
    	        
	            bcdh.refreshBlueChipLocations();
    	    }
    	}
    }
    
    public void onPageLoadEnd(FacesContext facescontext) {
}
    public void onPagePost(FacesContext facescontext) {
}
	protected HtmlGraphicImageEx getImageExHelpIcon1() {
		if (imageExHelpIcon1 == null) {
			imageExHelpIcon1 = (HtmlGraphicImageEx) findComponentInRoot("imageExHelpIcon1");
		}
		return imageExHelpIcon1;
	}
	protected HtmlMessages getMessagesGlobalErrors() {
		if (messagesGlobalErrors == null) {
			messagesGlobalErrors = (HtmlMessages) findComponentInRoot("messagesGlobalErrors");
		}
		return messagesGlobalErrors;
	}
	protected HtmlOutputText getTextRestrictedAccessText() {
		if (textRestrictedAccessText == null) {
			textRestrictedAccessText = (HtmlOutputText) findComponentInRoot("textRestrictedAccessText");
		}
		return textRestrictedAccessText;
	}
	protected HtmlOutputText getTextLoginLinkLabel() {
		if (textLoginLinkLabel == null) {
			textLoginLinkLabel = (HtmlOutputText) findComponentInRoot("textLoginLinkLabel");
		}
		return textLoginLinkLabel;
	}
	protected HtmlOutputText getTextUpdateErrorsMessage() {
		if (textUpdateErrorsMessage == null) {
			textUpdateErrorsMessage = (HtmlOutputText) findComponentInRoot("textUpdateErrorsMessage");
		}
		return textUpdateErrorsMessage;
	}
	protected HtmlOutputLinkEx getLinkExRestrictedLogIn() {
		if (linkExRestrictedLogIn == null) {
			linkExRestrictedLogIn = (HtmlOutputLinkEx) findComponentInRoot("linkExRestrictedLogIn");
		}
		return linkExRestrictedLogIn;
	}
	protected HtmlGraphicImageEx getImageExPrintIcon() {
		if (imageExPrintIcon == null) {
			imageExPrintIcon = (HtmlGraphicImageEx) findComponentInRoot("imageExPrintIcon");
		}
		return imageExPrintIcon;
	}
	/** 
	 * @managed-bean true
	 */
	protected BlueChipDrawManagementHandler getBlueChipDrawManagementHandler() {
		if (blueChipDrawManagementHandler == null) {
			blueChipDrawManagementHandler = (BlueChipDrawManagementHandler) getManagedBean("blueChipDrawManagementHandler");
		}
		return blueChipDrawManagementHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setBlueChipDrawManagementHandler(
			BlueChipDrawManagementHandler blueChipDrawManagementHandler) {
		this.blueChipDrawManagementHandler = blueChipDrawManagementHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUserHandler() {
		if (userHandler == null) {
			userHandler = (UserHandler) getManagedBean("userHandler");
		}
		return userHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUserHandler(UserHandler userHandler) {
		this.userHandler = userHandler;
	}
	protected HtmlOutputText getTextUnsavedChangesMsg() {
		if (textUnsavedChangesMsg == null) {
			textUnsavedChangesMsg = (HtmlOutputText) findComponentInRoot("textUnsavedChangesMsg");
		}
		return textUnsavedChangesMsg;
	}
	protected UIColumn getColumn2() {
		if (column2 == null) {
			column2 = (UIColumn) findComponentInRoot("column2");
		}
		return column2;
	}
	protected HtmlOutputText getTextHotelLocHeaderLabel() {
		if (textHotelLocHeaderLabel == null) {
			textHotelLocHeaderLabel = (HtmlOutputText) findComponentInRoot("textHotelLocHeaderLabel");
		}
		return textHotelLocHeaderLabel;
	}
	protected HtmlJspPanel getJspPanel3() {
		if (jspPanel3 == null) {
			jspPanel3 = (HtmlJspPanel) findComponentInRoot("jspPanel3");
		}
		return jspPanel3;
	}
	protected HtmlOutputText getTextLocationMarketID() {
		if (textLocationMarketID == null) {
			textLocationMarketID = (HtmlOutputText) findComponentInRoot("textLocationMarketID");
		}
		return textLocationMarketID;
	}
	protected HtmlOutputText getTextHotelOrderInfoHeaderLabel() {
		if (textHotelOrderInfoHeaderLabel == null) {
			textHotelOrderInfoHeaderLabel = (HtmlOutputText) findComponentInRoot("textHotelOrderInfoHeaderLabel");
		}
		return textHotelOrderInfoHeaderLabel;
	}
	protected HtmlDataTable getTable1() {
		if (table1 == null) {
			table1 = (HtmlDataTable) findComponentInRoot("table1");
		}
		return table1;
	}
	protected UIColumn getColumn9() {
		if (column9 == null) {
			column9 = (UIColumn) findComponentInRoot("column9");
		}
		return column9;
	}
	protected HtmlOutputText getTextProductName_A() {
		if (textProductName_A == null) {
			textProductName_A = (HtmlOutputText) findComponentInRoot("textProductName_A");
		}
		return textProductName_A;
	}
	protected HtmlOutputText getTextPODescHeaderLabel() {
		if (textPODescHeaderLabel == null) {
			textPODescHeaderLabel = (HtmlOutputText) findComponentInRoot("textPODescHeaderLabel");
		}
		return textPODescHeaderLabel;
	}
	protected HtmlOutputText getTextPOPubHeaderLabel() {
		if (textPOPubHeaderLabel == null) {
			textPOPubHeaderLabel = (HtmlOutputText) findComponentInRoot("textPOPubHeaderLabel");
		}
		return textPOPubHeaderLabel;
	}
	protected HtmlOutputText getTextPOQtyHeaderLabel() {
		if (textPOQtyHeaderLabel == null) {
			textPOQtyHeaderLabel = (HtmlOutputText) findComponentInRoot("textPOQtyHeaderLabel");
		}
		return textPOQtyHeaderLabel;
	}
	protected HtmlInputText getTextPODraw() {
		if (textPODraw == null) {
			textPODraw = (HtmlInputText) findComponentInRoot("textPODraw");
		}
		return textPODraw;
	}
	protected HtmlOutputText getTextPODeliveryDateHeaderLabel() {
		if (textPODeliveryDateHeaderLabel == null) {
			textPODeliveryDateHeaderLabel = (HtmlOutputText) findComponentInRoot("textPODeliveryDateHeaderLabel");
		}
		return textPODeliveryDateHeaderLabel;
	}
	protected HtmlJspPanel getJspPanel4() {
		if (jspPanel4 == null) {
			jspPanel4 = (HtmlJspPanel) findComponentInRoot("jspPanel4");
		}
		return jspPanel4;
	}
	protected HtmlGraphicImageEx getImageEx1() {
		if (imageEx1 == null) {
			imageEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageEx1");
		}
		return imageEx1;
	}
	protected HtmlCommandExButton getButtonSaveDrawEdits() {
		if (buttonSaveDrawEdits == null) {
			buttonSaveDrawEdits = (HtmlCommandExButton) findComponentInRoot("buttonSaveDrawEdits");
		}
		return buttonSaveDrawEdits;
	}
	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}
	protected HtmlPanelLayout getLayout1() {
		if (layout1 == null) {
			layout1 = (HtmlPanelLayout) findComponentInRoot("layout1");
		}
		return layout1;
	}
	protected HtmlJspPanel getJspPanelCountDownMsg() {
		if (jspPanelCountDownMsg == null) {
			jspPanelCountDownMsg = (HtmlJspPanel) findComponentInRoot("jspPanelCountDownMsg");
		}
		return jspPanelCountDownMsg;
	}
	protected HtmlDataTable getTableProductOrderData() {
		if (tableProductOrderData == null) {
			tableProductOrderData = (HtmlDataTable) findComponentInRoot("tableProductOrderData");
		}
		return tableProductOrderData;
	}
	protected HtmlPanelGrid getGrid2() {
		if (grid2 == null) {
			grid2 = (HtmlPanelGrid) findComponentInRoot("grid2");
		}
		return grid2;
	}
	protected HtmlOutputText getTextLocationIDStr() {
		if (textLocationIDStr == null) {
			textLocationIDStr = (HtmlOutputText) findComponentInRoot("textLocationIDStr");
		}
		return textLocationIDStr;
	}
	protected HtmlOutputText getTextLocationName() {
		if (textLocationName == null) {
			textLocationName = (HtmlOutputText) findComponentInRoot("textLocationName");
		}
		return textLocationName;
	}
	protected HtmlOutputText getTextLocationAddress1() {
		if (textLocationAddress1 == null) {
			textLocationAddress1 = (HtmlOutputText) findComponentInRoot("textLocationAddress1");
		}
		return textLocationAddress1;
	}
	protected HtmlOutputText getTextLocationPhone() {
		if (textLocationPhone == null) {
			textLocationPhone = (HtmlOutputText) findComponentInRoot("textLocationPhone");
		}
		return textLocationPhone;
	}
	protected UIColumn getColumnOrderInformation() {
		if (columnOrderInformation == null) {
			columnOrderInformation = (UIColumn) findComponentInRoot("columnOrderInformation");
		}
		return columnOrderInformation;
	}
	protected HtmlJspPanel getJspPanelInternalPOData() {
		if (jspPanelInternalPOData == null) {
			jspPanelInternalPOData = (HtmlJspPanel) findComponentInRoot("jspPanelInternalPOData");
		}
		return jspPanelInternalPOData;
	}
	protected HtmlOutputText getTextProductOrderID_A() {
		if (textProductOrderID_A == null) {
			textProductOrderID_A = (HtmlOutputText) findComponentInRoot("textProductOrderID_A");
		}
		return textProductOrderID_A;
	}
	protected UIColumn getColumn8() {
		if (column8 == null) {
			column8 = (UIColumn) findComponentInRoot("column8");
		}
		return column8;
	}
	protected HtmlOutputText getTextPODescription() {
		if (textPODescription == null) {
			textPODescription = (HtmlOutputText) findComponentInRoot("textPODescription");
		}
		return textPODescription;
	}
	protected UIColumn getColumn10() {
		if (column10 == null) {
			column10 = (UIColumn) findComponentInRoot("column10");
		}
		return column10;
	}
	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}
	protected HtmlGraphicImageEx getImageExViewHist() {
		if (imageExViewHist == null) {
			imageExViewHist = (HtmlGraphicImageEx) findComponentInRoot("imageExViewHist");
		}
		return imageExViewHist;
	}
	protected UIColumn getColumn11() {
		if (column11 == null) {
			column11 = (UIColumn) findComponentInRoot("column11");
		}
		return column11;
	}
	protected HtmlOutputText getTextPODeliveryDate() {
		if (textPODeliveryDate == null) {
			textPODeliveryDate = (HtmlOutputText) findComponentInRoot("textPODeliveryDate");
		}
		return textPODeliveryDate;
	}
	protected UIColumn getColumn12() {
		if (column12 == null) {
			column12 = (UIColumn) findComponentInRoot("column12");
		}
		return column12;
	}
	protected HtmlOutputText getTextHistoryKey() {
		if (textHistoryKey == null) {
			textHistoryKey = (HtmlOutputText) findComponentInRoot("textHistoryKey");
		}
		return textHistoryKey;
	}
	protected HtmlCommandExButton getButtonCancelChanges() {
		if (buttonCancelChanges == null) {
			buttonCancelChanges = (HtmlCommandExButton) findComponentInRoot("buttonCancelChanges");
		}
		return buttonCancelChanges;
	}
	protected HtmlOutputText getTextNoAccountsAssigned() {
		if (textNoAccountsAssigned == null) {
			textNoAccountsAssigned = (HtmlOutputText) findComponentInRoot("textNoAccountsAssigned");
		}
		return textNoAccountsAssigned;
	}
	protected HtmlOutputFormat getFormatDisclaimer() {
		if (formatDisclaimer == null) {
			formatDisclaimer = (HtmlOutputFormat) findComponentInRoot("formatDisclaimer");
		}
		return formatDisclaimer;
	}
	protected HtmlInputHidden getMinutesToStaleData() {
		if (minutesToStaleData == null) {
			minutesToStaleData = (HtmlInputHidden) findComponentInRoot("minutesToStaleData");
		}
		return minutesToStaleData;
	}
	protected HtmlInputHelperKeybind getInputHelperKeybind1() {
		if (inputHelperKeybind1 == null) {
			inputHelperKeybind1 = (HtmlInputHelperKeybind) findComponentInRoot("inputHelperKeybind1");
		}
		return inputHelperKeybind1;
	}
	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}
	protected UIColumnEx getColumnExCutOffTime() {
		if (columnExCutOffTime == null) {
			columnExCutOffTime = (UIColumnEx) findComponentInRoot("columnExCutOffTime");
		}
		return columnExCutOffTime;
	}
	protected HtmlOutputText getTextPOCutOffTime() {
		if (textPOCutOffTime == null) {
			textPOCutOffTime = (HtmlOutputText) findComponentInRoot("textPOCutOffTime");
		}
		return textPOCutOffTime;
	}
	protected HtmlScriptCollector getTemplateScriptCollector() {
		if (templateScriptCollector == null) {
			templateScriptCollector = (HtmlScriptCollector) findComponentInRoot("templateScriptCollector");
		}
		return templateScriptCollector;
	}
	protected HtmlOutputText getTextGlobalMessageText() {
		if (textGlobalMessageText == null) {
			textGlobalMessageText = (HtmlOutputText) findComponentInRoot("textGlobalMessageText");
		}
		return textGlobalMessageText;
	}
	protected HtmlPanelGrid getGridActionsGrid() {
		if (gridActionsGrid == null) {
			gridActionsGrid = (HtmlPanelGrid) findComponentInRoot("gridActionsGrid");
		}
		return gridActionsGrid;
	}
	protected HtmlCommandLink getLinkEditWeekLink() {
		if (linkEditWeekLink == null) {
			linkEditWeekLink = (HtmlCommandLink) findComponentInRoot("linkEditWeekLink");
		}
		return linkEditWeekLink;
	}
	protected HtmlOutputText getTextEditWeekLabel() {
		if (textEditWeekLabel == null) {
			textEditWeekLabel = (HtmlOutputText) findComponentInRoot("textEditWeekLabel");
		}
		return textEditWeekLabel;
	}
	protected HtmlCommandLink getViewHistoryLink() {
		if (viewHistoryLink == null) {
			viewHistoryLink = (HtmlCommandLink) findComponentInRoot("viewHistoryLink");
		}
		return viewHistoryLink;
	}
	protected HtmlOutputText getTextViewHistoryLabel() {
		if (textViewHistoryLabel == null) {
			textViewHistoryLabel = (HtmlOutputText) findComponentInRoot("textViewHistoryLabel");
		}
		return textViewHistoryLabel;
	}
	@SuppressWarnings({ "rawtypes" })
	public String doViewHistoryLinkAction() {
        Map requestParams = this.getRequestParam();
        String locID = (String)requestParams.get("locationID");
        String poID = (String)requestParams.get("poid");
        
        BlueChipDrawManagementHandler bcdm = this.getBlueChipDrawManagementHandler();

        Collection<BlueChipLocationHandler> locations = bcdm.getBlueChipLocations();
        Iterator<BlueChipLocationHandler> itr = locations.iterator();
        
        while (itr.hasNext()) {
            BlueChipLocationHandler locH = itr.next();
            if (locH.getLocationID().equalsIgnoreCase(locID)) {
                BlueChipLocationIntf loc = locH.getLocation();
                BlueChipMultiWeekProductOrderIntf mwpo = (BlueChipMultiWeekProductOrderIntf)loc.getProductOrder(poID);
                
                BlueChipProductOrderHistoryHandler historyH = this.getBcPoOrderHistoryHandler();
                
                historyH.setLocation(locH);
                BlueChipProductOrderHistoryIntf history = null;
                try {
                	 history = mwpo.getProductOrderHistory();
                }
                catch (Exception e) {
                	e.printStackTrace();
				}
                
                if (history == null || history.getNumberOfHistoryWeeks() == 0) {
            		// add faces error message
            		FacesContext context = FacesContext.getCurrentInstance( );
                    FacesMessage facesMsg = 
                            new FacesMessage(FacesMessage.SEVERITY_INFO, "No History available for selected Product Order", null);
                    
                    context.addMessage(null, facesMsg);
                    return "failure"; 
                }
                else {
                	historyH.setOwningPO(mwpo);
                	historyH.setHistory(history);
                	
                    FacesContext context = FacesContext.getCurrentInstance();
                    
                    HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
                    request.setAttribute("bcPoOrderHistoryHandler", historyH);
                    
                    return "success";
                }
                    
            }
        }
        
		FacesContext context = FacesContext.getCurrentInstance( );
        FacesMessage facesMsg = 
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Failed to retrieve history for selected Product Order", null);
        
        context.addMessage(null, facesMsg);
        return "failure";
	}
	/** 
	 * @managed-bean true
	 */
	protected BlueChipWeeklyProductOrderHandler getBlueChipPOForWeek() {
		if (blueChipPOForWeek == null) {
			blueChipPOForWeek = (BlueChipWeeklyProductOrderHandler) getManagedBean("blueChipPOForWeek");
		}
		return blueChipPOForWeek;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setBlueChipPOForWeek(
			BlueChipWeeklyProductOrderHandler blueChipPOForWeek) {
		this.blueChipPOForWeek = blueChipPOForWeek;
	}
	
	protected HtmlPanelGrid getGridSeperateorGrid1() {
		if (gridSeperateorGrid1 == null) {
			gridSeperateorGrid1 = (HtmlPanelGrid) findComponentInRoot("gridSeperateorGrid1");
		}
		return gridSeperateorGrid1;
	}
	protected HtmlOutputSeparator getSeparatorLink1() {
		if (separatorLink1 == null) {
			separatorLink1 = (HtmlOutputSeparator) findComponentInRoot("separatorLink1");
		}
		return separatorLink1;
	}
	/** 
	 * @managed-bean true
	 */
	protected BlueChipProductOrderHistoryHandler getBcPoOrderHistoryHandler() {
		if (bcPoOrderHistoryHandler == null) {
			bcPoOrderHistoryHandler = (BlueChipProductOrderHistoryHandler) getManagedBean("bcPoOrderHistoryHandler");
		}
		return bcPoOrderHistoryHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setBcPoOrderHistoryHandler(
			BlueChipProductOrderHistoryHandler bcPoOrderHistoryHandler) {
		this.bcPoOrderHistoryHandler = bcPoOrderHistoryHandler;
	}
}
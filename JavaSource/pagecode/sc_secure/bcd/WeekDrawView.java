/*
 * Created on Mar 24, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.bcd;

import java.util.Collection;
import java.util.Iterator;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlInputHelperKeybind;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.champion.handlers.blueChipDraw.BlueChipDrawManagementHandler;
import com.usatoday.champion.handlers.blueChipDraw.BlueChipWeeklyProductOrderHandler;
import com.usatoday.singlecopy.model.bo.blueChip.BlueChipProductOrderUpdaterService;
import com.usatoday.singlecopy.model.bo.users.UserBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf;
import javax.faces.component.html.HtmlInputText;
/**
 * @author aeast
 * @date Mar 24, 2008
 * @class bcdWeekDrawView
 * 
 * 
 */
public class WeekDrawView extends PageCodeBase {

    protected BlueChipWeeklyProductOrderHandler blueChipPOForWeek;
    protected HtmlScriptCollector scriptCollector2;
    protected HtmlForm formLogout;
    protected HtmlOutputFormat formatWelcomeMessage;
    protected HtmlScriptCollector scriptCollectorBlueChipWeeklyView;
    protected HtmlForm formWeeklyDrawEdits;
    protected HtmlGraphicImageEx imageExHelpIcon1;
    protected HtmlOutputText textUnsavedMsg;
    protected HtmlJspPanel jspPanelMainBody;
    protected HtmlJspPanel jspPanelCountDownMsg;
    protected HtmlOutputText textLocationHeaderLabel;
    protected HtmlOutputText textPOWeekHeaderLabel;
    protected HtmlOutputText textMarketID;
    protected HtmlCommandLink linkPrevWeek;
    protected HtmlOutputText textPrevWeekLabel;
    protected HtmlCommandLink linkNextWeek;
    protected HtmlOutputText text5;
    protected HtmlPanelLayout layoutBottomeLayout;
    protected HtmlCommandLink linkBackToDistDay;
    protected HtmlOutputText textBackToDistDayLabel;
    protected HtmlJspPanel jspPanelRightPanel;
    protected HtmlCommandExButton buttonUpdateDraw;
    protected HtmlJspPanel jspPanelNorthPanel;
    protected HtmlMessages messagesErrorMessages;
    protected HtmlOutputText textUpdateErrorsMessage;
    protected HtmlCommandExButton buttonDoLogoutBtn;
    protected HtmlInputHelperKeybind inputHelperKeybind1;
    protected HtmlPanelGrid gridTopGrid;
    protected HtmlPanelLayout layoutMainLayout;
    protected HtmlOutputText textLocID;
    protected HtmlOutputText text15;
    protected HtmlOutputText text1;
    protected HtmlOutputText text2;
    protected HtmlCommandExButton buttonUndoChanges;
    protected HtmlOutputFormat formatDisclaimer;
    protected HtmlInputHidden minutesToStaleData;
    protected HtmlInputHelperKeybind inputHelperKeybind2;
	protected BlueChipDrawManagementHandler blueChipDrawManagementHandler;
	protected HtmlGraphicImageEx imageExPrintIcon;
	protected HtmlOutputText textMondayDate;
	protected HtmlOutputText textTuesdayDateLabel;
	protected HtmlInputText textTuesdayDraw;
	protected HtmlOutputText textWednesdayDateLabel;
	protected HtmlInputText textWednesdayDraw;
	protected HtmlOutputText textThursdayDateLabel;
	protected HtmlInputText textThursdayDraw;
	protected HtmlOutputText textFridayDateLabel;
	protected HtmlInputText textFridayDraw;
	protected HtmlOutputText textSaturdayDateLabel;
	protected HtmlInputText textSaturdayDraw;
	protected HtmlOutputText textSundayDateLabel;
	protected HtmlInputText textSundayDraw;
	protected HtmlInputText textMondayDraw;
	protected HtmlOutputText text3;
	protected HtmlOutputText text4;
	protected HtmlOutputText text6;
	protected HtmlOutputText text7;
	protected HtmlOutputText text8;
	protected HtmlOutputText text9;
	protected HtmlOutputText text10;
    public String doButtonUpdateDrawAction() {
        // Type Java code that runs when the component is clicked

        BlueChipDrawManagementHandler bdh = (BlueChipDrawManagementHandler)this.getSessionScope().get("blueChipDrawManagementHandler");
        
        if (bdh.isPastStaleDataThreshold()) {
            return "failureExpired";
        }
        boolean hasChanges = bdh.getHasUnsavedChanges();
        if (hasChanges) {
            // clear any update errros from previous invocations
            bdh.clearUpdateErrors();
            
            try {
	            BlueChipProductOrderUpdaterService updateService = new BlueChipProductOrderUpdaterService();
	            UserHandler userH = (UserHandler)this.getSessionScope().get("userHandler");
	            
	            UserBO user = (UserBO) userH.getUser();
	            
	            updateService.saveChangedProductOrders(user);

	            // This forces us to go to the next delivery day view
	            // because the weekly handler gets blown away.
	            // otherwise, need to come up with a good way to reset the weekly view.
	            bdh.refreshBlueChipLocations(); 
            }
            catch (Exception e) {
    			FacesContext context = FacesContext.getCurrentInstance( );
		        FacesMessage facesMsg = 
		                new FacesMessage(FacesMessage.SEVERITY_INFO, "Failed to Save Changes. " + e.getMessage(), null);
		        
		        context.addMessage(null, facesMsg);
		        return "failure";    		          
            }
        }
		FacesContext context = FacesContext.getCurrentInstance( );
        FacesMessage facesMsg = 
                new FacesMessage(FacesMessage.SEVERITY_INFO, "All Changes Saved Successfully. Your view has been reset to the 'Next Delivery Day' View.", null);
        
        context.addMessage(null, facesMsg);
        
        return "success";
    }
    public String doLinkPrevWeekAction() {
        // Type Java code that runs when the component is clicked
        BlueChipWeeklyProductOrderHandler weekHandler = (BlueChipWeeklyProductOrderHandler)this.getSessionScope().get("blueChipPOForWeek");

        BlueChipDrawManagementHandler bdh = (BlueChipDrawManagementHandler)this.getSessionScope().get("blueChipDrawManagementHandler");
        
        if (bdh.isPastStaleDataThreshold()) {
            return "failureExpired";
        }
        
        if (weekHandler == null) {
            // add error message
            // return failure should go to daily draw page
            return "failure";
        }
        
        BlueChipProductOrderWeekIntf cWeek = weekHandler.getWpo();
        
        BlueChipProductOrderWeekIntf prevWeek = cWeek.getOwningMultiWeek().getWeekBefore(cWeek);
        
        if (prevWeek != null) {
            weekHandler.setWpo(prevWeek);
        }
        return "success";
    }
    public String doLinkNextWeekAction1() {
        // Type Java code that runs when the component is clicked
        BlueChipWeeklyProductOrderHandler weekHandler = (BlueChipWeeklyProductOrderHandler)this.getSessionScope().get("blueChipPOForWeek");
        
        BlueChipDrawManagementHandler bdh = this.getBlueChipDrawManagementHandler();
        
        if (bdh.isPastStaleDataThreshold()) {
            return "failureExpired";
        }
        
        if (weekHandler == null) {
            // add error message
            // return failure should go to daily draw page
            return "failure";
        }
        
        BlueChipProductOrderWeekIntf cWeek = weekHandler.getWpo();
        
        BlueChipProductOrderWeekIntf nextWeek = cWeek.getOwningMultiWeek().getWeekAfter(cWeek);
        
        if (nextWeek != null) {
            weekHandler.setWpo(nextWeek);
        }
        return "success";
    }
    public String doButtonUndoChangesAction() {
        // Type Java code that runs when the component is clicked

        BlueChipDrawManagementHandler bcdm = this.getBlueChipDrawManagementHandler();
        
        if (bcdm.isPastStaleDataThreshold()) {
            return "failureExpired";
        }
        else {
	        Collection<BlueChipProductOrderIntf> changedPOs = bcdm.getChangedProductOrders();
	        
	        if (!changedPOs.isEmpty()) {
	            Iterator<BlueChipProductOrderIntf> itr = changedPOs.iterator();
	            while (itr.hasNext()) {
	                BlueChipProductOrderIntf po = itr.next();
	                
	                po.resetToOriginal();
	            }
	        }
        }
        return "success";
    }
    
    public String doLinkBackToDistDayAction() {
        // Type Java code that runs when the component is clicked

        return "success";
    }
    public void onPageLoadBegin(FacesContext facescontext) {
        // Type Java code to handle page load begin event here
    }
    protected HtmlScriptCollector getScriptCollector2() {
        if (scriptCollector2 == null) {
            scriptCollector2 = (HtmlScriptCollector) findComponentInRoot("scriptCollector2");
        }
        return scriptCollector2;
    }
    protected HtmlForm getFormLogout() {
        if (formLogout == null) {
            formLogout = (HtmlForm) findComponentInRoot("formLogout");
        }
        return formLogout;
    }
    protected HtmlOutputFormat getFormatWelcomeMessage() {
        if (formatWelcomeMessage == null) {
            formatWelcomeMessage = (HtmlOutputFormat) findComponentInRoot("formatWelcomeMessage");
        }
        return formatWelcomeMessage;
    }
    protected HtmlScriptCollector getScriptCollectorBlueChipWeeklyView() {
        if (scriptCollectorBlueChipWeeklyView == null) {
            scriptCollectorBlueChipWeeklyView = (HtmlScriptCollector) findComponentInRoot("scriptCollectorBlueChipWeeklyView");
        }
        return scriptCollectorBlueChipWeeklyView;
    }
    protected HtmlForm getFormWeeklyDrawEdits() {
        if (formWeeklyDrawEdits == null) {
            formWeeklyDrawEdits = (HtmlForm) findComponentInRoot("formWeeklyDrawEdits");
        }
        return formWeeklyDrawEdits;
    }
    protected HtmlGraphicImageEx getImageExHelpIcon1() {
        if (imageExHelpIcon1 == null) {
            imageExHelpIcon1 = (HtmlGraphicImageEx) findComponentInRoot("imageExHelpIcon1");
        }
        return imageExHelpIcon1;
    }
    protected HtmlOutputText getTextUnsavedMsg() {
        if (textUnsavedMsg == null) {
            textUnsavedMsg = (HtmlOutputText) findComponentInRoot("textUnsavedMsg");
        }
        return textUnsavedMsg;
    }
    protected HtmlJspPanel getJspPanelMainBody() {
        if (jspPanelMainBody == null) {
            jspPanelMainBody = (HtmlJspPanel) findComponentInRoot("jspPanelMainBody");
        }
        return jspPanelMainBody;
    }
    protected HtmlJspPanel getJspPanelCountDownMsg() {
        if (jspPanelCountDownMsg == null) {
            jspPanelCountDownMsg = (HtmlJspPanel) findComponentInRoot("jspPanelCountDownMsg");
        }
        return jspPanelCountDownMsg;
    }
    protected HtmlOutputText getTextLocationHeaderLabel() {
        if (textLocationHeaderLabel == null) {
            textLocationHeaderLabel = (HtmlOutputText) findComponentInRoot("textLocationHeaderLabel");
        }
        return textLocationHeaderLabel;
    }
    protected HtmlOutputText getTextPOWeekHeaderLabel() {
        if (textPOWeekHeaderLabel == null) {
            textPOWeekHeaderLabel = (HtmlOutputText) findComponentInRoot("textPOWeekHeaderLabel");
        }
        return textPOWeekHeaderLabel;
    }
    protected HtmlOutputText getTextMarketID() {
        if (textMarketID == null) {
            textMarketID = (HtmlOutputText) findComponentInRoot("textMarketID");
        }
        return textMarketID;
    }
    protected HtmlCommandLink getLinkPrevWeek() {
        if (linkPrevWeek == null) {
            linkPrevWeek = (HtmlCommandLink) findComponentInRoot("linkPrevWeek");
        }
        return linkPrevWeek;
    }
    protected HtmlOutputText getTextPrevWeekLabel() {
        if (textPrevWeekLabel == null) {
            textPrevWeekLabel = (HtmlOutputText) findComponentInRoot("textPrevWeekLabel");
        }
        return textPrevWeekLabel;
    }
    protected HtmlCommandLink getLinkNextWeek() {
        if (linkNextWeek == null) {
            linkNextWeek = (HtmlCommandLink) findComponentInRoot("linkNextWeek");
        }
        return linkNextWeek;
    }
    protected HtmlOutputText getText5() {
        if (text5 == null) {
            text5 = (HtmlOutputText) findComponentInRoot("text5");
        }
        return text5;
    }
    protected HtmlPanelLayout getLayoutBottomeLayout() {
        if (layoutBottomeLayout == null) {
            layoutBottomeLayout = (HtmlPanelLayout) findComponentInRoot("layoutBottomeLayout");
        }
        return layoutBottomeLayout;
    }
    protected HtmlCommandLink getLinkBackToDistDay() {
        if (linkBackToDistDay == null) {
            linkBackToDistDay = (HtmlCommandLink) findComponentInRoot("linkBackToDistDay");
        }
        return linkBackToDistDay;
    }
    protected HtmlOutputText getTextBackToDistDayLabel() {
        if (textBackToDistDayLabel == null) {
            textBackToDistDayLabel = (HtmlOutputText) findComponentInRoot("textBackToDistDayLabel");
        }
        return textBackToDistDayLabel;
    }
    protected HtmlJspPanel getJspPanelRightPanel() {
        if (jspPanelRightPanel == null) {
            jspPanelRightPanel = (HtmlJspPanel) findComponentInRoot("jspPanelRightPanel");
        }
        return jspPanelRightPanel;
    }
    protected HtmlCommandExButton getButtonUpdateDraw() {
        if (buttonUpdateDraw == null) {
            buttonUpdateDraw = (HtmlCommandExButton) findComponentInRoot("buttonUpdateDraw");
        }
        return buttonUpdateDraw;
    }
    protected HtmlJspPanel getJspPanelNorthPanel() {
        if (jspPanelNorthPanel == null) {
            jspPanelNorthPanel = (HtmlJspPanel) findComponentInRoot("jspPanelNorthPanel");
        }
        return jspPanelNorthPanel;
    }
    protected HtmlMessages getMessagesErrorMessages() {
        if (messagesErrorMessages == null) {
            messagesErrorMessages = (HtmlMessages) findComponentInRoot("messagesErrorMessages");
        }
        return messagesErrorMessages;
    }
    protected HtmlOutputText getTextUpdateErrorsMessage() {
        if (textUpdateErrorsMessage == null) {
            textUpdateErrorsMessage = (HtmlOutputText) findComponentInRoot("textUpdateErrorsMessage");
        }
        return textUpdateErrorsMessage;
    }
    protected HtmlCommandExButton getButtonDoLogoutBtn() {
        if (buttonDoLogoutBtn == null) {
            buttonDoLogoutBtn = (HtmlCommandExButton) findComponentInRoot("buttonDoLogoutBtn");
        }
        return buttonDoLogoutBtn;
    }
    protected HtmlInputHelperKeybind getInputHelperKeybind1() {
        if (inputHelperKeybind1 == null) {
            inputHelperKeybind1 = (HtmlInputHelperKeybind) findComponentInRoot("inputHelperKeybind1");
        }
        return inputHelperKeybind1;
    }
    protected HtmlPanelGrid getGridTopGrid() {
        if (gridTopGrid == null) {
            gridTopGrid = (HtmlPanelGrid) findComponentInRoot("gridTopGrid");
        }
        return gridTopGrid;
    }
    protected HtmlPanelLayout getLayoutMainLayout() {
        if (layoutMainLayout == null) {
            layoutMainLayout = (HtmlPanelLayout) findComponentInRoot("layoutMainLayout");
        }
        return layoutMainLayout;
    }
    protected HtmlOutputText getTextLocID() {
        if (textLocID == null) {
            textLocID = (HtmlOutputText) findComponentInRoot("textLocID");
        }
        return textLocID;
    }
    protected HtmlOutputText getText15() {
        if (text15 == null) {
            text15 = (HtmlOutputText) findComponentInRoot("text15");
        }
        return text15;
    }
    protected HtmlOutputText getText1() {
        if (text1 == null) {
            text1 = (HtmlOutputText) findComponentInRoot("text1");
        }
        return text1;
    }
    protected HtmlOutputText getText2() {
        if (text2 == null) {
            text2 = (HtmlOutputText) findComponentInRoot("text2");
        }
        return text2;
    }
    protected HtmlCommandExButton getButtonUndoChanges() {
        if (buttonUndoChanges == null) {
            buttonUndoChanges = (HtmlCommandExButton) findComponentInRoot("buttonUndoChanges");
        }
        return buttonUndoChanges;
    }
    protected HtmlOutputFormat getFormatDisclaimer() {
        if (formatDisclaimer == null) {
            formatDisclaimer = (HtmlOutputFormat) findComponentInRoot("formatDisclaimer");
        }
        return formatDisclaimer;
    }
    protected HtmlInputHidden getMinutesToStaleData() {
        if (minutesToStaleData == null) {
            minutesToStaleData = (HtmlInputHidden) findComponentInRoot("minutesToStaleData");
        }
        return minutesToStaleData;
    }
    protected HtmlInputHelperKeybind getInputHelperKeybind2() {
        if (inputHelperKeybind2 == null) {
            inputHelperKeybind2 = (HtmlInputHelperKeybind) findComponentInRoot("inputHelperKeybind2");
        }
        return inputHelperKeybind2;
    }
	/** 
	 * @managed-bean true
	 */
	protected BlueChipWeeklyProductOrderHandler getBlueChipPOForWeek() {
		if (blueChipPOForWeek == null) {
			blueChipPOForWeek = (BlueChipWeeklyProductOrderHandler) getManagedBean("blueChipPOForWeek");
		}
		return blueChipPOForWeek;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setBlueChipPOForWeek(
			BlueChipWeeklyProductOrderHandler blueChipPOForWeek) {
		this.blueChipPOForWeek = blueChipPOForWeek;
	}
	/** 
	 * @managed-bean true
	 */
	protected BlueChipDrawManagementHandler getBlueChipDrawManagementHandler() {
		if (blueChipDrawManagementHandler == null) {
			blueChipDrawManagementHandler = (BlueChipDrawManagementHandler) getManagedBean("blueChipDrawManagementHandler");
		}
		return blueChipDrawManagementHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setBlueChipDrawManagementHandler(
			BlueChipDrawManagementHandler blueChipDrawManagementHandler) {
		this.blueChipDrawManagementHandler = blueChipDrawManagementHandler;
	}
	protected HtmlGraphicImageEx getImageExPrintIcon() {
		if (imageExPrintIcon == null) {
			imageExPrintIcon = (HtmlGraphicImageEx) findComponentInRoot("imageExPrintIcon");
		}
		return imageExPrintIcon;
	}
	protected HtmlOutputText getTextMondayDate() {
		if (textMondayDate == null) {
			textMondayDate = (HtmlOutputText) findComponentInRoot("textMondayDate");
		}
		return textMondayDate;
	}
	protected HtmlOutputText getTextTuesdayDateLabel() {
		if (textTuesdayDateLabel == null) {
			textTuesdayDateLabel = (HtmlOutputText) findComponentInRoot("textTuesdayDateLabel");
		}
		return textTuesdayDateLabel;
	}
	protected HtmlInputText getTextTuesdayDraw() {
		if (textTuesdayDraw == null) {
			textTuesdayDraw = (HtmlInputText) findComponentInRoot("textTuesdayDraw");
		}
		return textTuesdayDraw;
	}
	protected HtmlOutputText getTextWednesdayDateLabel() {
		if (textWednesdayDateLabel == null) {
			textWednesdayDateLabel = (HtmlOutputText) findComponentInRoot("textWednesdayDateLabel");
		}
		return textWednesdayDateLabel;
	}
	protected HtmlInputText getTextWednesdayDraw() {
		if (textWednesdayDraw == null) {
			textWednesdayDraw = (HtmlInputText) findComponentInRoot("textWednesdayDraw");
		}
		return textWednesdayDraw;
	}
	protected HtmlOutputText getTextThursdayDateLabel() {
		if (textThursdayDateLabel == null) {
			textThursdayDateLabel = (HtmlOutputText) findComponentInRoot("textThursdayDateLabel");
		}
		return textThursdayDateLabel;
	}
	protected HtmlInputText getTextThursdayDraw() {
		if (textThursdayDraw == null) {
			textThursdayDraw = (HtmlInputText) findComponentInRoot("textThursdayDraw");
		}
		return textThursdayDraw;
	}
	protected HtmlOutputText getTextFridayDateLabel() {
		if (textFridayDateLabel == null) {
			textFridayDateLabel = (HtmlOutputText) findComponentInRoot("textFridayDateLabel");
		}
		return textFridayDateLabel;
	}
	protected HtmlInputText getTextFridayDraw() {
		if (textFridayDraw == null) {
			textFridayDraw = (HtmlInputText) findComponentInRoot("textFridayDraw");
		}
		return textFridayDraw;
	}
	protected HtmlOutputText getTextSaturdayDateLabel() {
		if (textSaturdayDateLabel == null) {
			textSaturdayDateLabel = (HtmlOutputText) findComponentInRoot("textSaturdayDateLabel");
		}
		return textSaturdayDateLabel;
	}
	protected HtmlInputText getTextSaturdayDraw() {
		if (textSaturdayDraw == null) {
			textSaturdayDraw = (HtmlInputText) findComponentInRoot("textSaturdayDraw");
		}
		return textSaturdayDraw;
	}
	protected HtmlOutputText getTextSundayDateLabel() {
		if (textSundayDateLabel == null) {
			textSundayDateLabel = (HtmlOutputText) findComponentInRoot("textSundayDateLabel");
		}
		return textSundayDateLabel;
	}
	protected HtmlInputText getTextSundayDraw() {
		if (textSundayDraw == null) {
			textSundayDraw = (HtmlInputText) findComponentInRoot("textSundayDraw");
		}
		return textSundayDraw;
	}
	protected HtmlInputText getTextMondayDraw() {
		if (textMondayDraw == null) {
			textMondayDraw = (HtmlInputText) findComponentInRoot("textMondayDraw");
		}
		return textMondayDraw;
	}
	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}
	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}
	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}
	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}
	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}
	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}
	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}
}
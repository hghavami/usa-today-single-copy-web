/*
 * Created on Feb 19, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.profile;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlInputHelperKeybind;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.singlecopy.model.interfaces.users.AccessRightsIntf;

/**
 * @author aeast
 * @date Feb 19, 2007
 * @class profileMy_profile
 * 
 * 
 * 
 */
public class My_profile extends PageCodeBase {

    protected HtmlScriptCollector scriptCollector2;
    protected HtmlForm formLogout;
    protected HtmlOutputFormat formatWelcomeMessage;
    protected HtmlScriptCollector scriptCollector1;
    protected HtmlForm formUpdateProfile;
    protected HtmlOutputText textProfileUpdateInstructions;
    protected HtmlOutputText textUpdateProfileInstructionL2;
    protected HtmlOutputText textUpdateProfileInstructionL3;
    protected HtmlMessages messagesGeneralUpdateProfileErrors;
    protected HtmlOutputText textUpdateEmailAddressHeader;
    protected HtmlOutputText textCurrentEmailAddress;
    protected HtmlOutputText textCurrentEmailOutputText;
    protected HtmlOutputText textNewEmailAddress;
    protected HtmlInputText text_inputNewEmail;
    protected HtmlMessage message1;
    protected HtmlOutputText textConfirmNewEmail;
    protected HtmlInputText text_inputConfirmNewEmail;
    protected HtmlMessage message2;
    protected HtmlOutputText textUpdatePasswordHeader;
    protected HtmlOutputText textPasswordRules;
    protected HtmlOutputText textNewPassword;
    protected HtmlInputSecret secret_inputNewPassword;
    protected HtmlMessage message3;
    protected HtmlOutputText textConfirmNewPassword;
    protected HtmlInputSecret secret_inputConfirmNewPassword;
    protected HtmlMessage message4;
    protected HtmlJspPanel jspPanelBlueChipEmailReminders;
    protected HtmlSelectOneRadio radioReminderOptIn;
    protected HtmlSelectOneRadio radioEmailTime;
    protected HtmlCommandExButton buttonDoLogoutBtn;
    protected HtmlInputHelperKeybind inputHelperKeybind1;
    protected HtmlInputHidden reminderOptInHidden;
    protected HtmlCommandExButton buttonSubmitProfileUpdate;
	protected UserHandler userHandler;
	protected HtmlOutputText text1;
	protected HtmlJspPanel jspPanelEmailHeader;
    public String doButtonSubmitProfileUpdateAction() {
        // Type Java code that runs when the component is clicked
    	UserHandler uh = this.getUserHandler();
    	String newUid = getText_inputNewEmail().getValue().toString();
    	String confirmUid = getText_inputConfirmNewEmail().getValue().toString();
    	String newPwd = getSecret_inputNewPassword().getValue().toString();
    	String confirmPwd = getSecret_inputConfirmNewPassword().getValue().toString();
    	
    	String nullTest = null;
    	
    	if (newUid == null ||  newUid.equals("") || newUid.trim().length() == 0) {
    		if (newPwd == null ||  newPwd.equals("") || newPwd.trim().length() == 0) {
    		    if (confirmUid != null && confirmUid.trim().length() > 0) {
	    			FacesContext context = FacesContext.getCurrentInstance( );
			        FacesMessage facesMsg = 
			                new FacesMessage(FacesMessage.SEVERITY_INFO, "Please specify the new email address and then retype it into the confirmation email field.", null);
			        context.addMessage(getText_inputNewEmail().getClientId(context), facesMsg);
			        return "failure";    		          
    		    }
    		    else if (confirmPwd != null && confirmPwd.trim().length() > 0) {
	    			FacesContext context = FacesContext.getCurrentInstance();
			        FacesMessage facesMsg = 
			                new FacesMessage(FacesMessage.SEVERITY_INFO, "Please specify the new password and then retype it in the password confirmation field.", null);
			        context.addMessage(this.getSecret_inputNewPassword().getClientId(context), facesMsg);
			        return "failure";    		          
    		    }
    		    else if ((uh.getUser().getAccessRight(AccessRightsIntf.BLUE_CHIP_DRAW) == null) || (!uh.getUser().getAccessRight(AccessRightsIntf.BLUE_CHIP_DRAW).isChanged())){
	    			FacesContext context = FacesContext.getCurrentInstance( );
			        FacesMessage facesMsg = 
			                new FacesMessage(FacesMessage.SEVERITY_INFO, "No changes detected.", null);
			        context.addMessage(null, facesMsg);
			        return "failure";    		          
    		    }
    		}
    	} else {
    		try {
    			@SuppressWarnings("unused")
				javax.mail.internet.InternetAddress address = new javax.mail.internet.InternetAddress(newUid);
    			if (!newUid.equals(confirmUid)) {
    	    		FacesContext context = FacesContext.getCurrentInstance( );
    		        FacesMessage facesMsg = 
    		                new FacesMessage(FacesMessage.SEVERITY_INFO, "New Email and Confirm New Email addresses do not match.", null);
    		        context.addMessage(getText_inputNewEmail().getClientId(context), facesMsg);
    		        return "failure";    		          
    	        }
    		} 
    		catch (javax.mail.internet.AddressException e) {   
    			FacesContext context = FacesContext.getCurrentInstance( );
    			FacesMessage facesMsg = 
    				new FacesMessage(FacesMessage.SEVERITY_INFO, "The email address entered is not a valid email address format.", e.getLocalizedMessage());
    			context.addMessage(getText_inputNewEmail().getClientId(context), facesMsg);
    			return "failure";
    		}       
    	}
    	if (newPwd.equals(nullTest) ||  newPwd.equals("") || newPwd.trim().length() == 0) {
        		;
        } else {
        		if (!newPwd.equals(confirmPwd)) {
        	    	   FacesContext context = FacesContext.getCurrentInstance( );     		
        	           FacesMessage facesMsg = 
        		                new FacesMessage(FacesMessage.SEVERITY_INFO, "New Password and Confirm New Password values do not match.", null);
        		        context.addMessage(this.getSecret_inputNewPassword().getClientId(context), facesMsg);
        	            return "failure";
        	     }
        }
    	try {
    	    uh.saveUser(newUid,newPwd);
        	FacesContext context = FacesContext.getCurrentInstance( );     		
            FacesMessage facesMsg = 
    	                new FacesMessage(FacesMessage.SEVERITY_INFO, "Your Profile has been successfully updated. ", null);
	        context.addMessage(null, facesMsg);
    	}
    	catch (Exception e) {
        	FacesContext context = FacesContext.getCurrentInstance( );     		
            FacesMessage facesMsg = 
    	                new FacesMessage(FacesMessage.SEVERITY_INFO, e.getMessage(), null);
        	context.addMessage(null, facesMsg);
//        	String clientID = this.getSecret_inputNewPassword().getClientId(context);
//        	if (!uh.getUser().getPassword().equals(newPwd)) {
        	    // if updating password make sure message is replicated near password field
//                context.addMessage(clientID, facesMsg );
 //       	}
	        return "failure";
    	}
    	// clear previously entered values
    	this.getText_inputNewEmail().setValue("");
    	this.getText_inputConfirmNewEmail().setValue("");
    	
        return "success";
    }

    protected HtmlScriptCollector getScriptCollector2() {
        if (scriptCollector2 == null) {
            scriptCollector2 = (HtmlScriptCollector) findComponentInRoot("scriptCollector2");
        }
        return scriptCollector2;
    }
    protected HtmlForm getFormLogout() {
        if (formLogout == null) {
            formLogout = (HtmlForm) findComponentInRoot("formLogout");
        }
        return formLogout;
    }
    protected HtmlOutputFormat getFormatWelcomeMessage() {
        if (formatWelcomeMessage == null) {
            formatWelcomeMessage = (HtmlOutputFormat) findComponentInRoot("formatWelcomeMessage");
        }
        return formatWelcomeMessage;
    }
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlForm getFormUpdateProfile() {
        if (formUpdateProfile == null) {
            formUpdateProfile = (HtmlForm) findComponentInRoot("formUpdateProfile");
        }
        return formUpdateProfile;
    }
    protected HtmlOutputText getTextProfileUpdateInstructions() {
        if (textProfileUpdateInstructions == null) {
            textProfileUpdateInstructions = (HtmlOutputText) findComponentInRoot("textProfileUpdateInstructions");
        }
        return textProfileUpdateInstructions;
    }
    protected HtmlOutputText getTextUpdateProfileInstructionL2() {
        if (textUpdateProfileInstructionL2 == null) {
            textUpdateProfileInstructionL2 = (HtmlOutputText) findComponentInRoot("textUpdateProfileInstructionL2");
        }
        return textUpdateProfileInstructionL2;
    }
    protected HtmlOutputText getTextUpdateProfileInstructionL3() {
        if (textUpdateProfileInstructionL3 == null) {
            textUpdateProfileInstructionL3 = (HtmlOutputText) findComponentInRoot("textUpdateProfileInstructionL3");
        }
        return textUpdateProfileInstructionL3;
    }
    protected HtmlMessages getMessagesGeneralUpdateProfileErrors() {
        if (messagesGeneralUpdateProfileErrors == null) {
            messagesGeneralUpdateProfileErrors = (HtmlMessages) findComponentInRoot("messagesGeneralUpdateProfileErrors");
        }
        return messagesGeneralUpdateProfileErrors;
    }
    protected HtmlOutputText getTextUpdateEmailAddressHeader() {
        if (textUpdateEmailAddressHeader == null) {
            textUpdateEmailAddressHeader = (HtmlOutputText) findComponentInRoot("textUpdateEmailAddressHeader");
        }
        return textUpdateEmailAddressHeader;
    }
    protected HtmlOutputText getTextCurrentEmailAddress() {
        if (textCurrentEmailAddress == null) {
            textCurrentEmailAddress = (HtmlOutputText) findComponentInRoot("textCurrentEmailAddress");
        }
        return textCurrentEmailAddress;
    }
    protected HtmlOutputText getTextCurrentEmailOutputText() {
        if (textCurrentEmailOutputText == null) {
            textCurrentEmailOutputText = (HtmlOutputText) findComponentInRoot("textCurrentEmailOutputText");
        }
        return textCurrentEmailOutputText;
    }
    protected HtmlOutputText getTextNewEmailAddress() {
        if (textNewEmailAddress == null) {
            textNewEmailAddress = (HtmlOutputText) findComponentInRoot("textNewEmailAddress");
        }
        return textNewEmailAddress;
    }
    protected HtmlInputText getText_inputNewEmail() {
        if (text_inputNewEmail == null) {
            text_inputNewEmail = (HtmlInputText) findComponentInRoot("text_inputNewEmail");
        }
        return text_inputNewEmail;
    }
    protected HtmlMessage getMessage1() {
        if (message1 == null) {
            message1 = (HtmlMessage) findComponentInRoot("message1");
        }
        return message1;
    }
    protected HtmlOutputText getTextConfirmNewEmail() {
        if (textConfirmNewEmail == null) {
            textConfirmNewEmail = (HtmlOutputText) findComponentInRoot("textConfirmNewEmail");
        }
        return textConfirmNewEmail;
    }
    protected HtmlInputText getText_inputConfirmNewEmail() {
        if (text_inputConfirmNewEmail == null) {
            text_inputConfirmNewEmail = (HtmlInputText) findComponentInRoot("text_inputConfirmNewEmail");
        }
        return text_inputConfirmNewEmail;
    }
    protected HtmlMessage getMessage2() {
        if (message2 == null) {
            message2 = (HtmlMessage) findComponentInRoot("message2");
        }
        return message2;
    }
    protected HtmlOutputText getTextUpdatePasswordHeader() {
        if (textUpdatePasswordHeader == null) {
            textUpdatePasswordHeader = (HtmlOutputText) findComponentInRoot("textUpdatePasswordHeader");
        }
        return textUpdatePasswordHeader;
    }
    protected HtmlOutputText getTextPasswordRules() {
        if (textPasswordRules == null) {
            textPasswordRules = (HtmlOutputText) findComponentInRoot("textPasswordRules");
        }
        return textPasswordRules;
    }
    protected HtmlOutputText getTextNewPassword() {
        if (textNewPassword == null) {
            textNewPassword = (HtmlOutputText) findComponentInRoot("textNewPassword");
        }
        return textNewPassword;
    }
    protected HtmlInputSecret getSecret_inputNewPassword() {
        if (secret_inputNewPassword == null) {
            secret_inputNewPassword = (HtmlInputSecret) findComponentInRoot("secret_inputNewPassword");
        }
        return secret_inputNewPassword;
    }
    protected HtmlMessage getMessage3() {
        if (message3 == null) {
            message3 = (HtmlMessage) findComponentInRoot("message3");
        }
        return message3;
    }
    protected HtmlOutputText getTextConfirmNewPassword() {
        if (textConfirmNewPassword == null) {
            textConfirmNewPassword = (HtmlOutputText) findComponentInRoot("textConfirmNewPassword");
        }
        return textConfirmNewPassword;
    }
    protected HtmlInputSecret getSecret_inputConfirmNewPassword() {
        if (secret_inputConfirmNewPassword == null) {
            secret_inputConfirmNewPassword = (HtmlInputSecret) findComponentInRoot("secret_inputConfirmNewPassword");
        }
        return secret_inputConfirmNewPassword;
    }
    protected HtmlMessage getMessage4() {
        if (message4 == null) {
            message4 = (HtmlMessage) findComponentInRoot("message4");
        }
        return message4;
    }
    protected HtmlJspPanel getJspPanelBlueChipEmailReminders() {
        if (jspPanelBlueChipEmailReminders == null) {
            jspPanelBlueChipEmailReminders = (HtmlJspPanel) findComponentInRoot("jspPanelBlueChipEmailReminders");
        }
        return jspPanelBlueChipEmailReminders;
    }
    protected HtmlSelectOneRadio getRadioReminderOptIn() {
        if (radioReminderOptIn == null) {
            radioReminderOptIn = (HtmlSelectOneRadio) findComponentInRoot("radioReminderOptIn");
        }
        return radioReminderOptIn;
    }
    protected HtmlSelectOneRadio getRadioEmailTime() {
        if (radioEmailTime == null) {
            radioEmailTime = (HtmlSelectOneRadio) findComponentInRoot("radioEmailTime");
        }
        return radioEmailTime;
    }
    protected HtmlCommandExButton getButtonDoLogoutBtn() {
        if (buttonDoLogoutBtn == null) {
            buttonDoLogoutBtn = (HtmlCommandExButton) findComponentInRoot("buttonDoLogoutBtn");
        }
        return buttonDoLogoutBtn;
    }
    protected HtmlInputHelperKeybind getInputHelperKeybind1() {
        if (inputHelperKeybind1 == null) {
            inputHelperKeybind1 = (HtmlInputHelperKeybind) findComponentInRoot("inputHelperKeybind1");
        }
        return inputHelperKeybind1;
    }
    protected HtmlInputHidden getReminderOptInHidden() {
        if (reminderOptInHidden == null) {
            reminderOptInHidden = (HtmlInputHidden) findComponentInRoot("reminderOptInHidden");
        }
        return reminderOptInHidden;
    }
    protected HtmlCommandExButton getButtonSubmitProfileUpdate() {
        if (buttonSubmitProfileUpdate == null) {
            buttonSubmitProfileUpdate = (HtmlCommandExButton) findComponentInRoot("buttonSubmitProfileUpdate");
        }
        return buttonSubmitProfileUpdate;
    }

	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUserHandler() {
		if (userHandler == null) {
			userHandler = (UserHandler) getManagedBean("userHandler");
		}
		return userHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUserHandler(UserHandler userHandler) {
		this.userHandler = userHandler;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlJspPanel getJspPanelEmailHeader() {
		if (jspPanelEmailHeader == null) {
			jspPanelEmailHeader = (HtmlJspPanel) findComponentInRoot("jspPanelEmailHeader");
		}
		return jspPanelEmailHeader;
	}
}
/*
 * Created on Mar 16, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.drawManagement.forweek;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlOutputSeparator;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.champion.handlers.RDLForWeekHandler;
import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.singlecopy.UsatException;
import com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode;
/**
 * @author aeast
 * @date Mar 16, 2007
 * @class forweekConfirm
 * 
 * 
 * 
 */
public class Confirm extends PageCodeBase {

    protected HtmlScriptCollector scriptCollector1;
    protected HtmlForm formWeeklyMngmtConfirmation;
    protected HtmlOutputSeparator separator1;
    protected HtmlOutputText textHeaderWeekEndingDate;
    protected HtmlOutputText text2;
    protected HtmlOutputText text1;
    protected HtmlOutputText text14;
    protected HtmlOutputText text15;
    protected HtmlCommandExButton buttonBack;
    protected HtmlOutputSeparator separator2;
    protected HtmlOutputText textNumberChangedPOs;
    protected HtmlOutputText textSaveWarning;
    protected HtmlOutputText textConfirmTitle;
    protected HtmlOutputText textWeekEndingLabel;
    protected HtmlOutputText textRouteLabel;
    protected HtmlOutputText textPubCodeLabel;
    protected HtmlOutputText textNumPOChangedLabel;
    protected HtmlOutputText textDrawHeaderLabel;
    protected HtmlOutputText textReturnsHeaderLabel;
    protected HtmlOutputText textOriginalLabel;
    protected HtmlOutputText textNewValLabel;
    protected HtmlOutputText textOrigTotDraw;
    protected HtmlOutputText textOrigTotReturns;
    protected HtmlCommandExButton buttonSaveChanges;
	protected RDLForWeekHandler RDLForWeek;
	protected UserHandler userHandler;
	protected OmnitureTrackingCode omnitureTracking;
    public String doButtonSaveChangesAction() {
        // Type Java code that runs when the component is clicked

        RDLForWeekHandler rdl = this.getRDLForWeek();

        if (!rdl.getSaveInProgress()) {
        	rdl.setSaveInProgress(true);
        }
        else {
        	// To get a around an issue with requests being submitted multiple times.
    		UserHandler uh = this.getUserHandler();
    		if (uh != null) {
    			System.out.println("Daily RDL Save Request in process already. User: " + uh.getEmailAddress() + " Browser: " + uh.getBrowserUserAgentValue());
    		}
    		else {
    			System.out.println("Daily RDL Save Request in process already. User: " + rdl.getWdm().getUser().getEmailAddress());
    		}

        	int count = 0;
        	while (rdl.getSaveInProgress() && count < 5) {
        		count++;
	        	try {
	        		Thread.sleep(3000);
	        	}
	        	catch (Exception e) {
	        		;
	        	}
        	} // end while
        	
        	if (rdl.getSaveInProgress()) {
        		System.out.println("Weekly RDL Save Request exceeded threshold. User: " + rdl.getWdm().getUser().getEmailAddress());
        		rdl.setSaveInProgress(false);
	            FacesContext context = FacesContext.getCurrentInstance( );
	            FacesMessage facesMsg = 
	                new FacesMessage(FacesMessage.SEVERITY_INFO, "Save Entries May Have Failed. An unexpected error occurred. Please reload your weekly draw/return data to confirm that the changes were applied.", null);
	            context.addMessage(null, facesMsg);
                return "failure";
        	}
        	else {
        		return "success";
        	}
        	
        }
                
        try {
            // clear any previous error detail
            rdl.setErrorDetail("");
            rdl.getWdm().saveChanges();
            // reset error processing on product orders
            rdl.resetErrorProcessing();
            if (rdl.getWdm().getUpdateErrorsFlag()) {
                // errors during update
	            FacesContext context = FacesContext.getCurrentInstance( );
	            FacesMessage facesMsg = 
	                new FacesMessage(FacesMessage.SEVERITY_INFO, "Save Entries Failed. Please check Product Orders for Error Messages.", null);
	            context.addMessage(null, facesMsg);
                return "failure";
            }
        }
        catch (UsatException ue) {
            rdl.setErrorDetail(ue.getMessage());
            FacesContext context = FacesContext.getCurrentInstance( );
            FacesMessage facesMsg = 
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Save Entries Failed. " + ue.getMessage(),null);
            context.addMessage(null, facesMsg);
            return "failure";
            
        }
        catch (Exception e) {
            rdl.setErrorDetail(e.getMessage());
            FacesContext context = FacesContext.getCurrentInstance( );
            FacesMessage facesMsg = 
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Save Entries Failed. " + e.getMessage(), null);
            context.addMessage(null, facesMsg);
            return "failure";
        }
        finally {
    		rdl.setSaveInProgress(false);
        }
        
        return "success";
    }
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlForm getFormWeeklyMngmtConfirmation() {
        if (formWeeklyMngmtConfirmation == null) {
            formWeeklyMngmtConfirmation = (HtmlForm) findComponentInRoot("formWeeklyMngmtConfirmation");
        }
        return formWeeklyMngmtConfirmation;
    }
    protected HtmlOutputSeparator getSeparator1() {
        if (separator1 == null) {
            separator1 = (HtmlOutputSeparator) findComponentInRoot("separator1");
        }
        return separator1;
    }
    protected HtmlOutputText getTextHeaderWeekEndingDate() {
        if (textHeaderWeekEndingDate == null) {
            textHeaderWeekEndingDate = (HtmlOutputText) findComponentInRoot("textHeaderWeekEndingDate");
        }
        return textHeaderWeekEndingDate;
    }
    protected HtmlOutputText getText2() {
        if (text2 == null) {
            text2 = (HtmlOutputText) findComponentInRoot("text2");
        }
        return text2;
    }
    protected HtmlOutputText getText1() {
        if (text1 == null) {
            text1 = (HtmlOutputText) findComponentInRoot("text1");
        }
        return text1;
    }
    protected HtmlOutputText getText14() {
        if (text14 == null) {
            text14 = (HtmlOutputText) findComponentInRoot("text14");
        }
        return text14;
    }
    protected HtmlOutputText getText15() {
        if (text15 == null) {
            text15 = (HtmlOutputText) findComponentInRoot("text15");
        }
        return text15;
    }
    protected HtmlCommandExButton getButtonBack() {
        if (buttonBack == null) {
            buttonBack = (HtmlCommandExButton) findComponentInRoot("buttonBack");
        }
        return buttonBack;
    }
    protected HtmlOutputSeparator getSeparator2() {
        if (separator2 == null) {
            separator2 = (HtmlOutputSeparator) findComponentInRoot("separator2");
        }
        return separator2;
    }
    protected HtmlOutputText getTextNumberChangedPOs() {
        if (textNumberChangedPOs == null) {
            textNumberChangedPOs = (HtmlOutputText) findComponentInRoot("textNumberChangedPOs");
        }
        return textNumberChangedPOs;
    }
    protected HtmlOutputText getTextSaveWarning() {
        if (textSaveWarning == null) {
            textSaveWarning = (HtmlOutputText) findComponentInRoot("textSaveWarning");
        }
        return textSaveWarning;
    }
    protected HtmlOutputText getTextConfirmTitle() {
        if (textConfirmTitle == null) {
            textConfirmTitle = (HtmlOutputText) findComponentInRoot("textConfirmTitle");
        }
        return textConfirmTitle;
    }
    protected HtmlOutputText getTextWeekEndingLabel() {
        if (textWeekEndingLabel == null) {
            textWeekEndingLabel = (HtmlOutputText) findComponentInRoot("textWeekEndingLabel");
        }
        return textWeekEndingLabel;
    }
    protected HtmlOutputText getTextRouteLabel() {
        if (textRouteLabel == null) {
            textRouteLabel = (HtmlOutputText) findComponentInRoot("textRouteLabel");
        }
        return textRouteLabel;
    }
    protected HtmlOutputText getTextPubCodeLabel() {
        if (textPubCodeLabel == null) {
            textPubCodeLabel = (HtmlOutputText) findComponentInRoot("textPubCodeLabel");
        }
        return textPubCodeLabel;
    }
    protected HtmlOutputText getTextNumPOChangedLabel() {
        if (textNumPOChangedLabel == null) {
            textNumPOChangedLabel = (HtmlOutputText) findComponentInRoot("textNumPOChangedLabel");
        }
        return textNumPOChangedLabel;
    }
    protected HtmlOutputText getTextDrawHeaderLabel() {
        if (textDrawHeaderLabel == null) {
            textDrawHeaderLabel = (HtmlOutputText) findComponentInRoot("textDrawHeaderLabel");
        }
        return textDrawHeaderLabel;
    }
    protected HtmlOutputText getTextReturnsHeaderLabel() {
        if (textReturnsHeaderLabel == null) {
            textReturnsHeaderLabel = (HtmlOutputText) findComponentInRoot("textReturnsHeaderLabel");
        }
        return textReturnsHeaderLabel;
    }
    protected HtmlOutputText getTextOriginalLabel() {
        if (textOriginalLabel == null) {
            textOriginalLabel = (HtmlOutputText) findComponentInRoot("textOriginalLabel");
        }
        return textOriginalLabel;
    }
    protected HtmlOutputText getTextNewValLabel() {
        if (textNewValLabel == null) {
            textNewValLabel = (HtmlOutputText) findComponentInRoot("textNewValLabel");
        }
        return textNewValLabel;
    }
    protected HtmlOutputText getTextOrigTotDraw() {
        if (textOrigTotDraw == null) {
            textOrigTotDraw = (HtmlOutputText) findComponentInRoot("textOrigTotDraw");
        }
        return textOrigTotDraw;
    }
    protected HtmlOutputText getTextOrigTotReturns() {
        if (textOrigTotReturns == null) {
            textOrigTotReturns = (HtmlOutputText) findComponentInRoot("textOrigTotReturns");
        }
        return textOrigTotReturns;
    }
    protected HtmlCommandExButton getButtonSaveChanges() {
        if (buttonSaveChanges == null) {
            buttonSaveChanges = (HtmlCommandExButton) findComponentInRoot("buttonSaveChanges");
        }
        return buttonSaveChanges;
    }
	/** 
	 * @managed-bean true
	 */
	protected RDLForWeekHandler getRDLForWeek() {
		if (RDLForWeek == null) {
			RDLForWeek = (RDLForWeekHandler) getManagedBean("RDLForWeek");
		}
		return RDLForWeek;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setRDLForWeek(RDLForWeekHandler RDLForWeek) {
		this.RDLForWeek = RDLForWeek;
	}
	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUserHandler() {
		if (userHandler == null) {
			userHandler = (UserHandler) getManagedBean("userHandler");
		}
		return userHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUserHandler(UserHandler userHandler) {
		this.userHandler = userHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected OmnitureTrackingCode getOmnitureTracking() {
		if (omnitureTracking == null) {
			omnitureTracking = (OmnitureTrackingCode) getManagedBean("omnitureTracking");
		}
		return omnitureTracking;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setOmnitureTracking(OmnitureTrackingCode omnitureTracking) {
		this.omnitureTracking = omnitureTracking;
	}
}
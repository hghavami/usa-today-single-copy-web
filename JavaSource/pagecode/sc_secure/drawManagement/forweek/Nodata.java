/*
 * Created on May 23, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.drawManagement.forweek;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlCommandExButton;
/**
 * @author aeast
 * @date May 23, 2007
 * @class drawManagementNodata
 * 
 * 
 * 
 */
public class Nodata extends PageCodeBase {

    protected HtmlScriptCollector scriptCollector1;
    protected HtmlForm form1;
    protected HtmlOutputText textRouteIDText;
    protected HtmlOutputText textNoData;
    protected HtmlOutputText textRoute;
    protected HtmlOutputText textPub;
	protected HtmlOutputText textWeekOf;
	protected HtmlCommandExButton buttonCloseWin;
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlForm getForm1() {
        if (form1 == null) {
            form1 = (HtmlForm) findComponentInRoot("form1");
        }
        return form1;
    }
    protected HtmlOutputText getTextRouteIDText() {
        if (textRouteIDText == null) {
            textRouteIDText = (HtmlOutputText) findComponentInRoot("textRouteIDText");
        }
        return textRouteIDText;
    }
    protected HtmlOutputText getTextNoData() {
        if (textNoData == null) {
            textNoData = (HtmlOutputText) findComponentInRoot("textNoData");
        }
        return textNoData;
    }
    protected HtmlOutputText getTextRoute() {
        if (textRoute == null) {
            textRoute = (HtmlOutputText) findComponentInRoot("textRoute");
        }
        return textRoute;
    }
    protected HtmlOutputText getTextPub() {
        if (textPub == null) {
            textPub = (HtmlOutputText) findComponentInRoot("textPub");
        }
        return textPub;
    }
	protected HtmlOutputText getTextWeekOf() {
		if (textWeekOf == null) {
			textWeekOf = (HtmlOutputText) findComponentInRoot("textWeekOf");
		}
		return textWeekOf;
	}
	protected HtmlCommandExButton getButtonCloseWin() {
		if (buttonCloseWin == null) {
			buttonCloseWin = (HtmlCommandExButton) findComponentInRoot("buttonCloseWin");
		}
		return buttonCloseWin;
	}
}
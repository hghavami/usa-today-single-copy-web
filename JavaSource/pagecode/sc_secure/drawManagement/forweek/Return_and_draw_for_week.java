/*
 * Created on Mar 9, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.drawManagement.forweek;

import java.util.Collection;
import java.util.Iterator;

import javax.faces.component.UIColumn;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;

import pagecode.PageCodeBase;

import com.ibm.faces.bf.component.html.HtmlBfPanel;
import com.ibm.faces.bf.component.html.HtmlTabbedPanel;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlInputHelperKeybind;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlOutputSeparator;
import com.ibm.faces.component.html.HtmlOutputStatistics;
import com.ibm.faces.component.html.HtmlPagerWeb;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.champion.handlers.RDLForWeekHandler;
import com.usatoday.champion.handlers.WeeklyLocationHandler;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;
/**
 * @author aeast
 * @date Mar 9, 2007
 * @class forweekReturn_and_draw_for_week
 * 
 * 
 * 
 */
public class Return_and_draw_for_week extends PageCodeBase {

    
    protected HtmlScriptCollector scriptCollector1;
    protected HtmlForm formManageDraw;
    protected HtmlOutputSeparator separator1;
    protected HtmlOutputText textDMTitleLabel;
    protected HtmlOutputText textLateReturnsMsg;
    protected HtmlOutputText textNumLocationHeader;
    protected HtmlOutputText textCurrentRDLDate;
    protected HtmlOutputText textRouteIDHeaderLabel;
    protected HtmlOutputText textRouteIDHeaderValue;
    protected HtmlOutputText textRouteDescHeaderLabel;
    protected HtmlOutputText textRouteDescriptionHeaderValue;
    protected HtmlMessages messagesCatchAll;
    protected HtmlOutputText textLocationInfo;
    protected HtmlOutputText textNumLocsPrompt;
    protected HtmlOutputText textHeaderPubLabel;
    protected HtmlOutputText textHeaderPublicationValue;
    protected HtmlOutputText textProductDescriptionHeader;
    protected HtmlInputText textNumberLocsToDisplay;
    protected HtmlCommandExButton buttonUpdateNumLocsToDisplay;
    protected HtmlTabbedPanel tabbedPanelWeeklyDrawTabbedPanel;
    protected HtmlBfPanel bfpanelMainTabbedPanelWeeklyDraw;
    protected HtmlJspPanel jspPanelInsidePanelForDataGridWeeklyDraw;
    protected HtmlDataTable tableWeeklyDrawDataTable;
    protected UIColumn columnLocDataTableLoc;
    protected HtmlJspPanel jspPanelLocationInfoColumnPanel;
    protected HtmlOutputText textLocationIDColumnValue;
    protected HtmlOutputText textLocationNameColumnValue;
    protected HtmlOutputText textLocationAddress1ColumnValue;
    protected HtmlOutputText textLocationPhoneNumber;
    protected HtmlJspPanel jspPanelProductOrderDataTablePanel;
    protected HtmlDataTable tableInsideProductOrderDataTable;
    protected UIColumn columnProductOrderIDValue;
    protected HtmlOutputText textProductOrderIDValue;
    protected HtmlOutputText textProductOrderIDColHeader;
    protected HtmlOutputLinkEx linkExProductOrderTypeLink;
    protected HtmlOutputText textProductOrderTypeValue;
    protected HtmlOutputText textProductOrderTypeHeader;
    protected HtmlJspPanel jspPanelMondayDrawMngmt;
    protected HtmlMessage messageMonDrawErr;
    protected HtmlInputText textMondayDrawValue;
    protected HtmlInputText textMondayReturnValue;
    protected HtmlJspPanel jspPanelMondayColHeaderPanel;
    protected HtmlOutputText textMondayDrawMngmtHeaderLabel;
    protected HtmlOutputText textMondayDrawHeaderLabel;
    protected HtmlOutputText textMondayReturnsHeaderLabel;
    protected HtmlJspPanel jspPanelTuesdayDrawMngmtPanel;
    protected HtmlMessage messageTuesDrawErr;
    protected HtmlInputText textTuesdayDrawValue;
    protected HtmlInputText textTuesdayReturnValue;
    protected HtmlJspPanel jspPanelTuesdayColHeaderPanel;
    protected HtmlOutputText textTuesdayDrawMngmtHeaderLabel;
    protected HtmlOutputText textTuesdayDrawHeaderLabel;
    protected HtmlOutputText textTuesdayReturnsHeaderLabel;
    protected HtmlJspPanel jspPanelWednesdayDrawMngmtPanel;
    protected HtmlMessage messageWedDrawErr;
    protected HtmlInputText textWednesdayDrawValue;
    protected HtmlInputText textWednesdayReturnValue;
    protected HtmlJspPanel jspPanelWednesdayColHeaderPanel;
    protected HtmlOutputText text3;
    protected HtmlOutputText textWednesdayDrawHeaderLabel;
    protected HtmlOutputText textWednesdayReturnsHeaderLabel;
    protected HtmlJspPanel jspPanelThursdayDrawMngmtPanel;
    protected HtmlMessage messageThurDrawErr;
    protected HtmlInputText textThursdayDrawValue;
    protected HtmlInputText textThursdayReturnValue;
    protected HtmlJspPanel jspPanelThursdayColHeaderPanel;
    protected HtmlOutputText text4;
    protected HtmlOutputText textThursdayDrawHeaderLabel;
    protected HtmlOutputText textThursdayReturnsHeaderLabel;
    protected HtmlJspPanel jspPanelFridayDrawMngmtPanel;
    protected HtmlMessage messageFriDrawErr;
    protected HtmlInputText textFridayDrawValue;
    protected HtmlInputText textFridayReturnValue;
    protected HtmlJspPanel jspPanelFridayColHeaderPanel;
    protected HtmlOutputText text5;
    protected HtmlOutputText textFridayDrawHeaderLabel;
    protected HtmlOutputText textFridayReturnsHeaderLabel;
    protected HtmlJspPanel jspPanelSaturdayDrawMngmtPanel;
    protected HtmlMessage messageSatDrawErr;
    protected HtmlInputText textSaturdayDrawValue;
    protected HtmlInputText textSaturdayReturnValue;
    protected HtmlJspPanel jspPanelSaturdayColHeaderPanel;
    protected HtmlOutputText text6;
    protected HtmlOutputText textSaturdayDrawHeaderLabel;
    protected HtmlOutputText textSaturdayReturnsHeaderLabel;
    protected HtmlJspPanel jspPanelSundayDrawMngmtPanel;
    protected HtmlMessage messageSunDrawErr;
    protected HtmlInputText textSundayDrawValue;
    protected HtmlInputText textSundayReturnValue;
    protected HtmlJspPanel jspPanelSundayColHeaderPanel;
    protected HtmlOutputText text7;
    protected HtmlOutputText textSundayDrawHeaderLabel;
    protected HtmlOutputText textSundayReturnsHeaderLabel;
    protected HtmlPanelBox box1FooterNav;
    protected HtmlPanelLayout layout1;
    protected HtmlOutputStatistics statistics1;
    protected HtmlPagerWeb web1;
    protected HtmlCommandExButton tabbedPanel1_back;
    protected HtmlCommandExButton tabbedPanel1_next;
    protected HtmlCommandExButton tabbedPanel1_finish;
    protected HtmlCommandExButton tabbedPanel1_cancel;
    protected UIColumn columnLocDataTableProductOrders;
    protected UIColumn column2;
    protected UIColumn column1;
    protected HtmlMessage messageMonReturnErr;
    protected HtmlInputHidden hiddenMonZeroDrawReasonCode;
    protected UIColumn column3;
    protected HtmlMessage messageTuesReturnErr;
    protected HtmlInputHidden hiddenTuesZeroDrawReasonCode;
    protected UIColumn column4;
    protected HtmlMessage messageWedReturnErr;
    protected HtmlInputHidden hiddenWedZeroDrawReasonCode;
    protected UIColumn column5;
    protected HtmlMessage messageThurReturnErr;
    protected HtmlInputHidden hiddenThurZeroDrawReasonCode;
    protected UIColumn column6;
    protected HtmlMessage messageFriReturnErr;
    protected HtmlInputHidden hiddenFriZeroDrawReasonCode;
    protected UIColumn column7;
    protected HtmlMessage messageSatReturnErr;
    protected HtmlInputHidden hiddenSatZeroDrawReasonCode;
    protected UIColumn column8;
    protected HtmlMessage messageSunReturnErr;
    protected HtmlInputHidden hiddenSunZeroDrawReasonCode;
    protected HtmlCommandExButton buttonSubmitWeeklyDrawChanges;
    protected HtmlCommandExButton buttonResetAllValues;
    protected HtmlCommandExButton buttonQuitWithoutSavingWeeklyDraw;
    protected HtmlInputHelperKeybind inputHelperKeybind1;
    protected HtmlMessage message1;
    protected HtmlOutputText textNotEdittable;
	protected RDLForWeekHandler RDLForWeek;
  
    public String doButtonQuitWithoutSavingWeeklyDrawAction() {
        // Type Java code that runs when the component is clicked
        this.getTableWeeklyDrawDataTable().setFirst(0);

        RDLForWeekHandler rdl = this.getRDLForWeek();
        Iterator<WeeklyLocationHandler> itr = rdl.getLocations().iterator();
        while (itr.hasNext()) {
            WeeklyLocationHandler l = itr.next();
            l.getProductOrders().clear();
        }
        rdl.getLocations().clear();
        this.getSessionScope().remove("RDLForWeek");
        return "success";
    }
    public String doButtonSubmitWeeklyDrawChangesAction() {
        // Type Java code that runs when the component is clicked

        Collection<ProductOrderIntf> changedRecords = this.getRDLForWeek().getWdm().getChangedProductOrders();
        if (USATApplicationConstants.debug) {
	        Iterator<ProductOrderIntf> itr = changedRecords.iterator();
	        while (itr.hasNext()) {
	            WeeklyProductOrderIntf wpo = (WeeklyProductOrderIntf)itr.next();
                System.out.println("Changed PO: " + wpo.toString());
            }
        }
        
        return "success";
    }
 
    public String doButtonUpdateNumLocsToDisplayAction() {
        // Type Java code that runs when the component is clicked

        // 
        // return "success";
        // return "success";
        return "success";
    }
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlForm getFormManageDraw() {
        if (formManageDraw == null) {
            formManageDraw = (HtmlForm) findComponentInRoot("formManageDraw");
        }
        return formManageDraw;
    }
    protected HtmlOutputSeparator getSeparator1() {
        if (separator1 == null) {
            separator1 = (HtmlOutputSeparator) findComponentInRoot("separator1");
        }
        return separator1;
    }
    protected HtmlOutputText getTextDMTitleLabel() {
        if (textDMTitleLabel == null) {
            textDMTitleLabel = (HtmlOutputText) findComponentInRoot("textDMTitleLabel");
        }
        return textDMTitleLabel;
    }
    protected HtmlOutputText getTextLateReturnsMsg() {
        if (textLateReturnsMsg == null) {
            textLateReturnsMsg = (HtmlOutputText) findComponentInRoot("textLateReturnsMsg");
        }
        return textLateReturnsMsg;
    }
    protected HtmlOutputText getTextNumLocationHeader() {
        if (textNumLocationHeader == null) {
            textNumLocationHeader = (HtmlOutputText) findComponentInRoot("textNumLocationHeader");
        }
        return textNumLocationHeader;
    }
    protected HtmlOutputText getTextCurrentRDLDate() {
        if (textCurrentRDLDate == null) {
            textCurrentRDLDate = (HtmlOutputText) findComponentInRoot("textCurrentRDLDate");
        }
        return textCurrentRDLDate;
    }
    protected HtmlOutputText getTextRouteIDHeaderLabel() {
        if (textRouteIDHeaderLabel == null) {
            textRouteIDHeaderLabel = (HtmlOutputText) findComponentInRoot("textRouteIDHeaderLabel");
        }
        return textRouteIDHeaderLabel;
    }
    protected HtmlOutputText getTextRouteIDHeaderValue() {
        if (textRouteIDHeaderValue == null) {
            textRouteIDHeaderValue = (HtmlOutputText) findComponentInRoot("textRouteIDHeaderValue");
        }
        return textRouteIDHeaderValue;
    }
    protected HtmlOutputText getTextRouteDescHeaderLabel() {
        if (textRouteDescHeaderLabel == null) {
            textRouteDescHeaderLabel = (HtmlOutputText) findComponentInRoot("textRouteDescHeaderLabel");
        }
        return textRouteDescHeaderLabel;
    }
    protected HtmlOutputText getTextRouteDescriptionHeaderValue() {
        if (textRouteDescriptionHeaderValue == null) {
            textRouteDescriptionHeaderValue = (HtmlOutputText) findComponentInRoot("textRouteDescriptionHeaderValue");
        }
        return textRouteDescriptionHeaderValue;
    }
    protected HtmlMessages getMessagesCatchAll() {
        if (messagesCatchAll == null) {
            messagesCatchAll = (HtmlMessages) findComponentInRoot("messagesCatchAll");
        }
        return messagesCatchAll;
    }
    protected HtmlOutputText getTextLocationInfo() {
        if (textLocationInfo == null) {
            textLocationInfo = (HtmlOutputText) findComponentInRoot("textLocationInfo");
        }
        return textLocationInfo;
    }
    protected HtmlOutputText getTextNumLocsPrompt() {
        if (textNumLocsPrompt == null) {
            textNumLocsPrompt = (HtmlOutputText) findComponentInRoot("textNumLocsPrompt");
        }
        return textNumLocsPrompt;
    }
    protected HtmlOutputText getTextHeaderPubLabel() {
        if (textHeaderPubLabel == null) {
            textHeaderPubLabel = (HtmlOutputText) findComponentInRoot("textHeaderPubLabel");
        }
        return textHeaderPubLabel;
    }
    protected HtmlOutputText getTextHeaderPublicationValue() {
        if (textHeaderPublicationValue == null) {
            textHeaderPublicationValue = (HtmlOutputText) findComponentInRoot("textHeaderPublicationValue");
        }
        return textHeaderPublicationValue;
    }
    protected HtmlOutputText getTextProductDescriptionHeader() {
        if (textProductDescriptionHeader == null) {
            textProductDescriptionHeader = (HtmlOutputText) findComponentInRoot("textProductDescriptionHeader");
        }
        return textProductDescriptionHeader;
    }
    protected HtmlInputText getTextNumberLocsToDisplay() {
        if (textNumberLocsToDisplay == null) {
            textNumberLocsToDisplay = (HtmlInputText) findComponentInRoot("textNumberLocsToDisplay");
        }
        return textNumberLocsToDisplay;
    }
    protected HtmlCommandExButton getButtonUpdateNumLocsToDisplay() {
        if (buttonUpdateNumLocsToDisplay == null) {
            buttonUpdateNumLocsToDisplay = (HtmlCommandExButton) findComponentInRoot("buttonUpdateNumLocsToDisplay");
        }
        return buttonUpdateNumLocsToDisplay;
    }
    protected HtmlTabbedPanel getTabbedPanelWeeklyDrawTabbedPanel() {
        if (tabbedPanelWeeklyDrawTabbedPanel == null) {
            tabbedPanelWeeklyDrawTabbedPanel = (HtmlTabbedPanel) findComponentInRoot("tabbedPanelWeeklyDrawTabbedPanel");
        }
        return tabbedPanelWeeklyDrawTabbedPanel;
    }
    protected HtmlBfPanel getBfpanelMainTabbedPanelWeeklyDraw() {
        if (bfpanelMainTabbedPanelWeeklyDraw == null) {
            bfpanelMainTabbedPanelWeeklyDraw = (HtmlBfPanel) findComponentInRoot("bfpanelMainTabbedPanelWeeklyDraw");
        }
        return bfpanelMainTabbedPanelWeeklyDraw;
    }
    protected HtmlJspPanel getJspPanelInsidePanelForDataGridWeeklyDraw() {
        if (jspPanelInsidePanelForDataGridWeeklyDraw == null) {
            jspPanelInsidePanelForDataGridWeeklyDraw = (HtmlJspPanel) findComponentInRoot("jspPanelInsidePanelForDataGridWeeklyDraw");
        }
        return jspPanelInsidePanelForDataGridWeeklyDraw;
    }
    protected HtmlDataTable getTableWeeklyDrawDataTable() {
        if (tableWeeklyDrawDataTable == null) {
            tableWeeklyDrawDataTable = (HtmlDataTable) findComponentInRoot("tableWeeklyDrawDataTable");
        }
        return tableWeeklyDrawDataTable;
    }
    protected UIColumn getColumnLocDataTableLoc() {
        if (columnLocDataTableLoc == null) {
            columnLocDataTableLoc = (UIColumn) findComponentInRoot("columnLocDataTableLoc");
        }
        return columnLocDataTableLoc;
    }
    protected HtmlJspPanel getJspPanelLocationInfoColumnPanel() {
        if (jspPanelLocationInfoColumnPanel == null) {
            jspPanelLocationInfoColumnPanel = (HtmlJspPanel) findComponentInRoot("jspPanelLocationInfoColumnPanel");
        }
        return jspPanelLocationInfoColumnPanel;
    }
    protected HtmlOutputText getTextLocationIDColumnValue() {
        if (textLocationIDColumnValue == null) {
            textLocationIDColumnValue = (HtmlOutputText) findComponentInRoot("textLocationIDColumnValue");
        }
        return textLocationIDColumnValue;
    }
    protected HtmlOutputText getTextLocationNameColumnValue() {
        if (textLocationNameColumnValue == null) {
            textLocationNameColumnValue = (HtmlOutputText) findComponentInRoot("textLocationNameColumnValue");
        }
        return textLocationNameColumnValue;
    }
    protected HtmlOutputText getTextLocationAddress1ColumnValue() {
        if (textLocationAddress1ColumnValue == null) {
            textLocationAddress1ColumnValue = (HtmlOutputText) findComponentInRoot("textLocationAddress1ColumnValue");
        }
        return textLocationAddress1ColumnValue;
    }
    protected HtmlOutputText getTextLocationPhoneNumber() {
        if (textLocationPhoneNumber == null) {
            textLocationPhoneNumber = (HtmlOutputText) findComponentInRoot("textLocationPhoneNumber");
        }
        return textLocationPhoneNumber;
    }
    protected HtmlJspPanel getJspPanelProductOrderDataTablePanel() {
        if (jspPanelProductOrderDataTablePanel == null) {
            jspPanelProductOrderDataTablePanel = (HtmlJspPanel) findComponentInRoot("jspPanelProductOrderDataTablePanel");
        }
        return jspPanelProductOrderDataTablePanel;
    }
    protected HtmlDataTable getTableInsideProductOrderDataTable() {
        if (tableInsideProductOrderDataTable == null) {
            tableInsideProductOrderDataTable = (HtmlDataTable) findComponentInRoot("tableInsideProductOrderDataTable");
        }
        return tableInsideProductOrderDataTable;
    }
    protected UIColumn getColumnProductOrderIDValue() {
        if (columnProductOrderIDValue == null) {
            columnProductOrderIDValue = (UIColumn) findComponentInRoot("columnProductOrderIDValue");
        }
        return columnProductOrderIDValue;
    }
    protected HtmlOutputText getTextProductOrderIDValue() {
        if (textProductOrderIDValue == null) {
            textProductOrderIDValue = (HtmlOutputText) findComponentInRoot("textProductOrderIDValue");
        }
        return textProductOrderIDValue;
    }
    protected HtmlOutputText getTextProductOrderIDColHeader() {
        if (textProductOrderIDColHeader == null) {
            textProductOrderIDColHeader = (HtmlOutputText) findComponentInRoot("textProductOrderIDColHeader");
        }
        return textProductOrderIDColHeader;
    }
    protected HtmlOutputLinkEx getLinkExProductOrderTypeLink() {
        if (linkExProductOrderTypeLink == null) {
            linkExProductOrderTypeLink = (HtmlOutputLinkEx) findComponentInRoot("linkExProductOrderTypeLink");
        }
        return linkExProductOrderTypeLink;
    }
    protected HtmlOutputText getTextProductOrderTypeValue() {
        if (textProductOrderTypeValue == null) {
            textProductOrderTypeValue = (HtmlOutputText) findComponentInRoot("textProductOrderTypeValue");
        }
        return textProductOrderTypeValue;
    }
    protected HtmlOutputText getTextProductOrderTypeHeader() {
        if (textProductOrderTypeHeader == null) {
            textProductOrderTypeHeader = (HtmlOutputText) findComponentInRoot("textProductOrderTypeHeader");
        }
        return textProductOrderTypeHeader;
    }
    protected HtmlJspPanel getJspPanelMondayDrawMngmt() {
        if (jspPanelMondayDrawMngmt == null) {
            jspPanelMondayDrawMngmt = (HtmlJspPanel) findComponentInRoot("jspPanelMondayDrawMngmt");
        }
        return jspPanelMondayDrawMngmt;
    }
    protected HtmlMessage getMessageMonDrawErr() {
        if (messageMonDrawErr == null) {
            messageMonDrawErr = (HtmlMessage) findComponentInRoot("messageMonDrawErr");
        }
        return messageMonDrawErr;
    }
    protected HtmlInputText getTextMondayDrawValue() {
        if (textMondayDrawValue == null) {
            textMondayDrawValue = (HtmlInputText) findComponentInRoot("textMondayDrawValue");
        }
        return textMondayDrawValue;
    }
    protected HtmlInputText getTextMondayReturnValue() {
        if (textMondayReturnValue == null) {
            textMondayReturnValue = (HtmlInputText) findComponentInRoot("textMondayReturnValue");
        }
        return textMondayReturnValue;
    }
    protected HtmlJspPanel getJspPanelMondayColHeaderPanel() {
        if (jspPanelMondayColHeaderPanel == null) {
            jspPanelMondayColHeaderPanel = (HtmlJspPanel) findComponentInRoot("jspPanelMondayColHeaderPanel");
        }
        return jspPanelMondayColHeaderPanel;
    }
    protected HtmlOutputText getTextMondayDrawMngmtHeaderLabel() {
        if (textMondayDrawMngmtHeaderLabel == null) {
            textMondayDrawMngmtHeaderLabel = (HtmlOutputText) findComponentInRoot("textMondayDrawMngmtHeaderLabel");
        }
        return textMondayDrawMngmtHeaderLabel;
    }
    protected HtmlOutputText getTextMondayDrawHeaderLabel() {
        if (textMondayDrawHeaderLabel == null) {
            textMondayDrawHeaderLabel = (HtmlOutputText) findComponentInRoot("textMondayDrawHeaderLabel");
        }
        return textMondayDrawHeaderLabel;
    }
    protected HtmlOutputText getTextMondayReturnsHeaderLabel() {
        if (textMondayReturnsHeaderLabel == null) {
            textMondayReturnsHeaderLabel = (HtmlOutputText) findComponentInRoot("textMondayReturnsHeaderLabel");
        }
        return textMondayReturnsHeaderLabel;
    }
    protected HtmlJspPanel getJspPanelTuesdayDrawMngmtPanel() {
        if (jspPanelTuesdayDrawMngmtPanel == null) {
            jspPanelTuesdayDrawMngmtPanel = (HtmlJspPanel) findComponentInRoot("jspPanelTuesdayDrawMngmtPanel");
        }
        return jspPanelTuesdayDrawMngmtPanel;
    }
    protected HtmlMessage getMessageTuesDrawErr() {
        if (messageTuesDrawErr == null) {
            messageTuesDrawErr = (HtmlMessage) findComponentInRoot("messageTuesDrawErr");
        }
        return messageTuesDrawErr;
    }
    protected HtmlInputText getTextTuesdayDrawValue() {
        if (textTuesdayDrawValue == null) {
            textTuesdayDrawValue = (HtmlInputText) findComponentInRoot("textTuesdayDrawValue");
        }
        return textTuesdayDrawValue;
    }
    protected HtmlInputText getTextTuesdayReturnValue() {
        if (textTuesdayReturnValue == null) {
            textTuesdayReturnValue = (HtmlInputText) findComponentInRoot("textTuesdayReturnValue");
        }
        return textTuesdayReturnValue;
    }
    protected HtmlJspPanel getJspPanelTuesdayColHeaderPanel() {
        if (jspPanelTuesdayColHeaderPanel == null) {
            jspPanelTuesdayColHeaderPanel = (HtmlJspPanel) findComponentInRoot("jspPanelTuesdayColHeaderPanel");
        }
        return jspPanelTuesdayColHeaderPanel;
    }
    protected HtmlOutputText getTextTuesdayDrawMngmtHeaderLabel() {
        if (textTuesdayDrawMngmtHeaderLabel == null) {
            textTuesdayDrawMngmtHeaderLabel = (HtmlOutputText) findComponentInRoot("textTuesdayDrawMngmtHeaderLabel");
        }
        return textTuesdayDrawMngmtHeaderLabel;
    }
    protected HtmlOutputText getTextTuesdayDrawHeaderLabel() {
        if (textTuesdayDrawHeaderLabel == null) {
            textTuesdayDrawHeaderLabel = (HtmlOutputText) findComponentInRoot("textTuesdayDrawHeaderLabel");
        }
        return textTuesdayDrawHeaderLabel;
    }
    protected HtmlOutputText getTextTuesdayReturnsHeaderLabel() {
        if (textTuesdayReturnsHeaderLabel == null) {
            textTuesdayReturnsHeaderLabel = (HtmlOutputText) findComponentInRoot("textTuesdayReturnsHeaderLabel");
        }
        return textTuesdayReturnsHeaderLabel;
    }
    protected HtmlJspPanel getJspPanelWednesdayDrawMngmtPanel() {
        if (jspPanelWednesdayDrawMngmtPanel == null) {
            jspPanelWednesdayDrawMngmtPanel = (HtmlJspPanel) findComponentInRoot("jspPanelWednesdayDrawMngmtPanel");
        }
        return jspPanelWednesdayDrawMngmtPanel;
    }
    protected HtmlMessage getMessageWedDrawErr() {
        if (messageWedDrawErr == null) {
            messageWedDrawErr = (HtmlMessage) findComponentInRoot("messageWedDrawErr");
        }
        return messageWedDrawErr;
    }
    protected HtmlInputText getTextWednesdayDrawValue() {
        if (textWednesdayDrawValue == null) {
            textWednesdayDrawValue = (HtmlInputText) findComponentInRoot("textWednesdayDrawValue");
        }
        return textWednesdayDrawValue;
    }
    protected HtmlInputText getTextWednesdayReturnValue() {
        if (textWednesdayReturnValue == null) {
            textWednesdayReturnValue = (HtmlInputText) findComponentInRoot("textWednesdayReturnValue");
        }
        return textWednesdayReturnValue;
    }
    protected HtmlJspPanel getJspPanelWednesdayColHeaderPanel() {
        if (jspPanelWednesdayColHeaderPanel == null) {
            jspPanelWednesdayColHeaderPanel = (HtmlJspPanel) findComponentInRoot("jspPanelWednesdayColHeaderPanel");
        }
        return jspPanelWednesdayColHeaderPanel;
    }
    protected HtmlOutputText getText3() {
        if (text3 == null) {
            text3 = (HtmlOutputText) findComponentInRoot("text3");
        }
        return text3;
    }
    protected HtmlOutputText getTextWednesdayDrawHeaderLabel() {
        if (textWednesdayDrawHeaderLabel == null) {
            textWednesdayDrawHeaderLabel = (HtmlOutputText) findComponentInRoot("textWednesdayDrawHeaderLabel");
        }
        return textWednesdayDrawHeaderLabel;
    }
    protected HtmlOutputText getTextWednesdayReturnsHeaderLabel() {
        if (textWednesdayReturnsHeaderLabel == null) {
            textWednesdayReturnsHeaderLabel = (HtmlOutputText) findComponentInRoot("textWednesdayReturnsHeaderLabel");
        }
        return textWednesdayReturnsHeaderLabel;
    }
    protected HtmlJspPanel getJspPanelThursdayDrawMngmtPanel() {
        if (jspPanelThursdayDrawMngmtPanel == null) {
            jspPanelThursdayDrawMngmtPanel = (HtmlJspPanel) findComponentInRoot("jspPanelThursdayDrawMngmtPanel");
        }
        return jspPanelThursdayDrawMngmtPanel;
    }
    protected HtmlMessage getMessageThurDrawErr() {
        if (messageThurDrawErr == null) {
            messageThurDrawErr = (HtmlMessage) findComponentInRoot("messageThurDrawErr");
        }
        return messageThurDrawErr;
    }
    protected HtmlInputText getTextThursdayDrawValue() {
        if (textThursdayDrawValue == null) {
            textThursdayDrawValue = (HtmlInputText) findComponentInRoot("textThursdayDrawValue");
        }
        return textThursdayDrawValue;
    }
    protected HtmlInputText getTextThursdayReturnValue() {
        if (textThursdayReturnValue == null) {
            textThursdayReturnValue = (HtmlInputText) findComponentInRoot("textThursdayReturnValue");
        }
        return textThursdayReturnValue;
    }
    protected HtmlJspPanel getJspPanelThursdayColHeaderPanel() {
        if (jspPanelThursdayColHeaderPanel == null) {
            jspPanelThursdayColHeaderPanel = (HtmlJspPanel) findComponentInRoot("jspPanelThursdayColHeaderPanel");
        }
        return jspPanelThursdayColHeaderPanel;
    }
    protected HtmlOutputText getText4() {
        if (text4 == null) {
            text4 = (HtmlOutputText) findComponentInRoot("text4");
        }
        return text4;
    }
    protected HtmlOutputText getTextThursdayDrawHeaderLabel() {
        if (textThursdayDrawHeaderLabel == null) {
            textThursdayDrawHeaderLabel = (HtmlOutputText) findComponentInRoot("textThursdayDrawHeaderLabel");
        }
        return textThursdayDrawHeaderLabel;
    }
    protected HtmlOutputText getTextThursdayReturnsHeaderLabel() {
        if (textThursdayReturnsHeaderLabel == null) {
            textThursdayReturnsHeaderLabel = (HtmlOutputText) findComponentInRoot("textThursdayReturnsHeaderLabel");
        }
        return textThursdayReturnsHeaderLabel;
    }
    protected HtmlJspPanel getJspPanelFridayDrawMngmtPanel() {
        if (jspPanelFridayDrawMngmtPanel == null) {
            jspPanelFridayDrawMngmtPanel = (HtmlJspPanel) findComponentInRoot("jspPanelFridayDrawMngmtPanel");
        }
        return jspPanelFridayDrawMngmtPanel;
    }
    protected HtmlMessage getMessageFriDrawErr() {
        if (messageFriDrawErr == null) {
            messageFriDrawErr = (HtmlMessage) findComponentInRoot("messageFriDrawErr");
        }
        return messageFriDrawErr;
    }
    protected HtmlInputText getTextFridayDrawValue() {
        if (textFridayDrawValue == null) {
            textFridayDrawValue = (HtmlInputText) findComponentInRoot("textFridayDrawValue");
        }
        return textFridayDrawValue;
    }
    protected HtmlInputText getTextFridayReturnValue() {
        if (textFridayReturnValue == null) {
            textFridayReturnValue = (HtmlInputText) findComponentInRoot("textFridayReturnValue");
        }
        return textFridayReturnValue;
    }
    protected HtmlJspPanel getJspPanelFridayColHeaderPanel() {
        if (jspPanelFridayColHeaderPanel == null) {
            jspPanelFridayColHeaderPanel = (HtmlJspPanel) findComponentInRoot("jspPanelFridayColHeaderPanel");
        }
        return jspPanelFridayColHeaderPanel;
    }
    protected HtmlOutputText getText5() {
        if (text5 == null) {
            text5 = (HtmlOutputText) findComponentInRoot("text5");
        }
        return text5;
    }
    protected HtmlOutputText getTextFridayDrawHeaderLabel() {
        if (textFridayDrawHeaderLabel == null) {
            textFridayDrawHeaderLabel = (HtmlOutputText) findComponentInRoot("textFridayDrawHeaderLabel");
        }
        return textFridayDrawHeaderLabel;
    }
    protected HtmlOutputText getTextFridayReturnsHeaderLabel() {
        if (textFridayReturnsHeaderLabel == null) {
            textFridayReturnsHeaderLabel = (HtmlOutputText) findComponentInRoot("textFridayReturnsHeaderLabel");
        }
        return textFridayReturnsHeaderLabel;
    }
    protected HtmlJspPanel getJspPanelSaturdayDrawMngmtPanel() {
        if (jspPanelSaturdayDrawMngmtPanel == null) {
            jspPanelSaturdayDrawMngmtPanel = (HtmlJspPanel) findComponentInRoot("jspPanelSaturdayDrawMngmtPanel");
        }
        return jspPanelSaturdayDrawMngmtPanel;
    }
    protected HtmlMessage getMessageSatDrawErr() {
        if (messageSatDrawErr == null) {
            messageSatDrawErr = (HtmlMessage) findComponentInRoot("messageSatDrawErr");
        }
        return messageSatDrawErr;
    }
    protected HtmlInputText getTextSaturdayDrawValue() {
        if (textSaturdayDrawValue == null) {
            textSaturdayDrawValue = (HtmlInputText) findComponentInRoot("textSaturdayDrawValue");
        }
        return textSaturdayDrawValue;
    }
    protected HtmlInputText getTextSaturdayReturnValue() {
        if (textSaturdayReturnValue == null) {
            textSaturdayReturnValue = (HtmlInputText) findComponentInRoot("textSaturdayReturnValue");
        }
        return textSaturdayReturnValue;
    }
    protected HtmlJspPanel getJspPanelSaturdayColHeaderPanel() {
        if (jspPanelSaturdayColHeaderPanel == null) {
            jspPanelSaturdayColHeaderPanel = (HtmlJspPanel) findComponentInRoot("jspPanelSaturdayColHeaderPanel");
        }
        return jspPanelSaturdayColHeaderPanel;
    }
    protected HtmlOutputText getText6() {
        if (text6 == null) {
            text6 = (HtmlOutputText) findComponentInRoot("text6");
        }
        return text6;
    }
    protected HtmlOutputText getTextSaturdayDrawHeaderLabel() {
        if (textSaturdayDrawHeaderLabel == null) {
            textSaturdayDrawHeaderLabel = (HtmlOutputText) findComponentInRoot("textSaturdayDrawHeaderLabel");
        }
        return textSaturdayDrawHeaderLabel;
    }
    protected HtmlOutputText getTextSaturdayReturnsHeaderLabel() {
        if (textSaturdayReturnsHeaderLabel == null) {
            textSaturdayReturnsHeaderLabel = (HtmlOutputText) findComponentInRoot("textSaturdayReturnsHeaderLabel");
        }
        return textSaturdayReturnsHeaderLabel;
    }
    protected HtmlJspPanel getJspPanelSundayDrawMngmtPanel() {
        if (jspPanelSundayDrawMngmtPanel == null) {
            jspPanelSundayDrawMngmtPanel = (HtmlJspPanel) findComponentInRoot("jspPanelSundayDrawMngmtPanel");
        }
        return jspPanelSundayDrawMngmtPanel;
    }
    protected HtmlMessage getMessageSunDrawErr() {
        if (messageSunDrawErr == null) {
            messageSunDrawErr = (HtmlMessage) findComponentInRoot("messageSunDrawErr");
        }
        return messageSunDrawErr;
    }
    protected HtmlInputText getTextSundayDrawValue() {
        if (textSundayDrawValue == null) {
            textSundayDrawValue = (HtmlInputText) findComponentInRoot("textSundayDrawValue");
        }
        return textSundayDrawValue;
    }
    protected HtmlInputText getTextSundayReturnValue() {
        if (textSundayReturnValue == null) {
            textSundayReturnValue = (HtmlInputText) findComponentInRoot("textSundayReturnValue");
        }
        return textSundayReturnValue;
    }
    protected HtmlJspPanel getJspPanelSundayColHeaderPanel() {
        if (jspPanelSundayColHeaderPanel == null) {
            jspPanelSundayColHeaderPanel = (HtmlJspPanel) findComponentInRoot("jspPanelSundayColHeaderPanel");
        }
        return jspPanelSundayColHeaderPanel;
    }
    protected HtmlOutputText getText7() {
        if (text7 == null) {
            text7 = (HtmlOutputText) findComponentInRoot("text7");
        }
        return text7;
    }
    protected HtmlOutputText getTextSundayDrawHeaderLabel() {
        if (textSundayDrawHeaderLabel == null) {
            textSundayDrawHeaderLabel = (HtmlOutputText) findComponentInRoot("textSundayDrawHeaderLabel");
        }
        return textSundayDrawHeaderLabel;
    }
    protected HtmlOutputText getTextSundayReturnsHeaderLabel() {
        if (textSundayReturnsHeaderLabel == null) {
            textSundayReturnsHeaderLabel = (HtmlOutputText) findComponentInRoot("textSundayReturnsHeaderLabel");
        }
        return textSundayReturnsHeaderLabel;
    }
    protected HtmlPanelBox getBox1FooterNav() {
        if (box1FooterNav == null) {
            box1FooterNav = (HtmlPanelBox) findComponentInRoot("box1FooterNav");
        }
        return box1FooterNav;
    }
    protected HtmlPanelLayout getLayout1() {
        if (layout1 == null) {
            layout1 = (HtmlPanelLayout) findComponentInRoot("layout1");
        }
        return layout1;
    }
    protected HtmlOutputStatistics getStatistics1() {
        if (statistics1 == null) {
            statistics1 = (HtmlOutputStatistics) findComponentInRoot("statistics1");
        }
        return statistics1;
    }
    protected HtmlPagerWeb getWeb1() {
        if (web1 == null) {
            web1 = (HtmlPagerWeb) findComponentInRoot("web1");
        }
        return web1;
    }
    protected HtmlCommandExButton getTabbedPanel1_back() {
        if (tabbedPanel1_back == null) {
            tabbedPanel1_back = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_back");
        }
        return tabbedPanel1_back;
    }
    protected HtmlCommandExButton getTabbedPanel1_next() {
        if (tabbedPanel1_next == null) {
            tabbedPanel1_next = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_next");
        }
        return tabbedPanel1_next;
    }
    protected HtmlCommandExButton getTabbedPanel1_finish() {
        if (tabbedPanel1_finish == null) {
            tabbedPanel1_finish = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_finish");
        }
        return tabbedPanel1_finish;
    }
    protected HtmlCommandExButton getTabbedPanel1_cancel() {
        if (tabbedPanel1_cancel == null) {
            tabbedPanel1_cancel = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_cancel");
        }
        return tabbedPanel1_cancel;
    }
    protected UIColumn getColumnLocDataTableProductOrders() {
        if (columnLocDataTableProductOrders == null) {
            columnLocDataTableProductOrders = (UIColumn) findComponentInRoot("columnLocDataTableProductOrders");
        }
        return columnLocDataTableProductOrders;
    }
    protected UIColumn getColumn2() {
        if (column2 == null) {
            column2 = (UIColumn) findComponentInRoot("column2");
        }
        return column2;
    }
    protected UIColumn getColumn1() {
        if (column1 == null) {
            column1 = (UIColumn) findComponentInRoot("column1");
        }
        return column1;
    }
    protected HtmlMessage getMessageMonReturnErr() {
        if (messageMonReturnErr == null) {
            messageMonReturnErr = (HtmlMessage) findComponentInRoot("messageMonReturnErr");
        }
        return messageMonReturnErr;
    }
    protected HtmlInputHidden getHiddenMonZeroDrawReasonCode() {
        if (hiddenMonZeroDrawReasonCode == null) {
            hiddenMonZeroDrawReasonCode = (HtmlInputHidden) findComponentInRoot("hiddenMonZeroDrawReasonCode");
        }
        return hiddenMonZeroDrawReasonCode;
    }
    protected UIColumn getColumn3() {
        if (column3 == null) {
            column3 = (UIColumn) findComponentInRoot("column3");
        }
        return column3;
    }
    protected HtmlMessage getMessageTuesReturnErr() {
        if (messageTuesReturnErr == null) {
            messageTuesReturnErr = (HtmlMessage) findComponentInRoot("messageTuesReturnErr");
        }
        return messageTuesReturnErr;
    }
    protected HtmlInputHidden getHiddenTuesZeroDrawReasonCode() {
        if (hiddenTuesZeroDrawReasonCode == null) {
            hiddenTuesZeroDrawReasonCode = (HtmlInputHidden) findComponentInRoot("hiddenTuesZeroDrawReasonCode");
        }
        return hiddenTuesZeroDrawReasonCode;
    }
    protected UIColumn getColumn4() {
        if (column4 == null) {
            column4 = (UIColumn) findComponentInRoot("column4");
        }
        return column4;
    }
    protected HtmlMessage getMessageWedReturnErr() {
        if (messageWedReturnErr == null) {
            messageWedReturnErr = (HtmlMessage) findComponentInRoot("messageWedReturnErr");
        }
        return messageWedReturnErr;
    }
    protected HtmlInputHidden getHiddenWedZeroDrawReasonCode() {
        if (hiddenWedZeroDrawReasonCode == null) {
            hiddenWedZeroDrawReasonCode = (HtmlInputHidden) findComponentInRoot("hiddenWedZeroDrawReasonCode");
        }
        return hiddenWedZeroDrawReasonCode;
    }
    protected UIColumn getColumn5() {
        if (column5 == null) {
            column5 = (UIColumn) findComponentInRoot("column5");
        }
        return column5;
    }
    protected HtmlMessage getMessageThurReturnErr() {
        if (messageThurReturnErr == null) {
            messageThurReturnErr = (HtmlMessage) findComponentInRoot("messageThurReturnErr");
        }
        return messageThurReturnErr;
    }
    protected HtmlInputHidden getHiddenThurZeroDrawReasonCode() {
        if (hiddenThurZeroDrawReasonCode == null) {
            hiddenThurZeroDrawReasonCode = (HtmlInputHidden) findComponentInRoot("hiddenThurZeroDrawReasonCode");
        }
        return hiddenThurZeroDrawReasonCode;
    }
    protected UIColumn getColumn6() {
        if (column6 == null) {
            column6 = (UIColumn) findComponentInRoot("column6");
        }
        return column6;
    }
    protected HtmlMessage getMessageFriReturnErr() {
        if (messageFriReturnErr == null) {
            messageFriReturnErr = (HtmlMessage) findComponentInRoot("messageFriReturnErr");
        }
        return messageFriReturnErr;
    }
    protected HtmlInputHidden getHiddenFriZeroDrawReasonCode() {
        if (hiddenFriZeroDrawReasonCode == null) {
            hiddenFriZeroDrawReasonCode = (HtmlInputHidden) findComponentInRoot("hiddenFriZeroDrawReasonCode");
        }
        return hiddenFriZeroDrawReasonCode;
    }
    protected UIColumn getColumn7() {
        if (column7 == null) {
            column7 = (UIColumn) findComponentInRoot("column7");
        }
        return column7;
    }
    protected HtmlMessage getMessageSatReturnErr() {
        if (messageSatReturnErr == null) {
            messageSatReturnErr = (HtmlMessage) findComponentInRoot("messageSatReturnErr");
        }
        return messageSatReturnErr;
    }
    protected HtmlInputHidden getHiddenSatZeroDrawReasonCode() {
        if (hiddenSatZeroDrawReasonCode == null) {
            hiddenSatZeroDrawReasonCode = (HtmlInputHidden) findComponentInRoot("hiddenSatZeroDrawReasonCode");
        }
        return hiddenSatZeroDrawReasonCode;
    }
    protected UIColumn getColumn8() {
        if (column8 == null) {
            column8 = (UIColumn) findComponentInRoot("column8");
        }
        return column8;
    }
    protected HtmlMessage getMessageSunReturnErr() {
        if (messageSunReturnErr == null) {
            messageSunReturnErr = (HtmlMessage) findComponentInRoot("messageSunReturnErr");
        }
        return messageSunReturnErr;
    }
    protected HtmlInputHidden getHiddenSunZeroDrawReasonCode() {
        if (hiddenSunZeroDrawReasonCode == null) {
            hiddenSunZeroDrawReasonCode = (HtmlInputHidden) findComponentInRoot("hiddenSunZeroDrawReasonCode");
        }
        return hiddenSunZeroDrawReasonCode;
    }
    protected HtmlCommandExButton getButtonSubmitWeeklyDrawChanges() {
        if (buttonSubmitWeeklyDrawChanges == null) {
            buttonSubmitWeeklyDrawChanges = (HtmlCommandExButton) findComponentInRoot("buttonSubmitWeeklyDrawChanges");
        }
        return buttonSubmitWeeklyDrawChanges;
    }
    protected HtmlCommandExButton getButtonResetAllValues() {
        if (buttonResetAllValues == null) {
            buttonResetAllValues = (HtmlCommandExButton) findComponentInRoot("buttonResetAllValues");
        }
        return buttonResetAllValues;
    }
    protected HtmlCommandExButton getButtonQuitWithoutSavingWeeklyDraw() {
        if (buttonQuitWithoutSavingWeeklyDraw == null) {
            buttonQuitWithoutSavingWeeklyDraw = (HtmlCommandExButton) findComponentInRoot("buttonQuitWithoutSavingWeeklyDraw");
        }
        return buttonQuitWithoutSavingWeeklyDraw;
    }
    protected HtmlInputHelperKeybind getInputHelperKeybind1() {
        if (inputHelperKeybind1 == null) {
            inputHelperKeybind1 = (HtmlInputHelperKeybind) findComponentInRoot("inputHelperKeybind1");
        }
        return inputHelperKeybind1;
    }
    protected HtmlMessage getMessage1() {
        if (message1 == null) {
            message1 = (HtmlMessage) findComponentInRoot("message1");
        }
        return message1;
    }
    protected HtmlOutputText getTextNotEdittable() {
        if (textNotEdittable == null) {
            textNotEdittable = (HtmlOutputText) findComponentInRoot("textNotEdittable");
        }
        return textNotEdittable;
    }
	/** 
	 * @managed-bean true
	 */
	protected RDLForWeekHandler getRDLForWeek() {
		if (RDLForWeek == null) {
			RDLForWeek = (RDLForWeekHandler) getManagedBean("RDLForWeek");
		}
		return RDLForWeek;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setRDLForWeek(RDLForWeekHandler RDLForWeek) {
		this.RDLForWeek = RDLForWeek;
	}
}
/*
 * Created on Jun 14, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.drawManagement.forweek;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlOutputSeparator;
/**
 * @author aeast
 * @date Jun 14, 2007
 * @class forweekWeeklyHelpFile
 * 
 * 
 * 
 */
public class WeeklyHelpFile extends PageCodeBase {

    protected HtmlPanelBox QuestionBoxHeader;
    protected HtmlPanelBox QuestionAnswerBox;
    protected HtmlScriptCollector scriptCollector1;
    protected HtmlOutputLinkEx linkExCloseWindowTop;
    protected HtmlOutputText textCloseWindowTop;
    protected HtmlScriptCollector scriptCollector2;
    protected HtmlOutputText textDMHelpHeader;
    protected HtmlOutputLinkEx linkExHQ1;
    protected HtmlOutputText textQ1H;
    protected HtmlOutputLinkEx linkExHQ2;
    protected HtmlOutputText textQ2H;
    protected HtmlOutputLinkEx linkExHQ3;
    protected HtmlOutputText textQ3H;
    protected HtmlOutputLinkEx linkExHQ4;
    protected HtmlOutputText textQ4H;
    protected HtmlOutputLinkEx linkExHQ5;
    protected HtmlOutputText textQ5H;
    protected HtmlOutputSeparator separator1;
    protected HtmlOutputText textQ1;
    protected HtmlOutputText textBTP1;
    protected HtmlOutputText textBTP2;
    protected HtmlOutputText textBTP3;
    protected HtmlOutputText textBTP4;
    protected HtmlOutputText textBTP5;
    protected HtmlOutputText textA1;
    protected HtmlOutputLinkEx linkExBTP1;
    protected HtmlOutputText textQ2;
    protected HtmlOutputText textA2;
    protected HtmlOutputLinkEx linkExBTP2;
    protected HtmlOutputText textQ3;
    protected HtmlOutputText textA3;
    protected HtmlOutputLinkEx linkExBTP3;
    protected HtmlOutputText textQ4;
    protected HtmlOutputText textA4;
    protected HtmlOutputLinkEx linkExBTP4;
    protected HtmlOutputText textQ5;
    protected HtmlOutputText textA5;
    protected HtmlOutputLinkEx linkExBTP5;
    protected HtmlOutputLinkEx linkExCloseWindowBottom;
    protected HtmlOutputText textCloseWindowBottom;
    protected HtmlOutputLinkEx linkExHQ6;
    protected HtmlOutputText textQ6H;
    protected HtmlOutputText textQ6;
    protected HtmlOutputText textA6;
    protected HtmlOutputLinkEx linkExBTP6;
    protected HtmlOutputText textBTP6;
    protected HtmlOutputText textQ7;
    protected HtmlOutputText textA7;
    protected HtmlOutputLinkEx linkExBTP7;
    protected HtmlOutputText textBTP7;
    protected HtmlOutputLinkEx linkExHQ7;
    protected HtmlOutputText textQ7H;
    protected HtmlOutputLinkEx linkExHQ8;
    protected HtmlOutputText textQ8H;
    protected HtmlOutputText textQ8;
    protected HtmlOutputText textA8;
    protected HtmlOutputLinkEx linkExBTP8;
    protected HtmlOutputText textBTP8;
    protected HtmlOutputText textQ9;
    protected HtmlOutputText textA9;
    protected HtmlOutputLinkEx linkExBTP9;
    protected HtmlOutputText textBTP9;
    protected HtmlOutputLinkEx linkExHQ9;
    protected HtmlOutputText textQ9H;
    protected HtmlOutputLinkEx linkExHQ10;
    protected HtmlOutputText textQ10H;
    protected HtmlOutputText textQ10;
    protected HtmlOutputText textA10;
    protected HtmlOutputLinkEx linkExBTP10;
    protected HtmlOutputText textBTP10;
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlOutputLinkEx getLinkExCloseWindowTop() {
        if (linkExCloseWindowTop == null) {
            linkExCloseWindowTop = (HtmlOutputLinkEx) findComponentInRoot("linkExCloseWindowTop");
        }
        return linkExCloseWindowTop;
    }
    protected HtmlOutputText getTextCloseWindowTop() {
        if (textCloseWindowTop == null) {
            textCloseWindowTop = (HtmlOutputText) findComponentInRoot("textCloseWindowTop");
        }
        return textCloseWindowTop;
    }
    protected HtmlScriptCollector getScriptCollector2() {
        if (scriptCollector2 == null) {
            scriptCollector2 = (HtmlScriptCollector) findComponentInRoot("scriptCollector2");
        }
        return scriptCollector2;
    }
    protected HtmlOutputText getTextDMHelpHeader() {
        if (textDMHelpHeader == null) {
            textDMHelpHeader = (HtmlOutputText) findComponentInRoot("textDMHelpHeader");
        }
        return textDMHelpHeader;
    }
    protected HtmlOutputLinkEx getLinkExHQ1() {
        if (linkExHQ1 == null) {
            linkExHQ1 = (HtmlOutputLinkEx) findComponentInRoot("linkExHQ1");
        }
        return linkExHQ1;
    }
    protected HtmlOutputText getTextQ1H() {
        if (textQ1H == null) {
            textQ1H = (HtmlOutputText) findComponentInRoot("textQ1H");
        }
        return textQ1H;
    }
    protected HtmlOutputLinkEx getLinkExHQ2() {
        if (linkExHQ2 == null) {
            linkExHQ2 = (HtmlOutputLinkEx) findComponentInRoot("linkExHQ2");
        }
        return linkExHQ2;
    }
    protected HtmlOutputText getTextQ2H() {
        if (textQ2H == null) {
            textQ2H = (HtmlOutputText) findComponentInRoot("textQ2H");
        }
        return textQ2H;
    }
    protected HtmlOutputLinkEx getLinkExHQ3() {
        if (linkExHQ3 == null) {
            linkExHQ3 = (HtmlOutputLinkEx) findComponentInRoot("linkExHQ3");
        }
        return linkExHQ3;
    }
    protected HtmlOutputText getTextQ3H() {
        if (textQ3H == null) {
            textQ3H = (HtmlOutputText) findComponentInRoot("textQ3H");
        }
        return textQ3H;
    }
    protected HtmlOutputLinkEx getLinkExHQ4() {
        if (linkExHQ4 == null) {
            linkExHQ4 = (HtmlOutputLinkEx) findComponentInRoot("linkExHQ4");
        }
        return linkExHQ4;
    }
    protected HtmlOutputText getTextQ4H() {
        if (textQ4H == null) {
            textQ4H = (HtmlOutputText) findComponentInRoot("textQ4H");
        }
        return textQ4H;
    }
    protected HtmlOutputLinkEx getLinkExHQ5() {
        if (linkExHQ5 == null) {
            linkExHQ5 = (HtmlOutputLinkEx) findComponentInRoot("linkExHQ5");
        }
        return linkExHQ5;
    }
    protected HtmlOutputText getTextQ5H() {
        if (textQ5H == null) {
            textQ5H = (HtmlOutputText) findComponentInRoot("textQ5H");
        }
        return textQ5H;
    }
    protected HtmlOutputSeparator getSeparator1() {
        if (separator1 == null) {
            separator1 = (HtmlOutputSeparator) findComponentInRoot("separator1");
        }
        return separator1;
    }
    protected HtmlOutputText getTextQ1() {
        if (textQ1 == null) {
            textQ1 = (HtmlOutputText) findComponentInRoot("textQ1");
        }
        return textQ1;
    }
    protected HtmlOutputText getTextBTP1() {
        if (textBTP1 == null) {
            textBTP1 = (HtmlOutputText) findComponentInRoot("textBTP1");
        }
        return textBTP1;
    }
    protected HtmlOutputText getTextBTP2() {
        if (textBTP2 == null) {
            textBTP2 = (HtmlOutputText) findComponentInRoot("textBTP2");
        }
        return textBTP2;
    }
    protected HtmlOutputText getTextBTP3() {
        if (textBTP3 == null) {
            textBTP3 = (HtmlOutputText) findComponentInRoot("textBTP3");
        }
        return textBTP3;
    }
    protected HtmlOutputText getTextBTP4() {
        if (textBTP4 == null) {
            textBTP4 = (HtmlOutputText) findComponentInRoot("textBTP4");
        }
        return textBTP4;
    }
    protected HtmlOutputText getTextBTP5() {
        if (textBTP5 == null) {
            textBTP5 = (HtmlOutputText) findComponentInRoot("textBTP5");
        }
        return textBTP5;
    }
    protected HtmlOutputText getTextA1() {
        if (textA1 == null) {
            textA1 = (HtmlOutputText) findComponentInRoot("textA1");
        }
        return textA1;
    }
    protected HtmlOutputLinkEx getLinkExBTP1() {
        if (linkExBTP1 == null) {
            linkExBTP1 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP1");
        }
        return linkExBTP1;
    }
    protected HtmlOutputText getTextQ2() {
        if (textQ2 == null) {
            textQ2 = (HtmlOutputText) findComponentInRoot("textQ2");
        }
        return textQ2;
    }
    protected HtmlOutputText getTextA2() {
        if (textA2 == null) {
            textA2 = (HtmlOutputText) findComponentInRoot("textA2");
        }
        return textA2;
    }
    protected HtmlOutputLinkEx getLinkExBTP2() {
        if (linkExBTP2 == null) {
            linkExBTP2 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP2");
        }
        return linkExBTP2;
    }
    protected HtmlOutputText getTextQ3() {
        if (textQ3 == null) {
            textQ3 = (HtmlOutputText) findComponentInRoot("textQ3");
        }
        return textQ3;
    }
    protected HtmlOutputText getTextA3() {
        if (textA3 == null) {
            textA3 = (HtmlOutputText) findComponentInRoot("textA3");
        }
        return textA3;
    }
    protected HtmlOutputLinkEx getLinkExBTP3() {
        if (linkExBTP3 == null) {
            linkExBTP3 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP3");
        }
        return linkExBTP3;
    }
    protected HtmlOutputText getTextQ4() {
        if (textQ4 == null) {
            textQ4 = (HtmlOutputText) findComponentInRoot("textQ4");
        }
        return textQ4;
    }
    protected HtmlOutputText getTextA4() {
        if (textA4 == null) {
            textA4 = (HtmlOutputText) findComponentInRoot("textA4");
        }
        return textA4;
    }
    protected HtmlOutputLinkEx getLinkExBTP4() {
        if (linkExBTP4 == null) {
            linkExBTP4 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP4");
        }
        return linkExBTP4;
    }
    protected HtmlOutputText getTextQ5() {
        if (textQ5 == null) {
            textQ5 = (HtmlOutputText) findComponentInRoot("textQ5");
        }
        return textQ5;
    }
    protected HtmlOutputText getTextA5() {
        if (textA5 == null) {
            textA5 = (HtmlOutputText) findComponentInRoot("textA5");
        }
        return textA5;
    }
    protected HtmlOutputLinkEx getLinkExBTP5() {
        if (linkExBTP5 == null) {
            linkExBTP5 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP5");
        }
        return linkExBTP5;
    }
    protected HtmlOutputLinkEx getLinkExCloseWindowBottom() {
        if (linkExCloseWindowBottom == null) {
            linkExCloseWindowBottom = (HtmlOutputLinkEx) findComponentInRoot("linkExCloseWindowBottom");
        }
        return linkExCloseWindowBottom;
    }
    protected HtmlOutputText getTextCloseWindowBottom() {
        if (textCloseWindowBottom == null) {
            textCloseWindowBottom = (HtmlOutputText) findComponentInRoot("textCloseWindowBottom");
        }
        return textCloseWindowBottom;
    }
    protected HtmlOutputLinkEx getLinkExHQ6() {
        if (linkExHQ6 == null) {
            linkExHQ6 = (HtmlOutputLinkEx) findComponentInRoot("linkExHQ6");
        }
        return linkExHQ6;
    }
    protected HtmlOutputText getTextQ6H() {
        if (textQ6H == null) {
            textQ6H = (HtmlOutputText) findComponentInRoot("textQ6H");
        }
        return textQ6H;
    }
    protected HtmlOutputText getTextQ6() {
        if (textQ6 == null) {
            textQ6 = (HtmlOutputText) findComponentInRoot("textQ6");
        }
        return textQ6;
    }
    protected HtmlOutputText getTextA6() {
        if (textA6 == null) {
            textA6 = (HtmlOutputText) findComponentInRoot("textA6");
        }
        return textA6;
    }
    protected HtmlOutputLinkEx getLinkExBTP6() {
        if (linkExBTP6 == null) {
            linkExBTP6 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP6");
        }
        return linkExBTP6;
    }
    protected HtmlOutputText getTextBTP6() {
        if (textBTP6 == null) {
            textBTP6 = (HtmlOutputText) findComponentInRoot("textBTP6");
        }
        return textBTP6;
    }
    protected HtmlOutputText getTextQ7() {
        if (textQ7 == null) {
            textQ7 = (HtmlOutputText) findComponentInRoot("textQ7");
        }
        return textQ7;
    }
    protected HtmlOutputText getTextA7() {
        if (textA7 == null) {
            textA7 = (HtmlOutputText) findComponentInRoot("textA7");
        }
        return textA7;
    }
    protected HtmlOutputLinkEx getLinkExBTP7() {
        if (linkExBTP7 == null) {
            linkExBTP7 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP7");
        }
        return linkExBTP7;
    }
    protected HtmlOutputText getTextBTP7() {
        if (textBTP7 == null) {
            textBTP7 = (HtmlOutputText) findComponentInRoot("textBTP7");
        }
        return textBTP7;
    }
    protected HtmlOutputLinkEx getLinkExHQ7() {
        if (linkExHQ7 == null) {
            linkExHQ7 = (HtmlOutputLinkEx) findComponentInRoot("linkExHQ7");
        }
        return linkExHQ7;
    }
    protected HtmlOutputText getTextQ7H() {
        if (textQ7H == null) {
            textQ7H = (HtmlOutputText) findComponentInRoot("textQ7H");
        }
        return textQ7H;
    }
    protected HtmlOutputLinkEx getLinkExHQ8() {
        if (linkExHQ8 == null) {
            linkExHQ8 = (HtmlOutputLinkEx) findComponentInRoot("linkExHQ8");
        }
        return linkExHQ8;
    }
    protected HtmlOutputText getTextQ8H() {
        if (textQ8H == null) {
            textQ8H = (HtmlOutputText) findComponentInRoot("textQ8H");
        }
        return textQ8H;
    }
    protected HtmlOutputText getTextQ8() {
        if (textQ8 == null) {
            textQ8 = (HtmlOutputText) findComponentInRoot("textQ8");
        }
        return textQ8;
    }
    protected HtmlOutputText getTextA8() {
        if (textA8 == null) {
            textA8 = (HtmlOutputText) findComponentInRoot("textA8");
        }
        return textA8;
    }
    protected HtmlOutputLinkEx getLinkExBTP8() {
        if (linkExBTP8 == null) {
            linkExBTP8 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP8");
        }
        return linkExBTP8;
    }
    protected HtmlOutputText getTextBTP8() {
        if (textBTP8 == null) {
            textBTP8 = (HtmlOutputText) findComponentInRoot("textBTP8");
        }
        return textBTP8;
    }
    protected HtmlOutputText getTextQ9() {
        if (textQ9 == null) {
            textQ9 = (HtmlOutputText) findComponentInRoot("textQ9");
        }
        return textQ9;
    }
    protected HtmlOutputText getTextA9() {
        if (textA9 == null) {
            textA9 = (HtmlOutputText) findComponentInRoot("textA9");
        }
        return textA9;
    }
    protected HtmlOutputLinkEx getLinkExBTP9() {
        if (linkExBTP9 == null) {
            linkExBTP9 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP9");
        }
        return linkExBTP9;
    }
    protected HtmlOutputText getTextBTP9() {
        if (textBTP9 == null) {
            textBTP9 = (HtmlOutputText) findComponentInRoot("textBTP9");
        }
        return textBTP9;
    }
    protected HtmlOutputLinkEx getLinkExHQ9() {
        if (linkExHQ9 == null) {
            linkExHQ9 = (HtmlOutputLinkEx) findComponentInRoot("linkExHQ9");
        }
        return linkExHQ9;
    }
    protected HtmlOutputText getTextQ9H() {
        if (textQ9H == null) {
            textQ9H = (HtmlOutputText) findComponentInRoot("textQ9H");
        }
        return textQ9H;
    }
    protected HtmlOutputLinkEx getLinkExHQ10() {
        if (linkExHQ10 == null) {
            linkExHQ10 = (HtmlOutputLinkEx) findComponentInRoot("linkExHQ10");
        }
        return linkExHQ10;
    }
    protected HtmlOutputText getTextQ10H() {
        if (textQ10H == null) {
            textQ10H = (HtmlOutputText) findComponentInRoot("textQ10H");
        }
        return textQ10H;
    }
    protected HtmlOutputText getTextQ10() {
        if (textQ10 == null) {
            textQ10 = (HtmlOutputText) findComponentInRoot("textQ10");
        }
        return textQ10;
    }
    protected HtmlOutputText getTextA10() {
        if (textA10 == null) {
            textA10 = (HtmlOutputText) findComponentInRoot("textA10");
        }
        return textA10;
    }
    protected HtmlOutputLinkEx getLinkExBTP10() {
        if (linkExBTP10 == null) {
            linkExBTP10 = (HtmlOutputLinkEx) findComponentInRoot("linkExBTP10");
        }
        return linkExBTP10;
    }
    protected HtmlOutputText getTextBTP10() {
        if (textBTP10 == null) {
            textBTP10 = (HtmlOutputText) findComponentInRoot("textBTP10");
        }
        return textBTP10;
    }
}
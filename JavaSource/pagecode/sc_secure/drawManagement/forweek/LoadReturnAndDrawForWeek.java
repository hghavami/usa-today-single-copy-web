/*
 * Created on Apr 27, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.drawManagement.forweek;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlScriptCollector;
/**
 * @author aeast
 * @date Apr 27, 2007
 * @class forweekLoadReturnAndDrawForWeek
 * 
 * 
 * 
 */
public class LoadReturnAndDrawForWeek extends PageCodeBase {

    protected HtmlOutputText textPleaseWait;
    protected HtmlOutputText textLoading1;
    protected HtmlOutputText textLoadingP2;
    protected HtmlOutputText textLoadingDisclaimer;
    protected HtmlGraphicImageEx imageExBusy;
    protected HtmlScriptCollector scriptCollector1;
    protected HtmlOutputText getTextPleaseWait() {
        if (textPleaseWait == null) {
            textPleaseWait = (HtmlOutputText) findComponentInRoot("textPleaseWait");
        }
        return textPleaseWait;
    }
    protected HtmlOutputText getTextLoading1() {
        if (textLoading1 == null) {
            textLoading1 = (HtmlOutputText) findComponentInRoot("textLoading1");
        }
        return textLoading1;
    }
    protected HtmlOutputText getTextLoadingP2() {
        if (textLoadingP2 == null) {
            textLoadingP2 = (HtmlOutputText) findComponentInRoot("textLoadingP2");
        }
        return textLoadingP2;
    }
    protected HtmlOutputText getTextLoadingDisclaimer() {
        if (textLoadingDisclaimer == null) {
            textLoadingDisclaimer = (HtmlOutputText) findComponentInRoot("textLoadingDisclaimer");
        }
        return textLoadingDisclaimer;
    }
    protected HtmlGraphicImageEx getImageExBusy() {
        if (imageExBusy == null) {
            imageExBusy = (HtmlGraphicImageEx) findComponentInRoot("imageExBusy");
        }
        return imageExBusy;
    }
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
}
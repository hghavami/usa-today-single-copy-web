/*
 * Created on Feb 27, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.drawManagement;

import pagecode.PageCodeBase;
import com.usatoday.champion.handlers.RouteHandler;
import com.usatoday.champion.handlers.RouteListHandler;
import com.usatoday.singlecopy.model.bo.drawmngmt.RouteBO;

import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.UINamingContainer;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.UIColumn;
import com.ibm.faces.component.html.HtmlCommandExRowAction;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlOutputStatistics;
import com.ibm.faces.component.html.HtmlPagerWeb;
/**
 * @author aeast
 * @date Feb 27, 2007
 * @class drawManagementRouteSummaryFrag
 * 
 * 
 * 
 */
public class RouteSummaryFrag extends PageCodeBase {

    protected HtmlMessages messages1;
    protected HtmlForm formRouteSummaryFrgmtForm;
    protected HtmlMessages messagesRouteSummaryFragMessages;
    protected HtmlOutputText text1;
    protected HtmlJspPanel jspPanelRouteSummaryFrag;
    protected HtmlScriptCollector scriptCollectorRouteSummaryFrag;
    protected UINamingContainer subviewRouteSummaryFrag;
    protected HtmlDataTable tableRouteListTableSummary;
    protected UIColumn columnRouteSummaryRouteID;
    protected HtmlOutputText textRouteSummaryRouteID;
    protected HtmlOutputText textRouteSummaryRouteDesc;
    protected HtmlCommandExRowAction rowActionRouteSummaryFrag;
    protected HtmlPanelBox boxRouteSummaryFooterPanel1;
    protected HtmlOutputStatistics statisticsRouteSummary12;
    protected HtmlPanelBox boxRoutSummary2;
    protected HtmlOutputText textRouteTableDescriptiveText;
    protected HtmlOutputText textRouteSummary193;
    protected UIColumn columnRouteSummaryRouteDesc;
    protected HtmlOutputText textRouteSummary196;
    protected UIColumn columnRouteSummaryCommandColumn;
    protected HtmlOutputText textNoRoutesHeader;
    protected HtmlOutputText textNoRoutesMessage;
    protected HtmlPagerWeb web1;
    protected UIColumn columnRouteSummaryMarketID;
    protected HtmlOutputText textRouteSummaryMarketID;
    protected HtmlOutputText textRouteSummary197;
    protected UIColumn columnRouteSummaryDistrictID;
    protected HtmlOutputText textRouteSummaryDistrictID;
    protected HtmlOutputText textRouteSummary198;
    @SuppressWarnings("unchecked")
	public String doRowAction1Action() {
        // 
        //     Get the index of the selected row
        //     int row = getRowAction1().getRowIndex();
        //
        //     Copy key values from the selection to the request so they can be used in a database filter
        //     For example, if the table has a column "keyvalue" and there is an SDO object that uses the
        //     filter "request.keyvalue", then this code sets up the request so the filter will work
        //     correctly
        //     For V5.1 server use:
        //     Object keyvalue = getRoutesAsCollection().getDataObjectAccessBean(row).get("keyvalue");
        //     For V6 server use:
        //     Object keyvalue = ((DataObject)getRoutesAsCollection().get(row)).get("keyvalue");
        //
        //     getRequestScope().put("keyvalue", keyvalue);
        //
        //     Specify the return value (a string) which is used by the navigation map to determine
        //     the next page to display
    	
		Object routeIDParam = getRowActionRouteSummaryFrag().getRowParameters().get("routeID");
		Object marketIDParam = getRowActionRouteSummaryFrag().getRowParameters().get("marketID");
		Object districtIDParam = getRowActionRouteSummaryFrag().getRowParameters().get("districtID");
		
		String routeIDKey = (String)marketIDParam + (String)districtIDParam + (String)routeIDParam;
		RouteListHandler rlh = (RouteListHandler)getSessionScope().get("routeListHandler");
		RouteBO r = rlh.getRoute(routeIDKey);
		RouteHandler rh = new RouteHandler();
		rh.setRoute(r);
		getSessionScope().remove("route");
		getSessionScope().put("route", rh);

        return "success";

    }
    protected HtmlForm getFormRouteSummaryFrgmtForm() {
        if (formRouteSummaryFrgmtForm == null) {
            formRouteSummaryFrgmtForm = (HtmlForm) findComponentInRoot("formRouteSummaryFrgmtForm");
        }
        return formRouteSummaryFrgmtForm;
    }
    protected HtmlMessages getMessagesRouteSummaryFragMessages() {
        if (messagesRouteSummaryFragMessages == null) {
            messagesRouteSummaryFragMessages = (HtmlMessages) findComponentInRoot("messagesRouteSummaryFragMessages");
        }
        return messagesRouteSummaryFragMessages;
    }
    protected HtmlOutputText getText1() {
        if (text1 == null) {
            text1 = (HtmlOutputText) findComponentInRoot("text1");
        }
        return text1;
    }
    protected HtmlJspPanel getJspPanelRouteSummaryFrag() {
        if (jspPanelRouteSummaryFrag == null) {
            jspPanelRouteSummaryFrag = (HtmlJspPanel) findComponentInRoot("jspPanelRouteSummaryFrag");
        }
        return jspPanelRouteSummaryFrag;
    }
    protected HtmlScriptCollector getScriptCollectorRouteSummaryFrag() {
        if (scriptCollectorRouteSummaryFrag == null) {
            scriptCollectorRouteSummaryFrag = (HtmlScriptCollector) findComponentInRoot("scriptCollectorRouteSummaryFrag");
        }
        return scriptCollectorRouteSummaryFrag;
    }
    protected UINamingContainer getSubviewRouteSummaryFrag() {
        if (subviewRouteSummaryFrag == null) {
            subviewRouteSummaryFrag = (UINamingContainer) findComponentInRoot("subviewRouteSummaryFrag");
        }
        return subviewRouteSummaryFrag;
    }
    protected HtmlDataTable getTableRouteListTableSummary() {
        if (tableRouteListTableSummary == null) {
            tableRouteListTableSummary = (HtmlDataTable) findComponentInRoot("tableRouteListTableSummary");
        }
        return tableRouteListTableSummary;
    }
    protected UIColumn getColumnRouteSummaryRouteID() {
        if (columnRouteSummaryRouteID == null) {
            columnRouteSummaryRouteID = (UIColumn) findComponentInRoot("columnRouteSummaryRouteID");
        }
        return columnRouteSummaryRouteID;
    }
    protected HtmlOutputText getTextRouteSummaryRouteID() {
        if (textRouteSummaryRouteID == null) {
            textRouteSummaryRouteID = (HtmlOutputText) findComponentInRoot("textRouteSummaryRouteID");
        }
        return textRouteSummaryRouteID;
    }
    protected HtmlOutputText getTextRouteSummaryRouteDesc() {
        if (textRouteSummaryRouteDesc == null) {
            textRouteSummaryRouteDesc = (HtmlOutputText) findComponentInRoot("textRouteSummaryRouteDesc");
        }
        return textRouteSummaryRouteDesc;
    }
    protected HtmlCommandExRowAction getRowActionRouteSummaryFrag() {
        if (rowActionRouteSummaryFrag == null) {
            rowActionRouteSummaryFrag = (HtmlCommandExRowAction) findComponentInRoot("rowActionRouteSummaryFrag");
        }
        return rowActionRouteSummaryFrag;
    }
    protected HtmlPanelBox getBoxRouteSummaryFooterPanel1() {
        if (boxRouteSummaryFooterPanel1 == null) {
            boxRouteSummaryFooterPanel1 = (HtmlPanelBox) findComponentInRoot("boxRouteSummaryFooterPanel1");
        }
        return boxRouteSummaryFooterPanel1;
    }
    protected HtmlOutputStatistics getStatisticsRouteSummary12() {
        if (statisticsRouteSummary12 == null) {
            statisticsRouteSummary12 = (HtmlOutputStatistics) findComponentInRoot("statisticsRouteSummary12");
        }
        return statisticsRouteSummary12;
    }
    protected HtmlPanelBox getBoxRoutSummary2() {
        if (boxRoutSummary2 == null) {
            boxRoutSummary2 = (HtmlPanelBox) findComponentInRoot("boxRoutSummary2");
        }
        return boxRoutSummary2;
    }
    protected HtmlOutputText getTextRouteTableDescriptiveText() {
        if (textRouteTableDescriptiveText == null) {
            textRouteTableDescriptiveText = (HtmlOutputText) findComponentInRoot("textRouteTableDescriptiveText");
        }
        return textRouteTableDescriptiveText;
    }
    protected HtmlOutputText getTextRouteSummary193() {
        if (textRouteSummary193 == null) {
            textRouteSummary193 = (HtmlOutputText) findComponentInRoot("textRouteSummary193");
        }
        return textRouteSummary193;
    }
    protected UIColumn getColumnRouteSummaryRouteDesc() {
        if (columnRouteSummaryRouteDesc == null) {
            columnRouteSummaryRouteDesc = (UIColumn) findComponentInRoot("columnRouteSummaryRouteDesc");
        }
        return columnRouteSummaryRouteDesc;
    }
    protected HtmlOutputText getTextRouteSummary196() {
        if (textRouteSummary196 == null) {
            textRouteSummary196 = (HtmlOutputText) findComponentInRoot("textRouteSummary196");
        }
        return textRouteSummary196;
    }
    protected UIColumn getColumnRouteSummaryCommandColumn() {
        if (columnRouteSummaryCommandColumn == null) {
            columnRouteSummaryCommandColumn = (UIColumn) findComponentInRoot("columnRouteSummaryCommandColumn");
        }
        return columnRouteSummaryCommandColumn;
    }
    protected HtmlOutputText getTextNoRoutesHeader() {
        if (textNoRoutesHeader == null) {
            textNoRoutesHeader = (HtmlOutputText) findComponentInRoot("textNoRoutesHeader");
        }
        return textNoRoutesHeader;
    }
    protected HtmlOutputText getTextNoRoutesMessage() {
        if (textNoRoutesMessage == null) {
            textNoRoutesMessage = (HtmlOutputText) findComponentInRoot("textNoRoutesMessage");
        }
        return textNoRoutesMessage;
    }
    protected HtmlPagerWeb getWeb1() {
        if (web1 == null) {
            web1 = (HtmlPagerWeb) findComponentInRoot("web1");
        }
        return web1;
    }
    protected UIColumn getColumnRouteSummaryMarketID() {
        if (columnRouteSummaryMarketID == null) {
            columnRouteSummaryMarketID = (UIColumn) findComponentInRoot("columnRouteSummaryMarketID");
        }
        return columnRouteSummaryMarketID;
    }
    protected HtmlOutputText getTextRouteSummaryMarketID() {
        if (textRouteSummaryMarketID == null) {
            textRouteSummaryMarketID = (HtmlOutputText) findComponentInRoot("textRouteSummaryMarketID");
        }
        return textRouteSummaryMarketID;
    }
    protected HtmlOutputText getTextRouteSummary197() {
        if (textRouteSummary197 == null) {
            textRouteSummary197 = (HtmlOutputText) findComponentInRoot("textRouteSummary197");
        }
        return textRouteSummary197;
    }
    protected UIColumn getColumnRouteSummaryDistrictID() {
        if (columnRouteSummaryDistrictID == null) {
            columnRouteSummaryDistrictID = (UIColumn) findComponentInRoot("columnRouteSummaryDistrictID");
        }
        return columnRouteSummaryDistrictID;
    }
    protected HtmlOutputText getTextRouteSummaryDistrictID() {
        if (textRouteSummaryDistrictID == null) {
            textRouteSummaryDistrictID = (HtmlOutputText) findComponentInRoot("textRouteSummaryDistrictID");
        }
        return textRouteSummaryDistrictID;
    }
    protected HtmlOutputText getTextRouteSummary198() {
        if (textRouteSummary198 == null) {
            textRouteSummary198 = (HtmlOutputText) findComponentInRoot("textRouteSummary198");
        }
        return textRouteSummary198;
    }
}
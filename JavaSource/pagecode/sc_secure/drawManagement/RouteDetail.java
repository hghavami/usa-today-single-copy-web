/*
 * Created on Feb 21, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.drawManagement;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;

import org.joda.time.DateTime;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.sun.faces.config.beans.LocaleConfigBean;
import com.usatoday.champion.handlers.DrawManagementUtilHandler;
import com.usatoday.champion.handlers.RDLForDayHandler;
import com.usatoday.champion.handlers.RouteHandler;
import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.singlecopy.model.bo.drawmngmt.RouteBO;
import com.usatoday.singlecopy.model.bo.users.UserBO;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.bf.component.html.HtmlBfPanel;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlPanelLayout;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.UIColumn;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessage;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.bf.component.html.HtmlTabbedPanel;
import com.ibm.faces.component.html.HtmlInputHelperDatePicker;
import com.ibm.faces.component.html.HtmlInputHelperKeybind;
/**
 * @author aeast
 * @date Feb 21, 2007
 * @class drawManagementRun_list_detail
 * 
 * 
 * 
 */
public class RouteDetail extends PageCodeBase {


    protected LocaleConfigBean userLocale;
    protected DrawManagementUtilHandler drawManagmentUtil;
    protected HtmlForm MostJSPConte;
    protected HtmlOutputText Descrip;
    protected HtmlOutputText tMessag;
    protected HtmlOutputText textLogoutLink;
    protected HtmlCommandLink linkLogout;
    protected HtmlForm formLogout;
    protected HtmlOutputFormat formatWelcomeMessage;
    protected HtmlForm form1;
    protected HtmlMessages messages1;
    protected HtmlCommandExButton buttonDoLogoutBtn;
	protected HtmlInputHidden IdMarketKey;
	protected RouteHandler route;
	protected UserHandler userHandler;
	protected HtmlScriptCollector templateScriptCollector;
	protected HtmlOutputText textGlobalMessageText;
	protected HtmlOutputText textMarketLabel;
	protected HtmlOutputText text4;
	protected HtmlOutputText textDistrictLabel;
	protected HtmlOutputText text3;
	protected HtmlOutputText textRouteIDLabel;
	protected HtmlOutputText textDetailRouteID;
	protected HtmlOutputText textDescriptionLabel;
	protected HtmlOutputText text2;
	protected HtmlInputHidden hiddenMktDstRteKey;
	protected HtmlGraphicImageEx imageExHelpIcon1;
	protected HtmlBfPanel bfpanelRteMsgPanel;
	protected HtmlJspPanel jspPanelTabbedPannelRteMsg;
	protected HtmlPanelLayout layout1;
	protected HtmlDataTable tableRouteBroadcasstMessages;
	protected UIColumn columnBMsgIdCol;
	protected HtmlJspPanel jspPanel1111111;
	protected HtmlOutputFormat formatMessageHeaderf1;
	protected HtmlOutputText text10;
	protected HtmlInputText textReturnDrawChosenDate;
	protected HtmlCommandExButton buttonEnterReturnDrawForDate;
	protected HtmlMessage messageBadReturnDrawDate;
	protected HtmlCommandExButton buttonRDLPrintFriendly;
	protected HtmlJspPanel jspPanelWeeklyReturnDrawOuterPanel;
	protected HtmlDataTable tablePublicationsForWeeklyReturnDraw;
	protected UIColumn columnWeeklyReturnDrawPubsCol;
	protected HtmlOutputLinkEx linkExWeeklyDRProdDescriptionLink;
	protected HtmlOutputText textWeeklyDrawReturnProductID;
	protected HtmlOutputText textWeeklyDrawRetPubHeader;
	protected HtmlMessage messageInvalidWeeklyReturnDrawMsg;
	protected HtmlInputText textWeekEndingDateValue;
	protected HtmlOutputText textWeeklyDrawReturnAvailWeekHeader;
	protected HtmlCommandExButton buttonWeeklyReturnDrawLaunch;
	protected HtmlCommandExButton tabbedPanelWeeklyReturnPanel1_back;
	protected HtmlCommandExButton tabbedPanelWeeklyReturnPanel1_next;
	protected HtmlCommandExButton tabbedPanelWeeklyReturnPanel1_finish;
	protected HtmlCommandExButton tabbedPanelWeeklyReturnPanel1_cancel;
	protected HtmlTabbedPanel tabbedPanelWeeklyReturnPanel;
	protected HtmlOutputText text6;
	protected UIColumn columnBMsgTxtCol;
	protected HtmlOutputText text8;
	protected HtmlBfPanel bfpanelReturnDrawForDay;
	protected HtmlOutputFormat formatManagePubByDayInstructions;
	protected HtmlBfPanel bfpanelWeeklyReturnsDraw;
	protected HtmlOutputFormat formatManagePubByWeekReturnsOnlyInstructions1;
	protected UIColumn columnWeeklyReturnDrawWeeksHeader;
	protected HtmlJspPanel jspPanelAvailableDatesPanel;
	protected HtmlInputHelperDatePicker datePicker1;
	protected UIColumn columnGoButtonsCol;
	protected HtmlInputHelperKeybind inputHelperKeybind1;
	public String doButtonEnterReturnDrawForDateAction() {
        // Type Java code that runs when the component is clicked

        //if (this.textReturnDrawChosenDate.getValue())
        // 
        // don't do anything.
        return "";
    }

	protected HtmlOutputText getTextLogoutLink() {
        if (textLogoutLink == null) {
            textLogoutLink = (HtmlOutputText) findComponentInRoot("textLogoutLink");
        }
        return textLogoutLink;
    }
    protected HtmlCommandLink getLinkLogout() {
        if (linkLogout == null) {
            linkLogout = (HtmlCommandLink) findComponentInRoot("linkLogout");
        }
        return linkLogout;
    }
    @SuppressWarnings("unchecked")
	public String doButtonRDLPrintFriendlyAction() {
        // Type Java code that runs when the component is clicked

        String responseStr = "success";
    	try {
	        Object o = this.getTextReturnDrawChosenDate().getValue();
	        java.util.Date date =(java.util.Date)o;
	
	       String keyHash = (String) this.getHiddenMktDstRteKey().getValue(); 
	
	        RouteHandler rh = this.getRoute();
	        UserHandler uh = this.getUserHandler(); 
	        UserIntf user = uh.getUser();
	
	        UserBO userBO = (UserBO)user;
	        
	        RouteBO route = userBO.getRoute(keyHash);
	        
	        // if key doesn't match for some reason, use the session route
	        if (route == null) {
	        	route = rh.getRoute();
	        }
	        
	        RDLForDayHandler rdl = null;
	        
	        rdl = new RDLForDayHandler(date, route, user);
	        
	        this.getSessionScope().remove("RouteDetailRDLPrintWin");
	
	        if (rdl.getLocations().size() == 0) {
	            DateTime dt = new DateTime(date);
	            FacesContext context = FacesContext.getCurrentInstance( );
	            String noLocsMessage = "No locations found for this route on " + dt.toString("MM/dd/yyyy") + " . Nothing to print.";
	            FacesMessage facesMsg = 
	                new FacesMessage(FacesMessage.SEVERITY_INFO, noLocsMessage, null);
	            context.addMessage(this.getMessageBadReturnDrawDate().getClientId(context), facesMsg);
	            this.getRequestScope().put("openRDLPrintFailureAlert", noLocsMessage);
	            responseStr = "failure";
	        }
	        else {
	            this.getSessionScope().put("RouteDetailRDLPrintWin", rdl);
	            this.getRequestScope().put("openPrintWindow", "true");
	        }
	
	        this.getRequestScope().put("openRDLTab", "true");
	        
	        // set tab index to Daily RDL
	        //this.getBfpanelReturnDrawForDay().getId()
	        this.getTabbedPanelWeeklyReturnPanel().setInitialPanelId("form1:tabbedPanelWeeklyReturnPanel:bfpanelReturnDrawForDay");
    	}
    	catch (Exception e) {
    		System.out.println("RouteDetailPage(): Failed to open RDL Print Window: " + e.getMessage());
            FacesContext context = FacesContext.getCurrentInstance( );
            String message = "Failed to load data for print View: " + e.getMessage();
            FacesMessage facesMsg = 
                new FacesMessage(FacesMessage.SEVERITY_INFO, message, null);
            context.addMessage(this.getMessageBadReturnDrawDate().getClientId(context), facesMsg);
            this.getRequestScope().put("openRDLPrintFailureAlert", message);
    		responseStr = "failure";
    	}
        return responseStr;
    }
    protected HtmlForm getFormLogout() {
        if (formLogout == null) {
            formLogout = (HtmlForm) findComponentInRoot("formLogout");
        }
        return formLogout;
    }
    protected HtmlOutputFormat getFormatWelcomeMessage() {
        if (formatWelcomeMessage == null) {
            formatWelcomeMessage = (HtmlOutputFormat) findComponentInRoot("formatWelcomeMessage");
        }
        return formatWelcomeMessage;
    }
    protected HtmlForm getForm1() {
        if (form1 == null) {
            form1 = (HtmlForm) findComponentInRoot("form1");
        }
        return form1;
    }
    protected HtmlMessages getMessages1() {
        if (messages1 == null) {
            messages1 = (HtmlMessages) findComponentInRoot("messages1");
        }
        return messages1;
    }
    protected HtmlCommandExButton getButtonDoLogoutBtn() {
        if (buttonDoLogoutBtn == null) {
            buttonDoLogoutBtn = (HtmlCommandExButton) findComponentInRoot("buttonDoLogoutBtn");
        }
        return buttonDoLogoutBtn;
    }
	/** 
	 * @managed-bean true
	 */
	protected RouteHandler getRoute() {
		if (route == null) {
			route = (RouteHandler) getManagedBean("route");
		}
		return route;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setRoute(RouteHandler route) {
		this.route = route;
	}

	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUserHandler() {
		if (userHandler == null) {
			userHandler = (UserHandler) getManagedBean("userHandler");
		}
		return userHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUserHandler(UserHandler userHandler) {
		this.userHandler = userHandler;
	}

	protected HtmlScriptCollector getTemplateScriptCollector() {
		if (templateScriptCollector == null) {
			templateScriptCollector = (HtmlScriptCollector) findComponentInRoot("templateScriptCollector");
		}
		return templateScriptCollector;
	}

	protected HtmlOutputText getTextGlobalMessageText() {
		if (textGlobalMessageText == null) {
			textGlobalMessageText = (HtmlOutputText) findComponentInRoot("textGlobalMessageText");
		}
		return textGlobalMessageText;
	}

	protected HtmlOutputText getTextMarketLabel() {
		if (textMarketLabel == null) {
			textMarketLabel = (HtmlOutputText) findComponentInRoot("textMarketLabel");
		}
		return textMarketLabel;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlOutputText getTextDistrictLabel() {
		if (textDistrictLabel == null) {
			textDistrictLabel = (HtmlOutputText) findComponentInRoot("textDistrictLabel");
		}
		return textDistrictLabel;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getTextRouteIDLabel() {
		if (textRouteIDLabel == null) {
			textRouteIDLabel = (HtmlOutputText) findComponentInRoot("textRouteIDLabel");
		}
		return textRouteIDLabel;
	}

	protected HtmlOutputText getTextDetailRouteID() {
		if (textDetailRouteID == null) {
			textDetailRouteID = (HtmlOutputText) findComponentInRoot("textDetailRouteID");
		}
		return textDetailRouteID;
	}

	protected HtmlOutputText getTextDescriptionLabel() {
		if (textDescriptionLabel == null) {
			textDescriptionLabel = (HtmlOutputText) findComponentInRoot("textDescriptionLabel");
		}
		return textDescriptionLabel;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlInputHidden getHiddenMktDstRteKey() {
		if (hiddenMktDstRteKey == null) {
			hiddenMktDstRteKey = (HtmlInputHidden) findComponentInRoot("hiddenMktDstRteKey");
		}
		return hiddenMktDstRteKey;
	}

	protected HtmlGraphicImageEx getImageExHelpIcon1() {
		if (imageExHelpIcon1 == null) {
			imageExHelpIcon1 = (HtmlGraphicImageEx) findComponentInRoot("imageExHelpIcon1");
		}
		return imageExHelpIcon1;
	}

	protected HtmlBfPanel getBfpanelRteMsgPanel() {
		if (bfpanelRteMsgPanel == null) {
			bfpanelRteMsgPanel = (HtmlBfPanel) findComponentInRoot("bfpanelRteMsgPanel");
		}
		return bfpanelRteMsgPanel;
	}

	protected HtmlJspPanel getJspPanelTabbedPannelRteMsg() {
		if (jspPanelTabbedPannelRteMsg == null) {
			jspPanelTabbedPannelRteMsg = (HtmlJspPanel) findComponentInRoot("jspPanelTabbedPannelRteMsg");
		}
		return jspPanelTabbedPannelRteMsg;
	}

	protected HtmlPanelLayout getLayout1() {
		if (layout1 == null) {
			layout1 = (HtmlPanelLayout) findComponentInRoot("layout1");
		}
		return layout1;
	}

	protected HtmlDataTable getTableRouteBroadcasstMessages() {
		if (tableRouteBroadcasstMessages == null) {
			tableRouteBroadcasstMessages = (HtmlDataTable) findComponentInRoot("tableRouteBroadcasstMessages");
		}
		return tableRouteBroadcasstMessages;
	}

	protected UIColumn getColumnBMsgIdCol() {
		if (columnBMsgIdCol == null) {
			columnBMsgIdCol = (UIColumn) findComponentInRoot("columnBMsgIdCol");
		}
		return columnBMsgIdCol;
	}

	protected HtmlJspPanel getJspPanel1111111() {
		if (jspPanel1111111 == null) {
			jspPanel1111111 = (HtmlJspPanel) findComponentInRoot("jspPanel1111111");
		}
		return jspPanel1111111;
	}

	protected HtmlOutputFormat getFormatMessageHeaderf1() {
		if (formatMessageHeaderf1 == null) {
			formatMessageHeaderf1 = (HtmlOutputFormat) findComponentInRoot("formatMessageHeaderf1");
		}
		return formatMessageHeaderf1;
	}

	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}

	protected HtmlInputText getTextReturnDrawChosenDate() {
		if (textReturnDrawChosenDate == null) {
			textReturnDrawChosenDate = (HtmlInputText) findComponentInRoot("textReturnDrawChosenDate");
		}
		return textReturnDrawChosenDate;
	}

	protected HtmlCommandExButton getButtonEnterReturnDrawForDate() {
		if (buttonEnterReturnDrawForDate == null) {
			buttonEnterReturnDrawForDate = (HtmlCommandExButton) findComponentInRoot("buttonEnterReturnDrawForDate");
		}
		return buttonEnterReturnDrawForDate;
	}

	protected HtmlMessage getMessageBadReturnDrawDate() {
		if (messageBadReturnDrawDate == null) {
			messageBadReturnDrawDate = (HtmlMessage) findComponentInRoot("messageBadReturnDrawDate");
		}
		return messageBadReturnDrawDate;
	}

	protected HtmlCommandExButton getButtonRDLPrintFriendly() {
		if (buttonRDLPrintFriendly == null) {
			buttonRDLPrintFriendly = (HtmlCommandExButton) findComponentInRoot("buttonRDLPrintFriendly");
		}
		return buttonRDLPrintFriendly;
	}

	protected HtmlJspPanel getJspPanelWeeklyReturnDrawOuterPanel() {
		if (jspPanelWeeklyReturnDrawOuterPanel == null) {
			jspPanelWeeklyReturnDrawOuterPanel = (HtmlJspPanel) findComponentInRoot("jspPanelWeeklyReturnDrawOuterPanel");
		}
		return jspPanelWeeklyReturnDrawOuterPanel;
	}

	protected HtmlDataTable getTablePublicationsForWeeklyReturnDraw() {
		if (tablePublicationsForWeeklyReturnDraw == null) {
			tablePublicationsForWeeklyReturnDraw = (HtmlDataTable) findComponentInRoot("tablePublicationsForWeeklyReturnDraw");
		}
		return tablePublicationsForWeeklyReturnDraw;
	}

	protected UIColumn getColumnWeeklyReturnDrawPubsCol() {
		if (columnWeeklyReturnDrawPubsCol == null) {
			columnWeeklyReturnDrawPubsCol = (UIColumn) findComponentInRoot("columnWeeklyReturnDrawPubsCol");
		}
		return columnWeeklyReturnDrawPubsCol;
	}

	protected HtmlOutputLinkEx getLinkExWeeklyDRProdDescriptionLink() {
		if (linkExWeeklyDRProdDescriptionLink == null) {
			linkExWeeklyDRProdDescriptionLink = (HtmlOutputLinkEx) findComponentInRoot("linkExWeeklyDRProdDescriptionLink");
		}
		return linkExWeeklyDRProdDescriptionLink;
	}

	protected HtmlOutputText getTextWeeklyDrawReturnProductID() {
		if (textWeeklyDrawReturnProductID == null) {
			textWeeklyDrawReturnProductID = (HtmlOutputText) findComponentInRoot("textWeeklyDrawReturnProductID");
		}
		return textWeeklyDrawReturnProductID;
	}

	protected HtmlOutputText getTextWeeklyDrawRetPubHeader() {
		if (textWeeklyDrawRetPubHeader == null) {
			textWeeklyDrawRetPubHeader = (HtmlOutputText) findComponentInRoot("textWeeklyDrawRetPubHeader");
		}
		return textWeeklyDrawRetPubHeader;
	}

	protected HtmlMessage getMessageInvalidWeeklyReturnDrawMsg() {
		if (messageInvalidWeeklyReturnDrawMsg == null) {
			messageInvalidWeeklyReturnDrawMsg = (HtmlMessage) findComponentInRoot("messageInvalidWeeklyReturnDrawMsg");
		}
		return messageInvalidWeeklyReturnDrawMsg;
	}

	protected HtmlInputText getTextWeekEndingDateValue() {
		if (textWeekEndingDateValue == null) {
			textWeekEndingDateValue = (HtmlInputText) findComponentInRoot("textWeekEndingDateValue");
		}
		return textWeekEndingDateValue;
	}

	protected HtmlOutputText getTextWeeklyDrawReturnAvailWeekHeader() {
		if (textWeeklyDrawReturnAvailWeekHeader == null) {
			textWeeklyDrawReturnAvailWeekHeader = (HtmlOutputText) findComponentInRoot("textWeeklyDrawReturnAvailWeekHeader");
		}
		return textWeeklyDrawReturnAvailWeekHeader;
	}

	protected HtmlCommandExButton getButtonWeeklyReturnDrawLaunch() {
		if (buttonWeeklyReturnDrawLaunch == null) {
			buttonWeeklyReturnDrawLaunch = (HtmlCommandExButton) findComponentInRoot("buttonWeeklyReturnDrawLaunch");
		}
		return buttonWeeklyReturnDrawLaunch;
	}

	protected HtmlCommandExButton getTabbedPanelWeeklyReturnPanel1_back() {
		if (tabbedPanelWeeklyReturnPanel1_back == null) {
			tabbedPanelWeeklyReturnPanel1_back = (HtmlCommandExButton) findComponentInRoot("tabbedPanelWeeklyReturnPanel1_back");
		}
		return tabbedPanelWeeklyReturnPanel1_back;
	}

	protected HtmlCommandExButton getTabbedPanelWeeklyReturnPanel1_next() {
		if (tabbedPanelWeeklyReturnPanel1_next == null) {
			tabbedPanelWeeklyReturnPanel1_next = (HtmlCommandExButton) findComponentInRoot("tabbedPanelWeeklyReturnPanel1_next");
		}
		return tabbedPanelWeeklyReturnPanel1_next;
	}

	protected HtmlCommandExButton getTabbedPanelWeeklyReturnPanel1_finish() {
		if (tabbedPanelWeeklyReturnPanel1_finish == null) {
			tabbedPanelWeeklyReturnPanel1_finish = (HtmlCommandExButton) findComponentInRoot("tabbedPanelWeeklyReturnPanel1_finish");
		}
		return tabbedPanelWeeklyReturnPanel1_finish;
	}

	protected HtmlCommandExButton getTabbedPanelWeeklyReturnPanel1_cancel() {
		if (tabbedPanelWeeklyReturnPanel1_cancel == null) {
			tabbedPanelWeeklyReturnPanel1_cancel = (HtmlCommandExButton) findComponentInRoot("tabbedPanelWeeklyReturnPanel1_cancel");
		}
		return tabbedPanelWeeklyReturnPanel1_cancel;
	}

	protected HtmlTabbedPanel getTabbedPanelWeeklyReturnPanel() {
		if (tabbedPanelWeeklyReturnPanel == null) {
			tabbedPanelWeeklyReturnPanel = (HtmlTabbedPanel) findComponentInRoot("tabbedPanelWeeklyReturnPanel");
		}
		return tabbedPanelWeeklyReturnPanel;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected UIColumn getColumnBMsgTxtCol() {
		if (columnBMsgTxtCol == null) {
			columnBMsgTxtCol = (UIColumn) findComponentInRoot("columnBMsgTxtCol");
		}
		return columnBMsgTxtCol;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected HtmlBfPanel getBfpanelReturnDrawForDay() {
		if (bfpanelReturnDrawForDay == null) {
			bfpanelReturnDrawForDay = (HtmlBfPanel) findComponentInRoot("bfpanelReturnDrawForDay");
		}
		return bfpanelReturnDrawForDay;
	}

	protected HtmlOutputFormat getFormatManagePubByDayInstructions() {
		if (formatManagePubByDayInstructions == null) {
			formatManagePubByDayInstructions = (HtmlOutputFormat) findComponentInRoot("formatManagePubByDayInstructions");
		}
		return formatManagePubByDayInstructions;
	}

	protected HtmlBfPanel getBfpanelWeeklyReturnsDraw() {
		if (bfpanelWeeklyReturnsDraw == null) {
			bfpanelWeeklyReturnsDraw = (HtmlBfPanel) findComponentInRoot("bfpanelWeeklyReturnsDraw");
		}
		return bfpanelWeeklyReturnsDraw;
	}

	protected HtmlOutputFormat getFormatManagePubByWeekReturnsOnlyInstructions1() {
		if (formatManagePubByWeekReturnsOnlyInstructions1 == null) {
			formatManagePubByWeekReturnsOnlyInstructions1 = (HtmlOutputFormat) findComponentInRoot("formatManagePubByWeekReturnsOnlyInstructions1");
		}
		return formatManagePubByWeekReturnsOnlyInstructions1;
	}

	protected UIColumn getColumnWeeklyReturnDrawWeeksHeader() {
		if (columnWeeklyReturnDrawWeeksHeader == null) {
			columnWeeklyReturnDrawWeeksHeader = (UIColumn) findComponentInRoot("columnWeeklyReturnDrawWeeksHeader");
		}
		return columnWeeklyReturnDrawWeeksHeader;
	}

	protected HtmlJspPanel getJspPanelAvailableDatesPanel() {
		if (jspPanelAvailableDatesPanel == null) {
			jspPanelAvailableDatesPanel = (HtmlJspPanel) findComponentInRoot("jspPanelAvailableDatesPanel");
		}
		return jspPanelAvailableDatesPanel;
	}

	protected HtmlInputHelperDatePicker getDatePicker1() {
		if (datePicker1 == null) {
			datePicker1 = (HtmlInputHelperDatePicker) findComponentInRoot("datePicker1");
		}
		return datePicker1;
	}

	protected UIColumn getColumnGoButtonsCol() {
		if (columnGoButtonsCol == null) {
			columnGoButtonsCol = (UIColumn) findComponentInRoot("columnGoButtonsCol");
		}
		return columnGoButtonsCol;
	}

	protected HtmlInputHelperKeybind getInputHelperKeybind1() {
		if (inputHelperKeybind1 == null) {
			inputHelperKeybind1 = (HtmlInputHelperKeybind) findComponentInRoot("inputHelperKeybind1");
		}
		return inputHelperKeybind1;
	}
}
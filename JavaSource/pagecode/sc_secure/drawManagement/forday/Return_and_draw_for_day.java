/*
 * Created on Mar 8, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.drawManagement.forday;

import java.util.Iterator;

import pagecode.PageCodeBase;
import com.usatoday.champion.handlers.LocationHandler;
import com.usatoday.champion.handlers.RDLForDayHandler;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlPanelBox;
import javax.faces.event.ValueChangeEvent;
import javax.faces.component.html.HtmlForm;
import com.ibm.faces.component.html.HtmlOutputSeparator;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.bf.component.html.HtmlTabbedPanel;
import com.ibm.faces.bf.component.html.HtmlBfPanel;
import com.ibm.faces.component.html.HtmlPanelLayout;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.UIColumn;
import com.ibm.faces.component.html.HtmlJspPanel;
import javax.faces.component.html.HtmlInputText;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlCommandExButton;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlMessage;
import com.ibm.faces.component.html.HtmlInputHelperKeybind;
/**
 * @author aeast
 * @date Mar 8, 2007
 * @class drawManagementReturn_and_draw_for_dayV2
 * 
 * 
 *  
 */
public class Return_and_draw_for_day extends PageCodeBase {

    protected HtmlOutputText P;
    protected HtmlPanelBox box1;
    protected HtmlForm formManageDraw;
    protected HtmlOutputSeparator separator1;
    protected HtmlOutputLinkEx linkExProductOrderTyp;
    protected HtmlScriptCollector scriptCollector1;
    protected HtmlOutputText textCurrentRDLDate;
    protected HtmlOutputText textRouteIDText;
    protected HtmlOutputText textRouteDescription;
    protected HtmlOutputText textRouteIDHeaderLabel;
    protected HtmlOutputText textRouteDescHeaderLabel;
    protected HtmlOutputText textDailyDMTitleLabel;
    protected HtmlOutputText textNumLocationHeader;
    protected HtmlMessages messagesPageLevelErrors;
    protected HtmlOutputText textDrawEntryStatus;
    protected HtmlOutputText textLocationInfo;
    protected HtmlOutputText textPOInfo;
    protected HtmlTabbedPanel tabbedPanelProductOrders;
    protected HtmlBfPanel bfpanelMainPOPanel;
    protected HtmlPanelLayout layout1;
    protected HtmlDataTable table1;
    protected UIColumn columnLocationColumn;
    protected HtmlJspPanel jspPanelLocationInfoPanel1;
    protected HtmlOutputText textLocIdVar;
    protected HtmlOutputText textLocNameVar;
    protected HtmlOutputText textLocAddr1Var;
    protected HtmlOutputText textLocPhoneNumberVar;
    protected HtmlDataTable tableProductOrdersDataTable;
    protected UIColumn column376;
    protected HtmlOutputText textProductOrder;
    protected HtmlOutputText text4;
    protected HtmlOutputLinkEx linkExProductOrderType;
    protected HtmlOutputText textProductOrderType;
    protected HtmlOutputText text3;
    protected HtmlOutputText text6;
    protected HtmlOutputText textProductTypeText;
    protected HtmlInputText textDrawValueInput;
    protected HtmlOutputText text9;
    protected HtmlJspPanel jspPanel1;
    protected HtmlGraphicImageEx imageEx99;
    protected HtmlOutputText textProductOrderReturnDate;
    protected HtmlGraphicImageEx imageEx1;
    protected HtmlOutputText text2;
    protected HtmlInputText textReturnValueInput;
    protected HtmlOutputText text8;
    protected HtmlCommandExButton tabbedPanel1_back;
    protected HtmlCommandExButton tabbedPanel1_next;
    protected HtmlCommandExButton tabbedPanel1_finish;
    protected HtmlCommandExButton tabbedPanel1_cancel;
    protected UIColumn columnProductOrdersOuterColumn;
    protected UIColumn column344;
    protected UIColumn column498;
    protected HtmlOutputLinkEx linkExProductDescriptionLink;
    protected UIColumn column625;
    protected HtmlInputHidden hiddenZeroDrawReason;
    protected HtmlMessage messageDrawInvalid;
    protected UIColumn column959;
    protected UIColumn column567;
    protected HtmlMessage messageReturnMesssage;
    protected HtmlCommandExButton buttonSubmitDrawChanges;
    protected HtmlCommandExButton buttonResetValues;
    protected HtmlCommandExButton buttonQuitNoSave;
    protected HtmlInputHelperKeybind inputHelperKeybind1;
    protected HtmlOutputText textNotEdittable;
	protected RDLForDayHandler RDLForDay;
    public String doButtonSubmitDrawChangesAction() {
        // Type Java code that runs when the component is clicked

        // add any logic before saving..
        
        // 
        return "success";
    }
    protected HtmlPanelBox getBox1() {
        if (box1 == null) {
            box1 = (HtmlPanelBox) findComponentInRoot("box1");
        }
        return box1;
    }
    public void handleTextDrawValueInputValueChange(
            ValueChangeEvent valueChangedEvent) {
        // Type Java code to handle value changed event here
        // Note, valueChangeEvent contains new and old values
    }
    public void handleTextDrawValueInputValueChange1(
            ValueChangeEvent valueChangedEvent) {
        // Type Java code to handle value changed event here
        // Note, valueChangeEvent contains new and old values
    }
    public String doButtonQuitNoSaveAction() {
        // Type Java code that runs when the component is clicked

        
        
        RDLForDayHandler rdh = this.getRDLForDay();
        Iterator<LocationHandler> itr = rdh.getLocations().iterator();
        while (itr.hasNext()) {
            LocationHandler l = itr.next();
            l.getProductOrders().clear();
        }
        rdh.getLocations().clear();
        this.getSessionScope().remove("RDLForDay");
        return "success";
    }
    protected HtmlForm getFormManageDraw() {
        if (formManageDraw == null) {
            formManageDraw = (HtmlForm) findComponentInRoot("formManageDraw");
        }
        return formManageDraw;
    }
    protected HtmlOutputSeparator getSeparator1() {
        if (separator1 == null) {
            separator1 = (HtmlOutputSeparator) findComponentInRoot("separator1");
        }
        return separator1;
    }
    protected HtmlOutputLinkEx getLinkExProductOrderTyp() {
        if (linkExProductOrderTyp == null) {
            linkExProductOrderTyp = (HtmlOutputLinkEx) findComponentInRoot("linkExProductOrderTyp");
        }
        return linkExProductOrderTyp;
    }
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlOutputText getTextCurrentRDLDate() {
        if (textCurrentRDLDate == null) {
            textCurrentRDLDate = (HtmlOutputText) findComponentInRoot("textCurrentRDLDate");
        }
        return textCurrentRDLDate;
    }
    protected HtmlOutputText getTextRouteIDText() {
        if (textRouteIDText == null) {
            textRouteIDText = (HtmlOutputText) findComponentInRoot("textRouteIDText");
        }
        return textRouteIDText;
    }
    protected HtmlOutputText getTextRouteDescription() {
        if (textRouteDescription == null) {
            textRouteDescription = (HtmlOutputText) findComponentInRoot("textRouteDescription");
        }
        return textRouteDescription;
    }
    protected HtmlOutputText getTextRouteIDHeaderLabel() {
        if (textRouteIDHeaderLabel == null) {
            textRouteIDHeaderLabel = (HtmlOutputText) findComponentInRoot("textRouteIDHeaderLabel");
        }
        return textRouteIDHeaderLabel;
    }
    protected HtmlOutputText getTextRouteDescHeaderLabel() {
        if (textRouteDescHeaderLabel == null) {
            textRouteDescHeaderLabel = (HtmlOutputText) findComponentInRoot("textRouteDescHeaderLabel");
        }
        return textRouteDescHeaderLabel;
    }
    protected HtmlOutputText getTextDailyDMTitleLabel() {
        if (textDailyDMTitleLabel == null) {
            textDailyDMTitleLabel = (HtmlOutputText) findComponentInRoot("textDailyDMTitleLabel");
        }
        return textDailyDMTitleLabel;
    }
    protected HtmlOutputText getTextNumLocationHeader() {
        if (textNumLocationHeader == null) {
            textNumLocationHeader = (HtmlOutputText) findComponentInRoot("textNumLocationHeader");
        }
        return textNumLocationHeader;
    }
    protected HtmlMessages getMessagesPageLevelErrors() {
        if (messagesPageLevelErrors == null) {
            messagesPageLevelErrors = (HtmlMessages) findComponentInRoot("messagesPageLevelErrors");
        }
        return messagesPageLevelErrors;
    }
    protected HtmlOutputText getTextDrawEntryStatus() {
        if (textDrawEntryStatus == null) {
            textDrawEntryStatus = (HtmlOutputText) findComponentInRoot("textDrawEntryStatus");
        }
        return textDrawEntryStatus;
    }
    protected HtmlOutputText getTextLocationInfo() {
        if (textLocationInfo == null) {
            textLocationInfo = (HtmlOutputText) findComponentInRoot("textLocationInfo");
        }
        return textLocationInfo;
    }
    protected HtmlOutputText getTextPOInfo() {
        if (textPOInfo == null) {
            textPOInfo = (HtmlOutputText) findComponentInRoot("textPOInfo");
        }
        return textPOInfo;
    }
    protected HtmlTabbedPanel getTabbedPanelProductOrders() {
        if (tabbedPanelProductOrders == null) {
            tabbedPanelProductOrders = (HtmlTabbedPanel) findComponentInRoot("tabbedPanelProductOrders");
        }
        return tabbedPanelProductOrders;
    }
    protected HtmlBfPanel getBfpanelMainPOPanel() {
        if (bfpanelMainPOPanel == null) {
            bfpanelMainPOPanel = (HtmlBfPanel) findComponentInRoot("bfpanelMainPOPanel");
        }
        return bfpanelMainPOPanel;
    }
    protected HtmlPanelLayout getLayout1() {
        if (layout1 == null) {
            layout1 = (HtmlPanelLayout) findComponentInRoot("layout1");
        }
        return layout1;
    }
    protected HtmlDataTable getTable1() {
        if (table1 == null) {
            table1 = (HtmlDataTable) findComponentInRoot("table1");
        }
        return table1;
    }
    protected UIColumn getColumnLocationColumn() {
        if (columnLocationColumn == null) {
            columnLocationColumn = (UIColumn) findComponentInRoot("columnLocationColumn");
        }
        return columnLocationColumn;
    }
    protected HtmlJspPanel getJspPanelLocationInfoPanel1() {
        if (jspPanelLocationInfoPanel1 == null) {
            jspPanelLocationInfoPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanelLocationInfoPanel1");
        }
        return jspPanelLocationInfoPanel1;
    }
    protected HtmlOutputText getTextLocIdVar() {
        if (textLocIdVar == null) {
            textLocIdVar = (HtmlOutputText) findComponentInRoot("textLocIdVar");
        }
        return textLocIdVar;
    }
    protected HtmlOutputText getTextLocNameVar() {
        if (textLocNameVar == null) {
            textLocNameVar = (HtmlOutputText) findComponentInRoot("textLocNameVar");
        }
        return textLocNameVar;
    }
    protected HtmlOutputText getTextLocAddr1Var() {
        if (textLocAddr1Var == null) {
            textLocAddr1Var = (HtmlOutputText) findComponentInRoot("textLocAddr1Var");
        }
        return textLocAddr1Var;
    }
    protected HtmlOutputText getTextLocPhoneNumberVar() {
        if (textLocPhoneNumberVar == null) {
            textLocPhoneNumberVar = (HtmlOutputText) findComponentInRoot("textLocPhoneNumberVar");
        }
        return textLocPhoneNumberVar;
    }
    protected HtmlDataTable getTableProductOrdersDataTable() {
        if (tableProductOrdersDataTable == null) {
            tableProductOrdersDataTable = (HtmlDataTable) findComponentInRoot("tableProductOrdersDataTable");
        }
        return tableProductOrdersDataTable;
    }
    protected UIColumn getColumn376() {
        if (column376 == null) {
            column376 = (UIColumn) findComponentInRoot("column376");
        }
        return column376;
    }
    protected HtmlOutputText getTextProductOrder() {
        if (textProductOrder == null) {
            textProductOrder = (HtmlOutputText) findComponentInRoot("textProductOrder");
        }
        return textProductOrder;
    }
    protected HtmlOutputText getText4() {
        if (text4 == null) {
            text4 = (HtmlOutputText) findComponentInRoot("text4");
        }
        return text4;
    }
    protected HtmlOutputLinkEx getLinkExProductOrderType() {
        if (linkExProductOrderType == null) {
            linkExProductOrderType = (HtmlOutputLinkEx) findComponentInRoot("linkExProductOrderType");
        }
        return linkExProductOrderType;
    }
    protected HtmlOutputText getTextProductOrderType() {
        if (textProductOrderType == null) {
            textProductOrderType = (HtmlOutputText) findComponentInRoot("textProductOrderType");
        }
        return textProductOrderType;
    }
    protected HtmlOutputText getText3() {
        if (text3 == null) {
            text3 = (HtmlOutputText) findComponentInRoot("text3");
        }
        return text3;
    }
    protected HtmlOutputText getText6() {
        if (text6 == null) {
            text6 = (HtmlOutputText) findComponentInRoot("text6");
        }
        return text6;
    }
    protected HtmlOutputText getTextProductTypeText() {
        if (textProductTypeText == null) {
            textProductTypeText = (HtmlOutputText) findComponentInRoot("textProductTypeText");
        }
        return textProductTypeText;
    }
    protected HtmlInputText getTextDrawValueInput() {
        if (textDrawValueInput == null) {
            textDrawValueInput = (HtmlInputText) findComponentInRoot("textDrawValueInput");
        }
        return textDrawValueInput;
    }
    protected HtmlOutputText getText9() {
        if (text9 == null) {
            text9 = (HtmlOutputText) findComponentInRoot("text9");
        }
        return text9;
    }
    protected HtmlJspPanel getJspPanel1() {
        if (jspPanel1 == null) {
            jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
        }
        return jspPanel1;
    }
    protected HtmlGraphicImageEx getImageEx99() {
        if (imageEx99 == null) {
            imageEx99 = (HtmlGraphicImageEx) findComponentInRoot("imageEx99");
        }
        return imageEx99;
    }
    protected HtmlOutputText getTextProductOrderReturnDate() {
        if (textProductOrderReturnDate == null) {
            textProductOrderReturnDate = (HtmlOutputText) findComponentInRoot("textProductOrderReturnDate");
        }
        return textProductOrderReturnDate;
    }
    protected HtmlGraphicImageEx getImageEx1() {
        if (imageEx1 == null) {
            imageEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageEx1");
        }
        return imageEx1;
    }
    protected HtmlOutputText getText2() {
        if (text2 == null) {
            text2 = (HtmlOutputText) findComponentInRoot("text2");
        }
        return text2;
    }
    protected HtmlInputText getTextReturnValueInput() {
        if (textReturnValueInput == null) {
            textReturnValueInput = (HtmlInputText) findComponentInRoot("textReturnValueInput");
        }
        return textReturnValueInput;
    }
    protected HtmlOutputText getText8() {
        if (text8 == null) {
            text8 = (HtmlOutputText) findComponentInRoot("text8");
        }
        return text8;
    }
    protected HtmlCommandExButton getTabbedPanel1_back() {
        if (tabbedPanel1_back == null) {
            tabbedPanel1_back = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_back");
        }
        return tabbedPanel1_back;
    }
    protected HtmlCommandExButton getTabbedPanel1_next() {
        if (tabbedPanel1_next == null) {
            tabbedPanel1_next = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_next");
        }
        return tabbedPanel1_next;
    }
    protected HtmlCommandExButton getTabbedPanel1_finish() {
        if (tabbedPanel1_finish == null) {
            tabbedPanel1_finish = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_finish");
        }
        return tabbedPanel1_finish;
    }
    protected HtmlCommandExButton getTabbedPanel1_cancel() {
        if (tabbedPanel1_cancel == null) {
            tabbedPanel1_cancel = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_cancel");
        }
        return tabbedPanel1_cancel;
    }
    protected UIColumn getColumnProductOrdersOuterColumn() {
        if (columnProductOrdersOuterColumn == null) {
            columnProductOrdersOuterColumn = (UIColumn) findComponentInRoot("columnProductOrdersOuterColumn");
        }
        return columnProductOrdersOuterColumn;
    }
    protected UIColumn getColumn344() {
        if (column344 == null) {
            column344 = (UIColumn) findComponentInRoot("column344");
        }
        return column344;
    }
    protected UIColumn getColumn498() {
        if (column498 == null) {
            column498 = (UIColumn) findComponentInRoot("column498");
        }
        return column498;
    }
    protected HtmlOutputLinkEx getLinkExProductDescriptionLink() {
        if (linkExProductDescriptionLink == null) {
            linkExProductDescriptionLink = (HtmlOutputLinkEx) findComponentInRoot("linkExProductDescriptionLink");
        }
        return linkExProductDescriptionLink;
    }
    protected UIColumn getColumn625() {
        if (column625 == null) {
            column625 = (UIColumn) findComponentInRoot("column625");
        }
        return column625;
    }
    protected HtmlInputHidden getHiddenZeroDrawReason() {
        if (hiddenZeroDrawReason == null) {
            hiddenZeroDrawReason = (HtmlInputHidden) findComponentInRoot("hiddenZeroDrawReason");
        }
        return hiddenZeroDrawReason;
    }
    protected HtmlMessage getMessageDrawInvalid() {
        if (messageDrawInvalid == null) {
            messageDrawInvalid = (HtmlMessage) findComponentInRoot("messageDrawInvalid");
        }
        return messageDrawInvalid;
    }
    protected UIColumn getColumn959() {
        if (column959 == null) {
            column959 = (UIColumn) findComponentInRoot("column959");
        }
        return column959;
    }
    protected UIColumn getColumn567() {
        if (column567 == null) {
            column567 = (UIColumn) findComponentInRoot("column567");
        }
        return column567;
    }
    protected HtmlMessage getMessageReturnMesssage() {
        if (messageReturnMesssage == null) {
            messageReturnMesssage = (HtmlMessage) findComponentInRoot("messageReturnMesssage");
        }
        return messageReturnMesssage;
    }
    protected HtmlCommandExButton getButtonSubmitDrawChanges() {
        if (buttonSubmitDrawChanges == null) {
            buttonSubmitDrawChanges = (HtmlCommandExButton) findComponentInRoot("buttonSubmitDrawChanges");
        }
        return buttonSubmitDrawChanges;
    }
    protected HtmlCommandExButton getButtonResetValues() {
        if (buttonResetValues == null) {
            buttonResetValues = (HtmlCommandExButton) findComponentInRoot("buttonResetValues");
        }
        return buttonResetValues;
    }
    protected HtmlCommandExButton getButtonQuitNoSave() {
        if (buttonQuitNoSave == null) {
            buttonQuitNoSave = (HtmlCommandExButton) findComponentInRoot("buttonQuitNoSave");
        }
        return buttonQuitNoSave;
    }
    protected HtmlInputHelperKeybind getInputHelperKeybind1() {
        if (inputHelperKeybind1 == null) {
            inputHelperKeybind1 = (HtmlInputHelperKeybind) findComponentInRoot("inputHelperKeybind1");
        }
        return inputHelperKeybind1;
    }
    protected HtmlOutputText getTextNotEdittable() {
        if (textNotEdittable == null) {
            textNotEdittable = (HtmlOutputText) findComponentInRoot("textNotEdittable");
        }
        return textNotEdittable;
    }
	/** 
	 * @managed-bean true
	 */
	protected RDLForDayHandler getRDLForDay() {
		if (RDLForDay == null) {
			RDLForDay = (RDLForDayHandler) getManagedBean("RDLForDay");
		}
		return RDLForDay;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setRDLForDay(RDLForDayHandler RDLForDay) {
		this.RDLForDay = RDLForDay;
	}
}
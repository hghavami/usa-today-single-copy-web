/*
 * Created on Mar 21, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.drawManagement.forday;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlOutputSeparator;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.component.html.HtmlCommandExButton;
import javax.faces.component.html.HtmlForm;
/**
 * @author aeast
 * @date Mar 21, 2007
 * @class fordayComplete
 * 
 * 
 * 
 */
public class Complete extends PageCodeBase {

    protected HtmlScriptCollector scriptCollector1;
    protected HtmlOutputSeparator separator1;
    protected HtmlOutputText textCompletetitle;
    protected HtmlMessages messages1;
    protected HtmlOutputText textRouteLabel;
    protected HtmlOutputText text1;
    protected HtmlOutputText textRouteDesLabel;
    protected HtmlOutputText text4;
    protected HtmlCommandExButton button1;
    protected HtmlOutputSeparator separator2;
    protected HtmlForm form1;
    protected HtmlCommandExButton buttonOpenPrinterFriendly;
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlOutputSeparator getSeparator1() {
        if (separator1 == null) {
            separator1 = (HtmlOutputSeparator) findComponentInRoot("separator1");
        }
        return separator1;
    }
    protected HtmlOutputText getTextCompletetitle() {
        if (textCompletetitle == null) {
            textCompletetitle = (HtmlOutputText) findComponentInRoot("textCompletetitle");
        }
        return textCompletetitle;
    }
    protected HtmlMessages getMessages1() {
        if (messages1 == null) {
            messages1 = (HtmlMessages) findComponentInRoot("messages1");
        }
        return messages1;
    }
    protected HtmlOutputText getTextRouteLabel() {
        if (textRouteLabel == null) {
            textRouteLabel = (HtmlOutputText) findComponentInRoot("textRouteLabel");
        }
        return textRouteLabel;
    }
    protected HtmlOutputText getText1() {
        if (text1 == null) {
            text1 = (HtmlOutputText) findComponentInRoot("text1");
        }
        return text1;
    }
    protected HtmlOutputText getTextRouteDesLabel() {
        if (textRouteDesLabel == null) {
            textRouteDesLabel = (HtmlOutputText) findComponentInRoot("textRouteDesLabel");
        }
        return textRouteDesLabel;
    }
    protected HtmlOutputText getText4() {
        if (text4 == null) {
            text4 = (HtmlOutputText) findComponentInRoot("text4");
        }
        return text4;
    }
    protected HtmlCommandExButton getButton1() {
        if (button1 == null) {
            button1 = (HtmlCommandExButton) findComponentInRoot("button1");
        }
        return button1;
    }
    protected HtmlOutputSeparator getSeparator2() {
        if (separator2 == null) {
            separator2 = (HtmlOutputSeparator) findComponentInRoot("separator2");
        }
        return separator2;
    }
    protected HtmlForm getForm1() {
        if (form1 == null) {
            form1 = (HtmlForm) findComponentInRoot("form1");
        }
        return form1;
    }
    protected HtmlCommandExButton getButtonOpenPrinterFriendly() {
        if (buttonOpenPrinterFriendly == null) {
            buttonOpenPrinterFriendly = (HtmlCommandExButton) findComponentInRoot("buttonOpenPrinterFriendly");
        }
        return buttonOpenPrinterFriendly;
    }
}
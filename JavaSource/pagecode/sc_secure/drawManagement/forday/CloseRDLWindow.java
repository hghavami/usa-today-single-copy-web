/*
 * Created on Mar 13, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.drawManagement.forday;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlOutputText;
/**
 * @author aeast
 * @date Mar 13, 2007
 * @class fordayCloseRDLWindow
 * 
 * 
 * 
 */
public class CloseRDLWindow extends PageCodeBase {

    protected HtmlScriptCollector scriptCollector1;
    protected HtmlOutputText textCleanUp;
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlOutputText getTextCleanUp() {
        if (textCleanUp == null) {
            textCleanUp = (HtmlOutputText) findComponentInRoot("textCleanUp");
        }
        return textCleanUp;
    }
}
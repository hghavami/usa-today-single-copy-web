/*
 * Created on Mar 21, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.sc_secure.drawManagement.forday;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlOutputSeparator;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.champion.handlers.RDLForDayHandler;
import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.singlecopy.UsatException;
import com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode;
/**
 * @author aeast
 * @date Mar 21, 2007
 * @class fordayConfirm
 * 
 * 
 * 
 */
public class Confirm extends PageCodeBase {

    protected HtmlScriptCollector scriptCollector1;
    protected HtmlForm formWeeklyMngmtConfirmation;
    protected HtmlOutputSeparator separator1;
    protected HtmlOutputText textWeekEndingLabel;
    protected HtmlOutputText textHeaderWeekEndingDate;
    protected HtmlOutputText textRouteLabel;
    protected HtmlOutputText text2;
    protected HtmlOutputText textNumPOChangedLabel;
    protected HtmlOutputText textNumberChangedPOs;
    protected HtmlOutputText textDrawHeaderLabel;
    protected HtmlOutputText textReturnsHeaderLabel;
    protected HtmlOutputText textOriginalLabel;
    protected HtmlOutputText textOrigTotDraw;
    protected HtmlOutputText textOrigTotReturns;
    protected HtmlOutputText textNewValLabel;
    protected HtmlOutputText text14;
    protected HtmlOutputText text15;
    protected HtmlCommandExButton buttonBack;
    protected HtmlOutputText textConfirmTitle;
    protected HtmlOutputText textSaveWarning;
    protected HtmlCommandExButton buttonSaveChanges;
    protected HtmlOutputSeparator separator2;
	protected RDLForDayHandler RDLForDay;
	protected UserHandler userHandler;
	protected OmnitureTrackingCode omnitureTracking;
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlForm getFormWeeklyMngmtConfirmation() {
        if (formWeeklyMngmtConfirmation == null) {
            formWeeklyMngmtConfirmation = (HtmlForm) findComponentInRoot("formWeeklyMngmtConfirmation");
        }
        return formWeeklyMngmtConfirmation;
    }
    protected HtmlOutputSeparator getSeparator1() {
        if (separator1 == null) {
            separator1 = (HtmlOutputSeparator) findComponentInRoot("separator1");
        }
        return separator1;
    }
    protected HtmlOutputText getTextWeekEndingLabel() {
        if (textWeekEndingLabel == null) {
            textWeekEndingLabel = (HtmlOutputText) findComponentInRoot("textWeekEndingLabel");
        }
        return textWeekEndingLabel;
    }
    protected HtmlOutputText getTextHeaderWeekEndingDate() {
        if (textHeaderWeekEndingDate == null) {
            textHeaderWeekEndingDate = (HtmlOutputText) findComponentInRoot("textHeaderWeekEndingDate");
        }
        return textHeaderWeekEndingDate;
    }
    protected HtmlOutputText getTextRouteLabel() {
        if (textRouteLabel == null) {
            textRouteLabel = (HtmlOutputText) findComponentInRoot("textRouteLabel");
        }
        return textRouteLabel;
    }
    protected HtmlOutputText getText2() {
        if (text2 == null) {
            text2 = (HtmlOutputText) findComponentInRoot("text2");
        }
        return text2;
    }
    protected HtmlOutputText getTextNumPOChangedLabel() {
        if (textNumPOChangedLabel == null) {
            textNumPOChangedLabel = (HtmlOutputText) findComponentInRoot("textNumPOChangedLabel");
        }
        return textNumPOChangedLabel;
    }
    protected HtmlOutputText getTextNumberChangedPOs() {
        if (textNumberChangedPOs == null) {
            textNumberChangedPOs = (HtmlOutputText) findComponentInRoot("textNumberChangedPOs");
        }
        return textNumberChangedPOs;
    }
    protected HtmlOutputText getTextDrawHeaderLabel() {
        if (textDrawHeaderLabel == null) {
            textDrawHeaderLabel = (HtmlOutputText) findComponentInRoot("textDrawHeaderLabel");
        }
        return textDrawHeaderLabel;
    }
    protected HtmlOutputText getTextReturnsHeaderLabel() {
        if (textReturnsHeaderLabel == null) {
            textReturnsHeaderLabel = (HtmlOutputText) findComponentInRoot("textReturnsHeaderLabel");
        }
        return textReturnsHeaderLabel;
    }
    protected HtmlOutputText getTextOriginalLabel() {
        if (textOriginalLabel == null) {
            textOriginalLabel = (HtmlOutputText) findComponentInRoot("textOriginalLabel");
        }
        return textOriginalLabel;
    }
    protected HtmlOutputText getTextOrigTotDraw() {
        if (textOrigTotDraw == null) {
            textOrigTotDraw = (HtmlOutputText) findComponentInRoot("textOrigTotDraw");
        }
        return textOrigTotDraw;
    }
    protected HtmlOutputText getTextOrigTotReturns() {
        if (textOrigTotReturns == null) {
            textOrigTotReturns = (HtmlOutputText) findComponentInRoot("textOrigTotReturns");
        }
        return textOrigTotReturns;
    }
    protected HtmlOutputText getTextNewValLabel() {
        if (textNewValLabel == null) {
            textNewValLabel = (HtmlOutputText) findComponentInRoot("textNewValLabel");
        }
        return textNewValLabel;
    }
    protected HtmlOutputText getText14() {
        if (text14 == null) {
            text14 = (HtmlOutputText) findComponentInRoot("text14");
        }
        return text14;
    }
    protected HtmlOutputText getText15() {
        if (text15 == null) {
            text15 = (HtmlOutputText) findComponentInRoot("text15");
        }
        return text15;
    }
    protected HtmlCommandExButton getButtonBack() {
        if (buttonBack == null) {
            buttonBack = (HtmlCommandExButton) findComponentInRoot("buttonBack");
        }
        return buttonBack;
    }
    protected HtmlOutputText getTextConfirmTitle() {
        if (textConfirmTitle == null) {
            textConfirmTitle = (HtmlOutputText) findComponentInRoot("textConfirmTitle");
        }
        return textConfirmTitle;
    }
    protected HtmlOutputText getTextSaveWarning() {
        if (textSaveWarning == null) {
            textSaveWarning = (HtmlOutputText) findComponentInRoot("textSaveWarning");
        }
        return textSaveWarning;
    }
    public String doButtonSaveChangesAction() {
        RDLForDayHandler rdl = this.getRDLForDay();
        
        if (!rdl.isSaveInProgress()) {
        	rdl.setSaveInProgress(true);
        }
        else {
        	// To get a around an issue with requests being submitted multiple times.
    		UserHandler uh = this.getUserHandler();
    		if (uh != null) {
    			System.out.println("Daily RDL Save Request in process already. User: " + uh.getEmailAddress() + " Browser: " + uh.getBrowserUserAgentValue());
    		}
    		else {
    			System.out.println("Daily RDL Save Request in process already. User: " + rdl.getDdm().getUser().getEmailAddress());
    		}
        	int count = 0;
        	while (rdl.isSaveInProgress() && count < 5) {
        		count++;
	        	try {	        		
	        		Thread.sleep(3000);
	        	}
	        	catch (Exception e) {
	        		;
	        	}
        	} // end while
        	
        	if (rdl.isSaveInProgress()) {
        		System.out.println("Daily RDL Save Request exceeded threshold. User: " + rdl.getDdm().getUser().getEmailAddress());
        		rdl.setSaveInProgress(false);
	            FacesContext context = FacesContext.getCurrentInstance( );
	            FacesMessage facesMsg = 
	                new FacesMessage(FacesMessage.SEVERITY_INFO, "Save Entries May Have Failed. An unexpected error occurred. Please reload your daily draw/return data to confirm that the changes were applied.", null);
	            context.addMessage(null, facesMsg);
                return "failure";
        	}
        	else {
        		return "success";
        	}
        	
        }
        
        try {
            // clear any previous error detail
            rdl.setErrorDetail("");
            
            rdl.getDdm().saveChanges();
            // reset error processing on product orders
            rdl.resetErrorProcessing();
            if (rdl.getDdm().getUpdateErrorsFlag()) {
                // errors during update
	            FacesContext context = FacesContext.getCurrentInstance( );
	            FacesMessage facesMsg = 
	                new FacesMessage(FacesMessage.SEVERITY_INFO, "Save Entries Failed. Please check Product Orders for Error Messages.", null);
	            context.addMessage(null, facesMsg);
                return "failure";
            }
        }
        catch (UsatException ue) {
            rdl.setErrorDetail(ue.getMessage());
            FacesContext context = FacesContext.getCurrentInstance( );
            FacesMessage facesMsg = 
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Save Entries Failed.", ue.getLocalizedMessage());
            context.addMessage(null, facesMsg);
            return "failure";
            
        }
        catch (Exception e) {
            rdl.setErrorDetail(e.getMessage());
            FacesContext context = FacesContext.getCurrentInstance( );
            FacesMessage facesMsg = 
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Save Entries Failed.", e.getLocalizedMessage());
            context.addMessage(null, facesMsg);
            return "failure";
        }    
        finally {
    		rdl.setSaveInProgress(false);
        }
        
        return "success";
    }
    protected HtmlCommandExButton getButtonSaveChanges() {
        if (buttonSaveChanges == null) {
            buttonSaveChanges = (HtmlCommandExButton) findComponentInRoot("buttonSaveChanges");
        }
        return buttonSaveChanges;
    }
    protected HtmlOutputSeparator getSeparator2() {
        if (separator2 == null) {
            separator2 = (HtmlOutputSeparator) findComponentInRoot("separator2");
        }
        return separator2;
    }
	/** 
	 * @managed-bean true
	 */
	protected RDLForDayHandler getRDLForDay() {
		if (RDLForDay == null) {
			RDLForDay = (RDLForDayHandler) getManagedBean("RDLForDay");
		}
		return RDLForDay;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setRDLForDay(RDLForDayHandler RDLForDay) {
		this.RDLForDay = RDLForDay;
	}
	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUserHandler() {
		if (userHandler == null) {
			userHandler = (UserHandler) getManagedBean("userHandler");
		}
		return userHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUserHandler(UserHandler userHandler) {
		this.userHandler = userHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected OmnitureTrackingCode getOmnitureTracking() {
		if (omnitureTracking == null) {
			omnitureTracking = (OmnitureTrackingCode) getManagedBean("omnitureTracking");
		}
		return omnitureTracking;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setOmnitureTracking(OmnitureTrackingCode omnitureTracking) {
		this.omnitureTracking = omnitureTracking;
	}
}
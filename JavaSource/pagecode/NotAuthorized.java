/*
 * Created on May 18, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode;

import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
/**
 * @author aeast
 * @date May 18, 2007
 * @class pagecodeNotAuthorized
 * 
 * 
 * 
 */
public class NotAuthorized extends PageCodeBase {

    protected HtmlScriptCollector scriptCollector1;
    protected HtmlOutputText textNotAuthorizedText;
    protected HtmlOutputLinkEx linkEx1;
    protected HtmlOutputText textBacktoPrevious;
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlOutputText getTextNotAuthorizedText() {
        if (textNotAuthorizedText == null) {
            textNotAuthorizedText = (HtmlOutputText) findComponentInRoot("textNotAuthorizedText");
        }
        return textNotAuthorizedText;
    }
    protected HtmlOutputLinkEx getLinkEx1() {
        if (linkEx1 == null) {
            linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
        }
        return linkEx1;
    }
    protected HtmlOutputText getTextBacktoPrevious() {
        if (textBacktoPrevious == null) {
            textBacktoPrevious = (HtmlOutputText) findComponentInRoot("textBacktoPrevious");
        }
        return textBacktoPrevious;
    }
}
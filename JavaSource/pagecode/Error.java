/*
 * Created on Sep 8, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pagecode;

import com.usatoday.champion.handlers.MessageHandler;
import com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlMessages;
/**
 * @author aeast
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Error extends PageCodeBase {

	protected MessageHandler requestMessage;
	protected OmnitureTrackingCode omnitureTracking;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlOutputLinkEx linkExCloseWindowTop;
	protected HtmlOutputText textCloseWindowTop;
	protected HtmlScriptCollector scriptCollector2;
	protected HtmlOutputText textOmnitureTrackingCode1;
	protected HtmlOutputText textHeader;
	protected HtmlMessages messages1;
	protected HtmlOutputText textCustomMessage;
	protected HtmlOutputLinkEx linkExCloseWindowBottom;
	protected HtmlOutputText textCloseWindowBottom;
	/** 
	 * @managed-bean true
	 */
	protected MessageHandler getRequestMessage() {
		if (requestMessage == null) {
			requestMessage = (MessageHandler) getManagedBean("requestMessage");
		}
		return requestMessage;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setRequestMessage(MessageHandler requestMessage) {
		this.requestMessage = requestMessage;
	}
	/** 
	 * @managed-bean true
	 */
	protected OmnitureTrackingCode getOmnitureTracking() {
		if (omnitureTracking == null) {
			omnitureTracking = (OmnitureTrackingCode) getManagedBean("omnitureTracking");
		}
		return omnitureTracking;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setOmnitureTracking(OmnitureTrackingCode omnitureTracking) {
		this.omnitureTracking = omnitureTracking;
	}
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}
	protected HtmlOutputLinkEx getLinkExCloseWindowTop() {
		if (linkExCloseWindowTop == null) {
			linkExCloseWindowTop = (HtmlOutputLinkEx) findComponentInRoot("linkExCloseWindowTop");
		}
		return linkExCloseWindowTop;
	}
	protected HtmlOutputText getTextCloseWindowTop() {
		if (textCloseWindowTop == null) {
			textCloseWindowTop = (HtmlOutputText) findComponentInRoot("textCloseWindowTop");
		}
		return textCloseWindowTop;
	}
	protected HtmlScriptCollector getScriptCollector2() {
		if (scriptCollector2 == null) {
			scriptCollector2 = (HtmlScriptCollector) findComponentInRoot("scriptCollector2");
		}
		return scriptCollector2;
	}
	protected HtmlOutputText getTextOmnitureTrackingCode1() {
		if (textOmnitureTrackingCode1 == null) {
			textOmnitureTrackingCode1 = (HtmlOutputText) findComponentInRoot("textOmnitureTrackingCode1");
		}
		return textOmnitureTrackingCode1;
	}
	protected HtmlOutputText getTextHeader() {
		if (textHeader == null) {
			textHeader = (HtmlOutputText) findComponentInRoot("textHeader");
		}
		return textHeader;
	}
	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}
	protected HtmlOutputText getTextCustomMessage() {
		if (textCustomMessage == null) {
			textCustomMessage = (HtmlOutputText) findComponentInRoot("textCustomMessage");
		}
		return textCustomMessage;
	}
	protected HtmlOutputLinkEx getLinkExCloseWindowBottom() {
		if (linkExCloseWindowBottom == null) {
			linkExCloseWindowBottom = (HtmlOutputLinkEx) findComponentInRoot("linkExCloseWindowBottom");
		}
		return linkExCloseWindowBottom;
	}
	protected HtmlOutputText getTextCloseWindowBottom() {
		if (textCloseWindowBottom == null) {
			textCloseWindowBottom = (HtmlOutputText) findComponentInRoot("textCloseWindowBottom");
		}
		return textCloseWindowBottom;
	}
}
/*
 * Created on Apr 22, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode;

import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
/**
 * @author aeast
 * @date Apr 22, 2008
 * @class pagecodeEmailOptOutConfirmation
 * 
 * 
 * 
 */
public class EmailOptOutConfirmation extends PageCodeBase {

    protected HtmlScriptCollector scriptCollector1;
    protected HtmlOutputText textEmailUnsubscribe;
    protected HtmlOutputLinkEx linkEx1;
    protected HtmlOutputText text1;
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlOutputText getTextEmailUnsubscribe() {
        if (textEmailUnsubscribe == null) {
            textEmailUnsubscribe = (HtmlOutputText) findComponentInRoot("textEmailUnsubscribe");
        }
        return textEmailUnsubscribe;
    }
    protected HtmlOutputLinkEx getLinkEx1() {
        if (linkEx1 == null) {
            linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
        }
        return linkEx1;
    }
    protected HtmlOutputText getText1() {
        if (text1 == null) {
            text1 = (HtmlOutputText) findComponentInRoot("text1");
        }
        return text1;
    }
}
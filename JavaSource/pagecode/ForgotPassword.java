/*
 * Created on Feb 15, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode;



import javax.faces.application.FacesMessage;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.usatoday.singlecopy.model.bo.users.UserBO;

import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlMessages;
import javax.faces.context.FacesContext;
import javax.faces.component.html.HtmlInputText;
import com.ibm.faces.component.html.HtmlInputHelperKeybind;
/**
 * @author aeast
 * @date Feb 15, 2007
 * @class pagecodeForgotPassword
 * 
 * 
 * 
 */
public class ForgotPassword extends PageCodeBase {

    protected HtmlScriptCollector scriptCollector1;
    protected HtmlForm form1;
    protected HtmlOutputText textUserId;
    protected HtmlCommandExButton buttonEmailPassword;
	protected HtmlMessages globalmessages1;
	protected HtmlInputText textInputEmail;
	protected HtmlMessage message1;
	protected HtmlMessage message2;
    protected HtmlInputHelperKeybind inputHelperKeybind1;
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlForm getForm1() {
        if (form1 == null) {
            form1 = (HtmlForm) findComponentInRoot("form1");
        }
        return form1;
    }
    protected HtmlOutputText getTextUserId() {
        if (textUserId == null) {
            textUserId = (HtmlOutputText) findComponentInRoot("textUserId");
        }
        return textUserId;
    }
    protected HtmlCommandExButton getButtonEmailPassword() {
        if (buttonEmailPassword == null) {
            buttonEmailPassword = (HtmlCommandExButton) findComponentInRoot("buttonEmailPassword");
        }
        return buttonEmailPassword;
    }
	public String doButtonEmailPasswordAction() {
		// Type Java code that runs when the component is clicked
		String returnStatus = "success";

		 try {
			boolean emailSent = UserBO.forgotPassword(this.getTextInputEmail().getValue().toString());
			
			FacesContext context = FacesContext.getCurrentInstance( );
			
	        FacesMessage facesMsg = null;
	        if (emailSent) {
	            facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Password sent to your account's email address.", null);
	        }
	        else {
	            returnStatus = "failure";
	            facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Your account has not been activated. Contact your market for assistance.", null);
	        }
	        
	        context.addMessage(null, facesMsg);
	   	 
        } catch (Exception ue) {
			FacesContext context = FacesContext.getCurrentInstance( );
	        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Problems retrieving Password. " + ue.getMessage(), null);
	        context.addMessage(null, facesMsg);
		  	returnStatus = "failure";
        }
        return returnStatus;
		
	}
	protected HtmlMessages getGlobalmessages1() {
		if (globalmessages1 == null) {
			globalmessages1 = (HtmlMessages) findComponentInRoot("globalmessages1");
		}
		return globalmessages1;
	}
	protected HtmlInputText getTextInputEmail() {
		if (textInputEmail == null) {
			textInputEmail = (HtmlInputText) findComponentInRoot("textInputEmail");
		}
		return textInputEmail;
	}
	protected HtmlMessage getMessage1() {
		if (message1 == null) {
			message1 = (HtmlMessage) findComponentInRoot("message1");
		}
		return message1;
	}
	protected HtmlMessage getMessage2() {
		if (message2 == null) {
			message2 = (HtmlMessage) findComponentInRoot("message2");
		}
		return message2;
	}
    protected HtmlInputHelperKeybind getInputHelperKeybind1() {
        if (inputHelperKeybind1 == null) {
            inputHelperKeybind1 = (HtmlInputHelperKeybind) findComponentInRoot("inputHelperKeybind1");
        }
        return inputHelperKeybind1;
    }
}
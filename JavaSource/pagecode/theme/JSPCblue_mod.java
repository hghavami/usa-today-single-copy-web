/*
 * Created on Feb 16, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.theme;

import java.util.Date;

import javax.faces.event.ValueChangeEvent;

import pagecode.PageCodeBase;

import com.usatoday.champion.handlers.RDLForDayHandler;
import com.usatoday.champion.handlers.RDLForWeekHandler;
import com.usatoday.champion.handlers.RouteListHandler;
import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.champion.handlers.blueChipDraw.BlueChipDrawManagementHandler;
import com.usatoday.champion.handlers.blueChipDraw.BlueChipWeeklyProductOrderHandler;
import com.usatoday.gannett.singlecopy.util.OmnitureTrackingCode;
import com.usatoday.singlecopy.model.bo.users.UserBO;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputFormat;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlInputHelperKeybind;
import javax.faces.component.html.HtmlOutputText;
import com.usatoday.champion.handlers.MessageHandler;
import com.usatoday.champion.handlers.LocaleHandler;
import com.ibm.faces.component.html.HtmlScriptCollector;
/**
 * @author aeast
 * @date Feb 16, 2007
 * @class themeJSPCblue_mod
 * 
 * Handles the page template code behind
 * 
 */
public class JSPCblue_mod extends PageCodeBase {

    protected Date todaysDate;
    protected OmnitureTrackingCode omnitureTracking;
	protected HtmlForm formLogout;
	protected HtmlOutputFormat formatWelcomeMessage;
	protected HtmlCommandExButton buttonDoLogoutBtn;
	protected HtmlInputHelperKeybind inputHelperKeybind1;
	protected HtmlOutputText textGlobalMessageText;
	protected MessageHandler globalMessage;
	protected LocaleHandler userLocaleHandler;
	protected Date currentDateTime;
	protected UserHandler userHandler;
	protected Date now;
	protected HtmlScriptCollector templateScriptCollector;
	public String doButtonUpdateLangPrefAction() {
        // Type Java code that runs when the component is clicked

        // 
        return "success";
    }
    public void handleMenui18nPrefValueChange(ValueChangeEvent valueChangedEvent) {
        // Type Java code to handle value changed event here
        // Note, valueChangeEvent contains new and old values
    }
    public void handleMenui18nPrefValueChange1(
            ValueChangeEvent valueChangedEvent) {
}
    /**
     * 
     * @return
     */
    public String doButtonDoLogoutBtnAction() {
        // Type Java code that runs when the component is clicked
        UserHandler uh = this.getUserHandler();
        
        if (uh != null) {
	        UserIntf userIntf = uh.getUser();
	        UserBO user = (UserBO)userIntf;
	        uh.setUser(null);
	        try {
	            if (user != null && user.isUserAuthenticated()) {
	   	            user.logout();
	            }
	        } 
	        catch (Exception e) {
	            ;
	        }
	        
        }
        //this.getSessionScope().remove("userHandler");
        
        try {
	        RouteListHandler rlh = (RouteListHandler)this.getSessionScope().get("routeListHandler");
	        if (rlh != null) {
	            rlh.cleanUp();
	        }
	        
	        this.getSessionScope().remove("routeListHandler");
	        
	        RDLForDayHandler rdl = (RDLForDayHandler)this.getSessionScope().get("RDLForDay");
	        if (rdl != null) {
	            rdl.clear();
	            this.getSessionScope().remove("RDLForDay");
	        }
	        RDLForWeekHandler rdlw = (RDLForWeekHandler)this.getSessionScope().get("RDLForWeek");
	        if (rdlw != null) {
	            rdlw.clear();
	            this.getSessionScope().remove("RDLForWeek");
	        }
	
	        BlueChipDrawManagementHandler bcdh = (BlueChipDrawManagementHandler)this.getSessionScope().get("blueChipDrawManagementHandler");
	        if (bcdh != null) {
	            bcdh.clear();
	            this.getSessionScope().remove("blueChipDrawManagementHandler");
	        }
	        
	        BlueChipWeeklyProductOrderHandler bcwh = (BlueChipWeeklyProductOrderHandler)this.getSessionScope().get("blueChipPOForWeek");
	        if (bcwh != null) {
	            bcwh.clear();
	            this.getSessionScope().remove("blueChipPOForWeek");
	        }
        }
        catch (Exception e) {
            ;
        }
        return "success";
    }
    
	protected HtmlForm getFormLogout() {
		if (formLogout == null) {
			formLogout = (HtmlForm) findComponentInRoot("formLogout");
		}
		return formLogout;
	}
	protected HtmlOutputFormat getFormatWelcomeMessage() {
		if (formatWelcomeMessage == null) {
			formatWelcomeMessage = (HtmlOutputFormat) findComponentInRoot("formatWelcomeMessage");
		}
		return formatWelcomeMessage;
	}
	protected HtmlCommandExButton getButtonDoLogoutBtn() {
		if (buttonDoLogoutBtn == null) {
			buttonDoLogoutBtn = (HtmlCommandExButton) findComponentInRoot("buttonDoLogoutBtn");
		}
		return buttonDoLogoutBtn;
	}
	protected HtmlInputHelperKeybind getInputHelperKeybind1() {
		if (inputHelperKeybind1 == null) {
			inputHelperKeybind1 = (HtmlInputHelperKeybind) findComponentInRoot("inputHelperKeybind1");
		}
		return inputHelperKeybind1;
	}
	protected HtmlOutputText getTextGlobalMessageText() {
		if (textGlobalMessageText == null) {
			textGlobalMessageText = (HtmlOutputText) findComponentInRoot("textGlobalMessageText");
		}
		return textGlobalMessageText;
	}
	/** 
	 * @managed-bean true
	 */
	protected MessageHandler getGlobalMessage() {
		if (globalMessage == null) {
			globalMessage = (MessageHandler) getManagedBean("globalMessage");
		}
		return globalMessage;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setGlobalMessage(MessageHandler globalMessage) {
		this.globalMessage = globalMessage;
	}
	/** 
	 * @managed-bean true
	 */
	protected LocaleHandler getUserLocaleHandler() {
		if (userLocaleHandler == null) {
			userLocaleHandler = (LocaleHandler) getManagedBean("userLocaleHandler");
		}
		return userLocaleHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUserLocaleHandler(LocaleHandler userLocaleHandler) {
		this.userLocaleHandler = userLocaleHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected OmnitureTrackingCode getOmnitureTracking() {
		if (omnitureTracking == null) {
			omnitureTracking = (OmnitureTrackingCode) getManagedBean("omnitureTracking");
		}
		return omnitureTracking;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setOmnitureTracking(OmnitureTrackingCode omnitureTracking) {
		this.omnitureTracking = omnitureTracking;
	}
	/** 
	 * @managed-bean true
	 */
	protected Date getCurrentDateTime() {
		if (currentDateTime == null) {
			currentDateTime = (Date) getManagedBean("currentDateTime");
		}
		return currentDateTime;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setCurrentDateTime(Date currentDateTime) {
		this.currentDateTime = currentDateTime;
	}
	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUserHandler() {
		if (userHandler == null) {
			userHandler = (UserHandler) getManagedBean("userHandler");
		}
		return userHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUserHandler(UserHandler userHandler) {
		this.userHandler = userHandler;
	}
	public Date getNow() {
		if (now == null) {
			now = new Date();
		}
		return now;
	}
	public void setNow(Date now) {
		this.now = now;
	}
	protected HtmlScriptCollector getTemplateScriptCollector() {
		if (templateScriptCollector == null) {
			templateScriptCollector = (HtmlScriptCollector) findComponentInRoot("templateScriptCollector");
		}
		return templateScriptCollector;
	}
	/** 
	 * @managed-bean true
	 */
	protected MessageHandler getGlobalMessage1() {
		if (globalMessage == null) {
			globalMessage = (MessageHandler) getManagedBean("globalMessage");
		}
		return globalMessage;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setGlobalMessage1(MessageHandler globalMessage) {
		this.globalMessage = globalMessage;
	}
}
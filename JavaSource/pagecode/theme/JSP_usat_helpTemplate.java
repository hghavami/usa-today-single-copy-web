/*
 * Created on Jun 14, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.theme;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlOutputText;
/**
 * @author aeast
 * @date Jun 14, 2007
 * @class themeJSP_usat_helpTemplate
 * 
 * 
 * 
 */
public class JSP_usat_helpTemplate extends PageCodeBase {

    protected HtmlScriptCollector scriptCollector1;
    protected HtmlOutputLinkEx linkExCloseWindowTop;
    protected HtmlOutputText textCloseWindowTop;
    protected HtmlOutputLinkEx linkExCloseWindowBottom;
    protected HtmlOutputText textCloseWindowBottom;
	protected HtmlOutputText textOmnitureTrackingCode1;
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlOutputLinkEx getLinkExCloseWindowTop() {
        if (linkExCloseWindowTop == null) {
            linkExCloseWindowTop = (HtmlOutputLinkEx) findComponentInRoot("linkExCloseWindowTop");
        }
        return linkExCloseWindowTop;
    }
    protected HtmlOutputText getTextCloseWindowTop() {
        if (textCloseWindowTop == null) {
            textCloseWindowTop = (HtmlOutputText) findComponentInRoot("textCloseWindowTop");
        }
        return textCloseWindowTop;
    }
    protected HtmlOutputLinkEx getLinkExCloseWindowBottom() {
        if (linkExCloseWindowBottom == null) {
            linkExCloseWindowBottom = (HtmlOutputLinkEx) findComponentInRoot("linkExCloseWindowBottom");
        }
        return linkExCloseWindowBottom;
    }
    protected HtmlOutputText getTextCloseWindowBottom() {
        if (textCloseWindowBottom == null) {
            textCloseWindowBottom = (HtmlOutputText) findComponentInRoot("textCloseWindowBottom");
        }
        return textCloseWindowBottom;
    }
	protected HtmlOutputText getTextOmnitureTrackingCode1() {
		if (textOmnitureTrackingCode1 == null) {
			textOmnitureTrackingCode1 = (HtmlOutputText) findComponentInRoot("textOmnitureTrackingCode1");
		}
		return textOmnitureTrackingCode1;
	}
}
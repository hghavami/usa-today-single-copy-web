/*
 * Created on Jan 11, 2007
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package pagecode;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.champion.handlers.RouteListHandler;
import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.champion.handlers.blueChipDraw.BlueChipDrawManagementHandler;
import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.UsatLoginFailedException;
import com.usatoday.singlecopy.model.bo.users.UserBO;
import com.usatoday.singlecopy.model.interfaces.users.AccessRightsIntf;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;
import com.ibm.faces.component.html.HtmlInputHelperKeybind;
/**
 * @author aeast
 * @date Jan 11, 2007
 * @class pagecodeIndex
 * 
 * 
 * 
 */
public class Index extends PageCodeBase {

    protected HtmlCommandExButton button1;
    protected HtmlScriptCollector scriptCollector2;
    protected HtmlForm formLogout;
    protected HtmlOutputFormat formatWelcomeMessage;
    protected HtmlOutputText textLogoutLink;
    protected HtmlScriptCollector scriptCollector1;
    protected HtmlForm loginForm;
    protected HtmlCommandLink linkLogout;
    protected HtmlMessages messagesFormSubmissionErrors;
    protected HtmlOutputText textUserId;
    protected HtmlInputText textInputEmail;
    protected HtmlMessage messageEmailErrorMessage;
    protected HtmlOutputText textPassword;
    protected HtmlInputSecret secretPassword;
    protected HtmlCommandExButton buttonLogin;
    protected HtmlOutputText textForgotPassword;
    protected HtmlOutputText textPwdRules;
    protected HtmlInputHidden remmsg;
	protected HtmlOutputText textA1;
	protected HtmlScriptCollector templateScriptCollector;
	protected HtmlOutputText textGlobalMessageText;
	protected HtmlCommandExButton buttonDoLogoutBtn;
	protected HtmlInputHelperKeybind inputHelperKeybind1;
    protected HtmlCommandExButton getButton1() {
        if (button1 == null) {
            button1 = (HtmlCommandExButton) findComponentInRoot("button1");
        }
        return button1;
    }
    @SuppressWarnings("unchecked")
	public String doButtonLoginAction() {
        // Type Java code that runs when the component is clicked

        UserHandler uh = (UserHandler)this.getSessionScope().get("userHandler");
        UserIntf user = null;
        if (uh != null) {
            user = uh.getUser();
        }
        
        // remove any session expired messages
        this.getSessionScope().remove("SESS_EXPIRED");

    	Object o = this.getFacesContext().getExternalContext().getRequest();
    	String userAgent = "unknown";
    	if (o instanceof HttpServletRequest){
    		
    		HttpServletRequest req = (HttpServletRequest)o;
        	userAgent = req.getHeader("user-agent");
    	}
        
        // if user exists and is already authenticated simply forward to home page
        if (user != null && uh.getAuthenticated()) {
            //origEmail = user.getEmailAddress();
            //origPwd = user.getPassword();
        	//try {
        	//    user.setPassword("");
        	 //} catch (UsatException ue) {
             //  	System.out.println("Password Authentication Failed. E: " + ue.getMessage());           
             //   	  	
             //}
       		System.out.println("Index.java::loginButton() - Already authenticated forwarding to home page: User ID: " + user.getEmailAddress() + "  Browser info: " + userAgent);
       		try {
       			// force this thread to sleep for one second in case the session data hasn't loaded.
       			Thread.sleep(1000);
       		}catch (Exception e) {
       			;
       		}
       		
        	return "duplicatesession";
        }
        
        
        String uid = uh.getEmailAddress();
        uid = uid.trim();
        String pwd = this.getSecretPassword().getValue().toString();
        pwd = pwd.trim();
        try {
        	// store the browser information for this session
        	uh.setBrowserUserAgentValue(userAgent);
        	
	    	@SuppressWarnings("unused")
			javax.mail.internet.InternetAddress address = new javax.mail.internet.InternetAddress(uid);
        
    	    user = UserBO.authenticateUser(uid, pwd);
    	    
	    	uh.setAutoAuthOnly(false);            
		} 
		catch (javax.mail.internet.AddressException e) {
            FacesContext context = FacesContext.getCurrentInstance( );
            FacesMessage facesMsg = 
                new FacesMessage(FacesMessage.SEVERITY_INFO, "The email address entered is not a valid email address format.", e.getLocalizedMessage());
            context.addMessage(null, facesMsg);
            return "failure";
        }
		catch (UsatLoginFailedException ulf) {
		    
            FacesContext context = FacesContext.getCurrentInstance( );
            if (ulf.getErrorCode() == UsatLoginFailedException.ACCOUNT_NOT_ACTIVE) {
                FacesMessage facesMsg = 
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Authentication Failed. Your account has not been activated. Contact your market for assistance.", null);
                context.addMessage(null, facesMsg);
                
            }
            else {
                FacesMessage facesMsg = 
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Authentication Failed. Bad Email Address and/or Password.", null);
                context.addMessage(null, facesMsg);
                
            }
            return "failure";
		}
        catch (UsatException ue) {
            // handle failed log in
            //System.out.println("Authentication Failed." + ue.getMessage());
            FacesContext context = FacesContext.getCurrentInstance( );
            FacesMessage facesMsg = 
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Authentication Failed.", ue.getMessage());
            context.addMessage(null, facesMsg);
            return "failure";
        }
        catch (Exception e) {
            // 
            System.out.println("Authentication Failed. E: " + e.getMessage());
            FacesContext context = FacesContext.getCurrentInstance( );
            FacesMessage facesMsg = 
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Authentication Failed.", "");
            context.addMessage(null, facesMsg);
            return "failure";
        }
            
        
        uh.setUser(user);
        uh.setEmailAddress(user.getEmailAddress());
        
        
        if (user.hasAccess(AccessRightsIntf.ROUTE_DRAW_MNGMT)) {
	        RouteListHandler rlh = (RouteListHandler)this.getSessionScope().get("routeListHandler");
	        if (rlh == null) {
	            rlh = new RouteListHandler();
	            rlh.setOwningUser((UserBO)user);
	            this.getSessionScope().put("routeListHandler", rlh);
	        }
	        else {
	            rlh.setOwningUser((UserBO)user);
	        }
        }

        if (user.hasAccess(AccessRightsIntf.BLUE_CHIP_DRAW)) {
            BlueChipDrawManagementHandler bcdh = (BlueChipDrawManagementHandler)this.getSessionScope().get("blueChipDrawManagementHandler");
            if (bcdh == null) {
                bcdh = new BlueChipDrawManagementHandler();
                bcdh.setOwningUser((UserBO)user);
                this.getSessionScope().put("blueChipDrawManagementHandler", bcdh);
            }
            else {
                bcdh.setOwningUser((UserBO)user);
            }
        }
        
        return "success";
    }
    
    public void handleTextInputEmailValueChange(
            ValueChangeEvent valueChangedEvent) {
}
    protected HtmlScriptCollector getScriptCollector2() {
        if (scriptCollector2 == null) {
            scriptCollector2 = (HtmlScriptCollector) findComponentInRoot("scriptCollector2");
        }
        return scriptCollector2;
    }
    protected HtmlForm getFormLogout() {
        if (formLogout == null) {
            formLogout = (HtmlForm) findComponentInRoot("formLogout");
        }
        return formLogout;
    }
    protected HtmlOutputFormat getFormatWelcomeMessage() {
        if (formatWelcomeMessage == null) {
            formatWelcomeMessage = (HtmlOutputFormat) findComponentInRoot("formatWelcomeMessage");
        }
        return formatWelcomeMessage;
    }
    protected HtmlOutputText getTextLogoutLink() {
        if (textLogoutLink == null) {
            textLogoutLink = (HtmlOutputText) findComponentInRoot("textLogoutLink");
        }
        return textLogoutLink;
    }
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlForm getLoginForm() {
        if (loginForm == null) {
            loginForm = (HtmlForm) findComponentInRoot("loginForm");
        }
        return loginForm;
    }
    protected HtmlCommandLink getLinkLogout() {
        if (linkLogout == null) {
            linkLogout = (HtmlCommandLink) findComponentInRoot("linkLogout");
        }
        return linkLogout;
    }
    protected HtmlMessages getMessagesFormSubmissionErrors() {
        if (messagesFormSubmissionErrors == null) {
            messagesFormSubmissionErrors = (HtmlMessages) findComponentInRoot("messagesFormSubmissionErrors");
        }
        return messagesFormSubmissionErrors;
    }
    protected HtmlOutputText getTextUserId() {
        if (textUserId == null) {
            textUserId = (HtmlOutputText) findComponentInRoot("textUserId");
        }
        return textUserId;
    }
    protected HtmlInputText getTextInputEmail() {
        if (textInputEmail == null) {
            textInputEmail = (HtmlInputText) findComponentInRoot("textInputEmail");
        }
        return textInputEmail;
    }
    protected HtmlMessage getMessageEmailErrorMessage() {
        if (messageEmailErrorMessage == null) {
            messageEmailErrorMessage = (HtmlMessage) findComponentInRoot("messageEmailErrorMessage");
        }
        return messageEmailErrorMessage;
    }
    protected HtmlOutputText getTextPassword() {
        if (textPassword == null) {
            textPassword = (HtmlOutputText) findComponentInRoot("textPassword");
        }
        return textPassword;
    }
    protected HtmlInputSecret getSecretPassword() {
        if (secretPassword == null) {
            secretPassword = (HtmlInputSecret) findComponentInRoot("secretPassword");
        }
        return secretPassword;
    }
    protected HtmlCommandExButton getButtonLogin() {
        if (buttonLogin == null) {
            buttonLogin = (HtmlCommandExButton) findComponentInRoot("buttonLogin");
        }
        return buttonLogin;
    }
    protected HtmlOutputText getTextForgotPassword() {
        if (textForgotPassword == null) {
            textForgotPassword = (HtmlOutputText) findComponentInRoot("textForgotPassword");
        }
        return textForgotPassword;
    }
    protected HtmlOutputText getTextPwdRules() {
        if (textPwdRules == null) {
            textPwdRules = (HtmlOutputText) findComponentInRoot("textPwdRules");
        }
        return textPwdRules;
    }
    protected HtmlInputHidden getRemmsg() {
        if (remmsg == null) {
            remmsg = (HtmlInputHidden) findComponentInRoot("Id0");
        }
        return remmsg;
    }
	protected HtmlOutputText getTextA1() {
		if (textA1 == null) {
			textA1 = (HtmlOutputText) findComponentInRoot("textA1");
		}
		return textA1;
	}
	protected HtmlScriptCollector getTemplateScriptCollector() {
		if (templateScriptCollector == null) {
			templateScriptCollector = (HtmlScriptCollector) findComponentInRoot("templateScriptCollector");
		}
		return templateScriptCollector;
	}
	protected HtmlOutputText getTextGlobalMessageText() {
		if (textGlobalMessageText == null) {
			textGlobalMessageText = (HtmlOutputText) findComponentInRoot("textGlobalMessageText");
		}
		return textGlobalMessageText;
	}
	protected HtmlCommandExButton getButtonDoLogoutBtn() {
		if (buttonDoLogoutBtn == null) {
			buttonDoLogoutBtn = (HtmlCommandExButton) findComponentInRoot("buttonDoLogoutBtn");
		}
		return buttonDoLogoutBtn;
	}
	protected HtmlInputHelperKeybind getInputHelperKeybind1() {
		if (inputHelperKeybind1 == null) {
			inputHelperKeybind1 = (HtmlInputHelperKeybind) findComponentInRoot("inputHelperKeybind1");
		}
		return inputHelperKeybind1;
	}
}
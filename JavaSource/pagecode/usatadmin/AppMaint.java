/*
 * Created on Jul 23, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.usatadmin;

import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;

import org.joda.time.DateTime;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.champion.handlers.blueChipDraw.BlueChipDrawManagementHandler;
import com.usatoday.singlecopy.model.util.EmailScheduleCache;
import com.usatoday.singlecopy.model.util.IncidentMarketCache;
import com.usatoday.singlecopy.model.util.MarketCache;
import com.usatoday.singlecopy.model.util.ProductDescriptionCache;
import com.usatoday.singlecopy.model.util.ProductOrderTypesCache;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;
import com.usatoday.singlecopy.model.util.ZeroDrawReasonCodeCache;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlSelectOneRadio;
/**
 * @author aeast
 * @date Jul 23, 2007
 * @class usatadminAppMaintenance
 * 
 * 
 * 
 */
public class AppMaint extends PageCodeBase {

    protected HtmlScriptCollector scriptCollector1;
    protected HtmlForm form1;
    protected HtmlMessages messages1;
    protected HtmlCommandExButton buttonReloadCaches;
    protected HtmlOutputText textLastMessage;
    protected HtmlOutputText text1;
    protected HtmlPanelGrid grid1;
    protected HtmlCommandExButton buttonReloadConfiguration;
    protected HtmlOutputText text2;
    protected HtmlOutputText text3;
    protected HtmlCommandExButton buttonToggleAlerts;
    protected HtmlOutputText text4;
    protected HtmlInputText textCheckFreq;
    protected HtmlMessage message1;
    protected HtmlCommandExButton buttonUpdateCheckFrequency;
    protected HtmlOutputText textBlueChipStaleDataLabel;
    protected HtmlInputText textBlueChipStaleDateThreshold;
    protected HtmlMessage message2;
    protected HtmlCommandExButton buttonUpdateBlueChipStaleDataSetting;
	protected HtmlCommandExButton buttonUpdateGlobalMessage;
	protected HtmlInputText textGlobalMessageTextInput;
	protected HtmlSelectBooleanCheckbox checkboxShowMessage;
	protected HtmlOutputText textShowGlobalMessageDescription;
	protected HtmlOutputLinkEx linkEx1;
	protected HtmlOutputText text5;
	protected HtmlSelectOneRadio radioWhoGetsIt;
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlForm getForm1() {
        if (form1 == null) {
            form1 = (HtmlForm) findComponentInRoot("form1");
        }
        return form1;
    }
    public String doButtonReloadConfigurationAction() {
        // Type Java code that runs when the component is clicked

        USATApplicationConstants.loadProperties();

        this.getTextLastMessage().setValue("The application properties have been reloaded. " + (new DateTime()).toString("mm/dd/yyyy hh:mm:ss"));
        
        return "";
    }
    protected HtmlMessages getMessages1() {
        if (messages1 == null) {
            messages1 = (HtmlMessages) findComponentInRoot("messages1");
        }
        return messages1;
    }
    protected HtmlCommandExButton getButtonReloadCaches() {
        if (buttonReloadCaches == null) {
            buttonReloadCaches = (HtmlCommandExButton) findComponentInRoot("buttonReloadCaches");
        }
        return buttonReloadCaches;
    }
    public String doButtonReloadCachesAction() {
        // Type Java code that runs when the component is clicked

        ZeroDrawReasonCodeCache.getInstance().resetCache();
        ProductDescriptionCache.getInstance().resetCache();
        ProductOrderTypesCache.getProductOrderTypesCache().resetCache();
        MarketCache.getInstance().resetCache();
        EmailScheduleCache.getInstance().resetCache();
        IncidentMarketCache.getInstance().resetCache();
        
        this.getTextLastMessage().setValue("The application caches have been refreshed.");
        
        return "";
    }
    protected HtmlOutputText getTextLastMessage() {
        if (textLastMessage == null) {
            textLastMessage = (HtmlOutputText) findComponentInRoot("textLastMessage");
        }
        return textLastMessage;
    }
    protected HtmlOutputText getText1() {
        if (text1 == null) {
            text1 = (HtmlOutputText) findComponentInRoot("text1");
        }
        return text1;
    }
    protected HtmlPanelGrid getGrid1() {
        if (grid1 == null) {
            grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
        }
        return grid1;
    }
    protected HtmlCommandExButton getButtonReloadConfiguration() {
        if (buttonReloadConfiguration == null) {
            buttonReloadConfiguration = (HtmlCommandExButton) findComponentInRoot("buttonReloadConfiguration");
        }
        return buttonReloadConfiguration;
    }
    protected HtmlOutputText getText2() {
        if (text2 == null) {
            text2 = (HtmlOutputText) findComponentInRoot("text2");
        }
        return text2;
    }
    protected HtmlOutputText getText3() {
        if (text3 == null) {
            text3 = (HtmlOutputText) findComponentInRoot("text3");
        }
        return text3;
    }
    protected HtmlCommandExButton getButtonToggleAlerts() {
        if (buttonToggleAlerts == null) {
            buttonToggleAlerts = (HtmlCommandExButton) findComponentInRoot("buttonToggleAlerts");
        }
        return buttonToggleAlerts;
    }
    public String doButtonToggleAlertsAction() {
        // Type Java code that runs when the component is clicked

        // 
        if (USATApplicationConstants.alarmsEnabled) {
            this.getTextLastMessage().setValue("Alerts are now OFF.");
            USATApplicationConstants.alarmsEnabled = false;
        }
        else {
            this.getTextLastMessage().setValue("Alerts are now ON. Recipients: " + USATApplicationConstants.alarmRecipients);
            USATApplicationConstants.alarmsEnabled = true;
        }
        
        
        return "";
    }
    protected HtmlOutputText getText4() {
        if (text4 == null) {
            text4 = (HtmlOutputText) findComponentInRoot("text4");
        }
        return text4;
    }
    protected HtmlInputText getTextCheckFreq() {
        if (textCheckFreq == null) {
            textCheckFreq = (HtmlInputText) findComponentInRoot("textCheckFreq");
        }
        return textCheckFreq;
    }
    protected HtmlMessage getMessage1() {
        if (message1 == null) {
            message1 = (HtmlMessage) findComponentInRoot("message1");
        }
        return message1;
    }
    protected HtmlCommandExButton getButtonUpdateCheckFrequency() {
        if (buttonUpdateCheckFrequency == null) {
            buttonUpdateCheckFrequency = (HtmlCommandExButton) findComponentInRoot("buttonUpdateCheckFrequency");
        }
        return buttonUpdateCheckFrequency;
    }
    public String doButtonUpdateCheckFrequencyAction() {
        // Type Java code that runs when the component is clicked

        Object o = this.getTextCheckFreq().getValue();
        Long freqLong = (Long)o;
        
        try {
        
            long freq = freqLong.longValue();
            if (freq > 999) {
                USATApplicationConstants.backEndCheckFrequency = freq;
                this.getTextLastMessage().setValue("New Check Frequency Set: " + String.valueOf(freq));
                
            }
            else {
                this.getTextLastMessage().setValue("New Check Frequency Not Set, Invalid value. ");
            }
        }
        catch (Exception e) {
            this.getTextLastMessage().setValue("New Check Frequency Not Set: " + e.getMessage());
        }
        
        return "";
    }
    protected HtmlOutputText getTextBlueChipStaleDataLabel() {
        if (textBlueChipStaleDataLabel == null) {
            textBlueChipStaleDataLabel = (HtmlOutputText) findComponentInRoot("textBlueChipStaleDataLabel");
        }
        return textBlueChipStaleDataLabel;
    }
    protected HtmlInputText getTextBlueChipStaleDateThreshold() {
        if (textBlueChipStaleDateThreshold == null) {
            textBlueChipStaleDateThreshold = (HtmlInputText) findComponentInRoot("textBlueChipStaleDateThreshold");
        }
        return textBlueChipStaleDateThreshold;
    }
    protected HtmlMessage getMessage2() {
        if (message2 == null) {
            message2 = (HtmlMessage) findComponentInRoot("message2");
        }
        return message2;
    }
    protected HtmlCommandExButton getButtonUpdateBlueChipStaleDataSetting() {
        if (buttonUpdateBlueChipStaleDataSetting == null) {
            buttonUpdateBlueChipStaleDataSetting = (HtmlCommandExButton) findComponentInRoot("buttonUpdateBlueChipStaleDataSetting");
        }
        return buttonUpdateBlueChipStaleDataSetting;
    }
    public String doButtonUpdateBlueChipStaleDataSettingAction() {
        // Type Java code that runs when the component is clicked

        Object o = this.getTextBlueChipStaleDateThreshold().getValue();
        String thresholdStr = (String)o;
        
        try {
        
            int threshold = Integer.parseInt(thresholdStr);
            BlueChipDrawManagementHandler.setDefaultStaleDataThreshold(threshold);
            this.getTextLastMessage().setValue("New Stale Data Threshold Set: " + thresholdStr);
                
        }
        catch (Exception e) {
            this.getTextLastMessage().setValue("New Stale Data Threshold NOT Set: " + e.getMessage());
        }
        
        return "";    }
	protected HtmlCommandExButton getButtonUpdateGlobalMessage() {
		if (buttonUpdateGlobalMessage == null) {
			buttonUpdateGlobalMessage = (HtmlCommandExButton) findComponentInRoot("buttonUpdateGlobalMessage");
		}
		return buttonUpdateGlobalMessage;
	}
	public String doButtonUpdateGlobalMessageAction() {
		// Type Java code that runs when the component is clicked

		// don't need to do anything JSF updates the bean automatically 
		
		
		return "";
	}
	protected HtmlInputText getTextGlobalMessageTextInput() {
		if (textGlobalMessageTextInput == null) {
			textGlobalMessageTextInput = (HtmlInputText) findComponentInRoot("textGlobalMessageTextInput");
		}
		return textGlobalMessageTextInput;
	}
	protected HtmlSelectBooleanCheckbox getCheckboxShowMessage() {
		if (checkboxShowMessage == null) {
			checkboxShowMessage = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxShowMessage");
		}
		return checkboxShowMessage;
	}
	protected HtmlOutputText getTextShowGlobalMessageDescription() {
		if (textShowGlobalMessageDescription == null) {
			textShowGlobalMessageDescription = (HtmlOutputText) findComponentInRoot("textShowGlobalMessageDescription");
		}
		return textShowGlobalMessageDescription;
	}
	protected HtmlOutputLinkEx getLinkEx1() {
		if (linkEx1 == null) {
			linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
		}
		return linkEx1;
	}
	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}
	protected HtmlSelectOneRadio getRadioWhoGetsIt() {
		if (radioWhoGetsIt == null) {
			radioWhoGetsIt = (HtmlSelectOneRadio) findComponentInRoot("radioWhoGetsIt");
		}
		return radioWhoGetsIt;
	}
}
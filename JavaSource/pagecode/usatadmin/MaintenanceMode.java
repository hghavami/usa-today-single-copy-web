/*
 * Created on Sep 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pagecode.usatadmin;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.usatoday.champion.handlers.MessageHandler;
import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.singlecopy.UsatException;
import com.usatoday.champion.handlers.ApplicationFlagHandler;
import javax.faces.component.html.HtmlMessages;
import javax.faces.context.FacesContext;
/**
 * @author aeast
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MaintenanceMode extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm formMaintenanceMode;
	protected HtmlOutputLinkEx linkEx1;
	protected HtmlOutputText text1;
	protected HtmlCommandExButton buttonEnterMaintenanceMode;
	protected ApplicationFlagHandler applicationModeFlag;
	protected HtmlMessages messages1;
	protected HtmlOutputLinkEx linkEx2;
	protected HtmlOutputText text2;
	protected HtmlOutputText textAlreadyInMaintMode;
	protected MessageHandler globalMessage;
	protected UserHandler userHandler;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}
	protected HtmlForm getFormMaintenanceMode() {
		if (formMaintenanceMode == null) {
			formMaintenanceMode = (HtmlForm) findComponentInRoot("formMaintenanceMode");
		}
		return formMaintenanceMode;
	}
	protected HtmlOutputLinkEx getLinkEx1() {
		if (linkEx1 == null) {
			linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
		}
		return linkEx1;
	}
	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}
	protected HtmlCommandExButton getButtonEnterMaintenanceMode() {
		if (buttonEnterMaintenanceMode == null) {
			buttonEnterMaintenanceMode = (HtmlCommandExButton) findComponentInRoot("buttonEnterMaintenanceMode");
		}
		return buttonEnterMaintenanceMode;
	}
	public String doButtonEnterMaintenanceModeAction() {
		// Type Java code that runs when the component is clicked

		String errorMessage = "";
		try {
			MessageHandler globalMessage = this.getGlobalMessage();
			if (!globalMessage.getShowMessage().booleanValue()) {
				errorMessage = "You cannot put the site in maintenance mode without first warning people. Please enter a global message first and then put the site into maintenance mode.";
				throw new UsatException();				
			}

			UserHandler u = this.getUserHandler();
			
			System.out.println("Single Copy Web Site put into maintenance mode by: " + u.getDisplayName() + "  : " + u.getEmailAddress());
			
			ApplicationFlagHandler maintenanceFlag = this.getApplicationModeFlag();
			maintenanceFlag.setBooleanValue(true);

    		FacesContext context = FacesContext.getCurrentInstance( );
            FacesMessage facesMsg = 
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "The site is now in maintenance mode.", null);
            
            context.addMessage(null, facesMsg);
			
			
			return "success";
			
		}
		catch (Exception e) {
    		FacesContext context = FacesContext.getCurrentInstance( );
            FacesMessage facesMsg = 
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Failed to put site into maintenance mode: " + errorMessage + " : " + e.getMessage(), null);
            
            context.addMessage(null, facesMsg);
		}	
		
		return "failure";
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}
	protected HtmlOutputLinkEx getLinkEx2() {
		if (linkEx2 == null) {
			linkEx2 = (HtmlOutputLinkEx) findComponentInRoot("linkEx2");
		}
		return linkEx2;
	}
	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}
	protected HtmlOutputText getTextAlreadyInMaintMode() {
		if (textAlreadyInMaintMode == null) {
			textAlreadyInMaintMode = (HtmlOutputText) findComponentInRoot("textAlreadyInMaintMode");
		}
		return textAlreadyInMaintMode;
	}
	/** 
	 * @managed-bean true
	 */
	protected ApplicationFlagHandler getApplicationModeFlag() {
		if (applicationModeFlag == null) {
			applicationModeFlag = (ApplicationFlagHandler) getManagedBean("applicationModeFlag");
		}
		return applicationModeFlag;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setApplicationModeFlag(
			ApplicationFlagHandler applicationModeFlag) {
		this.applicationModeFlag = applicationModeFlag;
	}
	/** 
	 * @managed-bean true
	 */
	protected MessageHandler getGlobalMessage() {
		if (globalMessage == null) {
			globalMessage = (MessageHandler) getManagedBean("globalMessage");
		}
		return globalMessage;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setGlobalMessage(MessageHandler globalMessage) {
		this.globalMessage = globalMessage;
	}
	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUserHandler() {
		if (userHandler == null) {
			userHandler = (UserHandler) getManagedBean("userHandler");
		}
		return userHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUserHandler(UserHandler userHandler) {
		this.userHandler = userHandler;
	}
}
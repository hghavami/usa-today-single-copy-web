/*
 * Created on May 11, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode;

import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlForm;
/**
 * @author aeast
 * @date May 11, 2007
 * @class pagecodeSiteNotAvailable
 * 
 * 
 * 
 */
public class SiteNotAvailable extends PageCodeBase {

    protected HtmlScriptCollector scriptCollector2;
    protected HtmlOutputFormat formatWelcomeMessage;
    protected HtmlCommandLink linkLogout;
    protected HtmlOutputText textLogoutLink;
    protected HtmlScriptCollector scriptCollector1;
    protected HtmlForm formLogout;
    protected HtmlOutputText textSiteDownMessage;
    protected HtmlScriptCollector getScriptCollector2() {
        if (scriptCollector2 == null) {
            scriptCollector2 = (HtmlScriptCollector) findComponentInRoot("scriptCollector2");
        }
        return scriptCollector2;
    }
    protected HtmlOutputFormat getFormatWelcomeMessage() {
        if (formatWelcomeMessage == null) {
            formatWelcomeMessage = (HtmlOutputFormat) findComponentInRoot("formatWelcomeMessage");
        }
        return formatWelcomeMessage;
    }
    protected HtmlCommandLink getLinkLogout() {
        if (linkLogout == null) {
            linkLogout = (HtmlCommandLink) findComponentInRoot("linkLogout");
        }
        return linkLogout;
    }
    protected HtmlOutputText getTextLogoutLink() {
        if (textLogoutLink == null) {
            textLogoutLink = (HtmlOutputText) findComponentInRoot("textLogoutLink");
        }
        return textLogoutLink;
    }
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlForm getFormLogout() {
        if (formLogout == null) {
            formLogout = (HtmlForm) findComponentInRoot("formLogout");
        }
        return formLogout;
    }
    protected HtmlOutputText getTextSiteDownMessage() {
        if (textSiteDownMessage == null) {
            textSiteDownMessage = (HtmlOutputText) findComponentInRoot("textSiteDownMessage");
        }
        return textSiteDownMessage;
    }
}
/*
 * Created on Sep 8, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.usatoday.champion.handlers;


/**
 * @author aeast
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MessageHandler {

	private String message = "";
	private boolean showMessage = false;
	private String showToWhom = "all";
	
	/**
	 * 
	 */
	public MessageHandler() {
		super();
	}

	/**
	 * @return Returns the message.
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message The message to set.
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return Returns the showMessage.
	 */
	
	public Boolean getShowMessage() {
		if (this.showMessage && this.message != null && this.message.trim().length() > 0) {
			//return true;
			return Boolean.TRUE;
		}    
		else {
			//return false;
			return Boolean.FALSE;
		}
	}
	/**
	 * @param showMessage The showMessage to set.
	 */

	public void setShowMessage(Boolean showMessage) {
		this.showMessage = showMessage.booleanValue();
	}
	/**
	 * @return Returns the showToWhom.
	 */
	public String getShowToWhom() {
		return showToWhom;
	}
	/**
	 * @param showToWhom The showToWhom to set.
	 */
	public void setShowToWhom(String showToWhom) {
		if (showToWhom == null) {
			this.showToWhom = "all";
		}
		this.showToWhom = showToWhom;
	}
	
	public boolean showToAll() {
		if (this.showToWhom.equalsIgnoreCase("all")) {
			return true;
		}
		else {
			return false;
		}
	}

	public boolean showToHotels() {
		if (this.showToAll() || this.showToWhom.equalsIgnoreCase("hotel")) {
			return true;
		}
		else {
			return false;
		}
	}

	public boolean showToContractors() {
		if (this.showToAll() || this.showToWhom.equalsIgnoreCase("contractor")) {
			return true;
		}
		else {
			return false;
		}
	}
	
}

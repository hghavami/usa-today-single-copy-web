/*
 * Created on Mar 5, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyDrawLocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf;

/**
 * @author aeast
 * @date Mar 5, 2007
 * @class LocationHandler
 * 
 * 
 * 
 */
public class LocationHandler {

    DailyDrawLocationIntf ddl = null;
    
    ArrayList<ProductOrderHandler> productOrders = new ArrayList<ProductOrderHandler>();
    /**
     * 
     */
    public LocationHandler() {
        super();
    }

    public LocationHandler(DailyDrawLocationIntf source) {
        super();

        this.ddl = source;
        // convert product orders to product order handlers
        Collection<ProductOrderIntf> poBO = source.getProductOrders();
        Iterator<ProductOrderIntf> itr = poBO.iterator();
        while (itr.hasNext()) {
            DailyProductOrderIntf dpo = (DailyProductOrderIntf)itr.next();
            ProductOrderHandler dpoh = new ProductOrderHandler(dpo);
            productOrders.add(dpoh);
        }        
    }
    
    /**
     * @return Returns the locationAddress1.
     */
    public String getLocationAddress1() {
        return this.ddl.getAddress1();
    }
    /**
     * @param locationAddress1 The locationAddress1 to set.
     */
    public void setLocationAddress1(String locationAddress1) {
        ; // ignore
    }
    /**
     * @return Returns the locationID.
     */
    public String getLocationID() {
        return this.ddl.getLocationID();
    }
    /**
     * @param locationID The locationID to set.
     */
    public void setLocationID(String locationID) {
        ; // ignore setter
    }
    /**
     * @return Returns the locationName.
     */
    public String getLocationName() {
        return this.ddl.getLocationName();
    }
    /**
     * @param locationName The locationName to set.
     */
    public void setLocationName(String locationName) {
        ; // ignore
    }
    /**
     * @return Returns the sequenceNumber.
     */
    public String getSequenceNumber() {
        String seqNumberStr = String.valueOf(ddl.getSequenceNumber()); 
        return seqNumberStr;
    }
    /**
     * @param sequenceNumber The sequenceNumber to set.
     */
    public void setSequenceNumber(String sequenceNumber) {
        ; // ignore
    }
    /**
     * @return Returns the productOrders.
     */
    public ArrayList<ProductOrderHandler> getProductOrders() {
        return this.productOrders;
    }
    /**
     * @param productOrders The productOrders to set.
     */
    public void setProductOrders(ArrayList<ProductOrderHandler> productOrders) {
        ; // ignore
    }
    /**
     * @return Returns the phoneNumber.
     */
    public String getPhoneNumber() {
        return this.ddl.getPhoneNumber();
    }
    
    public String getLocationPhoneNumberFormatted() {
        String phone = this.getPhoneNumber();
        StringBuffer fPhone = new StringBuffer("");
        if (phone != null && phone.length() == 10) {
            fPhone.append("(");
	        fPhone.append(phone.substring(0,3));
	        fPhone.append(") ");
	        fPhone.append(phone.substring(3,6));
	        fPhone.append("-");
	        fPhone.append(phone.substring(6));
        }
        return fPhone.toString();
    }
    
    /**
     * @param phoneNumber The phoneNumber to set.
     */
    public void setPhoneNumber(String phoneNumber) {
        ; // ignore
    }
    
    /**
     * @return Returns the ddl.
     */
    public DailyDrawLocationIntf getDdl() {
        return this.ddl;
    }
    
    public void clear() {
        this.ddl = null;
        if (this.productOrders != null && !this.productOrders.isEmpty()) {
            Iterator<ProductOrderHandler> itr = this.productOrders.iterator();
            while (itr.hasNext()) {
                ProductOrderHandler poh = itr.next();
                poh.clear();
            }
            this.productOrders.clear();
        }
        this.productOrders = null;
    }
}

/*
 * Created on Mar 13, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers;


/**
 * @author aeast
 * @date Mar 13, 2008
 * @class CommentQuestionHandler
 * 
 * 
 * 
 */
public class CommentQuestionHandler {

    String name = null;
    String emailAddress = null;
    String initialPhone = null;
    String phoneAreaCode = null;
    String phonePrefix = null;
    String phoneLineNumber = null;
    String phoneExtension = null;
    boolean responseRequested = false;
    String locationID = null;
    String category = null;
    String comments = null;
        
    boolean emailSent = false;
    
    /**
     * 
     */
    public CommentQuestionHandler() {
        super();
    }

    public String getComments() {
        return this.comments;
    }
    public void setComments(String comments) {
        this.comments = comments;
    }
    public String getEmailAddress() {
        return this.emailAddress;
    }
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public boolean isEmailSent() {
        return this.emailSent;
    }
    public void setEmailSent(boolean emailSent) {
        this.emailSent = emailSent;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPhoneAreaCode() {
        return this.phoneAreaCode;
    }
    public void setPhoneAreaCode(String phoneAreaCode) {
        this.phoneAreaCode = phoneAreaCode;
    }
    public String getPhoneLineNumber() {
        return this.phoneLineNumber;
    }
    public void setPhoneLineNumber(String phoneLineNumber) {
        this.phoneLineNumber = phoneLineNumber;
    }
    public String getPhonePrefix() {
        return this.phonePrefix;
    }
    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }
    public boolean isResponseRequested() {
        return this.responseRequested;
    }
    public void setResponseRequested(boolean responseRequested) {
        this.responseRequested = responseRequested;
    }
    public String getInitialPhone() {
        return this.initialPhone;
    }
    public void setInitialPhone(String initialPhone) {
        if (initialPhone != null) {
            if (initialPhone.length() == 10) {
                try {
	                this.setPhoneAreaCode(initialPhone.substring(0,3));
	                this.setPhonePrefix(initialPhone.substring(3,6));
	                this.setPhoneLineNumber(initialPhone.substring(6));
                }
                catch (Exception e) {
                    // ignore
                }
            }
        }
        this.initialPhone = initialPhone;
    }
    public String getPhoneExtension() {
        return this.phoneExtension;
    }
    public void setPhoneExtension(String phoneExtension) {
        this.phoneExtension = phoneExtension;
    }
    public String getCategory() {
        return this.category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    /**
     * @return Returns the locationID.
     */
    public String getLocationID() {
        return locationID;
    }
    /**
     * @param locationID The locationID to set.
     */
    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }
}

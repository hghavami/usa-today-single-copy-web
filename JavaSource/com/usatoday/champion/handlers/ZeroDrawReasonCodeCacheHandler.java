/*
 * Created on Jun 8, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.model.SelectItem;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.ZeroDrawReasonCodeIntf;
import com.usatoday.singlecopy.model.util.ZeroDrawReasonCodeCache;

/**
 * @author aeast
 * @date Jun 8, 2007
 * @class ZeroDrawReasonCodeCacheHandler
 * 
 * POJO Cache Handler
 * 
 */
public class ZeroDrawReasonCodeCacheHandler {

    /**
     * 
     */
    public ZeroDrawReasonCodeCacheHandler() {
        super();
    }
    
    public ArrayList<SelectItem> getZeroDrawReasonCodesAsCollectionForDropDownSelection() {
        
        ArrayList<SelectItem> dropDownView = new ArrayList<SelectItem>();
        
	    if (ZeroDrawReasonCodeCache.getInstance().getZeroDrawReasonCodes().size() > 0) {
	        Iterator<ZeroDrawReasonCodeIntf> itr = ZeroDrawReasonCodeCache.getInstance().getZeroDrawReasonCodes().iterator();
	        while (itr.hasNext()) {
	            ZeroDrawReasonCodeIntf zdr = itr.next();
	            SelectItem si = new javax.faces.model.SelectItem(zdr, zdr.getDescription());
	            dropDownView.add(si);
	        }
	    }
        return dropDownView;
    }
    
    public Collection<ZeroDrawReasonCodeIntf> getZeroDrawReasonCodeCollection() {
        return ZeroDrawReasonCodeCache.getInstance().getZeroDrawReasonCodes();
    }

}

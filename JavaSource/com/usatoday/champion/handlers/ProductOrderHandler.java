/*
 * Created on Mar 5, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers;

import java.util.Date;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductDescriptionIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderTypeIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;
import com.usatoday.singlecopy.model.util.ProductDescriptionCache;
import com.usatoday.singlecopy.model.util.ProductOrderTypesCache;
import com.usatoday.singlecopy.model.util.ZeroDrawReasonCodeCache;
 
/**
 * @author aeast
 * @date Mar 5, 2007
 * @class ProductOrderHandler
 * 
 * This class handles product orders for a daily RDL
 * 
 */
public class ProductOrderHandler {

    private DailyProductOrderIntf po = null;

    private static String drawReturnCSS = "inputText dmEditField";
    private static String drawReturnErrCSS = "inputText_Error inputText";
    private static String disabledCSS = " inputText_disabled";
    private static String readOnly = "DISABLED";
    private static String emptyString = "";
    private static String inputTitleText = "Not Specified (blank)";
    private static String zdrIconCodeSet = "/scweb/images/zdr_ind.gif";
    private static String zdrIconNoCodeSet = "/scweb/images/zdr_ind_notset.gif";
    
    
    private boolean errorsProcessed = true;
    private String errorMessage = "";
    private String drawCSS = ProductOrderHandler.drawReturnCSS;
    private String returnCSS = ProductOrderHandler.drawReturnCSS;
    
  
    /**
     * 
     */
    public ProductOrderHandler() {
        super();
    }

    public ProductOrderHandler(DailyProductOrderIntf source) {
        super();
        
        this.po = source;
        
    }
    
    /**
     * @return Returns the drawValue.
     */
    public int getDrawValue() {
        int draw = -1;
        if (this.po != null) {
            draw = this.po.getDraw();
        }
        return draw;
    }

    public String getDrawDisplayValue() {
        String draw = ""; 
        if (this.po != null && this.po.getDraw() >= 0) {
            draw = String.valueOf(this.po.getDraw());
        }
        return draw;
    }

    /**
     * 
     * @return The display version of the champion draw value
     */
    public String getOriginalDrawDisplayValue() {
        String draw = inputTitleText; 
        if (this.po != null && this.po.getOriginalProductOrder().getDraw() >= 0) {
            draw = String.valueOf(this.po.getOriginalProductOrder().getDraw());
        }
        return draw;
    }
    
    public Integer getDrawValueObj() {
        // -1 indicates the value has not been set so a null is 
        // returned to show the field as blank
        // return the draw as an Integer object or null if not set (-1)
        Integer dValue = null;
        if (this.po != null && this.po.getDraw() >= 0) {
            dValue = new Integer(this.po.getDraw());
        }
        return dValue;
    }
    /**
     * @param drawValue The drawValue to set.
     */
    public void setDrawValue(int drawValue) {
        if (this.po != null) {
            try {
               this.po.setDraw(drawValue);
            }
            catch (UsatException ue) {
                System.out.println("Failed to set draw: " + ue.getMessage());
            }
        }
    }
    
    public void setDrawValueObj(Integer value) {
        if (this.po != null && value != null) {
            int iValue = value.intValue();
            this.setDrawValue(iValue);
        }
    }
    /**
     * @return Returns the maxDrawValue.
     */
    public int getMaxDrawValue() {
        return this.po.getMaxDrawThreshold();
    }
    
    public int getMaxReturnValue() {
        return this.po.getMaxReturns();
    }
    /**
     * @param maxDrawValue The maxDrawValue to set.
     */
    public void setMaxDrawValue(int maxDrawValue) {
        ; // ignore
    }
    
    public int getDrawDayReturns() {
        return this.po.getDrawDayReturns();
    }
    
    /**
     * 
     * @return
     */
    public String getProductDescription() {
        String description = "No publication description available.";
        
        ProductDescriptionCache pdCache = ProductDescriptionCache.getInstance();
        
        ProductDescriptionIntf pd = pdCache.getProductDesription(this.po.getProductCode());
        
        description = pd.getDescription();
        
        return description;
    }

    /**
     * @return Returns the productID.
     */
    public String getProductID() {
        return this.po.getProductCode();
    }
    /**
     * @param productID The productID to set.
     */
    public void setProductID(String productID) {
        ; // ignore
    }
    /**
     * @return Returns the productOrderID.
     */
    public String getProductOrderID() {
        return this.po.getProductOrderID();
    }
    /**
     * @param productOrderID The productOrderID to set.
     */
    public void setProductOrderID(String productOrderID) {
        ; // ignore
    }
    /**
     * @return Returns the returnValue.
     */
    public int getReturnValue() {
        return this.po.getReturns();        
    }
    
    /**
     * 
     * @return Display (string) value of returns
     */
    public String getReturnsDisplayValue() {
        String returns = "";
        if (this.po != null && this.po.getReturns() >= 0) {
            returns = String.valueOf(this.po.getReturns());
        }
        return returns;        
    }

    public String getOriginalReturnsDisplayValue() {
        String returns = inputTitleText;
        if (this.po != null && this.po.getOriginalProductOrder().getReturns() >= 0) {
            returns = String.valueOf(this.po.getOriginalProductOrder().getReturns());
        }
        return returns;        
    }
    

    public Integer getReturnValueObj() {
        Integer rValue = null;
        if (this.po.getReturns() >= 0) {
            rValue = new Integer(this.po.getReturns());
        }
        return rValue;
    }
    
    public void setReturnValueObj(Integer value) {
        if (this.po != null && value != null) {
            int iValue = value.intValue();
            this.setReturnValue(iValue);
        }
    }
    /**
     * @param returnValue The returnValue to set.
     */
    public void setReturnValue(int returnValue) {
        if (this.po != null) {
            if (returnValue >= -1) {
                try {
                    this.po.setReturns(returnValue);
                }
                catch (UsatException ue) {
                    System.out.println("Failed to set Return: " + ue.getMessage());
                }
            }
        }
    }
    /**
     * @return Returns the sequenceNumber.
     */
    public int getSequenceNumber() {
        int seqNum = 1;
        if (this.po != null) {
            seqNum = this.po.getSequenceNumber();
        }
        return seqNum;
    }
    /**
     * @param sequenceNumber The sequenceNumber to set.
     */
    public void setSequenceNumber(String sequenceNumber) {
        ; // ignore
    }
    /**
     * @return Returns the zeroDrawReasonCode.
     */
    public String getZeroDrawReasonCode() {
        String zdr = "";
        if (this.po != null) {
            zdr = this.po.getZeroDrawReasonCode();
        }
        
        return zdr;
    }

    public String getOriginalZeroDrawReasonCode() {
        String zdr = "";
        if (this.po != null) {
            zdr = this.po.getOriginalProductOrder().getZeroDrawReasonCode();
        }
        
        return zdr;
    }
    
    /**
     * 
     * @return
     */
    public String getZDRImageTitle() {
        String zdrTitle = "";
        if (this.po != null && this.po.getAllowDrawEdits()) {
            zdrTitle = "Zero Draw Reason. Click to Change/View";
        }
        else {
            // no edits
            zdrTitle = "Zero Draw Reason: " + this.getZeroDrawReasonDescription();
        }
        return zdrTitle;
    }
    
    /**
     * @return Returns the fridayZeroDrawReasonDescription.
     */
    public String getZeroDrawReasonDescription() {
        String zdr = "";
        if (this.po != null) {
            if (ZeroDrawReasonCodeCache.getInstance().containsCode(this.po.getZeroDrawReasonCode())) {
                zdr = ZeroDrawReasonCodeCache.getInstance().getDescription(this.po.getZeroDrawReasonCode());
            }
        }
        return zdr;
    }
    
    
    /**
     * @param zeroDrawReasonCode The zeroDrawReasonCode to set.
     */
    public void setZeroDrawReasonCode(String zeroDrawReasonCode) {
        if (this.po != null && zeroDrawReasonCode != null) {
            try {
                this.po.setZeroDrawReasonCode(zeroDrawReasonCode);
            }
            catch (UsatException ue) {
                System.out.println("Failed to set zer draw reason code: " + ue.getMessage());
            }            
        }
                
    }
    
    /**
     * @return Returns the drawFieldOpen.
     */
    public Boolean getDrawFieldOpen() {
        boolean drawOpen = false;
        if (this.po != null) {
            drawOpen = this.po.getAllowDrawEdits();
        }
        return new Boolean(drawOpen);
    }
    
    public String getDrawReadOnly() {
        String rOnly = ProductOrderHandler.readOnly;
        if (this.po != null) {
            if (this.po.getAllowDrawEdits()) {
                rOnly = ProductOrderHandler.emptyString;
            }
        }
        return rOnly;
    }
    /**
     * @param drawFieldOpen The drawFieldOpen to set.
     */
    public void setDrawFieldOpen(Boolean drawFieldOpen) {
        ; // ignore
    }
    /**
     * @return Returns the returnFieldOpen.
     */
    public Boolean getReturnFieldOpen() {
        boolean returnOpen = false;
        if (this.po != null) {
            returnOpen = !this.po.getAllowReturnsEdits();
        }
        return new Boolean(returnOpen);
    }
    
    public String getReturnReadOnly() {
        String rOnly = ProductOrderHandler.readOnly;
        if (this.po != null) {
            if (this.po.getAllowReturnsEdits()) {
                rOnly = ProductOrderHandler.emptyString;
            }
        }
        return rOnly;
    }
    
    /**
     * @param returnFieldOpen The returnFieldOpen to set.
     */
    public void setReturnFieldOpen(Boolean returnFieldOpen) {
        ; // ignore
    }
    /**
     * @return Returns the date.
     */
    public Date getDate() {
        return this.po.getRDLDate().toDate();
    }
    /**
     * @param date The date to set.
     */
    public void setDate(Date date) {
        ; //ignore
    }
    /**
     * @return Returns the dayType.
     */
    public String getDayType() {
        return this.po.getDayType();
    }
    /**
     * @param dayType The dayType to set.
     */
    public void setDayType(String dayType) {
        ; //ignore
    }
    /**
     * @return Returns the productOrderType.
     */
    public String getProductOrderType() {
        return this.po.getProductOrderTypeCode();
    }
    /**
     * @param productOrderType The productOrderType to set.
     */
    public void setProductOrderType(String productOrderType) {
        ; // ignore
    }
    
    public String getProductOrderTypeDescription() {
        
        ProductOrderTypesCache ptCache = ProductOrderTypesCache.getProductOrderTypesCache();
        ProductOrderTypeIntf pot = ptCache.getProductOrderType(this.po.getProductCode(), this.po.getProductOrderTypeCode());
        String description = "Unknown";
        if (pot != null) {
            description = pot.getDescription();
        }
        return description;
    }
    
    /**
     * @return Returns the returnDate.
     * 
     *  for comopatibility with the JSF page this method was
     *  added for the non JSF d4d.jsp page
     */
    public DateTime getReturnDate2() {
        return this.po.getReturnDate();
    }
    
    public String getReturnDate2Str() {
        String rDate = "N/A";
        if (this.po.getReturnDate() != null) {
            rDate = this.po.getReturnDate().toString("MM/dd/yy");
        }
        return rDate;
    }

    public Date getReturnDate() {
        if (this.po.getReturnDate() != null){ 
            return this.po.getReturnDate().toDate();
    	}
        return null;
    }
    
    /**
     * @param returnDate The returnDate to set.
     */
    public void setReturnDate(Date returnDate) {
        ; // ignore
    }
    
    /**
     * 
     *
     */
    public void resetErrorsProcessed() {
        this.errorsProcessed = false;
        this.drawCSS = ProductOrderHandler.drawReturnCSS;
        this.returnCSS = ProductOrderHandler.drawReturnCSS;
        this.errorMessage = "";
        this.po.clearUpdateErrors();
    }
    
    public String getErrorMessage() {
        if (!this.errorsProcessed) {
            this.processErrors();
        }
        return this.errorMessage;
    }

    public String getDrawCSS() {
        if (!this.errorsProcessed) {
            this.processErrors();
        }

        if (!this.po.getAllowDrawEdits()) {
            this.drawCSS += ProductOrderHandler.disabledCSS;
        }
        else {
            this.drawCSS += " drawInputText";
        }
       
       return this.drawCSS;
    }

    public String getReturnCSS() {
        
        if (!this.errorsProcessed) {
            this.processErrors();
        }
        
        if (!this.po.getAllowReturnsEdits()) {
            this.returnCSS += ProductOrderHandler.disabledCSS;
        }
        else {
            this.returnCSS += " returnInputText";
        }
        
        return this.returnCSS;
    }
    
    

    
    private void processErrors() {
        if (this.po.getUpdateErrors() != null) {
            DrawUpdateErrorIntf errors[] = (DrawUpdateErrorIntf[])this.po.getUpdateErrors();
            DrawUpdateErrorIntf error = null;
            
            for (int i = 0; i < errors.length; i++) {
                error = errors[i];

                this.errorMessage += error.getErrorMessage() + " ";
                if (DrawUpdateErrorIntf.DRAW_ERROR.equalsIgnoreCase(error.getAppliesTo())) {
                    this.drawCSS = ProductOrderHandler.drawReturnErrCSS;
                }
                else if (DrawUpdateErrorIntf.RETURN_ERROR.equalsIgnoreCase(error.getAppliesTo())) {
                    this.returnCSS = ProductOrderHandler.drawReturnErrCSS;
                }
                else {
                    this.drawCSS = ProductOrderHandler.drawReturnErrCSS;
                    this.returnCSS = ProductOrderHandler.drawReturnErrCSS;
                }
            }
        }
        this.errorsProcessed = true;
    }
    
    public boolean getLateReturnsFlag() {
        return this.po.getLateReturnsFlag();
    }
    
    public String getLateReturnsCSSString() {
        if (this.po.getLateReturnsFlag() && this.po.getAllowReturnsEdits()) {
            return "";
        }
        else {
            return "visibility: hidden";
        }
    }

    public String getZDRCSSString() {
        
        if ((this.po.getZeroDrawReasonCode() == null || (this.po.getZeroDrawReasonCode().trim().length() == 0)) && this.po.getAllowDrawEdits() == false) {
            return "display: none";
        }
            
        if ((this.po.getDraw() == 0) || 
            (this.po.getZeroDrawReasonCode() != null && this.po.getZeroDrawReasonCode().length() > 0)) {
            return "";
        }
        else {
            return "display: none";
        }
    }
    
    /**
     * 
     * @return
     */
    public String getZDRImageIcon() {
        String icon = ProductOrderHandler.zdrIconCodeSet;
        if (this.po.getDraw()==0 && this.po.getOriginalProductOrder().getDraw() == 0) {
            if (this.po.getZeroDrawReasonCode() == null || this.po.getZeroDrawReasonCode().trim().length() == 0) {
                icon = ProductOrderHandler.zdrIconNoCodeSet;
            }
        }
        return icon;
    }
    
    public void clear() {
        this.po = null;
    }
}

/*
 * Created on Mar 9, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductDescriptionIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderTypeIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyDrawUpdateErrorIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf;
import com.usatoday.singlecopy.model.util.ProductDescriptionCache;
import com.usatoday.singlecopy.model.util.ProductOrderTypesCache;
import com.usatoday.singlecopy.model.util.ZeroDrawReasonCodeCache;

/**
 * @author aeast
 * @date Mar 9, 2007
 * @class WeeklyProductOrderHandler
 * 
 * This class is a proxy class to the underlying Business object.
 * It's purpose is to interface with the JSF UI components.
 * 
 */
public class WeeklyProductOrderHandler {
    
    private WeeklyProductOrderIntf po = null;

    private static String aDayCSS = "inputText dmEditField";
    private static String bDayCSS = "inputTextAlt dmEditField";
    private static String aDayErrCSS = "inputText_Error inputText";
    private static String bDayErrCSS = "inputText_Error inputTextAlt";
    private static String readOnly = "DISABLED";
    private static String emptyString = "";
    private static String disabledCSS = " inputText_disabled";
    
    private static String inputTitleText = "Not Specified (blank)";
    private static String zdrIconCodeSet = "/scweb/images/zdr_ind.gif";
    private static String zdrIconNoCodeSet = "/scweb/images/zdr_ind_notset.gif";
    
    private boolean mondayErrorsProcessed = true;
    private String mondayErrorMessage = "";
    private String mondayDrawCSS = WeeklyProductOrderHandler.aDayCSS;
    private String mondayReturnCSS = WeeklyProductOrderHandler.aDayCSS;
    
    private boolean tuesdayErrorsProcessed = true;
    private String tuesdayErrorMessage = "";
    private String tuesdayDrawCSS = WeeklyProductOrderHandler.bDayCSS;
    private String tuesdayReturnCSS = WeeklyProductOrderHandler.bDayCSS;

    private boolean wednesdayErrorsProcessed = true;
    private String wednesdayErrorMessage = "";
    private String wednesdayDrawCSS = WeeklyProductOrderHandler.aDayCSS;
    private String wednesdayReturnCSS = WeeklyProductOrderHandler.aDayCSS;

    private boolean thursdayErrorsProcessed = true;
    private String thursdayErrorMessage = "";
    private String thursdayDrawCSS = WeeklyProductOrderHandler.bDayCSS;
    private String thursdayReturnCSS = WeeklyProductOrderHandler.bDayCSS;

    private boolean fridayErrorsProcessed = true;
    private String fridayErrorMessage = "";
    private String fridayDrawCSS = WeeklyProductOrderHandler.aDayCSS;
    private String fridayReturnCSS = WeeklyProductOrderHandler.aDayCSS;

    private boolean saturdayErrorsProcessed = true;
    private String saturdayErrorMessage = "";
    private String saturdayDrawCSS = WeeklyProductOrderHandler.bDayCSS;
    private String saturdayReturnCSS = WeeklyProductOrderHandler.bDayCSS;

    private boolean sundayErrorsProcessed = true;
    private String sundayErrorMessage = "";
    private String sundayDrawCSS = WeeklyProductOrderHandler.aDayCSS;
    private String sundayReturnCSS = WeeklyProductOrderHandler.aDayCSS;

    /**
     * 
     */
    public WeeklyProductOrderHandler() {
        super();
    }
    
    public WeeklyProductOrderHandler(WeeklyProductOrderIntf source) {
        this.po = source;
    }

    /**
     * 
     * @return A description of the product order type
     */
    public String getProductOrderTypeDescription() {

        String description = "Unknown";
        
        if (this.po == null) {
            return description;
        }
        
        ProductOrderTypesCache ptCache = ProductOrderTypesCache.getProductOrderTypesCache();
        ProductOrderTypeIntf pot = ptCache.getProductOrderType(this.po.getProductCode(), this.po.getProductOrderTypeCode());
        if (pot != null) {
            description = pot.getDescription();
        }
        return description;
    }
    
    /**
     * 
     * @return
     */
    public String getProductDescription() {
        String description = "No publication description available.";
        if (this.po == null) {
            return description;
        }
        
        ProductDescriptionCache pdCache = ProductDescriptionCache.getInstance();
        
        ProductDescriptionIntf pd = pdCache.getProductDesription(this.po.getProductCode());
        
        description = pd.getDescription();
        
        return description;
    }
    /**
     * @return Returns the fridayDayType.
     */
    public String getFridayDayType() {
        String dayType = "";
        if (this.po != null) {
            dayType = this.po.getFridayDayType();
        }
        return dayType;
    }
    /**
     * @param fridayDayType The fridayDayType to set.
     */
    public void setFridayDayType(String fridayDayType) {
        // ignore, this is read only
    }
    /**
     * @return Returns the fridayDrawFieldOpen.
     */
    public Boolean getFridayDrawFieldOpen() {
        boolean drawOpen = false;
        if (this.po != null) {
            drawOpen = this.po.getAllowFridayDrawEdits();
        }
        return new Boolean(drawOpen);
    }
    
    /**
     * 
     * @return The string "readonly" or an empty string
     */
    public String getFridayDrawReadonly() {
        String rOnly = WeeklyProductOrderHandler.readOnly;
        if (this.po != null) {
            if (this.po.getAllowFridayDrawEdits()) {
                rOnly = WeeklyProductOrderHandler.emptyString;
            }
        }
        return rOnly;
    }    
    /**
     * @param fridayDrawFieldOpen The fridayDrawFieldOpen to set.
     */
    public void setFridayDrawFieldOpen(Boolean fridayDrawFieldOpen) {
        // ignore no setting of this field
    }
    /**
     * @return Returns the fridayDrawValue.
     */
    public int getFridayDrawValue() {
        int draw = -1;
        if (this.po != null) {
            draw = this.po.getFridayDraw();
        }
        return draw;
    }

    /**
     * 
     * @return String representations of draw value
     */
    public String getFridayDrawDisplayValue() {
        String draw = "";
        if (this.po != null && this.po.getFridayDraw() >= 0) {
            draw = String.valueOf(this.po.getFridayDraw());
        }
        return draw;
    }
    
    // value saved in Champion
    public String getOriginalFridayDrawDisplayValue() {
        String draw = inputTitleText;
        if (this.po != null && this.po.getOriginalProductOrder().getFridayDraw() >= 0) {
            draw = String.valueOf(this.po.getOriginalProductOrder().getFridayDraw());
        }
        return draw;
    }

    /**
     * 
     * @return
     */
    public Integer getFridayDrawValueObj() {
        // return the draw as an Integer object or null if not set (-1)
        Integer dValue = null;
        if (this.po != null && this.po.getFridayDraw() >= 0) {
            dValue = new Integer(this.po.getFridayDraw());
        }
        return dValue;
    }

    /**
     * @return Returns the maxDrawValue for Friday.
     */
    public int getFridayMaxDrawValue() {
        int maxDraw = 100;
        if (this.po != null) {
            maxDraw = this.po.getFridayMaxDrawThreshold();
        }
        return maxDraw;
    }

    /**
     * @param fridayDrawValue The fridayDrawValue to set.
     */
    public void setFridayDrawValue(int fridayDrawValue) {
        if (this.po != null) {
            if (fridayDrawValue <= this.po.getMaxDrawThreshold() && fridayDrawValue >= 0) {
                try {
                    this.po.setFridayDraw(fridayDrawValue);
                }
                catch (UsatException ue) {
                    System.out.println("Failed to set draw: " + ue.getMessage());
                }
            }
        }
    }
    
    /**
     * 
     * @param value
     */
    public void setFridayDrawValueObj(Integer value) {
        if (this.po != null && value != null) {
            int iValue = value.intValue();
            this.setFridayDrawValue(iValue);
        }
    }
    /**
     * @return Returns the fridayReturnFieldOpen.
     */
    public Boolean getFridayReturnFieldOpen() {
        boolean returnOpen = false;
        if (this.po != null) {
            returnOpen = !this.po.getAllowFridayReturnsEdits();
        }
        return new Boolean(returnOpen);
    }
    
    /**
     * 
     * @return The string "readonly" or an empty string
     */
    public String getFridayReturnReadonly() {
        String rOnly = WeeklyProductOrderHandler.readOnly;
        if (this.po != null) {
            if (this.po.getAllowFridayReturnsEdits()) {
                rOnly = WeeklyProductOrderHandler.emptyString;
            }
        }
        return rOnly;
    }
    
    /**
     * @param fridayReturnFieldOpen The fridayReturnFieldOpen to set.
     */
    public void setFridayReturnFieldOpen(Boolean fridayReturnFieldOpen) {
        // ignore, read only field
    }
    /**
     * @return Returns the fridayReturnValue.
     */
    public int getFridayReturnValue() {
        int returnVal = -1;
        if (this.po != null) {
            returnVal = this.po.getFridayReturns();
        }
        return returnVal;
    }

    /**
     * 
     * @return String representations of return value
     */
    public String getFridayReturnDisplayValue() {
        String returns = "";
        if (this.po != null && this.po.getFridayReturns() >= 0) {
            returns = String.valueOf(this.po.getFridayReturns());
        }
        return returns;
    }

    // Champion value
    public String getOriginalFridayReturnDisplayValue() {
        String returns = inputTitleText;
        if (this.po != null && this.po.getOriginalProductOrder().getFridayReturns() >= 0) {
            returns = String.valueOf(this.po.getOriginalProductOrder().getFridayReturns());
        }
        return returns;
    }
    
    /**
     * 
     * @return
     */
    public Integer getFridayReturnValueObj() {
        Integer rValue = null;
        if (this.po != null && this.po.getFridayReturns() >= 0) {
            rValue = new Integer(this.po.getFridayReturns());
        }
        return rValue;
    }
    /**
     * @param fridayReturnValue The fridayReturnValue to set.
     */
    public void setFridayReturnValue(int fridayReturnValue) {
        if (this.po != null) {
            if (fridayReturnValue >= -1) {
                try {
                    this.po.setFridayReturns(fridayReturnValue);
                }
                catch (UsatException ue) {
                    System.out.println("Failed to set Return: " + ue.getMessage());
                }
            }
        }
    }

    /**
     * 
     * @param value
     */
    public void setFridayReturnValueObj(Integer value) {
        if (this.po != null && value != null) {
            int iValue = value.intValue();
            this.setFridayReturnValue(iValue);
        }
    }
    
    /**
     * @return Returns the maxDrawValue for Monday.
     */
    public int getMondayMaxDrawValue() {
        int maxDraw = 100;
        if (this.po != null) {
            maxDraw = this.po.getMondayMaxDrawThreshold();
        }
        return maxDraw;
    }
    
    /**
     * @return Returns the mondayDayType.
     */
    public String getMondayDayType() {
        String dayType = "";
        if (this.po != null) {
            dayType = this.po.getMondayDayType();
        }
        return dayType;
    }
    /**
     * @param mondayDayType The mondayDayType to set.
     */
    public void setMondayDayType(String mondayDayType) {
        //ignore, read only field
    }
    /**
     * @return Returns the mondayDrawFieldOpen.
     */
    public Boolean getMondayDrawFieldOpen() {
        boolean drawOpen = false;
        if (this.po != null) {
            drawOpen = this.po.getAllowMondayDrawEdits();
        }
        return new Boolean(drawOpen);
    }
    
    /**
     * 
     * @return The string "readonly" or an empty string
     */
    public String getMondayDrawReadonly() {
        String rOnly = WeeklyProductOrderHandler.readOnly;
        if (this.po != null) {
            if (this.po.getAllowMondayDrawEdits()) {
                rOnly = WeeklyProductOrderHandler.emptyString;
            }
        }
        return rOnly;
    }
    /**
     * @param mondayDrawFieldOpen The mondayDrawFieldOpen to set.
     */
    public void setMondayDrawFieldOpen(Boolean mondayDrawFieldOpen) {
        //Ignore, read only field
    }
    /**
     * @return Returns the mondayDrawValue.
     */
    public int getMondayDrawValue() {
        int draw = -1;
        if (this.po != null) {
            draw = this.po.getMondayDraw();
        }
        return draw;
    }
    
    /**
     * 
     * @return String representations of draw value
     */
    public String getMondayDrawDisplayValue() {
        String draw = "";
        if (this.po != null && this.po.getMondayDraw() >= 0) {
            draw = String.valueOf(this.po.getMondayDraw());
        }
        return draw;
    }

    // value saved in Champion
    public String getOriginalMondayDrawDisplayValue() {
        String draw = inputTitleText;
        if (this.po != null && this.po.getOriginalProductOrder().getMondayDraw() >= 0) {
            draw = String.valueOf(this.po.getOriginalProductOrder().getMondayDraw());
        }
        return draw;
    }

    /**
     * 
     * @return
     */
    public Integer getMondayDrawValueObj() {
        // return the draw as an Integer object or null if not set (-1)
        Integer dValue = null;
        if (this.po != null && this.po.getMondayDraw() >= 0) {
            dValue = new Integer(this.po.getMondayDraw());
        }
        return dValue;
    }
    
    /**
     * @param mondayDrawValue The mondayDrawValue to set.
     */
    public void setMondayDrawValue(int mondayDrawValue) {
        if (this.po != null) {
            if (mondayDrawValue <= this.po.getMaxDrawThreshold() && mondayDrawValue >= 0) {
                try {
                    this.po.setMondayDraw(mondayDrawValue);
                }
                catch (UsatException ue) {
                    System.out.println("Failed to set draw: " + ue.getMessage());
                }
            }
        }
    }
    
    /**
     * 
     * @param value
     */
    public void setMondayDrawValueObj(Integer value) {
        if (this.po != null && value != null) {
            int iValue = value.intValue();
            this.setMondayDrawValue(iValue);
        }
    }
    
    /**
     * @return Returns the mondayReturnFieldOpen.
     */
    public Boolean getMondayReturnFieldOpen() {
        boolean returnOpen = false;
        if (this.po != null) {
            returnOpen = !this.po.getAllowMondayReturnsEdits();
        }
        return new Boolean(returnOpen);
    }
    
    /**
     * 
     * @return The string "readonly" or an empty string
     */
    public String getMondayReturnReadonly() {
        String rOnly = WeeklyProductOrderHandler.readOnly;
        if (this.po != null) {
            if (this.po.getAllowMondayReturnsEdits()) {
                rOnly = WeeklyProductOrderHandler.emptyString;
            }
        }
        return rOnly;
    }
    
    /**
     * @param mondayReturnFieldOpen The mondayReturnFieldOpen to set.
     */
    public void setMondayReturnFieldOpen(Boolean mondayReturnFieldOpen) {
        //ignore, read only
    }
    /**
     * @return Returns the mondayReturnValue.
     */
    public int getMondayReturnValue() {
        int returnVal = -1;
        if (this.po != null) {
            returnVal = this.po.getMondayReturns();
        }
        return returnVal;
    }

    /**
     * 
     * @return String representations of return value
     */
    public String getMondayReturnDisplayValue() {
        String returns = "";
        if (this.po != null && this.po.getMondayReturns() >= 0) {
            returns = String.valueOf(this.po.getMondayReturns());
        }
        return returns;
    }
    
    // Champion value
    public String getOriginalMondayReturnDisplayValue() {
        String returns = inputTitleText;
        if (this.po != null && this.po.getOriginalProductOrder().getMondayReturns() >= 0) {
            returns = String.valueOf(this.po.getOriginalProductOrder().getMondayReturns());
        }
        return returns;
    }

    /**
     * 
     * @return
     */
    public Integer getMondayReturnValueObj() {
        Integer rValue = null;
        if (this.po != null && this.po.getMondayReturns() >= 0) {
            rValue = new Integer(this.po.getMondayReturns());
        }
        return rValue;
    }
    
    /**
     * @param mondayReturnValue The mondayReturnValue to set.
     */
    public void setMondayReturnValue(int mondayReturnValue) {
        if (this.po != null) {
            if (mondayReturnValue >= -1) {
                try {
                    this.po.setMondayReturns(mondayReturnValue);
                }
                catch (UsatException ue) {
                    System.out.println("Failed to set draw: " + ue.getMessage());
                }
            }
        }
    }

    /**
     * 
     * @param value
     */
    public void setMondayReturnValueObj(Integer value) {
        if (this.po != null && value != null) {
            int iValue = value.intValue();
            this.setMondayReturnValue(iValue);
        }
    }
    
    /**
     * @return Returns the productID.
     */
    public String getProductID() {
        String prodID = "";
        if (this.po != null) {
            prodID = po.getProductCode();
        }
        return prodID;
    }
    /**
     * @param productID The productID to set.
     */
    public void setProductID(String productID) {
        //ignore, read only field
    }
    /**
     * @return Returns the productOrderID.
     */
    public String getProductOrderID() {
        String prodOrderID = "";
        if (this.po != null) {
            prodOrderID = po.getProductOrderID();
        }
        return prodOrderID;
    }
    /**
     * @param productOrderID The productOrderID to set.
     */
    public void setProductOrderID(String productOrderID) {
       // ignore, read only
    }
    /**
     * @return Returns the saturdayDayType.
     */
    public String getSaturdayDayType() {
        String dayType = "";
        if (this.po != null) {
            dayType = this.po.getSaturdayDayType();
        }
        return dayType;
    }
    /**
     * @param saturdayDayType The saturdayDayType to set.
     */
    public void setSaturdayDayType(String saturdayDayType) {
        // ignore, read only
    }
    /**
     * @return Returns the saturdayDrawFieldOpen.
     */
    public Boolean getSaturdayDrawFieldOpen() {
        boolean drawOpen = false;
        if (this.po != null) {
            drawOpen = this.po.getAllowSaturdayDrawEdits();
        }
        return new Boolean(drawOpen);
    }
    
    /**
     * 
     * @return The string "readonly" or an empty string
     */
    public String getSaturdayDrawReadonly() {
        String rOnly = WeeklyProductOrderHandler.readOnly;
        if (this.po != null) {
            if (this.po.getAllowSaturdayDrawEdits()) {
                rOnly = WeeklyProductOrderHandler.emptyString;
            }
        }
        return rOnly;
    }    

    /**
     * @return Returns the maxDrawValue for Saturday.
     */
    public int getSaturdayMaxDrawValue() {
        int maxDraw = 100;
        if (this.po != null) {
            maxDraw = this.po.getSaturdayMaxDrawThreshold();
        }
        return maxDraw;
    }
    
    /**
     * @param saturdayDrawFieldOpen The saturdayDrawFieldOpen to set.
     */
    public void setSaturdayDrawFieldOpen(Boolean saturdayDrawFieldOpen) {
        // ignore, read only 
    }
    /**
     * @return Returns the saturdayDrawValue.
     */
    public int getSaturdayDrawValue() {
        int draw = -1;
        if (this.po != null) {
            draw = this.po.getSaturdayDraw();
        }
        return draw;
    }

    /**
     * 
     * @return String representations of draw value
     */
    public String getSaturdayDrawDisplayValue() {
        String draw = "";
        if (this.po != null && this.po.getSaturdayDraw() >= 0) {
            draw = String.valueOf(this.po.getSaturdayDraw());
        }
        return draw;
    }    

    // value saved in Champion
    public String getOriginalSaturdayDrawDisplayValue() {
        String draw = inputTitleText;
        if (this.po != null && this.po.getOriginalProductOrder().getSaturdayDraw() >= 0) {
            draw = String.valueOf(this.po.getOriginalProductOrder().getSaturdayDraw());
        }
        return draw;
    }

    /**
     * 
     * @return
     */
    public Integer getSaturdayDrawValueObj() {
        // return the draw as an Integer object or null if not set (-1)
        Integer dValue = null;
        if (this.po != null && this.po.getSaturdayDraw() >= 0) {
            dValue = new Integer(this.po.getSaturdayDraw());
        }
        return dValue;
    }
    
    /**
     * @param saturdayDrawValue The saturdayDrawValue to set.
     */
    public void setSaturdayDrawValue(int saturdayDrawValue) {
        if (this.po != null) {
            if (saturdayDrawValue <= this.po.getMaxDrawThreshold() && saturdayDrawValue >= 0) {
                try {
                    this.po.setSaturdayDraw(saturdayDrawValue);
                }
                catch (UsatException ue) {
                    System.out.println("Failed to set draw: " + ue.getMessage());
                }

            }
        }
    }
    
    /**
     * 
     * @param value
     */
    public void setSaturdayDrawValueObj(Integer value) {
        if (this.po != null && value != null) {
            int iValue = value.intValue();
            this.setSaturdayDrawValue(iValue);
        }
    }
    
    /**
     * @return Returns the saturdayReturnFieldOpen.
     */
    public Boolean getSaturdayReturnFieldOpen() {
        boolean returnOpen = false;
        if (this.po != null) {
            returnOpen = !this.po.getAllowSaturdayReturnsEdits();
        }
        return new Boolean(returnOpen);
    }
    
    /**
     * 
     * @return The string "readonly" or an empty string
     */
    public String getSaturdayReturnReadonly() {
        String rOnly = WeeklyProductOrderHandler.readOnly;
        if (this.po != null) {
            if (this.po.getAllowSaturdayReturnsEdits()) {
                rOnly = WeeklyProductOrderHandler.emptyString;
            }
        }
        return rOnly;
    }
    
    /**
     * @param saturdayReturnFieldOpen The saturdayReturnFieldOpen to set.
     */
    public void setSaturdayReturnFieldOpen(Boolean saturdayReturnFieldOpen) {
        //ignore, read only
    }
    /**
     * @return Returns the saturdayReturnValue.
     */
    public int getSaturdayReturnValue() {
        int returnVal = -1;
        if (this.po != null) {
            returnVal = this.po.getSaturdayReturns();
        }
        return returnVal;
    }

    /**
     * 
     * @return String representations of return value
     */
    public String getSaturdayReturnDisplayValue() {
        String returns = "";
        if (this.po != null && this.po.getSaturdayReturns() >= 0) {
            returns = String.valueOf(this.po.getSaturdayReturns());
        }
        return returns;
    }
    
    // Champion value
    public String getOriginalSaturdayReturnDisplayValue() {
        String returns = inputTitleText;
        if (this.po != null && this.po.getOriginalProductOrder().getSaturdayReturns() >= 0) {
            returns = String.valueOf(this.po.getOriginalProductOrder().getSaturdayReturns());
        }
        return returns;
    }

    /**
     * 
     * @return
     */
    public Integer getSaturdayReturnValueObj() {
        Integer rValue = null;
        if (this.po != null && this.po.getSaturdayReturns() >= 0) {
            rValue = new Integer(this.po.getSaturdayReturns());
        }
        return rValue;
    }
    
    /**
     * @param saturdayReturnValue The saturdayReturnValue to set.
     */
    public void setSaturdayReturnValue(int saturdayReturnValue) {
        if (this.po != null) {
            if (saturdayReturnValue >= -1) {
                try {
                    this.po.setSaturdayReturns(saturdayReturnValue);
                }
                catch (UsatException ue) {
                    System.out.println("Failed to set draw: " + ue.getMessage());
                }
            }
        }
    }
    /**
     * 
     * @param value
     */
    public void setSaturdayReturnValueObj(Integer value) {
        if (this.po != null && value != null) {
            int iValue = value.intValue();
            this.setSaturdayReturnValue(iValue);
        }
    }

    /**
     * @return Returns the sequenceNumber.
     */
    public int getSequenceNumber() {
        int seqNum = 1;
        if (this.po != null) {
            seqNum = this.po.getSequenceNumber();
        }
        return seqNum;
    }
    /**
     * @param sequenceNumber The sequenceNumber to set.
     */
    public void setSequenceNumber(int sequenceNumber) {
        //ignore, read only
    }
    /**
     * @return Returns the sundayDayType.
     */
    public String getSundayDayType() {
        String dayType = "";
        if (this.po != null) {
            dayType = this.po.getSundayDayType();
        }
        return dayType;
    }
    /**
     * @param sundayDayType The sundayDayType to set.
     */
    public void setSundayDayType(String sundayDayType) {
        //    ignore, read only
    }
    /**
     * @return Returns the sundayDrawFieldOpen.
     */
    public Boolean getSundayDrawFieldOpen() {
        boolean drawOpen = false;
        if (this.po != null) {
            drawOpen = this.po.getAllowSundayDrawEdits();
        }
        return new Boolean(drawOpen);
    }
    
    /**
     * @return Returns the maxDrawValue for Sunday.
     */
    public int getSundayMaxDrawValue() {
        int maxDraw = 100;
        if (this.po != null) {
            maxDraw = this.po.getSundayMaxDrawThreshold();
        }
        return maxDraw;
    }

    /**
     * 
     * @return The string "readonly" or an empty string
     */
    public String getSundayDrawReadonly() {
        String rOnly = WeeklyProductOrderHandler.readOnly;
        if (this.po != null) {
            if (this.po.getAllowSundayDrawEdits()) {
                rOnly = WeeklyProductOrderHandler.emptyString;
            }
        }
        return rOnly;
    }
    
    /**
     * @param sundayDrawFieldOpen The sundayDrawFieldOpen to set.
     */
    public void setSundayDrawFieldOpen(Boolean sundayDrawFieldOpen) {
        //      ignore, read only
    }
    /**
     * @return Returns the sundayDrawValue.
     */
    public int getSundayDrawValue() {
        int draw = -1;
        if (this.po != null) {
            draw = this.po.getSundayDraw();
        }
        return draw;
    }

    /**
     * 
     * @return String representations of draw value
     */
    public String getSundayDrawDisplayValue() {
        String draw = "";
        if (this.po != null && this.po.getSundayDraw() >= 0) {
            draw = String.valueOf(this.po.getSundayDraw());
        }
        return draw;
    }

    // value saved in Champion
    public String getOriginalSundayDrawDisplayValue() {
        String draw = inputTitleText;
        if (this.po != null && this.po.getOriginalProductOrder().getSundayDraw() >= 0) {
            draw = String.valueOf(this.po.getOriginalProductOrder().getSundayDraw());
        }
        return draw;
    }

    /**
     * 
     * @return
     */
    public Integer getSundayDrawValueObj() {
        // return the draw as an Integer object or null if not set (-1)
        Integer dValue = null;
        if (this.po != null && this.po.getSundayDraw() >= 0) {
            dValue = new Integer(this.po.getSundayDraw());
        }
        return dValue;
    }
    
    /**
     * @param sundayDrawValue The sundayDrawValue to set.
     */
    public void setSundayDrawValue(int sundayDrawValue) {
        if (this.po != null) {
            if (sundayDrawValue <= this.po.getMaxDrawThreshold() && sundayDrawValue >= 0) {
                try {
                    this.po.setSundayDraw(sundayDrawValue);
                }
                catch (UsatException ue) {
                    System.out.println("Failed to set draw: " + ue.getMessage());
                }
                
            }
        }
    }
    
    /**
     * 
     * @param value
     */
    public void setSundayDrawValueObj(Integer value) {
        if (this.po != null && value != null) {
            int iValue = value.intValue();
            this.setSundayDrawValue(iValue);
        }
    }
    
    /**
     * @return Returns the sundayReturnFieldOpen.
     */
    public Boolean getSundayReturnFieldOpen() {
        boolean returnOpen = false;
        if (this.po != null) {
            returnOpen = !this.po.getAllowSundayReturnsEdits();
        }
        return new Boolean(returnOpen);
    }
    
    /**
     * 
     * @return The string "readonly" or an empty string
     */
    public String getSundayReturnReadonly() {
        String rOnly = WeeklyProductOrderHandler.readOnly;
        if (this.po != null) {
            if (this.po.getAllowSundayReturnsEdits()) {
                rOnly = WeeklyProductOrderHandler.emptyString;
            }
        }
        return rOnly;
    }
     
    /**
     * @param sundayReturnFieldOpen The sundayReturnFieldOpen to set.
     */
    public void setSundayReturnFieldOpen(Boolean sundayReturnFieldOpen) {
        // ignore, read only
    }
    /**
     * @return Returns the sundayReturnValue.
     */
    public int getSundayReturnValue() {
        int returnVal = -1;
        if (this.po != null) {
            returnVal = this.po.getSundayReturns();
        }
        return returnVal;
    }
    
    /**
     * 
     * @return String representations of return value
     */
    public String getSundayReturnDisplayValue() {
        String returns = "";
        if (this.po != null && this.po.getSundayReturns() >= 0) {
            returns = String.valueOf(this.po.getSundayReturns());
        }
        return returns;
    }

    // Champion value
    public String getOriginalSundayReturnDisplayValue() {
        String returns = inputTitleText;
        if (this.po != null && this.po.getOriginalProductOrder().getSundayReturns() >= 0) {
            returns = String.valueOf(this.po.getOriginalProductOrder().getSundayReturns());
        }
        return returns;
    }

    /**
     * 
     * @return
     */
    public Integer getSundayReturnValueObj() {
        Integer rValue = null;
        if (this.po != null && this.po.getSundayReturns() >= 0) {
            rValue = new Integer(this.po.getSundayReturns());
        }
        return rValue;
    }
    
    /**
     * @param sundayReturnValue The sundayReturnValue to set.
     */
    public void setSundayReturnValue(int sundayReturnValue) {
        if (this.po != null) {
            if (sundayReturnValue >= -1) {
                try {
                    this.po.setSundayReturns(sundayReturnValue);
                }
                catch (UsatException ue) {
                    System.out.println("Failed to set draw: " + ue.getMessage());
                }                    
            }
        }
    }
    /**
     * 
     * @param value
     */
    public void setSundayReturnValueObj(Integer value) {
        if (this.po != null && value != null) {
            int iValue = value.intValue();
            this.setSundayReturnValue(iValue);
        }
    }
    
    /**
     * @return Returns the thursdayDayType.
     */
    public String getThursdayDayType() {
        String dayType = "";
        if (this.po != null) {
            dayType = this.po.getThursdayDayType();
        }
        return dayType;
    }
    /**
     * @param thursdayDayType The thursdayDayType to set.
     */
    public void setThursdayDayType(String thursdayDayType) {
        // ignore, read only
    }
    /**
     * @return Returns the thursdayDrawFieldOpen.
     */
    public Boolean getThursdayDrawFieldOpen() {
        boolean drawOpen = false;
        if (this.po != null) {
            drawOpen = this.po.getAllowThursdayDrawEdits();
        }
        return new Boolean(drawOpen);
    }
    
    /**
     * @return Returns the maxDrawValue for Thursday.
     */
    public int getThursdayMaxDrawValue() {
        int maxDraw = 100;
        if (this.po != null) {
            maxDraw = this.po.getThursdayMaxDrawThreshold();
        }
        return maxDraw;
    }

    /**
     * 
     * @return The string "readonly" or an empty string
     */
    public String getThursdayDrawReadonly() {
        String rOnly = WeeklyProductOrderHandler.readOnly;
        if (this.po != null) {
            if (this.po.getAllowThursdayDrawEdits()) {
                rOnly = WeeklyProductOrderHandler.emptyString;
            }
        }
        return rOnly;
    }    
    
    /**
     * @param thursdayDrawFieldOpen The thursdayDrawFieldOpen to set.
     */
    public void setThursdayDrawFieldOpen(Boolean thursdayDrawFieldOpen) {
        // ignore read only
    }
    /**
     * @return Returns the thursdayDrawValue.
     */
    public int getThursdayDrawValue() {
        int draw = -1;
        if (this.po != null) {
            draw = this.po.getThursdayDraw();
        }
        return draw;
    }
    
    /**
     * 
     * @return String representations of draw value
     */
    public String getThursdayDrawDisplayValue() {
        String draw = "";
        if (this.po != null && this.po.getThursdayDraw() >= 0) {
            draw = String.valueOf(this.po.getThursdayDraw());
        }
        return draw;
    }

    // value saved in Champion
    public String getOriginalThursdayDrawDisplayValue() {
        String draw = inputTitleText;
        if (this.po != null && this.po.getOriginalProductOrder().getThursdayDraw() >= 0) {
            draw = String.valueOf(this.po.getOriginalProductOrder().getThursdayDraw());
        }
        return draw;
    }

    /**
     * 
     * @return
     */
    public Integer getThursdayDrawValueObj() {
        // return the draw as an Integer object or null if not set (-1)
        Integer dValue = null;
        if (this.po != null && this.po.getThursdayDraw() >= 0) {
            dValue = new Integer(this.po.getThursdayDraw());
        }
        return dValue;
    }
    
    /**
     * @param thursdayDrawValue The thursdayDrawValue to set.
     */
    public void setThursdayDrawValue(int thursdayDrawValue) {
        if (this.po != null) {
            if (thursdayDrawValue <= this.po.getMaxDrawThreshold() && thursdayDrawValue >= 0) {
                try {
                    this.po.setThursdayDraw(thursdayDrawValue);
                }
                catch (UsatException ue) {
                    System.out.println("Failed to set draw: " + ue.getMessage());
                }
            }
        }
    }
    
    /**
     * 
     * @param value
     */
    public void setThursdayDrawValueObj(Integer value) {
        if (this.po != null && value != null) {
            int iValue = value.intValue();
            this.setThursdayDrawValue(iValue);
        }
    }
    
    /**
     * @return Returns the thursdayReturnFieldOpen.
     */
    public Boolean getThursdayReturnFieldOpen() {
        boolean returnOpen = false;
        if (this.po != null) {
            returnOpen = !this.po.getAllowThursdayReturnsEdits();
        }
        return new Boolean(returnOpen);
    }
    
    /**
     * 
     * @return The string "readonly" or an empty string
     */
    public String getThursdayReturnReadonly() {
        String rOnly = WeeklyProductOrderHandler.readOnly;
        if (this.po != null) {
            if (this.po.getAllowThursdayReturnsEdits()) {
                rOnly = WeeklyProductOrderHandler.emptyString;
            }
        }
        return rOnly;
    }
    
    /**
     * @param thursdayReturnFieldOpen The thursdayReturnFieldOpen to set.
     */
    public void setThursdayReturnFieldOpen(Boolean thursdayReturnFieldOpen) {
        // ignore, read only
    }
    /**
     * @return Returns the thursdayReturnValue.
     */
    public int getThursdayReturnValue() {
        int returnVal = -1;
        if (this.po != null) {
            returnVal = this.po.getThursdayReturns();
        }
        return returnVal;
    }

    /**
     * 
     * @return String representations of return value
     */
    public String getThursdayReturnDisplayValue() {
        String returns = "";
        if (this.po != null && this.po.getThursdayReturns() >= 0) {
            returns = String.valueOf(this.po.getThursdayReturns());
        }
        return returns;
    }

    // Champion value
    public String getOriginalThursdayReturnDisplayValue() {
        String returns = inputTitleText;
        if (this.po != null && this.po.getOriginalProductOrder().getThursdayReturns() >= 0) {
            returns = String.valueOf(this.po.getOriginalProductOrder().getThursdayReturns());
        }
        return returns;
    }

    /**
     * 
     * @return
     */
    public Integer getThursdayReturnValueObj() {
        Integer rValue = null;
        if (this.po != null && this.po.getThursdayReturns() >= 0) {
            rValue = new Integer(this.po.getThursdayReturns());
        }
        return rValue;
    }
    
    /**
     * @param thursdayReturnValue The thursdayReturnValue to set.
     */
    public void setThursdayReturnValue(int thursdayReturnValue) {
        if (this.po != null) {
            if (thursdayReturnValue >= -1) {
                try {
                    this.po.setThursdayReturns(thursdayReturnValue);
                }
                catch (UsatException ue) {
                    System.out.println("Failed to set draw: " + ue.getMessage());
                }
            }
        }
    }
    /**
     * 
     * @param value
     */
    public void setThursdayReturnValueObj(Integer value) {
        if (this.po != null && value != null) {
            int iValue = value.intValue();
            this.setThursdayReturnValue(iValue);
        }
    }
    
    /**
     * @return Returns the tuesdayDayType.
     */
    public String getTuesdayDayType() {
        String dayType = "";
        if (this.po != null) {
            dayType = this.po.getTuesdayDayType();
        }
        return dayType;
    }
    /**
     * @param tuesdayDayType The tuesdayDayType to set.
     */
    public void setTuesdayDayType(String tuesdayDayType) {
        // ignore, read only
    }
    /**
     * @return Returns the tuesdayDrawFieldOpen.
     */
    public Boolean getTuesdayDrawFieldOpen() {
        boolean drawOpen = false;
        if (this.po != null) {
            drawOpen = this.po.getAllowTuesdayDrawEdits();
        }
        return new Boolean(drawOpen);
    }
    
    /**
     * @return Returns the maxDrawValue for Tuesday.
     */
    public int getTuesdayMaxDrawValue() {
        int maxDraw = 100;
        if (this.po != null) {
            maxDraw = this.po.getTuesdayMaxDrawThreshold();
        }
        return maxDraw;
    }

    /**
     * 
     * @return The string "readonly" or an empty string
     */
    public String getTuesdayDrawReadonly() {
        String rOnly = WeeklyProductOrderHandler.readOnly;
        if (this.po != null) {
            if (this.po.getAllowTuesdayDrawEdits()) {
                rOnly = WeeklyProductOrderHandler.emptyString;
            }
        }
        return rOnly;
    }    
    
    /**
     * @param tuesdayDrawFieldOpen The tuesdayDrawFieldOpen to set.
     */
    public void setTuesdayDrawFieldOpen(Boolean tuesdayDrawFieldOpen) {
        // ignore, read only field
    }
    /**
     * @return Returns the tuesdayDrawValue.
     */
    public int getTuesdayDrawValue() {
        int draw = -1;
        if (this.po != null) {
            draw = this.po.getTuesdayDraw();
        }
        return draw;
    }
    
    /**
     * 
     * @return String representations of draw value
     */
    public String getTuesdayDrawDisplayValue() {
        String draw = "";
        if (this.po != null && this.po.getTuesdayDraw() >= 0) {
            draw = String.valueOf(this.po.getTuesdayDraw());
        }
        return draw;
    }

    // value saved in Champion
    public String getOriginalTuesdayDrawDisplayValue() {
        String draw = inputTitleText;
        if (this.po != null && this.po.getOriginalProductOrder().getTuesdayDraw() >= 0) {
            draw = String.valueOf(this.po.getOriginalProductOrder().getTuesdayDraw());
        }
        return draw;
    }

    /**
     * 
     * @return
     */
    public Integer getTuesdayDrawValueObj() {
        // return the draw as an Integer object or null if not set (-1)
        Integer dValue = null;
        if (this.po != null && this.po.getTuesdayDraw() >= 0) {
            dValue = new Integer(this.po.getTuesdayDraw());
        }
        return dValue;
    }
    
    /**
     * @param tuesdayDrawValue The tuesdayDrawValue to set.
     */
    public void setTuesdayDrawValue(int tuesdayDrawValue) {
        if (this.po != null) {
            if (tuesdayDrawValue <= this.po.getMaxDrawThreshold() && tuesdayDrawValue >= 0) {
                try {
                    this.po.setTuesdayDraw(tuesdayDrawValue);
                }
                catch (UsatException ue) {
                    System.out.println("Failed to set draw: " + ue.getMessage());
                }
            }
        }
    }

    /**
     * 
     * @param value
     */
    public void setTuesdayDrawValueObj(Integer value) {
        if (this.po != null && value != null) {
            int iValue = value.intValue();
            this.setTuesdayDrawValue(iValue);
        }
    }
    
    /**
     * @return Returns the tuesdayReturnFieldOpen.
     */
    public Boolean getTuesdayReturnFieldOpen() {
        boolean returnOpen = false;
        if (this.po != null) {
            returnOpen = !this.po.getAllowTuesdayReturnsEdits();
        }
        return new Boolean(returnOpen);
    }
    
    /**
     * 
     * @return The string "readonly" or an empty string
     */
    public String getTuesdayReturnReadonly() {
        String rOnly = WeeklyProductOrderHandler.readOnly;
        if (this.po != null) {
            if (this.po.getAllowTuesdayReturnsEdits()) {
                rOnly = WeeklyProductOrderHandler.emptyString;
            }
        }
        return rOnly;
    }
    
    /**
     * @param tuesdayReturnFieldOpen The tuesdayReturnFieldOpen to set.
     */
    public void setTuesdayReturnFieldOpen(Boolean tuesdayReturnFieldOpen) {
        //ignore, read only
    }
    /**
     * @return Returns the tuesdayReturnValue.
     */
    public int getTuesdayReturnValue() {
        int returnVal = -1;
        if (this.po != null) {
            returnVal = this.po.getTuesdayReturns();
        }
        return returnVal;
    }
    
    /**
     * 
     * @return String representations of return value
     */
    public String getTuesdayReturnDisplayValue() {
        String returns = "";
        if (this.po != null && this.po.getTuesdayReturns() >= 0) {
            returns = String.valueOf(this.po.getTuesdayReturns());
        }
        return returns;
    }
    
    // Champion value
    public String getOriginalTuesdayReturnDisplayValue() {
        String returns = inputTitleText;
        if (this.po != null && this.po.getOriginalProductOrder().getTuesdayReturns() >= 0) {
            returns = String.valueOf(this.po.getOriginalProductOrder().getTuesdayReturns());
        }
        return returns;
    }

    /**
     * 
     * @return
     */
    public Integer getTuesdayReturnValueObj() {
        Integer rValue = null;
        if (this.po != null && this.po.getTuesdayReturns() >= 0) {
            rValue = new Integer(this.po.getTuesdayReturns());
        }
        return rValue;
    }
    
    /**
     * @param tuesdayReturnValue The tuesdayReturnValue to set.
     */
    public void setTuesdayReturnValue(int tuesdayReturnValue) {
        if (this.po != null) {
            if (tuesdayReturnValue >= -1) {
                try {
                    this.po.setTuesdayReturns(tuesdayReturnValue);
                }
                catch (UsatException ue) {
                    System.out.println("Failed to set draw: " + ue.getMessage());
                }
            }
        }
    }
    /**
     * 
     * @param value
     */
    public void setTuesdayReturnValueObj(Integer value) {
        if (this.po != null && value != null) {
            int iValue = value.intValue();
            this.setTuesdayReturnValue(iValue);
        }
    }

    /**
     * @return Returns the wednesdayDayType.
     */
    public String getWednesdayDayType() {
        String dayType = "";
        if (this.po != null) {
            dayType = this.po.getWednesdayDayType();
        }
        return dayType;
    }
    /**
     * @param wednesdayDayType The wednesdayDayType to set.
     */
    public void setWednesdayDayType(String wednesdayDayType) {
        // ingore, read only field
    }
    /**
     * @return Returns the wednesdayDrawFieldOpen.
     */
    public Boolean getWednesdayDrawFieldOpen() {
        boolean drawOpen = false;
        if (this.po != null) {
            drawOpen = this.po.getAllowWednesdayDrawEdits();
        }
        return new Boolean(drawOpen);
    }
    
    /**
     * @return Returns the maxDrawValue for Tuesday.
     */
    public int getWednesdayMaxDrawValue() {
        int maxDraw = 100;
        if (this.po != null) {
            maxDraw = this.po.getWednesdayMaxDrawThreshold();
        }
        return maxDraw;
    }

    /**
     * 
     * @return The string "readonly" or an empty string
     */
    public String getWednesdayDrawReadonly() {
        String rOnly = WeeklyProductOrderHandler.readOnly;
        if (this.po != null) {
            if (this.po.getAllowWednesdayDrawEdits()) {
                rOnly = WeeklyProductOrderHandler.emptyString;
            }
        }
        return rOnly;
    }    
    
    /**
     * @param wednesdayDrawFieldOpen The wednesdayDrawFieldOpen to set.
     */
    public void setWednesdayDrawFieldOpen(Boolean wednesdayDrawFieldOpen) {
        // ingore, read only field
    }
    /**
     * @return Returns the wednesdayDrawValue.
     */
    public int getWednesdayDrawValue() {
        int draw = -1;
        if (this.po != null) {
            draw = this.po.getWednesdayDraw();
        }
        return draw;
    }

    /**
     * 
     * @return String representations of draw value
     */
    public String getWednesdayDrawDisplayValue() {
        String draw = "";
        if (this.po != null && this.po.getWednesdayDraw() >= 0) {
            draw = String.valueOf(this.po.getWednesdayDraw());
        }
        return draw;
    }

    // value saved in Champion
    public String getOriginalWednesdayDrawDisplayValue() {
        String draw = inputTitleText;
        if (this.po != null && this.po.getOriginalProductOrder().getWednesdayDraw() >= 0) {
            draw = String.valueOf(this.po.getOriginalProductOrder().getWednesdayDraw());
        }
        return draw;
    }

    /**
     * 
     * @return
     */
    public Integer getWednesdayDrawValueObj() {
        // return the draw as an Integer object or null if not set (-1)
        Integer dValue = null;
        if (this.po != null && this.po.getWednesdayDraw() >= 0) {
            dValue = new Integer(this.po.getWednesdayDraw());
        }
        return dValue;
    }
    
    /**
     * @param wednesdayDrawValue The wednesdayDrawValue to set.
     */
    public void setWednesdayDrawValue(int wednesdayDrawValue) {
        if (this.po != null) {
            if (wednesdayDrawValue <= this.po.getMaxDrawThreshold() && wednesdayDrawValue >= 0) {
                try {
                    this.po.setWednesdayDraw(wednesdayDrawValue);
                }
                catch (UsatException ue) {
                    System.out.println("Failed to set draw: " + ue.getMessage());
                }
            }
        }
    }
    
    /**
     * 
     * @param value
     */
    public void setWednesdayDrawValueObj(Integer value) {
        if (this.po != null && value != null) {
            int iValue = value.intValue();
            this.setWednesdayDrawValue(iValue);
        }
    }
    
    /**
     * @return Returns the wednesdayReturnFieldOpen.
     */
    public Boolean getWednesdayReturnFieldOpen() {
        boolean returnOpen = false;
        if (this.po != null) {
            returnOpen = !this.po.getAllowWednesdayReturnsEdits();
        }
        return new Boolean(returnOpen);
    }
    
    /**
     * 
     * @return The string "readonly" or an empty string
     */
    public String getWednesdayReturnReadonly() {
        String rOnly = WeeklyProductOrderHandler.readOnly;
        if (this.po != null) {
            if (this.po.getAllowWednesdayReturnsEdits()) {
                rOnly = WeeklyProductOrderHandler.emptyString;
            }
        }
        return rOnly;
    }
    
    /**
     * @param wednesdayReturnFieldOpen The wednesdayReturnFieldOpen to set.
     */
    public void setWednesdayReturnFieldOpen(Boolean wednesdayReturnFieldOpen) {
        // ingore, read only field
    }
    /**
     * @return Returns the wednesdayReturnValue.
     */
    public int getWednesdayReturnValue() {
        int returnVal = -1;
        if (this.po != null) {
            returnVal = this.po.getWednesdayReturns();
        }
        return returnVal;
    }

    /**
     * 
     * @return String representations of return value
     */
    public String getWednesdayReturnDisplayValue() {
        String returns = "";
        if (this.po != null && this.po.getWednesdayReturns() >= 0) {
            returns = String.valueOf(this.po.getWednesdayReturns());
        }
        return returns;
    }

    // Champion value
    public String getOriginalWednesdayReturnDisplayValue() {
        String returns = inputTitleText;
        if (this.po != null && this.po.getOriginalProductOrder().getWednesdayReturns() >= 0) {
            returns = String.valueOf(this.po.getOriginalProductOrder().getWednesdayReturns());
        }
        return returns;
    }

    /**
     * 
     * @return
     */
    public Integer getWednesdayReturnValueObj() {
        Integer rValue = null;
        if (this.po != null && this.po.getWednesdayReturns() >= 0) {
            rValue = new Integer(this.po.getWednesdayReturns());
        }
        return rValue;
    }
    
    /**
     * @param wednesdayReturnValue The wednesdayReturnValue to set.
     */
    public void setWednesdayReturnValue(int wednesdayReturnValue) {
        if (this.po != null) {
            if (wednesdayReturnValue >= -1) {
                try {
                    this.po.setWednesdayReturns(wednesdayReturnValue);
                }
                catch (UsatException ue) {
                    System.out.println("Failed to set draw: " + ue.getMessage());
                }
            }
        }
    }
    /**
     * 
     * @param value
     */
    public void setWednesdayReturnValueObj(Integer value) {
        if (this.po != null && value != null) {
            int iValue = value.intValue();
            this.setWednesdayReturnValue(iValue);
        }
    }

    /**
     * @return Returns the fridayZeroDrawReason.
     */
    public String getFridayZeroDrawReason() {
        String zdr = "";
        if (this.po != null) {
            zdr = this.po.getFridayZeroDrawReasonCode();
        }
        return zdr;
    }
    
    public String getOriginalFridayZeroDrawReason() {
        String zdr = "";
        if (this.po != null) {
            zdr = this.po.getOriginalProductOrder().getFridayZeroDrawReasonCode();
        }
        return zdr;
    }    
    
    /**
     * @return Returns the fridayZeroDrawReasonDescription.
     */
    public String getFridayZeroDrawReasonDescription() {
        String zdr = "";
        if (this.po != null) {
            if (ZeroDrawReasonCodeCache.getInstance().containsCode(this.po.getFridayZeroDrawReasonCode())) {
                zdr = ZeroDrawReasonCodeCache.getInstance().getDescription(this.po.getFridayZeroDrawReasonCode());
            }
        }
        return zdr;
    }
    
    /**
     * 
     * @return
     */
    public String getFridayZDRImageTitle() {
        String zdrTitle = "";
        if (this.po != null && this.po.getAllowFridayDrawEdits()) {
            zdrTitle = "Zero Draw Reason. Click to Change/View";
        }
        else {
            // no edits
            zdrTitle = "Zero Draw Reason: " + this.getFridayZeroDrawReasonDescription();
        }
        return zdrTitle;
    }

    public String getFridayZDRImageIcon() {
        String icon = WeeklyProductOrderHandler.zdrIconCodeSet;
        if (this.po.getFridayDraw()==0 && this.po.getOriginalProductOrder().getFridayDraw() == 0) {
            if (this.po.getFridayZeroDrawReasonCode() == null || this.po.getFridayZeroDrawReasonCode().trim().length() == 0) {
                icon = WeeklyProductOrderHandler.zdrIconNoCodeSet;
            }
        }
        return icon;
    }
    
    /**
     * @param fridayZeroDrawReason The fridayZeroDrawReason to set.
     */
    public void setFridayZeroDrawReason(String fridayZeroDrawReason) {
        if (this.po != null && fridayZeroDrawReason != null) {
            try {
                this.po.setFridayZeroDrawReasonCode(fridayZeroDrawReason);
            }
            catch (UsatException ue) {
                System.out.println("Failed to set draw: " + ue.getMessage());
            }
        }
    }
    
    /**
     * 
     * @return
     */
    public String getFridayZDRCSSString() {
        if ((this.po.getFridayZeroDrawReasonCode() == null || (this.po.getFridayZeroDrawReasonCode().trim().length() == 0)) && this.po.getAllowFridayDrawEdits() == false) {
            return "display: none";
        }

        if ((this.po.getFridayDraw() == 0 && this.po.getAllowFridayDrawEdits()) || (this.po.getFridayZeroDrawReasonCode() != null && this.po.getFridayZeroDrawReasonCode().length() > 0)) {
            return "";
        }
        else {
            return "display: none;";
        }
    }
    
    /**
     * @return Returns the mondayZeroDrawReason.
     */
    public String getMondayZeroDrawReason() {
        String zdr = "";
        if (this.po != null) {
            zdr = this.po.getMondayZeroDrawReasonCode();
        }
        return zdr;
    }

    public String getOriginalMondayZeroDrawReason() {
        String zdr = "";
        if (this.po != null) {
            zdr = this.po.getOriginalProductOrder().getMondayZeroDrawReasonCode();
        }
        return zdr;
    }
    
    
    /**
     * @return Returns the mondayZeroDrawReasonDescription.
     */
    public String getMondayZeroDrawReasonDescription() {
        String zdr = "";
        if (this.po != null) {
            if (ZeroDrawReasonCodeCache.getInstance().containsCode(this.po.getMondayZeroDrawReasonCode())) {
                zdr = ZeroDrawReasonCodeCache.getInstance().getDescription(this.po.getMondayZeroDrawReasonCode());
            }
        }
        return zdr;
    }

    /**
     * 
     * @return
     */
    public String getMondayZDRImageTitle() {
        String zdrTitle = "";
        if (this.po != null && this.po.getAllowMondayDrawEdits()) {
            zdrTitle = "Zero Draw Reason. Click to Change/View";
        }
        else {
            // no edits
            zdrTitle = "Zero Draw Reason: " + this.getMondayZeroDrawReasonDescription();
        }
        return zdrTitle;
    }

    public String getMondayZDRImageIcon() {
        String icon = WeeklyProductOrderHandler.zdrIconCodeSet;
        if (this.po.getMondayDraw()==0 && this.po.getOriginalProductOrder().getMondayDraw() == 0) {
            if (this.po.getMondayZeroDrawReasonCode() == null || this.po.getMondayZeroDrawReasonCode().trim().length() == 0) {
                icon = WeeklyProductOrderHandler.zdrIconNoCodeSet;
            }
        }
        return icon;
    }
    
    /**
     * @param mondayZeroDrawReason The mondayZeroDrawReason to set.
     */
    public void setMondayZeroDrawReason(String mondayZeroDrawReason) {
        if (this.po != null && mondayZeroDrawReason != null) {
            try { 
                this.po.setMondayZeroDrawReasonCode(mondayZeroDrawReason);
            }
            catch (UsatException ue) {
                System.out.println("Failed to set draw: " + ue.getMessage());
            }
        }
    }

    /**
     * 
     * @return
     */
    public String getMondayZDRCSSString() {
        if ((this.po.getMondayZeroDrawReasonCode() == null || (this.po.getMondayZeroDrawReasonCode().trim().length() == 0)) && this.po.getAllowMondayDrawEdits() == false) {
            return "display: none";
        }
        
        if ((this.po.getMondayDraw() == 0 && this.po.getAllowMondayDrawEdits()) || (this.po.getMondayZeroDrawReasonCode() != null && this.po.getMondayZeroDrawReasonCode().length() > 0)) {
            return "";
        }
        else {
            return "display: none;";
        }
    }
    
    /**
     * @return Returns the saturdayZeroDrawReason.
     */
    public String getSaturdayZeroDrawReason() {
        String zdr = "";
        if (this.po != null) {
            zdr = this.po.getSaturdayZeroDrawReasonCode();
        }
        return zdr;
    }

    public String getOriginalSaturdayZeroDrawReason() {
        String zdr = "";
        if (this.po != null) {
            zdr = this.po.getOriginalProductOrder().getSaturdayZeroDrawReasonCode();
        }
        return zdr;
    }
    
    /**
     * @return Returns the saturdayZeroDrawReasonDescription.
     */
    public String getSaturdayZeroDrawReasonDescription() {
        String zdr = "";
        if (this.po != null) {
            if (ZeroDrawReasonCodeCache.getInstance().containsCode(this.po.getSaturdayZeroDrawReasonCode())) {
                zdr = ZeroDrawReasonCodeCache.getInstance().getDescription(this.po.getSaturdayZeroDrawReasonCode());
            }
        }
        return zdr;
    }
    
    /**
     * @param saturdayZeroDrawReason The saturdayZeroDrawReason to set.
     */
    public void setSaturdayZeroDrawReason(String saturdayZeroDrawReason) {
        if (this.po != null && saturdayZeroDrawReason != null) {
            try {
                this.po.setSaturdayZeroDrawReasonCode(saturdayZeroDrawReason);
            }
            catch (UsatException ue) {
                System.out.println("Failed to set draw: " + ue.getMessage());
            }
        }
    }
    
    /**
     * 
     * @return
     */
    public String getSaturdayZDRCSSString() {
        if ((this.po.getSaturdayZeroDrawReasonCode() == null || (this.po.getSaturdayZeroDrawReasonCode().trim().length() == 0)) && this.po.getAllowSaturdayDrawEdits() == false) {
            return "display: none";
        }
        
        if ((this.po.getSaturdayDraw() == 0 && this.po.getAllowSaturdayDrawEdits()) || (this.po.getSaturdayZeroDrawReasonCode() != null && this.po.getSaturdayZeroDrawReasonCode().length() > 0)) {
            return "";
        }
        else {
            return "display: none;";
        }
    }

    /**
     * 
     * @return
     */
    public String getSaturdayZDRImageTitle() {
        String zdrTitle = "";
        if (this.po != null && this.po.getAllowSaturdayDrawEdits()) {
            zdrTitle = "Zero Draw Reason. Click to Change/View";
        }
        else {
            // no edits
            zdrTitle = "Zero Draw Reason: " + this.getSaturdayZeroDrawReasonDescription();
        }
        return zdrTitle;
    }
    
    public String getSaturdayZDRImageIcon() {
        String icon = WeeklyProductOrderHandler.zdrIconCodeSet;
        if (this.po.getSaturdayDraw()==0 && this.po.getOriginalProductOrder().getSaturdayDraw() == 0) {
            if (this.po.getSaturdayZeroDrawReasonCode() == null || this.po.getSaturdayZeroDrawReasonCode().trim().length() == 0) {
                icon = WeeklyProductOrderHandler.zdrIconNoCodeSet;
            }
        }
        return icon;
    }
    
    /**
     * @return Returns the sundayZeroDrawReason.
     */
    public String getSundayZeroDrawReason() {
        String zdr = "";
        if (this.po != null) {
            zdr = this.po.getSundayZeroDrawReasonCode();
        }
        return zdr;
    }
    
    public String getOriginalSundayZeroDrawReason() {
        String zdr = "";
        if (this.po != null) {
            zdr = this.po.getOriginalProductOrder().getSundayZeroDrawReasonCode();
        }
        return zdr;
    }

    /**
     * @return Returns the sundayZeroDrawReasonDescription.
     */
    public String getSundayZeroDrawReasonDescription() {
        String zdr = "";
        if (this.po != null) {
            if (ZeroDrawReasonCodeCache.getInstance().containsCode(this.po.getSundayZeroDrawReasonCode())) {
                zdr = ZeroDrawReasonCodeCache.getInstance().getDescription(this.po.getSundayZeroDrawReasonCode());
            }
        }
        return zdr;
    }
        
    /**
     * @param sundayZeroDrawReason The sundayZeroDrawReason to set.
     */
    public void setSundayZeroDrawReason(String sundayZeroDrawReason) {
        if (this.po != null && sundayZeroDrawReason != null) {
            try {
                this.po.setSundayZeroDrawReasonCode(sundayZeroDrawReason);
            }
            catch (UsatException ue) {
                System.out.println("Failed to set draw: " + ue.getMessage());
            }
        }
    }
    
    /**
     * 
     * @return
     */
    public String getSundayZDRCSSString() {
        if ((this.po.getSundayZeroDrawReasonCode() == null || (this.po.getSundayZeroDrawReasonCode().trim().length() == 0)) && this.po.getAllowSundayDrawEdits() == false) {
            return "display: none";
        }
        
        if ((this.po.getSundayDraw() == 0 && this.po.getAllowSundayDrawEdits()) || (this.po.getSundayZeroDrawReasonCode() != null && this.po.getSundayZeroDrawReasonCode().length() > 0)) {
            return "";
        }
        else {
            return "display: none;";
        }
    }

    /**
     * 
     * @return
     */
    public String getSundayZDRImageTitle() {
        String zdrTitle = "";
        if (this.po != null && this.po.getAllowSundayDrawEdits()) {
            zdrTitle = "Zero Draw Reason. Click to Change/View";
        }
        else {
            // no edits
            zdrTitle = "Zero Draw Reason: " + this.getSundayZeroDrawReasonDescription();
        }
        return zdrTitle;
    }

    public String getSundayZDRImageIcon() {
        String icon = WeeklyProductOrderHandler.zdrIconCodeSet;
        if (this.po.getSundayDraw()==0 && this.po.getOriginalProductOrder().getSundayDraw() == 0) {
            if (this.po.getSundayZeroDrawReasonCode() == null || this.po.getSundayZeroDrawReasonCode().trim().length() == 0) {
                icon = WeeklyProductOrderHandler.zdrIconNoCodeSet;
            }
        }
        return icon;
    }
    
    /**
     * @return Returns the thursdayZeroDrawReason.
     */
    public String getThursdayZeroDrawReason() {
        String zdr = "";
        if (this.po != null) {
            zdr = this.po.getThursdayZeroDrawReasonCode();
        }
        return zdr;
    }

    public String getOriginalThursdayZeroDrawReason() {
        String zdr = "";
        if (this.po != null) {
            zdr = this.po.getOriginalProductOrder().getThursdayZeroDrawReasonCode();
        }
        return zdr;
    }

    /**
     * @return Returns the thursdayZeroDrawReasonDescription.
     */
    public String getThursdayZeroDrawReasonDescription() {
        String zdr = "";
        if (this.po != null) {
            if (ZeroDrawReasonCodeCache.getInstance().containsCode(this.po.getThursdayZeroDrawReasonCode())) {
                zdr = ZeroDrawReasonCodeCache.getInstance().getDescription(this.po.getThursdayZeroDrawReasonCode());
            }
        }
        return zdr;
    }

    /**
     * 
     * @return
     */
    public String getThursdayZDRImageTitle() {
        String zdrTitle = "";
        if (this.po != null && this.po.getAllowThursdayDrawEdits()) {
            zdrTitle = "Zero Draw Reason. Click to Change/View";
        }
        else {
            // no edits
            zdrTitle = "Zero Draw Reason: " + this.getThursdayZeroDrawReasonDescription();
        }
        return zdrTitle;
    }

    public String getThursdayZDRImageIcon() {
        String icon = WeeklyProductOrderHandler.zdrIconCodeSet;
        if (this.po.getThursdayDraw()==0 && this.po.getOriginalProductOrder().getThursdayDraw() == 0) {
            if (this.po.getThursdayZeroDrawReasonCode() == null || this.po.getThursdayZeroDrawReasonCode().trim().length() == 0) {
                icon = WeeklyProductOrderHandler.zdrIconNoCodeSet;
            }
        }
        return icon;
    }    
    
    /**
     * @param thursdayZeroDrawReason The thursdayZeroDrawReason to set.
     */
    public void setThursdayZeroDrawReason(String thursdayZeroDrawReason) {
        if (this.po != null && thursdayZeroDrawReason != null) {
            try {
                this.po.setThursdayZeroDrawReasonCode(thursdayZeroDrawReason);
            }
            catch (UsatException ue) {
                System.out.println("Failed to set draw: " + ue.getMessage());
            }
        }
    }
    
    /**
     * 
     * @return
     */
    public String getThursdayZDRCSSString() {
        if ((this.po.getThursdayZeroDrawReasonCode() == null || (this.po.getThursdayZeroDrawReasonCode().trim().length() == 0)) && this.po.getAllowThursdayDrawEdits() == false) {
            return "display: none";
        }
        
        if ((this.po.getThursdayDraw() == 0 && this.po.getAllowThursdayDrawEdits()) || (this.po.getThursdayZeroDrawReasonCode() != null && this.po.getThursdayZeroDrawReasonCode().length() > 0)) {
            return "";
        }
        else {
            return "display: none;";
        }
    }
    
    /**
     * @return Returns the tuesdayZeroDrawReason.
     */
    public String getTuesdayZeroDrawReason() {
        String zdr = "";
        if (this.po != null) {
            zdr = this.po.getTuesdayZeroDrawReasonCode();
        }
        return zdr;
    }

    public String getOriginalTuesdayZeroDrawReason() {
        String zdr = "";
        if (this.po != null) {
            zdr = this.po.getOriginalProductOrder().getTuesdayZeroDrawReasonCode();
        }
        return zdr;
    }
    
    /**
     * @return Returns the tuesdayZeroDrawReasonDescription.
     */
    public String getTuesdayZeroDrawReasonDescription() {
        String zdr = "";
        if (this.po != null) {
            if (ZeroDrawReasonCodeCache.getInstance().containsCode(this.po.getTuesdayZeroDrawReasonCode())) {
                zdr = ZeroDrawReasonCodeCache.getInstance().getDescription(this.po.getTuesdayZeroDrawReasonCode());
            }
        }
        return zdr;
    }

    /**
     * 
     * @return
     */
    public String getTuesdayZDRImageTitle() {
        String zdrTitle = "";
        if (this.po != null && this.po.getAllowTuesdayDrawEdits()) {
            zdrTitle = "Zero Draw Reason. Click to Change/View";
        }
        else {
            // no edits
            zdrTitle = "Zero Draw Reason: " + this.getTuesdayZeroDrawReasonDescription();
        }
        return zdrTitle;
    }

    public String getTuesdayZDRImageIcon() {
        String icon = WeeklyProductOrderHandler.zdrIconCodeSet;
        if (this.po.getTuesdayDraw()==0 && this.po.getOriginalProductOrder().getTuesdayDraw() == 0) {
            if (this.po.getTuesdayZeroDrawReasonCode() == null || this.po.getTuesdayZeroDrawReasonCode().trim().length() == 0) {
                icon = WeeklyProductOrderHandler.zdrIconNoCodeSet;
            }
        }
        return icon;
    }
    
    /**
     * @param tuesdayZeroDrawReason The tuesdayZeroDrawReason to set.
     */
    public void setTuesdayZeroDrawReason(String tuesdayZeroDrawReason) {
        if (this.po != null && tuesdayZeroDrawReason != null) {
            try {
                this.po.setTuesdayZeroDrawReasonCode(tuesdayZeroDrawReason);
            }
            catch (UsatException ue) {
                System.out.println("Failed to set draw: " + ue.getMessage());
            }
        }
    }
    
    /**
     * 
     * @return
     */
    public String getTuesdayZDRCSSString() {
        if ((this.po.getTuesdayZeroDrawReasonCode() == null || (this.po.getTuesdayZeroDrawReasonCode().trim().length() == 0)) && this.po.getAllowTuesdayDrawEdits() == false) {
            return "display: none";
        }
        
        if ((this.po.getTuesdayDraw() == 0 && this.po.getAllowTuesdayDrawEdits()) || (this.po.getTuesdayZeroDrawReasonCode() != null && this.po.getTuesdayZeroDrawReasonCode().length() > 0)) {
            return "";
        }
        else {
            return "display: none;";
        }
    }
    
    /**
     * @return Returns the wednesdayZeroDrawReason.
     */
    public String getWednesdayZeroDrawReason() {
        String zdr = "";
        if (this.po != null) {
            zdr = this.po.getWednesdayZeroDrawReasonCode();
        }
        return zdr;
    }

    public String getOriginalWednesdayZeroDrawReason() {
        String zdr = "";
        if (this.po != null) {
            zdr = this.po.getOriginalProductOrder().getWednesdayZeroDrawReasonCode();
        }
        return zdr;
    }
    
    /**
     * @return Returns the wednesdayZeroDrawReasonDescription.
     */
    public String getWednesdayZeroDrawReasonDescription() {
        String zdr = "";
        if (this.po != null) {
            if (ZeroDrawReasonCodeCache.getInstance().containsCode(this.po.getWednesdayZeroDrawReasonCode())) {
                zdr = ZeroDrawReasonCodeCache.getInstance().getDescription(this.po.getWednesdayZeroDrawReasonCode());
            }
        }
        return zdr;
    }

    /**
     * 
     * @return
     */
    public String getWednesdayZDRImageTitle() {
        String zdrTitle = "";
        if (this.po != null && this.po.getAllowWednesdayDrawEdits()) {
            zdrTitle = "Zero Draw Reason. Click to Change/View";
        }
        else {
            // no edits
            zdrTitle = "Zero Draw Reason: " + this.getWednesdayZeroDrawReasonDescription();
        }
        return zdrTitle;
    }

    public String getWednesdayZDRImageIcon() {
        String icon = WeeklyProductOrderHandler.zdrIconCodeSet;
        if (this.po.getWednesdayDraw()==0 && this.po.getOriginalProductOrder().getWednesdayDraw() == 0) {
            if (this.po.getWednesdayZeroDrawReasonCode() == null || this.po.getWednesdayZeroDrawReasonCode().trim().length() == 0) {
                icon = WeeklyProductOrderHandler.zdrIconNoCodeSet;
            }
        }
        return icon;
    }
    
    /**
     * @param wednesdayZeroDrawReason The wednesdayZeroDrawReason to set.
     */
    public void setWednesdayZeroDrawReason(String wednesdayZeroDrawReason) {
        if (this.po != null && wednesdayZeroDrawReason != null) {
            try {
                this.po.setWednesdayZeroDrawReasonCode(wednesdayZeroDrawReason);
            }
            catch (UsatException ue) {
                System.out.println("Failed to set draw: " + ue.getMessage());
            }
        }
    }
    
    /**
     * 
     * @return
     */
    public String getWednesdayZDRCSSString() {
        if ((this.po.getWednesdayZeroDrawReasonCode() == null || (this.po.getWednesdayZeroDrawReasonCode().trim().length() == 0)) && this.po.getAllowWednesdayDrawEdits() == false) {
            return "display: none";
        }
        
        if ((this.po.getWednesdayDraw() == 0 && this.po.getAllowWednesdayDrawEdits()) || (this.po.getWednesdayZeroDrawReasonCode() != null && this.po.getWednesdayZeroDrawReasonCode().length() > 0)) {
            return "";
        }
        else {
            return "display: none;";
        }
    }
    
    /**
     * @return Returns the productOrderType.
     */
    public String getProductOrderType() {
        String productOrderType = "";
        if (this.po != null) {
            productOrderType = this.po.getProductOrderTypeCode();
            if (productOrderType == null) {
                productOrderType = "??";
            }
        }
        return productOrderType;
    }
    /**
     * @param productOrderType The productOrderType to set.
     */
    public void setProductOrderType(String productOrderType) {
        //ignore read only field;
    }
    
    public String getMondayErrorMessage() {
        if (!this.mondayErrorsProcessed) {
            this.processMondayErrors();
        }
        return this.mondayErrorMessage;
    }

    public String getMondayDrawCSS() {
        if (!this.mondayErrorsProcessed) {
            this.processMondayErrors();
        }
        if (!this.po.getAllowMondayDrawEdits()) {
            this.mondayDrawCSS += WeeklyProductOrderHandler.disabledCSS;
        }
        else {
            this.mondayDrawCSS += " drawInputText";
        }
        
        return this.mondayDrawCSS;
    }

    public String getMondayReturnCSS() {
        if (!this.mondayErrorsProcessed) {
            this.processMondayErrors();
        }
        if (!this.po.getAllowMondayReturnsEdits()) {
            this.mondayReturnCSS += WeeklyProductOrderHandler.disabledCSS;
        }
        else {
            this.mondayReturnCSS += " returnInputText";
        }

       return this.mondayReturnCSS;
    }

    
    private void processMondayErrors() {
        DrawUpdateErrorIntf[] errorsArray = this.po.getUpdateErrors(); 
        if (errorsArray != null) {
            WeeklyDrawUpdateErrorIntf error = null;
            
            for (int i = 0; i < errorsArray.length; i++) {
                error = (WeeklyDrawUpdateErrorIntf)errorsArray[i];
                DateTime weekEnding = error.getWeekEndingDate();
                DateTime monday = weekEnding.minusDays(6);
                // if error applies to Monday
                if (error.getErrorDate().getDayOfWeek() == monday.getDayOfWeek()){
                    // add  Monday error message
                    this.mondayErrorMessage += error.getErrorMessage() + " ";
                    if (DrawUpdateErrorIntf.DRAW_ERROR.equalsIgnoreCase(error.getAppliesTo())) {
                        this.mondayDrawCSS = WeeklyProductOrderHandler.aDayErrCSS;
                    }
                    else if (DrawUpdateErrorIntf.RETURN_ERROR.equalsIgnoreCase(error.getAppliesTo())) {
                        this.mondayReturnCSS = WeeklyProductOrderHandler.aDayErrCSS;
                    }
                    else  {
                        this.mondayDrawCSS = WeeklyProductOrderHandler.aDayErrCSS;
                        this.mondayReturnCSS = WeeklyProductOrderHandler.aDayErrCSS;
                    }
                    
                }
            }
        }
        this.mondayErrorsProcessed = true;
    }
    
    public String getTuesdayErrorMessage() {
        if (!this.tuesdayErrorsProcessed) {
            this.processTuesdayErrors();
        }
        return this.tuesdayErrorMessage;
    }

    public String getTuesdayDrawCSS() {
        if (!this.tuesdayErrorsProcessed) {
            this.processTuesdayErrors();
        }
        if (!this.po.getAllowTuesdayDrawEdits()) {
            this.tuesdayDrawCSS += WeeklyProductOrderHandler.disabledCSS;
        }
        else {
            this.tuesdayDrawCSS += " drawInputText";
        }

       return this.tuesdayDrawCSS;
    }

    public String getTuesdayReturnCSS() {
        if (!this.tuesdayErrorsProcessed) {
            this.processTuesdayErrors();
        }
        if (!this.po.getAllowTuesdayReturnsEdits()) {
            this.tuesdayReturnCSS += WeeklyProductOrderHandler.disabledCSS;
        }
        else {
            this.tuesdayReturnCSS += " returnInputText";
        }

       return this.tuesdayReturnCSS;
    }

    
    private void processTuesdayErrors() {
        DrawUpdateErrorIntf[] errorsArray = this.po.getUpdateErrors(); 
        if (errorsArray != null) {
            WeeklyDrawUpdateErrorIntf error = null;
            
            for (int i = 0; i < errorsArray.length; i++) {
                error = (WeeklyDrawUpdateErrorIntf)errorsArray[i];
                DateTime weekEnding = error.getWeekEndingDate();
                DateTime tuesday = weekEnding.minusDays(5);
                // if error applies to tuesday
                if (error.getErrorDate().getDayOfWeek() == tuesday.getDayOfWeek()){
                    // add  tuesday error message
                    this.tuesdayErrorMessage += error.getErrorMessage() + " ";
                    if (DrawUpdateErrorIntf.DRAW_ERROR.equalsIgnoreCase(error.getAppliesTo())) {
                        this.tuesdayDrawCSS = WeeklyProductOrderHandler.bDayErrCSS;
                    }
                    else if (DrawUpdateErrorIntf.RETURN_ERROR.equalsIgnoreCase(error.getAppliesTo())) {
                        this.tuesdayReturnCSS = WeeklyProductOrderHandler.bDayErrCSS;
                    }
                    else {
                        this.tuesdayDrawCSS = WeeklyProductOrderHandler.bDayErrCSS;
                        this.tuesdayReturnCSS = WeeklyProductOrderHandler.bDayErrCSS;
                    }
                    
                }
            }
        }
        this.tuesdayErrorsProcessed = true;
    }

    public String getWednesdayErrorMessage() {
        if (!this.wednesdayErrorsProcessed) {
            this.processWednesdayErrors();
        }
        return this.wednesdayErrorMessage;
    }

    public String getWednesdayDrawCSS() {
        if (!this.wednesdayErrorsProcessed) {
            this.processWednesdayErrors();
        }
        if (!this.po.getAllowWednesdayDrawEdits()) {
            this.wednesdayDrawCSS += WeeklyProductOrderHandler.disabledCSS;
        }
        else {
            this.wednesdayDrawCSS += " drawInputText";
        }

       return this.wednesdayDrawCSS;
    }

    public String getWednesdayReturnCSS() {
        if (!this.wednesdayErrorsProcessed) {
            this.processWednesdayErrors();
        }
        if (!this.po.getAllowWednesdayReturnsEdits()) {
            this.wednesdayReturnCSS += WeeklyProductOrderHandler.disabledCSS;
        }
        else {
            this.wednesdayReturnCSS += " returnInputText";
        }

       return this.wednesdayReturnCSS;
    }

    private void processWednesdayErrors() {
        DrawUpdateErrorIntf[] errorsArray = this.po.getUpdateErrors(); 
        if (errorsArray != null) {
            WeeklyDrawUpdateErrorIntf error = null;
            
            for (int i = 0; i < errorsArray.length; i++) {
                error = (WeeklyDrawUpdateErrorIntf)errorsArray[i];
                DateTime weekEnding = error.getWeekEndingDate();
                DateTime wednesday = weekEnding.minusDays(4);
                // if error applies to Wednesday
                if (error.getErrorDate().getDayOfWeek() == wednesday.getDayOfWeek()){
                    // add  Wednesday error message
                    this.wednesdayErrorMessage += error.getErrorMessage() + " ";
                    if (DrawUpdateErrorIntf.DRAW_ERROR.equalsIgnoreCase(error.getAppliesTo())) {
                        this.wednesdayDrawCSS = WeeklyProductOrderHandler.aDayErrCSS;
                    }
                    else if (DrawUpdateErrorIntf.RETURN_ERROR.equalsIgnoreCase(error.getAppliesTo())) {
                        this.wednesdayReturnCSS = WeeklyProductOrderHandler.aDayErrCSS;
                    }
                    else  {
                        this.wednesdayDrawCSS = WeeklyProductOrderHandler.aDayErrCSS;
                        this.wednesdayReturnCSS = WeeklyProductOrderHandler.aDayErrCSS;
                    }
                    
                }
            }
        }
        this.wednesdayErrorsProcessed = true;
    }
    
    public String getThursdayErrorMessage() {
        if (!this.thursdayErrorsProcessed) {
            this.processThursdayErrors();
        }
        return this.thursdayErrorMessage;
    }

    public String getThursdayDrawCSS() {
        if (!this.thursdayErrorsProcessed) {
            this.processThursdayErrors();
        }
        if (!this.po.getAllowThursdayDrawEdits()) {
            this.thursdayDrawCSS += WeeklyProductOrderHandler.disabledCSS;
        }
        else {
            this.thursdayDrawCSS += " drawInputText";
        }
        
       return this.thursdayDrawCSS;
    }

    public String getThursdayReturnCSS() {
        if (!this.thursdayErrorsProcessed) {
            this.processThursdayErrors();
        }
        if (!this.po.getAllowThursdayReturnsEdits()) {
            this.thursdayReturnCSS += WeeklyProductOrderHandler.disabledCSS;
        }
        else {
            this.thursdayReturnCSS += " returnInputText";
        }

       return this.thursdayReturnCSS;
    }

    
    private void processThursdayErrors() {
        DrawUpdateErrorIntf[] errorsArray = this.po.getUpdateErrors(); 
        if (errorsArray != null) {
            WeeklyDrawUpdateErrorIntf error = null;
            
            for (int i = 0; i < errorsArray.length; i++) {
                error = (WeeklyDrawUpdateErrorIntf)errorsArray[i];
                DateTime weekEnding = error.getWeekEndingDate();
                DateTime thursday = weekEnding.minusDays(3);
                // if error applies to Thursday
                if (error.getErrorDate().getDayOfWeek() == thursday.getDayOfWeek()){
                    // add  tuesday error Thursday
                    this.thursdayErrorMessage += error.getErrorMessage() + " ";
                    if (DrawUpdateErrorIntf.DRAW_ERROR.equalsIgnoreCase(error.getAppliesTo())) {
                        this.thursdayDrawCSS = WeeklyProductOrderHandler.bDayErrCSS;
                    }
                    else if (DrawUpdateErrorIntf.RETURN_ERROR.equalsIgnoreCase(error.getAppliesTo())) {
                        this.thursdayReturnCSS = WeeklyProductOrderHandler.bDayErrCSS;
                    }
                    else {
                        this.thursdayDrawCSS = WeeklyProductOrderHandler.bDayErrCSS;
                        this.thursdayReturnCSS = WeeklyProductOrderHandler.bDayErrCSS;
                    }
                    
                }
            }
        }
        this.thursdayErrorsProcessed = true;
    }

    public String getFridayErrorMessage() {
        if (!this.fridayErrorsProcessed) {
            this.processFridayErrors();
        }
        return this.fridayErrorMessage;
    }

    public String getFridayDrawCSS() {
        if (!this.fridayErrorsProcessed) {
            this.processFridayErrors();
        }
        if (!this.po.getAllowFridayDrawEdits()) {
            this.fridayDrawCSS += WeeklyProductOrderHandler.disabledCSS;
        }
        else {
            this.fridayDrawCSS += " drawInputText";
        }
        
       return this.fridayDrawCSS;
    }

    public String getFridayReturnCSS() {
        if (!this.fridayErrorsProcessed) {
            this.processFridayErrors();
        }
        if (!this.po.getAllowFridayReturnsEdits()) {
            this.fridayReturnCSS += WeeklyProductOrderHandler.disabledCSS;
        }
        else {
            this.fridayReturnCSS += " returnInputText";
        }
        
        return this.fridayReturnCSS;
    }

    private void processFridayErrors() {
        DrawUpdateErrorIntf[] errorsArray = this.po.getUpdateErrors(); 
        if (errorsArray != null) {
            WeeklyDrawUpdateErrorIntf error = null;
            
            for (int i = 0; i < errorsArray.length; i++) {
                error = (WeeklyDrawUpdateErrorIntf)errorsArray[i];
                DateTime weekEnding = error.getWeekEndingDate();
                DateTime friday = weekEnding.minusDays(2);
                // if error applies to Friday
                if (error.getErrorDate().getDayOfWeek() == friday.getDayOfWeek()){
                    // add  Wednesday error Friday
                    this.fridayErrorMessage += error.getErrorMessage() + " ";
                    if (DrawUpdateErrorIntf.DRAW_ERROR.equalsIgnoreCase(error.getAppliesTo())) {
                        this.fridayDrawCSS = WeeklyProductOrderHandler.aDayErrCSS;
                    }
                    else if (DrawUpdateErrorIntf.RETURN_ERROR.equalsIgnoreCase(error.getAppliesTo())) {
                        this.fridayReturnCSS = WeeklyProductOrderHandler.aDayErrCSS;
                    }
                    else  {
                        this.fridayDrawCSS = WeeklyProductOrderHandler.aDayErrCSS;
                        this.fridayReturnCSS = WeeklyProductOrderHandler.aDayErrCSS;
                    }
                    
                }
            }
        }
        this.fridayErrorsProcessed = true;
    }
    
    public String getSaturdayErrorMessage() {
        if (!this.saturdayErrorsProcessed) {
            this.processSaturdayErrors();
        }
        return this.saturdayErrorMessage;
    }

    public String getSaturdayDrawCSS() {
        if (!this.saturdayErrorsProcessed) {
            this.processSaturdayErrors();
        }
        if (!this.po.getAllowSaturdayDrawEdits()) {
            this.saturdayDrawCSS += WeeklyProductOrderHandler.disabledCSS;
        }
        else {
            this.saturdayDrawCSS += " drawInputText";
        }
        
        return this.saturdayDrawCSS;
    }

    public String getSaturdayReturnCSS() {
        if (!this.saturdayErrorsProcessed) {
            this.processSaturdayErrors();
        }
        if (!this.po.getAllowSaturdayReturnsEdits()) {
            this.saturdayReturnCSS += WeeklyProductOrderHandler.disabledCSS;
        }
        else {
            this.saturdayReturnCSS += " returnInputText";
        }

       return this.saturdayReturnCSS;
    }

    
    private void processSaturdayErrors() {
        DrawUpdateErrorIntf[] errorsArray = this.po.getUpdateErrors(); 
        if (errorsArray != null) {
            WeeklyDrawUpdateErrorIntf error = null;
            
            for (int i = 0; i < errorsArray.length; i++) {
                error = (WeeklyDrawUpdateErrorIntf)errorsArray[i];
                DateTime weekEnding = error.getWeekEndingDate();
                DateTime saturday = weekEnding.minusDays(1);
                // if error applies to Saturday
                if (error.getErrorDate().getDayOfWeek() == saturday.getDayOfWeek()){
                    // add  saturday error 
                    this.saturdayErrorMessage += error.getErrorMessage() + " ";
                    if (DrawUpdateErrorIntf.DRAW_ERROR.equalsIgnoreCase(error.getAppliesTo())) {
                        this.saturdayDrawCSS = WeeklyProductOrderHandler.bDayErrCSS;
                    }
                    else if (DrawUpdateErrorIntf.RETURN_ERROR.equalsIgnoreCase(error.getAppliesTo())) {
                        this.saturdayReturnCSS = WeeklyProductOrderHandler.bDayErrCSS;
                    }
                    else {
                        this.saturdayDrawCSS = WeeklyProductOrderHandler.bDayErrCSS;
                        this.saturdayReturnCSS = WeeklyProductOrderHandler.bDayErrCSS;
                    }
                    
                }
            }
        }
        this.saturdayErrorsProcessed = true;
    }

    public String getSundayErrorMessage() {
        if (!this.sundayErrorsProcessed) {
            this.processSundayErrors();
        }
        return this.sundayErrorMessage;
    }

    public String getSundayDrawCSS() {
        if (!this.sundayErrorsProcessed) {
            this.processSundayErrors();
        }
        if (!this.po.getAllowSundayDrawEdits()) {
            this.sundayDrawCSS += WeeklyProductOrderHandler.disabledCSS;
        }
        else {
            this.sundayDrawCSS += " drawInputText";
        }
        
        return this.sundayDrawCSS;
    }

    public String getSundayReturnCSS() {
        if (!this.sundayErrorsProcessed) {
            this.processSundayErrors();
        }
        if (!this.po.getAllowSundayReturnsEdits()) {
            this.sundayReturnCSS += WeeklyProductOrderHandler.disabledCSS;
        }
        else {
            this.sundayReturnCSS += " returnInputText";
        }
        
       return this.sundayReturnCSS;
    }

    private void processSundayErrors() {
        DrawUpdateErrorIntf[] errorsArray = this.po.getUpdateErrors(); 
        if (errorsArray != null) {
            WeeklyDrawUpdateErrorIntf error = null;
            
            for (int i = 0; i < errorsArray.length; i++) {
                error = (WeeklyDrawUpdateErrorIntf)errorsArray[i];
                DateTime weekEnding = error.getWeekEndingDate();
                // if error applies to Sunday
                if (error.getErrorDate().getDayOfWeek() == weekEnding.getDayOfWeek()){
                    // add  Sunday error 
                    this.sundayErrorMessage += error.getErrorMessage() + " ";
                    if (DrawUpdateErrorIntf.DRAW_ERROR.equalsIgnoreCase(error.getAppliesTo())) {
                        this.sundayDrawCSS = WeeklyProductOrderHandler.aDayErrCSS;
                    }
                    else if (DrawUpdateErrorIntf.RETURN_ERROR.equalsIgnoreCase(error.getAppliesTo())) {
                        this.sundayReturnCSS = WeeklyProductOrderHandler.aDayErrCSS;
                    }
                    else  {
                        this.sundayDrawCSS = WeeklyProductOrderHandler.aDayErrCSS;
                        this.sundayReturnCSS = WeeklyProductOrderHandler.aDayErrCSS;
                    }
                    
                }
            }
        }
        this.sundayErrorsProcessed = true;
    }
    
    /**
     * @param mondayErrorsProcessed The mondayErrorsProcessed to set.
     */
    public void resetErrorsProcessed() {
        
        // added the following but may need to remove.
        this.po.clearUpdateErrors();
        
        this.mondayErrorsProcessed = false;
        this.mondayDrawCSS = WeeklyProductOrderHandler.aDayCSS;
        this.mondayReturnCSS = WeeklyProductOrderHandler.aDayCSS;
        this.mondayErrorMessage = "";
        
        this.tuesdayErrorsProcessed = false;
        this.tuesdayDrawCSS = WeeklyProductOrderHandler.bDayCSS;
        this.tuesdayReturnCSS = WeeklyProductOrderHandler.bDayCSS;
        this.tuesdayErrorMessage = "";

        this.wednesdayErrorsProcessed = false;
        this.wednesdayDrawCSS = WeeklyProductOrderHandler.aDayCSS;
        this.wednesdayReturnCSS = WeeklyProductOrderHandler.aDayCSS;
        this.wednesdayErrorMessage = "";

        this.thursdayErrorsProcessed = false;
        this.thursdayDrawCSS = WeeklyProductOrderHandler.bDayCSS;
        this.thursdayReturnCSS = WeeklyProductOrderHandler.bDayCSS;
        this.thursdayErrorMessage = "";

        this.fridayErrorsProcessed = false;
        this.fridayDrawCSS = WeeklyProductOrderHandler.aDayCSS;
        this.fridayReturnCSS = WeeklyProductOrderHandler.aDayCSS;
        this.fridayErrorMessage = "";

        this.saturdayErrorsProcessed = false;
        this.saturdayDrawCSS = WeeklyProductOrderHandler.bDayCSS;
        this.saturdayReturnCSS = WeeklyProductOrderHandler.bDayCSS;
        this.saturdayErrorMessage = "";

        this.sundayErrorsProcessed = false;
        this.sundayDrawCSS = WeeklyProductOrderHandler.aDayCSS;
        this.sundayReturnCSS = WeeklyProductOrderHandler.aDayCSS;
        this.sundayErrorMessage = "";

    }
    
    public void clear() {
        this.po = null;
    }
}

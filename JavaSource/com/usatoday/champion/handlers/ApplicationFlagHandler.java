/*
 * Created on Sep 18, 2008
 *
 */
package com.usatoday.champion.handlers;

/**
 * @author aeast
 *
 * Instances of this class can be used for setting boolean indicators in the application
 * For example, an application scoped one is used to put the site into maintenance mode.
 */
public class ApplicationFlagHandler {

	private boolean booleanValue = false;
	
	/**
	 * 
	 */
	public ApplicationFlagHandler() {
		super();
	
	}

	/**
	 * @return Returns the booleanValue.
	 */
	public boolean getBooleanValue() {
		return booleanValue;
	}
	/**
	 * @param booleanValue The booleanValue to set.
	 */
	public void setBooleanValue(boolean booleanValue) {
		this.booleanValue = booleanValue;
	}
}

/*
 * Created on May 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers;

import java.util.Date;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductDescriptionIntf;
import com.usatoday.singlecopy.model.util.ProductDescriptionCache;

/**
 * @author aeast
 * @date May 2, 2007
 * @class WeeklyEndDatesHandler
 * 
 * 
 * 
 */
public class WeeklyEndDatesHandler {

    private String pub = null;
    private Date earliestDate = null;
    private Date latestDate = null;
    private Date selectedDate = null;
    
    /**
     * 
     */
    public WeeklyEndDatesHandler() {
        super();
    }

    /**
     * @return Returns the earliestDate.
     */
    public Date getEarliestDate() {
        return this.earliestDate;
    }

    public String getEarliestDateAsStr() {
        String dateStr = "";
        
        DateTime lTime = new DateTime(this.earliestDate);
        dateStr = lTime.toString("yyyy/mm/dd hh:mm:ss");
        return dateStr;
    }
    /**
     * @param earliestDate The earliestDate to set.
     */
    public void setEarliestDate(Date earliestDate) {
        this.earliestDate = earliestDate;
    }
    /**
     * @return Returns the latestDate.
     */
    public Date getLatestDate() {
        return this.latestDate;
    }

    /**
     * @return Returns the latestDate.
     */
    public String getLatestDateAsStr() {
        String dateStr = "";
        
        DateTime lTime = new DateTime(this.latestDate);
        //dateStr = lTime.toString("yyyy/MM/dd");
        dateStr = lTime.toString("yyyy/mm/dd hh:mm:ss");
        return dateStr;
    }
    /**
     * @param latestDate The latestDate to set.
     */
    public void setLatestDate(Date latestDate) {
        this.latestDate = latestDate;
    }
    /**
     * @return Returns the pub.
     */
    public String getPub() {
        return this.pub;
    }
    /**
     * @param pub The pub to set.
     */
    public void setPub(String pub) {
        this.pub = pub;
    }
    /**
     * @return Returns the selectedDate.
     */
    public Date getSelectedDate() {
        return this.selectedDate;
    }
    /**
     * @param selectedDate The selectedDate to set.
     */
    public void setSelectedDate(Date selectedDate) {
        this.selectedDate = selectedDate;
    }
    
    /**
     * 
     * @return
     */
    public String getProductDescription() {
        String description = "No publication description available.";
        
        ProductDescriptionCache pdCache = ProductDescriptionCache.getInstance();
        
        ProductDescriptionIntf pd = pdCache.getProductDesription(this.getPub());
        
        description = pd.getDescription();
        
        return description;
    }
    
}

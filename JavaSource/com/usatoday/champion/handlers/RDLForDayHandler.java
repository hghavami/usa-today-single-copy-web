/*
 * Created on Mar 6, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyDrawLocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyDrawManagementIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.RouteDrawManagementIntf;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;

/**
 * @author aeast
 * @date Mar 6, 2007
 * @class RDLForDayHandler
 * 
 * 
 * 
 */
public class RDLForDayHandler {

    private DateTime day;
    private Collection<LocationHandler> locations = new ArrayList<LocationHandler>();
    private DailyDrawManagementIntf ddm = null;
    //private RouteDrawManagementIntf route = null;
   // private String routeID = null;
    private String errorDetail = "";
    
    private boolean saveInProgress = false;
    
    
    /**
     * 
     */
    public RDLForDayHandler() {
        super();        
    }
    
    public RDLForDayHandler(Date d, RouteDrawManagementIntf route, UserIntf user) {
        super();
        
        if (d != null) {
            this.day = new DateTime(d);
        }
        else{
            this.day = new DateTime();
        }
        
        //this.route = route;
        try {
            
            this.ddm = route.retrieveDailyDraw(this.day, user);
      //      this.routeID = this.ddm.getRoute().getRouteID();
            Collection<LocationIntf> locationBOs = this.ddm.getLocations();
            Iterator<LocationIntf> locItr = locationBOs.iterator();
            
            while (locItr.hasNext()) {
                DailyDrawLocationIntf ddl = (DailyDrawLocationIntf)locItr.next();
                LocationHandler dlh = new LocationHandler(ddl);
                this.locations.add(dlh);
            }
            
        }
        catch(UsatException ue) {
            // ignore
            ;
        }        
    }
    /**
     * @return Returns the day.
     */
    public Date getDay() {
        return this.day.toDate();
    }
    /**
     * @param day The day to set.
     */
    public void setDay(Date day) {
        this.day = new DateTime(day);
    }
    /**
     * @return Returns the weeklyDrawEndDates.
     */
    public Collection<LocationHandler> getLocations() {
        return this.locations;
    }
    /**
     * @param weeklyDrawEndDates The weeklyDrawEndDates to set.
     */
    public void setLocations(Collection<LocationHandler> locations) {
        this.locations = locations;
    }
    
    /**
     * @return Returns the ddm.
     */
    public DailyDrawManagementIntf getDdm() {
        return this.ddm;
    }
    
    // resets error processing state on handler
    // so after a commit to 400 the error states are recalculated in the handler.
    public void resetErrorProcessing() {
        Iterator<LocationHandler> itr = this.locations.iterator();
        while(itr.hasNext()) {
            LocationHandler dlh = itr.next();
            Iterator<ProductOrderHandler> itr2 = dlh.getProductOrders().iterator();
            while (itr2.hasNext()) {
                ProductOrderHandler dpo = itr2.next();
                dpo.resetErrorsProcessed();
            }
        }
    }    
    
    public Integer getTotalDraw() {
        Integer totDraw = null;
        if (this.ddm != null) {
            totDraw = new Integer(this.ddm.getTotalDraw());
        }
        else {
            totDraw = new Integer(0);
        }
        return totDraw;
    }
    
    public Integer getOriginalTotalDraw() {
        Integer totDraw = null;
        if (this.ddm != null) {
            totDraw = new Integer(this.ddm.getOriginalTotalDraw());
        }
        else {
            totDraw = new Integer(0);
        }
        return totDraw;
    }

    public Integer getTotalReturns() {
        Integer totReturns = null;
        if (this.ddm != null) {
            totReturns = new Integer(this.ddm.getTotalReturns());
        }
        else {
            totReturns = new Integer(0);
        }
        return totReturns;
    }
    
    public Integer getOriginalTotalReturns() {
        Integer totReturns = null;
        if (this.ddm != null) {
            totReturns = new Integer(this.ddm.getOriginalTotalReturns());
        }
        else {
            totReturns = new Integer(0);
        }
        return totReturns;
    }
    
    public String getSaveFailedMessage() {
        String msg = "";
        
        if (ddm.getUpdateErrorsFlag()) {
            msg = "Not all product orders were updated. Please correct any errors indicated below and submit again. " + ddm.getGlobalUpdateErrorMessage() + " " + errorDetail;
        }
        return msg;
    }    
    
    public String getLateReturnsString() {
        if (ddm.getLateReturnsFlag()) {
            
            return "";
        }
        else {
            return "display: none";
        }
    }

    public String getEdittableFieldsCSS() {
        if (ddm.hasEdittableFields()) {
            return "display: none";
        }
        else {
            return "";
        }
    }
    
    
    /**
     * @return Returns the errorDetail.
     */
    public String getErrorDetail() {
        return this.errorDetail;
    }
    /**
     * @param errorDetail The errorDetail to set.
     */
    public void setErrorDetail(String errorDetail) {
        this.errorDetail = errorDetail;
    }
    
    public void clear() {
        try {
	        if (this.ddm != null) {
	            this.ddm.clear();
	            this.ddm = null;
	        }
	       // this.route = null;
	        if (this.locations != null && !this.locations.isEmpty()) {
	            Iterator<LocationHandler> itr = this.locations.iterator();
	            while (itr.hasNext()) {
	                LocationHandler loch = itr.next();
	                loch.clear();
	            }
	            this.locations.clear();
	        }
	        this.locations = null;
	    }
	    catch (Exception e) {
	        ;
	    }
        
    }
	/**
	 * @return Returns the saveInProgress.
	 */
	public boolean isSaveInProgress() {
		return saveInProgress;
	}
	/**
	 * @param saveInProgress The saveInProgress to set.
	 */
	public void setSaveInProgress(boolean saveInProgress) {
		this.saveInProgress = saveInProgress;
	}
}

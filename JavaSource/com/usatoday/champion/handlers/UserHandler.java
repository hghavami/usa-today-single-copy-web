/*
 * Created on Feb 2, 2007
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers;

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;



import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.model.SelectItem;

import com.usatoday.singlecopy.model.bo.users.UserBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipLocationIntf;
import com.usatoday.singlecopy.model.interfaces.markets.MarketIntf;
import com.usatoday.singlecopy.model.interfaces.users.AccessRightsIntf;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;

/**
 * @author aeast
 * @date Feb 2, 2007
 * @class UserHandler
 * 
 * This is the base handling class for Web Users. It is used to tie the business (model) 
 * object to the web tier.
 * 
 * 
 */
public class UserHandler {
    private boolean autoAuthOnly = false;
    private String eAddress = "";
    private Integer numberOfLocsPerPagePref = new Integer(1000);

    private String browserUserAgentValue = "unknown";
    
    // following would be a reference to the model type that corresponds to this handler
    // either a contractor or a base user class.
    private UserIntf user = null;
    
    /**
     * 
     */
    public UserHandler() {
        super();
    }
    
    public boolean getAuthenticated() {
        boolean authenticated = false;
        if (this.user != null && this.autoAuthOnly == false) {
            authenticated = this.user.isUserAuthenticated();
        }
        return authenticated;
    }
    public boolean getAutoAuthOnly() {
        return this.autoAuthOnly;
    }

    public ArrayList<SelectItem> getBlueChipLocationNamesForSelectItems() {
        
        ArrayList<SelectItem> dropDownView = new ArrayList<SelectItem>();
        UserIntf user = this.user;
        UserBO u = (UserBO) user;
        
        try {
            if (user != null) {
                Iterator<BlueChipLocationIntf> locations = u.getBlueChipLocations().values().iterator();
                
                while (locations.hasNext()) {
                    BlueChipLocationIntf loc = locations.next();
                    SelectItem si = new javax.faces.model.SelectItem();
                    si.setDescription(loc.getLocationName());
                    si.setLabel(loc.getLocationName());
                    si.setValue(loc.getLocationID());
                    dropDownView.add(si);
                }
    	    }
        }
        catch (Exception e) {
            System.out.println("BlueChipLocationNameHandler:  Failed to build Location Name select items: " + e.getMessage());
        }
        return dropDownView;
    }
    
    public boolean getCanWorkWithBlueChipDraw() {
       if (user != null && user.hasAccess(AccessRightsIntf.BLUE_CHIP_DRAW)) {
            return true;
       }
       return false;
    }
    
    public boolean getCanWorkWithRoutes() {
        if (user != null && user.hasAccess(AccessRightsIntf.ROUTE_DRAW_MNGMT)) {
            return true;
        }
        return false;
    }
    
    public String getDisplayName() {
        String name = "";
        if (this.user != null) {
            name = this.user.getDisplayName();
        }        
        return name;
    }
    
    public String getDivCss() {
        String css = "display: inline";
        if (this.user == null || !this.user.isUserAuthenticated()) {
            css = "";
        }
        return css;
    }

    public String getEmailAddress() {
        String email = eAddress;
        //if (this.user != null) {
        //    email = this.user.getEmailAddress();
        //}        
        return email;
    }

    public String getEmailReminderTimePreference() {
        String emailReminderTimePref = null;
        if (user != null && this.user.isUserAuthenticated()) {
            AccessRightsIntf right = user.getAccessRight(AccessRightsIntf.BLUE_CHIP_DRAW);
            if (right != null && right.getEmailReminderTimePreference() != null) {
            
                emailReminderTimePref = user.getAccessRight(AccessRightsIntf.BLUE_CHIP_DRAW).getEmailReminderTimePreference();
            }
        }
        return emailReminderTimePref;
    }
    /**
     * @return Returns the numberOfLocsPerPagePref.
     */
    public Integer getNumberOfLocsPerPagePref() {
        return this.numberOfLocsPerPagePref;
    }
    
    public String getReceivingEmailReminders() {
        String receivingEmails = "No";
        if (user != null && this.user.isUserAuthenticated()) {
            AccessRightsIntf right = user.getAccessRight(AccessRightsIntf.BLUE_CHIP_DRAW);
            if (right != null && right.isReceivingEmailReminders()) {
                receivingEmails = "Yes";
            }
        }
        return receivingEmails;
    }

    // used for initial page load with no changes
    public String getReceivingEmailRemindersReadOnly() {
        return this.getReceivingEmailReminders();
    }
    
    public void setReceivingEmailRemindersReadOnly(String newVal) {
    	// read only so no Setter.
    	;
    }    
    
    public UserIntf getUser() {
        return this.user;
    }
    
    public Collection<MarketIntf> getUserMarkets() {
        Collection<MarketIntf> c = null;
        try {
            if (this.user != null && this.user.isUserAuthenticated()) {
                UserBO u = (UserBO)this.user;
                c = u.getMarketsThatPertainToUser();
            }
        }
        catch(Exception e) {
            // ignore
            
        }
        if (c==null) {
            c = new ArrayList<MarketIntf>();
        }
        return c;
    }
    public String saveUser(String newEmail, String newPwd) throws Exception {
          // add logic to invoke the save method on the underlying BO
	      user.setEmailAddress(newEmail); 
	      user.setPassword(newPwd);
	        
	      user.save();   

	      if (newEmail != null) {
	          this.eAddress = newEmail;
	      }
	    // the return message may be used by JSF layer but the BO layer 
	    // should not know anything about it.
	    return "success";
    }
    public void setAutoAuthOnly(boolean autoAuthOnly) {
        this.autoAuthOnly = autoAuthOnly;
    }
    
    public void setCanWorkWithRoutes(boolean value) {
        // can't set this value
        // only here for bean compatibility
    }
    public void setEmailAddress(String emailAddress) {
        this.eAddress = emailAddress;
    }
    
    public void setEmailReminderTimePreference(String emailReminderTimePreference) {
        if (user.getAccessRight(AccessRightsIntf.BLUE_CHIP_DRAW) != null) {
            user.getAccessRight(AccessRightsIntf.BLUE_CHIP_DRAW).setEmailReminderTimePreference(emailReminderTimePreference);
        }
    }
    /**
     * @param numberOfLocsPerPagePref The numberOfLocsPerPagePref to set.
     */
    public void setNumberOfLocsPerPagePref(Integer numberOfLocsPerPagePref) {
        if (numberOfLocsPerPagePref != null && numberOfLocsPerPagePref.intValue() > 0) {
            this.numberOfLocsPerPagePref = numberOfLocsPerPagePref;
        }
    }
    
    public void setReceivingEmailReminders(String receivesEmailReminders) {
        user.getAccessRight(AccessRightsIntf.BLUE_CHIP_DRAW).setReceivingEmailReminders(false);
        if (user.getAccessRight(AccessRightsIntf.BLUE_CHIP_DRAW) != null) {
            if (receivesEmailReminders != null && receivesEmailReminders.equals("Yes")) {
                user.getAccessRight(AccessRightsIntf.BLUE_CHIP_DRAW).setReceivingEmailReminders(true);                
            }
        }
    }

    public void setUser(UserIntf user) {
        this.user = user;
        if (user != null) {
            this.eAddress = user.getEmailAddress();
        }
    }
    
	/**
	 * @return Returns the browserUserAgentValue.
	 */
	public String getBrowserUserAgentValue() {
		return browserUserAgentValue;
	}
	/**
	 * @param browserUserAgentValue The browserUserAgentValue to set.
	 */
	public void setBrowserUserAgentValue(String browserUserAgentValue) {
		this.browserUserAgentValue = browserUserAgentValue;
	}
}

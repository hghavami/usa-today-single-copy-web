/*
 * Created on Mar 24, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers.blueChipDraw;

import java.util.Date;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf;

/**
 * @author aeast
 * @date Mar 24, 2008
 * @class BlueChipWeeklyProductOrderHandler
 * 
 * Wraps the Weekly Product order view BO
 * 
 */
public class BlueChipWeeklyProductOrderHandler implements Comparable<BlueChipWeeklyProductOrderHandler> {

    private BlueChipProductOrderWeekIntf wpo = null;
    
    BlueChipProductOrderHandler mondayHandler = null;
    BlueChipProductOrderHandler tuesdayHandler = null;
    BlueChipProductOrderHandler wednesdayHandler = null;
    BlueChipProductOrderHandler thursayHandler = null;
    BlueChipProductOrderHandler fridayHandler = null;
    BlueChipProductOrderHandler saturdayHandler = null;
    BlueChipProductOrderHandler sundayHandler = null;
    
    
    /**
     * 
     */
    public BlueChipWeeklyProductOrderHandler() {
        super();
    }

    public BlueChipWeeklyProductOrderHandler(BlueChipProductOrderWeekIntf source) {
        super();

        this.wpo = source;
        
        if (this.wpo != null) {
            setupPOs();
        }
        
    }

    private void setupPOs() {
        if (this.mondayHandler != null) this.mondayHandler.setPo(null);
        this.mondayHandler = new BlueChipProductOrderHandler(this.wpo.getMondayPO());
        if (this.tuesdayHandler != null) this.tuesdayHandler.setPo(null);
        this.tuesdayHandler = new BlueChipProductOrderHandler(this.wpo.getTuesdayPO());
        if (this.wednesdayHandler != null) this.wednesdayHandler.setPo(null);
        this.wednesdayHandler = new BlueChipProductOrderHandler(this.wpo.getWednesdayPO());
        if (this.thursayHandler != null) this.thursayHandler.setPo(null);
        this.thursayHandler = new BlueChipProductOrderHandler(this.wpo.getThursdayPO());
        if (this.fridayHandler != null) this.fridayHandler.setPo(null);
        this.fridayHandler = new BlueChipProductOrderHandler(this.wpo.getFridayPO());
        if (this.saturdayHandler != null) this.saturdayHandler.setPo(null);
        this.saturdayHandler = new BlueChipProductOrderHandler(this.wpo.getSaturdayPO());
        if (this.sundayHandler != null) this.sundayHandler.setPo(null);
        this.sundayHandler = new BlueChipProductOrderHandler(this.wpo.getSundayPO());
        
    }
    
    public BlueChipProductOrderHandler getMondayPO() {
        
        return this.mondayHandler;
        
    }

    public BlueChipProductOrderHandler getTuesdayPO() {
        
        return this.tuesdayHandler;
        
    }

    public BlueChipProductOrderHandler getWednesdayPO() {
        
        return this.wednesdayHandler;
        
    }
    
    public BlueChipProductOrderHandler getThursdayPO() {
        
        return this.thursayHandler;
        
    }
    public BlueChipProductOrderHandler getFridayPO() {
        
        return this.fridayHandler;
        
    }
    public BlueChipProductOrderHandler getSaturdayPO() {
        
        return this.saturdayHandler;
        
    }
    
    public BlueChipProductOrderHandler getSundayPO() {
        
        return this.sundayHandler;
        
    }
    public BlueChipProductOrderWeekIntf getWpo() {
        return this.wpo;
    }
    public Date getWeekEndingDate() {
        Date d = null;
        if (this.wpo != null && this.wpo.getWeekEndingDate() != null) {
            d = this.wpo.getWeekEndingDate().toDate();
        }
        return d;
    }
    
    public int compareTo(BlueChipWeeklyProductOrderHandler o) {
        BlueChipWeeklyProductOrderHandler otherObj = o;
        Integer sourceInt = new Integer(this.wpo.getSequenceNumber());
        Integer compareToInt = new Integer(otherObj.getWpo().getSequenceNumber());
        return sourceInt.compareTo(compareToInt);
    }

    public void setWpo(BlueChipProductOrderWeekIntf wpo) {
        this.wpo = wpo;
        if (wpo != null) {
            this.setupPOs();
        }
    }
    
    public boolean getHasNextWeek() {
        boolean hasNextWeek = false;
        
        try {
	        if (this.wpo != null) {
	            BlueChipMultiWeekProductOrderIntf multiWeek = this.wpo.getOwningMultiWeek();
	            BlueChipProductOrderWeekIntf week = multiWeek.getWeekAfter(this.wpo);
	            if (week != null) {
	                hasNextWeek = true;
	            }
	        }
        }
        catch (Exception e) {
        	; 
        }
        return hasNextWeek;
    }
    
    public boolean getHasPrevWeek() {
        boolean hasPrevWeek = false;

        try {
	        if (this.wpo != null) {
	            BlueChipMultiWeekProductOrderIntf multiWeek = this.wpo.getOwningMultiWeek();
	            if (multiWeek != null) {
	            	BlueChipProductOrderWeekIntf week = multiWeek.getWeekBefore(this.wpo);
	            	if (week != null) {
	            		hasPrevWeek = true;
	            	}
	            }
	        }
        }
        catch (Exception e) {
        	; // ignore
        }
        return hasPrevWeek;
    }
    
    
    public void clear() {
        this.mondayHandler.setPo(null);
        this.tuesdayHandler.setPo(null);
        this.wednesdayHandler.setPo(null);
        this.thursayHandler.setPo(null);
        this.fridayHandler.setPo(null);
        this.saturdayHandler.setPo(null);
        this.sundayHandler.setPo(null);
        this.wpo = null;
    }
}

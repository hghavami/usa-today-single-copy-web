/*
 * Created on Mar 24, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers.blueChipDraw;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipLocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf;

/**
 * @author aeast
 * @date Mar 24, 2008
 * @class BlueChipLocationHandler
 * 
 * This class wraps the BlueChipLocation object. It is necessary to maintain state
 * of which week is currently displayed. JSF only exposes simple getter/setters with no
 * arguments. We need to specify the week type to the underlying business object.
 * 
 */
public class BlueChipLocationHandler implements Comparable<BlueChipLocationHandler> {
    
    private BlueChipLocationIntf location = null;
    private BlueChipDrawManagementHandler parentHandler = null;

    /**
     * 
     */
    public BlueChipLocationHandler() {
        super();
    }

    public BlueChipLocationIntf getLocation() {
        return this.location;
    }
    public void setLocation(BlueChipLocationIntf location) {
        this.location = location;
    }
    
    public BlueChipDrawManagementHandler getParentHandler() {
        return this.parentHandler;
    }
    public void setParentHandler(BlueChipDrawManagementHandler parentHandler) {
        this.parentHandler = parentHandler;
    }
    
    /**
     * 
     * @return List of product orders (BlueChipProductOrderHandler) that all for the next distribution date
     */
    public Collection<BlueChipProductOrderHandler> getProductOrdersForNextDistributionDate() {

        
        /*ArrayList<BlueChipProductOrderHandler> aList = new ArrayList<BlueChipProductOrderHandler>();
        
        Collection<BlueChipProductOrderIntf> pos = this.location.getNextDistributionDateProductOrders();
        Collection<BlueChipProductOrderIntf> pos = this.location.getProductOrdersThatCutOffToday();
        
        if (pos != null && !pos.isEmpty()) {
            Iterator<BlueChipProductOrderIntf> itr = pos.iterator();
            while (itr.hasNext()) {
                BlueChipProductOrderIntf po = itr.next();
                BlueChipProductOrderHandler poh = new BlueChipProductOrderHandler();
                poh.setPo(po);
                aList.add(poh);
            }            
        }
        
        Collections.sort(aList);
        return aList;
        */
    	ArrayList<BlueChipProductOrderHandler> aList = new ArrayList<BlueChipProductOrderHandler>();
    	
    	Collection<BlueChipProductOrderIntf> nextDayPOs = this.location.getNextDistributionDateProductOrders(); 
        Collection<BlueChipProductOrderIntf> cutOffTodayPOs = this.location.getProductOrdersThatCutOffToday();
        
        HashSet<BlueChipProductOrderIntf> theCollection = new HashSet<BlueChipProductOrderIntf>();
        
        HashMap<String, ArrayList<BlueChipProductOrderHandler>> posByPOID = new HashMap<String, ArrayList<BlueChipProductOrderHandler>>();
        
        
        if (cutOffTodayPOs != null && !cutOffTodayPOs.isEmpty()) {
        	for (BlueChipProductOrderIntf po : cutOffTodayPOs) {
        		boolean added = theCollection.add(po);
        		if (added) {
                    BlueChipProductOrderHandler poh = new BlueChipProductOrderHandler();
                    poh.setPo(po);
                    if (posByPOID.containsKey(po.getProductOrderID())) {
                    	ArrayList<BlueChipProductOrderHandler> poCol = posByPOID.get(po.getProductOrderID());
                    	poCol.add(poh);
                    }
                    else {
                    	ArrayList<BlueChipProductOrderHandler> poCol = new ArrayList<BlueChipProductOrderHandler>();
                    	poCol.add(poh);
                    	posByPOID.put(po.getProductOrderID(), poCol);
                    }
                    
                    
                    // aList.add(poh);  // old
        		}
        	}
        }
        
        if (nextDayPOs != null && !nextDayPOs.isEmpty()) {
        	for (BlueChipProductOrderIntf po : nextDayPOs) {
        		boolean added = theCollection.add(po);
        		if (added) {
                    BlueChipProductOrderHandler poh = new BlueChipProductOrderHandler();
                    poh.setPo(po);
                    if (posByPOID.containsKey(po.getProductOrderID())) {
                    	ArrayList<BlueChipProductOrderHandler> poCol = posByPOID.get(po.getProductOrderID());
                    	poCol.add(poh);
                    }
                    else {
                    	ArrayList<BlueChipProductOrderHandler> poCol = new ArrayList<BlueChipProductOrderHandler>();
                    	poCol.add(poh);
                    	posByPOID.put(po.getProductOrderID(), poCol);
                    }
                    
                   //   aList.add(poh);  old
        		}
        	}
        }
        
        theCollection.clear();
        
        //new move them into aList here
        Set<String> poidSet = posByPOID.keySet();
        
        ArrayList<String> poidList = new ArrayList<String>(poidSet);
        Collections.sort(poidList);
       
        for (String poidKey : poidList) {
        	ArrayList<BlueChipProductOrderHandler> poCol = posByPOID.get(poidKey);
        	Collections.sort(poCol, BlueChipProductOrderHandler.DELIVERY_DATE_ORDER);
        	
        	for (BlueChipProductOrderHandler poh : poCol) {
        		aList.add(poh);
        	}
        }
        
        
        // Collections.sort(aList); old
        return aList;
        
    }

    /**
     * 
     * @param date
     * @return
     */
    public Collection<BlueChipProductOrderHandler> getProductOrdersForDate(DateTime date) {

        
        ArrayList<BlueChipProductOrderHandler> aList = new ArrayList<BlueChipProductOrderHandler>();

        if (date == null) {
        	return aList;
        }
        
        Collection<BlueChipProductOrderIntf> pos = this.location.getProductOrdersForDay(date);
        
        if (pos != null && !pos.isEmpty()) {
            Iterator<BlueChipProductOrderIntf> itr = pos.iterator();
            while (itr.hasNext()) {
                BlueChipProductOrderIntf po = itr.next();
                BlueChipProductOrderHandler poh = new BlueChipProductOrderHandler();
                poh.setPo(po);
                aList.add(poh);
            }            
        }
        
        Collections.sort(aList);
        return aList;
        
    }
    
    public String getAddress1() {
        return this.location.getAddress1();
    }
    
    public String getLocationName() {
        return this.location.getLocationName();
    }
    
    public String getLocationID() {
        return this.location.getLocationID();
    }
    
    public String getOwningMarketID() {
        return this.location.getOwningMarketID();
    }
    
    public String getMarketAndLocID () {
        return this.location.getOwningMarketID() + "-" + this.location.getLocationID();
    }
    
    public String getPhoneNumber() {
        return this.location.getPhoneNumber();
    }
    
    public int compareTo(BlueChipLocationHandler o) {
        BlueChipLocationHandler otherObj = (BlueChipLocationHandler)o;
        String sourceString = this.getLocationName();
        String otherString = otherObj.getLocationName();
        return sourceString.compareTo(otherString);
    }
    
    public void clear(){
        this.location = null;
        this.parentHandler = null;
    }
}

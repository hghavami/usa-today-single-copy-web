/*
 * Created on May 28, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers.blueChipDraw;

import java.util.ArrayList;
import java.util.Iterator;

import javax.faces.model.SelectItem;

import com.usatoday.singlecopy.model.interfaces.EmailScheduleSentTimeIntf;
import com.usatoday.singlecopy.model.util.EmailScheduleCache;

/**
 * @author aeast
 * @date May 28, 2008
 * @class BlueChipEmailReminderCacheHandler
 * 
 * 
 * 
 */
public class BlueChipEmailReminderCacheHandler {

    /**
     * 
     */
    public BlueChipEmailReminderCacheHandler() {
        super();
    }
    
    public ArrayList<SelectItem> getBlueChipEmailSchedulesForForSelectItems() {
        
        ArrayList<SelectItem> dropDownView = new ArrayList<SelectItem>();
        
        try {
		    if (EmailScheduleCache.getInstance().getScheduleTimes().size() > 0) {
		        Iterator<EmailScheduleSentTimeIntf> itr = EmailScheduleCache.getInstance().getScheduleTimes().iterator();
		        while (itr.hasNext()) {
		            EmailScheduleSentTimeIntf time = itr.next();
		            SelectItem si = new javax.faces.model.SelectItem();
		            si.setDescription(time.getScheduleCodeDesc());
		            si.setLabel(time.getScheduleCodeDesc());
		            si.setValue(time.getScheduleCode());
		            dropDownView.add(si);
		        }
		    }
        }
        catch (Exception e) {
            System.out.println("BlueChipEmailScheduleHandler:  Failed to build email select items: " + e.getMessage());
        }
        return dropDownView;
    }
    
}

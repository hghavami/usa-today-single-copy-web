package com.usatoday.champion.handlers.blueChipDraw;

import java.util.ArrayList;
import java.util.Collection;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderHistoryIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderHistoryWeekIntf;

public class BlueChipProductOrderHistoryHandler {

	private BlueChipLocationHandler location = null;
	private BlueChipProductOrderHistoryIntf history = null;
	private BlueChipMultiWeekProductOrderIntf owningPO = null;

	public BlueChipProductOrderHistoryIntf getHistory() {
		return history;
	}

	public void setHistory(BlueChipProductOrderHistoryIntf hist) {
		this.history = hist;
	}

	public BlueChipLocationHandler getLocation() {
		return location;
	}

	public void setLocation(BlueChipLocationHandler location) {
		this.location = location;
	}
	
	public Collection<BlueChipProductOrderHistoryWeekHandler> getWeeks() {
		
		ArrayList<BlueChipProductOrderHistoryWeekHandler> weeks = new ArrayList<BlueChipProductOrderHistoryWeekHandler>();
		
		try {
			for (BlueChipProductOrderHistoryWeekIntf aWeeek : this.history.getAllWeeks()) {
				BlueChipProductOrderHistoryWeekHandler weekH = new BlueChipProductOrderHistoryWeekHandler();
				weekH.setWeek(aWeeek);
				weeks.add(weekH);
			}
		}
		catch (Exception e) {
			;
		}
		return weeks;
	}
	
	public String getProductOrderID() {
		String poid = null;
		
		try {
			poid = this.history.getProductOrderID();
		}
		catch (Exception e) {
			;
		}
		return poid;
	}

	public BlueChipMultiWeekProductOrderIntf getOwningPO() {
		return owningPO;
	}

	public void setOwningPO(BlueChipMultiWeekProductOrderIntf owningPO) {
		this.owningPO = owningPO;
	}
	
}

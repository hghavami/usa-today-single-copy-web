/*
 * Created on Mar 21, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers.blueChipDraw;

import java.util.Comparator;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;

/**
 * @author aeast
 * @date Mar 21, 2008
 * @class BlueChipProductOrderHandler
 * 
 * 
 * 
 */
public class BlueChipProductOrderHandler implements Comparable<BlueChipProductOrderHandler> {

    private static String DRAW_CSS = "inputText dmEditField";
    private static String DRAW_ERR_CSS = " inputText_Error";
    private static String DISABLED_CSS = " inputText_disabled";

    BlueChipProductOrderIntf po = null;
    private String drawCSS = BlueChipProductOrderHandler.DRAW_CSS;
    
    // Following class used for sorting PO's by delivery date
    public static final Comparator<BlueChipProductOrderHandler> DELIVERY_DATE_ORDER =
        new Comparator<BlueChipProductOrderHandler>() {
    	public int compare(BlueChipProductOrderHandler po1, BlueChipProductOrderHandler po2) {
    		return po1.getDate().compareTo(po2.getDate());
    	}
    };
    
    
    /**
     * 
     */
    public BlueChipProductOrderHandler() {
        
        super();
    }
    
    public BlueChipProductOrderHandler(BlueChipProductOrderIntf source) {
        this.po = source;
    }

    public BlueChipProductOrderIntf getPo() {
        return this.po;
    }
    public void setPo(BlueChipProductOrderIntf po) {
        this.po = po;
    }
    public boolean getAllowDrawEdits() {
        return this.po.getAllowDrawEdits();
    }
    /**
     * 
     * @return  The opposite of the value in a Boolean object. This is used to disable the JSF component (readonly,disabled) 
     * 
     */
    public Boolean getAllowDrawEditsObj() {
        return new Boolean(!this.po.getAllowDrawEdits());
    }
    public int getBaseDraw() {
        return this.po.getBaseDraw();
    }
    public DateTime getDate() {
        return this.po.getDate();
    }

    public Date getStandardDate() {
        return this.po.getDate().toDate();
    }
    
    public Date getCutOffDateTime() {
    	Date cDate  = null;
    	
    	if (this.po.getAllowDrawEdits() && this.po.getCutOffDateTime() != null) {
    		if (!this.po.isPastCutOffDateTime()) {
    			cDate = this.po.getCutOffDateTime().toDate();
    		}
    	}
    	
    	return cDate;
    }

    public String getCutOffTimeStr() {
    	
    	String cutOffString = "";
    	try {
    		if (!this.po.getAllowDrawEdits()) {
    			if (this.po.getDraw() == -1) {
    				cutOffString = "--";
    			}
    			else {
    				cutOffString = "Expired";
    			}
    		}
    		else {
    			if (this.po.isTodayCutOffDay()) {
    				DateTime cutOff = this.po.getCutOffDateTime();

    				StringBuilder cString = new StringBuilder();
    				cString.append("<span style=\"color: red\"><b>");
    				//cString.append(cutOff.toString("h:mm a z"));
    				cString.append("Today by ");
    				cString.append(cutOff.toString("h:mma z"));
    				cString.append("</b></span>");
    				cutOffString = cString.toString();
    			}
    			else {
    				// future
    				DateTime cutOff = this.po.getCutOffDateTime();
    				
    				StringBuilder cString = new StringBuilder();
    				cString.append(cutOff.toString("M/d/yy"));
    				cString.append(" by ");
    				cString.append(cutOff.toString("h:mma z"));
    				cutOffString = cString.toString();
    			}
    		}
    		
    	}
    	catch (Exception e) {
    		cutOffString = "--";
		}
    	return cutOffString;
    }
    
    public String getDateStr() {
        String dateStr = "";
        if (this.po != null && this.po.getDate() != null) {
            dateStr = this.po.getDate().toString("MM/dd");
        }
        return dateStr;
    }
    public String getDayType() {
        return this.po.getDayType();
    }
    public int getDraw() {
        return this.po.getDraw();
    }
    
    public String getOriginalDrawStr() {
        String origDraw = "not set";
        if (this.po != null) {
            int od = this.po.getOriginalProductOrder().getDraw();
            if (od >= 0) {
                origDraw = String.valueOf(od);
            }
            
            DrawUpdateErrorIntf errors[] = this.po.getUpdateErrors();
            if (errors != null && errors.length > 0) {
                StringBuffer errDetBuf = new StringBuffer();
                
                for (int i = 0; i < errors.length; i++) {
                    
                    errDetBuf.append(errors[i].getErrorMessage()).append(": ");
                }
                origDraw += " Update Error Detail: " + errDetBuf.toString();
            }
        }
        return origDraw;
    }
    
    public Integer getDrawObj() {
        // -1 indicates the value has not been set so a null is 
        // returned to show the field as blank
        // return the draw as an Integer object or null if not set (-1)
        Integer dVal = null;
        if (this.po != null && this.po.getDraw() >= 0) {
            dVal = new Integer(this.getDraw());
        }
        return dVal;
    }
    
    public void setDrawObj(Integer dVal) {
        if (this.po != null && dVal != null) {
            int iValue = dVal.intValue();
            try {
                this.setDraw(iValue);
            } catch (Exception e) {
                // ignore failed sets since they would have been notified on the browser already
            }
        }
    }
  
    public BlueChipProductOrderWeekIntf getOwningWeek() {
        return this.po.getOwningWeek();
    }
    public boolean getIsChanged() {
        if (this.po != null) {
            return this.po.isChanged();
        }
        return false;
    }
    public boolean getIsNextDistributionDay() {
        if (this.po != null) {
            return this.po.isNextDistributionDate();
        }
        return false;
    }
    public void setDraw(int quantity) throws UsatException {
        this.po.setDraw(quantity);
        this.po.clearUpdateErrors();
    }
    
    public String getDrawCSS() {
        
        if (!this.po.getAllowDrawEdits()) {
            this.drawCSS += BlueChipProductOrderHandler.DISABLED_CSS;
        }
        else {
            if (this.po.isNextDistributionDate()) {
                this.drawCSS += " drawInputText inputText_CurrentDay";
            }
            else {
                this.drawCSS += " drawInputText";
            }
            
            if (this.po.getUpdateErrors() != null && this.po.getUpdateErrors().length > 0) {
                this.drawCSS += DRAW_ERR_CSS;
            }
        }
       
       return this.drawCSS;
    }
    
    public String getDrawCSSNoHighlight() {
        if (!this.po.getAllowDrawEdits()) {
            this.drawCSS += BlueChipProductOrderHandler.DISABLED_CSS;
        }
        else {
            this.drawCSS += " drawInputText";
        }

        if (this.po.getUpdateErrors() != null && this.po.getUpdateErrors().length > 0) {
            this.drawCSS += DRAW_ERR_CSS;
        }
       
       return this.drawCSS;
        
    }
    
    public int compareTo(BlueChipProductOrderHandler o) {
        // POs will be sorted by location id and po id
        BlueChipProductOrderHandler otherObj = o;
        
        String sourceSortString = this.po.getOwningLocation().getLocationName() + this.po.getProductOrderID();
        String otherSortString = otherObj.getPo().getOwningLocation().getLocationName() + otherObj.getPo().getProductOrderID();
        return sourceSortString.compareTo(otherSortString);
    }
    
    /**
     * 
     * @return
     */
    public String getFieldTitle() {
        int originalDraw = this.getPo().getOriginalProductOrder().getDraw();
        String title = "Original Quantity when page loaded: ";
        if (originalDraw == -1) {
            title += "blank";
        }
        else {
            title += originalDraw;
        }
        return title;
    }
    
    /**
     * 
     * @return
     */
    public String getHistoryHTML() {
        StringBuffer html = new StringBuffer("<table cellpadding=\"0\" cellspacing=\"1\" border=\"0\" width=\"145\">");
        
        BlueChipProductOrderWeekIntf owningWeek = this.po.getOwningWeek();
        BlueChipProductOrderWeekIntf prevWeek = owningWeek.getOwningMultiWeek().getWeekBefore(owningWeek);
        //
        //html.append("<tr><td colspan=\"2\" align=\"right\" class=\"outputText_SM\" background=\"/scweb/images/panel_title_bar_fill.gif\"><a href=\"javascript:void(0);\" onclick=\"return func_hideDrawHistory(this, event);\" onmouseover=\"hoverCloseImage();\" onmouseout=\"normalCloseImage();\"><img id=\"closeImg\" src=\"/scweb/images/close_panel.gif\" alt=\"Close History\"></a></td></tr>");
        html.append("<tr><td colspan=\"2\" align=\"center\" class=\"outputText_SM\" background=\"/scweb/images/panel_title_bar_fill.gif\">Recent History</td></tr>");
        html.append("<tr><th align=\"center\" bgcolor=\"#a8b8e0\"><b>Date</b></th><th align=\"center\" bgcolor=\"#a8b8e0\"><b>&nbsp; Qty &nbsp;</b></th></tr>");
        
        BlueChipProductOrderWeekIntf tempWeek = owningWeek;
        
        int numberAdded = 0;

        int todayInt = this.po.getDate().getDayOfWeek();
        String rowStyle = "rowClass1";
        while (todayInt > DateTimeConstants.MONDAY) {
            todayInt--;
            BlueChipProductOrderIntf po = tempWeek.getProductOrderForDay(todayInt);
            if (po != null) {
                int draw = po.getDraw();
                if (draw != -1) {
                    numberAdded++;
                    if ((numberAdded % 2)==0) {
                        rowStyle = "rowClass3";
                    }
                    else {
                        rowStyle = "rowClass1";
                    }
                    html.append("<tr class=\"").append(rowStyle).append("\"><td align=\"left\">&nbsp; ").append(po.getDate().toString("EEE, MMM dd"));
                    html.append("</td><td align=\"center\">").append(draw).append("</td></tr>");
                }
            }
        }
        
        if (prevWeek != null) {
            tempWeek = prevWeek;
            todayInt = DateTimeConstants.SUNDAY;
            while (todayInt > DateTimeConstants.MONDAY) {
                todayInt--;
                BlueChipProductOrderIntf po = tempWeek.getProductOrderForDay(todayInt);
                if (po != null) {
                    int draw = po.getDraw();
                    if (draw != -1) {
                        numberAdded++;
                        if ((numberAdded % 2)==0) {
                            rowStyle = "rowClass3";
                        }
                        else {
                            rowStyle = "rowClass1";
                        }
                        html.append("<tr class=\"").append(rowStyle).append("\"><td align=\"left\">&nbsp; ").append(po.getDate().toString("EEE, MMM dd"));
                        html.append("</td><td align=\"center\">").append(draw).append("</td></tr>");
                    }
                }
            }
        }
        
        if (numberAdded == 0) {
            html.append("<tr><td align=\"center\" class=\"outputTextAlt\" colspan=\"2\">No History Available</td></tr>");            
        }
        html.append("</table>");
        return html.toString();
    }
    
    public String getLocationInformation() {
        StringBuffer locInfo = new StringBuffer();
        locInfo.append(this.po.getOwningLocation().getLocationName());
        locInfo.append(" (").append(this.po.getOwningLocation().getLocationID()).append(")");
        
        return locInfo.toString();
    }
    
    public String getLongDescription() {
        StringBuffer buf = new StringBuffer(this.getPo().getProductName());
        buf.append(" (ID: ").append(this.getPo().getProductOrderID());
        buf.append(")");
        return buf.toString();
    }
    
    public String getOwningMarketPhoneFormatted() {
        String phone = "no phone number found";
        try {
	        if (this.po != null) {
	            StringBuffer phoneBuf = new StringBuffer();
	            String tempPhone = this.po.getOwningWeek().getOwningLocation().getOwningMarket().getRDLPhone();
	            
	            if (tempPhone != null && tempPhone.length() == 10) {
	                phoneBuf.append("(");
	                phoneBuf.append(tempPhone.substring(0,3));
	                phoneBuf.append(") ");
	                phoneBuf.append(tempPhone.substring(3,6));
	                phoneBuf.append("-");
	                phoneBuf.append(tempPhone.substring(6));
	            }
	            else {
	                phoneBuf.append(tempPhone);
	            }
	            phone = phoneBuf.toString();
	        }
        }
        catch (Exception e) {
            //  ignore it
            System.out.println("BlueChipProductOrderHandler:getOwningMarketPhoneFormatted() - " + e.getMessage());
            
        }
        return phone;
    }
    
}

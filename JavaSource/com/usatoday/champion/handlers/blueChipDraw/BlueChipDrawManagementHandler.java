/*
 * Created on Mar 19, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers.blueChipDraw;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import com.usatoday.singlecopy.model.bo.users.UserBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipLocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;

/**
 * @author aeast
 * @date Mar 19, 2008
 * @class BlueChipDrawManagementHandler
 * 
 * This class represents the list of blue chip locations (accounts) 
 * bound to an authenticated user.
 * 
 */
public class BlueChipDrawManagementHandler {

    private static int defaultStaleDataThreshold = 30;
    
    private ArrayList<BlueChipLocationHandler> blueChipLocations = null;
    private UserBO owningUser = null;
    
    private DateTime blueChipLocationLastLoadTime = null;
    
    
    
    /**
     * 
     */
    public BlueChipDrawManagementHandler() {
        super();
        
        this.blueChipLocations = new ArrayList<BlueChipLocationHandler>();

        this.blueChipLocationLastLoadTime = new DateTime();
        
    }

    public void setOwningUser(UserBO user) {
        this.clear();
        
        this.owningUser = user;
        
        this.wrapBlueChipLocations(user);
    }
    
    private void wrapBlueChipLocations (UserBO user) {

        try {
            if (user != null) {
		        Iterator<BlueChipLocationIntf> locations = user.getBlueChipLocations().values().iterator();
		        
		        while (locations.hasNext()) {
		            BlueChipLocationIntf loc = locations.next();
		            BlueChipLocationHandler locHandler = new BlueChipLocationHandler();
		    	    locHandler.setLocation(loc);
		    	    locHandler.setParentHandler(this);
		    	    this.blueChipLocations.add(locHandler);
		        }
            }
        }
        catch (Exception e) {
           System.out.println("BlueChipDrawManagementHandler::wrapBlueChipLocations() - Failed to set up blue chip locations for user: " + e.getMessage());
        }
    }
    
    public Collection<BlueChipLocationHandler> getBlueChipLocations() {
        ArrayList<BlueChipLocationHandler> aList = new ArrayList<BlueChipLocationHandler>(this.blueChipLocations);
        Collections.sort(aList);
        return aList;
    }
    public void setBlueChipLocations(Collection<BlueChipLocationHandler> blueChipLocations) {
        this.blueChipLocations = new ArrayList<BlueChipLocationHandler>(blueChipLocations);
    }

    public boolean getHasProductOrdersForCurrentTimeline() {
        boolean posAssigned = false;
        
        if (this.blueChipLocations != null && !this.blueChipLocations.isEmpty()) {
            posAssigned = true;
        }
        return posAssigned;
    }

    
    public boolean getHasNoProductOrdersForCurrentTimeline() {
        return !this.getHasProductOrdersForCurrentTimeline();
    }
    
    public Collection<BlueChipProductOrderHandler> getNextDistributionDateProductOrders() {

        ArrayList<BlueChipProductOrderHandler> pos = new ArrayList<BlueChipProductOrderHandler>();
        
        if (this.blueChipLocations != null) {
            Iterator<BlueChipLocationHandler> itr = this.blueChipLocations.iterator();
            while (itr.hasNext()) {
                BlueChipLocationHandler bclh = itr.next();
                Collection<BlueChipProductOrderHandler> posForLoc = bclh.getProductOrdersForNextDistributionDate();
                if (!posForLoc.isEmpty()) {
                    pos.addAll(posForLoc);
                }
            }
            
        }

        Collections.sort(pos);
        
        return pos;
    }

    
    public String getUnsavedChangesMessageCSS() {
        String messageCSS = "display: none";
        if (this.blueChipLocations != null) {
            Iterator<BlueChipLocationHandler> itr = this.blueChipLocations.iterator();
            while (itr.hasNext()) {
                BlueChipLocationHandler bclh = itr.next();
                Collection<BlueChipMultiWeekProductOrderIntf> tempCol = bclh.getLocation().getChangedProductOrders();
                if (tempCol.size() > 0) {
                    messageCSS = "display: inline";
                    break;
                }
            }
        }
        return messageCSS;
    }
    
    /**
     * 
     * @return true if this user has made blue chip draw changes otherwise false.
     */
    public boolean getHasUnsavedChanges() {
        if (this.blueChipLocations != null) {
            Iterator<BlueChipLocationHandler> itr = this.blueChipLocations.iterator();
            while (itr.hasNext()) {
                BlueChipLocationHandler bclh = itr.next();
                Collection<BlueChipMultiWeekProductOrderIntf> tempCol = bclh.getLocation().getChangedProductOrders();
                if (tempCol.size() > 0) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public Collection<BlueChipProductOrderIntf> getChangedProductOrders() {
        Collection<BlueChipProductOrderIntf> changedPOs = new ArrayList<BlueChipProductOrderIntf>();
        if (this.blueChipLocations != null) {
            Iterator<BlueChipLocationHandler> itr = this.blueChipLocations.iterator();
            while (itr.hasNext()) {
                BlueChipLocationHandler bclh = itr.next();
                Collection<BlueChipMultiWeekProductOrderIntf> tempCol = bclh.getLocation().getChangedProductOrders();
                if (tempCol != null && tempCol.size() > 0) {
                    Iterator<BlueChipMultiWeekProductOrderIntf> wItr = tempCol.iterator();
                    while (wItr.hasNext()) {
                        BlueChipMultiWeekProductOrderIntf mwpo = wItr.next();
                        Collection<BlueChipProductOrderWeekIntf> weeks = mwpo.getChangedWeeks();
                        Iterator<BlueChipProductOrderWeekIntf> weekItr = weeks.iterator();
                        while (weekItr.hasNext()) {
                            BlueChipProductOrderWeekIntf aWeek = weekItr.next();
                            for (int i = DateTimeConstants.MONDAY; i <= DateTimeConstants.SUNDAY; i++) {
                                BlueChipProductOrderIntf po = aWeek.getProductOrderForDay(i);
                                if (po.isChanged()) {
                                    changedPOs.add(po);
                                }
                                
                            }
                        }
                    }
                }
            }
        }
        return changedPOs;
    }

    public int getMinutesToStaleData() {
        DateTime now = new DateTime();
        
        if (this.blueChipLocationLastLoadTime == null) {
            return Integer.MAX_VALUE;
        }

        DateTime threshold = this.blueChipLocationLastLoadTime.plusMinutes(BlueChipDrawManagementHandler.getDefaultStaleDataThreshold());
        
        long millisecondsToCutOff = threshold.getMillis() - now.getMillis();
        
        if (millisecondsToCutOff < 0) {
            millisecondsToCutOff = 0;
        }
        
        long minutesToCutOff = millisecondsToCutOff / 60000;
        int minutes = 0;
        if (minutesToCutOff < Integer.MAX_VALUE) {
            minutes = Integer.parseInt(new String(Long.toString(minutesToCutOff)));
        }
        else {
            minutes = Integer.MAX_VALUE;
        }
        
        return minutes;
    }
    
    public void setMinutesToStaleData(int minutesToCutOff) {
        ;
    }
        
    public void setShowCountdownToStaleData(boolean val) {
        ;  // ignore the setting of this field
    }
    
    /**
     * Clean up references to objects and propogate the clear into the bo layer
     *
     */
    public void clear() {
        this.owningUser = null;
        if (this.blueChipLocations != null && !this.blueChipLocations.isEmpty()) {
            Iterator<BlueChipLocationHandler> itr = this.blueChipLocations.iterator();
            while (itr.hasNext()) {
                BlueChipLocationHandler bclh = itr.next();
                bclh.clear();
            }
            this.blueChipLocations.clear();
        }
    }
    
    public void clearUpdateErrors() {
        if (this.blueChipLocations != null && !this.blueChipLocations.isEmpty()) {
            Iterator<BlueChipLocationHandler> itr = this.blueChipLocations.iterator();
            while (itr.hasNext()) {
                BlueChipLocationHandler bclh = itr.next();
                bclh.getLocation().clearAllUpdateErrors();
            }
        }        
    }
    
    /**
     * 
     * @return  true if errors from a save occurred otherwise false;
     */
    public boolean getShowSaveFailedMessage() {
        boolean showErrorMessage = false;
        
        if (this.blueChipLocations != null && !this.blueChipLocations.isEmpty()) {
            Iterator<BlueChipLocationHandler> itr = this.blueChipLocations.iterator();
            Collection<DrawUpdateErrorIntf> errors = new ArrayList<DrawUpdateErrorIntf>();
            while (itr.hasNext()) {
                BlueChipLocationHandler bclh = (BlueChipLocationHandler)itr.next();
                
                errors = bclh.getLocation().getAllUpdateErrors();
                if (errors.size() > 0) {
                    showErrorMessage = true;
                    errors.clear();
                    break;
                }
            }
        }
        
        return showErrorMessage;
    }    
    
    public static int getDefaultStaleDataThreshold() {
        return defaultStaleDataThreshold;
    }
    public static void setDefaultStaleDataThreshold(int defaultValue) {
        if (defaultValue > 5 && defaultValue <=30) {
            BlueChipDrawManagementHandler.defaultStaleDataThreshold = defaultValue;            
        }
    }
    public DateTime getBlueChipLocationLastLoadTime() {
        return this.blueChipLocationLastLoadTime;
    }
    public void setBlueChipLocationLastLoadTime(DateTime staleDataTime) {
        this.blueChipLocationLastLoadTime = staleDataTime;
    }
    
    public boolean isPastStaleDataThreshold() {
        boolean timedOut = false;
        
        DateTime now = new DateTime();
        DateTime threshold = this.blueChipLocationLastLoadTime.plusMinutes(BlueChipDrawManagementHandler.getDefaultStaleDataThreshold()-1);
        if (now.isAfter(threshold)) {
            timedOut = true;
        }
        return timedOut;
    }
    
    public void refreshBlueChipLocations() {
        UserBO tempUser = this.owningUser;
        try {
            tempUser.reloadBlueChipLocations();
            // a call to setOwning User resets the attributes and rewraps locations
            this.setOwningUser(tempUser);
            this.blueChipLocationLastLoadTime = new DateTime();
        }
        catch (Exception e) {
            System.out.println("BlueChipDrawManagementHandler::refreshBlueChipLocations - FAILED: User: "+ tempUser.getEmailAddress() + "  MSG: " + e.getMessage());
        }
    }
}
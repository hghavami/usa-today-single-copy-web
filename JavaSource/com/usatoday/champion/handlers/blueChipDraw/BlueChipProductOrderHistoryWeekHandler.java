package com.usatoday.champion.handlers.blueChipDraw;

import java.util.Date;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderHistoryWeekIntf;

public class BlueChipProductOrderHistoryWeekHandler {

	public static final String DEFAULT_DISPLAY_VALUE =  "--";
	
    private BlueChipProductOrderHistoryWeekIntf week = null;

    public String getDrawMondayHistoryDisplay() {
    	String draw = BlueChipProductOrderHistoryWeekHandler.DEFAULT_DISPLAY_VALUE;
    	try {
	    	if (this.week.getMondayPO().getDraw() >= 0) {
	    		draw = Integer.toString(this.week.getMondayPO().getDraw());
	    	}
    	}catch (Exception e) {
    		; // ignore
		}
    	return draw;
    }

    public String getDrawTuesdayHistoryDisplay() {
    	String draw = BlueChipProductOrderHistoryWeekHandler.DEFAULT_DISPLAY_VALUE;
    	try {
	    	if (this.week.getTuesdayPO().getDraw() >= 0) {
	    		draw = Integer.toString(this.week.getTuesdayPO().getDraw());
	    	}
    	}catch (Exception e) {
    		; // ignore
		}
    	return draw;
    }

    public String getDrawWednesdayHistoryDisplay() {
    	String draw = BlueChipProductOrderHistoryWeekHandler.DEFAULT_DISPLAY_VALUE;
    	try {
	    	if (this.week.getWednesdayPO().getDraw() >= 0) {
	    		draw = Integer.toString(this.week.getWednesdayPO().getDraw());
	    	}
    	}catch (Exception e) {
    		; // ignore
		}
    	return draw;
    }

    public String getDrawThursdayHistoryDisplay() {
    	String draw = BlueChipProductOrderHistoryWeekHandler.DEFAULT_DISPLAY_VALUE;
    	try {
	    	if (this.week.getThursdayPO().getDraw() >= 0) {
	    		draw = Integer.toString(this.week.getThursdayPO().getDraw());
	    	}
    	}catch (Exception e) {
    		; // ignore
		}
    	return draw;
    }

    public String getDrawFridayHistoryDisplay() {
    	String draw = BlueChipProductOrderHistoryWeekHandler.DEFAULT_DISPLAY_VALUE;
    	try {
	    	if (this.week.getFridayPO().getDraw() >= 0) {
	    		draw = Integer.toString(this.week.getFridayPO().getDraw());
	    	}
    	}catch (Exception e) {
    		; // ignore
		}
    	return draw;
    }

    public String getDrawSaturdayHistoryDisplay() {
    	String draw = BlueChipProductOrderHistoryWeekHandler.DEFAULT_DISPLAY_VALUE;
    	try {
	    	if (this.week.getSaturdayPO().getDraw() >= 0) {
	    		draw = Integer.toString(this.week.getSaturdayPO().getDraw());
	    	}
    	}catch (Exception e) {
    		; // ignore
		}
    	return draw;
    }

    public String getDrawSundayHistoryDisplay() {
    	String draw = BlueChipProductOrderHistoryWeekHandler.DEFAULT_DISPLAY_VALUE;
    	try {
	    	if (this.week.getSundayPO().getDraw() >= 0) {
	    		draw = Integer.toString(this.week.getSundayPO().getDraw());
	    	}
    	}catch (Exception e) {
    		; // ignore
		}
    	return draw;
    }

    public String getDrawWeekTotalDisplay() {
    	String draw = BlueChipProductOrderHistoryWeekHandler.DEFAULT_DISPLAY_VALUE;
    	try {
	    	if (this.week.getWeeklyDrawTotal() >= 0) {
	    		draw = Integer.toString(this.week.getWeeklyDrawTotal());
	    	}
    	}catch (Exception e) {
    		; // ignore
		}
    	return draw;
    }
    
    public BlueChipProductOrderHistoryWeekIntf getWeek() {
		return week;
	}

	public void setWeek(BlueChipProductOrderHistoryWeekIntf week) {
		this.week = week;
	}
    
    public Date getWeekEndingDateObj() {
    	Date d = null;
    	if (this.week != null) {
    		d = this.week.getWeekEndingDate().toDate();
    	}
    	return d;
    }
    
}

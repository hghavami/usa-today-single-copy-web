/*
 * Created on Mar 9, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyDrawLocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf;

/**
 * @author aeast
 * @date Mar 9, 2007
 * @class WeeklyLocationHandler
 * 
 * 
 * 
 */
public class WeeklyLocationHandler {

    WeeklyDrawLocationIntf wdl = null;
    ArrayList<WeeklyProductOrderHandler> productOrders = new ArrayList<WeeklyProductOrderHandler>();
    /**
     * 
     */
    public WeeklyLocationHandler() {
        super();
    }

    public WeeklyLocationHandler(WeeklyDrawLocationIntf source) {
        this.wdl = source;
        
        // convert product orders to product order handlers
        Collection<ProductOrderIntf> poBO = source.getProductOrders();
        Iterator<ProductOrderIntf> itr = poBO.iterator();
        while (itr.hasNext()) {
            WeeklyProductOrderIntf wpo = (WeeklyProductOrderIntf)itr.next();
            WeeklyProductOrderHandler wpoh = new WeeklyProductOrderHandler(wpo);
            productOrders.add(wpoh);
        }
    }
    /**
     * @return Returns the locationAddress1.
     */
    public String getLocationAddress1() {
        return this.wdl.getAddress1();
    }
    /**
     * @param locationAddress1 The locationAddress1 to set.
     */
    public void setLocationAddress1(String locationAddress1) {
        //ignore
    }
    /**
     * @return Returns the locationID.
     */
    public String getLocationID() {
        //return this.wdl.getLocationID() + "   Seq: " + this.wdl.getSequenceNumber();
        return this.wdl.getLocationID();
    }
    /**
     * @param locationID The locationID to set.
     */
    public void setLocationID(String locationID) {
        // ignore
    }
    /**
     * @return Returns the locationName.
     */
    public String getLocationName() {
        return this.wdl.getLocationName();
    }
    /**
     * @param locationName The locationName to set.
     */
    public void setLocationName(String locationName) {
        // ignore
    }
    
    public String getLocationPhoneNumber() {
        return this.wdl.getPhoneNumber();
    }
    
    public String getLocationPhoneNumberFormatter() {
        String phone = this.getLocationPhoneNumber();
        StringBuffer fPhone = new StringBuffer("");
        if (phone != null && phone.length() == 10) {
            fPhone.append("(");
	        fPhone.append(phone.substring(0,3));
	        fPhone.append(") ");
	        fPhone.append(phone.substring(3,6));
	        fPhone.append("-");
	        fPhone.append(phone.substring(6));
        }
        return fPhone.toString();
    }
    
    public void setLocationPhoneNumber(String number) {
        // ignore
    }
    
    /**
     * @return Returns the productOrders.
     */
    public Collection<WeeklyProductOrderHandler> getProductOrders() {
        return this.productOrders;
    }
    /**
     * @param productOrders The productOrders to set.
     */
    public void setProductOrders(Collection<WeeklyProductOrderHandler> productOrders) {
        // ignore
    }
    /**
     * @return Returns the sequenceNumber.
     */
    public String getSequenceNumber() {
        String seqNumberStr = String.valueOf(wdl.getSequenceNumber()); 
        return seqNumberStr;
    }
    /**
     * @param sequenceNumber The sequenceNumber to set.
     */
    public void setSequenceNumber(String sequenceNumber) {
       //ignore
    }
    
    public WeeklyDrawLocationIntf getLocation() {
        return this.wdl;
    }
    
    public void clear() {
        this.wdl = null;
        if (this.productOrders != null && !this.productOrders.isEmpty()) {
            Iterator<WeeklyProductOrderHandler> itr = this.productOrders.iterator();
            while (itr.hasNext()) {
                WeeklyProductOrderHandler wpoh =  itr.next();
                wpoh.clear();
            }
            this.productOrders.clear();
        }
        this.productOrders = null;
    }
}

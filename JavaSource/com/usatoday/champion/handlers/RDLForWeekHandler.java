/*
 * Created on Mar 9, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductDescriptionIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.RouteDrawManagementIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyDrawLocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyDrawManangementIntf;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;
import com.usatoday.singlecopy.model.util.ProductDescriptionCache;

/**
 * @author aeast
 * @date Mar 9, 2007
 * @class RDLForWeekHandler
 * 
 * This class is a proxy class for the weekly draw management business objects.
 * It exists to facilitate working with JSF U/I Components.
 * 
 */
public class RDLForWeekHandler {

    private DateTime weekEndingDate = null;
    private Collection<WeeklyLocationHandler> locations = new ArrayList<WeeklyLocationHandler>();
    private String publicationCode = null;
    WeeklyDrawManangementIntf wdm = null;
    private String routeID = null;
    private String errorDetail = "";
    
    private boolean saveInProgress = false;
    
    
    /**
     * 
     */
    public RDLForWeekHandler() {
        super();

    }
    
    public RDLForWeekHandler(DateTime d, String pubCode, RouteDrawManagementIntf route, UserIntf user, boolean returnsOnly) {
        super();

        this.publicationCode = pubCode;        
        
        try {
            
            this.wdm = route.retrieveWeeklyDraw(d, pubCode, user, returnsOnly);
            this.weekEndingDate = this.wdm.getWeekEndingDate();
            this.routeID = this.wdm.getRoute().getRouteID();
            Collection<LocationIntf> locationBOs = this.wdm.getLocations();
            Iterator<LocationIntf> locItr = locationBOs.iterator();
            
            while (locItr.hasNext()) {
                WeeklyDrawLocationIntf wdl = (WeeklyDrawLocationIntf)locItr.next();
                WeeklyLocationHandler wlh = new WeeklyLocationHandler(wdl);
                this.locations.add(wlh);
            }
            
        }
        catch(UsatException ue) {
            //ue.printStackTrace();
        }
    }
    /**
     * @return Returns the weeklyDrawEndDates.
     */
    public Collection<WeeklyLocationHandler> getLocations() {
        return this.locations;
    }
    /**
     * @param weeklyDrawEndDates The weeklyDrawEndDates to set.
     */
    public void setLocations(Collection<WeeklyLocationHandler> locations) {
        this.locations = locations;
    }
    /**
     * @return Returns the routeID.
     */
    public String getRouteID() {
        return this.routeID;
    }
    /**
     * @param routeID The routeID to set.
     */
    public void setRouteID(String routeID) {
        // ignore, read only
    }
    /**
     * @return Returns the weekEndingDate.
     */
    public Date getWeekEndingDate() {
        return this.weekEndingDate.toDate();
    }

    public String getMondayDateString() {
        DateTime dt = new DateTime(this.weekEndingDate);
        return dt.minusDays(6).toString("MM/dd");
    }
    public String getTuesdayDateString() {
        DateTime dt = new DateTime(this.weekEndingDate);
        return dt.minusDays(5).toString("MM/dd");
    }
    public String getWednesdayDateString() {
        DateTime dt = new DateTime(this.weekEndingDate);
        return dt.minusDays(4).toString("MM/dd");
    }
    public String getThursdayDateString() {
        DateTime dt = new DateTime(this.weekEndingDate);
        return dt.minusDays(3).toString("MM/dd");
    }
    public String getFridayDateString() {
        DateTime dt = new DateTime(this.weekEndingDate);
        return dt.minusDays(2).toString("MM/dd");
    }
    public String getSaturdayDateString() {
        DateTime dt = new DateTime(this.weekEndingDate);
        return dt.minusDays(1).toString("MM/dd");
    }    
    public String getSundayDateString() {
        DateTime dt = new DateTime(this.weekEndingDate);
        return dt.toString("MM/dd");
    }    
    /**
     * @param weekEndingDate The weekEndingDate to set.
     */
    public void setWeekEndingDate(Date weekEndingDate) {
        this.weekEndingDate = new DateTime(weekEndingDate.getTime());
    }
    
    /**
     * @return Returns the publicationCode.
     */
    public String getPublicationCode() {
        return this.publicationCode;
    }
    /**
     * @param publicationCode The publicationCode to set.
     */
    public void setPublicationCode(String publicationCode) {
        this.publicationCode = publicationCode;
    }
    
    public Integer getTotalDraw() {
        Integer totDraw = null;
        if (this.wdm != null) {
            totDraw = new Integer(this.wdm.getTotalDraw());
        }
        else {
            totDraw = new Integer(0);
        }
        return totDraw;
    }
    
    public Integer getOriginalTotalDraw() {
        Integer totDraw = null;
        if (this.wdm != null) {
            totDraw = new Integer(this.wdm.getOriginalTotalDraw());
        }
        else {
            totDraw = new Integer(0);
        }
        return totDraw;
    }

    public Integer getTotalReturns() {
        Integer totReturns = null;
        if (this.wdm != null) {
            totReturns = new Integer(this.wdm.getTotalReturns());
        }
        else {
            totReturns = new Integer(0);
        }
        return totReturns;
    }
    
    public Integer getOriginalTotalReturns() {
        Integer totReturns = null;
        if (this.wdm != null) {
            totReturns = new Integer(this.wdm.getOriginalTotalReturns());
        }
        else {
            totReturns = new Integer(0);
        }
        return totReturns;
    }
    /**
     * @return Returns the wdm.
     */
    public WeeklyDrawManangementIntf getWdm() {
        return this.wdm;
    }

    /**
     * 
     * @return A description of the product order type
     */
    public String getProductDescription() {
        String description = "";
        
        ProductDescriptionCache pdCache = ProductDescriptionCache.getInstance();
        
        ProductDescriptionIntf pd = pdCache.getProductDesription(this.getPublicationCode());
        
        description = pd.getDescription();
        
        return description;
    }
    
    public String getLateReturnsString() {
        if (wdm.getLateReturnsFlag()) {
            
            return "";
        }
        else {
            return "display: none";
        }
    }
    
    public String getEdittableFieldsCSS() {
        if (wdm.hasEdittableFields()) {
            return "display: none";
        }
        else {
            return "";
        }
    }
    
    public String getSaveFailedMessage() {
        String msg = "";
        
        if (wdm.getUpdateErrorsFlag()) {
            msg = "Not all product orders were updated. Please correct any errors indicated below and submit again. "  + wdm.getGlobalUpdateErrorMessage() + " " + errorDetail;
        }
        return msg;
    }
    
	/**
	 * 
	 * @return
	 */
    public Integer getFirstRowToDisplay() {
        Integer indexVal = null;

        int index = 0;
        
        if (wdm.getUpdateErrorsFlag()) { 
            Collection<LocationIntf> c = wdm.getLocations();
            Iterator<LocationIntf> itr = c.iterator();
            boolean done = false;
            while(!done && itr.hasNext()) {
                LocationIntf l = itr.next();
                Collection<ProductOrderIntf> poCol = l.getProductOrders();
                Iterator<ProductOrderIntf> poItr = poCol.iterator();
                while (poItr.hasNext()) {
                    ProductOrderIntf po = poItr.next();
                    if (po.getUpdateErrors() != null && po.getUpdateErrors().length > 0) {
                        done = true;
                        break;
                    }
                }
                if (!done) {
                    index++;
                }
            }
        }
        indexVal = new Integer(index);
        return indexVal;
    }
    
    
    // resets error processing state on handler
    // so after a commit to 400 the error states are recalculated in the handler.
    public void resetErrorProcessing() {
        Iterator<WeeklyLocationHandler> itr = this.locations.iterator();
        while(itr.hasNext()) {
            WeeklyLocationHandler wlh = itr.next();
            Iterator<WeeklyProductOrderHandler> itr2 = wlh.getProductOrders().iterator();
            while (itr2.hasNext()) {
                WeeklyProductOrderHandler wpo = itr2.next();
                wpo.resetErrorsProcessed();
            }
        }
        
    }
    /**
     * @return Returns the errorDetail.
     */
    public String getErrorDetail() {
        return this.errorDetail;
    }
    /**
     * @param errorDetail The errorDetail to set.
     */
    public void setErrorDetail(String errorDetail) {
        this.errorDetail = errorDetail;
    }
    
    public void clear() {
        try {
	        if (wdm != null) {
	            wdm.clear();
	            wdm = null;
	        }
	        if (this.locations != null && !this.locations.isEmpty()) {
	            Iterator<WeeklyLocationHandler> itr = this.locations.iterator();
	            while (itr.hasNext()) {
	                WeeklyLocationHandler loch = itr.next();
	                loch.clear();
	            }
	            this.locations.clear();
	        }
	        this.locations = null;
        }
        catch (Exception e) {
            ;
        }
    }
	/**
	 * @return Returns the saveInProgress.
	 */
	public boolean getSaveInProgress() {
		return saveInProgress;
	}
	/**
	 * @param saveInProgress The saveInProgress to set.
	 */
	public void setSaveInProgress(boolean saveInProgress) {
		this.saveInProgress = saveInProgress;
	}
}

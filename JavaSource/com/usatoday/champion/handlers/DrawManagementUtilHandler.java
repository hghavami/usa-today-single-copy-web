/*
 * Created on Oct 10, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.bo.drawmngmt.DrawManagementUtilBO;

/**
 * @author aeast
 * @date Oct 10, 2007
 * @class DrawManagementUtilHandler
 * 
 * 
 * 
 */
public class DrawManagementUtilHandler {

    private DrawManagementUtilBO dmu = null;
    
    /**
     * 
     */
    public DrawManagementUtilHandler() {
        super();
        this.dmu = new DrawManagementUtilBO();
    }

    /**
     * 
     * @return The date (string) for the current distribution date
     */
    public String getDistributionDateString() {
        String distDate = "unknown";

        try {
            DateTime dDate = dmu.getDistributionDate();
            distDate = dDate.toString("MM/dd/yyyy");
        } catch (UsatException ue) {
            System.out.println("Unable to determine distribution date!: " + ue.getMessage());
        }
        return distDate;
    }

    /**
     * 
     * @return The date that should be used as the default value for RDL Date
     */
    public String getDefaultRDLDateString() {
        String rdlDate = "unknown";

        try {
            DateTime dDate = dmu.getDefaultRDLDate();
            rdlDate = dDate.toString("MM/dd/yyyy");
        } catch (UsatException ue) {
            System.out.println("Unable to determine default RDL date!: " + ue.getMessage());
        }
        return rdlDate;
    }
}

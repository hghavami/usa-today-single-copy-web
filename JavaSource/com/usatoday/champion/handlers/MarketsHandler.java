/*
 * Created on Aug 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers;

import java.util.Collection;

import com.usatoday.singlecopy.model.interfaces.markets.MarketIntf;
import com.usatoday.singlecopy.model.util.MarketCache;

/**
 * @author aeast
 * @date Aug 2, 2007
 * @class MarketsHandler
 * 
 * 
 * 
 */
public class MarketsHandler {

    /**
     * 
     */
    public MarketsHandler() {
        super();
    }

    
    /**
     * @return Returns the markets.
     */
    public Collection<MarketIntf> getMarkets() {
        return MarketCache.getInstance().getMarkets();
    }
    /**
     * @param markets The markets to set.
     */
    public void setMarkets(Collection<MarketIntf> markets) {
        ;
    }
}

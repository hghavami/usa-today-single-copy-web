/*
 * Created on Feb 22, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.bo.drawmngmt.DrawManagementUtilBO;
import com.usatoday.singlecopy.model.bo.drawmngmt.RouteBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.BroadcastMessageIntf;
import com.usatoday.singlecopy.model.transferObjects.BroadcastMessageTO;

/**
 * @author aeast
 * @date Feb 22, 2007
 * @class RouteHandler
 * 
 * This class wraps the routeBO class for use in JSF components.
 * 
 */
public class RouteHandler {

    private RouteBO route = null;
    
    private DateTime lastBroadcastMessagePull = null;
    
    private Collection<BroadcastMessageIntf> broadcastMessages = null;
    
    private int numberBroadcastMessages = 0;
    
    private DateTime distributionDate = null;
    
    // temporary variable used for component RDL Selected Day
    private Date selectedRDLDate = null;
    /**
     * 
     */
    public RouteHandler() {
        super();
    }
    

    /**
     * @return Returns the route.
     */
    public RouteBO getRoute() {
        return this.route;
    }
    /**
     * @param route The route to set.
     */
    public void setRoute(RouteBO route) {
        this.route = route;
        DrawManagementUtilBO dmuBO = new DrawManagementUtilBO();
        try {
            this.distributionDate = dmuBO.getDistributionDate();
            this.selectedRDLDate = dmuBO.getDefaultRDLDate().toDate();
        }
        catch (UsatException ue) {
            this.distributionDate = route.getRDLEndDate();
        }
        
        if (this.broadcastMessages != null) {
            this.broadcastMessages.clear();
        	this.broadcastMessages = null;
        }
        this.numberBroadcastMessages = 0;
    }
    
    public Date getRDLBeginDateAsObject() {
        if (this.route != null) {
            return this.route.getRDLBeginDate().toDate();
        }
        return null;
    }
    
    public String getRDLBeginDateAsString() {
        if (this.route != null) {
            return this.route.getRDLBeginDate().toString("YYYY/MM/dd");
        }
        return "";
    }

    public Date getRDLEndDateAsObject() {
        if (this.route != null) {
            return this.route.getRDLEndDate().toDate();
        }
        return null;
    }
    
    public String getRDLEndDateAsString() {
        if (this.route != null) {
            return this.route.getRDLEndDate().toString("YYYY/MM/dd");
        }
        return "";
    }
    
    public Collection<String> getPublicationsWithWeeklyEndDates() {
        Collection<String> publications = null;
        try {
            publications = this.route.getWeeklyDrawPublications();
        } catch (Exception ue) {
            // in the event of an error return an empty array
            publications = new ArrayList<String>();
        }
        return publications;
    }
    
    public Date getWeeklyBeginDateAsObject() {
        if (this.route != null) {
            return this.route.getWeeklyDrawBeginDate().toDate();
        }
        return null;
    }
    
    public String getWeeklyBeginDateAsString() {
        if (this.route != null) {
            return this.route.getWeeklyDrawBeginDate().toString("YYYY/MM/dd");
        }
        return "";
    }

    public Date getWeeklyEndDateAsObject() {
        if (this.route != null) {
            return this.route.getWeeklyDrawEndDate().toDate();
        }
        return null;
    }
    
    public String getWeeklyEndDateAsString() {
        if (this.route != null) {
            return this.route.getWeeklyDrawEndDate().toString("YYYY/MM/dd");
        }
        return "";
    }

    public Collection<WeeklyEndDatesHandler> getWeeklyEndDatesHandler() {
        Collection<WeeklyEndDatesHandler> c = new ArrayList<WeeklyEndDatesHandler>();
        Collection<String> source = this.getPublicationsWithWeeklyEndDates();
        Iterator<String> itr = source.iterator();
        
        DateTime defaultSelectedDate = this.distributionDate;
        int dayOfWeekAdj = 7 - defaultSelectedDate.getDayOfWeek();
        defaultSelectedDate = defaultSelectedDate.plusDays(dayOfWeekAdj);
        
        while (itr.hasNext()){
            String pub = itr.next();
            WeeklyEndDatesHandler wedh = new WeeklyEndDatesHandler();
            wedh.setPub(pub);
            wedh.setEarliestDate(this.getWeeklyBeginDateAsObject());
            wedh.setLatestDate(this.getWeeklyEndDateAsObject());
            wedh.setSelectedDate(defaultSelectedDate.toDate());

            c.add(wedh);
        }
        return c;
    }
    /**
     * @return Returns the selectedRDLDate.
     */
    public Date getSelectedRDLDate() {
        return this.selectedRDLDate;
    }
    /**
     * @param selectedRDLDate The selectedRDLDate to set.
     */
    public void setSelectedRDLDate(Date selectedRDLDate) {
        this.selectedRDLDate = selectedRDLDate;
    }
    
    public Collection<BroadcastMessageIntf> getRouteBroadcastMessages() {
        Collection<BroadcastMessageIntf> messages = null;
        try {
            // cache broadcast messages for at most two minutes
            if (this.broadcastMessages == null || 
                    this.lastBroadcastMessagePull.plusMinutes(2).isBeforeNow()) {
                this.broadcastMessages = this.route.getRouteBroadcastMessages();
                this.lastBroadcastMessagePull = new DateTime();
                
            }
            messages = this.broadcastMessages;
        }
        catch (Exception ue) {
            // send alert?
            //System.out.println("Exception pulling route messages: " + ue.getMessage());
        }
        
        if(messages == null ) {
            messages = new ArrayList<BroadcastMessageIntf>();
        }
        
        // add a message to indicate that there are no messages.
        if (messages.size() == 0) {
            BroadcastMessageTO bMessage = new BroadcastMessageTO();
            bMessage.setMarketID(this.getRoute().getMarketID());
            bMessage.setDistrictID(this.getRoute().getRouteID());
            bMessage.setRouteID(this.getRoute().getRouteID());
            bMessage.setMessageID("NOMSG");
            bMessage.setMessage("No Messages for Route " + bMessage.getRouteID());
            messages.add(bMessage);
            this.numberBroadcastMessages = 0;
        }
        else {
            this.numberBroadcastMessages = messages.size();
            
        }
        
        return messages;
    }
    /**
     * @return Returns the numberBroadcastMessages.
     */
    public String getNumberBroadcastMessages() {
        if (this.broadcastMessages == null || 
                this.lastBroadcastMessagePull.plusMinutes(2).isBeforeNow()) {
            this.getRouteBroadcastMessages();
        }
        return String.valueOf(this.numberBroadcastMessages);
    }
    
    public String getMarketDistRouteKey() {
    	if (this.route != null) {
    		return this.route.getMarketID() + this.route.getDistrictID() + this.route.getRouteID();
    	}
    	else {
    		return "";
    	}
    }

	public void setMarketDistRouteKey(String value) {
		;
	}
}

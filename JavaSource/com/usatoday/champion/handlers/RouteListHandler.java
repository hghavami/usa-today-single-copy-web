/*
 * Created on Feb 20, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.champion.handlers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import javax.faces.model.SelectItem;

import com.usatoday.singlecopy.NotAuthorizedException;
import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.bo.drawmngmt.RouteBO;
import com.usatoday.singlecopy.model.bo.users.UserBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf;

/**
 * @author aeast
 * @date Feb 20, 2007
 * @class RouteListHandler
 * 
 * This class is used to hold a refrence to the routes associated with a user. 
 * It is used in conjunction with JSF U/I Components.
 * 
 */
public class RouteListHandler {

    private HashMap<String, RouteIntf> routes = null;
    private UserBO owningUser = null;
    
    private ArrayList<SelectItem> dropDownView = null;
    private ArrayList<RouteBO> sortedView = null;
    
    /**
     * 
     */
    public RouteListHandler() {
        super();
        //System.out.println(".........Creating a new RouteListHandler......");
    }

    
    /**
     * @return Returns the routes.
     */
    public HashMap<String, RouteIntf> getRoutes() {
        
        if (this.routes != null) {
            return this.routes;
        }
        if (this.owningUser != null) {
            try {
                this.routes = this.owningUser.getRoutes();
            }
            catch (Exception e) {
               ;
            }
            if (this.routes == null) {
                this.routes = new HashMap<String, RouteIntf>();
            }
        }
        return this.routes;
    }
    
    /**
     * 
     * @return
     */
    public ArrayList<RouteBO> getRoutesAsCollection() {
    	
        if (this.sortedView == null) {
            ArrayList<RouteIntf> aList = new ArrayList<RouteIntf>(this.getRoutes().values());
            
            this.sortedView = new ArrayList<RouteBO>();
            
            for (RouteIntf r : aList) {
            	RouteBO rbo = (RouteBO)r;
            	this.sortedView.add(rbo);
            }
            
            Collections.sort(this.sortedView);
        }

        return this.sortedView;
    }
    
    /**
     * 
     * @return
     */
    public ArrayList<SelectItem> getRoutesAsCollectionForDropDownSelection() {
        
        if (this.dropDownView == null) {
            this.dropDownView = new ArrayList<SelectItem>();
            if (this.getRoutes().size() > 0) {
	            Iterator<RouteIntf> itr = this.getRoutes().values().iterator();
	            while (itr.hasNext()) {
	            	RouteIntf r = itr.next();
	                SelectItem si = new javax.faces.model.SelectItem(r, r.getRouteID());
	                this.dropDownView.add(si);
	            }
            }
        }
        return this.dropDownView;
    }
    /**
     * @param routes The routes to set.
     */
    public void setRoutes(HashMap<String, RouteIntf> routes) {
        this.routes = routes;
    }
    /**
     * @return Returns the owningUser.
     */
    public UserBO getOwningUser() {
        return this.owningUser;
    }
    /**
     * @param owningUser The owningUser to set.
     */
    public void setOwningUser(UserBO owningUser) {
        this.owningUser = owningUser;
       // this.routes = null; may need this.???
    }
    
    /**
     * 
     * @return
     */
    public int getNumberOfRoutes() {
        return this.getRoutes().size();
    }
    
    /**
     * 
     * @param key The route id
     * @return
     */
    public RouteBO getRoute(Object key) {
        RouteBO r = null;
        try {
            r = this.owningUser.getRoute(key);
        }
        catch (NotAuthorizedException na) {
            System.out.println("Not Authorized");
        }
	    catch (UsatException ue) {
	        System.out.println("UsatException: " + ue.getMessage());
	    }
	    catch (Exception e) {
	        System.out.println("Exception: " + e.getMessage());
	    }
        return r;
    }
    
    public void cleanUp() {
        if (this.dropDownView != null) {
            this.dropDownView.clear();
        }
        if (this.sortedView != null) {
            this.sortedView.clear();
        }
        this.routes = null;
        this.owningUser = null;
    }
    
    public boolean getHasRoutesAssigned() {
    	HashMap<String, RouteIntf> theRoutes = this.getRoutes();
    	if (theRoutes != null && theRoutes.size() > 0) {
    		return true;
    	}
    	return false;
    }
    
    
}

package com.usatoday.champion.web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.singlecopy.model.interfaces.users.AccessRightsIntf;

/**
 * 
 * @author aeast
 * @date May 18, 2007
 * @class AccessRightsFilter
 * 
 * Currently this filter ensures that request to for draw management ar only made by 
 * users assigned access to routes
 * 
 * As new features are added, we can edit this to check for those paths as well.
 *
 */
public class AccessRightsFilter implements Filter {
	/* (non-Java-doc)
	 * @see java.lang.Object#Object()
	 */
	public AccessRightsFilter() {
		super();
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#init(FilterConfig arg0)
	 */
	public void init(FilterConfig arg0) throws ServletException {
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
	 */
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {

	    if (req instanceof HttpServletRequest && resp instanceof HttpServletResponse) {
	        HttpServletRequest request = (HttpServletRequest)req;
	        HttpServletResponse response = (HttpServletResponse)resp;
	        UserHandler user = (UserHandler)request.getSession().getAttribute("userHandler");
	        
	        String notAuthorizedURL = "/scweb/notAuthorized.usat";
	        
	        String reqURL = request.getRequestURL().toString();
	        if ((reqURL.indexOf("/drawManagement") > -1) && !user.getUser().hasAccess(AccessRightsIntf.ROUTE_DRAW_MNGMT)) {
		        response.sendRedirect(response.encodeRedirectURL(notAuthorizedURL));
		        return;
	        }
	        else if ((reqURL.indexOf("/bcd") > -1) && !user.getUser().hasAccess(AccessRightsIntf.BLUE_CHIP_DRAW)) {
		        response.sendRedirect(response.encodeRedirectURL(notAuthorizedURL));
		        return;
	        }
	        else if ((reqURL.indexOf("/usatadmin") > -1) && (user.getUser().getEmailAddress().indexOf("USATODAY.COM") < 0)) {
		        response.sendRedirect(response.encodeRedirectURL(notAuthorizedURL));
		        return;
	        }
	    }
	    
		chain.doFilter(req, resp);
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
	}

}
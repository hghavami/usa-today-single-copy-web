package com.usatoday.champion.web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.gannett.usat.singlecopy.web.portal.actions.SiteUpCheckAction;
import com.usatoday.champion.handlers.ApplicationFlagHandler;
import com.usatoday.gannett.singlecopy.util.AS400CurrentStatus;
import com.usatoday.gannett.singlecopy.util.AS400StatusMonitor;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

public class AS400StatusFilter implements Filter {
	/* (non-Java-doc)
	 * @see java.lang.Object#Object()
	 */
	
    private AS400StatusMonitor as400Monitor = null;
    private ApplicationFlagHandler applicationMode = null;
    private ServletContext context = null;
    
	public AS400StatusFilter() {
		super();
		this.as400Monitor = new AS400StatusMonitor();
		
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#init(FilterConfig arg0)
	 */
	public void init(FilterConfig config) throws ServletException {
		// set check frequency if set on servlet config;

	    String frequencyStr = config.getInitParameter("CheckFrequency");
	    this.context = config.getServletContext();
	    
	    try {
	        long freq = Long.parseLong(frequencyStr);
	        if (freq > 999) {
	            USATApplicationConstants.backEndCheckFrequency = freq;
	        }
	        else {
	            System.out.println("Invalid AS/400 Check Frequencey specified. Must be at least 999, using default value.");
	        }
	    }
	    catch(Exception e) {
	        // use default frequency of 1 minute
	    }
		this.as400Monitor.start();
		SiteUpCheckAction.iSeriesMonitor = this.as400Monitor;
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
	 */
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
	    
		
		try {
			if (this.applicationMode == null && this.context != null) {
				this.applicationMode = (ApplicationFlagHandler)context.getAttribute("applicationModeFlag");
			}
		}
		catch (Exception exp) {
			System.out.println("Faile to get maintenance mode object");
		}
		boolean inMaintenanceMode = false;
		if (this.applicationMode != null) {
			inMaintenanceMode = this.applicationMode.getBooleanValue();
		}
		
	    if (AS400CurrentStatus.getJdbcActive() && !inMaintenanceMode) {
	        chain.doFilter(req, res);
	    }
	    else {
	        // forward to System Not Available page
	        req.getRequestDispatcher("/siteNotAvailable.usat").forward(req, res);
	    }
	    
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
	    this.as400Monitor.setKeepMonitoring(false);
	    this.as400Monitor.interrupt();
	}
	
}
package com.usatoday.champion.web.filters;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;

public class AuthenticationFilter implements Filter {
	/* (non-Java-doc)
	 * @see java.lang.Object#Object()
	 */
	public AuthenticationFilter() {
		super();
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#init(FilterConfig arg0)
	 */
	public void init(FilterConfig arg0) throws ServletException {
		
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
	    if (request instanceof HttpServletRequest) {
	        HttpServletRequest req = (HttpServletRequest)request;
	        Object o = req.getSession().getAttribute("userHandler");
	        UserHandler uh = null;
	        
	        if (o != null && o instanceof UserHandler) {
	            uh = (UserHandler)o;
	        }
	        UserIntf user = null;
	        if (uh != null) {
	            user = uh.getUser();
	        }
	        
	        boolean sendToLoginPage = false;
	        if (user == null) {
	            sendToLoginPage = true;
	        }
	        else {
	            // check if user has only auto logged in.
	            if (uh.getAutoAuthOnly()) {
	                String requestedPage = req.getRequestURI();
	                if (requestedPage.indexOf("sc_secure/bcd/distributionDayDraw") >= 0) {
	                    sendToLoginPage = false;
	                }
	                else if (requestedPage.indexOf("sc_secure/bcd/order_history") >= 0) {
	                    sendToLoginPage = false;
	                }
	                else if (requestedPage.indexOf("sc_secure/bcd/bcdHelp") >= 0) {
	                	sendToLoginPage = false;
	                }
	                else {
	                    // need to login
	                    sendToLoginPage = true;
	                }
	            }
	            else {
	                if (!user.isUserAuthenticated()){
	                    sendToLoginPage = true;
	                }
	            }
	        }
	        
	        
	        if (sendToLoginPage) {
	            // redirect to login page
	            FacesContext context = FacesContext.getCurrentInstance();
	            if (context != null) {
		            FacesMessage facesMsg = 
		                new FacesMessage(FacesMessage.SEVERITY_INFO, "Please login to continue.", null);
		            context.addMessage(null, facesMsg);
	            }
	            else {
	                req.getSession().setAttribute("SESS_EXPIRED", "Please login to continue.");
	            }
	            HttpServletResponse res = (HttpServletResponse)response;
	            res.sendRedirect("/scweb/index.usat");
	            //req.getRequestDispatcher("/index.usat").forward(request, response);
	        }
	        else {
	            req.getSession().removeAttribute("SESS_EXPIRED");
	            chain.doFilter(request, response);
	        }
	    }
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
	}

}
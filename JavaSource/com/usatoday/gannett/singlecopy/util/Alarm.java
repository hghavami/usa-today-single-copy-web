/*
 * Created on Jul 23, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.gannett.singlecopy.util;

import java.util.ArrayList;

import org.joda.time.DateTime;

/**
 * @author aeast
 * @date Jul 23, 2007
 * @class Alarm
 * 
 * 
 * 
 */
public class Alarm {

    private DateTime alarmSent = null;
    private Integer alarmID = new Integer(-1);
    private ArrayList<String> messages = new ArrayList<String>();
    private String subject = "Single Copy Web Alarm";
    
    /**
     * 
     */
    public Alarm() {
        super();
        this.alarmSent = new DateTime();
    }

    /**
     * @return Returns the alarmID.
     */
    public Integer getAlarmID() {
        return this.alarmID;
    }
    /**
     * @param alarmID The alarmID to set.
     */
    public void setAlarmID(Integer alarmID) {
        this.alarmID = alarmID;
    }
    /**
     * @return Returns the alarmSent.
     */
    public DateTime getAlarmSent() {
        return this.alarmSent;
    }
    /**
     * @param alarmSent The alarmSent to set.
     */
    public void setAlarmSent(DateTime alarmSent) {
        this.alarmSent = alarmSent;
    }
    /**
     * @return Returns the messages.
     */
    public ArrayList<String> getMessages() {
        return this.messages;
    }
    /**
     * @param messages The messages to set.
     */
    public void setMessages(ArrayList<String> messages) {
        this.messages = messages;
    }
    
    public void addMessage(String msg) {
        this.messages.add(msg);
    }
    
    public void clearMessages() {
        this.messages.clear();
    }
    /**
     * @return Returns the subject.
     */
    public String getSubject() {
        return this.subject;
    }
    /**
     * @param subject The subject to set.
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }
}

/*
 * Created on Jul 12, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.gannett.singlecopy.util;

import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author aeast
 * @date Jul 12, 2007
 * @class OmnitureTrackingCode
 * 
 * 
 * 
 */
public class OmnitureTrackingCode {

    private boolean omnitureActive = true;
    private java.lang.String omnitureScriptLocation = "";
    private String serverName = "unknown";
    

    
    /**
     * 
     */
    public OmnitureTrackingCode() {
        super();
        
    	try {
    		java.net.InetAddress serverAddress = java.net.InetAddress.getLocalHost();
    		serverName = serverAddress.getHostName();
    		serverName = serverName.toUpperCase();
    		if (serverName.indexOf("MOC-") > -1) {
    		    // production
    		    this.omnitureScriptLocation = "/scweb/scripts/s_code.js";
    		}
    		else {
    		    // dev/test
    		    this.omnitureScriptLocation = "/scweb/scripts/s_code_dev.js";
    		}
    		
   		    this.omnitureActive = USATApplicationConstants.webAnalyticsActive; 
    	}
    	catch (Exception e) {
    		
    	}
        
    }

    
    public String getTrackingCodePart1() {
        return this.getOmnitureTrackingCodeVersionH23_Part1();
    }

    public String getTrackingCodePart2() {
    	return this.getOmnitureTrackingCodeVersionH23_Part2();
    }

    @SuppressWarnings("unused")
	private String getOmnitureTrackingCodeVersionH15_Part1() {
    	StringBuffer oStr = new StringBuffer("<!-- SiteCatalyst code version: H.15.1. \n" +
    	    	"Copyright 1997-2008 Omniture, Inc. More info available at\n" +
    	    	"   http://www.omniture.com --> \n" +
    	    	"<script language=\"JavaScript\" type=\"text/javascript\" src=\"" +
    	    	this.omnitureScriptLocation +
    	    	"\"></script> \n" +
    	    	" <script language=\"JavaScript\" type=\"text/javascript\"><!-- \n" +
    	    	"/* You may give each page an identifying name, server, and channel on " +
    	    	" the next lines. */ \n");
    	    	
		    	oStr.append("s.pageName=\"\"\n");
		    	oStr.append("s.server=\"").append(this.serverName).append("\"\n");
		    	oStr.append("s.channel=\"Single Copy Web\"\n");
		    	oStr.append("s.pageType=\"\"\n");
		    	oStr.append("s.prop1=\"\"\n");
		    	oStr.append("s.prop2=\"\"\n");
		    	oStr.append("s.prop3=\"\"\n");
		    	oStr.append("s.prop4=\"\"\n");
		    	oStr.append("s.prop5=\"\"\n");
		    	
    	    	
		    	oStr.append("//	-->\n</script>");
    	    	String oHTML = null;
    	    	
    	    	// always return the first part of the omniture tracking even if we aren't enabled.
    	    	// this prevents javascript errors if the page name is overriden.
   	    	    oHTML = oStr.toString();
    	    	return oHTML;
    }   
    
    private String getOmnitureTrackingCodeVersionH23_Part1() {

    	StringBuffer oStr = new StringBuffer("<!-- SiteCatalyst code version: H.23.3. \n" +
    	    	"Copyright 1996-2011 Adobe, Inc. All Rights Reserved\n" +
    	    	"More info available at http://www.omniture.com --> \n" +
    	    	"<script language=\"JavaScript\" type=\"text/javascript\" src=\"" +
    	    	this.omnitureScriptLocation +
    	    	"\"></script> \n" +
    	    	" <script language=\"JavaScript\" type=\"text/javascript\"><!-- \n" +
    	    	"/* You may give each page an identifying name, server, and channel on " +
    	    	" the next lines. */ \n");
    	    	
		    	oStr.append("s.pageName=\"\"\n");
		    	oStr.append("s.server=\"").append(this.serverName).append("\"\n");
		    	oStr.append("s.channel=\"Single Copy Web\"\n");
		    	oStr.append("s.pageType=\"\"\n");
		    	oStr.append("s.prop1=\"\"\n");
		    	oStr.append("s.prop2=\"\"\n");
		    	oStr.append("s.prop3=\"\"\n");
		    	oStr.append("s.prop4=\"\"\n");
		    	oStr.append("s.prop5=\"\"\n");
		    	
    	    	
		    	oStr.append("//	-->\n</script>");
    	    	String oHTML = null;
    	    	
    	    	// always return the first part of the omniture tracking even if we aren't enabled.
    	    	// this prevents javascript errors if the page name is overriden.
   	    	    oHTML = oStr.toString();
    	    	return oHTML;
    }

    @SuppressWarnings("unused")
	private String getOmnitureTrackingCodeVersionH15_Part2() {
    	String oHTML = "";
    	
    	// if omniture not enabled don't write the tracking pixel
    	if (!omnitureActive && !USATApplicationConstants.webAnalyticsActive) {
        	return oHTML;
    	}
    	StringBuffer oStr = new StringBuffer();
    	oStr.append(" <script language=\"JavaScript\" type=\"text/javascript\"><!-- \n");
    	
    	oStr.append("/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/ \n");
    	oStr.append("var s_code=s.t();if(s_code)document.write(s_code)//--></script>");
    	oStr.append("<script language=\"JavaScript\" type=\"text/javascript\"><!-- \n");
    	oStr.append("if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\\!-'+'-')");
    	oStr.append("//-->\n</script><noscript><a href=\"http://www.omniture.com\" title=\"Web Analytics\"><img");
    	
    	
    	if (this.omnitureScriptLocation.indexOf("dev") > 0) {
        	oStr.append(" src=\"https://102.112.2O7.net/b/ss/ganusatodaycircdev/1/H.10--NS/0\"");
    	}
    	else {
        	oStr.append(" src=\"https://102.112.2O7.net/b/ss/ganusatodaycircnationalsinglecopy/1/H.15.1--NS/0\"");    	    
    	}
    	oStr.append(" height=\"1\" width=\"1\" border=\"0\" alt=\"\" /></a></noscript><!--/DO NOT REMOVE/--> \n");
    	oStr.append("<!-- End SiteCatalyst code version: H.15.1. -->");
    	
   	    oHTML = oStr.toString();
    	return oHTML;
    }

    private String getOmnitureTrackingCodeVersionH23_Part2() {

    	String oHTML = "";
    	
    	// if omniture not enabled don't write the tracking pixel
    	if (!omnitureActive && !USATApplicationConstants.webAnalyticsActive) {
        	return oHTML;
    	}
    	StringBuffer oStr = new StringBuffer();
    	oStr.append(" <script language=\"JavaScript\" type=\"text/javascript\"><!-- \n");
    	
    	oStr.append("/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/ \n");
    	oStr.append("var s_code=s.t();if(s_code)document.write(s_code)//--></script>");
    	oStr.append("<script language=\"JavaScript\" type=\"text/javascript\"><!-- \n");
    	oStr.append("if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\\!-'+'-')");
    	oStr.append("//-->\n</script><noscript><a href=\"http://www.omniture.com\" title=\"Web Analytics\"><img");
    	
    	
    	if (this.omnitureScriptLocation.indexOf("dev") > 0) {
        	oStr.append(" src=\"https://102.112.2O7.net/b/ss/ganusatodaycircdev/1/H.23.3--NS/0\"");
    	}
    	else {
        	oStr.append(" src=\"https://102.112.2O7.net/b/ss/ganusatodaycircnationalsinglecopy/1/H.23.3--NS/0\"");    	    
    	}
    	oStr.append(" height=\"1\" width=\"1\" border=\"0\" alt=\"\" /></a></noscript><!--/DO NOT REMOVE/--> \n");
    	oStr.append("<!-- End SiteCatalyst code version: H.23.3. -->");
    	
   	    oHTML = oStr.toString();
    	return oHTML;
    }

    /**
     * @return Returns the omnitureActive.
     */
    public boolean isOmnitureActive() {
        return this.omnitureActive;
    }
    /**
     * @param omnitureActive The omnitureActive to set.
     */
    public void setOmnitureActive(boolean omnitureActive) {
        this.omnitureActive = omnitureActive;
    }
}

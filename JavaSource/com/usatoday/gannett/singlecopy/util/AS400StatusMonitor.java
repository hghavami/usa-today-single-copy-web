/*
 * Created on May 11, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.gannett.singlecopy.util;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.util.AS400ServerStatus;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author aeast
 * @date May 11, 2007
 * @class AS400StatusMonitor
 * 
 * 
 * 
 */
public class AS400StatusMonitor extends Thread {

    
    private String lastCheckMessage = "AS400 Status Unknown";
    
    // flag to stop monitoring thread
    private boolean keepMonitoring = true;
    
    private long downFrequency = 90000;   // if site down, then check more frequently
    
    /**
     * 
     */
    public AS400StatusMonitor() {
        super();
    }

    /**
     * @param target
     */
    public AS400StatusMonitor(Runnable target) {
        super(target);
    }

    /**
     * @param group
     * @param target
     */
    public AS400StatusMonitor(ThreadGroup group, Runnable target) {
        super(group, target);
    }

    /**
     * @param name
     */
    public AS400StatusMonitor(String name) {
        super(name);
    }

    /**
     * @param group
     * @param name
     */
    public AS400StatusMonitor(ThreadGroup group, String name) {
        super(group, name);
    }

    /**
     * @param target
     * @param name
     */
    public AS400StatusMonitor(Runnable target, String name) {
        super(target, name);
    }

    /**
     * @param group
     * @param target
     * @param name
     */
    public AS400StatusMonitor(ThreadGroup group, Runnable target, String name) {
        super(group, target, name);
    }

    /**
     * @param group
     * @param target
     * @param name
     * @param stackSize
     */
    public AS400StatusMonitor(ThreadGroup group, Runnable target, String name,
            long stackSize) {
        super(group, target, name, stackSize);
    }

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {

        AS400ServerStatus server = new AS400ServerStatus();

        try {
	        while (this.isKeepMonitoring()) {
	            
	            server.setServerName(USATApplicationConstants.as400Server);
	            
	            boolean previousJDBCState = AS400CurrentStatus.getJdbcActive();
	            
	            boolean jdbcServiceUp = server.isJDBCServiceUp();
            	AS400CurrentStatus.setJdbcActive(jdbcServiceUp);
	            
	            DateTime now = new DateTime();
	            
	            StringBuffer msg = new StringBuffer("Last Status Check: ");
	            msg.append(now.toString("yyyy-MM-dd'T'HH:mm:ss - "));
	            msg.append(USATApplicationConstants.as400Server).append(": JDBC Active: ").append(jdbcServiceUp);
	            
	            this.setLastCheckMessage(msg.toString());

	            if (USATApplicationConstants.debug || !server.isJDBCServiceUp()){
	                System.out.println("AS400StatusMonitor:run() - " + msg.toString());
	            }
	            
	            // if site is up sleep regular time, otherwise sleep for the down time frequency
	            if (jdbcServiceUp) {
	                
	                if (previousJDBCState == false) {
	                    // if prevoius status was down then clear out any pending alarm messages
	                	try {
				            AlarmManager alarmM = AlarmManager.getInstance();
				            alarmM.clearAlarm(AlarmManager.AS400NotPingable);
				            alarmM.sendAlarm("Single Copy Web - AS400 Is Pingable", "Successful ping of AS400, " + server.getServerName() + ".", AlarmManager.OneTimer);
	                	}
	                	catch (Exception mailExp) {
	                		System.out.println("AS400StatusMonitor::run() Failed to clear email alerts: " + mailExp.getMessage());
	                	}
	                }
	                
	                Thread.sleep(USATApplicationConstants.backEndCheckFrequency);
	            }
	            else {
		            // send alarm
	            	try {
	            		AlarmManager alarmM = AlarmManager.getInstance();
	            		alarmM.sendAlarm("Single Copy Web - AS400 (" + server.getServerName() + ") Not Pingable", msg.toString(), AlarmManager.AS400NotPingable);
	            	}
                	catch (Exception mailExp) {
                		System.out.println("AS400StatusMonitor::run() Failed to add email alert: " + mailExp.getMessage());
                	}
	                Thread.sleep(this.downFrequency);
	            }
	        }
        }
        catch(Exception e) {
            System.out.println("AS400 Status Monitor Thread shutting down. Exception:" + e.getMessage());
        }
    }
    /**
     * @return Returns the keepMonitoring.
     */
    public boolean isKeepMonitoring() {
        return this.keepMonitoring;
    }
    /**
     * @param keepMonitoring The keepMonitoring to set.
     */
    public void setKeepMonitoring(boolean keepMonitoring) {
        this.keepMonitoring = keepMonitoring;
    }
    /**
     * @return Returns the lastCheckMessage.
     */
    public String getLastCheckMessage() {
        return this.lastCheckMessage;
    }
    /**
     * @param lastCheckMessage The lastCheckMessage to set.
     */
    private void setLastCheckMessage(String lastCheckMessage) {
        this.lastCheckMessage = lastCheckMessage;
    }
}

/*
 * Created on Jul 23, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.gannett.singlecopy.util;

import java.util.ArrayList;
import java.util.HashMap;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.util.USATApplicationConstants;
import com.usatoday.singlecopy.model.util.email.EmailAlert;

/**
 * @author aeast
 * @date Jul 23, 2007
 * @class AlarmManager
 * 
 * 
 * 
 */
public class AlarmManager {

    public static final Integer AS400NotPingable = new Integer(0);
    public static final Integer UnexpectedAppError = new Integer(1);
    public static final Integer DefaultAlarm = new Integer(2);
    public static final Integer OneTimer = new Integer(99);
    
    private HashMap<Integer, Alarm> alarms = new HashMap<Integer, Alarm>();
    
    private static AlarmManager _instance = null;
    /**
     * 
     */
    private AlarmManager() {
        super();
        // just in case this hasn't been done..
        USATApplicationConstants.loadProperties();
        
    }

    public static AlarmManager getInstance() {
        if (AlarmManager._instance == null) {
            AlarmManager._instance = new AlarmManager();
        }
        return AlarmManager._instance;
    }
    
    public void sendAlarm(String subject, String message, Integer alarmID) {
        if (alarmID == null) {
            alarmID = AlarmManager.DefaultAlarm;
        }
        Alarm alarm = null;
        EmailAlert email = null;
        if (this.alarms.containsKey(alarmID)) {
           alarm  = this.alarms.get(alarmID);
           
           alarm.addMessage(message);
           // send an alarm no more frequently than once every 30 minutes.
           DateTime nextSend = alarm.getAlarmSent().plusMinutes(30);
           if (nextSend.isBeforeNow()) {
               email = this.generateEmail(alarm);
           }
            
        }
        else {
            alarm = new Alarm();
            alarm.addMessage(message);
            alarm.setSubject(subject);
            alarm.setAlarmID(alarmID);
            
            email = this.generateEmail(alarm);
            
            if (alarmID.compareTo(AlarmManager.OneTimer) != 0) {
                this.alarms.put(alarmID, alarm);
            }
        }
        
        if (email != null && USATApplicationConstants.alarmsEnabled) {
            email.sendAlert();
            alarm.setAlarmSent(new DateTime());
            alarm.clearMessages();
        }
    }
    
    /**
     * 
     * @param alarmID - The alarm id to clear (indicates all is ok)
     */
    public void clearAlarm(Integer alarmID) {
        if (alarmID != null) {
            this.alarms.remove(alarmID);
        }
    }
    
    /**
     * 
     * @param alarm
     * @return
     */
    private EmailAlert generateEmail(Alarm alarm) {
        EmailAlert email = new EmailAlert();
        
        email.setSubject(alarm.getSubject());
        email.setReceiverList(USATApplicationConstants.alarmRecipients);
        email.setSender(USATApplicationConstants.alarmSender);
        StringBuffer body = new StringBuffer();
        
        ArrayList<String> messages = alarm.getMessages();
        for (int i = 0; i < messages.size(); i++) {
            String msg = messages.get(i);
            body.append(msg).append("\n");
        }
        email.setBodyText(body.toString());
        return email;
    }
    
    public void resetAlarmManager() {
    	this.alarms.clear();
    	AlarmManager._instance = null;
    }
}

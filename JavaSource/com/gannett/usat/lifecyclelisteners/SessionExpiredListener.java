package com.gannett.usat.lifecyclelisteners;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.usatoday.champion.handlers.RDLForDayHandler;
import com.usatoday.champion.handlers.RDLForWeekHandler;
import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.champion.handlers.blueChipDraw.BlueChipDrawManagementHandler;
import com.usatoday.champion.handlers.blueChipDraw.BlueChipWeeklyProductOrderHandler;
import com.usatoday.singlecopy.model.bo.users.UserBO;

public class SessionExpiredListener implements HttpSessionListener {
	/* (non-Java-doc)
	 * @see java.lang.Object#Object()
	 */
	public SessionExpiredListener() {
		super();
	}

	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpSessionListener#sessionCreated(HttpSessionEvent arg0)
	 */
	public void sessionCreated(HttpSessionEvent arg0) {
		// don't do anything
	}

	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpSessionListener#sessionDestroyed(HttpSessionEvent arg0)
	 */
	public void sessionDestroyed(HttpSessionEvent event) {
	    try {
	        
	        HttpSession session = event.getSession();
	        
		    UserHandler uh = (UserHandler)session.getAttribute("userHandler");
		    if (uh != null) {
		        if (uh.getUser() != null) {
		            UserBO u = (UserBO)uh.getUser();
		            u.logout();
		        }
		    }
		    
	        RDLForDayHandler rdl = (RDLForDayHandler)session.getAttribute("RDLForDay");
	        if (rdl != null) {
	            rdl.clear();
	            session.removeAttribute("RDLForDay");
	        }
	        RDLForWeekHandler rdlw = (RDLForWeekHandler)session.getAttribute("RDLForWeek");
	        if (rdlw != null) {
	            rdlw.clear();
	            session.removeAttribute("RDLForWeek");
	        }
	        
	        BlueChipDrawManagementHandler bcdh = (BlueChipDrawManagementHandler)session.getAttribute("blueChipDrawManagementHandler");
	        if (bcdh != null) {
	            bcdh.clear();
	            session.removeAttribute("blueChipDrawManagementHandler");
	        }
	        
	        BlueChipWeeklyProductOrderHandler bcwh = (BlueChipWeeklyProductOrderHandler)session.getAttribute("blueChipPOForWeek");
	        if (bcwh != null) {
	            bcwh.clear();
	            session.removeAttribute("blueChipPOForWeek");
	        }
	        
	    }
	    catch (Exception e) {
	        ;// ignore
	    }
	}

	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpSessionAttributeListener#attributeAdded(HttpSessionBindingEvent arg0)
	 */
	public void attributeAdded(HttpSessionBindingEvent arg0) {
		// 
	}

	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpSessionAttributeListener#attributeRemoved(HttpSessionBindingEvent arg0)
	 */
	public void attributeRemoved(HttpSessionBindingEvent arg0) {
		// 
	}

	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpSessionAttributeListener#attributeReplaced(HttpSessionBindingEvent arg0)
	 */
	public void attributeReplaced(HttpSessionBindingEvent arg0) {
		// 
	}

}
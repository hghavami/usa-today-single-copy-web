/*
 * Created on May 21, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.gannett.usat.lifecyclelisteners;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletResponse;
import javax.faces.context.FacesContext;


/**
 * @author aeast
 * @date May 21, 2008
 * @class CacheControlPhaseListener
 * 
 * This class clears the browser cache so pages aren't cached on browser.
 * 
 */
public class CacheControlPhaseListener implements PhaseListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 
     */
    public CacheControlPhaseListener() {
        super();
    }

    public PhaseId getPhaseId() {
        return PhaseId.RENDER_RESPONSE;
    }
    
    public void afterPhase(PhaseEvent event) {}
    
    public void beforePhase(PhaseEvent event) {
        FacesContext facesContext = event.getFacesContext();
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        response.addHeader("Pragma", "no-cache");
        response.addHeader("Cache-Control", "no-cache");
        response.addHeader("Cache-Control", "no-store");
        response.addHeader("Expires", "Mon, 1 Jan 2007 10:00:00 GMT");
    }
    
}

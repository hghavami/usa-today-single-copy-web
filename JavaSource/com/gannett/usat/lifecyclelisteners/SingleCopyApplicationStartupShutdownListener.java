package com.gannett.usat.lifecyclelisteners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.usatoday.champion.handlers.blueChipDraw.BlueChipDrawManagementHandler;
import com.usatoday.singlecopy.model.util.ProductDescriptionCache;
import com.usatoday.singlecopy.model.util.ProductOrderTypesCache;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;
import com.usatoday.singlecopy.model.util.ZeroDrawReasonCodeCache;

public class SingleCopyApplicationStartupShutdownListener implements ServletContextListener {
    
    
	/* (non-Java-doc)
	 * @see java.lang.Object#Object()
	 */
	public SingleCopyApplicationStartupShutdownListener() {
		super();
	}

	/* (non-Java-doc)
	 * @see javax.servlet.ServletContextListener#contextInitialized(ServletContextEvent arg0)
	 */
	public void contextInitialized(ServletContextEvent arg0) {
	    
	    // load the properties for the middle tier object model
	    USATApplicationConstants.loadProperties();
	    
	    // Initialize caches
	    try {
	    @SuppressWarnings("unused")
		ProductDescriptionCache c1 = ProductDescriptionCache.getInstance();
	    @SuppressWarnings("unused")
		ProductOrderTypesCache c2 = ProductOrderTypesCache.getProductOrderTypesCache();
	    @SuppressWarnings("unused")
		ZeroDrawReasonCodeCache c3 = ZeroDrawReasonCodeCache.getInstance();
	    }
	    catch (Exception e) {
	        e.printStackTrace();
	        System.out.println("SingleCopyApplicationStartupShutdownListener::> Exception initializing appilcation: " + e.getMessage());
	    }
	    
	    String staleDataThreshold = arg0.getServletContext().getInitParameter("com.usatoday.singlecopy.bluechip.staleDataThreshold");
	    if (staleDataThreshold != null) {
	        try {
	            int intVal = Integer.parseInt(staleDataThreshold.trim());
	            BlueChipDrawManagementHandler.setDefaultStaleDataThreshold(intVal);
	        }
	        catch (Exception e) {
	            System.out.println("SingleCopyApplicationStartupShutdownListener - init() - Failed to set blue chip stale data threshold: check init parm 'com.usatoday.singlecopy.bluechip.staleDataThreshold' : " + e.getMessage());
	        }
	    }
	    
	    System.out.println("Single Copy Web App AS400 Server Status Initialized.");
	}

	/* (non-Java-doc)
	 * @see javax.servlet.ServletContextListener#contextDestroyed(ServletContextEvent arg0)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {
		
	    System.out.println("Single Copy Web App AS400 Server Status Listener is shut down.");
	}

}
/*
 * Created on Aug 1, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.gannett.usat.singlecopy.web.servlets;

/**
 * @author aeast
 * @date Aug 1, 2007
 * @class DrawFormConverter
 * 
 * 
 * 
 */
public abstract class DrawFormConverter {

    protected static final String DELIMITER = ":";
    /**
     * 
     */
    public DrawFormConverter() {
        super();
    }

    protected abstract ParsedParameter parseParameter(String value);
}

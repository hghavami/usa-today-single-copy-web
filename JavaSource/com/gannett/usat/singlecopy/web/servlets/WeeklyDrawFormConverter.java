/*
 * Created on Aug 1, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.gannett.usat.singlecopy.web.servlets;

import java.util.StringTokenizer;

/**
 * @author aeast
 * @date Aug 1, 2007
 * @class WeeklyDrawFormConverter
 * 
 * 
 * 
 */
public class WeeklyDrawFormConverter extends DrawFormConverter {

    protected static final String DRAW_TAG = "DrawValue";

    protected static final String RETURN_TAG = "ReturnValue";

    protected static final String ZDR_TAG = "ZeroDrawReasonCode";
    
   
    /**
     * 
     */
    public WeeklyDrawFormConverter() {
        super();
        
    }

    /* (non-Javadoc)
     * @see com.gannett.usat.singlecopy.web.servlets.DrawFormConverter#parseField(java.lang.String)
     * 
     */
    /**
     *  parse a draw form field for returns, draw, zero draw reason codes
     */
    protected ParsedParameter parseParameter(String parameterName) {
        ParsedParameter pParam = new ParsedParameter();
        
        StringTokenizer tokenizer = new StringTokenizer(parameterName, DrawFormConverter.DELIMITER);
        
        int numTokens = tokenizer.countTokens();
        
        if (numTokens != 4) {
            pParam.setLocationHash("");
            pParam.setProductOrderID("");
            pParam.setField(parameterName);
        }
        else {
            pParam.setLocationHash(tokenizer.nextToken());
            pParam.setProductOrderID(tokenizer.nextToken());
            pParam.setDayOfWeek(tokenizer.nextToken());
            pParam.setField(tokenizer.nextToken());
        }
        
        return pParam;
    }
}

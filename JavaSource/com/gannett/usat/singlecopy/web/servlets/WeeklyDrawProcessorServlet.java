package com.gannett.usat.singlecopy.web.servlets;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;

import com.usatoday.champion.handlers.RDLForWeekHandler;
import com.usatoday.champion.handlers.WeeklyLocationHandler;
import com.usatoday.singlecopy.model.bo.drawmngmt.instance.WeeklyProductOrderBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;
import com.usatoday.singlecopy.model.transferObjects.WeeklyDrawUpdateErrorTO;

public class WeeklyDrawProcessorServlet extends HttpServlet implements Servlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2555508954190147414L;

	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#HttpServlet()
	 */
	public WeeklyDrawProcessorServlet() {
		super();
	}

	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest req, HttpServletResponse resp)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}

	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest req, HttpServletResponse resp)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
	    
	    String action = req.getParameter("formManageDraw:buttonSubmit");
	    
	    RDLForWeekHandler rdl = (RDLForWeekHandler)req.getSession().getAttribute("RDLForWeek");
	    
	    if (rdl == null || rdl.getLocations().size() == 0) {
	        // send to no data page
	        req.getSession().removeAttribute("RDLForWeek");
	        req.getRequestDispatcher("/sc_secure/drawManagement/forweek/nodata.usat").forward(req, resp);
	    }
	    else if (action == null || action.indexOf("Quit")>=0) {
	        Iterator<WeeklyLocationHandler> itr = rdl.getLocations().iterator();
	        while (itr.hasNext()) {
	            WeeklyLocationHandler l = itr.next();
	            l.getProductOrders().clear();
	        }
	        rdl.getLocations().clear();
	        rdl.setLocations(null);;
	        req.getSession().removeAttribute("RDLForWeek");
	        req.getRequestDispatcher("/sc_secure/drawManagement/forweek/closeRDLWindow.usat").forward(req, resp);
	    }
	    else { // update
	         // reset any error processing
	        rdl.resetErrorProcessing();
	        
		    // process form fields and update middle tier objects
		    boolean errorsExist = updateBusinessObjects(rdl, req);
		    
		    if (errorsExist) {
			    req.getRequestDispatcher("/sc_secure/drawManagement/forweek/d4week.jsp").forward(req, resp);	        
		    }
		    else {
			    req.getRequestDispatcher("/sc_secure/drawManagement/forweek/confirm.usat").forward(req, resp);	        
		    }
	    }	    
	}

	/**
	 * 
	 * @param rdl - current route delivery list
	 * @param params - form parameters
	 * @return - true if errors exist (business rule violations) otherwise false
	 */
	@SuppressWarnings({ "rawtypes" })
	private boolean updateBusinessObjects(RDLForWeekHandler rdl, HttpServletRequest req) {
	    boolean errorsExist = false;
	    
	    Map params = req.getParameterMap();
	    Set keys = params.keySet();
	    Iterator itr = keys.iterator();

        WeeklyDrawFormConverter converter = new WeeklyDrawFormConverter();

	    // iterate over form fields
	    while (itr.hasNext()) {
	        String paramName = (String)itr.next();
	        String paramValue = req.getParameter(paramName);
	        
	        //if (paramValue == null || paramValue.length()==0) {
	        if (paramValue == null) {
	            // if no value submitted then go to next field, no changes required.
	            continue;
	        }

	        ParsedParameter pParam = converter.parseParameter(paramName);
	        
	        pParam.setSubmittedValue(paramValue);
	        
	        if (pParam.getLocationHash().length() > 0) {
	            // attempt to process form field
	            LocationIntf loc = rdl.getWdm().getLocation(pParam.getLocationHash());
	            if (loc != null) {
	                ProductOrderIntf po = loc.getProductOrder(pParam.getProductOrderID());
	                if (po != null && po instanceof WeeklyProductOrderBO) {
	                    WeeklyProductOrderBO wpo = (WeeklyProductOrderBO)po;
	                    
	                    // start with day of week
	                    if (pParam.getDayOfWeek().indexOf("Monday") >= 0) {
	                        if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.DRAW_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() == 0) {
	                                    wpo.setMondayDraw(-1);
	                                }
	                                else {
	                                    int intValue = Integer.parseInt(pParam.getSubmittedValue());
	                                    wpo.setMondayDraw(intValue);
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.DRAW_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(6), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                        else if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.RETURN_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() == 0) {
	                                    wpo.setMondayReturns(-1);
	                                }
	                                else {
		                                int intValue = Integer.parseInt(pParam.getSubmittedValue());
	                                    wpo.setMondayReturns(intValue);
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.RETURN_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(6), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                            
	                        }
	                        else if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.ZDR_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() > 0) {
	                                    wpo.setMondayZeroDrawReasonCode(pParam.getSubmittedValue());
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.ZDR_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(6), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }	                            
	                        }
	                    }
	                    else if (pParam.getDayOfWeek().indexOf("Tuesday") >= 0) {
	                        if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.DRAW_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() == 0) {
	                                    wpo.setTuesdayDraw(-1);
	                                }
	                                else {
	                                    int intValue = Integer.parseInt(pParam.getSubmittedValue());
	                                    wpo.setTuesdayDraw(intValue);
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.DRAW_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(5), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                        else if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.RETURN_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() == 0) {
	                                    wpo.setTuesdayReturns(-1);
	                                }
	                                else {
		                                int intValue = Integer.parseInt(pParam.getSubmittedValue());
	                                    wpo.setTuesdayReturns(intValue);
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.RETURN_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(5), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                        else if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.ZDR_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() > 0) {
	                                    wpo.setTuesdayZeroDrawReasonCode(pParam.getSubmittedValue());
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.ZDR_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(5), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                    }
	                    else if (pParam.getDayOfWeek().indexOf("Wednesday") >= 0) {
	                        if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.DRAW_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() == 0) {
	                                    wpo.setWednesdayDraw(-1);
	                                }
	                                else {
	                                    int intValue = Integer.parseInt(pParam.getSubmittedValue());
	                                    wpo.setWednesdayDraw(intValue);
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.DRAW_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(4), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                        else if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.RETURN_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() == 0) {
	                                    wpo.setWednesdayReturns(-1);
	                                }
	                                else {
		                                int intValue = Integer.parseInt(pParam.getSubmittedValue());
	                                    wpo.setWednesdayReturns(intValue);
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.RETURN_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(4), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                        else if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.ZDR_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() > 0) {
	                                    wpo.setWednesdayZeroDrawReasonCode(pParam.getSubmittedValue());
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.ZDR_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(4), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                    }
	                    else if (pParam.getDayOfWeek().indexOf("Thursday") >= 0) {
	                        if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.DRAW_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() == 0) {
	                                    wpo.setThursdayDraw(-1);
	                                }
	                                else {
	                                    int intValue = Integer.parseInt(pParam.getSubmittedValue());
	                                    wpo.setThursdayDraw(intValue);
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.DRAW_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(3), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                        else if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.RETURN_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() == 0) {
	                                    wpo.setThursdayReturns(-1);
	                                }
	                                else {
		                                int intValue = Integer.parseInt(pParam.getSubmittedValue());
	                                    wpo.setThursdayReturns(intValue);
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.RETURN_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(3), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                        else if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.ZDR_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() > 0) {
	                                    wpo.setThursdayZeroDrawReasonCode(pParam.getSubmittedValue());
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.ZDR_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(3), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                    }
	                    else if (pParam.getDayOfWeek().indexOf("Friday") >= 0) {
	                        if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.DRAW_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() == 0) {
	                                    wpo.setFridayDraw(-1);
	                                }
	                                else {
	                                    int intValue = Integer.parseInt(pParam.getSubmittedValue());
	                                    wpo.setFridayDraw(intValue);
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.DRAW_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(2), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                        else if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.RETURN_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() == 0) {
	                                    wpo.setFridayReturns(-1);
	                                }
	                                else {
		                                int intValue = Integer.parseInt(pParam.getSubmittedValue());
	                                    wpo.setFridayReturns(intValue);
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.RETURN_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(2), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                        else if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.ZDR_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() > 0) {
	                                    wpo.setFridayZeroDrawReasonCode(pParam.getSubmittedValue());
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.ZDR_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(2), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                    }
	                    else if (pParam.getDayOfWeek().indexOf("Saturday") >= 0) {
	                        if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.DRAW_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() == 0) {
	                                    wpo.setSaturdayDraw(-1);
	                                }
	                                else {
	                                    int intValue = Integer.parseInt(pParam.getSubmittedValue());
	                                    wpo.setSaturdayDraw(intValue);
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.DRAW_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(1), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                        else if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.RETURN_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() == 0) {
	                                    wpo.setSaturdayReturns(-1);
	                                }
	                                else {
		                                int intValue = Integer.parseInt(pParam.getSubmittedValue());
	                                    wpo.setSaturdayReturns(intValue);
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.RETURN_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(1), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                        else if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.ZDR_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() > 0) {
	                                    wpo.setSaturdayZeroDrawReasonCode(pParam.getSubmittedValue());
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.ZDR_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(1), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                    }
	                    else if (pParam.getDayOfWeek().indexOf("Sunday") >= 0) {
	                        if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.DRAW_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() == 0) {
	                                    wpo.setSundayDraw(-1);
	                                }
	                                else {
	                                    int intValue = Integer.parseInt(pParam.getSubmittedValue());
	                                    wpo.setSundayDraw(intValue);
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.DRAW_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate(), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                        else if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.RETURN_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() == 0) {
	                                    wpo.setSundayReturns(-1);
	                                }
	                                else {
		                                int intValue = Integer.parseInt(pParam.getSubmittedValue());
	                                    wpo.setSundayReturns(intValue);
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.RETURN_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate(), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                        else if (pParam.getField().equalsIgnoreCase(WeeklyDrawFormConverter.ZDR_TAG)) {
	                            try {
	                                if (pParam.getSubmittedValue().length() > 0) {
	                                    wpo.setSundayZeroDrawReasonCode(pParam.getSubmittedValue());
	                                }
	                            }
	                            catch (Exception e){
	                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
	                                // String errorMessage, String poID, DateTime weekEndingDate
	                                wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.ZDR_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
	                                       rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate(), 
	                                       e.getMessage(), wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	                                errorsExist = true;
	                            }
	                        }
	                    } // end if Sunday
	                    
	                } // end if po exists
	            } // end if location exists
	        } // end if parsded field is valid
	    } // end while more parameters
	    
	    if (!errorsExist) {
		    // further validate the pos
	        itr = rdl.getWdm().getChangedProductOrders().iterator();
	        while (itr.hasNext()) {
	            WeeklyProductOrderBO wpo = (WeeklyProductOrderBO)itr.next();
	            if (!wpo.isValidPO()) {
	                errorsExist = true;
                    wpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.BOTH_ERROR, rdl.getWdm().getRoute().getDistrictID(), 
                            rdl.getWdm().getRoute().getMarketID(), rdl.getWdm().getRoute().getRouteID(), rdl.getWdm().getWeekEndingDate().minusDays(6), 
                            "Invalid Draw/Return Settings Detected", wpo.getProductOrderID(), rdl.getWdm().getWeekEndingDate()));
	            }
	        }
	        
	    }
	    
	    rdl.getWdm().setUpdateErrorsFlag(errorsExist);
	    
	    return errorsExist;
	}
	
	/**
	 * 
	 * @param appliesTo
	 * @param district
	 * @param market
	 * @param route
	 * @param errorDate
	 * @param errorMessage
	 * @param poID
	 * @param weekEndingDate
	 * @return
	 */
	private WeeklyDrawUpdateErrorTO generateError(String appliesTo, String district, String market, String route, DateTime errorDate, String errorMessage, String poID, DateTime weekEndingDate ) {
        WeeklyDrawUpdateErrorTO error  = new WeeklyDrawUpdateErrorTO();;
        error.setAppliesTo(appliesTo);
        error.setDistrictID(district);
        error.setMarketID(market);
        error.setRouteID(route);
        error.setErrorDate(errorDate);
        error.setErrorMessage(errorMessage);
        error.setProductOrderID(poID);
        error.setWeekEndingDate(weekEndingDate);
	    
	    return error;
	}
}
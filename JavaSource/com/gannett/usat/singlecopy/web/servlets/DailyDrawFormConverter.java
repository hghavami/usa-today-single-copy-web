/*
 * Created on Aug 24, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.gannett.usat.singlecopy.web.servlets;

import java.util.StringTokenizer;

/**
 * @author aeast
 * @date Aug 24, 2007
 * @class DailyDrawFormConverter
 * 
 * 
 * 
 */
public class DailyDrawFormConverter extends DrawFormConverter {

    protected static final String DRAW_TAG = "DrawValue";
    protected static final String RETURN_TAG = "ReturnValue";
    protected static final String ZDR_TAG = "ZeroDrawReasonCode";

    
    /**
     * 
     */
    public DailyDrawFormConverter() {
        super();
    }

    /* (non-Javadoc)
     * @see com.gannett.usat.singlecopy.web.servlets.DrawFormConverter#parseParameter(java.lang.String)
     */
    protected ParsedParameter parseParameter(String parameterName) {
        ParsedParameter pParam = new ParsedParameter();
        
        StringTokenizer tokenizer = new StringTokenizer(parameterName, DrawFormConverter.DELIMITER);
        
        int numTokens = tokenizer.countTokens();
        
        if (numTokens != 3) {
            pParam.setLocationHash("");
            pParam.setProductOrderID("");
            pParam.setField(parameterName);
        }
        else {
            pParam.setLocationHash(tokenizer.nextToken());
            pParam.setProductOrderID(tokenizer.nextToken());
            pParam.setField(tokenizer.nextToken());
        }
        
        return pParam;
    }
}

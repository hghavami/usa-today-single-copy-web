/*
 * Created on Aug 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.gannett.usat.singlecopy.web.servlets;

/**
 * @author aeast
 * @date Aug 2, 2007
 * @class ParsedParameter
 * 
 * 
 * 
 */
public class ParsedParameter {
    /**
     * 
     */
    public ParsedParameter() {
        super();
    }

    String locationHash = null;
    String productOrderID = null;
    String dayOfWeek = "";
    String field = null;
        
    String submittedValue = null;
    
    /**
     * @return Returns the field.
     */
    public String getField() {
        return this.field;
    }
    /**
     * @param field The field to set.
     */
    public void setField(String field) {
        this.field = field;
    }
    /**
     * @return Returns the locationHash.
     */
    public String getLocationHash() {
        return this.locationHash;
    }
    /**
     * @param locationHash The locationHash to set.
     */
    public void setLocationHash(String locationHash) {
        this.locationHash = locationHash;
    }
    /**
     * @return Returns the productOrderID.
     */
    public String getProductOrderID() {
        return this.productOrderID;
    }
    /**
     * @param productOrderID The productOrderID to set.
     */
    public void setProductOrderID(String productOrderID) {
        this.productOrderID = productOrderID;
    }
    /**
     * @return Returns the submittedValue.
     */
    public String getSubmittedValue() {
        return this.submittedValue;
    }
    /**
     * @param submittedValue The submittedValue to set.
     */
    public void setSubmittedValue(String submittedValue) {
        this.submittedValue = submittedValue;
    }
    /**
     * @return Returns the dayOfWeek.
     */
    public String getDayOfWeek() {
        return this.dayOfWeek;
    }
    /**
     * @param dayOfWeek The dayOfWeek to set.
     */
    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }
}


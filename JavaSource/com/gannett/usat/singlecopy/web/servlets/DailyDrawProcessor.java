package com.gannett.usat.singlecopy.web.servlets;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;

import com.usatoday.champion.handlers.LocationHandler;
import com.usatoday.champion.handlers.RDLForDayHandler;
import com.usatoday.singlecopy.model.bo.drawmngmt.instance.DailyProductOrderBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;
import com.usatoday.singlecopy.model.transferObjects.DrawUpdateErrorTO;

public class DailyDrawProcessor extends HttpServlet implements Servlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3478929070462323243L;



	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#HttpServlet()
	 */
	public DailyDrawProcessor() {
		super();
	}

	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest arg0, HttpServletResponse arg1)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}

	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest arg0, HttpServletResponse arg1)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    String action = req.getParameter("formManageDraw:buttonSubmitDrawChanges");
	    
	    RDLForDayHandler rdl = (RDLForDayHandler)req.getSession().getAttribute("RDLForDay");
	    
	    if (rdl == null || rdl.getLocations().size() == 0) {
	        // send to no data page
	        req.getSession().removeAttribute("RDLForDay");
	        req.getRequestDispatcher("/sc_secure/drawManagement/forday/nodata.usat").forward(req, resp);
	    }
	    else if (action == null || action.indexOf("Quit")>=0) {
	        Iterator<LocationHandler> itr = rdl.getLocations().iterator();
	        while (itr.hasNext()) {
	            LocationHandler l = itr.next();
	            l.getProductOrders().clear();
	        }
	        rdl.getLocations().clear();
	        rdl.setLocations(null);;
	        req.getSession().removeAttribute("RDLForDay");
	        req.getRequestDispatcher("/sc_secure/drawManagement/forday/closeRDLWindow.usat").forward(req, resp);
	    }
	    else { // update
		    // process form fields and update middle tier objects
            rdl.resetErrorProcessing();	        

            boolean errorsExist = updateBusinessObjects(rdl, req);
		    
		    if (errorsExist) {
			    req.getRequestDispatcher("/sc_secure/drawManagement/forday/d4day.jsp").forward(req, resp);	        
		    }
		    else {
			    req.getRequestDispatcher("/sc_secure/drawManagement/forday/confirm.usat").forward(req, resp);	        
		    }
	    }	    
	}

	/**
	 * 
	 * @param rdl
	 * @param req
	 * @return
	 */
	@SuppressWarnings({ "rawtypes" })
	private boolean updateBusinessObjects(RDLForDayHandler rdl, HttpServletRequest req) {
	    boolean errorsExist = false;
	    
	    Map params = req.getParameterMap();
	    Set keys = params.keySet();
	    Iterator itr = keys.iterator();

        DailyDrawFormConverter converter = new DailyDrawFormConverter();

	    // iterate over form fields
	    while (itr.hasNext()) {
	        String paramName = (String)itr.next();
	        String paramValue = req.getParameter(paramName);
	        
	        //if (paramValue == null || paramValue.length()==0) {
	        if (paramValue == null) {
	            // if no value submitted then go to next field, no changes required.
	            continue;
	        }

	        ParsedParameter pParam = converter.parseParameter(paramName);
	        
	        pParam.setSubmittedValue(paramValue);
	        
	        if (pParam.getLocationHash().length() > 0) {
	            // attempt to process form field
	            LocationIntf loc = rdl.getDdm().getLocation(pParam.getLocationHash());
	            if (loc != null) {
	                ProductOrderIntf po = loc.getProductOrder(pParam.getProductOrderID());
	                if (po != null && po instanceof DailyProductOrderBO) {
	                    DailyProductOrderBO dpo = (DailyProductOrderBO)po;
	                    
	                    // start with day of week
	                    if (pParam.getField().equalsIgnoreCase(DailyDrawFormConverter.DRAW_TAG)) {
                            try {
                                if (pParam.getSubmittedValue().length() == 0) {
                                    dpo.setDraw(-1);
                                }
                                else {
                                    int intValue = Integer.parseInt(pParam.getSubmittedValue());
                                    dpo.setDraw(intValue);
                                }
                            }
                            catch (Exception e){
                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
                                // String errorMessage, String poID, DateTime weekEndingDate
                                dpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.DRAW_ERROR, rdl.getDdm().getRoute().getDistrictID(), 
                                       rdl.getDdm().getRoute().getMarketID(), rdl.getDdm().getRoute().getRouteID(), rdl.getDdm().getRDLDate(), 
                                       e.getMessage(), dpo.getProductOrderID()));
                                errorsExist = true;
                            }
	                    }
	                    else if (pParam.getField().equalsIgnoreCase(DailyDrawFormConverter.RETURN_TAG)) {
                            try {
                                // if blank then set to -1
                                if (pParam.getSubmittedValue().length() == 0) {
                                    dpo.setReturns(-1);
                                }
                                else {
                                    int intValue = Integer.parseInt(pParam.getSubmittedValue());
                                    dpo.setReturns(intValue);
                                }
                            }
                            catch (Exception e){
                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
                                // String errorMessage, String poID, DateTime weekEndingDate
                                dpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.RETURN_ERROR, rdl.getDdm().getRoute().getDistrictID(), 
                                        rdl.getDdm().getRoute().getMarketID(), rdl.getDdm().getRoute().getRouteID(), rdl.getDdm().getRDLDate(), 
                                        e.getMessage(), dpo.getProductOrderID()));
                                 errorsExist = true;
                            }
	                    }
	                    else if (pParam.getField().equalsIgnoreCase(DailyDrawFormConverter.ZDR_TAG)) {
                            try {
                                if (pParam.getSubmittedValue().length() > 0) {
                                    dpo.setZeroDrawReasonCode(pParam.getSubmittedValue());
                                }
                            }
                            catch (Exception e){
                                // String appliesTo, String district, String market, String route, DateTime errorDate, 
                                // String errorMessage, String poID, DateTime weekEndingDate
                                dpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.ZDR_ERROR, rdl.getDdm().getRoute().getDistrictID(), 
                                        rdl.getDdm().getRoute().getMarketID(), rdl.getDdm().getRoute().getRouteID(), rdl.getDdm().getRDLDate(), 
                                        e.getMessage(), dpo.getProductOrderID()));
                                 errorsExist = true;
                            }
                        }
	                	
	                } // end if po exists
	            } // end if location exists
	        } // end if parsded field is valid
	    } // end while more parameters
	    
	    if (!errorsExist) {
		    // further validate the pos
	        itr = rdl.getDdm().getChangedProductOrders().iterator();
	        while (itr.hasNext()) {
	            DailyProductOrderBO dpo = (DailyProductOrderBO)itr.next();
	            if (!dpo.isValidPO()) {
	                errorsExist = true;
	                dpo.addUpdateError(this.generateError(DrawUpdateErrorIntf.BOTH_ERROR, rdl.getDdm().getRoute().getDistrictID(), 
                            rdl.getDdm().getRoute().getMarketID(), rdl.getDdm().getRoute().getRouteID(), rdl.getDdm().getRDLDate(), 
                            "Invalid Draw/Return Settings Detected", dpo.getProductOrderID()));
	            }
	        }
	        
	    }

	    rdl.getDdm().setUpdateErrorsFlag(errorsExist);
	    
	    return errorsExist;
	}
	
	
	
	/**
	 * 
	 * @param appliesTo
	 * @param district
	 * @param market
	 * @param route
	 * @param errorDate
	 * @param errorMessage
	 * @param poID
	 * @param weekEndingDate
	 * @return
	 */
	private DrawUpdateErrorTO generateError(String appliesTo, String district, String market, String route, DateTime errorDate, String errorMessage, String poID ) {
	    DrawUpdateErrorTO error  = new DrawUpdateErrorTO();;
        error.setAppliesTo(appliesTo);
        error.setDistrictID(district);
        error.setMarketID(market);
        error.setRouteID(route);
        error.setErrorDate(errorDate);
        error.setErrorMessage(errorMessage);
        error.setProductOrderID(poID);
	    
	    return error;
	}
	
}
package com.gannett.usat.singlecopy.web.portal.forms;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application.
 * Users may access 1 field on this form:
 * <ul>
 * <li>d - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class RDLForDayLoaderBean extends ActionForm

{
    /**
	 * 
	 */
	private static final long serialVersionUID = -1334763219233895912L;
	private String dataExists = "Y";
    private String rte = null;
    private String d = null;

    /**
     * Get d
     * @return String
     */
    public String getD() {
        return d;
    }

    /**
     * Set d
     * @param <code>String</code>
     */
    public void setD(String d) {
        this.d = d;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {

        // Reset values are provided as samples only. Change as appropriate.
        dataExists = "Y";

        rte = null;
        d = null;

    }

    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
        // Validate the fields in your form, adding
        // adding each error to this.errors as found, e.g.

        // if ((field == null) || (field.length() == 0)) {
        //   errors.add("field", new org.apache.struts.action.ActionError("error.field.required"));
        // }
        return errors;

    }
    /**
     * @return Returns the dataExists.
     */
    public String getDataExists() {
        return this.dataExists;
    }
    /**
     * @param dataExists The dataExists to set.
     */
    public void setDataExists(String dataExists) {
        this.dataExists = dataExists;
    }
	/**
	 * @return Returns the rteKey.
	 */
	public String getRte() {
		return rte;
	}
	/**
	 * @param rteKey The rteKey to set.
	 */
	public void setRte(String rteKey) {
		this.rte = rteKey;
	}
}

package com.gannett.usat.singlecopy.web.portal.forms;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Form bean for a Struts application.
 * @version 	1.0
 * @author
 */
public class RDLForWeekLoaderBean extends ActionForm

{
    /**
	 * 
	 */
	private static final long serialVersionUID = -3504716055874604081L;
	private String d = null;
    private String pubCode = null;
    private String hideViewOnly = "N";
    private String dataExists = "Y";
    private String rte = null;

    public void reset(ActionMapping mapping, HttpServletRequest request) {

        // Reset field values here.
        d = null;
        pubCode = null;
        hideViewOnly = "N";
        dataExists = "Y";
        rte = null;
    }

    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
        // Validate the fields in your form, adding
        // adding each error to this.errors as found, e.g.

        // if ((field == null) || (field.length() == 0)) {
        //   errors.add("field", new org.apache.struts.action.ActionError("error.field.required"));
        // }
        return errors;

    }
    /**
     * @return Returns the d.
     */
    public String getD() {
        return this.d;
    }
    /**
     * @param d The d to set.
     */
    public void setD(String d) {
        this.d = d;
    }
    /**
     * @return Returns the pubCode.
     */
    public String getPubCode() {
        return this.pubCode;
    }
    /**
     * @param pubCode The pubCode to set.
     */
    public void setPubCode(String pubCode) {
        this.pubCode = pubCode;
    }
    /**
     * @return Returns the hideViewOnly.
     */
    public String getHideViewOnly() {
        return this.hideViewOnly;
    }
    /**
     * @param hideViewOnly The hideViewOnly to set.
     */
    public void setHideViewOnly(String hideViewOnly) {
        this.hideViewOnly = hideViewOnly;
    }
    /**
     * @return Returns the dataExists.
     */
    public String getDataExists() {
        return this.dataExists;
    }
    /**
     * @param dataExists The dataExists to set.
     */
    public void setDataExists(String dataExists) {
        this.dataExists = dataExists;
    }
	/**
	 * @return Returns the rte.
	 */
	public String getRte() {
		return rte;
	}
	/**
	 * @param rte The rte to set.
	 */
	public void setRte(String rte) {
		this.rte = rte;
	}
}

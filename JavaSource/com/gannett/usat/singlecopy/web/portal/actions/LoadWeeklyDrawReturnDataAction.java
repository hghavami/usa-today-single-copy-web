package com.gannett.usat.singlecopy.web.portal.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import com.usatoday.champion.handlers.RDLForWeekHandler;
import com.usatoday.champion.handlers.RouteHandler;
import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.RouteDrawManagementIntf;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;

/**
 * This Action loads the data for the selected week. It is invoked via an Ajax call so that
 * the "Please Wait" page will keep the progress image animated. Stupid IE.
 * 
 * @version 	1.0
 * @author
 */
public class LoadWeeklyDrawReturnDataAction extends Action

{

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionForward forward = null; // return value
	    StringBuffer sb = new StringBuffer("<DataLoaded>");

        try {

    	                
            String dayStr = request.getParameter("d");
            String pubCode = request.getParameter("pc");
            String viewOnly = request.getParameter("v");
            boolean returnsOnly = false;
            if (viewOnly != null && viewOnly.equalsIgnoreCase("Y")) {
                returnsOnly = true;
            }
            String rteKey = request.getParameter("rte");
            
            RouteHandler rh = (RouteHandler)request.getSession().getAttribute("route");
            UserHandler uh = (UserHandler)request.getSession().getAttribute("userHandler"); 
            UserIntf user = uh.getUser();
            
            if (pubCode == null) {
                pubCode = "UT";
            }
            
            DateTime dt = null;
            
            try {
                dt = DateTimeFormat.shortDate().parseDateTime(dayStr);
            }
            catch (Exception e) {
                dt = null;
            }

            boolean unexpectedRoute = false;
            if (rteKey != null) {
            	String compareRteKey = rh.getRoute().getMarketID() + rh.getRoute().getDistrictID() + rh.getRoute().getRouteID();
            	if (!rteKey.equalsIgnoreCase(compareRteKey)) {
            		unexpectedRoute = true;
            	}
            	
            }
            if (unexpectedRoute) {
        	    sb.append("X</DataLoaded>");
            }
            else {
	            RDLForWeekHandler oldRdl = (RDLForWeekHandler)request.getSession().getAttribute("RDLForWeek");
	            RDLForWeekHandler rdl = null;
	            if (oldRdl != null){
	                oldRdl.clear();
	                request.getSession().removeAttribute("RDLForWeek");
	                oldRdl = null;
	            }
	            
	            rdl = new RDLForWeekHandler(dt, pubCode, (RouteDrawManagementIntf)rh.getRoute(), user, returnsOnly);
	            
	            if (rdl.getLocations().size() == 0) {
	                // no data returned
	        	    sb.append("N</DataLoaded>");
	            }
	            else {
	                request.getSession().setAttribute("RDLForWeek", rdl);
	        	    sb.append("Y</DataLoaded>");
	            }
            }    	    
            response.setContentType("text/xml"); 
            response.setHeader("Cache-Control", "no-cache"); 
            response.getWriter().write(sb.toString()); 
            response.getWriter().close();

        } catch (Exception e) {

            System.out.println("SingleCopyWeb-LoadWeeklyDrawReturnDataAction() - Failed to retrieve weekly data: " + e.getMessage());
            sb.append("N</DataLoaded>");
            if (!response.isCommitted()) {
                try {
		            response.setContentType("text/xml"); 
		            response.setHeader("Cache-Control", "no-cache"); 
		            response.getWriter().write(sb.toString()); 
		            response.getWriter().close();
                }
                catch (Exception exx) {
                    ; // ignore
                }
            }
        }

        // Finish with
        return (forward);

    }
}

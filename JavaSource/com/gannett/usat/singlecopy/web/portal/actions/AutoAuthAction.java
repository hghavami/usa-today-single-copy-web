package com.gannett.usat.singlecopy.web.portal.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.champion.handlers.blueChipDraw.BlueChipDrawManagementHandler;
import com.usatoday.singlecopy.model.bo.users.UserBO;
import com.usatoday.singlecopy.model.interfaces.users.AccessRightsIntf;

/**
 * @version 	1.0
 * @author
 */
public class AutoAuthAction extends Action

{

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionForward forward = new ActionForward(); // return value

        boolean loggedIn = false;
        try {

            UserHandler userH = new UserHandler();
            String emailUID = request.getParameter("uid");
            
            if (emailUID == null ) {
                request.getSession().setAttribute("AUTO_LOGIN_FAILED", "true");
            }
            else {
	            UserBO user = UserBO.autoAuthUser(emailUID);
	            userH.setAutoAuthOnly(true);
	            userH.setUser(user);
	            
	            loggedIn = true;
	            
	            if (user.hasAccess(AccessRightsIntf.BLUE_CHIP_DRAW)) {
	                BlueChipDrawManagementHandler bcdh = (BlueChipDrawManagementHandler)request.getSession().getAttribute("blueChipDrawManagementHandler");
	                if (bcdh == null) {
	                    bcdh = new BlueChipDrawManagementHandler();
	                    bcdh.setOwningUser(user);
	                    request.getSession().setAttribute("blueChipDrawManagementHandler", bcdh);
	                }
	                else {
	                    bcdh.setOwningUser(user);
	                }
	            }
	            
            }

            request.getSession().setAttribute("userHandler", userH);

        } catch (Exception e) {

            request.getSession().setAttribute("AUTO_LOGIN_FAILED", "true");
            // 
        }

        // Write logic determining how the user should be forwarded.
        if (loggedIn) {
            forward = mapping.findForward("success");
        }
        else {
            forward = mapping.findForward("failure");
        }

        // Finish with
        return (forward);

    }
}

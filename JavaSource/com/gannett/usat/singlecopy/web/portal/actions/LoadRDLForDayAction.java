package com.gannett.usat.singlecopy.web.portal.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import com.usatoday.champion.handlers.RDLForDayHandler;
import com.usatoday.champion.handlers.RouteHandler;
import com.usatoday.champion.handlers.UserHandler;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;

/**
 * @version 	1.0
 * @author
 */
public class LoadRDLForDayAction extends Action

{

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionForward forward = null; // return value
	    StringBuffer sb = new StringBuffer("<DataLoaded>");

        try {
            String dayStr = request.getParameter("d");

            DateTime dt = null;
            
            try {
                dt = DateTimeFormat.shortDate().parseDateTime(dayStr);
            }
            catch (Exception e) {
                dt = new DateTime();
            }
            
            String rteKey = request.getParameter("rte");
            
            RouteHandler rh = (RouteHandler)request.getSession().getAttribute("route");
            UserHandler uh = (UserHandler)request.getSession().getAttribute("userHandler"); 
            UserIntf user = uh.getUser();

            boolean unexpectedRoute = false;
            if (rteKey != null) {
            	String compareRteKey = rh.getRoute().getMarketID() + rh.getRoute().getDistrictID() + rh.getRoute().getRouteID();
            	if (!rteKey.equalsIgnoreCase(compareRteKey)) {
            		unexpectedRoute = true;
            	}
            	
            }
            if (unexpectedRoute) {
        	    sb.append("X</DataLoaded>");
            }
            else {
	            RDLForDayHandler rdl = null;
	            rdl = (RDLForDayHandler)request.getSession().getAttribute("RDLForDay");
	            if (rdl != null) {
	                rdl.clear();
	                request.getSession().removeAttribute("RDLForDay");
	            }
	            
	            rdl = new RDLForDayHandler(dt.toDate(), rh.getRoute(), user);
	            
	
	            if (rdl.getLocations().size() == 0) {
	                // no data returned
	        	    sb.append("N</DataLoaded>");
	            }
	            else {
	                request.getSession().setAttribute("RDLForDay", rdl);
	        	    sb.append("Y</DataLoaded>");
	            }
            }
            response.setContentType("text/xml"); 
            response.setHeader("Cache-Control", "no-cache"); 
            response.getWriter().write(sb.toString()); 
            response.getWriter().close();
                        
        } catch (Exception e) {
            System.out.println("SingleCopyWeb-LoadRDLForDayAction() - Failed to retrieve RDL data: " + e.getMessage());
            sb.append("N</DataLoaded>");
            if (!response.isCommitted()) {
                try {
		            response.setContentType("text/xml"); 
		            response.setHeader("Cache-Control", "no-cache"); 
		            response.getWriter().write(sb.toString()); 
		            response.getWriter().close();
                }
                catch (Exception exx) {
                    ; // ignore
                }
            }
        }

        // Finish with
        return (forward);

    }
}

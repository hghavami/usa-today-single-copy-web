package com.gannett.usat.singlecopy.web.portal.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.usatoday.champion.handlers.ApplicationFlagHandler;
import com.usatoday.gannett.singlecopy.util.AS400CurrentStatus;
import com.usatoday.gannett.singlecopy.util.AS400StatusMonitor;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @version 	1.0
 * @author
 */
public class SiteUpCheckAction extends Action

{

	public static AS400StatusMonitor iSeriesMonitor = null;
	
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionForward forward = null; // return value

        try {

            boolean maintenanceMode = false; 
            try {
            	Object o = this.getServlet().getServletContext().getAttribute("applicationModeFlag");
            	if (o != null) {
            		ApplicationFlagHandler flag = (ApplicationFlagHandler)o;
            		maintenanceMode = flag.getBooleanValue();
            	}
            }
            catch (Exception exp) {
            	System.out.println("Single Copy Web: SiteUpCheckAction: Failed to read maintenance mode flag in site up check");
            }
        	
            StringBuffer returnXML = new StringBuffer("<siteup>");
            boolean currentiSeriesStatus = AS400CurrentStatus.getJdbcActive();
            if (currentiSeriesStatus && !maintenanceMode) {
            	returnXML.append("true");
            }
            else {
            	returnXML.append("false");
            }
            returnXML.append("</siteup>");
            
            response.getWriter().write(returnXML.toString());
            
            if (USATApplicationConstants.debug) {
            	System.out.println("Response Msg: " + returnXML.toString());
            }
     	   
	    	if (!SiteUpCheckAction.iSeriesMonitor.isAlive()) {
	    		SiteUpCheckAction.iSeriesMonitor.start();
	    	}

        } catch (Exception e) {

            // ignore exceptions
        	System.out.println("SiteUpCheckAction: " + e.getMessage());
        }


        // Finish with
        return (forward);

    }
}

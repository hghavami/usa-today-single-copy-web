package com.gannett.usat.singlecopy.web.portal.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.gannett.usat.singlecopy.web.portal.forms.RDLForWeekLoaderBean;
import com.usatoday.champion.handlers.MessageHandler;
import com.usatoday.champion.handlers.RDLForWeekHandler;

/**
 * This action forwards the Weekly "please wait" page to either the draw
 * management window OR the NO Data window based on what is returned from the Ajax call
 * that invokes teh LoadWeeklyDrawReturnDataAction.
 * 
 * @version 	1.0
 * @author
 */
public class ForwardRDLForWeekAction extends Action

{

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionErrors errors = new ActionErrors();
        ActionForward forward = new ActionForward(); // return value

        try {

            RDLForWeekLoaderBean aForm = (RDLForWeekLoaderBean)form;
            
            //String dayStr = aForm.getD();
            //String pubCode = aForm.getPubCode();
            //String viewMode = aForm.getHideViewOnly();
            String dataExists = aForm.getDataExists();

            RDLForWeekHandler data = (RDLForWeekHandler)request.getSession().getAttribute("RDLForWeek");
            
	        if (dataExists != null && dataExists.equalsIgnoreCase("X")) {
	        	forward = mapping.findForward("badroute");
	        	MessageHandler mh = new MessageHandler();
	        	mh.setMessage("The requested data does not match the currently selected route. Make sure you only have one Route Detail window open and try again. Or reselect the route from your Home page and try again.");
	        	mh.setShowMessage(true);
	        	request.setAttribute("requestMessage", mh);
	        }
            else if (data == null || dataExists == null || dataExists.equalsIgnoreCase("N")) {
                // send to no data page
                forward = mapping.findForward("failure");
            }
            else {
                // no JSF page
                forward = mapping.findForward("successNoJSF");
                
                // JSF Page
                //forward = mapping.findForward("success");
            }


        } catch (Exception e) {

            // Report the error using the appropriate name and ID.
            //errors.add("name", new ActionError("id"));
            forward = mapping.findForward("failure");
        }

        // If a message is required, save the specified key(s)
        // into the request for use by the <struts:errors> tag.

        if (!errors.isEmpty()) {
            saveErrors(request, errors);

            // Forward control to the appropriate 'failure' URI (change name as desired)
            forward = mapping.findForward("failure");

        }

        // Finish with
        return (forward);

    }
}

package com.gannett.usat.singlecopy.web.portal.actions;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.usatoday.singlecopy.OptOutFailedException;
import com.usatoday.singlecopy.model.bo.users.UserBO;
import com.usatoday.singlecopy.model.interfaces.users.GenericUpdateErrorIntf;

/**
 * @version 	1.0
 * @author
 */
public class EmailOptoutAction extends Action

{

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionForward forward = new ActionForward(); // return value

        String forwardAction = "success";
        try {

            String emailUID = request.getParameter("uid");
            
            emailUID = emailUID.replaceAll("'", "''");
            // invoke transaction to auto opt out
            // 
            boolean emailsRemoved = UserBO.optOutOfEmails(emailUID);

            if (!emailsRemoved) {
                forwardAction = "failure";
            }
            
        } catch (OptOutFailedException e) {
            forwardAction = "failure";
            
            Collection<GenericUpdateErrorIntf> errorMsgs = e.getErrorMessages();
            StringBuffer errMsgs = new StringBuffer();
            
            if (errorMsgs != null && !errorMsgs.isEmpty()) {
                Iterator<GenericUpdateErrorIntf> itr = errorMsgs.iterator();
                while(itr.hasNext()) {
                    GenericUpdateErrorIntf err = itr.next();
                    errMsgs.append("&nbsp;&nbsp;").append(err.getErrorMessage()).append("<br>");
                }
            }
            else {
                errMsgs.append(e.getMessage());
            }
            request.setAttribute("OptOutErrorDetail", errMsgs.toString());
            
        } catch (Exception e) {

            // Report the error using the appropriate name and ID.
            
            forwardAction = "failure";
            request.setAttribute("OptOutErrorDetail", e.getMessage());
        }

        forward = mapping.findForward(forwardAction);

        // Finish with
        return (forward);

    }
}

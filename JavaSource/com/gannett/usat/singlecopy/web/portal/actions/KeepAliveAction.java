package com.gannett.usat.singlecopy.web.portal.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.usatoday.champion.handlers.MessageHandler;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @version 	1.0
 * @author
 */
public class KeepAliveAction extends Action

{

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        
        //ActionForward forward = new ActionForward(); // return value
        ActionForward forward = null; // return value

        if (USATApplicationConstants.debug) {
            System.out.println("Keeping Alive Sesion ID: " + request.getSession().getId());
        }
        try {

        	StringBuffer sb = new StringBuffer("<KeepAlive><SessionKeepAlive>true</SessionKeepAlive>");

        	MessageHandler mh = (MessageHandler)request.getSession().getServletContext().getAttribute("globalMessage");
        	if (mh != null && mh.getShowMessage().booleanValue() && mh.showToContractors()) {
        		sb.append("<GlobalMesssageActive>true</GlobalMesssageActive>");
        		sb.append("<GlobalMesssageDetail>");
        		sb.append(mh.getMessage());
        		sb.append("</GlobalMesssageDetail>");
        	}
        	else {
        		sb.append("<GlobalMesssageActive>false</GlobalMesssageActive>");
        		sb.append("<GlobalMesssageDetail></GlobalMesssageDetail>");
        	}
        	sb.append("</KeepAlive>");
        	try {
        	    response.setContentType("application/xml"); 
        	    response.setHeader("Cache-Control", "no-cache"); 
        	    response.getWriter().write(sb.toString()); 
        	    response.getWriter().close();
        	}
        	catch (Exception e) {
        		System.out.println(e.getMessage());
        	}

        } catch (Exception e) {

            // Report the error using the appropriate name and ID.
            //errors.add("name", new ActionError("id"));

        }

        // Finish with return null
        return (forward);
    }
}

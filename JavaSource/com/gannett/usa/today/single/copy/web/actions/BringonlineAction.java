package com.gannett.usa.today.single.copy.web.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.usatoday.champion.handlers.ApplicationFlagHandler;

/**
 * @version 	1.0
 * @author
 */
public class BringonlineAction extends Action

{

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionErrors errors = new ActionErrors();
        ActionForward forward = new ActionForward(); // return value

        try {

            // do something here
        	ApplicationFlagHandler flag = (ApplicationFlagHandler)request.getSession().getServletContext().getAttribute("applicationModeFlag");
        	if (flag != null) {
        		flag.setBooleanValue(false);
        		System.out.println("Single Copy Web:  Application taken out of Maintenance Mode.");
        	}
        	else {
        		System.out.println("Single Copy Web:  Attempt to take application out of Maintenance Mode but no flag found in application context.");        		
        	}
        } catch (Exception e) {

            // Report the error using the appropriate name and ID.

        }

        // If a message is required, save the specified key(s)
        // into the request for use by the <struts:errors> tag.

        if (!errors.isEmpty()) {
            saveErrors(request, errors);
        }
        // Write logic determining how the user should be forwarded.
        forward = mapping.findForward("success");

        // Finish with
        return (forward);

    }
}
